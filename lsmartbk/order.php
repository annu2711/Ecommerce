<?php 
    include_once "helpers/index.php";
    $edit = "";
    $dataarr = [];
    if(isset($_GET['from'])){
        extract($_GET);
        $edit = 1;
        $subquery = "";
        if(!empty($from)){
            $subquery = " t1.created_at >= '$from'";
        }
        if(!empty($to)){
            if(empty($subquery)){
                $subquery = " t1.created_at <= '$to'";
            }else{
                $subquery = $subquery." AND t1.created_at <= '$to'";
            }
        }
        if(!empty($orderid)){
            if(empty($subquery)){
                $subquery = " order_key='$orderid'";
            }else{
                $subquery = $subquery." AND order_key='$orderid'";
            }
        }
        if(!empty($subquery)){
            $query = mysqli_query($conn, "SELECT t1.order_id, t1.order_key, t1.amount, t1.payment, t1.payment_status, t1.d_status, t1.created_at, t2.title, t2.name, t2.last_name FROM orders as t1 join clients as t2 on t1.user=t2.client_key WHERE $subquery ORDER BY t1.order_id DESC");
            if($query){
                if(mysqli_num_rows($query) > 0){
                    while($rows = mysqli_fetch_array($query)){
                        print_r($rows);
                        $dataarr[] = $rows;
                    }
                }
            }else{
                echo mysqli_error($conn);
            }
        }
        
    }
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Order List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Orders</a></li>
                    <li class="breadcrumb-item active">Order List</li>
                </ul>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Filter</div>
                    <div class="body">
                        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <label>From</label>
                                <input type="date" class="form-control" name="from" <?php echo (!empty($edit)) ? 'value="'.$from.'"' : '' ; ?>>
                            </div>
                            <div class="col-md-4">
                                <label>From</label>
                                <input type="date" class="form-control" name="to" <?php echo (!empty($edit)) ? 'value="'.$to.'"' : '' ; ?>>
                            </div>
                            <div class="col-md-4">
                                <label>Order Id</label>
                                <input type="text" class="form-control" name="orderid" <?php echo (!empty($edit)) ? 'value="'.$orderid.'"' : 'value=""' ; ?>>
                            </div>
                            <div class="col-md-12 mt-4" align="right">
                                <button type="submit" class="btn btn-success">Apply Filter</button>
                                <a href="order.php" class="btn btn-danger">Reset Filter</a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Order #</th>
                                        <th>Customer</th>
                                        <th>Payment</th>
                                        <th>Amount</th>
                                        <th>Shiping Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(empty($edit)){
                                        $query = mysqli_query($conn, "SELECT t1.order_id, t1.order_key, t1.amount, t1.payment, t1.payment_status, t1.d_status, t1.created_at, t2.title, t2.name, t2.last_name FROM orders as t1 join clients as t2 on t1.user=t2.client_id ORDER BY order_id DESC");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_array($query)){
                                                    $dataarr[] = $rows;
                                                }
                                            }
                                        }
                                    }

                                                if(!empty($dataarr)){
                                                    for($i = 0; $i < count($dataarr); $i++){
                                                        if($dataarr[$i][4] != 'Uncomplete'){ ?>
                                                            <tr>
                                                                <td><?php $date = explode(" ", $dataarr[$i][6]); echo $date[0]; ?></td>
                                                                <td><?php echo $dataarr[$i][1] ?></td>
                                                                <td><?php echo $dataarr[$i][7]." ".$dataarr[$i][8]." ".$dataarr[$i][9] ?></td>
                                                                <td><?php echo $dataarr[$i][3] ?></td>
                                                                <td><?php echo $dataarr[$i][2] ?></td>
                                                                <td><?php echo orderStatus($dataarr[$i][5]) ?></td>
                                                                <td>
                                                                <a href="order-detail.php?order=<?php echo $dataarr[$i][0] ?>" class="btn btn-primary waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="View Invoice Details"><i class="material-icons">remove_red_eye</i></a>
                                                                <a href="../helpers/pdfmaker.php?invoice=<?php echo $dataarr[$i][1] ?>" class="btn btn-info waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="Download Invoice"><i class="material-icons">picture_in_picture_alt</i></a>
                                                                </td>
                                                            </tr>
                                                           <?php   
                                                            }else{
                                                            ?>
                                                <tr>
                                                <td><?php $date = explode(" ", $dataarr[$i][6]); echo $date[0]; ?></td>
                                                                <td><?php echo $dataarr[$i][1] ?></td>
                                                                <td><?php echo $dataarr[$i][7]." ".$dataarr[$i][8]." ".$dataarr[$i][9] ?></td>
                                                                <td><?php echo $dataarr[$i][3] ?></td>
                                                                <td><?php echo $dataarr[$i][2] ?></td>
                                                                <td><?php echo orderStatus($dataarr[$i][5]) ?></td>
                                                                <td>
                                                                <a href="order-detail.php?order=<?php echo $dataarr[$i][0] ?>" class="btn btn-primary waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="View Invoice Details"><i class="material-icons">remove_red_eye</i></a>
                                                                <a href="../helpers/pdfmaker.php?invoice=<?php echo $dataarr[$i][1] ?>" class="btn btn-info waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="Download Invoice"><i class="material-icons">picture_in_picture_alt</i></a>
                                                                </td>
                                                    </tr>
                                                <?php    }
                                                }
                                            }
                                                ?>   
                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<!-- Large Size -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Order Detail</h4>
            </div>
            <div class="modal-body"> 
                <table class="table table-bordered table-striped table-hover" id="order-table">
                    <thead>
                    <tr>
                        <th>Product Img</th>
                        <th>Product</th>
                        <th>Po No.</th>
                        <th>Color</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody></tbody><tfoot><tr><td colspan="5" align="right">Total</td><td><strong id="total"></strong></td></tr></tfoot>
                </table>
                <hr>
                <input type="hidden" name="orderid" id="orderid" value="">
                <div class="row">
                    <div class="col-md-6">
                        <label>Order Dispatched Date</label>
                        <input type="date" class="form-control" name="date">
                    </div>
                    <div class="col-md-6">
                        <label>Order Status</label>
                        <select class="form-control" name="status">
                            <option value="1">Dispatched</option>
                            <option value="5">Cancel</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="order_status" class="btn btn-success btn-round waves-effect">Order Dispatched</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    function getOrderDetail(x){
        if(x != ""){
            $.ajax({
                type: 'post',
                url: 'helpers/event.php',
                data: {orderdetail: x},
                success: function(data){
                    // alert(data);
                    $('#order-table tbody').html(" ");
                    $('#total').text(" ");
                    $('#order-table tbody').html(data);
                    var price = 0;
                    $('.order-price').each(function(e){
                        price = price + parseInt($(this).text());
                    });
                    $('#total').text(price);
                    $('#orderid').val(" ");
                    $('#orderid').val(x);
                }
            })
        }
    }

    // function tableTotal(tbl){

    // }
</script>
<?php include_once "helpers/footer.php";

if(isset($_POST['order_status'])){
    extract($_POST);
    if(!empty($orderid) && !empty($date) && !empty($status)){
        mysqli_autocommit($conn, FALSE);
        $query = mysqli_query($conn, "UPDATE `orders` SET `d_status`='$status' WHERE `order_key`='$orderid'");
        if($query){
            $sql = mysqli_query($conn, "INSERT INTO orders_status (orderid, status, status_date) VALUES ('$orderid', '$status', '$date')");
            if($sql){
            mysqli_commit($conn);
            $mobile = getSinglevalue('clients', 'client_key', getSinglevalue('orders', 'order_key', $orderid, 2), 6);
            $msg = mobilesms($mobile, 'Your Order Dispatched Today. Order Id:'.$orderid);
            echo "<script>showNotification('alert-info', 'Status Changed Successfully', 'top', 'right', '', '')</script>";
            header('refresh: 0.5');
        }else{
            mysqli_rollback($conn);
        }
        }else{
            echo "<script>showNotification('alert-danger', 'Status Not Changed ', 'top', 'right', '', '')</script>";
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


?>