<?php 
include_once"helpers/index.php"; 

$edit = "";
    // if(isset($_GET['sizes'])){
    //     extract($_GET);
    //     if(!empty($sizes)){
    //     $sql = mysqli_query($conn, "SELECT * FROM size WHERE size_id='$sizes'");
    //     if($sql){
    //         if(mysqli_num_rows($sql) > 0){
    //             $array = mysqli_fetch_array($sql);
    //             $edit = 1;
                
    //         }
    //     }
    //     }
    // }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Home Page Slider
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Home Page Slider</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group form-float">
                                <label>Sections</label>
                                <select class="form-control" name="section">
                                    <option value="">-- Select Section --</option>
                                    <option value="1">Section One</option>
                                    <option value="2">Section Two</option>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Title"'; ?>  required>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Subtitle</label>
                                <input type="text" class="form-control" name="subtitle" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Subtitle"'; ?>  required>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Page Link</label>
                                <input type="text" class="form-control" name="link" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Page Link"'; ?>>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Image One<sup>*</sup></label>
                                <input type="file" name="primary_img" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" required>

                                <p class="text-danger"><strong>Note:</strong> Please use 515px x 287px image for better quality.</p>
                            </div>

                            <div class="form-group form-float">
                                <label>Image Two<sup>*</sup></label>
                                <input type="file" name="primary_img" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" required>

                                <p class="text-danger"><strong>Note:</strong> Please use 409px x 696px image for better quality.</p>
                            </div>

                            <div class="form-group form-float">
                                <label>Image Three<sup>*</sup></label>
                                <input type="file" name="primary_img" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" required>

                                <p class="text-danger"><strong>Note:</strong> Please use 270px x 520px image for better quality.</p>
                            </div>
                          
                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[2] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[2] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_size">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_size">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Size</strong> List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Size</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM size");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['size'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="size?sizes=<?php echo $rows['size_id'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once"helpers/footer.php"; 

if(isset($_POST['add_size'])){
    extract($_POST);
    if(!empty($size)){
        $chck_brand = check_duplicate('size', 'size', $size);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Category already exists', 'top', 'right', '', '')</script>";
        }else{
            $query = mysqli_query($conn, "INSERT INTO size (size, status, created_at) VALUES ('$size', 1, '$created_at')");
            if($query){
                echo "<script>showNotification('alert-info', 'Size Added Successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');

            }else{
                echo "<script>showNotification('alert-danger', 'Size Not Added ', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}




if(isset($_POST['update_size'])){
    extract($_POST);
    if(!empty($size)){
            $query = mysqli_query($conn, "UPDATE size SET size='$size', status='$status' WHERE size_id='$sizes'");
            if($query){
                echo "<script>showNotification('alert-info', 'size Updated Successfully', 'top', 'right', '', '')</script>";
                header('location: size');
            }else{
                echo "<script>showNotification('alert-danger', 'size Not Updated ', 'top', 'right', '', '')</script>";
            }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>