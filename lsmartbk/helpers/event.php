<?php 
date_default_timezone_set('Asia/Kolkata');
include_once "config.php";
include_once "shiprocket.php";
$created_at = date('Y-m-d H:i:s');

// function rand_char($size){
//     $length = $size;
//     $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $length);
//     return $randomletter;
//     }


function getSubcategory($category){
    $conn = $GLOBALS['conn'];
    if(!empty($category)){
        $query = mysqli_query($conn, "SELECT subcat_key, subcategory FROM subcategory WHERE cat_key='$category' AND status=1");
        if($query){
            while($rows = mysqli_fetch_assoc($query)){
                echo '<option value="'.$rows['subcat_key'].'">'.$rows['subcategory'].'</option>';
            }
        }
    }
}


// function getValueSum($tbl, $sumof, $clm="", $val="", $clm2="", $val2="", $clm3="", $val3=""){
//     $conn = $GLOBALS['conn'];
//     $where = "";
//     if($clm!="" && $val!=""){
//         $where = "WHERE $clm='$val'";
//     }
//     if($clm2!="" && $val2!=""){
//         $where = $where." AND $clm2='$val2'";
//     }
//     if($clm3!="" && $val3!=""){
//         $where = $where." AND $clm3='$val3'";
//     }
//     $sql = mysqli_query($conn, "SELECT SUM($sumof) as qty FROM $tbl $where");
//     if($sql){
//         if(mysqli_num_rows($sql) > 0){
//             $row = mysqli_fetch_assoc($sql);
//             return (!empty($row['qty'])) ? $row['qty'] : 0 ;
//             // echo intval($row['qty']);
//         }else{
//             return 0;
//         }
//     }else{
//         return 0;
//     }
// }


// function orderedQty($product, $color="", $size=""){
//     $conn = $GLOBALS['conn'];
//     $clr = "";
//     $pdt = "";
//     if($product!=""){
//         $pdt = "AND t1.product='$product'";
//     }
//     if($color!=""){
//         $clr = "AND t1.color='$color'";
//     }
//     $sze = "";
//     if($size!=""){
//         $sze = "AND t1.size='$size'";
//     }
// $query = mysqli_query($conn, "SELECT SUM(t1.qty) as qty FROM order_book as t1 JOIN orders as t2 on t1.order_key=t2.order_key WHERE t2.d_status=0 $pdt $clr $sze");
// if($query){
//     if(mysqli_num_rows($query) > 0){
//         $rows = mysqli_fetch_assoc($query);
//         return (!empty($rows['qty'])) ? $rows['qty'] : 0 ;
//     }else{
//         return 0;
//     }
// }else{
//     return mysqli_error($conn);
// }

// }  

// function getSinglevalue($tbl, $clm, $sign, $f_value, $xyz=''){
// 	$conn = $GLOBALS['conn'];
//     $query = mysqli_query($conn, "SELECT * FROM $tbl WHERE $clm='$sign' LIMIT 1");
//     if (mysqli_num_rows($query) > 0) {
//         $rows = mysqli_fetch_array($query);
//         $val = $rows[$f_value];
//         if ($val!="") {
//         	return $val;
//         }else{
//         	return $xyz;
//         }
//     }else{
//     	return $xyz;
//     }
//     }

// Get Brand
if(isset($_POST['store'])){
    extract($_POST);
    if(!empty($store)){
        $query = mysqli_query($conn, "SELECT brand_key, brand_name FROM brands WHERE store_key='$store' AND status=1");
        if($query){
            while($rows = mysqli_fetch_assoc($query)){
                echo '<option value="'.$rows['brand_key'].'">'.$rows['brand_name'].'</option>';
            }
        }
    }
}


// Get Subcategory
if(isset($_POST['category'])){
    extract($_POST);
    getSubcategory($category);
}

//get submenu
if(isset($_POST['menu'])){
    extract($_POST);
    if(!empty($menu)){
        $query = mysqli_query($conn, "SELECT category FROM menu WHERE menu_id='$menu'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                $row = mysqli_fetch_assoc($query);
                getSubcategory($row['category']);
            }
        }
    }
}

// Quick View Starts
if(isset($_POST['product'])){
    extract($_POST);
    if(!empty($product)){
        $query = mysqli_query($conn, "SELECT p_key, product_name, price, p_img, (SELECT brand_name FROM brands WHERE brand_key=products_db.brand AND products_db.brand!='') as brand_name FROM products_db WHERE p_key='$product' ORDER BY p_id DESC");
        if($query){
            $pd = mysqli_fetch_array($query);
            $color = [];
            $quantity = '';
            // get colors
            $sql = mysqli_query($conn, "SELECT color FROM product_detail WHERE product='$product' GROUP BY color");
            if($sql){
                if(mysqli_num_rows($sql) > 0){
                    while($colors = mysqli_fetch_assoc($sql)){
                        $color[] = $colors['color'];
                    }
                        $size = getSinglevalue('product_detail', 'pd_id', $color[0], 3);
                        if(empty($size)){
                            $color_qty = getValueSum('product_detail', 'qty', 'color', $color[0], 'product', $product);
                            $cart_qty = getValueSum('user_cart', 'qty', 'color', $color[0], 'product', $product);
                            $order_qty = orderedQty($product, $color[0]);
                            $quantity = $color_qty - $cart_qty - $order_qty;
                        }
                }
            }
            $first_color = $color[0];
            // get size
            $sizes = [];
            $sql = mysqli_query($conn, "SELECT DISTINCT(size) as size FROM product_detail WHERE color='$first_color' AND product='$product'");
            if($sql){
                if(mysqli_num_rows($sql) > 0){
                    while($rows = mysqli_fetch_assoc($sql)){
                        $sizes[] = $rows['size'];
                    }
                    $size_qty = getValueSum('product_detail', 'qty', 'color', $first_color, 'product', $product, 'size', $sizes[0]);
                    $cart_qty = getValueSum('user_cart', 'qty', 'color', $first_color, 'product', $product, 'size', $sizes[0]);
                    $order_qty = orderedQty($product, $first_color, $sizes[0]);
                    $quantity = $size_qty - $cart_qty - $order_qty ;
                }else{
                    // $sizes[] = mysqli_num_rows($sql);
                }
            }else{
                // $sizes[] = mysqli_error($conn);
            }
            $product_detail_array = ['product' => $pd, 'colors' => $color, 'size' => $sizes, 'quantity' => $quantity];
            echo json_encode(($product_detail_array));

        }else{
            echo json_encode(mysqli_error($conn));
        }
    }else{
        echo json_encode("Empty");
    }
    // echo json_encode("hii");
}
// Quick View End

if(isset($_POST['filter'])){
    extract($_POST);
    if(!empty($filter)){
        $category = "";
        $subcategory = "";
        $categoryelse = "";
        $subcategoryelse = "";
        $preferred = "";
        $preferredelse = "";
        $pass = 1;
        
        if(!empty($cat)){
            $category = "AND t1.category='$cat'";
            $categoryelse = "AND t2.category='$cat'";
        }
        if(!empty($subcat)){
            $subcategory = "AND t1.subcategory='$subcat'";
            $subcategoryelse = "AND t2.subcategory='$subcat'";
        }
        if(!empty($gender)){
            if($gender == 'Men' || $gender == 'Women'){
            $preferred = "AND t2.gender IN ('$gender', 'Both')";
            $preferredelse = "AND t1.gender IN ('$gender', 'Both')";
            }else{
            $preferred = "AND t2.gender='$gender'";
            $preferredelse = "AND t1.gender='$gender'";
            }
        }
        if(empty($sort)){
            $sorting = "ORDER BY t1.price ASC";
            $sortingelse = "ORDER BY t2.price ASC";
        }else{
            $sorting = "ORDER BY t1.price DESC";
            $sortingelse = "ORDER BY t2.price DESC";
        }
        $productdata = [];
        $error = [];
        // brand starts
        if(!empty($brand) && !empty($pass)){
            $brands = "";
            foreach($brand as $c => $d){
                if(empty($brands)){
                    $brands = $brands."'".$d."'";
                }else{
                    $brands = $brands.","."'".$d."'";
                }
            }
            $group = "";
            // $group = "GROUP BY t2.color, IF(t2.color='', t2.p_id, 0)";
            if(empty($color) && empty($size)){
                $group = "GROUP BY t2.color,t2.groupid";
            }
            // , IF(t2.color='', t2.p_id, 0)
            // foreach($brand as $c => $d){
                $query = mysqli_query($conn,"SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' $category $subcategory $preferred AND t1.brand IN ($brands) $group $sorting");
                $querycheck[] = "SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' $category $subcategory $preferred AND t1.brand IN ($brands) $group $sorting";
                if($query){
                    if(mysqli_num_rows($query) > 0){
                        while($rows = mysqli_fetch_assoc($query)){
                            $productdata[] = $rows['sku'];
                        }
                    }else{
                        $pass = 0;
                    }
                }else{
                    $error[] = 'error1=>'.mysqli_error($conn);
                }
            // }
        }
        // brand Ends
        // color starts
        // $querycheck = [];
        if(!empty($color) && !empty($pass)){
            if(!empty($productdata)){
                $uniqueKey = array_unique($productdata);
                $p_id = "";
                foreach($uniqueKey as $u => $v){
                    if(empty($p_id)){
                        $p_id = "'".$v."'";
                    }else{
                        $p_id = $p_id.","."'".$v."'";
                    }
                }
                $productdata = [];
            }else{
                $p_id="";
            }
            $colors = "";
            foreach($color as $c => $d){
                if(empty($colors)){
                    $colors = $colors."'".$d."'";
                }else{
                    $colors = $colors.","."'".$d."'";
                }
            }
            $group = "";
            if(empty($size)){
                $group = "GROUP BY t1.color,t1.groupid";
            }
            // foreach($color as $c => $d){
                if(!empty($p_id)){
                    $product_list = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.color IN ($colors) AND t1.p_id IN ($p_id) $group $sortingelse";
                    $querycheck[] = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.color IN ($colors) AND t1.p_id IN ($p_id) $group $sortingelse";
                }else{
                    $product_list = "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.color IN ($colors) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                    $querycheck[]=  "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.color IN ($colors) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                }
                $query = mysqli_query($conn, $product_list);
                if($query){
                    if(mysqli_num_rows($query) > 0){
                        while($rows = mysqli_fetch_assoc($query)){
                            $productdata[] = $rows['p_id'];
                        }
                    }else{
                        $pass = 0;
                    }
                }else{
                    $error[] = 'error2=>'.mysqli_error($conn);
                }
            // }
            }else{
            $color = "";
        }
        // color ends 
        // size starts
        if(!empty($size) && !empty($pass)){
            if(!empty($productdata)){
                $uniqueKey = array_unique($productdata);
                $p_id = "";
                foreach($uniqueKey as $u => $v){
                    if(empty($p_id)){
                        $p_id = $p_id."'".$v."'";
                    }else{
                        $p_id = $p_id.","."'".$v."'";
                    }
                }
                $productdata = [];
            }else{
                $p_id="";
            }
            $sizes = "";
            foreach($size as $c => $d){
                if(empty($sizes)){
                    $sizes = $sizes."'".$d."'";
                }else{
                    $sizes = $sizes.","."'".$d."'";
                }
            }
            if(!empty($p_id)){
                    $product_list = "AND product IN ($p_id)";
                }else{
                    $product_list = "";
                }
            // foreach($size as $c => $d){
                // if(!empty($p_id)){
                //     $product_list = "AND product IN ($p_id)";
                // }else{
                //     $product_list = "";
                // }
                $group = "GROUP BY t1.color,t1.groupid";
                // , IF(t1.color='', t1.p_id, 0)
                if(!empty($p_id)){
                    $product_list = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.size IN ($sizes) AND t1.p_id IN ($p_id) $group $sortingelse";
                    $querycheck[] = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.size IN ($sizes) AND t1.p_id IN ($p_id) $group $sortingelse";
                }else{
                    $product_list = "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.size IN ($sizes) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                    $querycheck[]= "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.size IN ($sizes) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                }

                $query = mysqli_query($conn, $product_list);
                if($query){
                    if(mysqli_num_rows($query) > 0){
                        while($rows = mysqli_fetch_assoc($query)){
                            $productdata[] = $rows['p_id'];
                        }
                    }else{
                        $pass = 0;
                    }
                }else{
                    $error[] = mysqli_error($conn);
                }
            // }
        }
        // size ends
        
        //storage starts
        if(!empty($storage) && !empty($pass)){
            if(!empty($productdata)){
                $uniqueKey = array_unique($productdata);
                $p_id = "";
                foreach($uniqueKey as $u => $v){
                    if(empty($p_id)){
                        $p_id = $p_id."'".$v."'";
                    }else{
                        $p_id = $p_id.","."'".$v."'";
                    }
                }
                $productdata = [];
            }else{
                $p_id="";
            }
            $storages = "";
            foreach($storage as $c => $d){
                if(empty($storages)){
                    $storages = $storages."'".$d."'";
                }else{
                    $storages = $storages.","."'".$d."'";
                }
            }
            
            if(!empty($p_id)){
                    $product_list = "AND product IN ($p_id)";
                }else{
                    $product_list = "";
                }
                
            $group = "GROUP BY t1.color,t1.groupid";
            // , IF(t1.color='', t1.p_id, 0)
            if(!empty($p_id)){
                    $product_list = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.storage IN ($storages) AND t1.p_id IN ($p_id) $group $sortingelse";
                    $querycheck[] = "SELECT t1.p_id FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.storage IN ($storages) AND t1.p_id IN ($p_id) $group $sortingelse";
                }else{
                    $product_list = "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.storage IN ($storages) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                    $querycheck[]= "SELECT DISTINCT(t1.p_id) FROM product_attributes as t1 join product as t2 on t1.p_id=t2.sku WHERE t1.storage IN ($storages) $categoryelse $subcategoryelse $preferredelse $group $sortingelse";
                }
                
                $query = mysqli_query($conn, $product_list);
                if($query){
                    if(mysqli_num_rows($query) > 0){
                        while($rows = mysqli_fetch_assoc($query)){
                            $productdata[] = $rows['p_id'];
                        }
                    }else{
                        $pass = 0;
                    }
                }else{
                    $error[] = mysqli_error($conn);
                }
                
        }
        //storage ends

        $finaldata = [];
        if(!empty($min) && !empty($max)){
            // check filters
            if(!empty($color) || !empty($brand) || !empty($size) || !empty($storage)){
            if(!empty($productdata)){
                    $uniqueKey = array_unique($productdata);
                    foreach($uniqueKey as $u => $v){
                        $query = mysqli_query($conn, "SELECT sku FROM product WHERE publish='1' AND price >= '$min' AND price <= '$max' AND sku='$v'  GROUP BY groupid");
                        $querycheck[] = "SELECT sku FROM product WHERE publish='1' AND price >= '$min' AND price <= '$max' AND sku='$v'  GROUP BY groupid";
                        if($query){
                            if(mysqli_num_rows($query) > 0){
                                $row = mysqli_fetch_assoc($query);
                                $finaldata[] = $row['sku'];
                            }
                        }else{
                            $error[] = mysqli_error($conn);
                        }
                    }
                }
            }else{
                if(empty($sort)){
                    $query = mysqli_query($conn, "SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' AND t1.price >= '$min' AND t1.price <= '$max' $category $subcategory $preferred GROUP BY t2.color,t2.groupid ORDER BY t1.price ASC");
                    // , IF(t2.color='', t2.p_id, 0)
                    $querycheck[] = "Q=>"."SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' AND t1.price >= '$min' AND t1.price <= '$max' $category $subcategory $preferred GROUP BY t1.groupid ORDER BY t1.price ASC";
                }else{
                    $query = mysqli_query($conn, "SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' AND t1.price >= '$min' AND t1.price <= '$max' $category $subcategory $preferred GROUP BY t2.color,t2.groupid ORDER BY t1.price DESC");
                    // , IF(t2.color='', t2.p_id, 0)
                    $querycheck[] = "Q=>1"."SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish='1' AND t1.price >= '$min' AND t1.price <= '$max' $category $subcategory $preferred GROUP BY t1.groupid ORDER BY t1.price DESC";
                }
                    
                    if($query){
                        $rws = mysqli_num_rows($query);
                        if(mysqli_num_rows($query) > 0){
                            while($row = mysqli_fetch_assoc($query)){
                                $finaldata[] = $row['sku'];
                            }
                        }
                    }else{
                        $error[] = mysqli_error($conn);
                    }
            }
        }
        $newdata = $finaldata;
        $data = [];
        $totalproduct = "";
        if(!empty($finaldata)){
            $p_ids = array_unique($finaldata);
            $totalproduct = count($p_ids);
            $product_count = count($p_ids);
            if($page!= ""){
                $product_count = $product_count - ($page * 12 );
                $index = $page * 12;
            }else{
                $index = 0;
            }

            if($page!= ""){
                if($product_count > 12){
                    $loop = ($page * 12) + 12;
                }else{
                    $loop = $totalproduct;
                }
            }else{
                if($product_count > 12){
                    $loop = 12;
                }else{
                    $loop = $product_count;
                }
            }
        
            for($i = $index; $i <= $loop - 1 ; $i++){
                $p_id = $p_ids[$i];
                $query = mysqli_query($conn, "SELECT p_key, sku, product_name, mrp, price, (select image from product_images where product_images.product=product.sku order by pi_id ASC limit 1) as image_a, (select image from product_images where product_images.product=product.sku order by pi_id ASC limit 1, 1) as image_b, (select brand_name FROM brands WHERE product.brand=brands.brand_key) as brand FROM product WHERE sku='$p_id'");
                $querycheck[] = "SELECT p_key, sku, product_name, mrp, price, (select image from product_images where product_images.product=product.sku order by pi_id ASC limit 1) as image_a, (select image from product_images where product_images.product=product.sku order by pi_id ASC limit 1, 1) as image_b, (select brand_name FROM brands WHERE product.brand=brands.brand_key) as brand FROM product WHERE sku='$p_id'";
                if($query){
                    if(mysqli_num_rows($query) > 0){
                $products = mysqli_fetch_array($query);
                $f_image = $products[5];
                if(file_exists('../../products/'.$f_image)){
                    $products[5] = $f_image;
                }else{
                    $products[5] = 'no-product.png';
                }
                $data[] = $products;
                    }
                }else{
                    $error[] = mysqli_error($conn);
                }
            }
        }
        echo json_encode([ 'productdata' => $data, 'productcount' => $totalproduct, 'query' => $querycheck, 'error' => $productdata, 'filter' => $rws]);
    }
}




// get more data

if(isset($_POST['con'])){
    extract($_POST);
    $urlarray = explode("_", $con);
    $category = "";
    $subcategory = "";
    $product = "";
    $product_list = "";
    $quote = "'";
    $prf="";
    $pstatus = "";
    $catarray = explode("-",$urlarray[0]);
    if($catarray[0]!= ""){
        $category = "AND t1.category='".$catarray[0]."'";
    }
    if($catarray[1]!= ""){
        $subcategory = "AND t1.subcategory='".$catarray[1]."'";
    }
    if($urlarray[2]!= ""){
        if($urlarray[2] === 'Men' || $urlarray[2] === 'Women'){
			$prf = "AND t2.gender IN ('".$urlarray[2]."', 'Both')";
		}else{
			$prf = "AND t2.gender='".$urlarray[2]."'";
		}
    }
    $status = explode("!",$urlarray[1]);
    if($status[1]!= ""){
        $pstatus = "AND status='$status[1]'";
    }
    if(empty($sort)){
        $sorting = "ORDER BY t1.price ASC";
        $sortingelse = "ORDER BY t2.price ASC";
    }else{
        $sorting = "ORDER BY t1.price DESC";
        $sortingelse = "ORDER BY t2.price DESC";
    }
    $offset = $page * 12;
    $query = mysqli_query($conn, "SELECT t1.p_key, t1.sku, t1.product_name, t1.mrp, t1.price, (select image from product_images where product_images.product=t1.sku order by pi_id ASC limit 1) as image_a, (select image from product_images where product_images.product=t1.sku order by pi_id ASC limit 1, 1) as image_b, (select brand_name FROM brands WHERE t1.brand=brands.brand_key) as brand FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $category $subcategory $prf $pstatus GROUP BY t2.color,t2.groupid $sorting LIMIT $offset, 12");
    if($query){
        $productdata = [];
        if(mysqli_num_rows($query) > 0){
            while($products = mysqli_fetch_array($query)){
                $f_image = $products[5];
                if(file_exists('../../products/'.$f_image)){
                    $products[5] = $f_image;
                }else{
                    $products[5] = 'no-product.png';
                }
                $productdata[] = $products;
            }
        }
    }
    echo json_encode($productdata);
}


// brand page data
if(isset($_POST['brandcon'])){
    extract($_POST);
    $urlarray = explode("_", $brandcon);
    $category = "";
    $brand = "";
    $product_list = "";
    $quote = "'";
    $prf="";
    $pstatus = "";
    if($urlarray[2]!= ""){
        $category = "AND t1.category='".$urlarray[2]."'";
    }
    if($urlarray[0]!= ""){
        $bd = getSinglevalue('brands', 'brand_name', $urlarray[0], 1);
        $brand = "AND t1.brand='".$bd."'";
    }
    if($urlarray[1]!= ""){
        if($urlarray[1] === 'Men' || $urlarray[1] === 'Women'){
			$prf = "AND t2.gender IN ('".$urlarray[1]."', 'Both')";
		}else{
			$prf = "AND t2.gender='".$urlarray[1]."'";
		}
    }
    if(empty($sort)){
        $sorting = "ORDER BY t1.price ASC";
        $sortingelse = "ORDER BY t2.price ASC";
    }else{
        $sorting = "ORDER BY t1.price DESC";
        $sortingelse = "ORDER BY t2.price DESC";
    }
    
    $offset = $page * 12;
   $query = mysqli_query($conn, "SELECT t1.p_key, t1.sku, t1.product_name, t1.mrp, t1.price, (select image from product_images where product_images.product=t1.sku order by pi_id ASC limit 1) as image_a, (select image from product_images where product_images.product=t1.sku order by pi_id ASC limit 1, 1) as image_b, (select brand_name FROM brands WHERE t1.brand=brands.brand_key) as brand FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $brand $category $prf GROUP BY t2.color,t1.groupid $sorting LIMIT $offset, 12");
    $sl = "";
    if($query){
        $productdata = [];
        if(mysqli_num_rows($query) > 0){
            while($products = mysqli_fetch_array($query)){
                $f_image = $products[5];
                if(file_exists('../../products/'.$f_image)){
                    $products[5] = $f_image;
                }else{
                    $products[5] = 'no-product.png';
                }
                $productdata[] = $products;
            }
        }
    }else{
        $error = mysqli_error($conn);
    }
    echo json_encode(['data' => $productdata, 'error' => $error, 'query' => $sl]);
}

// cart event

if(isset($_POST['cartquantity'])){
    extract($_POST);
    if(!empty($cartquantity)){
        $totalavailablestock = 0;
        $soldstock = 0;
        $capital = 0;
        // capital quantityt
        // $query = mysqli_query($conn, "SELECT SUM(stock) as qty FROM product WHERE sku='$cartquantity'");
        // if($query){
        //     if(mysqli_num_rows($query) > 0){
        //         $row = mysqli_fetch_assoc($query);
        //         $capital = $capital + $row['qty'];
        //     }
        // }
        $sql = mysqli_query($conn, "SELECT SUM(qty) as qty FROM product_stock WHERE product='$cartquantity'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $row = mysqli_fetch_assoc($sql);
                $totalavailablestock = $totalavailablestock + $row['qty'];
            }
        }

        $sql2 = mysqli_query($conn, "SELECT SUM(qty) as qty FROM order_book WHERE product='$cartquantity'");
        if($sql2){
            if(mysqli_num_rows($sql2) > 0){
                $row = mysqli_fetch_assoc($sql2);
                $soldstock = $row['qty'];
            }
        }
<<<<<<< HEAD

        // $cart_data // it is cart array
=======
        
        // $cart_data // it is cart array
        if(isset($_COOKIE['shopping_cart'])){
            $cookie_data = stripcslashes($_COOKIE['shopping_cart']);
            $cart_data = json_decode($cookie_data, true);
        }else{
            $sql = mysqli_query($conn, "SELECT * FROM user_cart WHERE user='$id'");
    if($sql){
        $cart_data = [];
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $totalcartquantity = $totalcartquantity + intval($rows[6]);
                $totalcartamount = $totalcartamount + (intval($rows[6]) * getSinglevalue('product', 'sku', $rows[3], 10));
                $cart_data[] = ['cartid' => $rows[1], 'product' => $rows[3], 'color' => $rows[4], 'size' => $rows[5], 'qty' => $rows[6]];
            }
        }
    }
        }

        if(!empty($cart_data)){
>>>>>>> f7f879507622745efd2240b8c6276600c361ed5e
        $product = $cartquantity;
        $cart_quantity = 0;
        for($i = 0; $i < count($cart_data); $i++){
            if($cart_data[$i]['product'] == $product){
                $cart_quantity = $cart_quantity + $cart_data[$i]['qty'];
            }
        }
<<<<<<< HEAD

        echo ($totalavailablestock - $soldstock) - $cart_quantity;
=======
        echo (($totalavailablestock - $soldstock) - $cart_quantity) + $alreadyqty;
        }else{
            echo $totalavailablestock - $soldstock;
        }
>>>>>>> f7f879507622745efd2240b8c6276600c361ed5e
    }
}


if(isset($_POST['productitem'])){
    extract($_POST);
    $size = [];
    $quantity = 0;
    if(!empty($productitem) && !empty($colorval)){
        $query = mysqli_query($conn, "SELECT DISTINCT(size) as sze FROM product_detail WHERE product='$productitem' AND color='$colorval'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                while($rows = mysqli_fetch_assoc($query)){
                    $size_qty = getValueSum('product_detail', 'qty', 'color', $colorval, 'product', $productitem, 'size', $rows['sze']);
                    $cart_qty = getValueSum('user_cart', 'qty', 'color', $colorval, 'product', $productitem, 'size', $rows['sze']);
                    $order_qty = orderedQty($productitem, $colorval, $rows['sze']);
                    $quantity = $size_qty - $cart_qty - $order_qty ;
                    if($quantity > 0){
                        $size[] = $rows['sze'];
                    }
                }
            }
        }
    }
    echo json_encode($size);
}


if(isset($_POST['getcartquantity'])){
    extract($_POST);
    if(!empty($getcartquantity)){
        echo getSinglevalue('user_cart', 'uc_key', $getcartquantity, 6);
    }
}


if(isset($_POST['orderdetail'])){
    extract($_POST);
    if(!empty($orderdetail)){
        $view = 'web';
        $sql = mysqli_query($conn, "SELECT * FROM order_book WHERE order_key='$orderdetail'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                while($rows = mysqli_fetch_assoc($sql)){
                    if($view == 'web'){
                        echo '
                        <tr>
                        <td><img src="'.PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $rows['product'], 2).'" width="100px"></td>
                        <td>'.getSinglevalue('product', 'sku', $rows['product'], 4).'</td>
                        <td>'.$rows['pono'].'</td>
                        <td>'.$rows['color'].'</td>
                        <td>'.$rows['size'].'</td>
                        <td>'.$rows['qty'].'</td>
                        <td class="order-price">'.$rows['price'].'</td>
                    </tr>
                        ';
                    }else{
                    echo '
                    <tr>
                        <td><img src="products/'.getSinglevalue('product_images', 'product', $rows['product'], 2).'" width="100px"></td>
                        <td>'.getSinglevalue('product', 'sku', $rows['product'], 4).'</td>
                        <td>'.$rows['color'].'</td>
                        <td>'.$rows['size'].'</td>
                        <td>'.$rows['qty'].'</td>
                        <td class="order-price">'.$rows['price'].'</td>
                    </tr>
                    ';
                }
                }
            }
        }
    }
}



if(isset($_POST['exattribute'])){
    extract($_POST);
    if($exattribute != "" && $vertualproduct != ""){
        if($exattribute == 'COLOR'){
            $query = ", sample";
        }else{
            $query = "";
        }
        $sql = mysqli_query($conn, "SELECT value $query FROM product_attributes WHERE p_id='$vertualproduct' AND attribute='$exattribute'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $row = mysqli_fetch_assoc($sql);
                if(!empty($query)){
                    echo $row['value'].'#'.$row['sample'];
                }else{
                echo $row['value'];
                }
            }
        }
    }
}


if(isset($_POST['address'])){
    extract($_POST);
    $addressdata = "";
    if(!empty($address)){
        $sql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$address' ORDER BY ab_id DESC LIMIT 1");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $row = mysqli_fetch_array($sql);
                $addressdata = $row;
            }
        }
    }
    echo json_encode($addressdata);
}


if(isset($_POST['addtype'])){
    extract($_POST);
    if(!empty($name) && !empty($mobile) && !empty($addresss) && !empty($city) && !empty($state) && !empty($pincode))
    {
        $status = verifyAddress($pincode);
        if(array_key_exists("message", $status)){
            echo 401;
        }
        if(array_key_exists("success", $status)){
            $postcode_state = $status["postcode_details"]['state'];
            $postcode_city = $status["postcode_details"]['city'];
            $pass = true;
            if($pass == true && $postcode_state !== $state){
                $pass = false;
            }
            if($pass){
                $city = $postcode_city;
                echo $city;
            }else{
                echo 403;
            }
        }
    }else{
        echo "empty Fields";
    }
}

// $status = verifyAddress('201102');
// print_r($status);

?>