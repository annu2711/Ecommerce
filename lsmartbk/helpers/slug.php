<div class="col-md-3">
    <label>Reviews<sup class="text-danger">*</sup></label>
    <select class="form-control" name="review" required>
        <option value="">-- Select --</option>
        <option value="0" <?php 
        if(!empty($edit)){
            if($array[14] == 0){
                echo 'selected="selected"';
            }
        } ?>>Enable</option>
        <option value="1"
        <?php 
        if(!empty($edit)){
            if($array[14] == 1){
                echo 'selected="selected"';
            }
        } ?>
        >Disable</option>
    </select>
</div>