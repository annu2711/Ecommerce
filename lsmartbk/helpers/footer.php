<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/plugins/momentjs/moment.js"></script>
<script src="assets/js/pages/forms/basic-form-elements.js"></script>
<script src="assets/plugins/jquery-validation/jquery.validate.js"></script>
<script src="assets/plugins/jquery-steps/jquery.steps.js"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="assets/bundles/datatablescripts.bundle.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

<!-- <script src="assets/bundles/mainscripts.bundle.js"></script> -->
<script src="assets/js/pages/forms/form-validation.js"></script> 
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<!-- <script src="assets/bundles/mainscripts.bundle.js"></script>Custom Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->


<script src="assets/bundles/morrisscripts.bundle.js"></script>
<script src="assets/bundles/jvectormap.bundle.js"></script>
<script src="assets/bundles/flotscripts.bundle.js"></script> 
<script src="assets/bundles/sparkline.bundle.js"></script> 
<script src="assets/bundles/knob.bundle.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/ecommerce.js"></script>
<!-- <script src="assets/js/pages/charts/jquery-knob.min.js"></script> -->
<script src="assets/default-js/main.js"></script>
<script src="assets/js/pages/tables/jquery-datatable.js"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->
<!-- <script src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> -->
<script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- <script src="assets/js/pages/forms/advanced-form-elements.js"></script>  -->
<!-- <script src="assets/plugins/dropzone/dropzone.js"></script> -->
<script src="https://cdn.rawgit.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
<script src="https://cloud.tinymce.com/4/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
<script>
    tinymce.init({
  selector: '.editor',
  height: 200,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  mobile: { 
    theme: 'mobile' 
  },
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tiny.cloud/css/codepen.min.css'
  ],
});
</script>
<!-- <script>showNotification('alert-danger', 'Variation Not Added Successfully', 'top', 'right', '', '')</script> -->
</body>
</html>