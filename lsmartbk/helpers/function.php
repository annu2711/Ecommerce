<?php 
date_default_timezone_set('Asia/Kolkata');
error_reporting(E_ALL);
$created_at = date('Y-m-d H:i:s');
$url = 'demo.inovoteq.com';
setlocale(LC_MONETARY, 'en_IN');
define("PRODUCT_DIRECTORY", "../products/");
define("WEBSITE_DIRECTORY", "../website_images/");
define("PRODUCT_IMAGE_URL", "products/");
define("WEBSITE_IMAGE_URL", "website_images/");
define("NOT_FOUND_IMAGE", "no-product.png");
define("NOT_FOUND_IMAGE_WEB", "no-product.png");
define("PAYMENT_RETURN_URL", 'http://localhost/lsmart/ecommerce/order-success.php?orderid=');

function client_session($id, $token){
    $conn = $GLOBALS['conn'];
    if(!empty($id) && !empty($token)){
    $query = mysqli_query($conn, "SELECT * FROM clients WHERE client_key='$id' AND token='$token' ORDER BY client_id DESC LIMIT 1");
    if($query){
        $row = mysqli_num_rows($query);
        if($row < 1){
            header('location: logout');
        }
    }
}else{
    header('location: index');
}
}

function user_session($id, $token){
    // echo '<script>alert('.$id.')</script>';
    $conn = $GLOBALS['conn'];
    if(!empty($id) && !empty($token)){
    $query = mysqli_query($conn, "SELECT * FROM admin_users WHERE au_id='$id' AND token='$token' ORDER BY au_id DESC LIMIT 1");
    if($query){
        $row = mysqli_num_rows($query);
        if($row == 0){
            // sesssion_destroy();
            header('location: logout');
        }
    }
}else{
    header('location: index');
}
}


// function user_session($id, $token){
//     // echo '<script>alert('.$id.')</script>';
//     $conn = $GLOBALS['conn'];
//     if(!empty($id) && !empty($token)){
//     $query = mysqli_query($conn, "SELECT * FROM admin_users WHERE au_id='$id' AND token='$token' ORDER BY au_id DESC LIMIT 1");
//     if($query){
//         $row = mysqli_num_rows($query);
//         if($row == 0){
//             // sesssion_destroy();
//             header('location: logout');
//         }
//     }
// }else{
//     header('location: index');
// }
// }

function flash_session($msg, $msg2){
    if(isset($_SESSION['result'])){
        if($_SESSION['result'] == 1){
            echo "<script>showNotification('alert-success', '".$msg." Successfully', 'top', 'right', '', '')</script>";
        }else{
            echo "<script>showNotification('alert-warning', '".$msg2." successfully', 'top', 'right', '', '')</script>";
        }
        unset($_SESSION['result']);
    }
}

function flash_session_admin(){
    if(isset($_SESSION['result'])){
        $result = $_SESSION['result'];
        if($result[0] == 1){
            echo "<script>showNotification('alert-success', '".$result[1]."', 'top', 'right', '', '')</script>";
        }else{
            echo "<script>showNotification('alert-warning', '".$result[1]."', 'top', 'right', '', '')</script>";
        }
        unset($_SESSION['result']);
    }
}

function flash_session_web(){
    if(isset($_SESSION['result'])){
        $result = $_SESSION['result'];
        if($result[0] == 1){
            echo "<script>$.notify('".$result[1]."', '".$result[2]."');</script>";
        }else{
            echo "<script>$.notify('".$result[1]."', '".$result[2]."');</script>";
        }
        unset($_SESSION['result']);
    }
}

function rand_char($size){
    $length = $size;
    $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $length);
    return $randomletter;
    }

function login($username, $password){
    $conn = $GLOBALS['conn'];
    $p = md5($password);
    $token = rand_char(32);
    $query = mysqli_query($conn, "UPDATE admin_users SET token='$token' WHERE username='$username' AND password='$p'");
    if($query){
        return $token;
    }else{
        return mysqli_error($conn);
    }
}

function logout($userid, $token){
    $conn = $GLOBALS['conn'];
    $query = mysqli_query($conn, "UPDATE admin_users SET token='0' WHERE au_id='$userid' AND token='$token'");
    if($query){
        return 1;
    }
}

function getSinglevalue($tbl, $clm, $sign, $f_value, $xyz=''){
	$conn = $GLOBALS['conn'];
    $query = mysqli_query($conn, "SELECT * FROM $tbl WHERE $clm='$sign' LIMIT 1");
    if (mysqli_num_rows($query) > 0) {
        $rows = mysqli_fetch_array($query);
        $val = $rows[$f_value];
        if ($val!="") {
        	return $val;
        }else{
        	return $xyz;
        }
    }else{
    	return $xyz;
    }
}


    function check_duplicate($tbl, $clm, $val, $clmm="", $type=""){
        $conn = $GLOBALS['conn'];
        if($type!=""){
            $query = mysqli_query($conn, "SELECT * FROM $tbl WHERE $clm='$val' AND $clmm='$type' LIMIT 1");
            if($query){
            if (mysqli_num_rows($query) > 0) {
                return 1;
            }else{
                return 0;
            }
        }
        }else{
            $query = mysqli_query($conn, "SELECT * FROM $tbl WHERE $clm='$val' LIMIT 1");
            if($query){
            if (mysqli_num_rows($query) > 0) {
                return 1;
            }else{
                return 0;
            }
        }
    }
    }


    function display_option_selected($tbl, $a, $b, $val="", $orderby="", $option="", $value=""){
        $conn = $GLOBALS['conn'];
        $subquery = "";
        if(!empty($option) && $value!=""){
            $subquery = "WHERE ".$option."='".$value."'";
        }
        $query = mysqli_query($conn, "SELECT * FROM $tbl $subquery $orderby");
        while ($rows = mysqli_fetch_array($query)) {
            echo '<option value="'.$rows[$a].'"';
            if($val == $rows[$a]){ 
                echo 'selected="selected"'; 
            }
            echo '>'.$rows[$b].'</option>';
        }
    }

    function status($key){
        if ($key == 1) {
            echo "<p class='text-success'>Active</p>";
        }else{
            echo "<p class='text-danger'>Inactive</p>";
        }
    }

    function resize($name, $file, $width, $height) {
        $file_type = pathinfo($name, PATHINFO_EXTENSION);
        // return $file_type;
        if($file_type == 'JPG' || $file_type == 'jpg' || $file_type == 'JPEG' || $file_type == 'jpeg'){
            return imagejpeg(imagescale(imagecreatefromjpeg($file), $width, $height), $file);
        }
        // elseif($file_type == 'webp' || $file_type == 'WEBP'){
        //     return imagewebp(imagescale(imagecreatefromwebp($file), $width, $height), $file);
        // }
        elseif($file_type == 'png' || $file_type == 'PNG'){
            return imagepng(imagescale(imagecreatefrompng($file), $width, $height), $file);
        }else{
            return imagejpeg(imagescale(imagecreatefromjpeg($file), $width, $height), $file);
        }


    }


    function compress($source, $destination, $quality) {

        $info = getimagesize($source);
    
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);
    
        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);
    
        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);
    
        $img = imagejpeg($image, $destination, $quality);
    
        // return $destination;

        if($img){
            return true;
        }else{
            return false;
        }
    }


    function fileUpload($file, $file_tmp, $type, $directory, $product = ""){
        if(!empty($file)){
            $file_type = pathinfo($file, PATHINFO_EXTENSION);
                if ($file_type == "jpeg" || $file_type == "JPEG" || $file_type == "jpg" || $file_type == "JPG" || $file_type == "png" || $file_type == "PNG") {
                    if(!empty($product)){
                    resize($file, $file_tmp, 540, 692);
                    }
                    $profile_pic = $type;
                    $path = $directory.$profile_pic;

                    $d = compress($file_tmp, $path, 60);

                    if($d){
                        return $profile_pic;
                    }else{
                        return 0;
                    }
                }
        }
    }


    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
     
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
     }


    //  save localstorage
    function saveStorage($token, $userkey, $key){
        echo '<script>userSession("'.$token.'", "'.$userkey.'", "'.$key.'")</script>';
        return 1;
    }
    //clear storage 
    function removeSession(){
        echo '<script>removeSession()</script>';
    }

    function sendEmail($senderid, $receiverid, $subject, $content){
        $to = $receiverid;
        // $subject = "HTML email";
        
        $message ='<html>
        <head>
            <title>Newsletter</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
        </head>
        <body>
            <table border="1" cellspacing="0" cellpadding="0" style="max-width: 600px; width:100%; padding:10px" align="center">
                <tr>
                    <td style="display:inline-block; width:100%; border:0;">
                        <img src="https://lsmart.in/img/logo.png" style="max-width: 110px; width:100%; display: block; margin:0 auto">
                    </td>
                </tr>
                <tr>
                    <td style="height: 300px; border:0">'.$content.'</td>
                </tr>
                <tr>
                    <td style="display:inline-block; width:100%; padding-top:10px; border:0; border-top:1px solid #ffc734;">
                        <p style="margin:1px 0; font-size:16px" align="center"><b>LSMART</b></p>
                        <p style="margin:1px 0; font-size:14px" align="center">30/1 east Patel Nagar New Delhi-110008, INDIA.</p>
                        <p style="margin:1px 0; font-size:14px" align="center">support@lsmart.com</p>
                        <p style="margin:1px 0; font-size:14px" align="center">+91 9599123123</p>
                    </td>
                </tr>
            </table>
        </body>
        </html>';
        
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // More headers
        $headers .= 'From: <'.$senderid.'>';
        
        $mail = mail($to,$subject,$message,$headers);
        if($mail){
            return true;
        }else{
            return false;
        }
        
            }

            function getValueSum($tbl, $sumof, $clm="", $val="", $clm2="", $val2="", $clm3="", $val3=""){
                $conn = $GLOBALS['conn'];
                $where = "";
                if($clm!="" && $val!=""){
                    $where = "WHERE $clm='$val'";
                }
                if($clm2!="" && $val2!=""){
                    $where = $where." AND $clm2='$val2'";
                }
                if($clm3!="" && $val3!=""){
                    $where = $where." AND $clm3='$val3'";
                }
                $sql = mysqli_query($conn, "SELECT SUM($sumof) as qty FROM $tbl $where");
                if($sql){
                    if(mysqli_num_rows($sql) > 0){
                        $row = mysqli_fetch_assoc($sql);
                        return intval($row['qty']);
                    }else{
                        return 0;
                    }
                }else{
                    return 0;
                }
            }


        function orderedQty($product, $color="", $size=""){
            $conn = $GLOBALS['conn'];
                $clr = "";
                if($color!=""){
                    $clr = "AND t1.color='$color'";
                }
                $sze = "";
                if($size!=""){
                    $sze = "AND t1.size='$size'";
                }
            $query = mysqli_query($conn, "SELECT SUM(t1.qty) as qty FROM order_book as t1 JOIN orders as t2 on t1.order_key=t2.order_key WHERE t2.d_status=0 AND t1.product='$product' $clr $sze");
            if($query){
                if(mysqli_num_rows($query) > 0){
                    $rows = mysqli_fetch_assoc($query);
                    return $rows['qty'];
                }else{
                    return 0;
                }
            }else{
                return mysqli_error($conn);
            }
            
        }  


        function slider_alignment($align){
            if($align == 'left'){
                echo "start";
            }elseif($align == 'center'){
                echo "center";
            }else{
                echo "end";
            }
        }


        function orderStatus($key){
            if($key == 1){
                echo '<p class="text-info">Proccessing</p>';
            }elseif($key == 2){
                echo '<p class="text-success">Complete</p>';
            }else{
                echo '<p class="text-warning">Pending</p>';
            }
        }

        function getComleteAddress($key, $tbl, $clm){
            $conn = $GLOBALS['conn'];
            if($key!= ""){
                $sql = mysqli_query($conn, "SELECT title, name, last_name, mobile, address, town, state, pincode FROM $tbl WHERE $clm='$key'");
                if($sql){
                    if(mysqli_num_rows($sql) > 0){
                        $row = mysqli_fetch_assoc($sql);
                        return $row['title']." ".$row['name']." ".$row['last_name']."<br>".$row['mobile']."<br>".$row['address']." ".$row['town']." ".$row['state']." Pincode:".$row['pincode'];
                    }
                }
            }
        }

        function SendSMS($hostUrl){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $hostUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // change to 1 to verify cert
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            $result = curl_exec($ch);
            return $result;
            } 

        function mobilesms($mobile, $message){
        $sender ='EMBSSY';
        $mob = $mobile;
        $auth='D!~5402uhbTpNvn7w';
        $msg = urlencode($message); 
        $url = 'https://global.datagenit.com/API/sms-api.php?auth='.$auth.'&msisdn='.$mob.'&senderid='.$sender.'&message='.$msg.'';  // API URL
        $result = SendSMS($url);  // call function that return response with code
        return json_decode($result, true);
        }


        function productQty($productcode){
            $conn = $GLOBALS['conn'];
            $cartqty = 0;
            $orderqty = 0;
            // cart qty
            $sql = mysqli_query($conn, "SELECT SUM(qty) as cartqty FROM product_stock WHERE product='$productcode'");
            if($sql){
                if(mysqli_num_rows($sql) > 0){
                    while($rows = mysqli_fetch_assoc($sql)){
                        $cartqty = $cartqty + intval($rows['cartqty']);
                    }
                }
            }
            // order qty
            $sql2 = mysqli_query($conn, "SELECT SUM(qty) as orderqty FROM order_book WHERE product='$productcode'");
            if($sql2){
                if(mysqli_num_rows($sql2) > 0){
                    while($rows = mysqli_fetch_assoc($sql2)){
                        $orderqty = $orderqty + intval($rows['orderqty']);
                    }
                }
            }

            // $productqty = intval(getSinglevalue('product', 'sku', $productcode, 33));

            return $cartqty - $orderqty;
        }

        
        function userAccess($array, $deg){
            // print_r($array);
            // echo $deg;
            // foreach($array as $a =>$b){
            //     if($deg == $b){
            //         header('dashboard');
            //     }else{
            //         // echo $deg."=".$b;
            //     }
            // }
            if(!in_array($deg, $array)){
                header('location: dashboard');
            }
        }

// function numberToCurrency($number)
// {
//     if(setlocale(LC_MONETARY, 'en_IN'))
//       return money_format('%.0n', $number);
//     else {
//       $explrestunits = "" ;
//       $number = explode('.', $number);
//       $num = $number[0];
//       if(strlen($num)>3){
//           $lastthree = substr($num, strlen($num)-3, strlen($num));
//           $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
//           $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
//           $expunit = str_split($restunits, 2);
//           for($i=0; $i<sizeof($expunit); $i++){
//               // creates each of the 2's group and adds a comma to the end
//               if($i==0)
//               {
//                   $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
//               }else{
//                   $explrestunits .= $expunit[$i].",";
//               }
//           }
//           $thecash = $explrestunits.$lastthree;
//       } else {
//           $thecash = $num;
//       }
//       if(!empty($number[1])) {
//       	if(strlen($number[1]) == 1) {
//       		return $thecash . '.' . $number[1] . '0';
//       	} else if(strlen($number[1]) == 2){
//       		return $thecash . '.' . $number[1];
//       	} else {
//             return 'cannot handle decimal values more than two digits...';
//         }
//       } else {
//       	return $thecash.'.00';
//       }
//     }
// }



// table count

function tableCount($table, $id){
    $conn = $GLOBALS['conn'];
    if(!empty($table) && !empty($id)){
        $sql = mysqli_query($conn, "SELECT COUNT($id) FROM $table");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $row = mysqli_fetch_array($sql);
                return $row[0];
            }else{
                return 0;
            }
        }
    }
}



function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(
        0 => '',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen',
        20 => 'twenty',
        30 => 'thirty',
        40 => 'forty',
        50 => 'fifty',
        60 => 'sixty',
        70 => 'seventy',
        80 => 'eighty',
        90 => 'ninety');
    $digits = array(
        '',
        'hundred',
        'thousand',
        'lakh',
        'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural .
                ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] .
                ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal %
        10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}

function paginationMine($totalpages, $currentpage){
    $x = ceil($currentpage / 5);
    $pagination = '
    <nav aria-label="...">
  <ul class="pagination">
    <li class="page-item">
      <a class="page-link" href="test.php?page=1" tabindex="-1">First</a>
    </li>';
    if($currentpage > 1){
        $pagination = $pagination.'<li class="page-item"><a class="page-link" href="test.php?page='.($currentpage - 1).'" tabindex="-1">Previous</a></li>';
    }
  $balpages = $totalpages - $currentpage ;
    if($totalpages > 5){
        $pagecount = 5;
    }else{
        $pagecount = $totalpages;
    }
    $index = ($x - 1) * 5;
    $pg = $x * 5;

    if($pg > $totalpages){
        $pg = $totalpages;
    }

    for($i = $index; $i < $pg; $i++){
        $pagination = $pagination.'<li class="page-item ';
        if($currentpage == ($i + 1)) { 
            $pagination .= "active" ;
        }
        $pagination .= '"><a class="page-link" href="test.php?page='.($i + 1).'">'.($i + 1).'</a></li>';
    }

    if($totalpages > $currentpage){
        $pagination .='<li class="page-item"><a class="page-link" href="test.php?page='.($currentpage + 1).'">Next</a>
      </li>';
    }

    $pagination .= '<li class="page-item">
    <a class="page-link" href="test.php?page='. $totalpages .'" tabindex="-1">Last</a>
  </li>';

  return $pagination;
    
}

function checkImage($image){
	return (file_exists('products/'.$image)) ? $image : 'no-product.png' ;
}


// function money_format($formato, $valor) {

//     if (setlocale(LC_MONETARY, 0) == 'C') { 
//         return number_format($valor, 2); 
//     }

//     $locale = localeconv(); 

//     $regex = '/^'.             // Inicio da Expressao 
//              '%'.              // Caractere % 
//              '(?:'.            // Inicio das Flags opcionais 
//              '\=([\w\040])'.   // Flag =f 
//              '|'. 
//              '([\^])'.         // Flag ^ 
//              '|'. 
//              '(\+|\()'.        // Flag + ou ( 
//              '|'. 
//              '(!)'.            // Flag ! 
//              '|'. 
//              '(-)'.            // Flag - 
//              ')*'.             // Fim das flags opcionais 
//              '(?:([\d]+)?)'.   // W  Largura de campos 
//              '(?:#([\d]+))?'.  // #n Precisao esquerda 
//              '(?:\.([\d]+))?'. // .p Precisao direita 
//              '([in%])'.        // Caractere de conversao 
//              '$/';             // Fim da Expressao 

//     if (!preg_match($regex, $formato, $matches)) { 
//         trigger_error('Formato invalido: '.$formato, E_USER_WARNING); 
//         return $valor; 
//     } 

//     $opcoes = array( 
//         'preenchimento'   => ($matches[1] !== '') ? $matches[1] : ' ', 
//         'nao_agrupar'     => ($matches[2] == '^'), 
//         'usar_sinal'      => ($matches[3] == '+'), 
//         'usar_parenteses' => ($matches[3] == '('), 
//         'ignorar_simbolo' => ($matches[4] == '!'), 
//         'alinhamento_esq' => ($matches[5] == '-'), 
//         'largura_campo'   => ($matches[6] !== '') ? (int)$matches[6] : 0, 
//         'precisao_esq'    => ($matches[7] !== '') ? (int)$matches[7] : false, 
//         'precisao_dir'    => ($matches[8] !== '') ? (int)$matches[8] : $locale['int_frac_digits'], 
//         'conversao'       => $matches[9] 
//     ); 

//     if ($opcoes['usar_sinal'] && $locale['n_sign_posn'] == 0) { 
//         $locale['n_sign_posn'] = 1; 
//     } elseif ($opcoes['usar_parenteses']) { 
//         $locale['n_sign_posn'] = 0; 
//     } 
//     if ($opcoes['precisao_dir']) { 
//         $locale['frac_digits'] = $opcoes['precisao_dir']; 
//     } 
//     if ($opcoes['nao_agrupar']) { 
//         $locale['mon_thousands_sep'] = ''; 
//     } 

//     $tipo_sinal = $valor >= 0 ? 'p' : 'n'; 
//     if ($opcoes['ignorar_simbolo']) { 
//         $simbolo = ''; 
//     } else { 
//         $simbolo = $opcoes['conversao'] == 'n' ? $locale['currency_symbol'] 
//                                                : $locale['int_curr_symbol']; 
//     } 
//     $numero = number_format(abs($valor), $locale['frac_digits'], $locale['mon_decimal_point'], $locale['mon_thousands_sep']); 


//     $sinal = $valor >= 0 ? $locale['positive_sign'] : $locale['negative_sign']; 
//     $simbolo_antes = $locale[$tipo_sinal.'_cs_precedes']; 

//     $espaco1 = $locale[$tipo_sinal.'_sep_by_space'] == 1 ? ' ' : ''; 

//     $espaco2 = $locale[$tipo_sinal.'_sep_by_space'] == 2 ? ' ' : ''; 

//     $formatado = ''; 
//     switch ($locale[$tipo_sinal.'_sign_posn']) { 
//     case 0: 
//         if ($simbolo_antes) { 
//             $formatado = '('.$simbolo.$espaco1.$numero.')'; 
//         } else { 
//             $formatado = '('.$numero.$espaco1.$simbolo.')'; 
//         } 
//         break; 
//     case 1: 
//         if ($simbolo_antes) { 
//             $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero; 
//         } else { 
//             $formatado = $sinal.$numero.$espaco1.$simbolo; 
//         } 
//         break; 
//     case 2: 
//         if ($simbolo_antes) { 
//             $formatado = $simbolo.$espaco1.$numero.$sinal; 
//         } else { 
//             $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal; 
//         } 
//         break; 
//     case 3: 
//         if ($simbolo_antes) { 
//             $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero; 
//         } else { 
//             $formatado = $numero.$espaco1.$sinal.$espaco2.$simbolo; 
//         } 
//         break; 
//     case 4: 
//         if ($simbolo_antes) { 
//             $formatado = $simbolo.$espaco2.$sinal.$espaco1.$numero; 
//         } else { 
//             $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal; 
//         } 
//         break; 
//     } 

//     if ($opcoes['largura_campo'] > 0 && strlen($formatado) < $opcoes['largura_campo']) { 
//         $alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT; 
//         $formatado = str_pad($formatado, $opcoes['largura_campo'], $opcoes['preenchimento'], $alinhamento); 
//     } 

//     return $formatado; 
// } 

function callShipRocketApi($url, $data="", $header, $method)
{
    $curl = curl_init();
    if($method == 'post'){
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }else{
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    }
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    $err = curl_error($curl);
    if ($err) {
        return 'curl error' . $err;
    }else{
        $response = json_decode($result, true);
        return $response;
    }
}

function getRowArray($tbl, $clm, $id){
    $conn = $GLOBALS['conn'];
    if(!empty($tbl) && !empty($clm) && !empty($id)){
        $sql = mysqli_query($conn, "SELECT * FROM $tbl WHERE $clm='$id'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                return mysqli_fetch_array($sql);
            }
        }
    }
}


function indianState($selected=""){
    $state = [ "Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Punjab","Rajasthan","Sikkim","Tamil Nadu","Telangana","Tripura","Uttarakhand","Uttar Pradesh","West Bengal","Andaman and Nicobar Islands","Chandigarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Lakshadweep","Puducherry"];
    $option = "";
    for($i = 0; $i < count($state); $i++){
        $selectedval = "";
        if($selected == $state[$i]){
            $selectedval = 'selected="selected"';
        }
        $option = $option."<option ".$selectedval." value='".$state[$i]."'>".$state[$i]."</option>";
    }
    return $option;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}



?>


