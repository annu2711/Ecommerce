<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Admin for Moonraker">

<title>Moonraker</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css"/>
<link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<!-- Bootstrap Select Css -->
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/ecommerce.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<link rel="stylesheet" href="https://www.tinymce.com/css/codepen.min.css">
<!-- Bootstrap Material Datetime Picker Css -->
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<!-- Dropzone Css -->
<!-- <link rel="stylesheet" href="assets/plugins/dropzone/dropzone.css"> -->
<link rel="stylesheet" href="assets/css/mystyle.css">
<style>
    .error{
        color: red;
    }
</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/refresh.png" width="48" height="48" alt="Compass"></div>
        <p>Please wait...</p>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
<nav class="navbar">
    <div class="col-12">        
        <div class="navbar-header text-center">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="dashboard">
                <img src="../img/logo.png" width="50px" alt="Bquest India">
                <!-- <span class="m-l-10">Moonraker</span> -->
            </a>
        </div>
        <ul class="nav navbar-nav navbar-left">
            <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>            
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true"><i class="zmdi zmdi-fullscreen"></i></a>
            </li>
            <li><a href="logout" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a></li>
        </ul>
    </div>
</nav>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <div class="image"><a href="profile.html"><img src="assets/images/profile_av.jpg" alt="User"></a></div>
                    <div class="detail">
                        <h4 style="margin-bottom: 10px;"><?php echo $admin ?></h4>
                        <!-- <small>Super Admin</small>                         -->
                    </div>
                </div>
            </li>
            <!-- <li class="header">MAIN</li> -->
            <li> <a href="dashboard" class="menu-toggle"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
            </li>
            <?php if($deg != 3){ ?>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Admin Users</span> </a>
                <ul class="ml-menu">
                    <li><a href="add-admin-user">Add Users</a></li>
                    <li><a href="user-list">Users List</a></li>
                </ul>
            </li>

            <!-- <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Attribute Builder</span> </a>
                <ul class="ml-menu">
                    <li><a href="add-attribute">Add Attribute</a></li>
                    <li><a href="assign-attribute">Assign Attribute</a></li>
                    <li><a href="attribute-dd">Attribute DD Master</a></li>
                </ul>
            </li>    -->
            <?php } ?>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Masters</span> </a>
                <ul class="ml-menu">
                    <li><a href="add_store">Add Store</a></li>
                    <li><a href="brand">Add Brand</a></li>
                    <li><a href="category">Add Category</a></li>
                    <li><a href="subcategory">Add Subcategory</a></li>
                    <li><a href="fit">Add Fit</a></li>
                    <li><a href="status">Add Season</a></li>
                    <li><a href="brand-profile">Brand Profile</a></li>
                </ul>
            </li>            
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-swap-alt"></i><span>Products Management</span> </a>
                <ul class="ml-menu">
                    <li> <a href="product">Add Product</a> </li>
                    <li> <a href="product-list">Active Products</a> </li>
                    <li><a href="bulk-upload">Bulk Upload</a> </li>
                    <li><a href="bulk-image-upload">Bulk Image Upload</a> </li>
                </ul>
            </li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Stock Management</span> </a>
                <ul class="ml-menu">
                    <li><a href="product-stock-list">Product Stock</a> </li>
                    <li><a href="po-stock-list">Products Stock Report</a> </li>
                    <li><a href="stock-bulk-upload.php">Stock Bulk Upload</a> </li>
                </ul>
            </li>
            <?php if($deg != 3){ ?>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-grid"></i><span>Order Management</span> </a>
                <ul class="ml-menu">                        
                    <li> <a href="order.php">Orders</a> </li>
                    <li> <a href="">Return Orders</a> </li>
                </ul>
            </li>            
           
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>User Management</span> </a>
                <ul class="ml-menu">
                    <li> <a href="users">Clients List</a> </li>
                    <li> <a href="store-list">Store List</a> </li>
                    <li> <a href="cart">User Carts</a> </li>
                    <li> <a href="wishlist">Wishlist</a> </li>
                    <li> <a href="news-letter">News Letter</a> </li>
                </ul>
            </li>
            <?php } ?>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Website management</span> </a>
                <ul class="ml-menu">
                    <li> <a href="menu">Menu</a></li>
                    <li> <a href="submenu">Submenu</a></li>
                    <li> <a href="slider">Home Page Slider</a></li>
                    <li> <a href="shopbycategory">Shop By Category</a></li>
                    <li> <a href="buyerpick">Buyer's Pick</a></li>
                    <li> <a href="blogs">Blogs</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
