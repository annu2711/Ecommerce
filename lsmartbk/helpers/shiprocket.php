<?php 
include_once "function.php";
define("SHIP_USERNAME", "anand.verma@inovoteq.com");
define("SHIP_PASSWORD", "Anand@123#");
define("SHIP_ROCKET_AUTH_URL", "https://apiv2.shiprocket.in/v1/external/auth/login");
define("SHIP_CREATE_ORDER", "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc");
define("LENGTH", 14);
define("BREADTH", 13);
define("HEIGHT", 1);
define("WEIGHT", 0.012);

// echo getSinglevalue('clients', 'client_id', 4, 4);


function ShipOrder($order_id){
$conn = $GLOBALS['conn'];
$shipproductarr = "";

$query = mysqli_query($conn, "SELECT t1.product, (SELECT SUM(qty) FROM order_book WHERE product=t1.product AND order_key='$order_id' ) as qty, t2.product_name, t2.price FROM order_book as t1 join product as t2 on t1.product=t2.sku WHERE t1.order_key='$order_id' GROUP BY t1.product ORDER BY t1.ob_id DESC");
if($query){
    if(mysqli_num_rows($query) > 0){
        
        while($rows = mysqli_fetch_assoc($query)){
            if(empty($shipproductarr)){
            $shipproductarr = $shipproductarr.'{
                "name": "'.$rows['product_name'].'",
                "sku": "'.$rows['product'].'",
                "units": "'.$rows['qty'].'",
                "selling_price": "'.$rows['price'].'",
                "discount": "",
                "tax": "",
                "hsn": ""
            }';
        }else{
            $shipproductarr = $shipproductarr.',{
                "name": "'.$rows['product_name'].'",
                "sku": "'.$rows['product'].'",
                "units": "'.$rows['qty'].'",
                "selling_price": "'.$rows['price'].'",
                "discount": "",
                "tax": "",
                "hsn": ""
            }';
        }
            
        }
    }
}


// billing address starts
$email = getSinglevalue('clients', 'client_key', getSinglevalue('orders', 'order_key', $order_id, 2), 5);
// shipping address starts
$shippingaddresskey = getSinglevalue('orders', 'order_key', $order_id, 6);
$billingaddresskey = getSinglevalue('orders', 'order_key', $order_id, 5);
$paymentmethod = getSinglevalue('orders', 'order_key', $order_id, 8);
if($paymentmethod != 'cod'){
    $paymentmethod = 'Prepaid';
}
$totalamount = getSinglevalue('orders', 'order_key', $order_id, 4);
$totalitems = getSinglevalue('orders', 'order_key', $order_id, 3);
$orderdatetime = getSinglevalue('orders', 'order_key', $order_id, 11);
$length = $totalitems * LENGTH;
$breadth = $totalitems * BREADTH;
$height = $totalitems * HEIGHT;
$weight = $totalitems * WEIGHT;
$shippingaddarr = getRowArray('address_book', 'ab_key', $shippingaddresskey);
$billingaddarr = getRowArray('address_book', 'ab_key', $billingaddresskey);
if($shippingaddresskey == $billingaddresskey){
    $true = true;
}else{
    $true = false;
}
// shipping details
$shippingname = $shippingaddarr[3]." ".$shippingaddarr[4]." ".$shippingaddarr[5];
$shippingaddress = $shippingaddarr[7];
$shippingcity = $shippingaddarr[8];
$shippingstate = $shippingaddarr[9];
$shippingpincode = $shippingaddarr[10];
$shippingmobile = $shippingaddarr[6];
// billing details
$billingname = $billingaddarr[3]." ".$billingaddarr[4]." ".$billingaddarr[5];
$billingaddress = $billingaddarr[7];
$billingcity = $billingaddarr[8];
$billingstate = $billingaddarr[9];
$billingpincode = $billingaddarr[10];
$billingmobile = $billingaddarr[6];

$auth_header = ['content-type: application/json'];
$auth_body = '{
    "email": "'.SHIP_USERNAME.'",
    "password": "'.SHIP_PASSWORD.'"
}';
$loginApi = callShipRocketApi(SHIP_ROCKET_AUTH_URL, $auth_body, $auth_header, 'post');
// print_r($loginApi);
if(!empty($loginApi)){
if(!empty($loginApi['token'])){
$order_body = '{
    "order_id": "'.$order_id.'",
    "order_date": "'.$orderdatetime.'",
    "pickup_location": "Bquest",
    "channel_id": "",
    "comment": "",
    "reseller_name": "",
    "company_name": "",
    "billing_customer_name": "'.$billingname.'",
    "billing_last_name": "",
    "billing_address": "'.$billingaddress.'",
    "billing_address_2": "",
    "billing_isd_code": "",
    "billing_city": "'.$billingcity.'",
    "billing_pincode": "'.$billingpincode.'",
    "billing_state": "'.$billingstate.'",
    "billing_country": "India",
    "billing_email": "'.$email.'",
    "billing_phone": "'.$billingmobile.'",
    "billing_alternate_phone":"",
    "shipping_is_billing": '.$true.',
    "shipping_customer_name": "",
    "shipping_last_name": "",
    "shipping_address": "'.$shippingaddress.'",
    "shipping_address_2": "",
    "shipping_city": "'.$shippingcity.'",
    "shipping_pincode": "'.$shippingpincode.'",
    "shipping_country": "India",
    "shipping_state": "'.$shippingstate.'",
    "shipping_email": "'.$email.'",
    "shipping_phone": "'.$shippingmobile.'",
    "order_items": ['.$shipproductarr.'],
    "payment_method": "'.$paymentmethod.'",
    "shipping_charges": "",
    "giftwrap_charges": "",
    "transaction_charges": "",
    "total_discount": "",
    "sub_total": "'.$totalamount.'",
    "length": "'.$length.'",
    "breadth": "'.$breadth.'",
    "height": "'.$height.'",
    "weight": "'.$weight.'",
    "ewaybill_no": "",
    "customer_gstin": ""
}';

$sql = mysqli_query($conn, "INSERT INTO api_response (response, api, orderid) VALUES ('$order_body', 'Shiprocket', '$order_id')");

$token = 'Bearer '.$loginApi["token"];
$bookorder_header = ['content-Type: application/json', 'Authorization: '.$token];
$api = callShipRocketApi(SHIP_CREATE_ORDER, $order_body, $bookorder_header, 'post');
    return $api;
}
}else{
    return "Empty Fields";
}

// echo $shipproductarr;
}



function verifyAddress($pincode){
    $auth_header = ['content-type: application/json'];
    $auth_body = '{
        "email": "'.SHIP_USERNAME.'",
        "password": "'.SHIP_PASSWORD.'"
    }';
    $loginApi = callShipRocketApi(SHIP_ROCKET_AUTH_URL, $auth_body, $auth_header, 'post');
    // print_r($loginApi);
    if(!empty($loginApi['token'])){
        $locationurl = "https://apiv2.shiprocket.in/v1/external/open/postcode/details?postcode=".$pincode;
        $token = 'Bearer '.$loginApi["token"];
        $bookorder_header = ['content-Type: application/json', 'Authorization: '.$token];
        $locationApi = callShipRocketApi($locationurl,'', $bookorder_header, 'get');
        // print_r($locationApi);
        return $locationApi;
    }
}


function trackOrder($shipid){
    $auth_header = ['content-type: application/json'];
    $auth_body = '{
        "email": "'.SHIP_USERNAME.'",
        "password": "'.SHIP_PASSWORD.'"
    }';
    $loginApi = callShipRocketApi(SHIP_ROCKET_AUTH_URL, $auth_body, $auth_header, 'post');
    if(!empty($loginApi['token'])){
        $locationurl = "https://apiv2.shiprocket.in/v1/external/courier/track?order_id=".$shipid."&channel_id=";
        $token = 'Bearer '.$loginApi["token"];
        $bookorder_header = ['content-Type: application/json', 'Authorization: '.$token];
        $locationApi = callShipRocketApi($locationurl,'', $bookorder_header, 'get');
        return $locationApi;
    }
}

function pickup_location($shipid){
    $auth_header = ['content-type: application/json'];
    $auth_body = '{
        "email": "'.SHIP_USERNAME.'",
        "password": "'.SHIP_PASSWORD.'"
    }';
    $loginApi = callShipRocketApi(SHIP_ROCKET_AUTH_URL, $auth_body, $auth_header, 'post');
    if(!empty($loginApi['token'])){
        $locationurl = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup";
        $token = 'Bearer '.$loginApi["token"];
        $bookorder_header = ['content-Type: application/json', 'Authorization: '.$token];
        $locationApi = callShipRocketApi($locationurl,'', $bookorder_header, 'get');
        return $locationApi;
    }
}

function cancelOrder($shipid){
    $auth_header = ['content-type: application/json'];
    $auth_body = '{
        "email": "'.SHIP_USERNAME.'",
        "password": "'.SHIP_PASSWORD.'"
    }';
    $loginApi = callShipRocketApi(SHIP_ROCKET_AUTH_URL, $auth_body, $auth_header, 'post');
    if(!empty($loginApi['token'])){
        $locationurl = "https://apiv2.shiprocket.in/v1/external/orders/cancel";
        $token = 'Bearer '.$loginApi["token"];
        $bookorder_header = ['content-Type: application/json', 'Authorization: '.$token];
        $order_body = '{"ids":["'.$shipid.'"]}';
        $locationApi = callShipRocketApi($locationurl, $order_body, $bookorder_header, 'post');
        return $locationApi;

    }
}
?>