                            <div class="col-md-4">
                                <label>Buying Season</label>
                                <select class="form-control" name="status">
                                    <option value="">-- Select Season --</option>
                                    <?php $statusvalue = (!empty($edit)) ? $array[16] : ''; ?>
                                    <?php display_option_selected('color', 0, 1, $statusvalue, 'status', 1); ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label>HSN Code</label>
                                <input type="text" class="form-control" name="hsncode" value="<?php echo (!empty($edit)) ? $array[37] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4 getattributes">
                                <label>Category<sup class="text-danger">*</sup></label>
                                <select class="form-control category" name="category" id="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[6] : '';
                                    display_option_selected('category', 1, 2, $value, 'ORDER BY category ASC') ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <div id="subcategory">
                                <label>Subcategory<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" required>
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[7] : '';
                                    display_option_selected('subcategory', 1, 3, $value, 'ORDER BY subcategory ASC') ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Scent Type<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="scent_type" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Fragrance Depth<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="fragrance_depth" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4 mine-select">
                                <label>Collection Tag<sup class="text-danger">*</sup></label>
                                <select class="" name="collection_tag" required>
                                    <option value="">-- Select Collection Tag --</option>
                                    <?php $value = (!empty($edit)) ? $array[27] : ''; ?>
                                    <option value="PE" <?php echo ($value == 'PE') ? 'selected="selected"' : ''; ?>>Permanent</option>
                                    <option value="SE" <?php echo ($value == 'SE') ? 'selected="selected"' : ''; ?>>Seasonal</option>
                                    <option value="SP" <?php echo ($value == 'SP') ? 'selected="selected"' : ''; ?>>Special</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label>UPC or VPN Code<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="vpn" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Style Name<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="style_code" value="<?php echo (!empty($edit)) ? $array[30] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Product Title<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[4].'"' : 'placeholder="Enter Product Title"'; ?> required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Manufacturing Date<sup class="text-danger">*</sup></label>
                                <input type="date" class="form-control" name="mnf_date" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Expiry Date<sup class="text-danger">*</sup></label>
                                <input type="date" class="form-control" name="expire_date" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Retail Price<sup class="text-danger">*</sup></label>
                                <input type="number" class="form-control" name="price" <?php echo (!empty($edit)) ? 'value="'.$array[10].'"' : 'placeholder="Enter Price"'; ?> required>
                            </div>

                            <div class="col-md-4">
                                <label>Size</label>
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type="text" name="size" class="form-control" value="<?php echo (!empty($edit)) ? $array[45] : '' ; ?>">
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="sizeunit">
                                            <option value="">-- Unit --</option>
                                            <option value="ml">ML</option>
                                            <option value="gm">GM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label>Gender</label>
                                <select class="form-control" name="gender">
                                    <option value="">-- Select Gender --</option>
                                    <option value="Men" <?php if(!empty($edit)) echo ($array[49] == 'Men') ? 'selected="selected"' : ''; ?>>Men</option>
                                    <option value="Women" <?php if(!empty($edit)) echo ($array[49] == 'Women') ? 'selected="selected"' : ''; ?>>Women</option>
                                    <option value="Kids" <?php if(!empty($edit)) echo ($array[49] == 'Kids') ? 'selected="selected"' : ''; ?>>Kids</option>
                                    <option value="Both" <?php if(!empty($edit)) echo ($array[49] == 'Both') ? 'selected="selected"' : ''; ?>>Unisex</option>
                                </select>
                            </div>

                            <!-- image starts -->

                            <div class="col-md-12">
                                <hr>
                                <h3 class="mb-0"><strong>Add Images<sup class="text-danger">*</sup></strong></h3>
                                <p class="text-danger mb-1">First & second images will be use as thumbnail.</p>
                                <p class="text-primary">Image size resolution (1020 * 1328) pixels</p>
                                <div class="row">
                                <div class="col-sm-2 imgUp">
                                    <div class="imagePreview"></div>
                                        <label class="btn btn-primary">Add Featured Image<input type="file" class="uploadFile img" name="file[]" style="width: 0px;height: 0px;overflow: hidden;" <?php echo (!empty($edit)) ? '' : 'required' ; ?>></label>
                                    </div><!-- col-2 -->
                                <span class="imgAdd">+</span>
                                </div>
                                <?php if(!empty($edit)){ ?>
                                    <div class="row">
                                        <?php 
                                            $imgsql = mysqli_query($conn, "SELECT * FROM product_images WHERE product='$array[5]'");
                                            if($imgsql){
                                                if(mysqli_num_rows($imgsql) > 0){
                                                    $vimagearr = [];
                                                    while($imgss = mysqli_fetch_array($imgsql)){
                                                        $vimagearr[] = $imgss;
                                                    }
                                                    $sno = 1;
                                                    ?>
                                        <table class="table table-bordered table-striped table-hover ">
                                            <tr>
                                                <th>S.No</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                            </tr>
                                            <?php for($i = 0; $i <= count($vimagearr) - 1; $i++){ ?>
                                                <tr>
                                                    <td><?php echo $sno++ ?></td>
                                                    <td><a href="<?php echo PRODUCT_DIRECTORY.$vimagearr[$i]['image'] ?>" target="_blank"><img src="<?php echo PRODUCT_DIRECTORY.$vimagearr[$i]['image'] ?>" width="100px" height="100px"></a></td>
                                                    <td><a href="delete.php?image=<?php echo $vimagearr[$i]['pi_id'] ?>" class="btn btn-danger">Delete</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <?php      }
                                            }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>

                            <!-- image ends -->

                            <div class="col-md-12" align="center">
                                <?php if(empty($edit)){ ?>
                                <input type="submit" name="add_product" value="Save & Continue" class="btn btn-success">
                                <?php }else{ ?>
                                    <input type="submit" name="update_product" value="Update Product" class="btn btn-success">
                                <?php } ?>
                            </div>