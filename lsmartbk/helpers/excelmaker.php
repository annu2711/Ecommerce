<?php
session_start();
define("LAST_URL", $_SERVER['HTTP_REFERER']);
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 

function exportExcel($clmarr, $dataarr, $fname){
// Excel file name for download 
$fileName = $fname. date('Ymd').".xlsx"; 
 
// Column names 
$fields = $clmarr; 

// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 
 
if(!empty($dataarr)){ 
    // Output each row of the data 
    for($i = 0; $i < count($dataarr); $i++){
        $rowData = []; 
        $rowData[] = $i;
        for($j = 0; $j < count($clmarr); $j++){
            if(!empty($dataarr[$i][$j])){
            $rowData[] = $dataarr[$i][$j];
            }else{
                $rowData[] = "NA";
            } 
        }
        array_walk($rowData, 'filterData'); 
        $excelData .= implode("\t", array_values($rowData)) . "\n"; 
    } 
}else{ 
    $excelData .= 'No records found...'. "\n"; 
     
} 
 
// Headers for download 
header("Content-Disposition: attachment; filename=Bquestindia.xlsx"); 
header("Content-Type: application/vnd.ms-excel"); 
 
// Render excel data 
echo $excelData; 
 
exit;

}
$clmarr = ['vpn', 'error'];
$dataarr = [['o1', 'o2'], ['o1', 'o2']];
exportExcel($clmarr, $dataarr, 'BQUESTINDIA-');
// if(isset($_SESSION['column'])){
//     $clmarr = $_SESSION['column'];
//     $dataarr = $_SESSION['dataarr'];
//     exportExcel($clmarr, $dataarr, 'BQUESTINDIA-');
//     unset($_SESSION['column']);
//     unset($_SESSION['dataarr']);
//     header('location:'.LAST_URL);
// }