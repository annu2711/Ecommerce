<?php include_once "helpers/index.php"; ?>
<section class="content">
    <div class="block-header mt-5">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product Bulk Upload</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="bulk-upload.php">Product Bulk Upload</a></li>
                    <li class="breadcrumb-item active">Creed Product Bulk Upload</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form acton="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Choose CSV Format File Only</label>
                                                    <input type="file" class="form-control" name="file" accept=".csv">
                                                </div>

                                                <div class="col-md-4">
                                                    <input type="submit" name="upload_excel" class="btn btn-success mt-4" value="Upload">
                                                    <a href="../csv-formats/creed-bulk-upload.xlsx" download="creed-bulk-upload.xlsx" class="btn btn-primary mt-4">Download CSV Format</a>
                                                </div>

                                                <div class="col-md-12">
                                                    <hr>
                                                    <h6 class="mt-3"><strong>Guidelines For Bulk Upload</strong></h6>
                                                    <ol type="1">
                                                        <li>Replace empty column with NA</li>
                                                        <li>There should not be any empty row in sheet </li>
                                                        <li>Do not use Special Character “|” in sheet</li>
                                                        <li>Make sure excel should not have more columns than the original one.</li>
                                                        <li>UPC/VPN numbers are unique and should not be duplicated</li>
                                                        <li>While uploading the Data sheet please remove the columns header</li>
                                                        <li>Please ensure all master data(Brand, Category, Subcategory etc) should be added previously in Masters.</li>
                                                        <li>Price of the products should be greater than 0.</li>
                                                        <li>File Format should be CSV only & "|" base seprated.</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>

</section>
<?php 

include_once "helpers/footer.php"; 

// excel upload items

if(isset($_POST["upload_excel"]))
{
 $file = $_FILES["file"]["tmp_name"];
 if(!empty($file)){
    $file_type = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
    if ($file_type == "csv" || $file_type == "CSV") {
        $filename = date('Ymdhis').$_FILES['file']['name'];
        $path = "../uploaded-csv/".$filename;
        // move_uploaded_file($file, $path);
        $file_open = fopen($file,"r");
        $success_count = 0;
        $faliure_count = 0;
        $error = "";
       $num = 1;
        while(($csv = fgetcsv($file_open, 1000, "|")) !== false)
        {
            $store = ($csv[0]!= "NA") ? $csv[0] : "";
            $brand = ($csv[1]!= "NA") ? $csv[1] : "";
            $buying_season = ($csv[2]!= "NA") ? $csv[2] : "";
            $hsncode = ($csv[3]!= "NA") ? $csv[3] : "";
            $category = ($csv[4]!= "NA") ? $csv[4] : "";
            $subcategory = ($csv[5]!= "NA") ? $csv[5] : "";
            $olfactory = ($csv[6]!= "NA") ? $csv[6] : "";
            $fragrance_depth = ($csv[7]!= "NA") ? $csv[7] : "";
            $collection_tag = ($csv[8]!= "NA") ? $csv[8] : "";
            $vpn = ($csv[9]!= "NA") ? $csv[9] : "";
            $style_name = ($csv[10]!= "NA") ? $csv[10] : "";
            $style_no = ($csv[11]!= "NA") ? $csv[11] : "";
            $title = ($csv[12]!= "NA") ? $csv[12] : "";
            $mnf_date = ($csv[13]!= "NA") ? $csv[13] : "";
            $expire_date = ($csv[14]!= "NA") ? $csv[14] : "";
            $price = ($csv[15]!= "NA") ? $csv[15] : "";
            $size = ($csv[16]!= "NA") ? $csv[16] : "";
            $unit = ($csv[17]!= "NA") ? $csv[17] : "";
            $gender = ($csv[18]!= "NA") ? $csv[18] : "";
            $featured_image = ($csv[19]!= "NA") ? $csv[19] : "";
            $other_image_1 = ($csv[20]!= "NA") ? $csv[20] : "";
            $other_image_2 = ($csv[21]!= "NA") ? $csv[21] : "";
            $other_image_3 = ($csv[22]!= "NA") ? $csv[22] : "";
            $other_image_4 = ($csv[23]!= "NA") ? $csv[23] : "";
            $other_image_5 = ($csv[24]!= "NA") ? $csv[24] : "";
            $goupid = ($csv[25]!= "NA") ? $csv[25] : "";
            $imagearray = [$featured_image, $other_image_1, $other_image_2, $other_image_3, $other_image_4, $other_image_5];

            //get store
            $sql = mysqli_query($conn, "SELECT store_key FROM store where store_name='$store' ORDER BY store_id ASC LIMIT 1");
            if(mysqli_num_rows($sql) > 0){
                $client = mysqli_fetch_assoc($sql);
                $store = $client['store_key'];
            }else{
                // $error = $error.mysqli_error($conn);
                $error = "Store not found,";
                $store = "";
            }

            // get brand
            $sql1 = mysqli_query($conn, "SELECT brand_key FROM brands WHERE brand_name='$brand' ORDER BY brand_id ASC LIMIT 1");
            if(mysqli_num_rows($sql1) > 0){
                $c = mysqli_fetch_assoc($sql1);
                $brand = $c['brand_key'];
            }else{
                $error = $error.", Brand not found,";
                $brand = "";
            }

            // get category
            $sql2 = mysqli_query($conn, "SELECT cat_key FROM category WHERE category='$category' ORDER BY cat_id ASC LIMIT 1");
            if(mysqli_num_rows($sql2) > 0){
                $p = mysqli_fetch_assoc($sql2);
                $category = $p['cat_key'];
            }else{
                $error = $error." Category not found,";
                $category = "";
            }

            // get Subcategory
            $sql3 = mysqli_query($conn, "SELECT subcat_key FROM subcategory WHERE subcategory='$subcategory' ORDER BY subcat_id ASC LIMIT 1");
            if(mysqli_num_rows($sql3) > 0){
                $it = mysqli_fetch_assoc($sql3);
                $subcategory = $it['subcat_key'];
            }else{
                // $error = $error.mysqli_error($conn);
                $subcategory = "";
            }
            // insert data starts
            if(!empty($store) && !empty($brand) && !empty($title) && !empty($category) && $price!=""){
                $code = $vpn;
                $chkvpn = check_duplicate('product', 'sku', $code);
                if($chkvpn == 1){
                    $faliure_count =  $faliure_count + 1;
                    $error = $error."Duplicate Product Found,";
                }else{
                    $type= 0;
                    $dig = $num + 1;
                    $groupid = empty($groupid) ? $vpn : $vpn;  
                    $publish = 1;
                    $color_code="";
                    mysqli_autocommit($conn, FALSE);
                    $key = rand_char(5);
                    try {
                        $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `price`, `publish`, `status`, `type`, `groupid`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                        $stmt->bind_param("sssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $price, $publish, $buying_season, $type, $groupid, $created_at);
                        if ($stmt->execute()) {
                            $stmtb = $conn->prepare("INSERT INTO berluti (p_id, collection_tag, vpn, style_name, style_no,  hsncode, scent_type, fragrance_depth, mnf_date, expire_date, created_at) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                            $stmtb->bind_param('sssssssssss', $code, $collection_tag, $vpn, $style_name, $style_no, $hsncode, $olfactory, $fragrance_depth, $mnf_date, $expire_date, $created_at);
                            if($stmtb->execute()){
                                try{
                                    // $color  = (!empty($color)) ? strtoupper(str_replace(" ","",$color)) : '' ;
                                    $size  = (!empty($size)) ? strtoupper($size) : '' ;
                                    $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, size, gender, created_at) VALUES (?, ?, ?, ?, ?, ?)");
                                    $stmt2->bind_param('ssssss', $code, $groupid, $size, $unit, $gender, $created_at);
                                    if($stmt2->execute()){
                                        if(!empty($imagearray)){
                                            foreach($imagearray as $keey => $tmp_name){
                                                if(!empty($tmp_name)){
                                                    $tmp_name = str_replace(" ","",$tmp_name).".jpg";
                                                    $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$tmp_name', '$created_at')");
                                                }
                                            }
                                         }
                                        $success_count = $success_count + 1;
                                        mysqli_commit($conn);
                                    }else{
                                        mysqli_rollback($conn);
                                        throw new exception($conn->error);
                                    }
                                }catch(Exception $e){
                                    echo $error = $error."Error: " . $e->getMessage();
                                     $faliure_count = $faliure_count + 1;
                                }
                            }else{
                                mysqli_rollback($conn);
                                $error = $error.mysqli_error($conn);
                                $faliure_count = $faliure_count + 1;
                            }
                        }else{
                            throw new exception($conn->error);
                        }
                    }catch(Exception $e){
                        echo $error = $error."Error: " . $e->getMessage().",";
                        $faliure_count = $faliure_count + 1;
                    }
                }
            }else{
                $error = $error."Manadatory fields are empty.";
                $faliure_count = $faliure_count + 1;
                
            }
            // insert data ends
            $data = $store."|".$brand."|".$buying_season."|".$hsncode."|".$category."|".$subcategory."|".$collection_tag."|".$vpn."|".$style_no."|".$style_name."|".$olfactory."|".$fragrance_depth."|".$mnf_date."|".$expire_date."|".$title."|".$size."|".$price."|".$gender."|".$unit."|".$featured_image."|".$other_image_1."|".$other_image_2."|".$other_image_3."|".$other_image_4."|".$other_image_5."|".$goupid;
            $sql = mysqli_query($conn, "INSERT INTO excel_upload_data (excel_name, vpn, data, error, created_at) VALUES ('$filename', '$vpn', '$data', '$error', '$created_at')");
            
        }
    move_uploaded_file($file, $path);
    echo "<script>showNotification('alert-success', '".$success_count." Product Added Successfully', 'top', 'right', '', '')</script>";
    echo "<script>showNotification('alert-danger', '".$faliure_count." Product Not Added ', 'top', 'right', '', '')</script>";
    echo "SELECT vpn, error FROM excel_upload_data WHERE excel_name='$filename'";
    $query_new = mysqli_query($conn, "SELECT vpn, error FROM excel_upload_data WHERE excel_name='$filename'");
    if($query_new){
        if(mysqli_num_rows($query_new) > 0){
            $clmarr = ['vpn', 'error'];
            $dataarr = [];
            while($rows = mysqli_fetch_array($query_new)){
                $dataarr[] = $rows;
            }
            // print_r($clmarr);
            print_r($dataarr);
            // $_SESSION['column'] = $clmarr;
            // $_SESSION['dataarr'] = $dataarr;
            // header('location: helpers/excelmaker.php');
        }
    }else{
        echo mysqli_error($conn);
    }
    }else{
        echo "<script>showNotification('alert-success', 'Invalid File Format', 'top', 'right', '', '')</script>";
    }
 }else{
    echo "<script>showNotification('alert-success', 'Please Select File', 'top', 'right', '', '')</script>";
 }

}



?>
