<?php 
    include_once "helpers/index.php";
    userAccess(['1', '2'], $deg);
    $edit = "";
    if(isset($_GET['admin'])){
        extract($_GET);
        if(!empty($admin)){
        // $store_key = $store;
        $sql = mysqli_query($conn, "SELECT * FROM admin_users WHERE au_id='$admin'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Admin User
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Admin User</a></li>
                    <li class="breadcrumb-item active">Add User</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Name<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="name" <?php echo (!empty($edit)) ? 'value="'.$array[4].'"' : 'placeholder="Enter Store Name"'; ?> required>
                            </div>
                            <div class="form-group form-float">
                                <label>Designation<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="designation">
                                    <option value="">-- Select Designation --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[7] : '';
                                    display_option_selected('designation', 0, 1, $value) ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label>Email Id<sup class="text-danger">*</sup></label>
                                <input type="email" class="form-control" name="email" <?php echo (!empty($edit)) ? 'value="'.$array[5].'"' : 'placeholder="Enter Email"'; ?> required>                                
                            </div>
                            <div class="form-group form-float">
                                <label>Mobile No<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="mobile" maxlength="10" <?php echo (!empty($edit)) ? 'value="'.$array[6].'"' : 'placeholder="Enter mobile"'; ?> required>                              
                            </div>
                            <div class="form-group form-float">
                                <label>Username<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="username" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Username"'; ?>>                                
                            </div>
                            <?php ?>
                            <div class="form-group form-float">
                                <label>Password<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="password" value="" required>                                
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[8] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[8] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_store">UPDATE</button>
                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_store">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
    </div>
</section>
<?php 
include_once "helpers/footer.php";
// save store

if(isset($_POST['add_store'])){
    extract($_POST);
    if(!empty($name) && !empty($designation) && !empty($email) && !empty($mobile) && !empty($username) && !empty($password)){
        $chck_mob = check_duplicate('admin_users', 'mobile', $mobile);
        $chck_email = check_duplicate('admin_users', 'email', $email);
        $chck_username = check_duplicate('admin_users', 'username', $username);
        if($chck_email == 1){
            echo "<script>showNotification('alert-warning', 'Email already exists', 'top', 'right', '', '')</script>";
        }
        if($chck_mob == 1){
            echo "<script>showNotification('alert-warning', 'Mobile No. already exists', 'top', 'right', '', '')</script>";
        }
        if($chck_username == 1){
            echo "<script>showNotification('alert-warning', 'Username already exists', 'top', 'right', '', '')</script>";
        }
        if($chck_email == 0 && $chck_mob == 0 && $chck_username == 0){
            $key = rand_char(5);
            $pass = md5($password);
            try{
                $stmt = $conn->prepare("INSERT INTO admin_users (username, password, name, email, mobile, deg, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param('sssssss', $username, $pass, $name, $email, $mobile, $designation, $created_at);
                if($stmt->execute()){
                    echo "<script>showNotification('alert-success', 'User Added Successfully', 'top', 'right', '', '')</script>";
                }else{
                    echo mysqli_error($conn);
                    throw new exception($conn->error);
                }
            }catch(Exception $e){
                echo "<script>showNotification('alert-danger', 'User Not Added ', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_store'])){
    extract($_POST);
    if(!empty($name) && !empty($designation) && !empty($email) && !empty($mobile) && !empty($username) && !empty($password)){
        $query = mysqli_query($conn, "UPDATE admin_users SET name='$name', email='$email', mobile='$mobile', username='$username', password='$password', deg='$designation', status='$status' WHERE au_id='$admin'");
        if($query){
            $_SESSION['result'] = true;
            header('location: user-list');
        }else{
            echo "<script>showNotification('alert-danger', 'User Not Updated ', 'top', 'right', '', '')</script>";
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}

?>