<?php 
include_once "helpers/index.php"; 

$edit = "";
    if(isset($_GET['subcat'])){
        extract($_GET);
        if(!empty($subcat)){
        $sql = mysqli_query($conn, "SELECT * FROM subcategory WHERE subcat_key='$subcat'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Subcategory
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Subcategory</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Category Name<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label>Subcategory Name</label>
                                <input type="text" class="form-control" name="subcategory" <?php echo (!empty($edit)) ? 'value="'.$array[3].'"' : 'placeholder="Enter Subcategory"'; ?>  required>                              
                            </div>
                          
                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[4] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[4] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_subcategory">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_subcategory">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Subcategory</strong> List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT t1.*, t2.category FROM subcategory as t1 join category as t2 on t1.cat_key=t2.cat_key");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['category'] ?></td>
                                        <td><?php echo $rows['subcategory'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="subcategory?subcat=<?php echo $rows['subcat_key'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php include_once "helpers/footer.php"; 
flash_session_admin();
if(isset($_POST['add_subcategory'])){
    extract($_POST);
    if(!empty($category) && !empty($subcategory)){
        $chck_brand = check_duplicate('subcategory', 'subcategory', $subcategory, 'cat_key', $category);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Subcategory already exists', 'top', 'right', '', '')</script>";
        }else{
            $key = rand_char(5);
            $query = mysqli_query($conn, "INSERT INTO subcategory (subcat_key, cat_key, subcategory, status, created_at) VALUES ('$key', '$category', '$subcategory', 1, '$created_at')");
            if($query){
                $_SESSION['result'] = [true, 'Subcategory Added Successfully', 'success'];
            }else{
                $_SESSION['result'] = [false, 'Subcategory Not Added ', 'error'];
            }
            header('location: subcategory');
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_subcategory'])){
    extract($_POST);
    if(!empty($category) && !empty($subcategory)){
        $query = mysqli_query($conn, "UPDATE subcategory SET cat_key='$category', subcategory='$subcategory', status='$status' WHERE subcat_key='$subcat'");
        if($query){
            $_SESSION['result'] = [true, 'Subcategory Updated Successfully', 'success'];
        }else{
            $_SESSION['result'] = [false, 'Subcategory Not Updated', 'error'];
        }
        header('location: subcategory');

    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>