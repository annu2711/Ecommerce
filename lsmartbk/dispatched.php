<?php 
    include_once "helpers/index.php";
    userAccess(['1', '2'], $deg);
    // $msg = mobilesms('9718190486', 'Hii This is Test msg');

?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Order List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Orders</a></li>
                    <li class="breadcrumb-item active">Order List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Date & Time</th>
                                        <th>Order ID</th>
                                        <th>Client Name</th>
                                        <th>Shiping Address</th>
                                        <th>Billing Address</th>
                                        <th>Payment</th>
                                        <th>Total Items</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT t1.*, t2.title, t2.name, t2.last_name FROM orders as t1 JOIN clients AS t2 ON t1.user=t2.client_key WHERE t1.d_status=1 ORDER BY order_id DESC");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $num++ ?></td>
                                            <td><?php echo $rows['created_at'] ?></td>
                                            <td><?php echo $rows['order_key'] ?></td>
                                            <td><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name']  ?></td>
                                            <td><?php echo getComleteAddress($rows['shipping_address'], 'address_book', 'ab_key') ?></td>
                                            <td><?php if($rows['billing_address'] == $rows['shipping_address']){ echo '<p class="text-dager">Same As Shipping Address</p>'; }else{
                                                echo getComleteAddress($rows['billing_address'], 'billing_addresses', 'ba_key');
                                            } ?></td>
                                            <td><?php echo $rows['payment'] ?></td>
                                            <td><?php echo $rows['total_items'] ?></td>
                                            <td><?php echo $rows['amount'] ?></td>
                                            <td><?php echo orderStatus($rows['d_status']) ?></td>
                                            <td><button type="button" class="btn btn-success waves-effect" data-toggle="modal" data-target="#largeModal" onclick="getOrderDetail('<?php echo $rows['order_key'] ?>')">View Detail</button></td>
                                        </tr>
                                            <?php }
                                            }
                                        }else{
                                            echo mysqli_error($conn);
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>
<!-- Large Size -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Order Detail</h4>
            </div>
            <div class="modal-body"> 
                <table class="table table-bordered table-striped table-hover" id="order-table">
                    <thead>
                    <tr>
                        <th>Product Img</th>
                        <th>Product</th>
                        <th>Color</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody></tbody><tfoot><tr><td colspan="5" align="right">Total</td><td><strong id="total"></strong></td></tr></tfoot>
                </table>
                <hr>
                <input type="hidden" name="orderid" id="orderid" value="">
                <div class="row">
                    <div class="col-md-6">
                        <label>Order Dispatched Date</label>
                        <input type="date" class="form-control" name="date">
                    </div>
                    <div class="col-md-6">
                        <label>Order Status</label>
                        <select class="form-control" name="status">
                            <option value="2">Delivered</option>
                            <option value="5">Cancel</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="order_status" class="btn btn-success btn-round waves-effect">Order Dispatched</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    function getOrderDetail(x){
        if(x != ""){
            $.ajax({
                type: 'post',
                url: 'helpers/event.php',
                data: {orderdetail: x},
                success: function(data){
                    // alert(data);
                    $('#order-table tbody').html(" ");
                    $('#total').text(" ");
                    $('#order-table tbody').html(data);
                    var price = 0;
                    $('.order-price').each(function(e){
                        price = price + parseInt($(this).text());
                    });
                    $('#total').text(price);
                    $('#orderid').val(" ");
                    $('#orderid').val(x);
                }
            })
        }
    }

    // function tableTotal(tbl){

    // }
</script>
<?php include_once"helpers/footer.php";


if(isset($_POST['order_status'])){
    extract($_POST);
    if(!empty($orderid) && !empty($date) && !empty($status)){
        mysqli_autocommit($conn, FALSE);
        $query = mysqli_query($conn, "UPDATE `orders` SET `d_status`='$status' WHERE `order_key`='$orderid'");
        if($query){
            $sql = mysqli_query($conn, "INSERT INTO orders_status (orderid, status, status_date) VALUES ('$orderid', '$status', '$date')");
            if($sql){
            mysqli_commit($conn);
            $mobile = getSinglevalue('clients', 'client_key', getSinglevalue('orders', 'order_key', $orderid, 2), 6);
            $deliveredcontent = 'Delivery Successful!<br>
            Thanks for shopping with Lsmart. We hope you have a lovely day. For today’s deal '.$url;
            $msg = mobilesms($mobile, $deliveredcontent);
            echo "<script>showNotification('alert-info', 'Status Changed Successfully', 'top', 'right', '', '')</script>";
            header('refresh: 0.5');
        }else{
            mysqli_rollback($conn);
        }
        }else{
            echo "<script>showNotification('alert-danger', 'Status Not Changed ', 'top', 'right', '', '')</script>";
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}




// if(isset($_POST['order_status'])){
//     extract($_POST);
//     if(!empty($orderid) && !empty($date) && !empty($status)){
//         $query = mysqli_query($conn, "UPDATE `orders` SET `d_status`='$status' WHERE `order_key`='$orderid'");
//         if($query){
//             $mobile = getSinglevalue('clients', 'client_key', getSinglevalue('orders', 'order_key', $orderid, 2), 6);
//             $deliveredcontent = 'Delivery Successful!<br>
//             Thanks for shopping with Lsmart. We hope you have a lovely day. For today’s deal '.$url;
//             $msg = mobilesms($mobile, $deliveredcontent);
//         //     echo $msg;
        
//         // {"status":"success","totalnumbers_sbmited":1,"campg_id":129325,"logid":"5ee0c10d7950d","code":"100","ts":"2020-06-10 16:46:29"}

//             echo "<script>showNotification('alert-info', 'Status Changed Successfully', 'top', 'right', '', '')</script>";
//             header('refresh: 0.5');
//         }else{
//             echo "<script>showNotification('alert-danger', 'Status Not Changed Successfully', 'top', 'right', '', '')</script>";
//         }
//     }else{
//         echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
//     }
// }


?>