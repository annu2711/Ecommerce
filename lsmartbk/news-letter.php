<?php 
    include_once"helpers/index.php";
?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>News Letter</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">User</a></li>
                    <li class="breadcrumb-item active">News Letter</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">

         <!-- Table Starts -->

         <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>News Letter</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM news_letter");
                                    if($query){
                                        $num = 1;
                                        if(mysqli_num_rows($query) > 0){
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['email'] ?></td>
                                    </tr>
                                    <?php   }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>

    </section>



<?php 
include_once"helpers/footer.php";
?>