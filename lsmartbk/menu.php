<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['menu'])){
        extract($_GET);
        if(!empty($menu)){
        $menu_id = $_GET['menu'];
        $sql = mysqli_query($conn, "SELECT * FROM menu WHERE menu_id='$menu_id'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Menu
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Menu</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">

                        <div class="form-group form-float">
                                <label>Menu Name</label>
                                <input type="text" class="form-control" name="menu" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Brand Name"'; ?> required>                              
                            </div>
                            <div class="form-group form-float">
                                <label>Select Category<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[3] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[3] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_menu">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_menu">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 

         <!-- Table Starts -->

         <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Menu</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Menu</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM menu");
                                    if($query){
                                        $num = 1;
                                        if(mysqli_num_rows($query) > 0){
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['menu_name'] ?></td>
                                        <td><?php echo getSinglevalue('category', 'cat_key', $rows['category'], 2) ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="menu?menu=<?php echo $rows['menu_id'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <?php   }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->


    </div>

    </section>



<?php 
include_once"helpers/footer.php";

if(isset($_POST['add_menu'])){
    extract($_POST);
    if(!empty($menu) && !empty($category)){
        $chck_brand = check_duplicate('menu', 'menu_name', $menu);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Menu already exists', 'top', 'right', '', '')</script>";
        }else{
            $query = mysqli_query($conn, "INSERT INTO menu (menu_name, category, status, created_at) VALUES ('$menu', '$category', 1, '$created_at')");
            if($query){
                echo "<script>showNotification('alert-info', 'Menu Added Successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');
            }else{
                echo "<script>showNotification('alert-danger', 'Menu Not Added', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_menu'])){
    extract($_POST);
    if(!empty($menu) && !empty($category)){
        $query = mysqli_query($conn, "UPDATE menu SET menu_name='$menu', category='$category', status='$status' WHERE menu_id='$menu_id'");
        if($query){
            echo "<script>showNotification('alert-info', 'Menu Updated Successfully', 'top', 'right', '', '')</script>";
            header('location: menu');
        }else{
            echo "<script>showNotification('alert-danger', 'Menu Not Updated Successfully', 'top', 'right', '', '')</script>";
        }

    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>