<?php
include_once "helpers/index.php";
?>
<section class="content">
    <div class="block-header mt-5">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Stock Management<h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="bulk-upload.php">Stock Management</a></li>
                    <li class="breadcrumb-item active">Stock Bulk Upload</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form acton="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Choose CSV Format File Only</label>
                                                    <input type="file" class="form-control" name="file" accept=".csv">
                                                </div>

                                                <div class="col-md-4">
                                                    <input type="submit" name="upload_excel" class="btn btn-success mt-4" value="Upload">
                                                    <a href="../csv-formats/bulk-stock.xlsx" download="stock-bulk-upload.xlsx" class="btn btn-primary mt-4">Download CSV Format</a>
                                                </div>

                                                <div class="col-md-12">
                                                    <hr>
                                                    <h6 class="mt-3"><strong>Guidelines For Bulk Upload</strong></h6>
                                                    <ol type="1">
                                                        <li>Replace empty column with NA</li>
                                                        <li>There should not be any empty row in sheet </li>
                                                        <li>Do not use Special Character “|” in sheet</li>
                                                        <li>Make sure excel should not have more columns than the original one.</li>
                                                        <li>UPC/VPN numbers are unique and should not be duplicated</li>
                                                        <li>While uploading the Data sheet please remove the columns header</li>
                                                        <li>Please ensure all master data(Brand, Category, Subcategory etc) should be added previously in Masters.</li>
                                                        <li>Price of the products should be greater than 0.</li>
                                                        <li>File Format should be CSV only & "|" base seprated.</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>

</section>


<?php
include_once "helpers/footer.php";


if(isset($_POST["upload_excel"]))
{
 $file = $_FILES["file"]["tmp_name"];
 if(!empty($file)){
    $file_type = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
    if ($file_type == "csv" || $file_type == "CSV") {
        $filename = date('Ymdhis').$_FILES['file']['name'];
        $path = "../uploaded-csv/".$filename;
        // move_uploaded_file($file, $path);
        $file_open = fopen($file,"r");
        $success_count = 0;
        $faliure_count = 0;
        $error = "";
        // $checklist = "";
        // $success_user = "";
        // $failure_user = [];
       $num = 1;
        while(($csv = fgetcsv($file_open, 1000, "|")) !== false)
        {
            $vpn = ($csv[0]!= "NA") ? $csv[0] : "";
            $invoice = ($csv[1]!= "NA") ? $csv[1] : "";
            $order_date = ($csv[2]!= "NA") ? $csv[2] : "";
            $qty = ($csv[3]!= "NA") ? $csv[3] : "";
            $start_ship = ($csv[4]!= "NA") ? $csv[4] : "";
            $end_ship = ($csv[5]!= "NA") ? $csv[5] : "";
            $price = ($csv[6]!= "NA") ? $csv[6] : "";

            // insert data starts
            if(!empty($vpn) && !empty($invoice) && !empty($qty)){
                try{
                    $stmt = $conn->prepare("INSERT INTO product_stock (product, challan, date, qty, start_date, end_date, purchase_price, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param('ssssssss', $vpn, $invoice, $order_date, $qty, $start_ship, $end_ship, $price, $created_at);
                    if($stmt->execute()){
                        $success_count = $success_count + 1;
                    }else{
                        throw new exception($conn->error);
                    }
                }catch(Exception $e){
                    echo "Error: " . $e->getMessage();
                    $faliure_count = $faliure_count + 1;

                }
            }else{
                $error = $error."Manadatory fields are empty.";
                $faliure_count = $faliure_count + 1;
            }
            // insert data ends
            $data = $vpn."|".$invoice."|".$order_date."|".$qty."|".$start_ship."|".$end_ship."|".$price;
            $sql = mysqli_query($conn, "INSERT INTO excel_upload_data (excel_name, vpn, data, error, created_at) VALUES ('$filename', '$vpn', '$data', '$error', '$created_at')");
            
        }
    move_uploaded_file($file, $path);
    echo "<script>showNotification('alert-success', '".$success_count." Products stock upload Successfully', 'top', 'right', '', '')</script>";
    echo "<script>showNotification('alert-danger', '".$faliure_count." Product stock Not upload ', 'top', 'right', '', '')</script>";
    $query_new = mysqli_query($conn, "SELECT vpn, error FROM excel_upload_data WHERE excel_name='$filename'");
    if($query_new){
        if(mysqli_num_rows($query_new) > 0){
            $clmarr = ['vpn', 'error'];
            $dataarr = [];
            while($rows = mysqli_fetch_array($query_new)){
                $dataarr[] = $rows;
            }
            print_r($dataarr);
        }
    }else{
        echo mysqli_error($conn);
    }

    }else{
        // echo "<script>showSuccessMsg('Invalid File Format', 'Please Select CSV Format Only', 'danger')</script>";
        echo "<script>showNotification('alert-danger', 'Invalid File Format', 'top', 'right', '', '')</script>";
    }
 }else{
    // echo "<script>showSuccessMsg('File Not Found', 'Please Select File', 'danger')</script>";
    echo "<script>showNotification('alert-danger', 'Please Select File', 'top', 'right', '', '')</script>";
 }

}
?>