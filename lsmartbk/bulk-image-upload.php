<?php include_once "helpers/index.php"; ?>
<section class="content">
    <div class="block-header mt-5">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Image Bulk Upload</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="bulk-upload.php">Product Management</a></li>
                    <li class="breadcrumb-item active">Image Bulk Upload</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form acton="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Choose Multitple Images</label>
                                                    <input type="file" class="form-control" name="file[]" accept="image/x-png,image/jpg,image/jpeg" multiple="multiple">
                                                </div>

                                                <div class="col-md-4">
                                                    <input type="submit" name="upload_images" class="btn btn-success mt-2" value="Upload">
                                                </div>

                                                <div class="col-md-12">
                                                    <hr>
                                                    <h6 class="mt-3"><strong>Guidelines For Bulk Upload</strong></h6>
                                                    <ol type="1">
                                                        <li>Replace empty column with NA</li>
                                                        <li>There should not be any empty row in sheet </li>
                                                        <li>Do not use Special Character “|” in sheet</li>
                                                        <li>Make sure excel should not have more columns than the original one.</li>
                                                        <li>UPC/VPN numbers are unique and should not be duplicated</li>
                                                        <li>While uploading the Data sheet please remove the columns header</li>
                                                        <li>Please ensure all master data(Brand, Category, Subcategory etc) should be added previously in Masters.</li>
                                                        <li>Price of the products should be greater than 0.</li>
                                                        <li>File Format should be CSV only & "|" base seprated.</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>

</section>
<?php 

include_once "helpers/footer.php"; 

// excel upload items

if(isset($_POST["upload_images"])){
    extract($_POST);
    if(!empty($_FILES['file']['name'])){
        $tmp_files_arr = $_FILES['file']['tmp_name'];
        $success = 0;
        $failure = 0;
        foreach($tmp_files_arr as $key => $tmp_name){
            $file_name = $_FILES['file']['name'][$key];
            list($width, $height) = getimagesize($tmp_name);
            // echo $width;
            // echo $height;
            if($width == '1020' && $height == '1328'){
                $filename = fileUpload($file_name, $tmp_name, $file_name, PRODUCT_DIRECTORY);

                if(!empty($filename)){
                    $success = $success + 1;
                }else{
                    $failure = $failure + 1;
                }
            }else{
                $failure = $failure + 1;
            }
        }
        echo "<script>showNotification('alert-success', '".$success." Images Upload Successfully', 'top', 'right', '', '')</script>";
        if($failure > 0){
            echo "<script>showNotification('alert-danger', '".$failure." Images Not Upload ', 'top', 'right', '', '')</script>";
        }

    }else{
        echo "<script>showNotification('alert-success', 'Please Select Images', 'top', 'right', '', '')</script>";
    }
}



?>
