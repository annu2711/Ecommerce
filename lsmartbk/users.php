<?php 
    include_once "helpers/index.php";

 
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Client List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> User Management</a></li>
                    <li class="breadcrumb-item active">Registered Users List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Client Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                    
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT * FROM clients");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $num++ ?></td>
                                            <td><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name'] ?></td>
                                            <td><?php echo $rows['email'] ?></td>
                                            <td><?php echo $rows['mobile'] ?></td>
                                            <td><?php status($rows['status']) ?></td>
                                            <td>
                                            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                                            <input type="hidden" name="clientid" value="<?php echo $rows['client_key'] ?>">
                                            <input type="hidden" name="status" value="<?php echo $rows['status'] ?>">
                                            <button type="submit" name="change_status" class="btn btn-primary">Change Status</button>
                                            </form>
                                            </td>
                                        </tr>
                                            <?php }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once"helpers/footer.php";

if(isset($_POST['change_status'])){
    extract($_POST);
    if(!empty($clientid) && $status!= ""){
        $newstatus = ($status == '0') ? 1 : 0 ;
        $query = mysqli_query($conn, "UPDATE clients SET status='$newstatus' WHERE client_key='$clientid'");
        if($query){
            echo "<script>showNotification('alert-info', 'Status Updated Successfully', 'top', 'right', '', '')</script>";
            header('refresh: 0.5');
        }else{
            echo "<script>showNotification('alert-error', 'Status Not Updated Successfully', 'top', 'right', '', '')</script>";
        }
    }
}

?>