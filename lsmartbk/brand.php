<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['brand'])){
        extract($_GET);
        if(!empty($brand)){
        $brand_key = $_GET['brand'];
        $sql = mysqli_query($conn, "SELECT * FROM brands WHERE brand_key='$brand_key'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }

?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Brand
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Brand</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Store Name<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="store" required>
                                    <option value="">-- Select Store --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('store', 1, 2, $value) ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label>Brand Name</label>
                                <input type="text" class="form-control" name="brand" <?php echo (!empty($edit)) ? 'value="'.$array[3].'"' : 'placeholder="Enter Brand Name"'; ?> required>                              
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[4] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[4] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_brand">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_brand">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 

        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Brands</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Store Name</th>
                                        <th>Brand Name</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT t1.*, t2.store_name FROM brands as t1 join store as t2 on t1.store_key=t2.store_key");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['store_name'] ?></td>
                                        <td><?php echo $rows['brand_name'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="brand?brand=<?php echo $rows['brand_key'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 

include_once "helpers/footer.php"; 
flash_session_admin();
if(isset($_POST['add_brand'])){
    extract($_POST);
    if(!empty($store) && !empty($brand)){
        $chck_brand = check_duplicate('brands', 'brand_name', $brand, 'store_key', $store);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Brand already exists', 'top', 'right', '', '')</script>";
        }else{
            $key = rand_char(5);
            $query = mysqli_query($conn, "INSERT INTO brands (brand_key, store_key, brand_name, status, created_at) VALUES ('$key', '$store', '$brand', 1, '$created_at')");
            if($query){
                $_SESSION['result'] = [true, 'Brand Added Successfully', 'success'];
            }else{
                $_SESSION['result'] = [true, 'Brand Not Added Successfully', 'success'];
            }
            header('location:brand');
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}



if(isset($_POST['update_brand'])){
    extract($_POST);
    if(!empty($store) && !empty($brand)){
        $query = mysqli_query($conn, "UPDATE brands SET store_key='$store', brand_name='$brand', status='$status' WHERE brand_key='$brand_key'");
        if($query){
            $_SESSION['result'] = [true, 'Brand Updated Successfully', 'success'];
        }else{
            $_SESSION['result'] = [true, 'Brand Not Updated Successfully', 'success'];
        }
        header('location:brand');

    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>
