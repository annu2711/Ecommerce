// alert('hello');

// open login alert

function loginAlert(){
    $('.loginmodal').css('display', 'block');
}

// brands
// $('#store').on('change', function(){
//     var store = $('#store').val();
//     // alert(store);
//     if(store!= ""){
//         $.ajax({
//             type:'post',
//             url:'helpers/event.php',
//             data:{store:store},
//             success: function(data){
//                 // alert(data);
//                 var singleQ = "'";
//                 $('#brand').html("");
//                 $('#brand').html('<label>Brand<sup classs="text-danger">*</sup></label><select class="form-control brand" name="brand" onchange="brandFields(' + singleQ + 'brand' + singleQ + ')" id="brand"><option value="">-- Select Category --</option>' + data + '</select>');
//             }
//         });
//     }
// });

// subcategory
$('#category').on('change', function(){
    var category = $('#category').val();
    if(category!= ""){
        $.ajax({
            type:'post',
            url:'helpers/event.php',
            data: {category: category},
            success: function(data){
                // alert(data);
                $('#subcategory').html("");
                $('#subcategory').html('<label>Subcategory<sup class="text-danger">*</sup></label><select class="form-control" name="subcategory"><option value="">-- Select Subcategory --</option>' + data + '</select>');
            }
        })
    }
});


// menu change
$('#menu').on('change', function(){
    var menu = $('#menu').val();
    if(menu!= ""){
        $.ajax({
            type:'post',
            url:'helpers/event.php',
            data: {menu:menu},
            success: function(data){
                $('#subcategory').html("");
                $('#subcategory').html('<label>Subcategory<sup classs="text-danger">*</sup></label><select class="form-control subcategory" name="subcategory" id="subcategory" data-live-search="true"><option value="">-- Select Subcategory --</option>' + data + '</select>');
            }
        })
    }
});

// add custom attributes

function addAttr(){
    var rowLength = $('.attr-table tbody tr').length;
    var id = 'newrow_' + rowLength;
    var singleQ = "'";
    var data = '<tr id="' + id + '"><td><div class="form-group"><input type="text" class="form-control" name="size[]" placeholder="Enter Size"></div><span class="text-primary float-right" onclick="removeThis(' + singleQ + id + singleQ + ')">- Remove Row</span></td></tr>';
    $('.attr-table tbody').append(data);
}

function addMoreColor(){
    var rowLength = $('.color-row-parent .color-row').length;
    var id = 'color_row_' + rowLength;
    var singleQ = "'";
    var content = '<div class="col-md-12 color-row" id="color_row_' + id + '"> <div class="row"> <div class="col-md-12"> <label>Color <span class="text-danger">[ <strong>Note:</strong> Enter six digit color code. Do not enter <strong>"#"</strong> .]</span></label> <input type="text" class="form-control" placeholder="Enter Color Code"> </div><div class="col-md-6 mt-3"> <div class="table-responsive"> <table class="table table-hover attr-table"> <thead> <tr> <th>Add Size</th> </tr></thead> <tbody> <tr> <td> <div class="form-group"> <input type="text" name="size[]" class="form-control" placeholder="Enter Size"> </div></td></tr></tbody> <tfoot> <tr> <td colspan="5"><span class="text-primary float-right" onclick="addAttr()">+Add More Size</span></td></tr></tfoot> </table> </div></div><div class="col-md-6"> <label class="mt-2">Color Primary Image<sup>*</sup></label> <input type="file" name="primary_img" id="slide_file" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image(' + singleQ + 'slide_file' + singleQ + ', ' + singleQ + 'image_preview1' + singleQ + ')"> <div id="image_preview1" class="img_preview_block"></div><label class="mt-2">Color Secondary Image</label> <input type="file" name="second_img" id="slide_file1" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image(' + singleQ + 'slide_file1' + singleQ + ', ' + singleQ + 'image_preview2' + singleQ + ')"> <div id="image_preview2" class="img_preview_block"></div><label class="mt-2">Color Variation Images</label> <input type="file" name="other_img[]" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" multiple> </div><div class="col-md-12"><p class="text-danger float-right mt-3" onclick="removeColor(' + singleQ + id + singleQ + ')">Remove Color</p></div><div class="col-md-12"><hr class="mt-2"></div></div>';
    $('.color-row-parent').append(content);
}

function removeThis(x){
    $('.attr-table tbody #' + x).remove();
}

function removeColor(x){
    $('.color-row-parent #color_row_' + x).remove();
}

//image preview
function preview_image(x, y) {
 var total_file=document.getElementById(x).files.length;
 $('#' + y).html(" ");
 for(var i=0;i<total_file;i++)
 {
  $('#' + y).append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
 }
}


// onkeyup="motorTotal()"

function setQty(x){
    var myval = $('.qtr_' + x).val();
    // alert(myval);
    if(myval == "" || myval == 0){
        $('.qtr_' + x).val(1);
    }
}

// image preview
$(".imgAdd").click(function(){
    $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" name="file[]" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
  });

  function addImage(x){
    // alert('hii');
  $('.imgAddd').closest(".row").find('.imgAddd').before('<div class="col-sm-2 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" name="' + x + '[]" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
};

  $(document).on("click", "i.del" , function() {
      $(this).parent().remove();
  });
  $(function() {
      $(document).on("change",".uploadFile", function()
      {
              var uploadFile = $(this);
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
   
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
   
              reader.onloadend = function(){ // set image data as background of div
                  //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
  uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
              }
          }
        
      });
  });

  function addAttribute(attrtype){
    //   alert($('#' + attrtype).prop('checked'));
    var file = '';
    if(attrtype == 'COLOR'){
        file = '<label>Color Sample</label><input type="file" class="form-control" name="clrsample" accept=".png, .jpeg, .jpg">';
    }
    var name = attrtype.toLowerCase();
    if(attrtype == 'GENDER'){
        var content = '<div class="col-md-12 mt-2 form-group div_' + attrtype + '"><div class="row"><div class="col-md-6"><label>Suitable For</label><select class="form-control" name="' + name + '"><option value="">-- Select Gender --</option><option value="Men">Men</option><option value="Women">Women</option><option value="Kids">Kids</option><option value="Both">Both (Men, Women)</option></select></div><div class="col-md-6">' + file + '</div></div></div>';
    }else{
      var content = '<div class="col-md-12 mt-2 form-group div_' + attrtype + '"><div class="row"><div class="col-md-6"><label>' + attrtype + '</label><input type="hidden" name="attr[]" value="' + attrtype + '"><input type="text" class="form-control" name="' + name + '"></div><div class="col-md-6">' + file + '</div></div></div>';
    }
      if($('#' + attrtype).prop('checked') == true ){
          $('.attr').append(content);
      }else{
          $('.attr .div_' + attrtype).remove();
      }
      attrBlockData();
  }

  function attrBlockData(){
      var attrhtml = $('.attr').html();
    //   alert(attrhtml);
    if(attrhtml != ''){
        $('.no-attr').hide();
    }else{  
        $('.no-attr').show();
    }
  }

  

  function skipAttribute(product, attribute){
    //   alert($('').prop('checked'));
    var checkbox = attribute.toLowerCase();
    if($('#' + checkbox).prop('checked') == true){
    if(product != "" && attribute != ""){
        $.ajax({
            type: 'post',
            url: 'helpers/event.php',
            data: {exattribute: attribute, vertualproduct: product},
            success: function(data){
                // alert(data);
                if(attribute == 'COLOR'){
                    var multidata = data.split("#");
                    var sample = '<input type="hidden" name="varclrsample" value="' + multidata[1] + '"><input type="hidden" name="vsi" value="1">';
                    var dataval = multidata[0]; 
                    $('.colorsample').html(" ");
                }else{
                    var dataval = data;
                    var sample = "";
                }
               var content = '<input type="hidden" name="varattrval[]" value="' + dataval + '">' + sample;
               $('.' + attribute).html(" ");
               $('.' + attribute).html(content);
            }
        })
    }
}else{
    var content = '<label>' + attribute + '</label><input type="text" class="form-control" name="varattrval[]" value=""><input type="hidden" name="vsi" value="0">';
    $('.' + attribute).html(" ");
    $('.' + attribute).html(content);
    if(attribute == 'COLOR'){
        $('.colorsample').html('<label>Color Sample</label><input type="file" class="form-control" name="varclrsample" accept=".png, .jpeg, .jpg">');
    }
}
  }


  function skipImages(x){
    if($('#image-skip').prop('checked') == true){
      $('#' + x).val(1);
      $('.img-group-div').hide();
    }else{
        $('#' + x).val(0);
        $('.img-group-div').show();
    }
  }

  function hideDiv(div, atr){
    if($('#' + atr).prop('checked') == true ){
        $('.' + div).hide();
    }else{
        $('.' + div).show();
    }
  }

  $(document).ready(function() {
    
    var totalpage = $('#totalpage').val();
    var currentpage = $('#currentpage').val();
    var filterval = $('#filterval').val();
    if(filterval != ""){
        filter = filterval + "&&";
    }else{
        filter = "";
    }
    $('#pagination-here').bootpag({
        total: totalpage,          
        page: currentpage,            
        maxVisible: 5,     
        leaps: true,
        href: "?" + filter + "page={{number}}",
    })
    
    });
    //page click action
    $('#pagination-here').on("page", function(event, num){
        //show / hide content or pull via ajax etc
        $("#content").html("Page " + num); 
    });
    
    function changeMoqVal(x, y){
        var moqval = $('#' + x).val();
        if(moqval == '10+'){
            $('#' + y).html("");
            $('#' + y).html('<input type="number" name="moq" class="form-control" value="" required>');
        }else{
            $('#' + y).html("");
        }
    }

    // brand-fields

    function brandFields(x){
        var txt = $('#' + x + ' :selected').text().toUpperCase();
        window.location.href = "product.php?brand=" + txt;
    }

    // attribute builder starts
    // $('.getattributes select').on('change',  function(){
    //     // alert('hoo');
    //     var brand = $('#brand :selected').val();
    //     var category = $('#category :selected').val();
    //     var subcategory = $(' #subcategory :selected').val();
    //     console.log("brand=>", brand);
    //     console.log("category=>", category);
    //     console.log("subcategory=>", subcategory);
    //     if(brand != "" && category != "" && subcategory != "" && subcategory != 'undefined' && subcategory != 'null'){
    //         // alert("hii");  
    //         $.ajax({
    //             type: 'post',
    //             url: 'helpers/event.php',
    //             data: {attribute: 1, brand: brand, category: category, subcategory: subcategory},
    //         });   
    //     }
    // });

    $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});