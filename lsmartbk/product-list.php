﻿<?php 
include_once "helpers/index.php";
$edit = "";
if(isset($_GET['page'])){
    $page = $_GET['page'];
}else{
    $page = 1;
}

$limit = 10;
$skip  = ($page - 1) * $limit;

$query = "";
if(isset($_GET['filter'])){
    $edit = 1;
    extract($_GET);
    
    if($sku != ""){
        $query = $query." AND t1.sku='$sku'";
    }
    if($brand != ""){
        $query = $query." AND t1.brand='$brand'";
    }
    if($category!= ""){
        $query = $query." AND t1.category='$category'";
    }
    if($subcategory!= ""){
        $query = $query." AND t1.subcategory='$subcategory'";
    }
    if(!empty($minprice)){
        $query = $query." AND t1.price >= $minprice";
    }
    if(!empty($maxprice)){
        $query = $query." AND t1.price <= $maxprice";
    }
    if(!empty($status)){
        $query = $query." AND t1.status='$status'";
    }
    if(!empty($upc)){
        $query = $query." AND t2.vpn='$upc'";
    }
    if(!empty($batchno)){
        $query = $query." AND t2.batch_no='$batchno'";
    }
    if(!empty($query)){
    $result_db = mysqli_query($conn,"SELECT COUNT(t1.p_id) FROM product as t1 join berluti as t2 on t1.sku=t2.p_id WHERE t1.publish IN ('0', '1') $query"); 
    $row_db = mysqli_fetch_row($result_db);  
    $total_records = $row_db[0];
    $sql = mysqli_query($conn, "SELECT t1.p_id, t1.product_name, t1.sku, t1.category, t1.subcategory, t1.mrp, t1.price, t1.status, t1.publish, t1.brand FROM product as t1 join berluti as t2 on t1.sku=t2.p_id WHERE t1.publish IN ('0', '1') $query ORDER BY t1.p_id DESC LIMIT $skip, $limit");
    if($sql){
        $filterresult = [];
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $filterresult[] = $rows;
            }
        }
    }
}
}

?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Products Management</a></li>
                    <li class="breadcrumb-item active">Product List</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- filter starts -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Filters</div>
                    <div class="body">
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
                        <div class="row">
                            <input type="hidden" name="filter" value="1">
                            <div class="col-md-4">
                                <label>Product SKU</label>
                                <input type="text" class="form-control" name="sku" <?php echo (!empty($edit)) ? 'value="'.$sku.'"' : 'placeholder="Product SKU"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Brand</label>
                                <select class="form-control z-index" name="brand" data-live-search="true">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $brand : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Category</label>
                                <select class="form-control" name="category" id="category" data-live-search="true">
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $category : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                            <div id="subcategory">
                                <label>Subcategory</label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" data-live-search="true">
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    if(!empty($edit)){
                                        $value = (!empty($edit)) ? $subcategory : '';
                                        display_option_selected('subcategory', 1, 3, $value);
                                    } 
                                    ?>
                                </select>
                            </div>
                            </div>

                            <div class="col-md-4">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="">-- Select Status Tag --</option>
                                    <?php $statusvalue = (!empty($edit)) ? $status : ''; ?>
                                    <?php display_option_selected('color', 0, 1, $statusvalue); ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Price</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="minprice" <?php echo (!empty($edit)) ? 'value="'.$minprice.'"' : 'placeholder="Min Price"' ; ?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="maxprice" <?php echo (!empty($edit)) ? 'value="'.$maxprice.'"' : 'placeholder="Max Price"' ; ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label>UPC or VPC code</label>
                                <input type="text" class="form-control" name="upc" <?php echo (!empty($edit)) ? 'value="'.$upc.'"' : 'placeholder="UPC or VPC code"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Batch No</label>
                                <input type="text" class="form-control" name="batchno" <?php echo (!empty($edit)) ? 'value="'.$batchno.'"' : 'placeholder="Batch No"' ; ?>>
                            </div>

                            <div class="col-md-12 mt-3 text-right">
                                <input type="submit" class="btn btn-success" name="apply_filter" value="Apply Filter">
                                <a href="product-list" class="btn btn-danger">Reset</a>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- filter ends -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                <?php if(!empty($edit)){ ?>
                <div class="card-header text-success">Filter Applied</div>
                <?php } ?>
                    <div class="body">
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="float-right mb-3">
                        <button type="submit" name="exportexcel" class="btn btn-primary">Export Excel</button>
                    </form>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Vpn</th>
                                        <th>Product Name</th>
                                        <th>Brand</th>
                                        <th>Category</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(empty($edit)){
                                        $result_db = mysqli_query($conn,"SELECT COUNT(p_id) FROM product"); 
                                        $row_db = mysqli_fetch_row($result_db);  
                                        $total_records = $row_db[0];
                                        $sql2 = mysqli_query($conn, "SELECT t1.p_id, t1.product_name, t1.sku, t2.vpn, t1.brand, t1.category, t1.subcategory, t1.mrp, t1.price, t1.status, t1.publish FROM product as t1 join berluti as t2 on t1.sku=t2.p_id ORDER BY t1.p_id DESC LIMIT $skip, $limit");
                                        if($sql2){
                                        if(mysqli_num_rows($sql2) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($sql2)){ ?>
                                        <tr>
                                            <td>
                                            <?php 
                                                $f_image = 'no-product.png';
                                                $primaryimage = getSinglevalue('product_images', 'product', $rows['sku'], 2);
                                                if(file_exists(PRODUCT_DIRECTORY.$primaryimage)){
                                                    $f_image = $primaryimage;
                                                }
                                            ?>
                                            <img src="<?php echo PRODUCT_DIRECTORY.$f_image ?>" width="50px" height="50px">
                                            </td>
                                            <td><?php echo $rows['vpn'] ?></td>
                                            <td><?php echo $rows['product_name'] ?></td>
                                            <td><?php echo $brand_name = getSinglevalue('brands', 'brand_key', $rows['brand'], 3) ?></td>
                                            <td><?php echo getSinglevalue('category', 'cat_key', $rows['category'], 2).' >> '.getSinglevalue('subcategory', 'subcat_key', $rows['subcategory'], 3) ?></td>
                                            <td><strike class="text-danger"><?php echo $rows['mrp'] ?></strike> <?php echo $rows['price'] ?></td>
                                            
                                            <td><?php status($rows['publish']) ?></td>
                                            <td>
                                            <a href="product?brand=<?php echo strtoupper($brand_name) ?>&product=<?php echo $rows['p_id'] ?>" class="btn btn-primary" style="padding: 3px 7px;" data-toggle="tooltip" title="Edit"><i class="material-icons">mode_edit</i></a>
                                            <a href="add-stock?product=<?php echo $rows['p_id'] ?>" class="btn btn-success" style="padding: 3px 7px;"  data-toggle="tooltip" title="Add Stock"><i class="material-icons">picture_in_picture_alt</i></a>
                                            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                                                <input type="hidden" name="sku" value="<?php echo $rows['sku'] ?>">
                                                <input type="hidden" name="status" value="<?php echo (!empty($rows['publish'])) ? 0 : 1 ; ?>">
                                                <button type="submit" class="btn btn-info" name="change_status" style="padding: 3px 7px;" data-toggle="tooltip" title="Change Status"><i class="material-icons">remove_red_eye</i></button>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php   }
                                        }
                                    }else{
                                        echo mysqli_error($conn);
                                    }
                                }else{ 
                                    if(!empty($filterresult)){
                                   for($i = 0; $i <= count($filterresult) - 1; $i++){
                                    ?>
                                <tr>
                                            <td><img src="<?php echo PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $filterresult[$i][2], 2) ?>" width="50px" height="50px"></td>
                                            <td><?php echo $filterresult[$i][2] ?></td>
                                            <td><?php echo $filterresult[$i][1] ?></td>
                                            <td><?php echo $brand_name = getSinglevalue('brands', 'brand_key', $filterresult[$i][9], 3) ?></td>
                                            <td><?php echo getSinglevalue('category', 'cat_key', $filterresult[$i][3], 2).' >> '.getSinglevalue('subcategory', 'subcat_key', $filterresult[$i][4], 3) ?></td>
                                            <td><strike class="text-danger"><?php echo $filterresult[$i][5] ?></strike> <?php echo $filterresult[$i][6] ?></td>
                                            
                                            <td><?php status($filterresult[$i][8]) ?></td>
                                            <td>

                                            <a href="product?brand=<?php echo strtoupper($brand_name) ?>&product=<?php echo $filterresult[$i][0] ?>" class="btn btn-primary" style="padding: 3px 7px;" data-toggle="tooltip" title="Edit"><i class="material-icons">mode_edit</i></a>
                                            <a href="add-stock?product=<?php echo $filterresult[$i][0] ?>" class="btn btn-success" style="padding: 3px 7px;"  data-toggle="tooltip" title="Add Stock"><i class="material-icons">picture_in_picture_alt</i></a>
                                            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                                                <input type="hidden" name="sku" value="<?php echo $filterresult[$i][2] ?>">
                                                <input type="hidden" name="status" value="<?php echo (!empty($filterresult[$i][8])) ? 0 : 1 ; ?>">
                                                <button type="submit" class="btn btn-info" name="change_status" style="padding: 3px 7px;" data-toggle="tooltip" title="Change Status"><i class="material-icons">remove_red_eye</i></button>
                                            </form>
                                            </td>
                                        </tr>
                                <?php }
                                }else{ ?>
                                    <tr>
                                        <td colspan="7" align="center"><p class="m-0"><strong>No Result Found.</strong></p></td>
                                    </tr>
                                <?php } 
                            }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- pagination here -->
                        <div class="row">
                            <div class="col-md-6">
                            <?php 
                                if(!empty($edit)){
                                    // echo $_SERVER['QUERY_STRING'];
                                    ?>
                                
                            <input type="hidden" id="filterval" value="<?php echo $_SERVER['QUERY_STRING'] ?>">   
                            <?php }else{ ?>
                                <input type="hidden" id="filterval" value="">
                            <?php } ?>
                            <input type="hidden" id="currentpage" value="<?php echo $page ?>">
                            <p><strong>Total Pages: </strong><input type="text" id="totalpage" value="<?php echo (!empty($edit)) ? ceil($total_records / 10) : ceil($total_records / 10); ?>" style="border: 10px"></p>
                            </div>
                            <div class="col-md-6">
                                <?php //if(empty($edit)){ ?>
                                    <p id="pagination-here"></p>
                                <?php //} ?>
                            </div>
                        </div>
                        <!-- pagination ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php 
include_once "helpers/footer.php";

flash_session_admin();

if(isset($_POST['change_status'])){
    extract($_POST);
    if($sku!= "" && $status != ""){
        $sql = mysqli_query($conn, "UPDATE product SET publish='$status' WHERE sku='$sku'");
        if($sql){
            $_SESSION['result'] = [true, 'Status Changed Successfully', 'succes'];
        }else{
            $_SESSION['result'] = [false, 'Status Not Changed ', 'danger'];
        }
        header('refresh: 0');
    }
}

if(isset($_POST['exportexcel'])){
    extract($_POST);
    $dataarr = [];
if(empty($edit)){
    $query5 = mysqli_query($conn, "SELECT t1.product_name, t2.vpn, (select brand_name from brands where brand_key=t1.brand) as brand, (select category FROM category where cat_key=t1.category) as category, (select subcategory from subcategory where subcat_key=t1.subcategory) as subcategory, t1.price, t1.publish FROM product as t1 join berluti as t2 on t1.sku=t2.p_id ORDER BY t1.p_id DESC");
    if($query5){
        if(mysqli_num_rows($query5) > 0){
            while($row = mysqli_fetch_array($query5)){
                $dataarr[] = $row;
            }
        }
    }
}else{
    echo $query;
    $sql = mysqli_query($conn, "SELECT t1.product_name, t2.vpn, (select brand_name from brands where brand_key=t1.brand) as brand, (select category FROM category where cat_key=t1.category) as category, (select subcategory from subcategory where subcat_key=t1.subcategory) as subcategory, t1.price, t1.publish FROM product as t1 join berluti as t2 on t1.sku=t2.p_id WHERE t1.publish IN ('0', '1') $query ORDER BY t1.p_id DESC LIMIT $skip, $limit");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            while($row = mysqli_fetch_array($sql)){
                $dataarr[] = $row;
            }
        }
    }else{
        echo mysqli_error($conn);
    }
}
    $clmarr = ['S_No', 'Product', 'Vpn', 'Brand', 'Category', 'Subcategory', 'Price', 'Publish'];
    $_SESSION['column'] = $clmarr;
    $_SESSION['dataarr'] = $dataarr;
    header('location: helpers/excelmaker.php');
}

?>