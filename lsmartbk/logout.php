<?php
include_once "helpers/config.php";
include_once "helpers/index.php";
$logout = logout($id, $token);
if($logout === 1){
    session_destroy();
    header('location: index');
}
?>