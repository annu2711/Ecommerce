<?php 
include_once"helpers/index.php"; 

$edit = "";
    if(isset($_GET['categorys'])){
        extract($_GET);
        if(!empty($categorys)){
        $sql = mysqli_query($conn, "SELECT * FROM category WHERE cat_key='$categorys'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Category
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Category</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Category Name</label>
                                <input type="text" class="form-control" name="category" <?php echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Category"'; ?> required>                              
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[3] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[3] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_category">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_category">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Category</strong> List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Category Name</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM category");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['category'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="category?categorys=<?php echo $rows['cat_key'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once "helpers/footer.php"; 
flash_session_admin();
if(isset($_POST['add_category'])){
    extract($_POST);
    if(!empty($category)){
        $chck_brand = check_duplicate('category', 'category', $category);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Category already exists', 'top', 'right', '', '')</script>";
        }else{
            $key = rand_char(5);
            $query = mysqli_query($conn, "INSERT INTO category (cat_key, category, status, created_at) VALUES ('$key', '$category', 1, '$created_at')");
            if($query){
                $_SESSION['result'] = [true, 'Category Added Successfully', 'success'];
            }else{
                $_SESSION['result'] = [false, 'Category Not Added Successfully', 'error'];
            }
            header('location: category');
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

if(isset($_POST['update_category'])){
    extract($_POST);
    if(!empty($category)){
            $query = mysqli_query($conn, "UPDATE category SET category='$category', status='$status' WHERE cat_key='$categorys'");
            if($query){
                $_SESSION['result'] = [true, 'Category Updated Successfully', 'success'];
            }else{
                $_SESSION['result'] = [false, 'Category Not Updated Successfully', 'error'];
            }
            header('location: category');
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>