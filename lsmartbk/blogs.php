<?php 
    include_once"helpers/index.php";
?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><a href="manage-blog">Add Blog</a></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Blog</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">

         <!-- Table Starts -->

         <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Blog</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Author</th>
                                        <th>Title</th>
                                        <th>Text</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM blogs");
                                    if($query){
                                        $num = 1;
                                        if(mysqli_num_rows($query) > 0){
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['blog_author'] ?></td>
                                        <td><?php echo $rows['blog_title'] ?></td>
                                        <td><?php echo $rows['blog_text'] ?></td>
                                        <td><img src="<?php echo WEBSITE_DIRECTORY.$rows['blog_image'] ?>" width="50px" height="50px"></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="manage-blog?blog=<?php echo $rows['blog_id'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <?php   }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>

    </section>



<?php 
include_once"helpers/footer.php";
?>