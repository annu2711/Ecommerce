<?php 
    include_once "helpers/index.php";
    $edit = "";
    $color = "";
    $size = "";
    $sample = "";
    $gender = "";
    $fragrance_depth = "";
    $scent_type = "";
    $unit = "";
    $mnf_date = "";
    $expire_date = "";
    $fit = "";
    $material_code = "";
    $style_no = "";
    $color_code = "";
    $color = "";
    $stock = 0;
    $varstock = 0;
    // $mrp = "";
    if(isset($_GET['product'])){
        extract($_GET);
        $query = mysqli_query($conn, "SELECT t1.*, t2.*, t3.* FROM product as t1 join berluti as t2 on t1.sku=t2.p_id join product_attributes as t3 on t1.sku=t3.p_id WHERE t1.p_id='$product'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                $array = mysqli_fetch_array($query);
                $edit = 1;
                $code = $array[5];
            }else{
                // header('location: product-list');
            }
        }else{
            echo mysqli_error($conn);
        }
    }

    // print_r($array);
    $brand_name = "";
    if(isset($_GET['brand'])){
        // $store_name = $_GET['store'];
        $brand_name = $_GET['brand'];
    }
?>
<style>
.imagePreview {
    width: 100%;
    height: 180px;
    background-position: center center;
  background:url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
  background-color:#fff;
    background-size: cover;
  background-repeat:no-repeat;
    display: inline-block;
  box-shadow:0px -3px 6px 2px rgba(0,0,0,0.2);
}
.btn-primary
{
  display:block;
  border-radius:0px;
  box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
  margin-top:-5px;
}
.imgUp
{
  margin-bottom:15px;
}
.del
{
  position:absolute;
  top:0px;
  right:15px;
  width:30px;
  height:30px;
  text-align:center;
  line-height:30px;
  background-color:rgba(255,255,255,0.6);
  cursor:pointer;
}
.imgAdd, .imgAddd
{
  width:30px;
  height:30px;
  border-radius:50%;
  background-color:#4bd7ef;
  color:#fff;
  box-shadow:0px 0px 2px 1px rgba(0,0,0,0.2);
  text-align:center;
  line-height:30px;
  margin-top:0px;
  cursor:pointer;
  font-size:15px;
}
    </style>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2> <?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active"><?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
            <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <?php 
                                if(!empty($edit)){ ?>
                                    <input type="hidden" class="form-control" name="type" value="1">
                                    <input type="hidden" class="form-control" name="groupid" value="<?php echo $array[18] ?>">
                            <?php } ?>
                            <div class="form-group col-md-6">
                                <label>Store<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="store" id="store" required>
                                    <option value="">-- Select Store --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('store', 1, 2, $value) ?>
                                </select>
                            </div>


                            <div class="form-group col-md-6 getattributes">
                                <!-- <div id="brand"> -->
                                <label>Brand<sup class="text-danger">*</sup></label>
                                <select class="form-control brand" name="brand" onchange="brandFields('brand')" id="brand" style="padding: 0px !important;">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[3] : '';
                                    $sql = mysqli_query($conn, "SELECT DISTINCT(a.brand_name) as brand_name, (SELECT brand_key FROM brands WHERE brand_id=a.brand_id) as brand_key FROM brands as a WHERE status=1 ORDER BY brand_name ASC");
                                    if($sql){
                                        if(mysqli_num_rows($sql) > 0){
                                            while($row = mysqli_fetch_assoc($sql)){ 
                                            if($row['brand_name'] == $brand_name){
                                               $brand_new_key = $row['brand_key']; 
                                            }
                                            
                                            ?>
                                                <option value="<?php echo $row['brand_key'] ?>" <?php if(!empty($brand_name)) echo (strtoupper($row['brand_name']) == $brand_name) ? 'selected="selected"' : '' ; ?>><?php echo strtoupper($row['brand_name']) ?></option>
                                    <?php    }
                                        }
                                    }
                                    ?>
                                </select>
                                <!-- </div> -->
                            </div>

                            <?php
                                if(!empty($brand_name)){
                                    switch($brand_name){ 
                                        case "BERLUTI":
                                            include_once "helpers/berluti.php";
                                            break;
                                        case "TODS":
                                            include_once "helpers/berluti.php";
                                            break;
                                        case "SAINT LAURANT":
                                            include_once "helpers/berluti.php";
                                            break;
                                        case "YSL":
                                            include_once "helpers/berluti.php";
                                            break;
                                        case "CREED":
                                            include_once "helpers/creed.php";
                                            break;
                                    }

                                }

                            ?>

                            <!-- form end point -->

                        </div>
                    </div>
                </div>
                
                <!-- variation starts -->

                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Product</strong> Variations</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12 col-lg-12">
                                <div class="panel-group full-body" id="accordion_5" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingOne_5">
                                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion_5" href="#collapseOne_5" aria-expanded="true" aria-controls="collapseOne_5">Add Product Variations </a> </h4>
                                        </div>
                                        <div id="collapseOne_5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_5">
                                        <div class="panel-body" style="background-color: transparent; color: #555;"> 
                                        <?php if(!empty($edit)){ ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Retail Price</label>
                                                <input type="text" class="form-control" name="varprice" value="">
                                                <p class="text-danger">Vertual Price will be added if it will be empty.</p>
                                            </div>
                                        <div class="col-md-12">
                                            <?php 
                                            // if($brand_name == 'berluti' || $brand_name == 'tods' || $brand_name == 'saint laurent' || $brand_name == 'brunello cucinelli' || $brand_name == 'creed' || $brand_name == 'molten brown' || $brand_name == 'tom ford' || $brand_name == 'gucci'){ 
                                                ?>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>Collection Tag<sup class="text-danger">*</sup></label>
                                                    <select class="form-control" name="varcollection_tag" required>
                                                        <option value="">-- Select Collection Tag --</option>
                                                        <option value="PE">Permanent</option>
                                                        <option value="SE">Seasonal</option>
                                                        <option value="SP">Special</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Line of Business</label>
                                                    <input type="text" class="form-control" name="varline_of_business" value="">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>UPC or VPN Code<sup class="text-danger">*</sup></label>
                                                    <input type="text" class="form-control" name="varvpn" value="" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Style Name<sup class="text-danger">*</sup></label>
                                                    <input type="text" class="form-control" name="varstyle_code" value="" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Style No</label>
                                                    <input type="text" class="form-control" name="varstyle_no" value="">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Serial No<sup class="text-danger">*</sup></label>
                                                    <input type="text" class="form-control" name="varserial_no" value="" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Color Code</label>
                                                    <input type="text" class="form-control" name="varcolor_code" value="">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Material Code / Name</label>
                                                    <input type="text" class="form-control" name="varmaterial" value="">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Fit</label>
                                                    <select class="form-control" name="varfit">
                                                        <option value="">-- Select Fit --</option>
                                                        <?php $value = "";
                                                        display_option_selected('fit', 1, 1, $value);
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label>Batch No</label>
                                                    <input type="text" class="form-control" name="varbatch_no" value="">
                                                </div>
                                            </div>
                                            <?php // } ?>
                                        </div>

                                                    <div class="col-md-12">
                                                        <div class="row mt-2">
                                                        <?php 
                                                // Attribute column name
                                                $query = mysqli_query($conn, "SELECT `COLUMN_NAME` 
                                                FROM `INFORMATION_SCHEMA`.`COLUMNS` 
                                                WHERE `TABLE_SCHEMA`='moonraker' 
                                                    AND `TABLE_NAME`='product_attributes';");
                                                if($query){
                                                    if(mysqli_num_rows($query) > 0){
                                                        $clmarr = [];
                                                        while($rs = mysqli_fetch_array($query)){
                                                            $clmarr[] = $rs;
                                                        }
                                                        // print_r($clmarr);
                                                    }
                                                }else{
                                                    echo mysqli_error($conn);
                                                }
                                                        // attribute column name dnds
                                                $sql = mysqli_query($conn, "SELECT * FROM product_attributes WHERE p_id='$array[5]'");
                                                if($sql){
                                                    if(mysqli_num_rows($sql) > 0){
                                                    $attrrows = mysqli_fetch_array($sql);
                                                             ?>
                                                           

                                                <?php if(!empty($attrrows[4])){ ?>
                                                    <div class="col-md-7">
                                                        <label class="color">Color</label>
                                                        <input type="text" name="varcolor" class="form-control color" value="">
                                                        <label onclick="hideDiv('color', 'atr_1')"><input type="checkbox" class="" id="atr_1"> SKIP & USE VERTUAL COLOR</label>
                                                    </div>
                                                    <div class="col-md-4 color">
                                                        <label>Color Sample</label>
                                                        <input type="file" class="form-control" name="varclrsample" accept=".png, .jpeg, .jpg">
                                                    </div>
                                                <?php  
                                            } ?>

                                            <?php 
                                                $columnarr = [5,9];
                                                for($i = 0; $i <= count($columnarr) - 1; $i++){
                                                    // echo $attrrows[$columnarr[$i]]."<br>";
                                                     if(!empty($attrrows[$columnarr[$i]])){ ?>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-7">
                                                        <label class="<?php echo $latr = strtolower($clmarr[$columnarr[$i]]['COLUMN_NAME']) ?>"><?php echo $uatr = strtoupper($clmarr[$columnarr[$i]]['COLUMN_NAME']) ?></label>
                                                        <input type="text" name="var<?php echo $latr ?>" class="form-control <?php echo $latr ?>" value="">
                                                        <label onclick="hideDiv('<?php echo $latr ?>', 'atr_<?php echo $latr ?>')"><input type="checkbox" class="" id="atr_<?php echo $latr ?>"> SKIP & USE VERTUAL <?php echo $uatr ?></label>
                                                        </div>
                                                    <?php } 
                                                }
                                            ?>
                                                
                                                <?php if(!empty($attrrows[9])){ ?>
                                                    <div class="col-md-7">
                                                    <label class="gender">Gender</label>
                                                    <input type="text" name="vargender" class="form-control gender" value="<?php echo $attrrows[9] ?>">
                                                    <label onclick="hideDiv('gender', 'atr_3')"><input type="checkbox" class="" id="atr_3"> SKIP & USE VERTUAL GENDER</label>
                                                    </div>
                                                <?php } } } ?>



                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 img-group-div">
                                                        <div class="row mt-4">
                                                            <div class="col-sm-2 imgUp">
                                                                <div class="imagePreview"></div>
                                                                <label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" name="variableimg[]" style="width: 0px;height: 0px;overflow: hidden;"></label>
                                                            </div><!-- col-2 -->
                                                            <span class="imgAddd" onclick="addImage('variableimg')">+</span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label><input type="checkbox" id="image-skip" onclick="skipImages('imageuse')"> SKIP AND USER VERTUAL IMAGE</label>
                                                        <input type="hidden" name="imageuse" id="imageuse" value="0">
                                                        <?php // print_r($vimagearr); ?>
                                                    </div>

                                                    <div class="col-md-12 text-center">
                                                        <input type="submit" name="addvariation" class="btn btn-success" value="Add Variation">
                                                    </div>
                                                </div>
                                               <?php }else{ ?>
                                                <p class="text-danger"><strong>Note: </strong>Please Save Product First</p>
                                               <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

                <!-- variation ends -->

            </form>
                        


<?php 
include_once "helpers/footer.php";

if(isset($_POST['add_product'])){
    extract($_POST);
    $brand = $brand_name;
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($category) && !empty($subcategory) && $price!=""){
        $code = date('Ymdhis');
        if(empty($edit)){
            $type= 0;
            $groupid = $code; 
         }
         $publish = 1;
         mysqli_autocommit($conn, FALSE);
         $key = rand_char(5);
         try {
            $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `price`, `publish`, `type`, `groupid`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $price, $publish, $type, $groupid, $created_at);
            if ($stmt->execute()) {
                $stmtb = $conn->prepare("INSERT INTO berluti (p_id, collection_tag, vpn, style_name, style_no, color_code, material_code, fit, hsncode, scent_type, fragrance_depth, mnf_date, expire_date, created_at) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmtb->bind_param('ssssssssssssss', $code, $collection_tag, $vpn, $style_code, $style_no, $color_code, $material, $fit, $hsncode, $scent_type, $fragrance_depth, $mnf_date, $expire_date, $created_at);
                if($stmtb->execute()){
                if(!empty($color)){
                    $sample = fileUpload($_FILES['clrsample']['name'], $_FILES['clrsample']['tmp_name'], 'clrsample', PRODUCT_DIRECTORY);
                }
                try{
                    $color  = (!empty($color)) ? strtoupper(str_replace(" ","",$color)) : '' ;
                    $size  = (!empty($size)) ? strtoupper($size) : '' ;
                    // $storage  = (!empty($storage)) ? strtoupper(str_replace(" ","",$storage)) : '' ;
                    $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, color, size, gender, unit, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                    $stmt2->bind_param('ssssssss', $code, $groupid, $sample, $color, $size, $gender, $unit, $created_at);
                    if($stmt2->execute()){
                        if(!empty($_FILES['file']['name'])){
                            $num = 0;
                            foreach($_FILES['file']['tmp_name'] as $keey => $tmp_name){
                                $num = $num + 1;
                                $imgcode = $code;
                            if (strpos($imgcode, '/') == true) {
                                echo str_replace("/","_",$imgcode);
                            }
                            $order_unique = 'product_'.$imgcode.'_'.$num;
                                $filename = fileUpload($_FILES['file']['name'][$keey], $tmp_name, $order_unique, PRODUCT_DIRECTORY);
                                $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$filename', '$created_at')");
                            }
                            mysqli_commit($conn);
                            echo "<script>showNotification('alert-success', 'Product Added Successfully', 'top', 'right', '', '')</script>";

                        }else{
                            // image not found
                            echo "<script>showNotification('alert-success', 'Product Added Successfully', 'top', 'right', '', '')</script>";
                        }
                    }else{
                        mysqli_rollback($conn);
                        echo mysqli_error($conn);
                        throw new exception($conn->error);
                    }
                }catch(Exception $e){
                    echo "Error: " . $e->getMessage();
                    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
                }
            }else{
                mysqli_rollback($conn);
                echo mysqli_error($conn);
                echo "<script>showNotification('alert-Danger', 'Product Not Added', 'top', 'right', '', '')</script>";
            }
            }else{
                echo mysqli_error($conn);
                throw new exception($conn->error);
            }
        }catch(Exception $e){
            echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
        }
    }else{
         echo "Data =>".$store."=".$brand."=".$title."=".$category."=".$subcategory."=".$price;
        echo "<script>showNotification('alert-info', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['addvariation'])){
    extract($_POST);
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($category) && !empty($subcategory) && $price!=""){
        $varcode = date('Ymdhis');
        if(empty($edit)){
            $type= 0;
            $groupid = $code; 
         }
         $publish = 1;
         mysqli_autocommit($conn, FALSE);
         $key = rand_char(5);
         if(!empty($varprice)){
            $price = $varprice;
         }
         try {
            $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `price`, `publish`, `type`, `groupid`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssssssssss", $key, $store, $brand, $title, $varcode, $category, $subcategory, $price, $publish, $type, $groupid, $created_at);
            if ($stmt->execute()) {

                // $brand_name = strtolower(getSinglevalue('brands', 'brand_key', $brand, 3));
                // if($brand_name == 'berluti' || $brand_name == 'tods' || $brand_name == 'saint laurent' || $brand_name == 'brunello cucinelli' || $brand_name == 'creed' || $brand_name == 'molten brown' || $brand_name == 'tom ford' || $brand_name == 'gucci'){
                    
                    $stmtb = $conn->prepare("INSERT INTO berluti (p_id, collection_tag, vpn, style_name, style_no, color_code, material_code, fit, hsncode, created_at) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    $stmtb->bind_param('ssssssssss', $varcode, $varcollection_tag, $varvpn, $varstyle_code, $varstyle_no, $varcolor_code, $varmaterial, $varfit, $hsncode, $created_at);
                // }

                // if($brand_name == 'apple'){
                //     $stmtb = $conn->prepare("INSERT INTO apple (p_id, npi, marketing_flag, upc_ean, barcode, item_group, subproduct_group, manufacturer, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
                //     $stmtb->bind_param('sssssssss', $code, $npi, $marketing_flag, $upc_ean, $barcode, $item_group, $subproduct_group, $manufacturer, $created_at);
                // }
                if($stmtb->execute()){
                    $sample = $prev_sample;
                    // color attribute
                    if(!empty($varcolor)){
                        $color  = $varcolor;
                        if(!empty($_FILES['varclrsample']['name'])){
                            $sample = fileUpload($_FILES['varclrsample']['name'], $_FILES['varclrsample']['tmp_name'], 'clrsample', PRODUCT_DIRECTORY);
                        }
                    }
                    // size attribute
                    if(!empty($varsize)){
                        $size = $varsize;
                    }
        
                    // size attribute
                    if(!empty($vargender)){
                        $gender = $vargender;
                    }
                try{
                    $color  = (!empty($color)) ? strtoupper(str_replace(" ","",$color)) : '' ;
                    $size  = (!empty($size)) ? strtoupper($size) : '' ;
                    // $storage  = (!empty($storage)) ? strtoupper(str_replace(" ","",$storage)) : '' ;
                    $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, color, size, gender, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                    $stmt2->bind_param('sssssss', $code, $groupid, $sample, $color, $size, $gender, $created_at);
                    if($stmt2->execute()){
                        if(empty($imageuse)){
                            $num = 0;
                            if(!empty($_FILES['variableimg']['name'])){
                                foreach($_FILES['variableimg']['tmp_name'] as $keey => $tmp_name){
                                    $num = $num + 1;
                                    $imgcode = $varcode;
                                if (strpos($imgcode, '/') == true) {
                                    echo str_replace("/","_",$imgcode);
                                }
                                $order_unique = 'product_'.$imgcode.'_'.$num;
                                    // $images = $images.$_FILES['files']['name'][$key];
                                    $filename = fileUpload($_FILES['variableimg']['name'][$keey], $tmp_name, $order_unique, PRODUCT_DIRECTORY);
                                    $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$varcode', '$filename', '$created_at')");
                                    // if($sql){
                                    //     echo "IMAGE => Done";
                                    // }else{
                                    //     echo "Image Err => ".mysqli_error($conn);
                                    // }
                                    // $images = $images.$filename.",";
                                }
                            }
                        }else{
                            for($i = 0; $i <= count($vimagearr) - 1; $i++){
                                $nimagename = $vimagearr[$i]['image'];
                                $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$varcode', '$nimagename', '$created_at')");
                                if($sql){
                                    echo "Done";
                                }else{
                                    echo mysqli_error($conn);
                                }
                            }
                        }
                        
                    mysqli_commit($conn);
                    echo "<script>showNotification('alert-success', 'Variation Added Successfully', 'top', 'right', '', '')</script>";

                    }else{
                        mysqli_rollback($conn);
                        echo mysqli_error($conn);
                        throw new exception($conn->error);
                    }
                }catch(Exception $e){
                    echo "Error: " . $e->getMessage();
                    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
                }
            }else{
                mysqli_rollback($conn);
                echo mysqli_error($conn);
                echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
            }
            }else{
                echo mysqli_error($conn);
                throw new exception($conn->error);
            }
        }catch(Exception $e){
            echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
        }
    }
}


if(isset($_POST['update_product'])){
    extract($_POST);
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($category) && !empty($subcategory) && $price!=""){
    $publish = 1;
    mysqli_autocommit($conn, FALSE);
    try{
        $stmt = $conn->prepare("UPDATE product SET store=?, brand=?, product_name=?, category=?, subcategory=?, price=?, publish=?, type=?, groupid=? WHERE p_id=?");
        $stmt->bind_param('ssssssssss', $store, $brand, $title, $category, $subcategory, $price, $publish, $type, $groupid, $product);
        if($stmt->execute()){
              
                    $stmtb = $conn->prepare("UPDATE berluti SET collection_tag=?, vpn=?, style_name=?, style_no=?, color_code=?, material_code=?, fit=?, hsncode=? WHERE p_id=?");
                    $stmtb->bind_param('sssssssss', $collection_tag, $vpn, $style_code, $style_no, $color_code, $material, $fit, $hsncode, $code);
                
            if($stmtb->execute()){
            if(!empty($color)){
                if(!empty($_FILES['clrsample']['name'])){
                    $sample = fileUpload($_FILES['clrsample']['name'], $_FILES['clrsample']['tmp_name'], 'clrsample', PRODUCT_DIRECTORY);
                }else{
                    $sample = $prev_sample;
                }
            }
            try{
                $color  = (!empty($color)) ? strtoupper(str_replace(" ","",$color)) : '' ;
                $size  = (!empty($size)) ? strtoupper($size) : '' ;
                // $storage  = (!empty($storage)) ? strtoupper(str_replace(" ","",$storage)) : '' ;
                $stmt2 = $conn->prepare("UPDATE product_attributes SET sample=?, color=?, size=?, gender=? WHERE p_id=?");
                $stmt2->bind_param('sssss', $sample, $color, $size, $gender, $code);
                if($stmt2->execute()){
                    $num = 0;
                    if(!empty($_FILES['file']['name'])){
                        foreach($_FILES['file']['tmp_name'] as $keey => $tmp_name){
                            $num = $num + 1;
                            $imgcode = $code;
                            if (strpos($imgcode, '/') == true) {
                                echo str_replace("/","_",$imgcode);
                            }
                            $order_unique = 'product_'.$imgcode.'_'.$num;
                            $filename = fileUpload($_FILES['file']['name'][$keey], $tmp_name, $order_unique, PRODUCT_DIRECTORY);
                            if(!empty($filename)){
                            $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$filename', '$created_at')");
                            }
                        }
                        mysqli_commit($conn);
                        echo "<script>showNotification('alert-success', 'Product Updated Successfully', 'top', 'right', '', '')</script>";
                        header('refresh: 0.5');

                    }else{
                        // image not found
                        mysqli_commit($conn);
                        echo "<script>showNotification('alert-success', 'Product Updated Successfully', 'top', 'right', '', '')</script>";
                        header('refresh: 0.5');
                    }
                }else{
                    mysqli_rollback($onn);
                    // echo mysqli_error($conn);
                    throw new exception($conn->error);
                }
            }catch(Exception $e){
                // echo "Error: " . $e->getMessage();
                echo "<script>showNotification('alert-danger', 'Product Not Updated ', 'top', 'right', '', '')</script>";
            }
        }else{
            mysqli_rollback($conn);
            echo mysqli_error($conn);
            echo "<script>showNotification('alert-danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
        }
        }else{
            throw new exception($conn->error);
        }
    }catch(Exception $e){
        // echo "Error: " . $e->getMessage();
        echo "<script>showNotification('alert-danger', 'Product Not Updated ', 'top', 'right', '', '')</script>";
    }
}else{
    echo "<script>showNotification('alert-info', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
}
}

?>

