<?php 
include_once"helpers/index.php"; 
    $edit = "";
    if(isset($_GET['slider'])){
        extract($_GET);
        if(!empty($slider)){
        $sql = mysqli_query($conn, "SELECT * FROM shop_by_cat WHERE sb_id='$slider'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Shop By
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Shop By Category Block</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group form-float">
                                <label>Block</label>
                                <select class="form-control" name="block">
                                    <option value="1" <?php if(!empty($edit)) { echo ($array[5] == 1) ? 'selected="selected"' : '' ; } ?>>Buyer's Pick</option>
                                    <option value="2" <?php if(!empty($edit)) { echo ($array[5] == 2) ? 'selected="selected"' : '' ; } ?>>Bestsellers</option>
                                </select>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Title"'; ?>  required>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Page Link</label>
                                <input type="text" class="form-control" name="link" <?php echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Page Link"'; ?>>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Image<sup>*</sup></label>
                                <input type="file" name="file" id="slide_file" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image('slide_file', 'image_preview');" <?php echo (!empty($edit)) ? '' : 'required'; ?>>
                                <div class="row">
                                    <div class="col-md-6">
                                    <p><b>New Uploaded Image</b></p>
                                        <div id="image_preview"></div>
                                    </div>
                                    <?php if(!empty($edit)){ ?>
                                    <div class="col-md-6">
                                        <p><b>Previous Image</b></p>
                                        <img src="website_images/<?php echo $array[3] ?>" width="500px">
                                    </div>
                                    <?php } ?>
                                </div>
                                
                                <p class="text-danger"><strong>Note:</strong> Please use 350px x 432px image for better quality.</p>
                            </div>
                          
                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[4] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[4] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_slider">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_slider">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Home Page Slider</strong> List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Title</th>
                                        <th>Link</th>
                                        <th>Type</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM shop_by_cat WHERE type!=0");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['title'] ?></td>
                                        <td><?php echo $rows['link'] ?></td>
                                        <td><?php echo ($rows['type'] == 1) ? "Buyer's Pick" : "Bestsellers"; ?></td>
                                        <td><img src="<?php echo WEBSITE_DIRECTORY.$rows['img'] ?>" width="50px" height="50px"></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="buyerpick?slider=<?php echo $rows['sb_id'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once "helpers/footer.php"; 

if(isset($_POST['add_slider'])){
    extract($_POST);
    if(!empty($title) && !empty($_FILES['file']['name'])){

        // upload image in folder
        $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], 'slider', WEBSITE_DIRECTORY);
            $status = 1;
            try {
                $stmt = $conn->prepare("INSERT INTO shop_by_cat (title, link, img, status, type, created_at) VALUES (?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("ssssss", $title, $link, $image1, $status, $block, $created_at);
                if ($stmt->execute()) {
                echo "<script>showNotification('alert-info', 'Block Added Successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');
                }else{
                    throw new exception($conn->error);
                }
                $stmt->close();
            }
            catch(Exception $e){
                    echo "<script>showNotification('alert-danger', 'Block Not Added ', 'top', 'right', '', '')</script>";
            }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}




if(isset($_POST['update_slider'])){
    extract($_POST);
    if(!empty($title)){
        $image1 = "";
        if(!empty($_FILES['file']['name'])){
            $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], 'slider', WEBSITE_DIRECTORY);
        }else{
            $image1 = $array[3];
        }
        try {
            $stmt = $conn->prepare("UPDATE shop_by_cat SET title=?, link=?, img=?, status=?, type=? WHERE sb_id=?");
            $stmt->bind_param("sssssi", $title, $link, $image1, $status, $block, $slider);
            if ($stmt->execute()) {
            echo "<script>showNotification('alert-info', 'Block Updated Successfully', 'top', 'right', '', '')</script>";
            header('location: buyerpick');
            }else{
                throw new exception($conn->error);
            }
            $stmt->close();
        }
        catch(Exception $e){
                echo "<script>showNotification('alert-danger', 'Block Not Updated ', 'top', 'right', '', '')</script>";
        }


    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>