<?php 
include_once "helpers/index.php"; 

$edit = "";
    if(isset($_GET['aaid'])){
        extract($_GET);
        if(!empty($aaid)){
        $sql = mysqli_query($conn, "SELECT * FROM assigned_attributes WHERE aaid='$aaid'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Assign Attributes</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Attribute Builder</a></li>
                    <li class="breadcrumb-item active">Assign Attribute</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="row">
                            <div class="form-group col-md-4 brand getattributes">
                                <label>Brand <sup class="text-danger">*</sup></label>
                                <select class="form-control" name="brand" style="padding: 0px !important;" required>
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[1] : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 category getattributes">
                                <label>Category <sup class="text-danger">*</sup></label>
                                <select class="form-control" name="category" id="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 subcategory getattributes">
                                <div id="subcategory">
                                <label>Subcategory <sup class="text-danger">*</sup></label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" required>
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    if(!empty($edit))
                                    $value = (!empty($edit)) ? $array[3] : '';
                                    display_option_selected('subcategory', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Attribute Name <sup class="text-danger">*</sup></label>
                                <select name="attribute" class="form-control" required>
                                    <option value="">-- Select Attribute --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[4] : '';
                                    display_option_selected('attribute_tbl', 1, 1, $value) ?>
                                </select>                             
                            </div>

                            <div class="form-group col-md-4">
                                <label>Attribute Field Type <sup class="text-danger">*</sup></label>
                                <select class="form-control" name="attribute_type" required>
                                    <option value="">-- Select Field Type</option>
                                    <?php $value = (!empty($edit)) ? $array[5] : '' ; ?>
                                    <option value="text" <?php if($value == 'text'){ echo 'selected="selected"'; } ?>>Text</option>
                                    <option value="dd" <?php if($value == 'dd'){ echo 'selected="selected"'; } ?>>Dropdown</option>
                                </select>
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float col-md-4">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[6] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[6] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_assign_attribute">UPDATE</button>

                            <?php }else{ ?>
                            <div class="form-group col-md-12">
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="assign_attribute">SUBMIT</button>
                            <?php } ?>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Assigned </strong>Attribute List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Brand</th>
                                        <th>Category</th>
                                        <th>Subcategory</th>
                                        <th>Attributes</th>
                                        <th>Attribute Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT t1.aaid, t1.attribute, t1.attribute_type, t1.status, t2.brand_name, t3.category, t4.subcategory FROM assigned_attributes as t1 join brands as t2 on t1.brand=t2.brand_key join category as t3 on t1.category=t3.cat_key join subcategory as t4 on t1.subcategory=t4.subcat_key ORDER BY t2.brand_name ASC");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['brand_name'] ?></td>
                                        <td><?php echo $rows['category'] ?></td>
                                        <td><?php echo $rows['subcategory'] ?></td>
                                        <td><?php echo $rows['attribute'] ?></td>
                                        <td><?php echo $rows['attribute_type'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="assign-attribute?aaid=<?php echo $rows['aaid'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }else{
                                        echo mysqli_error($conn);
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once "helpers/footer.php"; 
flash_session_admin();
if(isset($_POST['assign_attribute'])){
    extract($_POST);
    if(!empty($brand) && !empty($category) && !empty($subcategory) && !empty($attribute) && !empty($attribute_type)){
            $query = mysqli_query($conn, "INSERT INTO assigned_attributes (brand, category, subcategory, attribute, attribute_type, created_at) VALUES ('$brand', '$category', '$subcategory', '$attribute', '$attribute_type', '$created_at')");
            if($query){
                header('location: assign-attribute');
                $_SESSION['result'] = [true, 'Attribute Assigned Successfully', 'success'];
            }else{
                header('location: assign-attribute');
                $_SESSION['result'] = [false, 'Attribute Not Assigned Successfully', 'success'];
                // echo mysqli_error($conn);
            }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

if(isset($_POST['update_assign_attribute'])){
    extract($_POST);
    if(!empty($brand) && !empty($category) && !empty($subcategory) && !empty($attribute) && !empty($attribute_type)){
            $query = mysqli_query($conn, "UPDATE assigned_attributes SET brand='$brand', category='$category', subcategory='$subcategory', attribute='$attribute', attribute_type='$attribute_type', status='$status' WHERE aaid='$aaid'");
            if($query){
                header('location: assign-attribute');
                $_SESSION['result'] = [true, 'Assigned Attribute Updated Successfully', 'success'];
            }else{
                header('location: add-attribute');
                $_SESSION['result'] = [false, 'Assigned Attribute Not Updated Successfully', 'success'];
            }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>