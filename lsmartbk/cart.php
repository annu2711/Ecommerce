<?php 
    include_once"helpers/index.php";
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Store List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Cart</a></li>
                    <li class="breadcrumb-item active">Cart List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Client Name</th>
                                        <th>Product</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT t1.*, t2.title, t2.name, t2.last_name FROM user_cart as t1 join clients as t2 on t1.user=t2.client_key");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $num++ ?></td>
                                            <td><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name'] ?></td>
                                            <td><img src="products/<?php echo getSinglevalue('products_db', 'p_key', $rows['product'], 24) ?>" width="100px"><br><?php echo getSinglevalue('products_db', 'p_key', $rows['product'], 4); ?></td>
                                            <td><?php echo $rows['color'] ?></td>
                                            <td><?php echo $rows['size'] ?></td>
                                            <td><?php echo $rows['qty'] ?></td>
                                            <td>
                                            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                                            <input type="hidden" name="cartid" value="<?php echo $rows['uc_key'] ?>">
                                            <button type="submit" name="remove_cart" class="btn btn-primary">Remove</button>
                                            </form>
                                            </td>
                                        </tr>
                                            <?php }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once"helpers/footer.php";

if(isset($_POST['remove_cart'])){
    extract($_POST);
    if(!empty($cartid)){
        $query = mysqli_query($conn, "DELETE FROM user_cart WHERE uc_key='$cartid'");
        if($query){
            echo "<script>showNotification('alert-info', 'Item Removed From Cart Successfully', 'top', 'right', '', '')</script>";
            header('refresh: 0.5');
        }else{
            echo "<script>showNotification('alert-error', 'Item Not Removed From Cart Successfully', 'top', 'right', '', '')</script>";
        }
    }
}

?>