<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['submenu'])){
        extract($_GET);
        if(!empty($submenu)){
            $submenu_id = $submenu;
        $sql = mysqli_query($conn, "SELECT * FROM submenu WHERE submenu_id='$submenu_id'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Submenu
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Submenu</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Menu<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="menu" id="menu" required>
                                    <option value="">-- Select Menu --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[1] : '' ;
                                    display_option_selected('menu', 0, 1, $value) ?>
                                </select>
                            </div>
                            <div >
                            <div class="form-group form-float">
                            <div id="subcategory">
                                <label>Subcategory<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="subcategory" required>
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[3] : '';
                                    display_option_selected('subcategory', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label>Submenu Name</label>
                                <input type="text" class="form-control" name="submenu" <?php echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Submenu Name"'; ?> required>                              
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[4] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[4] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_submenu">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_submenu">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- #END# Advanced Validation --> 

         <!-- Table Starts -->

         <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Submenu</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Menu</th>
                                        <th>Submenu</th>
                                        <th>Subcategory</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT * FROM submenu");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                            <tr>
                                                <td><?php echo $num++ ?></td>
                                                <td><?php echo getSinglevalue('menu', 'menu_id', $rows['menu'], 1) ?></td>
                                                <td><?php echo $rows['submenu'] ?></td>
                                                <td><?php echo getSinglevalue('subcategory', 'subcat_key', $rows['subcategory'], 3); ?></td>
                                                <td><?php status($rows['status']) ?></td>
                                                <td>
                                                    <a href="submenu?submenu=<?php echo $rows['submenu_id'] ?>" class="btn btn-primary">Edit</a>
                                                </td>
                                            </tr>
                                        <?php   }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->


    </div>

    </section>



<?php 
include_once"helpers/footer.php";

if(isset($_POST['add_submenu'])){
    extract($_POST);
    if(!empty($menu) && !empty($subcategory) && !empty($submenu)){
        $chck_brand = check_duplicate('submenu', 'submenu', $submenu);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Subcategory already exists', 'top', 'right', '', '')</script>";
        }else{
            $query = mysqli_query($conn, "INSERT INTO submenu (menu, submenu, subcategory, status, created_at) VALUES ('$menu', '$submenu', '$subcategory', 1, '$created_at')");
            if($query){
                echo "<script>showNotification('alert-info', 'Submenu Added Successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');
            }else{
                echo "<script>showNotification('alert-danger', 'Submenu Not Added Successfully', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_submenu'])){
    extract($_POST);
    if(!empty($menu) && !empty($submenu) && !empty($subcategory)){
        $query = mysqli_query($conn, "UPDATE submenu SET menu='$menu', submenu='$submenu', subcategory='$subcategory', status='$status' WHERE submenu_id='$submenu_id'");
        if($query){
            echo "<script>showNotification('alert-info', 'Submenu Updated Successfully', 'top', 'right', '', '')</script>";
            header('location: submenu');
        }else{
            echo "<script>showNotification('alert-danger', 'Submenu Not Updated Successfully', 'top', 'right', '', '')</script>";
        }

    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}





?>