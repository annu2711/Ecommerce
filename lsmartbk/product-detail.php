﻿<?php 
    include_once"helpers/index.php";
    
    if(isset($_GET['product'])){
        extract($_GET);
        $query = mysqli_query($conn, "SELECT * FROM products_db WHERE p_key='$product'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                $pd_rows = mysqli_fetch_assoc($query);
            }else{
                header('location: product-list');
            }
        }
    }else{
        header('location: product-list');
    }
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Product Detail</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="preview col-lg-12 col-md-12">
                                <div class="product-images">
                                <img class="image-preview" src="products/<?php echo $pd_rows['p_img'] ?>">
                                    <?php 

                                        $other_images = explode(",", $pd_rows['o_img']);
                                        foreach($other_images as $key => $img){
                                            if($img!= ""){
                                                echo '<img class="image-preview" src="products/'.$img.'">';
                                            }
                                        }
                                    ?>
                                    <img src="">    
                                </div>
                            </div>
                            <div class="details col-lg-12 col-md-12">
                                <h3 class="product-title m-b-0"><?php echo $pd_rows['product_name'] ?></h3>
                                <h4 class="price m-t-0">Price: <strike class="text-danger"><?php echo $pd_rows['regular_price'] ?></strike> <span class="col-amber"><?php echo $pd_rows['price'] ?></span></h4>
                                <div class="rating">
                                    <div class="stars">
                                        <span class="zmdi zmdi-star col-amber"></span>
                                        <span class="zmdi zmdi-star col-amber"></span>
                                        <span class="zmdi zmdi-star col-amber"></span>
                                        <span class="zmdi zmdi-star col-amber"></span>
                                        <span class="zmdi zmdi-star-outline"></span>
                                    </div>
                                    <span class="m-l-10">41 reviews</span>
                                </div>
                                <hr>
                                
                                <div class="product-description">
                                    <?php echo (!empty($pd_rows['features'])) ? $pd_rows['features'] : 'No Features Available' ; ?>
                                </div>
                                <!-- <p class="vote"><strong>78%</strong> of buyers enjoyed this product! <strong>(23 votes)</strong></p> -->
                                <h5 class="sizes">sizes:
                                    <?php
                                        $sizes = explode(",",$pd_rows['size']);
                                        if($sizes!= ""){
                                            foreach($sizes as $key => $size){
                                                if($size!= ""){
                                                    echo '<span class="size" title="'.$size.'">'.$size.'</span>';
                                                }
                                            }
                                        }
                                    ?>
                                </h5>
                                <h5 class="colors">colors:
                                <span class="size" title=""><?php echo $pd_rows['color'] ?></span>
                                </h5>
                                <h5>
                                <?php 
                                    if($pd_rows['attribute']!= ""){
                                        $fr_at = explode(",", $pd_rows['attribute']);
                                        foreach($fr_at as $key => $item){
                                            if($item!= ""){
                                                $items = explode("!", $item);
                                                echo $items[0].'<span>'.$items[1].'</span>';
                                            }
                                        }
                                    }
                                ?>  
                                </h5>
                                <h5>Preffered For: <?php echo $pd_rows['prf'] ?></h5>
                                <h5 class="colors">Stock:
                                <span class="size" title=""><?php echo $pd_rows['stock'] ?></span>
                                </h5>
                                <hr>
                                <div class="action" style="text-align: right;">
                                    <a href="product?product=<?php echo $product; ?>" class="btn btn-primary btn-round waves-effect">Edit Product</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#description">Description</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#review">Specification</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#about">Other Information</a></li>
                    </ul>
                </div>
                <div class="card">
                    <div class="body">                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="description">
                                <?php echo $pd_rows['description'] ?>
                            </div>
                            <div class="tab-pane" id="review">
                                <?php echo $pd_rows['specification'] ?>
                            </div>
                            <div class="tab-pane" id="about">
                                <h6>Shipping Information</h6>
                                <?php 
                                    if($pd_rows['shipping_info']!= ""){
                                        $sh = explode(",", $pd_rows['shipping_info']); ?>
                                    <p>Weight: <?php echo $sh[0] ?> || Width: <?php echo $sh[1] ?> || Height: <?php echo $sh[2] ?></p>   
                                <?php  }else{
                                    echo '<p>No Information Available</p>';
                                }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once"helpers/footer.php"; ?>