<?php 
    include_once"helpers/index.php";

    $edit = "";
    if(isset($_GET['blog'])){
        extract($_GET);
        if(!empty($blog)){
            $blog_id = $_GET['blog'];
            $sql = mysqli_query($conn, "SELECT * FROM blogs WHERE blog_id='$blog_id'");
            if($sql){
                if(mysqli_num_rows($sql) > 0){
                    $array = mysqli_fetch_array($sql);
                    $edit = 1;
                }
            }
        }
    }
?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Blog
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Blog</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype= multipart/form-data>
                            <div class="form-group form-float">
                                <label>Blog Author</label>
                                <input type="text" class="form-control" name="author" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Blog Author"'; ?> required>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Blog Title</label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Blog Author"'; ?> required>                              
                            </div>

                            <div class="form-group form-float">
                                <label>Image<sup>*</sup></label>
                                <input type="file" name="file" id="slide_file" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG" onchange="preview_image('slide_file', 'image_preview');" <?php echo (!empty($edit)) ? '' : 'required'; ?>>
                                <div class="row">
                                    <div class="col-md-6">
                                    <p><b>New Uploaded Image</b></p>
                                        <div id="image_preview"></div>
                                    </div>
                                    <?php if(!empty($edit)){ ?>
                                    <div class="col-md-6">
                                        <p><b>Previous Image</b></p>
                                        <img src="<?php echo WEBSITE_DIRECTORY.$array[4] ?>" width="500px">
                                    </div>
                                    <?php } ?>
                                </div>
                                
                                <p class="text-danger"><strong>Note:</strong> Please use 1920px x 990px image for better quality.</p>
                            </div>

                            <div class="form-group form-float">
                                <label>Blog Text</label>
                                <textarea class="form-control editor" name="text"  placeholder="Enter Blog Text" required><?php echo (!empty($edit)) ? $array[3] : ""; ?></textarea>                              
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[5] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[5] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_blog">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_blog">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
         <!-- Table Ends -->
    </div>

</section>

<?php 
include_once "helpers/footer.php";
flash_session_admin();

if(isset($_POST['add_blog'])){
    extract($_POST);
    if(!empty($title) && !empty($author) && !empty($text) && !empty($_FILES['file']['name'])){
        $file_type = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $imagename = date('dmyhis')."_".$_FILES['file']['name'];
        $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], $imagename, WEBSITE_DIRECTORY);
        
        $status = 1;
        $chck_brand = check_duplicate('blogs', 'blog_title', $title);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Author already exists', 'top', 'right', '', '')</script>";
        }else{
            $query = mysqli_query($conn, "INSERT INTO blogs (blog_author,blog_title, blog_text, blog_image, status, created_at) VALUES ('$author','$title', '$text','$imagename', 1, '$created_at')");
            if($query){
                echo "<script>showNotification('alert-info', 'Blog Added Successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');
            }else{
                echo "<script>showNotification('alert-danger', 'Blog Not Added Successfully', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_blog'])){
    extract($_POST);
    if(!empty($author) && !empty($title) && !empty($text)){
        $image1 = "";
        if(!empty($_FILES['file']['name'])){
            $imagename = date('dmyhis')."_".$_FILES['file']['name'];
            $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], $imagename, WEBSITE_DIRECTORY);
        }else{
            $image1 = $array[4];
        }
        $query = mysqli_query($conn, "UPDATE blogs SET blog_author='$author', blog_title='$title', blog_text='$text', blog_image='$image1',status='$status' WHERE blog_id='$blog_id'");
        if($query){
            $_SESSION['result'] = [true, 'Blog Updated Successfully', 'success'];
        }else{
            $_SESSION['result'] = [false, 'Blog Not Updated Successfully', 'danger'];
        }
        header('location: manage-blog?blog='.$_GET['blog']);
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>