<?php 
    include_once "helpers/index.php";

    userAccess(['1', '2'], $deg);
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Admin User List</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Admin User List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Username</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT * FROM admin_users");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $num++ ?></td>
                                            <td><?php echo $rows['name'] ?></td>
                                            <td><?php echo getSinglevalue('designation', 'deg_id', $rows['deg'], 1) ?></td>
                                            <td><?php echo $rows['email'] ?></td>
                                            <td><?php echo $rows['mobile'] ?></td>
                                            <td><?php echo $rows['username'] ?></td>
                                            <td><?php status($rows['status']) ?></td>
                                            <td><a href="add-admin-user?admin=<?php echo $rows['au_id'] ?>" class="btn btn-primary">Edit</a></td>
                                        </tr>
                                            <?php }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once "helpers/footer.php";
flash_session('User Updated', 'User Not Updated');
 ?>