﻿<?php
ob_start();
session_start();
include_once"helpers/config.php";
include_once"helpers/function.php";
if(isset($_SESSION['userid'])){
    header('location:dashboard');
}
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>Moonraker</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-cyan authentication sidebar-collapse">
<div class="page-header">
    <div class="page-header-image" style="background-image:url(assets/images/login.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                    <div class="header">
                        <div class="logo-container">
                            <img src="../img/logo.png" alt="">
                        </div>
                        <h5>Log in</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="text" class="form-control" name="username" placeholder="Enter User Name">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg">
                            <input type="password" placeholder="Password" name="password" class="form-control" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <input type="submit" name="admin_login" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" value="SIGN IN">
                        <!-- <h6 class="m-t-20"><a href="forgot-password.html" class="link">Forgot Password?</a></h6> -->
                    </div>

                    <!-- modal starts -->

                    <div class="loginmodal modal fade show" id="defaultModal" tabindex="-1" role="dialog" style="padding-left: 15px;">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                <h4 class="title" id="defaultModalLabel" style="color: #555">Modal title</h4>
            </div>
            <div class="modal-body" style="color: #555"> You have already logged into another device. Do you want to continue with this device. </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect">No</button>
                <button type="submit" class="btn btn-primary waves-effect" data-dismiss="modal" name="access">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- modal ends -->


                </form>
            </div>
        </div>
    </div>
</div>


<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
<!-- default-js -->
<script src="assets/default-js/main.js"></script>


<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>

<?php

if(isset($_POST['admin_login'])){
    extract($_POST);
    if(!empty($username) && !empty($password)){
        $p = md5($password);
        try{
            $stmt = $conn->prepare("SELECT * FROM admin_users WHERE username=? AND password=? ORDER BY au_id DESC LIMIT 1");
            $stmt->bind_param('ss', $username, $p);
            if($stmt->execute()){
                $result = $stmt->get_result();
                $token = $row['token'];
                if(!empty($token)){
                    $_SESSION['user'] = $username;
                    $_SESSION['pass'] = $password;
                    echo '<script>loginAlert()</script>';
                }else{
                    $token = login($username, $password);
                    $_SESSION['token'] = $token;
                    $_SESSION['username'] = $username;
                    $_SESSION['userid'] = 1;
                    $_SESSION['deg'] = getSinglevalue('admin_users', 'username', $username, 7);
                    header('location: dashboard');
                }
            }else{
                throw new exception($conn->error);
            }
        }catch(Exception $e){   
            echo $conn->error;
            echo "<script>showNotification('alert-danger', 'Invalid Credantials', 'top', 'right', '', '');</script>";
        }
        
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '');</script>";
    }
}


if(isset($_POST['access'])){
    extract($_POST);
    $username = $_SESSION['user'];
    $password = $_SESSION['pass'];
    $token = login($username, $password);
    $_SESSION['userid'] = getSinglevalue('admin_users', 'token', $token, 0);
    unset($_SESSION['user']);
    unset($_SESSION['pass']);
    $_SESSION['token'] = $token;
    $_SESSION['username'] = $username;
    $_SESSION['deg'] = getSinglevalue('admin_users', 'username', $username, 7);
    header('location: dashboard');
}




?>