<?php 
include_once "helpers/index.php";
$edit = "";
if(isset($_GET['page'])){
    $page = $_GET['page'];
}else{
    $page = 1;
}
$limit = 10;
$skip  = ($page - 1) * $limit;
if(isset($_GET['filter'])){
    $edit = 1;
    extract($_GET);
    $query = "";
    if($sku != ""){
        $query = $query." AND t1.sku='$sku'";
    }
    if($brand != ""){
        $query = $query." AND t1.brand='$brand'";
    }
    if($category!= ""){
        $query = $query." AND t1.category='$category'";
    }
    if($subcategory!= ""){
        $query = $query." AND t1.subcategory='$subcategory'";
    }
    if(!empty($minprice)){
        $query = $query." AND t1.price >= $minprice";
    }
    if(!empty($maxprice)){
        $query = $query." AND t1.price <= $maxprice";
    }
    if(!empty($status)){
        $query = $query." AND t1.status='$status'";
    }
    if(!empty($upc)){
        $query = $query." AND t1.vpn='$upc'";
    }
    if(!empty($batchno)){
        $query = $query." AND t1.batch_no='$batchno'";
    }
    if(!empty($query)){
    // echo $query;
    $result_db = mysqli_query($conn,"SELECT COUNT(t1.p_id) FROM product as t1 WHERE t1.publish=1 $query"); 
    $row_db = mysqli_fetch_row($result_db);  
    $total_records = $row_db[0];

    $sql = mysqli_query($conn, "SELECT t1.p_id, t1.product_name, t1.sku, t1.category, t1.subcategory, t1.mrp, t1.price, t1.status, t1.publish, t2.color, t2.size, t1.brand, (SELECT SUM(qty) FROM product_stock WHERE t1.sku=product_stock.product) as pdqty, (SELECT SUM(qty) FROM order_book WHERE t1.sku=order_book.product) as soldqty FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE publish=1 $query ORDER BY p_id DESC LIMIT $skip, $limit");
    if($sql){
        $filterresult = [];
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $filterresult[] = $rows;
            }
        }
    }
    // print_r($filterresult);
}
}
?>
<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Stock Management</a></li>
                    <li class="breadcrumb-item active">Product Stock List</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- filter starts -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Filters</div>
                    <div class="body">
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
                        <div class="row">
                            <input type="hidden" name="filter" value="1">
                            <div class="col-md-4">
                                <label>Product SKU</label>
                                <input type="text" class="form-control" name="sku" <?php echo (!empty($edit)) ? 'value="'.$sku.'"' : 'placeholder="Product SKU"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Brand</label>
                                <select class="form-control z-index" name="brand" data-live-search="true">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $brand : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Category</label>
                                <select class="form-control" name="category" id="category" data-live-search="true">
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $category : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                            <div id="subcategory">
                                <label>Subcategory</label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" data-live-search="true">
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    if(!empty($edit)){
                                        $value = (!empty($edit)) ? $subcategory : '';
                                        display_option_selected('subcategory', 1, 3, $value);
                                    } 
                                    ?>
                                </select>
                            </div>
                            </div>

                            <div class="col-md-4">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="">-- Select Status Tag --</option>
                                    <?php $statusvalue = (!empty($edit)) ? $status : ''; ?>
                                    <?php display_option_selected('color', 0, 1, $statusvalue); ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Price</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="minprice" <?php echo (!empty($edit)) ? 'value="'.$minprice.'"' : 'placeholder="Min Price"' ; ?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="maxprice" <?php echo (!empty($edit)) ? 'value="'.$maxprice.'"' : 'placeholder="Max Price"' ; ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label>UPC or VPC code</label>
                                <input type="text" class="form-control" name="upc" <?php echo (!empty($edit)) ? 'value="'.$upc.'"' : 'placeholder="UPC or VPC code"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Batch No</label>
                                <input type="text" class="form-control" name="batchno" <?php echo (!empty($edit)) ? 'value="'.$batchno.'"' : 'placeholder="Batch No"' ; ?>>
                            </div>

                            <div class="col-md-12 mt-3 text-right">
                                <input type="submit" class="btn btn-success" name="apply_filter" value="Apply Filter">
                                <a href="product-list" class="btn btn-danger">Reset</a>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- filter ends -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                <?php if(!empty($edit)){ ?>
                <div class="card-header text-success">Filter Applied</div>
                <?php } ?>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product SKU</th>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Stock</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(empty($edit)){
                                        $result_db = mysqli_query($conn,"SELECT COUNT(p_id) FROM product WHERE publish=1"); 
                                        $row_db = mysqli_fetch_row($result_db);  
                                        $total_records = $row_db[0];
                                        $query = mysqli_query($conn, "SELECT t1.p_id, t1.product_name, t1.brand, t1.sku, t1.category, t1.subcategory, t1.mrp, t1.price, t1.status, t1.publish, t2.color, t2.size, (SELECT SUM(qty) FROM product_stock WHERE t1.sku=product_stock.product) as pdqty, (SELECT SUM(qty) FROM order_book WHERE t1.sku=order_book.product) as soldqty FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 ORDER BY t1.p_id DESC LIMIT $skip, $limit");
                                        if($query){
                                        if(mysqli_num_rows($query) > 0){

                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>

                                            <td><img src="<?php echo PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $rows['sku'], 2) ?>" width="50px" height="50px"></td>
                                            <td><?php echo $rows['sku'] ?></td>
                                            <td><?php echo $rows['product_name'] ?></td>
                                            <td><?php echo getSinglevalue('category', 'cat_key', $rows['category'], 2).' >> '.getSinglevalue('subcategory', 'subcat_key', $rows['subcategory'], 3) ?></td>
                                            <td><?php echo getSinglevalue('brands', 'brand_key', $rows['brand'], 3); ?></td>
                                            <td><?php echo $rows['color'] ?></td>
                                            <td><?php echo $rows['size'] ?></td>
                                            <td><?php echo intval($rows['pdqty'] - $rows['soldqty']) ?></td>
                                            <td><strike class="text-danger"><?php echo $rows['mrp'] ?></strike> <?php echo $rows['price'] ?></td>
                                            <td><?php status($rows['publish']) ?></td>
                                            <td>
                                            <a href="add-stock?product=<?php echo $rows['p_id'] ?>" class="btn btn-success">Add Stock</a>
                                            </td>
                                            
                                        </tr>
                                    <?php   }
                                        }
                                    }else{
                                        echo mysqli_error($conn);
                                    }
                                }else{ 
                                    if(!empty($filterresult)){
                                   for($i = 0; $i <= count($filterresult) - 1; $i++){
                                    ?>
                                <tr>
                                            <td><img src="<?php echo PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $filterresult[$i][2], 2) ?>" width="50px" height="50px"></td>
                                            <td><?php echo $filterresult[$i][2] ?></td>
                                            <td><?php echo $filterresult[$i][1] ?></td>
                                            <td><?php echo getSinglevalue('category', 'cat_key', $filterresult[$i][3], 2).' >> '.getSinglevalue('subcategory', 'subcat_key', $filterresult[$i][4], 3) ?></td>
                                            <td><?php echo getSinglevalue('brands', 'brand_key', $filterresult[$i][11], 3); ?></td>
                                            <td><?php echo $filterresult[$i][9] ?></td>
                                            <td><?php echo $filterresult[$i][10] ?></td>
                                            <td><?php echo intval($filterresult[$i][12] - $filterresult[$i][13]) ?></td>
                                            <td><strike class="text-danger"><?php echo $filterresult[$i][5] ?></strike> <?php echo $filterresult[$i][6] ?></td>
                                            <td><?php status($filterresult[$i][8]) ?></td>
                                            <td>
                                            <a href="product?product=<?php echo $filterresult[$i][0] ?>" class="btn btn-primary">Edit</a>
                                            <a href="add-stock?product=<?php echo $filterresult[$i][0] ?>" class="btn btn-success">Add Stock</a>
                                            </td>
                                        </tr>
                                <?php }
                                }else{ ?>
                                    <tr>
                                        <td colspan="7" align="center"><p class="m-0"><strong>No Result Found.</strong></p></td>
                                    </tr>
                                <?php } 
                            }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- pagination here -->
                        <div class="row">
                            <div class="col-md-6">
                            <?php 
                                if(!empty($edit)){
                                    // echo $_SERVER['QUERY_STRING'];
                                    ?>
                                
                            <input type="hidden" id="filterval" value="<?php echo $_SERVER['QUERY_STRING'] ?>">   
                            <?php }else{ ?>
                                <input type="hidden" id="filterval" value="">
                            <?php } ?>
                            <input type="hidden" id="currentpage" value="<?php echo $page ?>">
                            <p><strong>Total Pages: </strong><input type="text" id="totalpage" value="<?php echo (!empty($edit)) ? ceil($total_records / 10) : ceil($total_records / 10); ?>" style="border: 10px"></p>
                            </div>
                            <div class="col-md-6">
                                <?php //if(empty($edit)){ ?>
                                    <p id="pagination-here"></p>
                                <?php //} ?>
                            </div>
                        </div>
                        <!-- pagination ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once "helpers/footer.php"; ?>