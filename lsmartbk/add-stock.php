<?php 
include_once "helpers/index.php"; 

$poarr = [];
if(isset($_GET['poid'])){
    extract($_GET);
    if(!empty($poid)){
        $sql = mysqli_query($conn, "SELECT * FROM product_stock WHERE ps_id='$poid' ORDER BY ps_id DESC LIMIT 1");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $poarr = mysqli_fetch_array($sql);
                $product = getSinglevalue('product', 'sku', $poarr[1], 0);
                // print_r($poarr);
            }
        }
    }   
}

if(isset($_GET['product'])){
    extract($_GET);
}

if(!empty($product)){
    // echo "product=>".$product;
    $sql = mysqli_query($conn, "SELECT sku, product_name FROM product WHERE p_id='$product' ORDER BY p_id DESC LIMIT 1");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            $productarr = mysqli_fetch_array($sql);
        }
    }
}


// print_r($productarr);


// product stock
$query = mysqli_query($conn, "SELECT SUM(qty) as qty FROM product_stock WHERE product='$productarr[0]' ORDER BY ps_id DESC");
if($query)
{
    if(mysqli_num_rows($query) > 0){
        $row = mysqli_fetch_assoc($query);
        $stockqty = $row['qty'];
    }
}

?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Stock
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Stock Management</a></li>
                    <li class="breadcrumb-item active">Add Stock</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-4">
                                <p><strong>Product Name: </strong><?php echo $productarr[1] ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><strong>Product SKU: </strong><?php echo $productarr[0] ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><strong>Availabel Stock: </strong><?php echo $stockqty ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" class="row" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float col-md-4">
                                <label>Invoice No / PO <sup classs="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="invoice" value="<?php echo !empty($poarr) ? $poarr[2] : '' ; ?>" required>
                            </div>

                            <div class="form-group form-float col-md-4">
                                <label>Order Create Date <sup classs="text-danger">*</sup></label>
                                <input type="date" class="form-control" name="date" value="<?php echo !empty($poarr) ? $poarr[3] : '' ; ?>" required>
                            </div>

                            <div class="form-group form-float col-md-4">
                                <label>Quantity <sup classs="text-danger">*</sup></label>
                                <input type="number" class="form-control" name="qty" value="<?php echo !empty($poarr) ? $poarr[4] : '' ; ?>" required>
                            </div>

                            <div class="form-group form-float col-md-4">
                                <label>Start Shipping</label>
                                <input type="date" class="form-control" name="stdate" value="<?php echo !empty($poarr) ? $poarr[5] : '' ; ?>">
                            </div>

                            <div class="form-group form-float col-md-4">
                                <label>End Shipping</label>
                                <input type="date" class="form-control" name="enddate" value="<?php echo !empty($poarr) ? $poarr[6] : '' ; ?>">
                            </div>

                            <div class="form-group form-float col-md-4">
                                <label>Purchase Price<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="purchase_price" value="<?php echo !empty($poarr) ? $poarr[8] : '' ; ?>" required>
                            </div>
                            <div class="form-group form-float col-md-12" align="right">
                            <?php if(!empty($poarr)){ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_stock">UPDATE</button>
                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_stock">SUBMIT</button>
                            <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
    </div>
</section>
<?php 
include_once "helpers/footer.php";
flash_session_admin();
// save store
if(isset($_POST['add_stock'])){
    extract($_POST);
    if(!empty($invoice) && !empty($date) && !empty($qty) && !empty($purchase_price)){
        $status = 1;
        if(!empty($enddate)){
           $thedate = strtotime($enddate);
           $currentdate = strtotime(date('Y-m-d'));
            if($thedate > $currentdate){
                $status = 0;
            }
        }
        $sql = mysqli_query($conn, "INSERT INTO product_stock (product, challan, date, qty, start_date, end_date, purchase_price, status, created_at) VALUES ('$productarr[0]', '$invoice', '$date', '$qty', '$stdate', '$enddate', '$purchase_price', '$status', '$created_at')");
        if($sql){
            $_SESSION['result'] = [true, 'Stock Added Successfully', 'success'];
        }else{
            echo mysqli_error($conn);
            // echo "Date=>".$stdate;
            $_SESSION['result'] = [false, 'Stock Not Added Successfully', 'error'];
        }
        
        header('location: '.$_SERVER['REQUEST_URI']);
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_stock'])){
    extract($_POST);
    if(!empty($invoice) && !empty($date) && !empty($qty) && !empty($purchase_price)){
        $status = 1;
        if(!empty($enddate)){
           $thedate = strtotime($enddate);
           $currentdate = strtotime(date('Y-m-d'));
            if($thedate > $currentdate){
                $status = 0;
            }
        }
        $sql = mysqli_query($conn, "UPDATE product_stock SET product='$productarr[0]', challan='$invoice', date='$date', qty='$qty', start_date='$stdate', end_date='$enddate', purchase_price='$purchase_price', status='$status' WHERE ps_id='$poid'");
        if($sql){
            $_SESSION['result'] = [true, 'Stock Update Successfully', 'success'];
        }else{
            echo mysqli_error($conn);
            $_SESSION['result'] = [false, 'Stock Not Update Successfully', 'error'];
        }
        header('location: '.$_SERVER['REQUEST_URI']);
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}

?>