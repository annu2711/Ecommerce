<?php 
include_once "helpers/index.php";
$edit = "";
if(isset($_GET['page'])){
    $page = $_GET['page'];
}else{
    $page = 1;
}
$limit = 10;
$skip  = ($page - 1) * $limit;
if(isset($_GET['filter'])){
    $edit = 1;
    extract($_GET);
    $query = "";
    if($sku != ""){
        $query = $query." AND sku='$sku'";
    }
    if($brand != ""){
        $query = $query." AND brand='$brand'";
    }
    if($category!= ""){
        $query = $query." AND category='$category'";
    }
    if($subcategory!= ""){
        $query = $query." AND subcategory='$subcategory'";
    }
    if(!empty($minprice)){
        $query = $query." AND price >= $minprice";
    }
    if(!empty($maxprice)){
        $query = $query." AND price <= $maxprice";
    }
    if(!empty($status)){
        $query = $query." AND status='$status'";
    }
    if(!empty($upc)){
        $query = $query." AND vpn='$upc'";
    }
    if(!empty($batchno)){
        $query = $query." AND batch_no='$batchno'";
    }
    if(!empty($query)){
    $result_db = mysqli_query($conn,"SELECT COUNT(p_id) FROM product WHERE publish=1 $query"); 
    $row_db = mysqli_fetch_row($result_db);  
    $total_records = $row_db[0];

    $sql = mysqli_query($conn, "SELECT t1.p_id, t1.product_name, t1.sku, t1.category, t1.subcategory, t1.mrp, t1.price, t1.status, t1.publish, t2.color, t2.size, t1.brand, (SELECT SUM(qty) FROM product_stock WHERE t1.sku=product_stock.product) as pdqty, (SELECT SUM(qty) FROM order_book WHERE t1.sku=order_book.product) as soldqty FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE publish=1 $query ORDER BY p_id DESC LIMIT $skip, $limit");
    if($sql){
        $filterresult = [];
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $filterresult[] = $rows;
            }
        }
    }
    // print_r($filterresult);
}
}
?>
<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product Order List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Stock Management</a></li>
                    <li class="breadcrumb-item active">Product Order List</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- filter starts -->
    <!-- <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Filters</div>
                    <div class="body">
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
                        <div class="row">
                            <input type="hidden" name="filter" value="1">
                            <div class="col-md-4">
                                <label>Product SKU</label>
                                <input type="text" class="form-control" name="sku" <?php echo (!empty($edit)) ? 'value="'.$sku.'"' : 'placeholder="Product SKU"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Brand</label>
                                <select class="form-control z-index" name="brand" data-live-search="true">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $brand : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Category</label>
                                <select class="form-control" name="category" id="category" data-live-search="true">
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $category : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                            <div id="subcategory">
                                <label>Subcategory</label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" data-live-search="true">
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    if(!empty($edit)){
                                        $value = (!empty($edit)) ? $subcategory : '';
                                        display_option_selected('subcategory', 1, 3, $value);
                                    } 
                                    ?>
                                </select>
                            </div>
                            </div>

                            <div class="col-md-4">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="">-- Select Status Tag --</option>
                                    <?php $statusvalue = (!empty($edit)) ? $status : ''; ?>
                                    <?php display_option_selected('color', 0, 1, $statusvalue); ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>Price</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="minprice" <?php echo (!empty($edit)) ? 'value="'.$minprice.'"' : 'placeholder="Min Price"' ; ?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="maxprice" <?php echo (!empty($edit)) ? 'value="'.$maxprice.'"' : 'placeholder="Max Price"' ; ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label>UPC or VPC code</label>
                                <input type="text" class="form-control" name="upc" <?php echo (!empty($edit)) ? 'value="'.$upc.'"' : 'placeholder="UPC or VPC code"' ; ?>>
                            </div>

                            <div class="col-md-4">
                                <label>Batch No</label>
                                <input type="text" class="form-control" name="batchno" <?php echo (!empty($edit)) ? 'value="'.$batchno.'"' : 'placeholder="Batch No"' ; ?>>
                            </div>

                            <div class="col-md-12 mt-3 text-right">
                                <input type="submit" class="btn btn-success" name="apply_filter" value="Apply Filter">
                                <a href="product-list" class="btn btn-danger">Reset</a>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- filter ends -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                <?php if(!empty($edit)){ ?>
                <div class="card-header text-success">Filter Applied</div>
                <?php } ?>
                <div class="card-header text-success">Purchase Order List</div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>PO No.</th>
                                        <th>Image</th>
                                        <th>Product SKU</th>
                                        <th>Qty</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Order create Date</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Ws Price</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(empty($edit)){
                                        $query = mysqli_query($conn, "SELECT t1.ps_id, t1.product, t1.challan, t1.qty, t1.start_date, t1.end_date, t1.place_order_date, t1.purchase_price, t1.status, t2. color, t2.size, (SELECT image FROM product_images WHERE product=t1.product ORDER BY pi_id ASC LIMIT 1) as image FROM product_stock as t1 join product_attributes as t2 on t1.product=t2.p_id");
                                        if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $rows['challan'] ?></td>
                                            <td><img src="<?php echo PRODUCT_DIRECTORY.$rows['image']?>" width="50px"></td>
                                            <td><?php echo $rows['product'] ?></td>
                                            <td><?php echo $rows['qty'] ?></td>
                                            <td><?php echo $rows['start_date'] ?></td>
                                            <td><?php echo $rows['end_date'] ?></td>
                                            <td><?php echo $rows['place_order_date'] ?></td>
                                            <td><?php echo $rows['color'] ?></td>
                                            <td><?php echo $rows['size'] ?></td>
                                            <td><?php echo $rows['purchase_price'] ?></td>
                                            <td><?php status($rows['status']) ?></td>
                                            <td><a href="add-stock?poid=<?php echo $rows['ps_id'] ?>" class="btn btn-success">Edit</a></td>
                                        </tr>
                                    <?php   }
                                        }
                                    }else{
                                        echo mysqli_error($conn);
                                    }
                                }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once "helpers/footer.php"; ?>