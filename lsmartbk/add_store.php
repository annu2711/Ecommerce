﻿<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['store'])){
        extract($_GET);
        if(!empty($store)){
        $store_key = $store;
        $sql = mysqli_query($conn, "SELECT * FROM store WHERE store_key='$store_key'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Store
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Add Store</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Store Name <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="store" <?php echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Store Name"'; ?> required>
                            </div>
                            <div class="form-group form-float">
                                <label>Email Id <sup class="text-danger">*</sup></label>
                                <input type="email" class="form-control" name="email" <?php echo (!empty($edit)) ? 'value="'.$array[3].'" readonly ' : 'placeholder="Enter Email"'; ?> required>                                
                            </div>
                            <div class="form-group form-float">
                                <label>Mobile No <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="mobile" maxlength="10" <?php echo (!empty($edit)) ? 'value="'.$array[4].'" readonly ' : 'placeholder="Enter mobile"'; ?> required>                              
                            </div>
                            <div class="form-group form-float">
                                <label>GST No.</label>
                                <input type="text" class="form-control" name="gst" <?php echo (!empty($edit)) ? 'value="'.$array[5].'"' : 'placeholder="Enter GST No."'; ?>>                                
                            </div>
                            <div class="form-group form-float">
                                <label>Address <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="address" <?php echo (!empty($edit)) ? 'value="'.$array[7].'"' : 'placeholder="Enter Address"'; ?> required>                                
                            </div>
                            <div class="form-group form-float">
                                <label>Password <sup class="text-danger">*</sup></label>
                                <input type="password" class="form-control" name="password" placeholder="Enter Password" required>                       
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[8] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[8] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_store">UPDATE</button>
                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_store">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
    </div>
</section>
<?php 
include_once"helpers/footer.php";

// save store

if(isset($_POST['add_store'])){
    extract($_POST);
    if(!empty($store) && !empty($email) && !empty($mobile) && !empty($address) && !empty($password)){
        $chck_mob = check_duplicate('store', 'mobile', $mobile);
        $chck_email = check_duplicate('store', 'email', $email);
        if($chck_email == 1){
            echo "<script>showNotification('alert-warning', 'Email already exists', 'top', 'right', '', '')</script>";
        }
        if($chck_mob == 1){
            echo "<script>showNotification('alert-warning', 'Mobile No. already exists', 'top', 'right', '', '')</script>";
        }
        if($chck_email == 0 && $chck_mob == 0){
            $key = rand_char(5);
            $pass = md5($password);
            $query = mysqli_query($conn, "INSERT INTO store (store_key, store_name, email, mobile, gst, password, address, status, created_at) VALUES ('$key', '$store', '$email', '$mobile', '$gst', '$pass', '$address', 1, '$created_at')");
            if($query){
                echo "<script>showNotification('alert-info', 'Store added successfully', 'top', 'right', '', '')</script>";
                header('refresh: 2');
            }else{
                echo "<script>showNotification('alert-danger', 'Store Not added ', 'top', 'right', '', '')</script>";
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}


if(isset($_POST['update_store'])){
    extract($_POST);
    if(!empty($store)  && !empty($address)){
        $query = mysqli_query($conn, "UPDATE store SET store_name='$store', gst='$gst', address='$address' WHERE store_key='$store_key'");
        if($query){
            header('location: store-list');
        }else{
            echo "<script>showNotification('alert-danger', 'Store Not Updated ', 'top', 'right', '', '')</script>";
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please fill all fields', 'top', 'right', '', '')</script>";
    }
}

?>