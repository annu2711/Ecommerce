<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['product'])){
        extract($_GET);
        $query = mysqli_query($conn, "SELECT * FROM products_db WHERE p_key='$product'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                $array = mysqli_fetch_array($query);
                $edit = 1;
            }else{
                header('location: product-list');
            }
        }
    }
?>
<style>
.img_preview_block img{
    width: 200px;
    border: 1px solid #ccc;
    box-shadow: 0px 0px 5px #ccc;
    margin: 10px;
}
    </style>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2> <?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active"><?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Store<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="store" id="store" required>
                                    <option value="">-- Select Store --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('store', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <div id="brand">
                                <label>Brand</label>
                                <select class="form-control" name="brand" style="padding: 0px !important;">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[3] : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-8">
                                <label>Product Title<sup classs="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[4].'"' : 'placeholder="Enter Product Title"'; ?> required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Product Code<sup classs="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="code" <?php echo (!empty($edit)) ? 'value="'.$array[5].'"' : 'placeholder="Enter Code"'; ?> required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Category<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="category" id="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[6] : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <div id="subcategory">
                                <label>Subcategory</label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;">
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[7] : '';
                                    display_option_selected('subcategory', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Additional Category</label>
                                <input type="text" class="form-control" name="add_cat" <?php echo (!empty($edit)) ? 'value="'.$array[8].'"' : 'placeholder="Enter Additional Category"'; ?>>
                            </div>

                            <div class="form-group col-md-6">
                                <label>Editor's Note</label>
                                <textarea class="form-control editor" name="specification" required>
                                <?php echo (!empty($edit)) ? $array[21] : ''; ?>
                                </textarea>
                                <hr>
                            </div>

                            <div class="form-group col-md-6">
                                <label>Size & Fit<sup classs="text-danger">*</sup></label>
                                <textarea class="form-control editor" name="description" required>
                                <?php echo (!empty($edit)) ? $array[22] : ''; ?>
                                </textarea>
                                <hr>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Technical Specification</label>
                                <textarea class="form-control editor" name="features">
                                <?php echo (!empty($edit)) ? $array[23] : ''; ?>
                                </textarea>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 

        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2><strong>Product</strong> Additional Info</h2>
                </div>
                <div class="body" style="padding-top: 0px;"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" style="padding-top: 0px;">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">GENERAL</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">ATTRIBUTE</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages">INVENTORY</a></li> -->
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings">STATUS</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#shipping">SHIPPING</a></li>
                    </ul>                        
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active" id="home">
                             <b>General</b>
                             <hr>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Regular Price</label>
                                    <input type="number" class="form-control" name="regular_price" <?php echo (!empty($edit)) ? 'value="'.$array[9].'"' : 'placeholder="Enter Regular Price"'; ?>>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Sale Price<sup classs="text-danger">*</sup></label>
                                    <input type="number" class="form-control" name="sale_price" <?php echo (!empty($edit)) ? 'value="'.$array[10].'"' : 'placeholder="Enter Sale Price"'; ?> required>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Offer ( In Percentage )</label>
                                    <input type="number" class="form-control" name="offer" <?php echo (!empty($edit)) ? 'value="'.$array[14].'"' : 'placeholder="Enter Offer"'; ?>>
                                </div>

                                <div class="form-group col-md-4 demo-masked-input">
                                    <label>Offer Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i> </span>
                                        <input type="date" class="form-control date" name="offer_start_date" <?php echo (!empty($edit)) ? 'value="'.$array[15].'"' : ''; ?>>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Offer End Date</label>
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="date" name="offer_end_date" class="form-control date" <?php echo (!empty($edit)) ? 'value="'.$array[16].'"' : ''; ?>>
                                </div>
                                </div>

                                <div class="col-md-12">
                                    <label>Reviews<sup>*</sup></label>
                                    <select class="form-control" name="review" required>
                                        <option value="">-- Select --</option>
                                        <option value="0" <?php 
                                        if(!empty($edit)){
                                            if($array[24] == 0){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Enable</option>
                                        <option value="1"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[24] == 1){
                                                echo 'selected="selected"';
                                            }
                                        } ?>
                                        >Disable</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Preferred For</label>
                                    <select class="form-control" name="prf">
                                        <option value="">-- Select --</option>
                                        <option value="Men"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[17] == 'Men'){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Men</option>
                                        <option value="Women"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[17] == 'Women'){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Women</option>
                                        <option value="Kids"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[17] == 'Kids'){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Kids</option>
                                        <option value="Both"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[17] == 'Both'){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Both (Men, Women)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <b>Attributes</b><hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Color</label>
                                    <input type="text" class="form-control" name="color" value="">
                                </div>
                                <div class="col-md-6">
                                    <label>Size</label>
                                    <input type="text" class="form-control" name="size" value="">
                                </div>
                            </div>
                            <!-- <div class="row color-row-parent">
                                <div class="col-md-12 color-row" id="color_row_0">
                                     <div class="row">
                                        <div class="col-md-12">
                                            <label>Color <span class="text-danger">[ <strong>Note:</strong> Enter six digit color code. Do not enter <strong>"#"</strong> .]</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Color Code">
                                        </div>

                                        <div class="col-md-6 mt-3">
                                            <div class="table-responsive">
                                                <table class="table table-hover attr-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Add Size</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group">
                                                                <input type="text" name="size[]" class="form-control" placeholder="Enter Size">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="5"><span class="text-primary float-right" onclick="addAttr()">+Add More Size</span></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="mt-2">Color Primary Image<sup>*</sup></label>
                                            <input type="file" name="primary_img" id="slide_file" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image('slide_file', 'image_preview1')" <?php echo (!empty($edit)) ? '' : 'required'; ?>>
                                            <div id="image_preview1" class="img_preview_block"></div>
                                            <label class="mt-2">Color Secondary Image</label>
                                            <input type="file" name="second_img" id="slide_file1" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image('slide_file1', 'image_preview2')">
                                            <div id="image_preview2" class="img_preview_block"></div>
                                            <label class="mt-2">Color Variation Images</label>
                                            <input type="file" name="other_img[]" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" multiple>
                                        </div>
                                    </div> 
                                    
                                </div>
                                <div class="col-md-12"><hr></div>
                            </div>      
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-danger float-right mt-1" onclick="addMoreColor()">+ Add More Color</p>
                                </div>
                            </div> -->
                           

                                <!-- <div class="col-md-12">
                                    <b>Add Custom Attribute</b>
                                </div>
                                <div class="col-md-9 master-attr">
                                <div class="row">
                                <div class="col-md-5">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="atr[]" placeholder="Enter Attribute Name">
                                </div>

                                <div class="col-md-5">
                                    <label>Value</label>
                                    <input type="text" class="form-control" name="atr_val[]" placeholder="Enter Attribute Value">
                                </div>
                               
                                </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-info" onclick="addAttr()" style="position: absolute; bottom: 0px;">Add More Attribute</button>
                                </div> -->

                                
                            
                        </div>
                        <!-- <div role="tabpanel" class="tab-pane" id="messages"> 
                            <b>Inventory</b>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Stock<sup>*</sup></label>
                                    <input type="number" class="form-control" name="stock" <?php //echo (!empty($edit)) ? 'value="'.$array[11].'"' : 'placeholder="Enter Stock"'; ?>>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                    <b>Set Sale Qty</b>
                                </div>
                                <div class="col-md-4">
                                    <label>Min Quantity</label>
                                    <input type="number" class="form-control" name="min_qty" <?php //echo (!empty($edit)) ? 'value="'.$array[12].'"' : 'placeholder="Enter Min Sale Qty"'; ?>>
                                </div>

                                <div class="col-md-4">
                                    <label>Max Quantity</label>
                                    <input type="number" class="form-control" name="max_qty" <?php //echo (!empty($edit)) ? 'value="'.$array[13].'"' : 'placeholder="Enter Max Sale Qty"'; ?>>
                                </div>
                            </div>
                        </div> -->
                        <div role="tabpanel" class="tab-pane" id="settings"> <b>Status</b><hr>
                           <div class="row">
                                <div class="col-md-12">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option value="">-- Select Status --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[25] : '';
                                    display_option_selected('color', 1, 1, $value) ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="shipping"> <b>Shipping</b><hr>
                           <div class="row">
                               <?php 
                                $shipping_detail = (!empty($edit)) ? $array[30] : '' ;
                                if(!empty($shipping_detail)){
                                $sh = explode(",", $shipping_detail);
                                }
                               ?>
                                <div class="col-md-4">
                                    <label>Weight (In gm)</label>
                                    <input type="number" class="form-control" name="weight" <?php echo (!empty($shipping_detail)) ? $sh[0] : 'placeholder="Enter Weight"'; ?>>
                                </div>

                                <div class="col-md-4">
                                    <label>Width (In cm)</label>
                                    <input type="number" class="form-control" name="width" <?php echo (!empty($shipping_detail)) ? $sh[1] : 'placeholder="Enter Width"'; ?>>
                                </div>

                                <div class="col-md-4">
                                    <label>Height (In cm)</label>
                                    <input type="number" class="form-control" name="height" <?php echo (!empty($shipping_detail)) ? $sh[2] : 'placeholder="Enter Height"'; ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Example Tab --> 
    <!-- Image Upload Starts -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2><strong>Product</strong> Pictures
                </h2>
                </div>
                <div class="body" style="padding-top: 0px;"> 
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-danger">Note: Image size should be 540x692px.</p>
                        </div>
                        <div class="col-md-6">
                            <label>Product Primary Image<sup>*</sup></label>
                            <input type="file" name="primary_img" id="slide_file" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image('slide_file', 'image_preview1')" <?php echo (!empty($edit)) ? '' : 'required'; ?>>
                            <div id="image_preview1" class="img_preview_block"></div>
                        </div>

                        <div class="col-md-6">
                            <label>Product Secondary Image</label>
                            <input type="file" name="second_img" id="slide_file1" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" onchange="preview_image('slide_file1', 'image_preview2')">
                            <div id="image_preview2" class="img_preview_block"></div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <label>Product Variation Images</label>
                            <input type="file" name="other_img[]" class="form-control" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG, image/WEBP, image/webp" multiple>
                        </div>

                        <?php if(!empty($edit)){ ?>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                </div>

                                
                                <div class="col-md-6">
                                    <p><b>Previous Primary Image</b></p>
                                    <img src="products/<?php echo $array[27] ?>" width="200px">
                                </div>
                                <div class="col-md-6">
                                    <p><b>Previous Secondary Image</b></p>
                                    <img src="products/<?php echo $array[28] ?>" width="200px">
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                    <p><b>Previous Other Images</b></p>
                                    <?php 
                                        if($array[29]!= ""){
                                            $other_images = explode(",", $array[29]);
                                        foreach($other_images as $key => $img){
                                            if($img!= ""){
                                                echo '<img class="image-preview" src="products/'.$img.'">';
                                            }
                                        }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                        <?php if(!empty($edit)){ ?>
                            <div class="col-md-12">
                                <label>Status</label>
                                <select class="form-control" name="visible">
                                    <option value="1" <?php echo ($array[26] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[26] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_product" style="float: right; margin-top: 15px;">UPDATE</button>
                            </div>
                            <?php }else{ ?>

                        <div class="col-md-12">
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_product" style="float: right; margin-top: 15px;">SUBMIT</button>
                        </div>
                            <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Image Upload Ends -->
    </form>

    </div>
</section>


<?php 
include_once"helpers/footer.php";

// save store
if(isset($_POST['add_product'])){
    extract($_POST);
    if(!empty($store) && !empty($title) && !empty($code) && !empty($category) && !empty($description) && !empty($sale_price) && $review!=""){
        $primary_img = $_FILES['primary_img']['name'];
        $secondry_img = $_FILES['second_img']['name'];
        $other = $_FILES['other_img']['name'];
        $image1 = "";
        $image2 = "";
        $images = "";
        $num = 0;
        $attributes = "";
        // $shipping_info = $weight.','.$width.','.$height;
        if(!empty($primary_img)){
           $image1 = fileUpload($primary_img, $_FILES['primary_img']['tmp_name'], 'primary', 'products/', 1);
        }
        if(!empty($secondry_img)){
            $image2 = fileUpload($secondry_img, $_FILES['second_img']['tmp_name'], 'second', 'products/', 1);
         }
         if(!empty($other)){
         foreach($_FILES['other_img']['tmp_name'] as $key => $tmp_name){
            $num = $num + 1;
            $order_unique = 'other'.$num;
            // $images = $images.$_FILES['files']['name'][$key];
            $filename = fileUpload($_FILES['other_img']['name'][$key], $_FILES['other_img']['tmp_name'][$key], $order_unique, 'products/', 1);
            $images = $images.$filename.",";
        }
        }

        // custom attributes starts

            // if($atr!= "" && $atr_val!= ""){
            //     foreach($atr as $key => $attribute){
            //         $attributes = $attributes.','.$attribute.'!'.$atr_val[$key];
            //     }
            // }

        // custom attributes ends
        $a = 1;
        $key = rand_char(5);
        mysqli_autocommit($conn, FALSE);
        try {
        $stmt = $conn->prepare("INSERT INTO `products_db`(`p_key`, `store`, `brand`, `product_name`, `code`, `category`, `subcategory`, `add_cat`, `regular_price`, `price`, `offer`, `offer_start`, `offer_end`, `prf`, `specification`, `description`, `features`, `reviews`, `status`, `visible`, `p_img`, `s_img`, `o_img`, `weight`, `width`, `height`, `create_by`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssssssssssssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $add_cat, $regular_price, $sale_price, $offer, $offer_start_date, $offer_end_date, $prf, $specification, $description, $features, $review, $status, $a, $image1, $image2, $images, $weight, $width, $height, $id, $created_at);
        if ($stmt->execute()) {
        foreach($color as $m => $n){
            $cln = strtoupper($n);
            $se = strtoupper($size[$m]);
            $qy = $qty[$m];
            $ar = strtoupper($atr[$m]);
            $ar_val = strtoupper($atr_val[$m]);
            try {
                $stmt = $conn->prepare("INSERT INTO product_detail (product, color, size, qty, name, value, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("sssssss", $key, $cln, $se, $qy, $ar, $ar_val, $created_at);
                if($stmt->execute()){
                    mysqli_commit($conn);
                }else{
                    mysqli_rollback($conn);
					throw new exception($conn->error);
                }
            }
            catch(Exception $e){
                echo "Error: " . $e->getMessage();
                    
            }

        }
        $exsql = mysqli_query($conn, "SELECT * FROM products_db WHERE p_key='$key'");
        if($exsql){
            if(mysqli_num_rows($exsql) > 0){
                echo "<script>showNotification('alert-info', 'Product Added Successfully', 'top', 'right', '', '')</script>";
            }else{
                echo "<script>showNotification('alert-danger', 'Product Not Added', 'top', 'right', '', '')</script>";
            }
        }
        $stmt->close();
        }else{
            throw new exception($conn->error);
        }
        // $stmt->close();
    }
    catch(Exception $e){
        echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
    }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

// Update Product


if(isset($_POST['update_product'])){
    extract($_POST);
    if(!empty($store) && !empty($title) && !empty($code) && !empty($category) && !empty($subcategory) && !empty($specification) && !empty($description) && !empty($sale_price) && $review!=""){
        // chk Images
        $image1 = "";
        $image2 = "";
        $images = "";
        $num = 0;
        $attributes = "";
        $shipping_info = $weight.','.$width.','.$height;
        if(!empty($_FILES['primary_img']['name'])){
            $image1 = fileUpload($primary_img, $_FILES['primary_img']['tmp_name'], 'primary', 'products/', 1);
        }else{
            $image1 = $array[27];
        }

        if(!empty($_FILES['primary_img']['name'])){
            $image2 = fileUpload($secondry_img, $_FILES['second_img']['tmp_name'], 'second', 'products/', 1);
        }else{
            $image2 = $array[28];
        }

        if(!empty($_FILES['primary_img']['name'])){
            foreach($_FILES['other_img']['tmp_name'] as $key => $tmp_name){
                $num = $num + 1;
                $order_unique = 'other'.$num;
                // $images = $images.$_FILES['files']['name'][$key];
                $filename = fileUpload($_FILES['other_img']['name'][$key], $_FILES['other_img']['tmp_name'][$key], $order_unique, 'products/', 1);
                $images = $images.$filename.",";
            }
        }else{
            $images = $array[29];
        }

        // attributes
        if($atr!= "" && $atr_val!= ""){
            foreach($atr as $key => $attribute){
                $attributes = $attributes.','.$attribute.'!'.$atr_val[$key];
            }
        }

        // excute query

        try {
            $stmt = $conn->prepare("UPDATE `products_db` SET `store`=?,`brand`=?,`product_name`=?,`code`=?,`category`=?,`subcategory`=?,`add_cat`=?,`regular_price`=?,`price`=?,`stock`=?,`min_qty`=?,`max_qty`=?,`offer`=?,`offer_start`=?,`offer_end`=?,`prf`=?,`color`=?,`size`=?,`attribute`=?,`specification`=?,`description`=?,`features`=?,`reviews`=?,`status`=?,`visible`=?,`p_img`=?,`s_img`=?,`o_img`=?,`shipping_info`=? WHERE `p_key` = ? ");
            $stmt->bind_param("ssssssssssssssssssssssssssssss", $store, $brand, $title, $code, $category, $subcategory, $add_cat, $regular_price, $sale_price, $stock, $min_qty, $max_qty, $offer, $offer_start_date, $offer_end_date, $prf, $color, $size, $attributes, $specification, $description, $features, $review, $status, $visible, $image1, $image2, $images, $shipping_info, $product);
            if ($stmt->execute()) {
                echo "<script>showNotification('alert-info', 'Product Updated Successfully', 'top', 'right', '', '')</script>";
                $stmt->close();
                header('location: product-list');
            }else{
                throw new exception($conn->error);
            }
            // $stmt->close();
        }catch(Exception $e){
            echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-danger', 'Product Not Updated ', 'top', 'right', '', '')</script>";
        }



    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}


?>