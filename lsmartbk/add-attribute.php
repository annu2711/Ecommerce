<?php 
include_once "helpers/index.php"; 

$edit = "";
    if(isset($_GET['attributeid'])){
        extract($_GET);
        if(!empty($attributeid)){
        $sql = mysqli_query($conn, "SELECT * FROM attribute_tbl WHERE at_id='$attributeid'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }
?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Attributes
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Attribute Builder</a></li>
                    <li class="breadcrumb-item active">Add Attribute</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-group form-float">
                                <label>Attribute Name</label>
                                <input type="text" class="form-control" name="attribute" <?php echo (!empty($edit)) ? 'value="'.$array[1].'"' : 'placeholder="Enter Attribute"'; ?> required>                              
                            </div>

                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[2] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[2] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_category">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_category">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 
        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Attribute</strong> List </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Attribute</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tboot>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM attribute_tbl");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $rows['name'] ?></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="add-attribute?attributeid=<?php echo $rows['at_id'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once "helpers/footer.php"; 
flash_session_admin();
if(isset($_POST['add_category'])){
    extract($_POST);
    if(!empty($attribute)){
        $chck_brand = check_duplicate('attribute_tbl', 'name', $attribute);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Attrbiute already exists', 'top', 'right', '', '')</script>";
        }else{
            // $key = rand_char(5);
            $attribute = strtoupper($attribute);
            $query = mysqli_query($conn, "INSERT INTO attribute_tbl (name, status, created_at) VALUES ('$attribute', 1, '$created_at')");
            if($query){
                // echo "<script>showNotification('alert-info', 'Attribute Added Successfully', 'top', 'right', '', '')</script>";
                header('location: add-attribute');
                $_SESSION['result'] = [true, 'Attribute Added Successfully', 'success'];
            }else{
                // echo "<script>showNotification('alert-danger', 'Attribute Not Added Successfully', 'top', 'right', '', '')</script>";
                header('location: add-attribute');
                $_SESSION['result'] = [true, 'Attribute Not Added ', 'success'];
            }
        }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

if(isset($_POST['update_category'])){
    extract($_POST);
    if(!empty($attribute)){
        $attribute = strtoupper($attribute);
            $query = mysqli_query($conn, "UPDATE attribute_tbl SET name='$attribute', status='$status' WHERE at_id='$attributeid'");
            if($query){
                // echo "<script>showNotification('alert-info', 'Category Updated Successfully', 'top', 'right', '', '')</script>";
                // header('location: category');
                header('location: add-attribute');
                $_SESSION['result'] = [true, 'Attribute Updated Successfully', 'success'];
            }else{
                // echo "<script>showNotification('alert-danger', 'Category Not Updated Successfully', 'top', 'right', '', '')</script>";
                header('location: add-attribute');
                $_SESSION['result'] = [true, 'Attribute Not Updated ', 'success'];
            }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>