<?php 
    include_once "helpers/index.php";
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Store List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> User Management</a></li>
                    <li class="breadcrumb-item active">Store List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Store Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Address</th>
                                        <th>GST</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT * FROM store");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?php echo $num++ ?></td>
                                            <td><?php echo $rows['store_name'] ?></td>
                                            <td><?php echo $rows['email'] ?></td>
                                            <td><?php echo $rows['mobile'] ?></td>
                                            <td><?php echo $rows['address'] ?></td>
                                            <td><?php echo $rows['gst'] ?></td>
                                            <td><?php status($rows['status']) ?></td>
                                            <td><a href="add_store?store=<?php echo $rows['store_key'] ?>" class="btn btn-primary">Edit</a></td>
                                        </tr>
                                            <?php }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once"helpers/footer.php"; ?>