<?php 
include_once "helpers/index.php";
?>


<section class="content">
    <div class="block-header mt-5">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product Bulk Upload</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="bulk-upload.php">Product Bulk Upload</a></li>
                    <li class="breadcrumb-item active">Creed Product Bulk Upload</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">

<?php
if(isset($_GET['page'])){
    extract($_GET);
    echo paginationMine(98, $page);
}
?>

                                    </div>
                                </div>
                            </div>
                        </div>

            </div>
        </div>
    </div>
</section>


<?php 
include_once "helpers/footer.php";
?>

