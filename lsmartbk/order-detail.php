<?php 
    include_once "helpers/index.php";
    if(isset($_GET['order'])){
        extract($_GET);
        if(!empty($order)){
            $sql = mysqli_query($conn, "SELECT * FROM orders where order_id='$order' ORDER BY order_id DESC LIMIT 1");
            if($sql){
                if(mysqli_num_rows($sql) > 0){
                    $orderarr = mysqli_fetch_array($sql);
                }
            }
        }
    }
    // userAccess(['1', '2'], $deg);
    // $msg = mobilesms('9718190486', 'Hii This is Test msg');

?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Order Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Orders</a></li>
                    <li class="breadcrumb-item active">Order Detail</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12 col-lg-12">
                                <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingOne_1">
                                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="true" aria-controls="collapseOne_1"> 1. Info </a> </h4>
                                        </div>
                                        <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1">
                                            <div class="panel-body">
                                                <table class="table table-bordered table-striped table-hover" id="info-table">
                                                    <thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Order #</td>
                                                            <td><?php echo $order ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Payment Method</td>
                                                            <td><?php echo $orderarr[8] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Payment Status</td>
                                                            <td><?php echo $orderarr[9] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Created On</td>
                                                            <td><?php echo $orderarr[10] ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingTwo_1">
                                            <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseTwo_1" aria-expanded="false"
                                                    aria-controls="collapseTwo_1"> 2. Billing & Shipping </a> </h4>
                                        </div>
                                        <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
                                            <div class="panel-body"> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <p class="mb-0"><strong>Billing Address</strong></p>
                                                    <hr class="mt-1">
                                                        <?php 
                                                            $sql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$orderarr[5]' ORDER BY ab_id DESC LIMIT 1");
                                                            if($sql){
                                                                if(mysqli_num_rows($sql) > 0){
                                                                    $rows = mysqli_fetch_assoc($sql); ?>
                                                            <p><strong><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name'] ?></strong><br>Mo: <?php echo $rows['mobile'] ?><br><?php echo $rows['address']." ".$rows['town']." ".$rows['state']."-".$rows['pincode'] ?></p>
                                                        <?php        }
                                                            }

                                                        ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <p class="mb-0"><strong>Shipping Address</strong></p>
                                                    <hr class="mt-1">
                                                        <?php 
                                                            $sql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$orderarr[6]' ORDER BY ab_id DESC LIMIT 1");
                                                            if($sql){
                                                                if(mysqli_num_rows($sql) > 0){
                                                                    $rows = mysqli_fetch_assoc($sql); ?>
                                                            <p><strong><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name'] ?></strong><br>Mo: <?php echo $rows['mobile'] ?><br><?php echo $rows['address']." ".$rows['town']." ".$rows['state']."-".$rows['pincode'] ?></p>
                                                        <?php        }
                                                            }

                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingThree_1">
                                            <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseThree_1" aria-expanded="false"
                                                    aria-controls="collapseThree_1"> 3. Products </a> </h4>
                                        </div>
                                        <div id="collapseThree_1" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingThree_1">
                                            <div class="panel-body"> 
                                                <table class="table table-bordered table-striped table-hover" id="order-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Picture</th>
                                                            <th>Product Name</th>
                                                            <th>Price</th>
                                                            <th>Quantity</th>
                                                            <th>Discount</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = mysqli_query($conn, "SELECT t1.*, t2.product_name, t2.price, t2.sku FROM order_book as t1 join product as t2 on t1.product=t2.sku WHERE order_key='$orderarr[1]'");
                                                            if($sql){
                                                                if(mysqli_num_rows($sql) > 0){
                                                                    while($rows = mysqli_fetch_assoc($sql)){ ?>
                                                                    <tr>
                                                                        <td align="center"><img src="<?php echo PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $rows['product'], 2); ?>" width="150px" height="150px"></td>
                                                                        <td><?php echo $rows['product_name'] ?><br><strong><i>SKU: </i></strong><?php echo $rows['sku'] ?></td>
                                                                        <td><?php echo $rows['price'] ?></td>
                                                                        <td><?php echo $rows['qty'] ?></td>
                                                                        <td></td>
                                                                        <td><?php echo $rows['price'] * $rows['qty'] ?></td>
                                                                    </tr>
                                                        <?php       }
                                                                }
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once "helpers/footer.php"; ?>