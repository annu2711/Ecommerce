<?php include_once "helpers/index.php"; ?>
<section class="content">
    <div class="block-header mt-5">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product Bulk Management</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Product Management</a></li>
                    <li class="breadcrumb-item active">Product Bulk Upload</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a href="berluti-bulk-upload.php" class="btn btn-primary w-100">Berluti Bulk Upload</a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="ysl-bulk-upload.php" class="btn btn-primary w-100">YSL / Saint Laurent Bulk Upload</a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="tods-bulk-upload.php" class="btn btn-primary w-100">Tod's Bulk Upload</a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="creed-bulk-upload.php" class="btn btn-primary w-100">Creed Bulk Upload</a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="molton-bulk-upload.php" class="btn btn-primary w-100">Molton Bulk Upload</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>

</section>
<?php 

include_once "helpers/footer.php"; 

// excel upload items

if(isset($_POST["upload_excel"]))
{
 $file = $_FILES["file"]["tmp_name"];
 $file_open = fopen($file,"r");
 $success_count = 0;
 $faliure_count = 0;
 $error = "";
 $checklist = "";
 $success_user = "";
 $failure_user = [];
$num = 1;
 while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
 {
  $store = ($csv[0]!= "NA") ? $csv[0] : "";
  $brand = ($csv[1]!= "NA") ? $csv[1] : "";
  $buying_season = ($csv[2]!= "NA") ? $csv[2] : "";
  $hsncode = ($csv[3]!= "NA") ? $csv[3] : "";
  $category = ($csv[4]!= "NA") ? $csv[4] : "";
  $subcategory = ($csv[5]!= "NA") ? $csv[5] : "";
  $collection_tag = ($csv[6]!= "NA") ? $csv[6] : "";
  $vpn = ($csv[7]!= "NA") ? $csv[7] : "";
  $style_no = ($csv[8]!= "NA") ? $csv[8] : "";
  $style_name = ($csv[9]!= "NA") ? $csv[9] : "";
  $color = ($csv[10]!= "NA") ? $csv[10] : "";
  $fit = ($csv[11]!= "NA") ? $csv[11] : "";
  $material = ($csv[12]!= "NA") ? $csv[12] : "";
  $title = ($csv[13]!= "NA") ? $csv[13] : "";
  $size = ($csv[14]!= "NA") ? $csv[14] : "";
  $price = ($csv[15]!= "NA") ? $csv[15] : "";
  $gender = ($csv[16]!= "NA") ? $csv[16] : "";
  $sample = ($csv[17]!= "NA") ? $csv[17] : "";
  $featured_image = ($csv[18]!= "NA") ? $csv[18] : "";
  $other_image_1 = ($csv[19]!= "NA") ? $csv[19] : "";
  $other_image_2 = ($csv[20]!= "NA") ? $csv[20] : "";
  $other_image_3 = ($csv[21]!= "NA") ? $csv[21] : "";
  $other_image_4 = ($csv[22]!= "NA") ? $csv[22] : "";
  $other_image_5 = ($csv[23]!= "NA") ? $csv[23] : "";
  $goupid = ($csv[24]!= "NA") ? $csv[24] : "";
  $scent_type = ($csv[25]!= "NA") ? $csv[25] : "";
  $fragrance_depth = ($csv[26]!= "NA") ? $csv[26] : "";
  $unit = ($csv[27]!= "NA") ? $csv[27] : "";
  $mnf_date = ($csv[28]!= "NA") ? $csv[28] : "";
  $expire_date = ($csv[29]!= "NA") ? $csv[29] : "";
  $imagearray = [$featured_image, $other_image_1, $other_image_2, $other_image_3, $other_image_4, $other_image_5];

    //   get store
  $sql = mysqli_query($conn, "SELECT store_key FROM store where store_name='$store' ORDER BY store_id ASC LIMIT 1");
  if(mysqli_num_rows($sql) > 0){
      $client = mysqli_fetch_assoc($sql);
      $store = $client['store_key'];
  }else{
    $error = $error.mysqli_error($conn);
    $store = "";
  }

    // get brand

  $sql1 = mysqli_query($conn, "SELECT brand_key FROM brands WHERE brand_name='$brand' ORDER BY brand_id ASC LIMIT 1");
  if(mysqli_num_rows($sql1) > 0){
      $c = mysqli_fetch_assoc($sql1);
      $brand = $c['brand_key'];
  }else{
    $error = $error.mysqli_error($conn);
      $brand = "";
  }

    // get category

  $sql2 = mysqli_query($conn, "SELECT cat_key FROM category WHERE category='$category' ORDER BY cat_id ASC LIMIT 1");
  if(mysqli_num_rows($sql2) > 0){
    $p = mysqli_fetch_assoc($sql2);
    $category = $p['cat_key'];
  }else{
      $error = $error.mysqli_error($conn);
      $category = "";
  }

    // get Subcategory

  $sql3 = mysqli_query($conn, "SELECT subcat_key FROM subcategory WHERE subcategory='$subcategory' ORDER BY subcat_id ASC LIMIT 1");
  if(mysqli_num_rows($sql3) > 0){
    $it = mysqli_fetch_assoc($sql3);
    $subcategory = $it['subcat_key'];
    }else{
        $error = $error.mysqli_error($conn);
        $subcategory = "";
    }

// Insert data now
if(!empty($store) && !empty($brand) && !empty($title) && !empty($category) && $price!=""){
// $chkvpn = check_duplicate('berluti', 'vpn', $vpn);
// if($chkvpn == 1){
//     $faliure_count =  $faliure_count + 1;
//     $failure_user[] = $vpn;
// }else{
$type= 0;
$dig = $num + 1;
$code = $vpn;
$groupid = empty($groupid) ? $vpn : $vpn;  
$publish = 1;
$color_code="";
mysqli_autocommit($conn, FALSE);
$key = rand_char(5);
try {
   $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `price`, `publish`, `status`, `type`, `groupid`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
   $stmt->bind_param("sssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $price, $publish, $buying_season, $type, $groupid, $created_at);
   if ($stmt->execute()) {
           
           $stmtb = $conn->prepare("INSERT INTO berluti (p_id, collection_tag, vpn, style_name, style_no, color_code, material_code, fit, hsncode, scent_type, fragrance_depth, mnf_date, expire_date, created_at) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
           $stmtb->bind_param('ssssssssssssss', $code, $collection_tag, $vpn, $style_name, $style_no, $color_code, $material, $fit, $hsncode, $scent_type, $fragrance_depth, $mnf_date, $expire_date, $created_at);

       if($stmtb->execute()){
       try{
           $color  = (!empty($color)) ? strtoupper(str_replace(" ","",$color)) : '' ;
           $size  = (!empty($size)) ? strtoupper($size) : '' ;
           $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, color, size, gender, unit, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
           $stmt2->bind_param('ssssssss', $code, $groupid, $sample, $color, $size, $gender, $unit, $created_at);
           if($stmt2->execute()){
               if(!empty($imagearray)){
               
                   foreach($imagearray as $keey => $tmp_name){
                    if(!empty($tmp_name)){
                    $tmp_name = $tmp_name."-1.png";
                    $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$tmp_name', '$created_at')");
                    if($sql){

                    }else{
                        echo mysqli_error($sql);
                    }
                }
                   }
                }
                $success_count = $success_count + 1;
                   mysqli_commit($conn);
                //    echo "<script>showNotification('alert-success', 'Product Added Successfully', 'top', 'right', '', '')</script>";
           }else{
               mysqli_rollback($conn);
            //    echo mysqli_error($conn);
               throw new exception($conn->error);
           }
       }catch(Exception $e){
           echo "Error: " . $e->getMessage();
            $faliure_count = $faliure_count + 1;
            $failure_user[] = $vpn;
        //    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
       }
   }else{
       mysqli_rollback($conn);
       echo mysqli_error($conn);
    //    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
   }
   }else{
       echo mysqli_error($conn);
       throw new exception($conn->error);
   }
}catch(Exception $e){
   echo "Error: " . $e->getMessage();
//    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
}


// }
 }else{
     $faliure_count = $faliure_count + 1;
     $failure_user[] = $vpn;
     echo $store."=".$brand."=".$title."=".$category."=".$subcategory."=".$price;
 }

 }
 echo "Data =>".$store."=".$brand."=".$title."=".$category."=".$subcategory."=".$price;
 echo "<script>showNotification('alert-success', '".$success_count." Product Added Successfully', 'top', 'right', '', '')</script>";
 echo "<script>showNotification('alert-danger', '".$faliure_count." Product Not Added Successfully', 'top', 'right', '', '')</script>";
 print_r($failure_user);
 
}


?>