﻿<?php
include_once "helpers/index.php";
?>

<!-- Main Content -->
<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Bquest Dashboard
                <small class="text-muted">Welcome to Bquest</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <div class="header">
                        <h2><strong>Total</strong> Orders</h2>                        
                    </div>
                    <div class="body">
                        <h3 class="m-b-0">
                        <?php 
                        $sql = mysqli_query($conn, "SELECT count(t1.order_id) as qty FROM orders as t1 join clients as t2 on t1.user=t2.client_key WHERE d_status > 0 AND payment_status NOT IN ('Failure', 'Aborted')");
                        if($sql){
                            if(mysqli_num_rows($sql) > 0){
                                $row = mysqli_fetch_assoc($sql);
                                echo $row['qty'];
                            }else{
                                echo 0;
                            }
                        }
                    ?>
                    </h3>
                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(63, 81, 181)" data-highlight-Line-Color="#222"
                        data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(63, 81, 181)" data-spot-Color="rgb(63, 81, 181, 0.7)"
                        data-offset="90" data-width="100%" data-height="60px" data-line-Width="1" data-line-Color="rgb(63, 81, 181, 0.7)"
                        data-fill-Color="rgba(221,94,137, 0.2)"> 1,2,3,1,4,3,6,4,4,1 </div>                        
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
            <div class="card">
                    <div class="header">
                        <h2><strong>Total</strong> Customers</h2>                        
                    </div>
                    <div class="body">
                        <h3 class="m-b-0"><?php echo tableCount('clients', 'client_id'); ?></h3>
                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(63, 81, 181)" data-highlight-Line-Color="#222"
                        data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(120, 184, 62)" data-spot-Color="rgb(63, 81, 181, 0.7)"
                        data-offset="90" data-width="100%" data-height="60px" data-line-Width="1" data-line-Color="rgb(63, 81, 181, 0.7)"
                        data-fill-Color="rgba(128,133,233, 0.2)"> 4,5,2,8,4,8,7,4,8,5</div>
                    </div>
                </div>
            </div>         
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <div class="header">
                        <h2><strong>Total</strong> Sold Products</h2>                        
                    </div>
                    <div class="body">
                        <h3 class="m-b-0">
                        <?php 
                                $sql = mysqli_query($conn, "SELECT SUM(t1.qty) as qty FROM order_book as t1 join orders as t2 on t1.order_key = t2.order_key WHERE t2.d_status > 0 AND t2.payment_status NOT IN ('Failure', 'Aborted')");
                                if($sql){
                                    if(mysqli_num_rows($sql) > 0){
                                        $row = mysqli_fetch_assoc($sql);
                                        echo $row['qty'];
                                    }else{
                                        echo 0;
                                    }
                                }
                            ?>
                        </h3>

                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(63, 81, 181)" data-highlight-Line-Color="#222"
                        data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(63, 81, 181)" data-spot-Color="rgb(63, 81, 181, 0.7)"
                        data-offset="90" data-width="100%" data-height="60px" data-line-Width="1" data-line-Color="rgb(63, 81, 181, 0.7)"
                        data-fill-Color="rgba(221,94,137, 0.2)"> 1,2,3,1,4,3,6,4,4,1 </div>                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            <div class="card">
                    <div class="header">
                        <h2><strong>Total</strong> Products</h2>                        
                    </div>
                    <div class="body">
                        <h3 class="m-b-0"><?php echo tableCount('product', 'p_id'); ?></h3>
                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(63, 81, 181)" data-highlight-Line-Color="#222"
                        data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(120, 184, 62)" data-spot-Color="rgb(63, 81, 181, 0.7)"
                        data-offset="90" data-width="100%" data-height="60px" data-line-Width="1" data-line-Color="rgb(63, 81, 181, 0.7)"
                        data-fill-Color="rgba(128,133,233, 0.2)"> 4,5,2,8,4,8,7,4,8,5</div>
                    </div>
                </div>
            </div>      
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Latest</strong> 10 Orders</h2>
                    </div>
                    <div class="body">
                    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                        <th>Date</th>
                                        <th>Order #</th>
                                        <th>Customer</th>
                                        <th>Payment</th>
                                        <th>Amount</th>
                                        <th>Shiping Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(empty($edit)){
                                        $query = mysqli_query($conn, "SELECT t1.order_id, t1.order_key, t1.amount, t1.payment, t1.payment_status, t1.d_status, t1.created_at, t2.title, t2.name, t2.last_name FROM orders as t1 join clients as t2 on t1.user=t2.client_id WHERE d_status > 0 AND t1.payment_status NOT IN ('Failure', 'Aborted') ORDER BY t1.order_id DESC LIMIT 10");
                                        if($query){
                                            if(mysqli_num_rows($query) > 0){
                                                $num = 1;
                                                while($rows = mysqli_fetch_array($query)){
                                                    $dataarr[] = $rows;
                                                }
                                            }
                                        }
                                    }

                                                if(!empty($dataarr)){
                                                    for($i = 0; $i < count($dataarr); $i++){
                                                if($dataarr[$i][3] === 'paynow' && $dataarr[$i][2] > 0){
                                                        if($dataarr[$i][4] != 'Uncomplete'){ ?>
                                                            <tr>
                                                                <td><?php echo $dataarr[$i][6] ?></td>
                                                                <td><?php 
                                                               echo $dataarr[$i][1]."<br>";
                                                                $shipping_id= getSinglevalue('order_shipping', 'order_id', $dataarr[$i][1], 2);
                                                                $sh = (!empty($shipping_id)) ? $shipping_id : 'NA';
                                                                echo "<b>Ship ID:</b>".$sh;
                                                                
                                                                ?></td>
                                                                <td><?php echo $dataarr[$i][7]." ".$dataarr[$i][8]." ".$dataarr[$i][9] ?>
                                                                
                                                                
                                                                </td>
                                                                <td><?php echo $dataarr[$i][3] ?></td>
                                                                <td><?php echo $dataarr[$i][2] ?></td>
                                                                <td><?php 
                                                                if($dataarr[$i][5] == '0'){
                                                                    echo '<p class="text-danger">Cancelled</p>';
                                                                }else{
                                                                    echo orderStatus($dataarr[$i][5]);
                                                                }
                                                                ?></td>
                                                                <td>
                                                                <a href="order-detail.php?order=<?php echo $dataarr[$i][0] ?>" class="btn btn-primary waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="View Invoice Details"><i class="material-icons">remove_red_eye</i></a>
                                                                <a href="../inc/pdfmaker.php?invoice=<?php echo $dataarr[$i][1] ?>" class="btn btn-info waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="Download Invoice"><i class="material-icons">picture_in_picture_alt</i></a>
                                                                <?php if($dataarr[$i][5] == '1'){ ?>
                                                                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="mb-0">
                                                                    <input type="hidden" name="shipid" value="<?php echo getSinglevalue('order_shipping', 'order_id', $dataarr[$i][1], 2) ?>">
                                                                    <input type="hidden" name="order_id" value="<?php echo $dataarr[$i][0] ?>">
                                                                    <input type="hidden" name="order_key" value="<?php echo $dataarr[$i][1] ?>">
                                                                    <button type="submit" class="btn btn-danger w-100" name="cancel_order">Cancel</button>
                                                                </form>
                                                                <?php } ?>
                                                                <?php if(empty($shipping_id)){ ?>
                                                                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="mb-0">
                                                                    <input type="hidden" name="orderid" value="<?php echo $dataarr[$i][1] ?>">
                                                                    <button type="submit" class="btn btn-danger w-100" name="book_order">Book</button>
                                                                </form>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                           <?php   
                                                            }
                                                        }else{
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $dataarr[$i][6] ?></td>
                                                                <td><?php 
                                                                echo $dataarr[$i][1]."<br>";
                                                                $shipping_id= getSinglevalue('order_shipping', 'order_id', $dataarr[$i][1], 2);
                                                               $sh = (!empty($shipping_id)) ? $shipping_id : 'NA';
                                                                echo "<b>Ship ID:</b>".$sh;
                                                                ?></td>
                                                                <td><?php echo $dataarr[$i][7]." ".$dataarr[$i][8]." ".$dataarr[$i][9] ?></td>
                                                                <td><?php echo $dataarr[$i][3] ?></td>
                                                                <td><?php echo $dataarr[$i][2] ?></td>
                                                                <td><?php 
                                                                if($dataarr[$i][5] == '0'){
                                                                    echo '<p class="text-danger">Cancelled</p>';
                                                                }else{
                                                                    echo orderStatus($dataarr[$i][5]);
                                                                }
                                                                ?></td>
                                                                <td>
                                                                <a href="order-detail.php?order=<?php echo $dataarr[$i][0] ?>" class="btn btn-primary waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="View Invoice Details"><i class="material-icons">remove_red_eye</i></a>
                                                                <a href="../inc/pdfmaker.php?invoice=<?php echo $dataarr[$i][1] ?>" class="btn btn-info waves-effect" style="padding: 3px 7px;" data-toggle="tooltip" title="Download Invoice"><i class="material-icons">picture_in_picture_alt</i></a>
                                                                <?php if($dataarr[$i][5] == '1'){ ?>
                                                                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="mb-0">
                                                                    <input type="hidden" name="shipid" value="<?php echo getSinglevalue('order_shipping', 'order_id', $dataarr[$i][1], 2) ?>">
                                                                    <input type="hidden" name="order_id" value="<?php echo $dataarr[$i][0] ?>">
                                                                    <input type="hidden" name="order_key" value="<?php echo $dataarr[$i][1] ?>">
                                                                    <button type="submit" class="btn btn-danger w-100" name="cancel_order">Cancel</button>
                                                                </form>
                                                                <?php } ?>

                                                                <?php if(empty($shipping_id)){ ?>
                                                                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="mb-0">
                                                                    <input type="hidden" name="orderid" value="<?php echo $dataarr[$i][1] ?>">
                                                                    <button type="submit" class="btn btn-danger w-100" name="book_order">Book</button>
                                                                </form>
                                                                <?php } ?>
                                                                </td>
                                                    </tr>
                                                <?php    }
                                                }
                                            }
                                                ?> 
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2><strong>Top 10</strong> Most Earning Days</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Total Orders</th>
                                        <th>Amount</th>
                                    </tr>
                            </thead>
                            <tbody>
                            <?php 
													$sql = mysqli_query($conn, "SELECT DATE(created_at) AS date, COUNT(order_id) AS total_sales, SUM(amount) as amount FROM orders WHERE d_status > 0 AND payment_status NOT IN ('Aborted', 'Failure') GROUP BY DATE(created_at) ORDER BY amount DESC LIMIT 10");
													if($sql){
														if(mysqli_num_rows($sql) > 0){
															while($row = mysqli_fetch_array($sql)){ ?>
																<tr>
																	<td><?php echo $row['date'] ?></td>
																	<td><?php echo $row['total_sales'] ?></td>
																	<td><?php echo $row['amount'] ?></td>
																<tr>
												<?php		}
														}
													}
												?>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2><strong>Top 10 </strong>Trending Products</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
										<th>Image</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                            $sql = mysqli_query($conn, "SELECT SUM(t1.qty) as qty, t1.product FROM `order_book` as t1 join orders as t2 on t1.order_key=t2.order_key WHERE t2.d_status > 0 AND t2.payment_status NOT IN ('Aborted', 'Failure') GROUP BY t1.product ORDER BY qty DESC LIMIT 10");
                                            if($sql){
                                                if(mysqli_num_rows($sql) > 0){
                                                    while($row = mysqli_fetch_array($sql)){ 
                                                        ?>
                                                        <tr>
                                                            <td><img src="<?php echo PRODUCT_DIRECTORY.getSingleValue('product_images', 'product', $row['product'], 2); ?>" width="50px"></td>
                                                            <td><?php echo getSingleValue('product', 'sku', $row['product'], 4); ?></td>
                                                            <td><?php echo $row['qty'] ?></td>
                                                        <tr>
                                                        <?php		
                                                    }
                                                }
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- SECTION END -->

<?php include_once "helpers/footer.php"; ?>