<?php 
    include_once"helpers/index.php";
    $edit = "";
    if(isset($_GET['brand'])){
        extract($_GET);
        if(!empty($brand)){
        $brand_key = $_GET['brand'];
        $sql = mysqli_query($conn, "SELECT * FROM brand_profile WHERE brand_key='$brand_key'");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $array = mysqli_fetch_array($sql);
                $edit = 1;
                
            }
        }
        }
    }

?>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Brand</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active">Brand Profile</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group form-float">
                                <label>Store Name<sup classs="text-danger">*</sup></label>
                                <select class="form-control" name="store" id="store" required>
                                    <option value="">-- Select Store --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? getSinglevalue('brands', 'brand_key', $array[1], 2) : '';
                                    display_option_selected('store', 1, 2, $value) ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <div id="brand">
                                <label>Brand Name</label>
                                <select class="form-control" name="brand" style="padding: 0px !important;">
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[1] : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>  
                                </div>                         
                            </div>
                            <!-- <div class="form-group form-float">
                                <label>Brand Title</label>
                                <input type="text" class="form-control" name="title" <?php //echo (!empty($edit)) ? 'value="'.$array[2].'"' : 'placeholder="Enter Brand Name"'; ?>>
                            </div> -->
                            <div class="form-group form-float">
                                <label>Brand Description</label>
                                <textarea class="form-control editor" name="subtitle" required>
                                <?php echo (!empty($edit)) ? $array[3] : ''; ?>
                                </textarea>
                            </div>
                            <div class="form-group form_float">
                                <label>Background Image</label>
                                <input type="file" class="form-control" name="file" id="slide_file" accept="image/png, image/jpeg, image/JPEG, image/jpg, image/JPG, image/PNG" onchange="preview_image('slide_file', 'image_preview');" <?php echo (!empty($edit)) ? '' : 'required'; ?>>
                                <div class="row">
                                    <div class="col-md-6">
                                    <p><b>New Uploaded Image</b></p>
                                        <div id="image_preview"></div>
                                    </div>
                                    <?php if(!empty($edit)){ ?>
                                    <div class="col-md-6">
                                        <p><b>Previous Image</b></p>
                                        <img src="website_images/<?php echo $array[4] ?>" width="500px">
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php if(!empty($edit)){ ?>
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php echo ($array[5] == 1) ? 'selected="selected"' : '' ;?>>Active</option>
                                    <option value="0" <?php echo ($array[5] == 0) ? 'selected="selected"' : '' ;?>>Inactive</option>
                                </select>
                            </div>

                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="update_brand">UPDATE</button>

                            <?php }else{ ?>
                            <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit" name="add_brand">SUBMIT</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Validation --> 

        <!-- Table Starts -->

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Brands</strong> List </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Brand</th>
                                        <th>Subtitle</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $query = mysqli_query($conn, "SELECT * FROM brand_profile");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo getSinglevalue('brands', 'brand_key', $rows['brand_key'], 3) ?></td>
                                        <td><?php echo $rows['subtitle'] ?></td>
                                        <td><img src="<?php echo WEBSITE_DIRECTORY.$rows['image'] ?>" width="300px"></td>
                                        <td><?php status($rows['status']) ?></td>
                                        <td>
                                        <a href="brand-profile?brand=<?php echo $rows['brand_key'] ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                <?php        }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table Ends -->
    </div>
</section>

<?php 
include_once "helpers/footer.php"; 

if(isset($_POST['add_brand'])){
    extract($_POST);
    if(!empty($store) && !empty($brand)){
        $chck_brand = check_duplicate('brand_profile', 'brand_key', $brand);
        if($chck_brand == 1){
            echo "<script>showNotification('alert-warning', 'Brand already exists', 'top', 'right', '', '')</script>";
        }else{
            $key = rand_char(5);
            $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], 'brandpic', WEBSITE_DIRECTORY);
            $status = 1;
            try {
                $stmt = $conn->prepare("INSERT INTO `brand_profile`(`brand_key`, `subtitle`, `image`, `status`, `created_at`) VALUES (?, ?, ?, ?, ?)");
                $stmt->bind_param("sssss", $brand, $subtitle, $image1, $status, $created_at);
                if ($stmt->execute()) {
                    echo "<script>showNotification('alert-info', 'Brand Added Successfully', 'top', 'right', '', '')</script>";
                    header('refresh: 2');
                }else{
                    echo $conn->error;
                    throw new exception($conn->error);
                }
                $stmt->close();
            }
            catch(Exception $e){
                echo "<script>showNotification('alert-danger', 'Brand Not Added ', 'top', 'right', '', '')</script>";
            
        }
    }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}



if(isset($_POST['update_brand'])){
    extract($_POST);
    if(!empty($store) && !empty($brand)){
        $image1 = "";
        if(!empty($_FILES['file']['name'])){
            $image1 = fileUpload($_FILES['file']['name'], $_FILES['file']['tmp_name'], 'slider',  WEBSITE_DIRECTORY);
        }else{
            $image1 = $array[4];
        }
       
        try {
            $stmt = $conn->prepare("UPDATE `brand_profile` SET `brand_key`=?,`subtitle`=?,`image`=?,`status`=?  WHERE brand_key=?");
            $stmt->bind_param("sssss", $brand, $subtitle, $image1, $status, $brand_key);
          
            if ($stmt->execute()) {
            echo "<script>showNotification('alert-success', 'Brand Updated Successfully', 'top', 'right', '', '')</script>";
            // header('location: brand-profile');
            }else{
                throw new exception($conn->error);
            }
            $stmt->close();
        }
        catch(Exception $e){
                echo "<script>showNotification('alert-danger', 'Brand Not Updated ', 'top', 'right', '', '')</script>";
        }



    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

?>