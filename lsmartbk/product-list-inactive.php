<?php 
    include_once"helpers/index.php";
?>

<section class="content ecommerce-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Product List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Products Management</a></li>
                    <li class="breadcrumb-item active">Product List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product SKU</th>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $query = mysqli_query($conn, "SELECT p_id, product_name, sku, category, subcategory, mrp, price, status, publish FROM product WHERE publish=0 ORDER BY p_id DESC");
                                        if($query){
                                        if(mysqli_num_rows($query) > 0){
                                            $num = 1;
                                            while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><img src="<?php echo PRODUCT_DIRECTORY.getSinglevalue('product_images', 'product', $rows['sku'], 2) ?>" width="50px" height="50px"></td>
                                            <td><?php echo $rows['sku'] ?></td>
                                            <td><?php echo $rows['product_name'] ?></td>
                                            <td><?php echo getSinglevalue('category', 'cat_key', $rows['category'], 2).' >> '.getSinglevalue('subcategory', 'subcat_key', $rows['subcategory'], 3) ?></td>
                                            <td><strike class="text-danger"><?php echo $rows['mrp'] ?></strike> <?php echo $rows['price'] ?></td>
                                            
                                            <td><?php status($rows['publish']) ?></td>
                                            <td>
                                            <a href="product?product=<?php echo $rows['p_id'] ?>" class="btn btn-primary">Edit</a>
                                            <a href="add-stock?product=<?php echo $rows['p_id'] ?>" class="btn btn-success">Add Stock</a>
                                            </td>
                                        </tr>
                                    <?php   }
                                        }
                                    }else{
                                        echo mysqli_error($conn);
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<?php include_once"helpers/footer.php"; ?>