<?php include_once "helpers/index.php"; ?>

	<div class="brandDirectory">
				<div class="container">
					<div class="heading w-100 mt-2 mt-md-6 mt-lg-5">
						<h2>Brand Directory</h2>
					</div>
			

				</div>
	</div>
<section class="brandName pb-5 pt-0 pt-lg-5 ">
	<div class="container">
		<div class="row">
			<div id="alphabetA" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img1.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Alexander McQueen__">Berluti</a>
			</div>
		</div>
		<div id="alphabetB" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img2.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Balmain__">Saint Laurent</a>
			</div>
			
		</div>
		<div id="alphabetC" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img3.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Chole__">Tod's</a>
			</div>			
		</div>
		<div id="alphabetC" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img4.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Chole__">Brunello Cucinelli</a>
			</div>			
		</div>
		<div id="alphabetC" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img5.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Chole__">Molton Brown</a>
			</div>			
		</div>
		<div id="alphabetC" class="mainBrandDirectory">
			<div class="brandDirectoryInn8">
				<img src="img/logo/img6.png" alt="">
			</div>
			<div class="brandDirectoryInn23">
				<a href="brand?brand=Chole__">Creed</a>
			</div>			
		</div>
		</div>		
	</div>
</section>



<?php include_once"helpers/footer.php"; ?>