<?php include_once"helpers/index.php"; ?>
<script src="js/jssor.slider-28.0.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.jssor_1_slider_init = function() {

            var jssor_1_SlideoTransitions = [
              [{b:0,d:1000,o:1}],
              [{b:0,d:1000,y:-88,ls:0.1,e:{y:3,ls:1}}],
              [{b:-1,d:1,da:[0,2000]},{b:600,d:1500,da:[460,2000],e:{da:1}}],
              [{b:1600,d:500,o:0.4}],
              [{b:1500,d:500,o:0.4}],
              [{b:1400,d:500,o:0.7}],
              [{b:600,d:1000,o:1}],
              [{b:-1,d:1,tZ:-100},{b:600,d:1000,x:668,y:302,rY:12,tZ:-30,e:{x:1,y:1,rY:1,tZ:1}}],
              [{b:-1,d:1,tZ:-20}],
              [{b:-1,d:1,tZ:20}],
              [{b:-1,d:1,da:[460,2000]},{b:1200,d:1500,da:[760,2000],e:{da:1}}],
              [{b:1200,d:500,o:0.4}],
              [{b:1100,d:500,o:0.4}],
              [{b:1000,d:500,o:0.7}],
              [{b:0,d:1500,y:60,o:1,e:{y:1}}],
              [{b:-1,d:1,ls:1},{b:0,d:1500,o:1,ls:0,e:{ls:1}}],
              [{b:-1,d:1,da:[760,2000]},{b:1600,d:1500,da:[1040,2000],e:{da:1}}],
              [{b:1600,d:500,o:0.4}],
              [{b:1500,d:500,o:0.4}],
              [{b:1400,d:500,o:0.7}],
              [{b:-1,d:1,so:1},{b:0,d:1000,so:0,e:{so:1}}],
              [{b:-1,d:1,rY:-20}],
              [{b:-1,d:1,so:1},{b:1000,d:1000,so:0,e:{so:1}}],
              [{b:-1,d:1,rY:-20}],
              [{b:-1,d:1,ls:2},{b:0,d:2000,y:68,o:0.7,ls:0.12,e:{y:7,ls:1}}],
              [{b:-1,d:1,ls:2},{b:0,d:2000,y:68,o:0.7,ls:0.12,e:{y:7,ls:1}}],
              [{b:1100,d:1200,y:-40,o:1},{b:2300,d:1200,y:-80,o:0}],
              [{b:1700,d:1200,y:-40,o:1},{b:2900,d:1200,y:-80,o:0}],
              [{b:2300,d:1200,y:-40,o:1},{b:3500,d:1200,y:-80,o:0}],
              [{b:2900,d:1200,y:-40,o:1},{b:4100,d:1200,y:-80,o:0}],
              [{b:3500,d:1200,y:-40,o:1},{b:4700,d:1200,y:-80,o:0}],
              [{b:4100,d:1200,y:-40,o:1},{b:5300,d:1200,y:-80,o:0}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions,
                $Controls: [{r:1100,e:4700},{r:1700,e:5300},{r:2300,e:5900},{r:2900,e:6500},{r:3500,e:7100},{r:4100,e:7700}]
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$,
                $SpacingX: 20,
                $SpacingY: 20
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 2500;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin double-tail-spin css*/
        .jssorl-004-double-tail-spin img {
            animation-name: jssorl-004-double-tail-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-004-double-tail-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 071 css*/
        .jssorb071 .i {position:absolute;color:#fff;font-family:"Helvetica neue",Helvetica,Arial,sans-serif;text-align:center;cursor:pointer;z-index:0;}
        .jssorb071 .i .b {fill:#000;opacity:.2;}
        .jssorb071 .i:hover {opacity:.7;}
        .jssorb071 .iav {color:#000;}
        .jssorb071 .iav .b {fill:#fff;opacity:1;}
        .jssorb071 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
	

	<?php 
	$sql = mysqli_query($conn, "SELECT * FROM shop_by_cat WHERE type=0 AND status=1 ORDER BY sb_id DESC LIMIT 4");
	if($sql){
		if(mysqli_num_rows($sql) > 0){ ?>
<section class="my-4">
	<div class="container px-auto px-md-0">
		<div class="row">
		<?php while($rows = mysqli_fetch_assoc($sql)){ ?>
			<div class="col-lg-3 col-6 mt-2">
				<div class="productImg">
					<div class="productImgInn">
				    		<a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>">
				    			<img src="<?php echo WEBSITE_IMAGE_URL.$rows['img'] ?>" alt="" class="carousel-cover">
				    		</a>
				    		<div class="carousel-content mt-3"> 
					        	<p><a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>"><?php echo $rows['title'] ?> ></a></p> 
					    	</div>
				    	</div>
				    	
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } } ?>

	


<?php 
    $query = mysqli_query($conn, "SELECT * FROM home_slider WHERE status=1 AND block_type='top' ORDER BY slider_id DESC");
    if($query){
		if(mysqli_num_rows($query) > 0){ 
			$num = 0;
			$sliderarr = [];
			while($rows = mysqli_fetch_array($query)){
				$sliderarr[] = $rows;
			}

		}
	}
	// print_r($sliderarr);
			?>
<section class="d-none d-md-block">
<div class="container px-0">
<div id="jssor_1"  style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:382px;overflow:hidden;visibility:hidden;">
        
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:382px;overflow:hidden;">
		<?php for($i = 0; $i <= count($sliderarr) - 1; $i++){ ?>
            <div class="slidesperimg">
                <img data-u="image" src="<?php echo WEBSITE_IMAGE_URL.$sliderarr[$i][4]; ?>" />
            </div>
		<?php } ?>
        </div>
       
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
</div>
</section>
<script type="text/javascript">jssor_1_slider_init();</script>


<!--slider on mobile  -->
<section class="carouselCaption d-block d-md-none">
	<div class="container">
		<div class="row">
			<div id="carouselExampleControls" class="carousel carousel-fade w-100" data-ride="carousel">
			  <div class="carousel-inner">
			  	<?php for($i = 0; $i <= count($sliderarr) - 1; $i++){ ?>
				 <div class="carousel-item active HomeBanner" style="background-image: url(<?php echo WEBSITE_IMAGE_URL.$sliderarr[$i][4]; ?>)">										
			    </div>
				<?php } ?>
			  </div>
			  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon customArrow" aria-hidden="true">
			    	<img src="img/arrow/leftArrow.svg">
			    </span>
			    <!-- <span class="sr-only">Previous</span> -->
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			    <span class="carousel-control-next-icon customArrow" aria-hidden="true">
			    	<img src="img/arrow/rightArrow.svg">
			    </span>
			    <!-- <span class="sr-only">Next</span> -->
			  </a>
			</div>
		</div>
	</div>
</section>
<?php 
	$sql = mysqli_query($conn, "SELECT * FROM shop_by_cat WHERE type=1 AND status=1 ORDER BY sb_id DESC");
	if($sql){
		if(mysqli_num_rows($sql) > 0){ ?>

<!-- <section class="buyerSection my-4">
	<div class="container px-0">
		<div class="heading w-100 text-center">
			<h2>Our Buyers' picks</h2>
		</div>
		<div class="row justify-content-center">
				<div class="buyer col-sm-12 col-12">
					<div class="owl-carousel owl-theme">
					<?php while($rows = mysqli_fetch_assoc($sql)){ ?>
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>">
				    			<img src="<?php echo WEBSITE_IMAGE_URL.$rows['img'] ?>" alt="" class="carousel-item">
				    		</a>
				    	</div>
				    	<div class="carousel-content"> 
					        <p><a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>"><?php echo $rows['title'] ?></a></p> 
					    </div>
					</div>
					<?php } ?>
				</div>
				</div>
		</div>
	</div>
</section>
<?php } } ?> -->


<!-- <?php 
    $query = mysqli_query($conn, "SELECT * FROM home_slider WHERE status=1 AND block_type='middle' ORDER BY slider_id DESC LIMIT 1");
    if($query){
		if(mysqli_num_rows($query) > 0){
			$slider = mysqli_fetch_assoc($query); ?>
<section class="container px-0 my-4">
	<div class="bannerMid" style="background: url('<?php echo WEBSITE_IMAGE_URL.$slider['image'] ?>')">
		<div class="row">
			<div class="bannerMidCont"> 
			 <h2><strong><?php echo $slider['title'] ?></strong></h2> 
			 <h4><?php echo (!empty($slider['subtitle'])) ? $slider['subtitle'] : ''; ?></h4> 
	         <p><a class="button" href="<?php echo (!empty($slider['link'])) ? $slider['link'] : '#'; ?>">Shop now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p> 
	        </div>
		</div>
	</div>
</section>
<?php } } ?> -->









<!-- <?php 
    $query = mysqli_query($conn, "SELECT * FROM home_slider WHERE status=1 AND block_type='bottom' ORDER BY slider_id DESC LIMIT 1");
    if($query){
		if(mysqli_num_rows($query) > 0){
			$slider = mysqli_fetch_assoc($query); ?>
<section class="container px-0 my-4">
	<div class="bannerMid" style="background: url('<?php echo WEBSITE_IMAGE_URL.$slider['image'] ?>')">
		<div class="row">
			<div class="bannerMidCont"> 
			 <h2><strong><?php echo $slider['title'] ?></strong></h2> 
			 <p><?php echo (!empty($slider['subtitle'])) ? $slider['subtitle'] : ''; ?></p> 
	         <p><a class="button" href="<?php echo (!empty($slider['link'])) ? $slider['link'] : '#'; ?>">Shop now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p> 
	        </div>
		</div>
	</div>
</section>
<?php } } ?> -->


<section class="trendingStories my-4">
	<div class="container px-auto px-md-0">
		<div class="heading w-100 text-center">
			<h2>Trending stories</h2>
		</div>
		<div class="row justify-content-center">
				<div class="col-12">
					<div class="owl-carousel owl-theme">
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
								<img src="img/home/Section-6/img2.png" alt="" class="carousel-item">
				    		</a>
				    	</div>

				    	<div class="carousel-content"> 
				    		<p class="mt-3 mb-0"><strong>INTRODUCING: THE VAMPIRE'S WIFE EXCLUSIVE-TO-LSMART COLLECTION</strong></p>
					        <p class="mb-3"><a href="javascript:void(0)">Read more ></a></p> 
					    </div>
				    </div>
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
				    			<img src="img/home/Section-6/img2.png" alt="" class="carousel-item">
				    		</a>
				    	</div>
				    	<div class="carousel-content">
					        <p class="mt-3 mb-0"><strong>TAKE 5: SPRING DRESSES WE LOVE</strong></p>
					        <p class="mb-3"><a href="javascript:void(0)">Read more ></a></p>  
					    </div>
				    </div>
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
				    			<img src="img/home/Section-6/img3.png" alt="" class="carousel-item">
				    		</a>
				    	</div>
				    	<div class="carousel-content"> 
				    		<p class="mt-3 mb-0"><strong>DESIGNER SPOTLIGHT: A-COLD-WALL* FOUNDER SAMUEL ROSS</strong></p>
					        <p class="mb-3"><a href="javascript:void(0)">Read more ></a></p> 
					    </div>
				    </div>
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
				    			<img src="img/home/Section-6/img4.png" alt="" class="carousel-item">
				    		</a>
				    	</div>
				    	<div class="carousel-content">
					        <p class="mt-3 mb-0"><strong>OUR BEAUTY EDITOR PICKS THE TOP 5 SCENTS OF SPRING</strong></p>
					        <p class="mb-3"><a href="javascript:void(0)">Read more ></a></p>  
					    </div>
				    </div>
				</div>
				</div>
		</div>
	</div>
</section>

<section class="mb-5 d-none d-md-block">
	<div class="container px-auto px-md-0 bussiness pt-5 pb-4 ">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-md-10 text-center">
				<h5><strong>Business as usual at Bquest</strong></h5>
				<div class="row justify-content-center mt-4">
					<div class="col-md-12">
					<div class="row">
					<div class="col-md-6">
						<div class="imgDivN">
						  <img src="img/emperio/img1.png" alt="">
						</div>
					</div>
					<div class="col-md-6 mt-3 mt-md-0">
						<div class="imgDivN">
						  <img src="img/emperio/img2.png" alt="">
						</div>
					</div>
					<div class="col-12">
					<div class="row bquestHeading my-5">
							<div class="col-md-3 col-sm-6 col-12 mb-4 mb-md-0 text-center">
								<div class="bquestImg">
									<img src="img/icons/virtual-shopping.png" alt="">
								</div>
								<h6>Virtual<br> Shopping</h6>
							</div>
							<div class="col-md-3 col-sm-6 col-12 mb-4 mb-md-0 text-center">
								<div class="bquestImg">
									<img src="img/icons/pick-up.png" alt="">
								</div>
								<h6>Pick up<br> at porch</h6>
							</div>
							<div class="col-md-3 col-sm-6 col-12 mb-4 mb-md-0 text-center">
								<div class="bquestImg">
									<img src="img/icons/one-to-one.png" alt="">
								</div>
								<h6>One - One<br>Appointment</h6>
							</div>
							<div class="col-md-3 col-sm-6 col-12 mb-4 mb-md-0 text-center">
								<div class="bquestImg">
									<img src="img/icons/home.png" alt="">
								</div>
								<h6>Home<br>Shopping</h6>
							</div>
						</div>
					</div>
					</div>
						
					</div>
				</div>
				
					</div>
		</div>
	</div>
</section>
<section class="mb-5 d-block d-md-none">
	<div class="container px-auto px-md-0 bussiness pt-5 pb-4 ">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-md-10 text-center">
				<h5><strong>Business as usual at Bquest</strong></h5>
				<div class="row justify-content-center mt-4">
					<div class="col-md-12">
					<div class="row">
					<div class="col-md-12">
						<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="imgDivN">
										<img src="img/emperio/img1.png" alt="">
									</div>
								</div>
								<div class="carousel-item">
								<div class="imgDivN">
									<img src="img/emperio/img2.png" alt="">
								</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-12">
						<div class="row bquestHeading my-5">
								<div class="col-md-3 col-6 mb-4 mb-md-0 text-center">
									<div class="bquestImg">
										<img src="img/icons/virtual-shopping.png" alt="">
									</div>
									<h6>Virtual<br> Shopping</h6>
								</div>
								<div class="col-md-3 col-6 mb-4 mb-md-0 text-center">
									<div class="bquestImg">
										<img src="img/icons/pick-up.png" alt="">
									</div>
									<h6>Pick up<br> at porch</h6>
								</div>
								<div class="col-md-3 col-6 mb-4 mb-md-0 text-center">
									<div class="bquestImg">
										<img src="img/icons/one-to-one.png" alt="">
									</div>
									<h6>One - One<br>Appointment</h6>
								</div>
								<div class="col-md-3 col-6 mb-4 mb-md-0 text-center">
									<div class="bquestImg">
										<img src="img/icons/home.png" alt="">
									</div>
									<h6>Home<br>Shopping</h6>
								</div>
							</div>
						</div>
					</div>
				
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- <?php 
	$sql = mysqli_query($conn, "SELECT * FROM shop_by_cat WHERE type=2 AND status=1 ORDER BY sb_id DESC");
	if($sql){
		if(mysqli_num_rows($sql) > 0){ ?>

					<?php while($rows = mysqli_fetch_assoc($sql)){ ?>
				    <div class="item">
				    	<div class="img-and-cont">
				    		<a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>">
				    			<img src="<?php echo WEBSITE_IMAGE_URL.$rows['img'] ?>" alt="" class="carousel-item">
				    		</a>
				    	</div>

				    	<div class="carousel-content text-center"> 
					        <p><a href="<?php echo (!empty($rows['link'])) ? $rows['link'] : '#'; ?>"><?php echo $rows['title'] ?></a></p> 
					    </div>
					</div>
					<?php } ?>
				</div>
				</div>
		</div>
	</div>
</section>
<?php } } ?> -->
<?php include_once"helpers/footer.php" ?>