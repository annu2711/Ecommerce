<?php include_once"helpers/index.php" ?>
<section class="middle_part py-5">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="heading w-100 text-center">
               <h2>Privacy policy</h2>
            </div>
            <div class='main-cont'>
               <p>We, iSmart Business Solutions Pvt. Ltd., are registered at 30/1, Patel Nagar, New Delhi, Delhi &ndash; 110008, hereinafter referred to as iSmart. At iSmart, we value your trust &amp; respect your privacy. This Privacy Policy provides you with details about the manner in which your data is collected, stored &amp; used by us. You are advised to read this Privacy Policy carefully. By visiting iSmart&rsquo;s website/WAP site/applications you expressly give us consent to use &amp; disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access iSmart website, WAP site or mobile applications.<br><br>
                  Note: Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to iSmart desktop website, iSmart mobile WAP site &amp; iSmart mobile applications.<br><br>
               <h6>General</h6>
               We will not sell, share or rent your personal information to any 3rd party or use your email address/mobile number for unsolicited emails and/or SMS. Any emails and/or SMS sent by iSmart will only be in connection with the provision of agreed services &amp; products and this Privacy Policy. Periodically, we may reveal general statistical information about iSmart &amp; its users, such as number of visitors, number and type of goods and services purchased, etc. We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure. Personal Information Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, and any details that may have been voluntarily provide by the user in connection with availing any of the services on iSmart. When you browse through iSmart, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.<br><br>
               <h6>Use of Personal Information</h6>
               We use personal information to provide you with services &amp; products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform youn about offers, products, services, updates, customize your experience, detect &amp; protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. We also use your contact information to send you offers based on your previous orders and interests. We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at iSmart, providing you with content that we think you might be interested in and to display content according to your preferences.<br><br>
               <h6>Cookies</h6>
               A &ldquo;cookie&rdquo; is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. iSmart uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.<br><br>
               <h6>Links to Other Sites</h6>
               Our site links to other websites that may collect personally identifiable information about you. iSmart is not responsible for the privacy practices or the content of those linked websites.<br><br>
               <h6>Security</h6>
               iSmart has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.<br><br> 
               <h6>Consent</h6>
               By using iSmart and/or by providing your information, you consent to the collection and use of the information you disclose on iSmart in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include_once"helpers/footer.php" ?>