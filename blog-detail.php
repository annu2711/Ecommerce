<?php 
include_once "helpers/index.php"; 

if(isset($_GET['blog']) && $_GET['blog'] != ''){
    $blog  = mysqli_real_escape_string($conn,$_GET['blog']);
    $blog_detail = mysqli_query($conn,"SELECT * FROM `blogs` WHERE `blog_id`='$blog'");
    if(mysqli_num_rows($blog_detail) > 0){
        $row = mysqli_fetch_assoc($blog_detail);
        $blog_id = $row['blog_id'];
    }else{
        header('location:blogs');
    }
}else{
    header('location:blogs');
}
?>

<section class="mb-5 d-md-block">
	<div class="container px-auto px-md-0 blogs pt-5 pb-4 ">
		<div class="row justify-content-center">
			<div class="col-xl-12 col-lg-12 col-md-12">
				<div class="row mt-4">
                    
                    <div class="col-md-9">
                        <div class="row">
                             <div class="blog_details">
                                <div class="blog-detail-image">
                                    <img src="<?=WEBSITE_IMAGE_URL.$row['blog_image'] ?>" width="100%" alt="">
                                </div>
                                <div class="blog-details-date">
                                    <span><?=time_elapsed_string($row['created_at']); ?> </span>| <span><?=$row['blog_author']?></span>
                                </div>
                                <div class="blog__title">
                                    <h6>
                                        <?=$row['blog_title']?>
                                    </h6>
                                </div>

                                <div class="blog__caption">
                                    <p>
                                        <?=$row['blog_text']?>
                                    </p>
                                </div>

                             </div>
                        </div>
                        
                    </div>  

                    <div class="col-md-3">
                        <div class="recent-blog">
                            <div class="recent-heading">
                                <h2>Recent Blogs</h2>
                                <hr>
                            </div>
                            <div class="recent-blog-content">
                                <div class="row">
                                    <?php 
                                        $recentBlog = mysqli_query($conn,"SELECT * FROM `blogs` WHERE `status`=1 AND `blog_id`!='$blog_id' ORDER BY `blog_id` DESC LIMIT 5");
                                        if(mysqli_num_rows($recentBlog) > 0){
                                            while($recentRow = mysqli_fetch_assoc($recentBlog)){
                                                ?>
                                                <div class="col-md-12">
                                                    <div class="recent-blog-row">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="recent-blog-image">
                                                                    <a href="blog-detail?blog=<?=$recentRow['blog_id']?>"><img src="<?=WEBSITE_IMAGE_URL.$recentRow['blog_image'] ?>" width="100%" alt=""></a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="recent-blog-title">
                                                                    <h6><a href="blog-detail?blog=<?=$recentRow['blog_id']?>"><?=$recentRow['blog_title']?></a></h6>
                                                                </div>
                                                                <div class="blog-details-date">
                                                                    <i><span><?=time_elapsed_string($recentRow['created_at']); ?> </span>| <span><?=$recentRow['blog_author']?></span></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>

<?php include_once"helpers/footer.php" ?>