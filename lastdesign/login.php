<?php include_once"helpers/index.php" ?>
        <!-- PAGE-HEADER-AREA-START -->
        <div class="page-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h2>Login Or Register</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAGE-HEADER-AREA-END -->
        <!-- BREADCRUMB-AREA-START -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-menu">
                            <ul>
                                <li><a href="#">Home</a> | </li>
                                <li><span>Login Or Register</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMB-AREA-END -->
        <!-- LOGIN-REGISTER-AREA-START -->
        <div class="login-register-area">
            <div class="container">
				<div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="login-form-area">
                           <h5 class="form-title">Login</h5>
                           <p class="sub-form-title">Hello, Welcome to your account</p>
                           <!-- <div class="social-login">
                                <a href="#" class="facebook-login"><i class="fa fa-facebook"></i>Sign In With Facebook</a>
                                <a href="#" class="twitter-login"><i class="fa fa-twitter"></i>Sign In With Twitter</a>
                            </div> -->
                            <form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                                <p class="login-username">
                                    <label>Email Address</label>
                                    <input type="email" name="username" required>
                                </p>
                                <p class="login-password">
                                    <label>Password</label>
                                    <input type="password" name="password" required>
                                </p>
                                <a href="#" class="forgot-password">Forgot Your password?</a>
                                <p class="login-remember">
                                    <!-- <label>
                                        <input type="checkbox" value="forever"> Remember me!
                                    </label> -->
                                </p>
                                <p class="login-submit">
                                    <input type="submit" value="LogIn" name="login" class="button-primary" style="margin-top: 20px">
                                </p>
                            </form>
                        </div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="registration-form">
							<h5 class="form-title">Create a new account</h5>
							<p class="sub-form-title">Create your own Unicase account</p>
							<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                                <p>
                                    <label>Name</label>
                                    <input type="text" name="name" required>
                                </p>
                                <p>
                                    <label>Email Address</label>
                                    <input type="email" name="email" required>
                                </p>
                                <p>
                                    <label>Password</label>
                                    <input type="password" name="password" required>
                                </p>
                                <p class="submit">
                                    <input type="submit" name="signup" value="SignUp">
                                </p>
                            </form>
                            <div class="registration-info">
                                <h5>SignUp today and you'll be able to:</h5>
                                <ul>
                                    <li>Speed your way through the checkout.</li>
                                    <li>Track your orders easily.</li>
                                    <li>Keep a record of all your purchases.</li>
                                </ul>
                            </div>
						</div>
					</div>
				</div> 
			</div>
        </div>
        <!-- LOGIN-REGISTER-AREA-END -->        
 <?php 
 
 include_once"helpers/footer.php"; 
 
 if(isset($_POST['signup'])){
    // echo '<script>alert("Please Fill All Fields")</script>';
     extract($_POST);
     if(!empty($name) && !empty($email) && !empty($password)){
        $chck_dup = check_duplicate('clients', 'email', $email);
        if(empty($chck_dup)){
            // $query = mysqli_query
            $key = rand_char(5);
            $token = rand_char(32);
            $pass = md5($password);
            $status = 1;
            try{
                $stmt = $conn->prepare("INSERT INTO clients (client_key, name, email, password, token, status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param('sssssss', $key, $name, $email, $pass, $token, $status, $created_at);
                if ($stmt->execute()){
                    $_SESSION['user'] = $key;
                    $_SESSION['token']= $token;
                    // echo "<script>showNotification('alert-info', 'Product Added Successfully', 'top', 'right', '', '')</script>";
                    header('location: my-account');

                    }else{
                        throw new exception($conn->error);
                    }
                    $stmt->close();
            }catch(Exception $e){
                echo "Error: " . $e->getMessage();
                echo "<script>showNotification('alert-danger', 'Signup failed', 'top', 'right', '', '')</script>";
            }
        }else{
            echo '<script>alert("User Already Registered")</script>';
        }
     }else{
        echo '<script>alert("Please Fill All Fields")</script>';
     }
 }


 if(isset($_POST['login'])){
     extract($_POST);
     if(!empty($username) && !empty($password)){
         $pass = md5($password);
         $sql = mysqli_query($conn, "SELECT * FROM clients WHERE email='$username' AND password='$pass' ORDER BY client_id DESC LIMIT 1");
         if($sql){
             if(mysqli_num_rows($sql) > 0){
                 $data = mysqli_fetch_assoc($sql);
                 $userkey = $data['client_key'];
                 $token = rand_char(32);
                 $query = mysqli_query($conn, "UPDATE clients SET token='$token' WHERE client_key='$userkey'");
                 if($query){
                     $_SESSION['user'] = $userkey;
                     $_SESSION['token'] = $token;
                     $storage = saveStorage($token, $_SESSION['user'], '0');
                 }
             }
         }
     }
}
 
 ?>