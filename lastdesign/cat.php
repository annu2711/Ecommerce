<?php 
include_once"helpers/index.php"; 
if(isset($_GET['cat'])){
    extract($_GET);
    $urlarray = explode("_", $cat);
    $category = "";
    $subcategory = "";
    $product = "";
    $product_list = "";
    $quote = "'";
    $prf="";
    $pstatus = "";
    $catarray = explode("-",$urlarray[0]);
    if($catarray[0]!= ""){
        $category = "AND category='".$catarray[0]."'";
    }
    if($catarray[1]!= ""){
        $subcategory = "AND subcategory='".$catarray[1]."'";
    }
    if($urlarray[2]!= ""){
        $prf = "AND prf='$urlarray[2]'";
    }
    $status = explode("!",$urlarray[1]);
    if($status[1]!= ""){
        $pstatus = "AND status='$status[1]'";
    }

    $countq = mysqli_query($conn, "SELECT * FROM products_db WHERE visible=1 $category $subcategory $prf $pstatus");
    if($countq){
        $rows = mysqli_num_rows($countq);
        if($rows > 12){
            // echo "rows =>".$rows;
            $totalpages = ceil($rows / 12);
            echo'<input type="hidden" id="totalpages" value="'.$totalpages.'"><input type="hidden" id="currentpage" value="1">';
        }else{
            // echo "rows2 =>".$rows;
            echo'<input type="hidden" id="totalpages" value="1"><input type="hidden" id="currentpage" value="1">';
        }
    }

    $query = mysqli_query($conn, "SELECT * FROM products_db WHERE visible=1 $category $subcategory $prf $pstatus ORDER BY p_id DESC LIMIT 12");
    if($query){
        if(mysqli_num_rows($query) > 0){
            // $products_array = mysqli_fetch_array($query);
            while($products = mysqli_fetch_assoc($query)){
                // echo $products['p_id']
                if($products['s_img']!= ""){
                    $second_img = $products['s_img'];
                }else{
                    $second_img = $products['p_img'];
                }
                $product = $product.'<div class="col-sm-6 col-md-4">
                <div class="single-product">
                    <div class="product-image">
                        <a href="single-product?p='.$products['p_key'].'">
                            <img class="primary-image" alt="Special" src="admin/products/'.$products['p_img'].'">
                            <img class="secondary-image" alt="Special" src="admin/products/'.$second_img.'">
                        </a>
                        <span class="onsale">Sale!</span>
                        <div class="category-action-buttons">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="category">
                                    <a href="single-product?p='.$products['p_key'].'">'.$products['prf'].'</a>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="action-button">
                                        <ul>
                                            <li>
                                                <a href="#" data-toggle="tooltip" title="Add to Wishlist"><i class="pe-7s-like"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="cart-button" data-toggle="modal" data-target="#productModal" title="Quick View" onclick="productQuickView('.$quote.$products['p_key'].$quote.')"><i class="pe-7s-cart"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <div class="product-title">
                            <a href="#">'.$products['product_name'].'</a>
                        </div>
                        <div class="price-rating">
                            <div class="star-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="price">
                                <span class="old-price"><i class="fa fa-inr"></i> '.$products['regular_price'].'</span>
                                <span class="new-price">'.$products['price'].'</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

            $product_list = $product_list.'<div class="col-md-12">
            <div class="single-list-product">
                <div class="product-image">
                    <a href="single-product?p='.$products['p_key'].'">
                        <img src="admin/products/'.$products['p_img'].'" alt="">
                    </a>
                </div>
                <div class="item-content-info">
                    <h2>
                        <a href="single-product?p='.$products['p_key'].'">'.$products['product_name'].'</a>
                    </h2>
                    <div class="category">
                        <a href="#">'.$products['prf'].'</a>
                    </div>
                    <div class="list-price-rating">
                        <div class="ro-rate">
                            <div class="star-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <!-- <a href="#" class="review-link">
                                <span>2</span>
                                 Review (s) / Add your Review
                            </a> -->
                            
                        </div>
                        <div class="price">
                        <span class="old-price"><i class="fa fa-inr"></i>'.$products['regular_price'].'</span>
                        <span class="new-price">'.$products['price'].'</span>
                        </div>
                    </div>
                    <div class="product-desc">'.$products['description'].'</div>
                    <div class="product-button">
                        <ul>
                            <li>
                                <a href="#"><i class="pe-7s-like"></i></a>
                            </li>
                            <li>
                                <a title="Quick View" data-target="#productModal" data-toggle="modal" href="#" onclick="productQuickView('.$quote.$products['p_key'].$quote.')"><i class="pe-7s-cart"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>';
            }
        }else{
            $error = '<p>Products are not available</p>';
        }
    }

}else{
    header('location: index');
}
?>
<style>
.product-desc {
    max-height: 122px;
    overflow-y: hidden;
}
</style>
<input type="hidden" id="filter" value="0">
        <!-- PAGE-HEADER-AREA-START -->
        <div class="page-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h2>Products</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAGE-HEADER-AREA-END -->
        <!-- BREADCRUMB-AREA-START -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-menu">
                            <ul>
                                <li><a href="#">Home</a> | </li>
                                <li><span>Products</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMB-AREA-END -->
        <div class="main-content-area">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 sidebar-area">
                        <!-- category-area-start -->
                        <?php
                                if($catarray[0]!=""){
                                    $query = mysqli_query($conn, "SELECT DISTINCT(a.subcategory) as subcat, (SELECT COUNT(*) FROM products_db WHERE subcategory=a.subcategory) as rc FROM `products_db` as a WHERE visible=1 AND category='$catarray[0]'");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){ ?>
                        <div class="category-area">
                            <div class="section-title">
                                <h2><?php echo getSinglevalue('category', 'cat_key', $catarray[0], 2); ?></h2>
                            </div>
                            <div id="cate-toggle" class="sideber-menu">
                                <ul>
                           
                            <?php  while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <li><a href="cat?cat=<?php echo $catarray[0] ?>-<?php echo $rows['subcat'] ?>_st!_"><?php echo getSinglevalue('subcategory', 'subcat_key', $rows['subcat'], 3); ?><span class="count">(<?php echo $rows['rc'] ?>)</span></a></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    <?php }
                                    }
                                }else{
                                    $query = mysqli_query($conn, "SELECT DISTINCT(a.category) as cat, (SELECT COUNT(*) FROM products_db WHERE category=a.category) as rc FROM `products_db` as a WHERE visible=1");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){ ?>
                                        <div class="category-area">
                            <div class="section-title">
                                <h2>Categories</h2>
                            </div>
                            <div id="cate-toggle" class="sideber-menu">
                                <ul>
                                <?php  while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <li><a href="cat?cat=<?php echo $rows['cat'] ?>-_st!_"><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?><span class="count">(<?php echo $rows['rc'] ?>)</span></a></li>
                                    <?php } ?>
                                        </ul>
                                        </div>
                        </div>
                                <?php   }
                                    }
                                }
                            ?>
                        <!-- category-area-end -->

                        <!-- brand-area-start -->
                        <?php 
                        $pass = 0;
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b, (SELECT COUNT(*) FROM products_db WHERE brand=a.brand) as brd FROM products_db as a WHERE visible=1 AND subcategory='$catarray[1]' AND brand!=''");
                                $pass = 1;
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b, (SELECT COUNT(*) FROM products_db WHERE brand=a.brand) as brd FROM products_db as a WHERE visible=1 AND category='$catarray[0]' AND brand!=''");
                                $pass = 1;
                            }

                            if($pass == 1){
                                if($query){
                                    if(mysqli_num_rows($query) > 0){ ?>
                                    <div class="brand-area">
                            <div class="section-title">
                                <h2>Brands</h2>
                            </div>
                            <div class="sideber-menu">
                                <ul>
                                    <?php while($rows = mysqli_fetch_assoc($query)){ ?>
                                        <li><label onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>')"><input type="checkbox" name="brand[]" value="<?php echo $rows['b'] ?>"> <?php echo getSinglevalue('brands', 'brand_key', $rows['b'], 3); ?><span class="count">(<?php echo $rows['brd'] ?>)</span></label></li>
                                     <?php } ?>
                                </ul>
                            </div>
                        </div>
                                <?php }
                                }
                            }
                            
                            
                            ?>
                        <!-- brand-area-end -->
                        <!-- color-area-start -->
                        <?php 
                            $pass = 0;
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(color) as color FROM products_db WHERE visible=1 AND subcategory='$catarray[1]' AND color!=''");
                                $pass = 1;
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(color) as color FROM products_db WHERE visible=1 AND category='$catarray[0]' AND color!=''");
                                $pass = 1;
                            }

                            if($pass == 1){
                                if($query){ ?>

                            <div class="color-area">
                            <div class="section-title">
                                <h2>Color</h2>
                            </div>
                            <div class="sideber-menu">
                                <ul>

                                 <?php    if(mysqli_num_rows($query) > 0){
                                        $colorarr = "";
                                        while($colors = mysqli_fetch_assoc($query)){
                                            $arr = explode(",", $colors['color']);
                                            foreach($arr as $x => $y){
                                                $colorarr = $colorarr.$y.",";
                                            }
                                        }
                                        $unique = array_unique(explode(",",$colorarr));
                                        foreach($unique as $u => $v){
                                            if(!empty($v)){
                                            $sql = mysqli_query($conn, "SELECT count(*) as ct FROM products_db WHERE color LIKE '%$v%'");
                                            if($sql){
                                                if(mysqli_num_rows($sql) > 0){
                                                    $cnt = mysqli_fetch_assoc($sql); ?>
                                                    <li><label onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>')"><input type="checkbox" name="color[]" value="<?php echo $v ?>"><?php echo $v ?><span class="count">(<?php echo $cnt['ct'] ?>)</span></label></li>
                                            <?php    }
                                                }
                                            }
                                        }
                                    } ?>
                                </ul>
                            </div>
                        </div>

                            <?php    }
                            }
                        ?>      
                        <!-- color-area-end -->

                        <!-- size-area-start -->
                        <?php 
                            $pass = 0;
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(size) as size FROM products_db WHERE visible=1 AND subcategory='$catarray[1]' AND size!=''");
                                $pass = 1;
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(size) as size FROM products_db WHERE visible=1 AND category='$catarray[0]' AND size!=''");
                                $pass = 1;
                            }

                            if($pass == 1){
                                if($query){ ?>

                            <div class="color-area">
                            <div class="section-title">
                                <h2>Size</h2>
                            </div>
                            <div class="sideber-menu">
                                <ul>

                                 <?php    if(mysqli_num_rows($query) > 0){
                                        $colorarr = "";
                                        while($colors = mysqli_fetch_assoc($query)){
                                            $arr = explode(",", $colors['size']);
                                            foreach($arr as $x => $y){
                                                $colorarr = $colorarr.$y.",";
                                            }
                                        }
                                        $unique = array_unique(explode(",",$colorarr));
                                        // print_r($unique);
                                        // $ct = count($unique);
                                        // for($i=0; $i <= count($unique); $i++){

                                        // }
                                        foreach($unique as $u => $v){
                                            // echo $v;
                                            if(!empty($v)){
                                            $sql = mysqli_query($conn, "SELECT count(*) as ct FROM products_db WHERE size LIKE '%$v%'");
                                            if($sql){
                                                if(mysqli_num_rows($sql) > 0){
                                                    $cnt = mysqli_fetch_assoc($sql); ?>
                                                    <li><label onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>')"><input type="checkbox" name="size[]" value="<?php echo strtoupper($v) ?>"><?php echo $v ?><span class="count">(<?php echo $cnt['ct'] ?>)</span></label></li>
                                            <?php    }
                                                }
                                            }
                                        }
                                    } ?>
                                </ul>
                            </div>
                        </div>

                            <?php    }
                            }
                        ?>      
                        <!-- size-area-end -->
                        <!-- price-slider-area-start -->
                       
                            <?php
                            $pass = 0;
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM products_db WHERE visible=1 AND subcategory='$catarray[1]' AND color!=''");
                                $pass = 1;
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM products_db WHERE visible=1 AND category='$catarray[0]' AND color!=''");
                                $pass = 1;
                            }

                            if($pass == 1){
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
                             <div class="price-slider-area">
                            <div class="section-title">
                                <h2>Price</h2>
                            </div>
                            <div class="filter-price">
                                <div id="slider-price"></div>
                                <div class="slider-values">
                                    <input type="hidden" id="rangeMin" value="<?php echo $priceRows['minprice'] ?>">
                                    <input type="hidden" id="rangeMax" value="<?php echo $priceRows['maxprice'] ?>">
                                    <input id="price-from" disabled="disabled" type="text" value="<?php echo $priceRows['minprice'] ?>"/> -
                                    <input id="price-to" disabled="disabled" type="text" value="<?php echo $priceRows['maxprice'] ?>"/>
                                    <button class="button" onclick="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>')" type="button">Filter</button>
                                </div>
                            </div>
                            </div>
                            <?php    }
                                }
                            }
                            ?>
                        
                        <!-- price-slider-area-end -->
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 page-content">
                        <div class="toolbar">
                            <div class="view-mode">
                                <span>View as:</span>
                                <ul>
                                    <li class="active"><a data-toggle="tab" href="#grid"><i class="fa fa-th"></i></a></li>
                                    <li><a data-toggle="tab" href="#list"><i class="fa fa-list"></i></a></li>
                                </ul>
                            </div>
                            <div class="ordering-product">
                                <div class="sorter">
                                    <span>Short by:</span>
                                    <select>
                                        <option>Position</option>
                                        <option>Name</option>
                                        <option>Price</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- shop-product-list-start -->
                        <div class="product-list-area">
                            <div class="row">
                                <div class="tab-content">
                                    <!-- product-grid-start -->
                                    <div id="grid" class="tab-pane fade in active">
                                        <?php if(!empty($product)){ 
                                                    echo $product;
                                                }else{
                                                    echo $error;
                                                }
                                            ?>
                                        <!-- silge-product-end -->
                                    </div>
                                    <!-- product-grid-end -->
                                    <!-- product-list-start -->
                                    <div id="list" class="tab-pane fade">
                                        <!-- single-product-start -->
                                        <?php if(!empty($product_list)){ 
                                                    echo $product_list;
                                                }else{
                                                    echo $error;
                                                }
                                            ?>
                                        <!-- single-product-end -->
                                    </div>
                                    <!-- product-list-end -->
                                </div>
                            </div>
                        </div>
                        <!-- shop-product-list-end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- FOOTER TOP AREA START -->
       <?php include_once"helpers/footer.php" ?>