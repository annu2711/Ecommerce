<?php include_once"helpers/index.php" ?>
<script>
// 	setTimeout(function(){ 
// 		var barItem =  parseInt($('.total-cart-item').text());
// if(barItem < 1){
// 	window.location = 'https://lsmart.in/'; 
// } 
// 		}, 900);
</script>
		<!-- coupon-area start -->
		<div class="coupon-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="coupon-accordion">
							<?php if(empty($id) && empty($usertoken)){ ?>
							<!-- ACCORDION START -->
							<h3>Returning customer? <span id="showlogin">Click here to login</span></h3>
							<div id="checkout-login" class="coupon-content">
								<div class="coupon-info">
									<p class="coupon-text">If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing & Shipping section.</p>
									<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
										<p class="form-row-first">
											<label>Username or email <span class="required">*</span></label>
											<input type="text" name="username"/>
										</p>
										<p class="form-row-last">
											<label>Password  <span class="required">*</span></label>
											<input type="password" name="password"/>
										</p>
										<p class="form-row">                    
											<input type="submit" name="login" value="Login" />
										</p>
										<p class="lost-password">
											<a href="#">Lost your password?</a>
										</p>
									</form>
								</div>
							</div>
							<!-- ACCORDION END --> 
							<?php } ?>

							<!-- ACCORDION START -->
							<!-- <h3>Have a coupon? <span id="showcoupon">Click here to enter your code</span></h3>
							<div id="checkout_coupon" class="coupon-checkout-content">
								<div class="coupon-info">
									<form action="#">
										<p class="form-row-first">
											<input type="text" placeholder="Coupon code" />
										</p>
										<p class="form-row-last">
											<input type="submit" value="Apply Coupon" />
										</p>
									</form>
								</div>
							</div> -->
							<!-- ACCORDION END -->                      
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- coupon-area end -->
		<!-- checkout-area start -->
		<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
		<div class="checkout-area" style="margin-top: 50px">
			<div class="container">
				<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="checkbox-form">                     
								<h3>Billing & Shipping Address Details</h3>
								<div class="row">
								<?php if(empty($id) && empty($usertoken)){ ?>
									<div class="col-md-6">
										<div class="checkout-form-list">
											<label>Name <span class="required">*</span></label>                                       
											<input type="text" name="name" placeholder="" required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkout-form-list">
											<label>Phone <span class="required">*</span></label>                                        
											<input type="text" name="mobile" placeholder="" required/>
										</div>
									</div>
									<div class="col-md-12">
										<div class="checkout-form-list">
											<label>Email Address <span class="required">*</span></label>                                        
											<input type="email" name="email" placeholder="" required/>
										</div>
									</div>
									<div class="col-md-12">
										<div class="checkout-form-list">
											<p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
											<label>Account password  <span class="required">*</span></label>
											<input type="password" name="password" placeholder="password" required/> 
										</div>  
									</div>
									<?php } ?>
									<input type="hidden" name="myaddress" value="no">
									<div class="col-md-12">
										<div class="checkout-form-list">
											<label>Address <span class="required">*</span></label>
											<textarea name="address" required></textarea>
										</div>
									</div>
									<div class="col-md-12">
										<div class="checkout-form-list">
											<label>Town / City <span class="required">*</span></label>
											<input type="text" name="city" placeholder="Town / City" required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="country-select">
											<label>State<span class="required">*</span></label>                         
											<select name="state" id="state" class="state"></select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkout-form-list">
											<label>Zip <span class="required">*</span></label>                                      
											<input type="text" name="pincode" placeholder="Postcode / Zip" />
										</div>
									</div>                              
								</div>
							</div>

								<div class="different-address">
									<div class="ship-different-title">
											<h3>
												<label>Ship to a different address?</label>
												<input id="ship-box" type="checkbox"/>
												<input type="hidden" id="ship_add" name="ship_add" value="0">
											</h3>
										</div>
									<div id="ship-box-info" class="row"></div>
									<div class="order-notes">
										<div class="checkout-form-list">
											<label>Order Notes</label>
											<textarea id="checkout-mess" name="note" cols="30" rows="10" placeholder="Notes about your order, e.g. special notes for delivery." ></textarea>
										</div>                                  
									</div>
								</div>                                                  
						</div>		
						<div class="col-lg-6 col-md-6">
							<div class="your-order">
								<h3>Your order</h3>
								<div class="your-order-table table-responsive">
									<table>
										<thead>
											<tr>
												<th class="product-name">Product</th>
												<th class="product-total">Total</th>
											</tr>                           
										</thead>
										<tbody id="checkout-container">
											<?php 
											$total = 0;
												if(!empty($id) && !empty($usertoken)){
													$sql = mysqli_query($conn, "SELECT t1.*, t2.price FROM user_cart as t1 join products_db as t2 on t1.product=t2.p_key WHERE t1.user='$id'");
													if($sql){
														if(mysqli_num_rows($sql) > 0){
															
															while($rows = mysqli_fetch_assoc($sql)){
																$price = $rows['price'];
																$total = $total + $price;
																?>
															<tr class="cart_item"><td class="product-name"><input type="hidden" name="product[]" value="<?php echo $rows['product'] ?>"><input type="hidden" name="color[]" value="<?php echo $rows['color'] ?>"><input type="hidden" name="size[]" value="<?php echo $rows['size'] ?>"><input type="hidden" name="qty[]" value="<?php echo $rows['qty'] ?>"><input type="hidden" name="price[]" value="<?php echo $rows['price'] ?>"><?php echo getSinglevalue('products_db', 'p_key', $rows['product'], 4) ?> x <strong class="product-quantity"><?php echo $rows['qty'] ?></strong></td><td class="product-total"><span class="amount"><?php echo $rows['qty'] * $price; ?></span></td></tr>
											<?php		}
														}
													}
												}
											?>
										</tbody>
										<tfoot>
											<tr class="cart-subtotal">
												<th>Cart Subtotal</th>
												<td><span class="amount carTotal"><?php echo $total ?></span></td>
											</tr>
											<tr class="order-total">
												<th>Order Total</th>
												<td>	
												<strong><span class="amount carTotal"><?php echo $total ?></span></strong>
												</td>
											</tr>                               
										</tfoot>
									</table>
								</div>
								<div class="payment-method">
									<div class="payment-accordion">
										<!-- ACCORDION START -->
										<h3 class="open">Card On Delivery</h3>
										<!-- <div class="payment-content">
											<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
										</div> -->
										<!-- ACCORDION END -->  
										<!-- ACCORDION START -->
										<!-- <h3>Cheque Payment</h3>
										<div class="payment-content">
											<p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
										</div> -->
										<!-- ACCORDION END -->  
										<!-- ACCORDION START -->
										<!-- <h3>PayPal <img src="img/payment.png" alt="" /></h3>
										<div class="payment-content">
											<p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
										</div> -->
										<!-- ACCORDION END -->                                  
									</div>
									<div class="order-button-payment">
										<input type="submit" value="Place order" name="place_order" />
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		</form>
		<!-- checkout-area end -->
   
<?php include_once"helpers/footer.php";


if(isset($_POST['login'])){
	extract($_POST);
	if(!empty($username) && !empty($password)){
		$pass = md5($password);
		$sql = mysqli_query($conn, "SELECT * FROM clients WHERE email='$username' AND password='$pass' ORDER BY client_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$data = mysqli_fetch_assoc($sql);
				$userkey = $data['client_key'];
				$token = rand_char(32);
				$query = mysqli_query($conn, "UPDATE clients SET token='$token' WHERE client_key='$userkey'");
				if($query){
					$_SESSION['user'] = $userkey;
					$_SESSION['token'] = $token;
					$storage = saveStorage($token, $_SESSION['user'], 1);
				}else{
					echo mysqli_query($conn);
				}
			}
		}
	}
}


if(isset($_POST['place_order'])){
	extract($_POST);
	$user = $id;
	
	mysqli_autocommit($conn, FALSE);
	if(empty($id) && empty($usertoken)){
		if(!empty($name) && !empty($email) && !empty($mobile) && !empty($email)){
			$chck_dup = check_duplicate('clients', 'email', $email);
        	if(empty($chck_dup)){
			$key = rand_char(5);
			$pass = md5($password);
			$token = rand_char(32);
			$status = 1;
			// $query = mysqli_query($conn, "")
			try {
				$stmt = $conn->prepare("INSERT INTO clients (client_key, name, email, mobile, password, token, status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
				$stmt->bind_param("ssssssss", $key, $name, $email, $mobile, $pass, $token, $status, $created_at);
				if ($stmt->execute()) {
					$user = $key;
					// $rcemail = $email;
				}else{
					throw new exception($conn->error);
				}
				$stmt->close();
			}
			catch(Exception $e){
				echo "Error: " . $e->getMessage();
					echo "<script>alert('".mysqli_error($conn)."')</script>";
			}
			}else{
				echo '<script>alert("User Already Registered. Please Login")</script>';
			}
		}else{
			echo '<script>alert("Please FIll All Fields")</script>';
		}
	}
		if(!empty($user)){
			$rcemail = getSinglevalue('clients', 'client_key', $user, 3);
			if($myaddress === 'no'){
				$firstaddress = rand_char(5);
				// $conn->
				try {
					$stmt = $conn->prepare("INSERT INTO address_book (ab_key, user, address, town, state, pincode, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param("sssssss", $firstaddress, $user, $address, $city, $state, $pincode, $created_at);
					if ($stmt->execute()) {
						$billing_address = $firstaddress;
					}else{
						mysqli_rollback($conn);
						throw new exception($conn->error);
					}
					$stmt->close();
				}
				catch(Exception $e){
					
					echo "Error: " . $e->getMessage();
						echo "<script>alert('".mysqli_error($conn)."')</script>";
				}
			}else{
				$billing_address = $last_address;
			}

			if($ship_add == '1'){

				$shipkey = rand_char(5);
				try{
					$stmt = $conn->prepare("INSERT INTO address_book (ab_key, user, name, email, mobile, address, town, state, pincode, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param("ssssssssss", $firstaddress, $user, $ship_name, $ship_email, $ship_mobile, $ship_address, $ship_city, $ship_state, $ship_pincode, $created_at);
					if ($stmt->execute()) {
						$shipping_address = $ship_key;
					}else{
						mysqli_rollback($conn);
						throw new exception($conn->error);
					}
					$stmt->close();
				}catch(Exception $e){
					
					echo "Error: " . $e->getMessage();
						echo "<script>alert('".mysqli_error($conn)."')</script>";
				}

			}else{
				$shipping_address = $billing_address;
			}

			if(!empty($product) && !empty($qty) && !empty($price)){
				$total_amount = 0;
				$order_key = rand_char(5);
				$total_items = count($product);
				foreach($price as $p => $am){
					$total_amount = $total_amount + intval($am);
				}
				try{
					$stmt = $conn->prepare("INSERT INTO orders (order_key, user, total_items, amount, billing_address, shipping_address, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param("sssssss", $order_key, $user, $total_items, $total_amount, $billing_address, $shipping_address, $created_at);
					if ($stmt->execute()) {
						$success = 0;
						foreach($product as $x => $y){
							// echo $price[$x];
							$stmt = $conn->prepare("INSERT INTO order_book (order_key, user, product, color, size, qty, price, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
							$stmt->bind_param("ssssssss", $order_key, $user, $y, $color[$x], $size[$x], $qty[$x], $price[$x], $created_at);
							if($stmt->execute()){
								$success = 1;
							}else{
								echo $conn->error;
								throw new exception($conn->error);
								$success = 0;
							}
						}
						if($success == 1){
							$sql = mysqli_query($conn, "SELECT * FROM user_cart WHERE user='$user'");
							if($sql){
								if(mysqli_num_rows($sql) > 0){
									$subquery = mysqli_query($conn, "DELETE FROM user_cart WHERE user='$user'");
									if($subquery){
										mysqli_commit($conn);
										$content = 'Hi,<br>

										We are pleased to confirm your order no '.$order_key.'.
										Thank you for shopping with Lsmart.<br>
										Check your order <a href="https://lsmart.in/my-account">here</a>';
										$mail = sendEmail('orders@lsmart.in', $rcemail, 'Order Successfully', $content);
								// 		if($mail!= 0){
										header('location: order-success?orderid='.$order_key);
								// 		}
								// echo $mail;
									}
								}else{
									mysqli_commit($conn);
									// header('location: order-success?orderid='.$order_key);
									$content = 'Hi,<br>

										We are pleased to confirm your order no '.$order_key.'.
										Thank you for shopping with Lsmart.<br>
										Check your order <a href="https://lsmart.in/my-account">here</a>';
										$mail = sendEmail('orders@lsmart.in', $rcemail, 'Order Successfully', $content);
								// 		if($mail!=  0){
										header('location: order-success?orderid='.$order_key);
								// 		}
								// echo $mail;
								}
							}
						}else{
							mysqli_rollback($conn);
						}
					}else{
						echo $conn->error;
						mysqli_rollback($conn);
						throw new exception($conn->error);
					}
					$stmt->close();
				}catch(Exception $e){
					echo "Error: " . $e->getMessage();
						echo "<script>alert('".mysqli_error($conn)."')</script>";
				}
			}
	}

}


?>