<?php include_once"helpers/index.php" ?>
        <!-- PAGE-HEADER-AREA-START -->
        <div class="page-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h2>Login Or Register</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAGE-HEADER-AREA-END -->
        <!-- BREADCRUMB-AREA-START -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-menu">
                            <ul>
                                <li><a href="#">Home</a> | </li>
                                <li><span>Forgot password</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMB-AREA-END -->
        <!-- LOGIN-REGISTER-AREA-START -->
        <div class="login-register-area">
            <div class="container">
				<div class="row">
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                        <div class="password-form">
                           <h5 class="form-title">Recover password</h5>
                            <form method="post" action="#">
                                <p class="login-username">
                                    <label>Email Address</label>
                                    <input type="text">
                                </p>
                                <p class="login-submit">
                                    <input type="submit" value="Submit" class="button-primary">
                                </p>
                            </form>
                        </div>
					</div>
				</div> 
			</div>
        </div>
        <!-- LOGIN-REGISTER-AREA-END -->        
 <?php 
 
 include_once"helpers/footer.php"; 
 ?>