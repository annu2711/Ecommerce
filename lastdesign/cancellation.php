<?php include_once "helpers/index.php";?>

        <div class="main-content-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="wishlist-title text-center">
                            <h2 class="heading">Cancellation</h2>
                        </div>
                        <div class='main-cont'>
                           <p><span style="font-size: small;"><strong>How can I cancel my order?</strong></span></p>
<p><span style="font-size: small;">You can cancel your order before it has been confirmed by the brand/seller.<br /> We'll refund the full amount you've paid for such a cancellation. Here's how to cancel your order:</span></p>
<ul>
<li>
<p><span style="font-size: small;">Log in to your account and go to My Account</span></p>
</li>
<li>
<p><span style="font-size: small;">Select the appropriate order from &lsquo;My Orders&rsquo;</span></p>
</li>
<li>
<p><span style="font-size: small;">Click on &lsquo;Cancel Order&rsquo; for the item(s) you want to cancel, individually</span></p>
</li>
<li>
<p><span style="font-size: small;">Select the reason and confirm the cancellation</span></p>
</li>
</ul>
<p><span style="font-size: small;">Once you're done, we'll send you an acknowledgement of the cancellation. Remember, you won&rsquo;t be able to cancel an order after it has been processed by the brand/seller.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>When can I cancel my order?</strong></span></p>
<p><span style="font-size: small;">Make sure you cancel your order before it has been processed by the brand/seller. That's the only way we can refund the full amount you've spent.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>Can I cancel only part of my order?</strong></span></p>
<p><span style="font-size: small;">Yes you can. You can individually cancel product(s) in your bag before your order is processed by the brand/seller. Go to Order History in the My Account section to cancel products.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>Why do I see a disabled cancel link?</strong></span></p>
<p><span style="font-size: small;">That means the product(s) from your order have already been shipped and you can't make any cancellations now.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>How will I get my refund when I cancel an order?</strong></span></p>
<p><span style="font-size: small;">Once your order has been cancelled, it will take 3-4 business days for your refund to be processed and the amount to be transferred back to the source account. In the case of certain public sector banks, it can take up to 10-15 working days.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>Will I get the complete refund for the order I&rsquo;ve cancelled?</strong></span></p>
<p><span style="font-size: small;">Yes. We'll refund the entire amount for a cancelled order.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>What should I do if I don&rsquo;t get my refund in the promised time?</strong></span></p>
<p><span style="font-size: small;">We work quickly to make sure your refund gets to you on time. On the off-chance that it's been delayed, please get in touch with&nbsp;us here.&nbsp;Don't forget to keep your order number handy.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>This is not what I ordered. How do I replace it?</strong></span></p>
<p><span style="font-size: small;">If your order or a part of it does not match the product description, we'll look into it right away. You can initiate a return request though My Orders in the My Account section.</span></p>
<p><br /> </p>
<p><span style="font-size: small;"><strong>Why was my order cancelled by iworld?</strong></span></p>
<p><span style="font-size: small;">Sometimes our sellers cancel orders for various reasons &ndash; the product could be out-of-stock, of unacceptable quality, etc.&nbsp;Once an order has been cancelled, the refund will be processed immediately. In all it will take 3-4 business days for your refund to be processed and the amount to be transferred back to the source account. In the case of certain public sector banks, it can take up to 10-15 working days.</span></p>
<p><br /> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include_once "helpers/footer.php";?>