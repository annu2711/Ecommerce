<?php include_once"helpers/header.php" ?>
<section class="middle_part py-5">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="heading w-100 text-center">
               <h2>Return & refund</h2>
            </div>
            <div class='main-cont'>
               <p><span style="font-size: small;"><strong>How can I initiate a return?</strong></span></p>
               <ul>
                  <li>
                     <p><span style="font-size: small;">Go to Order History and select the item(s) you want to return.</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Click on the Return option below the item.</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Select your reason for returning the item and confirm your return request.</span></p>
                  </li>
               </ul>
               <p><span style="font-size: small;"><br /> You will have to share your order number (the one from your confirmation email or SMS) with us, along with the reason for your return. Note that in some cases, we might not find the reason for returns acceptable because of seller policies; in such cases, we will call and advice you on next steps.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>What is the time-period within which I can return a product?</strong></span></p>
               <p><span style="font-size: small;">Apparel and other non-electronic products can be returned within 30 days of receiving the product. Watches can be returned within 15 days of receiving the product. For electronics and appliances, you can return the product within 7 days of delivery only. Returns will be accepted only if the product received is not as described, defective, damaged or not working when reviewed.<br /> In case of post-usage defects, the manufacturer warranty can be used; please visit their authorised service centre for assistance. *Note: lSmart reserves the right to amend the Returns Policy as and when necessary.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>When are returns not possible?</strong></span></p>
               <p><span style="font-size: small;">Returns are not possible in cases where the brand/seller has mandated a no return policy, or for hygiene reasons. Also, we cannot accept returns after 30 days from the date of delivery for non-electronic items and 7 days for electronic products on lSmart. Watches can be returned within 15 days of receiving the product.<br /> If you&rsquo;ve shopped on lSmart, here are some products we can&rsquo;t take back at all:<br /> </span><br /> </p>
               <ul>
                  <li>
                     <p><span style="font-size: small;">Innerwear, lingerie, socks, clothing freebies and swimsuits, for hygiene reasons</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Perfumes, personal and beauty care products, as mandated by the seller</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Products that have already been used or installed, as mandated by the seller</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Products that have been tampered with or are missing serial numbers, as mandated by the seller</span></p>
                  </li>
                  <li>
                     <p><span style="font-size: small;">Personalised/engraved items, as mandated by the seller.</span></p>
                  </li>
               </ul>
               <p><span style="font-size: small;"><br /> For complete details please refer to our Returns Policy.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Why can't you pick up my returns from my address?</strong></span></p>
               <p><span style="font-size: small;">If the pick-up facility is not available at the PIN code where you stay, we regret we won&rsquo;t be able to pick up products you wish to return. Instead, we request you to courier the package yourself. In such a case, all you have to do is share your airway bill (AWB) with us, and we&rsquo;ll reimburse you as per the returns policy, after we receive the product and a quality check is completed by our brand/seller partner.<br /> For self-courier returns, refer to the Return Policy for number of days within which the product has been returned.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>How should I pack my product for the return?</strong></span></p>
               <p><span style="font-size: small;">Enclose the product(s), in original condition and packaging, along with the original box, as part of the return package. Remember, we can&rsquo;t offer refunds for products if their tags/labels or seals have been tampered with &ndash; so be extra careful. Also, in case of self-couriered returns, please retain proof of the postage until we send you a confirmation that we have received your returns.<br /> To find out all you need to know about returns, refer to our Returns Policy.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Can I get a replacement for the product I&rsquo;ve ordered?</strong></span></p>
               <p><span style="font-size: small;">Unfortunately, we can&rsquo;t replace products that you&rsquo;ve already ordered. Instead, you can request a return and place a separate order for the new product(s) you want to order.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Can I return a part of my order?</strong></span></p>
               <p><span style="font-size: small;">Yes you can. Just select the product(s) you want to return and initiate the return right away. You should know that we can&rsquo;t accept returns of incomplete product combos (for example: Buy one get one free combos, free gifts, etc.).</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>When I&rsquo;m returning a product, do I have to return the free gift that I got with it as well?</strong></span></p>
               <p><span style="font-size: small;">Yes. Any freebies/gifts that you received will need to be returned along with the original product.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>The free gift I&rsquo;ve received is damaged/not working. What should I do?</strong></span></p>
               <p><span style="font-size: small;">Get in touch with us and we&rsquo;ll send you another gift right away.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>What is the pick-up process for the return of a product?</strong></span></p>
               <p><span style="font-size: small;">Once you&rsquo;ve made a return request with us, a pick-up will be arranged in 2 working days, in the case of apparel and accessories. In the case of electronics products, a pick-up will be initiated after the return request has been approved.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Can I cancel my return request and choose to keep the product?</strong></span></p>
               <p><span style="font-size: small;">Sorry, but you can&rsquo;t &lsquo;cancel&rsquo; a return request. Here&rsquo;s what you can do &ndash; at the time of pick-up, make it clear that you&rsquo;d like to keep the product.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Within how many days to I need to self-courier a product that I want to return?</strong></span></p>
               <p><span style="font-size: small;">Ship the product(s) to us after you initiate a return request. Send your returns to the address mentioned on the invoice and email us proof of dispatch (the slip/air way bill provided by the logistics company) at pod@lSmart.com.</span></p>
               <p><span style="font-size: small;">I would like additional customised work done during the installation process. Who will bear the charges?</span></p>
               <p><span style="font-size: small;">Any additional work you request during installation is chargeable. You will have to pay the amount directly to the technician. This may include charges for extra wires/pipes etc.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>There is no seal on the warranty card I received. How will I claim my warranty?</strong></span></p>
               <p><span style="font-size: small;">A seal is not mandatory for a warranty to be valid. The invoice you received along with the product can be used to claim the warranty as it contains the details of your purchase.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>I lost my warranty card/I did not receive a warranty card. What should I do?</strong></span></p>
               <p><span style="font-size: small;">You can get warranty protection for your product even without a warranty card. Please use the invoice, which will serve as your warranty, at any brand-recognised authorised service centre.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>How do I track the status of a product I have returned?</strong></span></p>
               <p><span style="font-size: small;">Once the return request is approved, you will receive a SMS notification that contains the airway bill (AWB) number. You can use it to track your return on our logistics partner's website</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>I have received my order, but I can't see an option to initiate returns. What should I do?</strong></span></p>
               <p><span style="font-size: small;">It may take our logistics partner some time to update the delivery status online. Please allow 48 hours for the delivery status to be updated. You can then place your return request.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>Why has my return request been rejected?</strong></span></p>
               <p><span style="font-size: small;">It's unfortunate, but the reason for making the return does not comply with our policy.</span></p>
               <p><br /> </p>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include_once"helpers/footer.php" ?>