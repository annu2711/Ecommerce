<?php include_once"helpers/index.php" ?>

<section class="middle_part py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="heading w-100 text-center">
							<h2>Reviews guideline</h2>
						</div>
                        <div class='main-cont'>
                            <p>At iSmart we want customers to get the information they need to make smart buying choices, and we'd love to have your help doing that. As a customer you can use the feature of reviews by sharing authentic feedback about our products. Your participation will help future shoppers make more informed buying decisions. So, tell us - what did you like or not like about a specific product? Please be honest with your feedback!</p>


                            <h6>ELIGIBILITY</h6>

                            <p>Anyone who has purchased items from iSmart(Buyer). All we ask is that you follow a few simple rules (see "Content Restrictions" below).</p>    


                            <h6>CONTENT RESTRICTIONS</h6>
                            <p>If the content you submit does not comply with these Guidelines, it will be rejected or removed. We are unable to accept the following content:</p>


                            <p>Inappropriate, Offensive or Illegal Content</p>
                            <ul>
                                <li>Do not submit content that is libellous, defamatory, harassing, threatening or inflammatory. For example, do not use obscenities or profanity, express hatred or intolerance for people based on race, ethnicity, nationality, gender or gender identity, religion, sexual orientation, age, or disability, or promote organizations with such views.</li>
                                <li>Do not submit private information such as phone numbers, mailing addresses or other personal information.</li>
                                <li>Do not engage in name-calling or attack people.</li>
                                <li>Do not submit content that encourages or supports behaviour that is fraudulent, abusive, illegal or violent.</li>
                            </ul>


                            <p>Do not impersonate other people or organizations.<br><br>
                            Do not submit content that infringes the intellectual property or other proprietary rights of others. Only submit your own content or content that you have permission to use.</p>


                            <h6>Sexual Content</h6>
                            <p>Do not submit sexual content. We reserve the right to restrict sexual content on our site as some of our customers may be sensitive to such content.</p>


                            <h6>Promotional Content and Commercial Solicitations</h6>
                            <p>To preserve the integrity of the content in reviews, content and activities consisting of advertising, promotion, or solicitation (whether direct or indirect) is not allowed, including:</p>
                            <ul>
                                <li>Creating, modifying, or submitting content regarding your (or your relative's, close friends, business associates, or employer's) products or services.</li>
                                <li>Creating, modifying, or submitting content regarding your competitors' products or services.</li>
                                <li>Creating, modifying, or submitting content in exchange for compensation of any kind (including free or discounted products) or on behalf of anyone else.</li>
                                <li>Offering compensation or requesting compensation (including free or discounted products) in exchange for creating, modifying, or submitting content.</li>
                            </ul>


                            <h6>Additional Non-Compliant Content</h6>
                            <ul>
                                <li>Content that is not relevant to the product.</li>
                                <li>Content that includes non-iSmart URL’s or links, or links to phishing or other malware sites.</li>
                                <li>Content that is written in a language other than English unless there is a clear connection to the product such as a phrase appearing on the product or product page.</li>
                            </ul>


                            <h6>HOW TO WRITE A GOOD CUSTOMER REVIEW</h6>
                            <ul>
                                <li>Include the "why": The best reviews include not only whether you liked or disliked a product, but also why.</li>
                                <li>Be specific: Your review should be relevant to the product you're reviewing and focus on specific features and/or your experience with the product. Comments about pricing or product availability are not about the product and should not be included in your review.</li>
                                <li>Not too short, not too long: The ideal length is 75 to 500 words.</li>
                                <li>Be sincere: We welcome your honest opinion about the product. We do not remove reviews because they are critical of a product if the reviews comply with the Guidelines.</li>
                            </ul>


                            <h6>HOW TO REPORT A REVIEW OR REQUEST REMOVAL OF A CUSTOMER REVIEW</h6>
                            <p>After a review is submitted you cannot delete the review. You may request removal of the review by contacting us at customer-reviews@iSmart.com. We may but shall not be obligated to remove your review except as required by law. Removal does not ensure the review will be completely or comprehensively deleted.</p>

                            <h6>GENERAL TERMS</h6>


                            <p>You agree to give us permission to use, free of charge, any review for business promotion.<br><br>
                                You agree that we shall be released and held harmless for any claims, liabilities, or causes of action of any kind or nature for any injury, loss or damages of any kind including direct, indirect, incidental, consequential or punitive damages to persons, including without limitation disability or death.<br><br>
                            In case of any dispute the Courts in Mumbai shall have exclusive Jurisdiction</p>
                        </div>
                    </div>
                </div>
            </div>
</section>

<?php include_once"helpers/footer.php" ?>