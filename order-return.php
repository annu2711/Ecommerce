<?php 
include_once "helpers/index.php";
client_session($id, $usertoken);
?>
<style>
#ac-two {
    background: #fff;
    font-weight: bold;
}
</style>
<section>
	<div class="container-fluid px-0">
		<div class="accountMainDiv">
			<div id="breadcrumb">
			    <ul class="mb-0">
					<li class="d-inline-block"><a href="#">Home <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block"><a href="#">My account <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block">Order & Return</li>
			    </ul>
			</div>
			<?php include_once "helpers/accountmenu.php"; ?>
			<div class="accountContent pb-5">
					<div class="heading w-100 text-center my-5">
						<h2>My Orders</h2>
					</div>

			<?php 
			
			?>
		<div class="col-md-12">
			<?php 
				$ordersql = mysqli_query($conn, "SELECT t1.*, t2.order_id, t2.order_key, t2.d_status, t2.payment_status, t2.shipping_address, t2.created_at as orderd FROM order_book as t1 join orders as t2 on t1.order_key=t2.order_key WHERE t2.user='$id' AND t2.payment_status!='Uncomplete'");
				if($ordersql){
					if(mysqli_num_rows($ordersql) > 0){
						while($rows = mysqli_fetch_assoc($ordersql)){
			?>
			<div class="row order-row mb-3">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-4 m-bg-dark">
                            <p>Order Placed <br> <?php $date = explode(" ", $rows['orderd']); echo $date[0]; ?></p>
                        </div>
                        <div class="col-md-4 m-bg-dark">
                            <p>Total <br><i class="fa fa-inr"></i> <?php  echo $rows['price'] ?></p>
                        </div>
                        <div class="col-md-4 m-bg-dark">
                            <p>Ship To </p>
							<?php  $ship_key = $rows['shipping_address']; ?>
                            <div class="dropdowna"><a class="hover-order-link" style="color: #fff !important;"><?php echo getSingleValue('address_book', 'ab_key', $ship_key, 4) ?> <i class="fa fa-angle-down"></i></a>
                            <div class="dropdown-content">
							<?php 
                                    $shipaddress = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$ship_key'");
                                    if($shipaddress){
										$ship_add = mysqli_fetch_assoc($shipaddress);
							?>
                                        <p><strong><?php  echo $ship_add['title']." ".$ship_add['name']." ".$ship_add['last_name'] ?></strong></p>
                                        <p><?php  echo $ship_add['address']." ".$ship_add['town']." ".$ship_add['state']." ".$ship_add['pincode'] ?></p>
                                        <p>India</p>
                                        <p>Phone: <?php  echo $ship_add['mobile'] ?></p>
                            <?php   
                                    }  
                            ?>
                            </div>
                            </div>
                        
                        </div>

                        <div class="col-md-12">
                            <div class="row pb-3">
                                <div class="col-md-12 delivery-status">
									<h4><?php  orderStatus($rows['d_status']) ?></h4>
                                </div>
                                <div class="col-md-3">
                                    <!-- product image -->
									<?php  // echo $rows['product'] ?>
                                    <img src="<?php  echo PRODUCT_IMAGE_URL.getSinglevalue('product_images', 'product', $rows['product'], 2) ?>" width="100%">
                                </div>
                                <div class="col-md-9">
                                    <!-- product details -->
                                    <p><?php  echo getSinglevalue('product', 'sku', $rows['product'], 4)." ".getSinglevalue('product', 'sku', $rows['product'], 21); ?></p>
                                    <p>Quantity: <?php  echo $rows['qty'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12 m-bg-dark" align="right">
                            <p>ORDER # <?php  echo $rows['order_id'] ?></p>
                            <a href="helpers/pdfmaker.php?invoice=<?php echo $rows['order_key'] ?>" class="invoice-link hover-order-link" style="color: #fff !important;">Invoice <i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                </div>
            </div>
		<?php } } } ?>
		</div>


	
			</div>
		</div>
	</div>
</section>
<script>
    function getOrderDetail(x){
        if(x != ""){
            $.ajax({
                type: 'post',
                url: 'admin/helpers/event.php',
                data: {orderdetail: x, view: 'web'},
                success: function(data){
                    $('#order-table').html(" ");
                    $('#order-table').html(data);
                }
            })
        }
    }

    // function tableTotal(tbl){

    // }
</script>
<?php include_once "helpers/footer.php"; 

// downloadInvoice($html);

?>