<?php include_once "helpers/index.php";

if(isset($_GET['p'])){
    extract($_GET);
    $query = mysqli_query($conn, "SELECT * FROM product WHERE sku='$p' ORDER BY p_id DESC LIMIT 1");
    if($query){
        if(mysqli_num_rows($query) > 0){
			$product_detail = mysqli_fetch_array($query);

			// get images
			$imgsql = mysqli_query($conn, "SELECT image FROM product_images WHERE product='$product_detail[5]'");
			if($imgsql){
				$imgarr = [];
				if(mysqli_num_rows($imgsql) > 0){
					while($imgrow = mysqli_fetch_array($imgsql)){
						$imgarr[] = $imgrow; 
					}
				}
			}




			if(isset($_COOKIE['recently_viewed'])){
				$view_data = stripcslashes($_COOKIE['recently_viewed']);
				$rcview_data = json_decode($view_data, true);
			}else{
				$rcview_data = [];
			}
			$rc_array = $product_detail[5];
			if (!in_array($rc_array, $rcview_data)) {
				$rcview_data[] = $rc_array;
				$rcitems = json_encode($rcview_data);
				setcookie('recently_viewed', $rcitems, time() + (172800 * 30));
			}
        }else{
            header('location: index');
        }
    }
}else{
    header('location: index');
}
?>


<section class="mt-1 mt-md-4 mt-lg-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 mb-1 mb-md-5 mb-lg-0">
				<div class="zoomDiv d-none d-lg-block">
					<div class="pdp-image-gallery-block">
						<div class="row">
							<div class="col-md-2">
										<!-- Gallery  Thumbnail Images-->
									<div class="gallery_pdp_container">
										<div id="gallery_pdp" class="carouselNew">
										<?php 
                        					for($i = 0; $i <= count($imgarr) - 1; $i++){ 
									    $zoom_child_img = $imgarr[$i]['image'];
									         if(file_exists(PRODUCT_IMAGE_URL.$zoom_child_img)){
                                                    $f_image = $zoom_child_img;
                                                }else{
                                                    $f_image = NOT_FOUND_IMAGE_WEB;
                                                }
                    					?>
											<a href="#" data-image="<?php echo PRODUCT_IMAGE_URL.$f_image ?>" data-zoom-image="<?php echo PRODUCT_IMAGE_URL.$f_image ?>">
												<img id="" src="<?php echo PRODUCT_IMAGE_URL.$f_image ?>" />
											</a>
											<?php  } ?>
										</div>
									</div>

									<!-- Gallery -->
							</div>
							<div class="col-md-10">
								<div class="imgViewerDiv">
									<!-- gallery Viewer medium image -->
									<div class="gallery-viewer">
									    <?php 
									    $zoom_img = $imgarr[0]['image'];
									         if(file_exists(PRODUCT_IMAGE_URL.$zoom_img)){
                                                    $f_image = $zoom_img;
                                                }else{
                                                    $f_image = NOT_FOUND_IMAGE_WEB;
                                                }
									    ?>
										<img id="zoom_10" src="<?php echo PRODUCT_IMAGE_URL.$f_image ?>" data-zoom-image="<?php echo PRODUCT_IMAGE_URL.$f_image ?>" />
										
									</div>
									<!-- gallery Viewer -->
								</div>
									<div class="imgViewerDiv2 w-100">
										<p class="pl-3 mb-1"><b>VIEW MORE</b></p>
										<ul class="m-0">

											<li class="d-inline-block m-0 mx-3"><a href="cat?cat=<?php echo $product_detail[6] ?>-_st!_"><?php echo getSinglevalue('category', 'cat_key', $product_detail[6], 2) ?></a></li>
											<li class="d-inline-block m-0 mx-3"><a href="cat?cat=<?php echo $product_detail[6] ?>-<?php echo $product_detail[7] ?>_st!_"><?php echo getSinglevalue('subcategory', 'subcat_key', $product_detail[7], 3) ?></a></li>
											<?php if(!empty($product_detail[3])){ ?>
											<li class="d-inline-block m-0 mx-3"><a href="brand?brand=<?php echo getSinglevalue('brands', 'brand_key', $product_detail[3], 3); ?>__<?php echo $product_detail[6] ?>"><?php echo getSinglevalue('brands', 'brand_key', $product_detail[3], 3) ?></a></li>
											<?php } ?>
										</ul>
									</div>
							</div>
						</div>
					</div>


					<div id="enlarge_gallery_pdp">
						<div class="enl_butt">
							<a class="enl_but enl_fclose" title="Close"></a>
							<a class="enl_but enl_fright" title="Next"></a>
							<a class="enl_but enl_fleft" title="Prev"></a>
						</div>
						<div class="mega_enl"></div>
					</div>
				</div>
				<!-- carousel on mobile -->
				<div class="d-block d-lg-none">
					<div class="row justify-content-center">
				<div class="buyer col-sm-12 singlePro">
					<div class="indexs owl-carousel owl-theme">
					<div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
				    			<img src="<?php echo PRODUCT_IMAGE_URL.$imgarr[0]['image'] ?>" alt="" class="carousel-item">
				    		</a>
				    	</div>
					</div>
					<?php 
                    for($i = 0; $i <= count($imgarr) - 1; $i++){ 
					?>
					<div class="item">
				    	<div class="img-and-cont">
				    		<a href="javascript:void(0)">
				    			<img src="<?php echo PRODUCT_IMAGE_URL.$imgarr[$i]['image'] ?>" alt="" class="carousel-item">
				    		</a>
				    	</div>
				    </div>
					<?php  } ?>
				
				</div>
				</div>
		</div>
				</div>
			</div>
			<div class="col-lg-5">

				<div class="detailCont">
					<div class="categoryContent">
						<h2><?php echo getSinglevalue('brands', 'brand_key', $product_detail[3], 3) ?></h2>
						<p class="my-3"><?php echo $product_detail[4] ?></p>
						<p><i class="fa fa-inr"></i> <?php echo str_replace('INR', '',money_format('%i', $product_detail[10])) ?></p>
					</div>

					<div class="sizeguide my-4">
					<!-- form starts here -->
					<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
					<input type="hidden" name="product" value="<?php echo $p ?>">

						<div class="row">
					<?php $crntclr = getSinglevalue('product_attributes', 'p_id', $product_detail[5], 4);
							if(!empty($crntclr)){
					?>
						<!-- color starts -->
						<div class="col-md-12">
						    <?php $product_stock = productQty($p); if($product_stock < 1){ ?>
						    <!--<label class="text-danger"><strong>Out Of Stock</strong></label><br> -->
						    <?php } ?>
						<input type="radio" name="color" value="<?php echo $crntclr ?>" checked>
						<label>Color : <span class="text-dark"><strong><?php echo $crntclr ?></strong></span></label>
						<ul class="color-list">
						    <?php 
						    $crnt_sample_image = getSinglevalue('product_attributes', 'p_id', $product_detail[5], 8);
						    if(strlen($crnt_sample_image) > 5){ ?>
						<li><a href="single-product?p=<?php echo $product_detail[5] ?>">
						    
						    <img class="active" src="<?php echo PRODUCT_IMAGE_URL.$crnt_sample_image ?>">
						    </a></li>
						    <?php } ?>
							<?php 
								$sql = mysqli_query($conn, "SELECT DISTINCT(color) as color, (SELECT p_id FROM product_attributes WHERE pa_id=t1.pa_id) as p_id, (SELECT sample FROM product_attributes WHERE pa_id=t1.pa_id) as img FROM product_attributes as t1 WHERE groupid='$product_detail[20]' AND color!='$crntclr' GROUP BY color");
								if($sql){
									if(mysqli_num_rows($sql) > 0){
										while($rows = mysqli_fetch_assoc($sql)){ ?>
										<li>
										   
										    <a href="single-product?p=<?php echo $rows['p_id'] ?>">
										   <img src="<?php echo PRODUCT_IMAGE_URL.$rows['img'] ?>"></a>
										   
										   </li>
							<?php		}
									}
								}
							?>
						</ul>
						</div>
						<!-- color ends -->
					<?php } ?>
				<?php 
						$crntsize = getSinglevalue('product_attributes', 'p_id', $product_detail[5], 5);
						if(!empty($crntsize)){
					?>
						<!-- size starts -->
						
						<div class="col-md-12">
							<div class="selectDiv">
								<select name="size" id="itemsize" class="mb-4" onchange="changeSize('itemsize')" required>
								<?php 
								$clr = (!empty($crntclr)) ? "AND color='$crntclr'" : "" ;
								$sql = mysqli_query($conn, "SELECT DISTINCT(size) as size, (SELECT t2.p_id FROM product_attributes as t2 WHERE t1.pa_id=t2.pa_id ) as product_id FROM product_attributes as t1 WHERE t1.groupid='$product_detail[18]' $clr GROUP BY size");
								if($sql){
									if(mysqli_num_rows($sql) > 0){ ?>
										<option value=""> Select Size</option>
								<?php	while($rows = mysqli_fetch_assoc($sql)){
										$product_qty = productQty($rows['product_id']);
										$lowstock = getSinglevalue('product', 'sku', $rows['product_id'], 34);
										if($product_qty > 0){ ?>
										<option value="<?php echo $rows['size'] ?>" id="<?php echo $rows['product_id'] ?>" <?php echo ($rows['product_id'] == $product_detail[5]) ? 'selected="selected"' : '' ; ?>><?php echo $rows['size'] ?> <?php 
										if(!empty($lowstock)){ if($product_qty < 4){ ?><span style="color: red">Low Stock</span> <?php } } ?></option>
									<?php	}else{
									?>
										<option value="<?php echo $rows['size'] ?>" id="<?php echo $rows['product_id'] ?>" disabled><?php echo $rows['size'] ?></option>
								<?php	} 
								}
									}
								}else{
								echo mysqli_error($conn);
								}
								?>
								</select>
							</div>
						</div>
						<!-- SIZE ENDS -->
					<?php } ?>
							
					</div>
					<div class="addToBag w-100">
						<p class="mb-0"><img src="img/atcart.svg" alt=""> <input type="submit" class="addToCartBtn d-block py-3 w-100 border-0" name="addcart" value="Add to bag"></p> 
					</div>
					<div class="addToBag w-100 mt-4">
						<p class="mb-0"><img src="img/atwishlist.svg" alt=""><input type="submit" name="wishlist" class="wishlistbutton d-block py-3 w-100" value="Add to wishlist"></p> 
					</div>
					</form>
					<!-- form should be end here -->
					<div class="accordion">
						<?php if(!empty($product_detail[11])){ ?>
						<h4 class="accordion-toggle">Editor's Notes</h4>
						<div class="accordion-content accordion_content-open">
							<p><?php echo $product_detail[11] ?></p>
						</div>
						<?php } ?>
						<?php if(!empty($product_detail[12])){ ?>
						<h4 class="accordion-toggle">Size & Fit</h4>
						<div class="accordion-content">
						<?php echo $product_detail[12] ?>
						</div>
						<?php } ?>
						<?php if(!empty($product_detail[13])){ ?>
						<h4 class="accordion-toggle">Technical Specification</h4>
						<div class="accordion-content">
							<p><?php echo $product_detail[13] ?></p>
						</div>
						<?php } ?>
					</div>
					<p class="singlePTag mt-2">Product Code <?php echo $product_detail
					[5] ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<hr>
<?php 
		$sql = mysqli_query($conn, "SELECT * FROM products_db WHERE brand='$product_detail[3]' AND visible=1 AND p_id!='$product_detail[0]' ORDER BY p_id DESC LIMIT 12");
		if($sql){
		if(mysqli_num_rows($sql)){ ?>
<section class="alsoLikePage my-5">
	<div class="container">
		<div class="row">
		<div class="youMayAlsoLikeHeading">
			<h5>YOU MAY ALSO LIKE</h5>
		</div>
		<?php while($brand_products = mysqli_fetch_assoc($sql)){ ?>
			<div class="col-lg-3 col-md-6 col-xs-12 mb-5 mb-lg-0">
				<div class="alsoLikeInnPage">
					<a href="single-product?p=<?php echo $brand_products['p_key'] ?>">
						<img src="<?php echo PRODUCT_IMAGE_URL.$brand_products['p_img'] ?>" align="" class="withoutHoverImg">
					<?php if(!empty($product_detail[25])){ ?>
						<img src="<?php echo PRODUCT_IMAGE_URL.$brand_products['s_img'] ?>" class="withHoverImg">
					<?php }else{ ?>
						<img src="<?php echo PRODUCT_IMAGE_URL.$brand_products['p_img'] ?>" class="withHoverImg">
					<?php } ?>
					</a>
					<div class="categoryContent text-center pt-2">
						<p class="mb-0"><b><?php echo getSinglevalue('brands', 'brand_key', $brand_products['brand'], 3) ?></b></p>
						<p class="mb-0"><?php echo $brand_products['product_name'] ?></p>
						<p><i class="fa fa-inr"></i> <?php echo $brand_products['price'] ?>.<span>00</span></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php } } ?>
<!-- Modal -->
<div class="detailPage modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h6 class="modal-title" id="exampleModalLabel">Size guide</h6>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <div class="row">
							        	<div class="col-3">
							        		<div class="detailPopupImg1">
							        			<img src="img/productDetail/img1.jpg" width="100%" alt="">
							        		</div>
							        	</div>
							        	<div class="col-9">
							        		<div class="detailPopupImg">
							        			<h6>Oliver Spencer Loungewear</h6>
							        			<p>Cannington Gingham Cotton Robe</p>
							        			<ul class="ml-3">
													<li>Width of Strap: 2cm / 0.8in</li>
													<li>Width of Face: 4.2cm / 1.7in</li>
													<li>Max. Strap Length: 20cm / 7.9in</li>
													<li>Min. Strap Length: 16cm / 6.3in</li>
												</ul>
							        		</div>
							        	</div>
							        </div>
							      </div>
							    </div>
							  </div>
							</div>
							<!-- modal end -->
<?php 
include_once "helpers/footer.php";

flash_session_web();

if(isset($_POST['addcart'])){
	extract($_POST);
	$quantity = 1;
    if(!empty($product) && !empty($quantity)){
		$cartcolor = "";
		$cartsize = "";
		$querypart = "";
		$querypart2 = "";
        if(isset($_POST['color'])){
			$cartcolor = $color;
			$querypart = " AND t2.color='$cartcolor'";
        }
        if(isset($_POST['size'])){
			$cartsize = $size;
			$querypart2 = " AND t2.size='$cartsize'";
        }
		if(empty($id) && empty($usertoken)){
        if(isset($_COOKIE['shopping_cart'])){
            $cookie_data = stripcslashes($_COOKIE['shopping_cart']);
            $cart_data = json_decode($cookie_data, true);
        }else{
            $cart_data = [];
		}
		// product code
		$sql = mysqli_query($conn, "SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.groupid='$product' $querypart $querypart2");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$row = mysqli_fetch_assoc($sql);
				$product = $row['sku'];
			}
		}
		// else{
		// 	echo mysqli_error($conn);
		// }
        $key = rand_char(5);
        $item_array = array(
            'cartid' => $key,
            'product' => $product,
            'color' => $cartcolor,
            'size' => $cartsize,
            'qty' => $quantity
        );

        $cart_data[] = $item_array;
        $item_data = json_encode($cart_data);
        $cartadd = setcookie('shopping_cart', $item_data, time() + (172800 * 30)); // 86400 = 1 day
        if($cartadd){
			echo '<script>$.notify("Item Added in Cart Successfully", "success");</script>';
			header('location: shopping-cart');
		}
	}else{
		$key = rand_char(5);
		$sql = mysqli_query($conn, "INSERT INTO user_cart (uc_key, user, product, color, size, qty, created_at) VALUES ('$key', '$id', '$product', '$cartcolor', '$cartsize', '$quantity', '$created_at')");
		if($sql){
			// echo '<script>$.notify("Item Added in Cart Successfully", "success");</script>';
			header('location: shopping-cart');
		}else{
			echo '<script>$.notify("'.mysqli_error($conn).'", "success");</script>';
		}
	}
    }else{
        echo '<script>alert("Empty")</script>';
	}
}

// wishlist

if(isset($_POST['wishlist'])){
	extract($_POST);
	if(!empty($product)){
		$quantity = 1;
		$cartcolor = "";
        $cartsize = "";
        if(isset($_POST['color'])){
            $cartcolor = $color;
        }
        if(isset($_POST['size'])){
            $cartsize = $size;
		}

		// product code
		$sql = mysqli_query($conn, "SELECT t1.sku FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.groupid='$product' $querypart $querypart2");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$row = mysqli_fetch_assoc($sql);
				$product = $row['sku'];
			}
		}
		if(empty($id) && empty($usertoken)){
			if(isset($_COOKIE['wishlist'])){
				$cookie_data = stripcslashes($_COOKIE['wishlist']);
				$wishlist_data = json_decode($cookie_data, true);
			}else{
				$wishlist_data = [];
			}
			$wproducts = array_column($wishlist_data, 'product');
			$myval = 0;
foreach($wproducts as $prdt){
    if($prdt == $product){
        $myval = 1;
    }
}
	if($myval == 0){
		$key = rand_char(5);
        $item_array = array(
            'w_key' => $key,
            'product' => $product,
            'color' => $cartcolor,
            'size' => $cartsize,
            'qty' => $quantity
        );

        $wishlist_data[] = $item_array;
		$item_data = json_encode($wishlist_data);
		$wishadd = setcookie('wishlist', $item_data, time() + (172800 * 30)); // 86400 = 1 day
        if($wishadd){
			// echo '<script>$.notify("Item Added in Wishlist Successfully", "success");</script>';
			$_SESSION['result'] = [true, 'Item Added in Wishlist Successfully', 'success'];
			header('location: '.$_SERVER['REQUEST_URI']);
		}
	}else{
		$_SESSION['result'] = [false, 'Item already Exists in wishlist', 'error'];
		header('location: '.$_SERVER['REQUEST_URI']);
		// echo '<script>$.notify("Item already Exists in wishlist", "info");</script>';
	}
		}else{
			// server add
			$check_pdt = check_duplicate('wishlist', 'product', $product, 'user', $id);
			if(empty($check_pdt)){
			$key = rand_char(5);
			$sql = mysqli_query($conn, "INSERT INTO wishlist (w_key, user, product, color, size, qty, created_at) VALUES ('$key', '$id', '$product', '$cartcolor', '$cartsize', '$quantity', '$created_at')");
			if($sql){
				$_SESSION['result'] = [true, 'Item Added in Wishlist Successfully', 'success'];
				header('location: '.$_SERVER['REQUEST_URI']);
			}else{
				
				$_SESSION['result'] = [false, 'Item Not Added in Wishlist Successfully', 'error'];
				header('location: '.$_SERVER['REQUEST_URI']);
			}
		}else{
			$_SESSION['result'] = [false, 'Item already Exists in wishlist', 'error'];
			header('location: '.$_SERVER['REQUEST_URI']);
		}
		}
	}
}

?>
