<?php include_once"helpers/index.php"; 
client_session($id, $usertoken);
if(isset($_GET['ad'])){
	extract($_GET);
	if(!empty($ad)){
		// $sql = mysqli_query($conn, "SELECT ")
	}
}
?>
<style>
#ac-three {
    background: #fff;
    font-weight: bold;
}
</style>
<section>
	<div class="container-fluid px-0">
		<div class="accountMainDiv">
			<div id="breadcrumb">
			    <ul class="mb-0">
					<li class="d-inline-block"><a href="#">Home <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block"><a href="#">My account <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block">Address Book</li>
			    </ul>
			</div>
			<?php include_once"helpers/accountmenu.php" ?>

			<div class="accountContent pb-5">
					<div class="heading w-100 text-center my-5">
						<h2>Address Book</h2>
					</div>
					<div class="row justify-content-center m-0">
						<div class="col-lg-6 col-md-9">
									<?php 
						$sql = mysqli_query($conn, "SELECT * FROM address_book WHERE user='$id' AND status=1");
						if($sql){
							if(mysqli_num_rows($sql) > 0){
								while($rows = mysqli_fetch_assoc($sql)){ ?>
								<div class="savedAddress mb-4">
									<div class="row">
								<div class="col-8">
											<div class="addressDiv">
												<p><span>verified</span></p>
												<p><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name'] ?></p>
												<p><?php echo $rows['mobile'] ?></p>
												<p><?php echo $rows['address']." ".$rows['town'] ?></p>
												<p><?php $rows['state'] ?></p>
												<p><?php $rows['pincode'] ?></p>
												<p>India</p>
												<a href="javascript:void()" class="ad-btn" data-toggle="modal" data-target="#exampleModal" onclick="getAddress('<?php echo $rows['ab_key'] ?>')"><i class="fa fa-pencil"></i> Edit</a>
																<a href="delete?address=<?php echo $rows['ab_key'] ?>" class="ad-btn"><i class="fa fa-trash"></i> Delete</a>
											</div>
										</div>
										</div>
								</div>
					<?php		}
							}
						}
					?>
										
										
									

								<div class="addressBookContent text-center">
									<p class="my-4">You have no saved addresses, add a new address to make your next purchase even easier.</p>
									<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" onclick="formReset()">+ add a new address</a>
								</div>

						</div>
					</div>
			</div>

<!-- end of content -->
		</div>
	</div>
</section>

<?php include_once "helpers/footer.php";

if(isset($_POST['save_address'])){
	extract($_POST);
	$address = $add_line_1." ".$add_line_2;
	if(!empty($name) && !empty($mobile) && !empty($city) && !empty($state) && !empty($address) && !empty($pincode)){
		$key = rand_char(5);
		$query = mysqli_query($conn, "INSERT INTO `address_book`(`ab_key`, `user`, `title`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `created_at`) VALUES ('$key', '$id', '$title', '$name', '$last_name', '$mobile', '$address', '$city', '$state', '$pincode', '$created_at')");
		if($query){
			echo '<script>$.notify("Address Added Successfully", "success");</script>';
			header('refresh: 1');
		}else{
			echo '<script>$.notify("Address Not Added Successfully", "error");</script>';
		}
	}else{
		echo '<script>$.notify("Please fill all fields", "error");</script>';
	}
}
?>