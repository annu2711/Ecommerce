<?php include_once"helpers/index.php"; ?>
<style>
#ac-five {
    background: #fff;
    font-weight: bold;
}
</style>
<section >
	<div class="container-fluid px-0">
		<div class="accountMainDiv">
			<div id="breadcrumb">
			    <ul class="mb-0">
					<li class="d-inline-block"><a href="#">Home <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block"><a href="#">My account <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block">My preference</li>
			    </ul>
			</div>
			<?php include_once"helpers/accountmenu.php" ?>
			<div class="accountContent pb-5">
					<div class="heading w-100 text-center my-5">
						<h2>My Preferences</h2>
					</div>
					<div class="accountCont mx-3 p-4">
						<div class="row">
							<div class="col-md-7">
								<div class="myPreference">
									<p>Select your preferred communication language</p>
									<p>This language will be used on any communication from us relating to your online order(s), including emails and text messages.</p>
								</div>
							</div>
							<div class="col-md-5">
								<div class="recentOrder justify-content-start">
									<select>
										<option>English</option>
									</select>
								</div>
							</div>
						</div>
					</div>

			</div>
		</div>
	</div>
</section>

<?php include_once"helpers/footer.php" ?>