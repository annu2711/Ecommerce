<?php include_once"helpers/index.php"; ?>

<section class="mb-5 d-md-block">
    <div class="blog-banner">
        <img src="https://images.wallpapersden.com/image/download/love-and-nature_a2tuZmaUmZqaraWkpJRmbmdlrWZlbWU.jpg" width="100%" alt="">
    </div>
</section>

<section class="mb-5 d-md-block">
	<div class="container px-auto px-md-0 blogs pt-5 pb-4 ">
		<div class="row justify-content-center">
			<div class="col-xl-12 col-lg-12 col-md-12">
				<h5 class="text-center"><strong>DISCOVER MORE</strong></h5>
				<div class="row mt-4">
                    <div class="col-md-12">
                        <div class="row">
                        <?php 
                            $sql = mysqli_query($conn,"SELECT * FROM `blogs` WHERE `status`=1");
                            if(mysqli_num_rows($sql) > 0){
                                while($rows = mysqli_fetch_assoc($sql)){
                                    ?>
                                    <div class="col-md-4">
                                        <div class="blog">
                                            <div class="blog__image">
                                                <a href="blog-detail?blog=<?=$rows['blog_id']?>">
                                                    <img src="<?=WEBSITE_IMAGE_URL.$rows['blog_image'] ?>" width="100%" alt="">
                                                </a>
                                            </div>
                                            <div class="blog__author">
                                                <span><i><?=$rows['blog_author']?></i></span>
                                            </div>
                                            <div class="blog__title">
                                                <h6>
                                                    <?=$rows['blog_title']?>
                                                </h6>
                                            </div>
                                            <div class="blog_date">
                                                <span><?=time_elapsed_string($rows['created_at']); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>

                        </div>
                        
                    </div>  
                </div>
			</div>
		</div>
	</div>
</section>

<?php include_once"helpers/footer.php" ?>