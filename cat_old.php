<?php include_once"helpers/index.php";

if(isset($_GET['cat'])){
    extract($_GET);
    $urlarray = explode("_", $cat);
    $category = "";
    $subcategory = "";
    $product = "";
    $product_list = "";
    $quote = "'";
    $prf="";
    $pstatus = "";
    $catarray = explode("-",$urlarray[0]);
    if($catarray[0]!= ""){
        $category = "AND category='".$catarray[0]."'";
    }
    if($catarray[1]!= ""){
        $subcategory = "AND subcategory='".$catarray[1]."'";
    }
    if($urlarray[2]!= ""){
        $prf = "AND prf='$urlarray[2]'";
    }
    $status = explode("!",$urlarray[1]);
    if($status[1]!= ""){
        $pstatus = "AND status='$status[1]'";
    }

    $countq = mysqli_query($conn, "SELECT * FROM products_db WHERE visible=1 $category $subcategory $prf $pstatus");
    if($countq){
        $prdrows = mysqli_num_rows($countq);
        if($prdrows > 12){
            // echo "rows =>".$rows;
            $totalpages = ceil($prdrows / 12);
            echo'<input type="hidden" id="totalpages" value="'.$totalpages.'"><input type="hidden" id="currentpage" value="1">';
        }else{
            // echo "rows2 =>".$rows;
            echo'<input type="hidden" id="totalpages" value="1"><input type="hidden" id="currentpage" value="1">';
        }
    }

    $query = mysqli_query($conn, "SELECT * FROM products_db WHERE visible=1 $category $subcategory $prf $pstatus ORDER BY p_id DESC LIMIT 12");
    if($query){
        if(mysqli_num_rows($query) > 0){
            // $products_array = mysqli_fetch_array($query);
            while($products = mysqli_fetch_assoc($query)){
                // echo $products['p_id']
                if($products['s_img']!= ""){
                    $second_img = $products['s_img'];
                }else{
                    $second_img = $products['p_img'];
                }
                $product = $product.'
                <div class="col-xl-4 col-sm-6 col-12">
								<div class="categoryOuter">
									<div class="categoryOuterImg>
										<a href="single-product?p='.$products['p_key'].'">
											<img src="admin/products/'.$products['p_img'].'" class="imgWithOutHover img-fluid" alt="">
											<img src="admin/products/'.$products['s_img'].'" class="imgWithHover img-fluid" alt="">
										</a>
									</div>
									<div class="categoryButton">
										<div class="row mobileNone ">
										<div class="col-6">
											<div class="categoryBtn">
												<p  onclick="productQuickView('.$quote.$products['p_key'].$quote.')"><a class="button btnHover" data-toggle="modal" data-target="#exampleModalCenter" href="javascript:void(0)">
													<span>
														<img src="img/zoom-in.svg" alt="">
														<img src="img/zoom-in-grey.svg" alt="">
													</span> Quick Shop</a></p> 
											</div>
										</div>
										<div class="col-6">
											<div class="categoryBtn">
											<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
											<input type="hidden" name="product" value="'.$products['p_key'].'">
												<p><button type="submit" name="wishlist" class="whislist btnHover">
													<span>
														<img src="img/heart.svg" alt="">
														<img src="img/heart-grey.svg" alt="">
													</span> Wish list</button>
												</p> 
											</form>
											</div>
										</div>
									</div>
									<div class="categoryContent">
										<a href="single-product?p='.$products['p_key'].'"><h6>'.getSinglevalue('brands', 'brand_key', $products['brand'], 3).'</h6>
										<p>'.$products['product_name'].'</p>
										</a>
										<p class="price"><i class="fa fa-inr"></i> '.$products['price'].'.<span>00</span></p>
									</div>
									</div>
								</div>
							</div>';
            }
        }else{
            $error = '<p>Products are not available</p>';
        }
    }

}else{
    header('location: index');
}

?>
<input type="hidden" id="filter" value="0">
<section class="category py-5">
	<div class="container">
		<div class="row">
			<!-- filter on mobile -->
					<div class="mobilefilter">
						<div class="mobileFilterInn text-center">
							<div class="row align-items-center">
								<div class="col-3"></div>
								<div class="col-6">
									<h6>More refine option</h6>
									<p class="mb-0"><span>2654 </span>Product</p>
								</div>
								<div class="col-3 text-right">
									<a class="closefilter mr-4" href="javascript:void()"><span><i class="fa fa-times" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<ul>
							<li><a class="womenCategory" href="javascript:void(0)">Womens <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
							<li><a class="womenCategory" href="javascript:void(0)">Refine by Brand <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
						</ul>
					</div>
			<!-- filter on mobile end -->

			<div class="col-lg-2">
				<div class="filterMainDiv">


                <!-- category-area-start -->
                <?php
                    if($catarray[0]!=""){
                    $query = mysqli_query($conn, "SELECT DISTINCT(a.subcategory) as subcat, (SELECT COUNT(*) FROM products_db WHERE subcategory=a.subcategory) as rc FROM `products_db` as a WHERE visible=1 AND category='$catarray[0]'");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
					<div class="filterCategory">
						<div class="mobileFilterInn text-center d-block d-lg-none">
							<div class="row align-items-center">
								<div class="col-3">
									<a class="backtocategory" href="javascript:void(0)"><span class="d-block d-lg-none"><i class="fa fa-angle-left" aria-hidden="true"></i></span></a>
								</div>
								<div class="col-6">
									<h6><?php echo getSinglevalue('category', 'cat_key', $catarray[0], 2); ?> </h6>
									<!-- <p class="mb-0"><span>2654 </span>Product</p> -->
								</div>
								<div class="col-3 text-right">
									<a class="closefilter mr-4" href="javascript:void()"><span><i class="fa fa-times" aria-hidden="true"></i></span></a>
								</div>
							</div>
                        </div>
                            <h6 class="mb-4 d-none d-lg-block"><?php echo getSinglevalue('category', 'cat_key', $catarray[0], 2); ?> </h6>
                            <?php  while($rows = mysqli_fetch_assoc($query)){ ?>
                                <p><a href="cat?cat=<?php echo $catarray[0] ?>-<?php echo $rows['subcat'] ?>_st!_"><?php echo getSinglevalue('subcategory', 'subcat_key', $rows['subcat'], 3); ?> <span>(<?php echo $rows['rc'] ?>)</span> <span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
                                    <?php } ?>
                    <?php }
                                    }
                                }else{
                                    $query = mysqli_query($conn, "SELECT DISTINCT(a.category) as cat, (SELECT COUNT(*) FROM products_db WHERE category=a.category) as rc FROM `products_db` as a WHERE visible=1");
                                    if($query){
                                        if(mysqli_num_rows($query) > 0){ ?>
                                <?php  while($rows = mysqli_fetch_assoc($query)){ ?>
                                    <p><a href="cat?cat=<?php echo $rows['cat'] ?>-_st!_"><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?> <span>(<?php echo $rows['rc'] ?>)</span> <span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
                                    <?php } ?>
                                <?php   }
                                    }
                                }
                            ?>
                        <!-- category-area-end -->
                    </div>
                    
                    <!-- brand-area-start -->
                    <?php 
                        $pass = 0;
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b, (SELECT COUNT(*) FROM products_db WHERE brand=a.brand) as brd FROM products_db as a WHERE visible=1 AND subcategory='$catarray[1]' AND brand!=''");
                                $pass = 1;
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b, (SELECT COUNT(*) FROM products_db WHERE brand=a.brand) as brd FROM products_db as a WHERE visible=1 AND category='$catarray[0]' AND brand!=''");
                                $pass = 1;
                            }

                            if($pass == 1){
                                if($query){
                                    if(mysqli_num_rows($query) > 0){ ?>
                            <div class="refinedBrand">
						<div class="mobileFilterInn text-center d-block d-lg-none">
							<div class="row align-items-center">
								<div class="col-3">
									<a class="backtocategory" href="javascript:void(0)"><span class="d-block d-lg-none"><i class="fa fa-angle-left" aria-hidden="true"></i></span></a>
								</div>
								<div class="col-6">
									<h6>Refine By Brand</h6>
									<!-- <p class="mb-0"><span>2654 </span>Product</p> -->
								</div>
								<div class="col-3 text-right">
									<a class="closefilter mr-4" href="javascript:void()"><span><i class="fa fa-times" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<h6 class="my-4 d-none d-lg-block">Refine By Brand</h6>
						<form>
									<div class="input-group inputSearch">
										<input type="text" class="searchInput" placeholder="Search for brands" name="">
										<div class="input-group-append">
											<img src="img/search.svg" alt="">
										</div>
									</div>
									<div class="scrolly mt-3">
                                    <?php while($rows = mysqli_fetch_assoc($query)){ ?>

                                        <div class="checkboxGroup my-lg-3">
											<input type="checkbox" aria-label="Checkbox for following text input" name="brand[]" value="<?php echo $rows['b'] ?>">
											<p><a href="javascript:void(0)"><?php echo getSinglevalue('brands', 'brand_key', $rows['b'], 3); ?> <span>(<?php echo $rows['brd'] ?>)</span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>

                                     <?php } ?>
									</div>
						</form>

						
					</div>
                                <?php }
                                }
                            }
                            
                            
                            ?>
                        <!-- brand-area-end -->
					
				</div>
			</div>
			<div class="col-lg-10">
				<div class="row">
					<div class="col-lg-8 col-6">
						<div class="navigatePage align-items-end d-flex h-100">
							<ul class="mb-0 d-none d-lg-block">
								<li class="d-inline">Home /</li>
								<li class="d-inline">Women's</li>
							</ul>
							<div class="d-block d-lg-none w-100">
								<p class="mb-0"><a class="refineBtn" href="javascript:void()">Refine</a></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-6">
						<div class="categoryDropdown">
							<form>
									<div class="inputSearch">
										<label>Sort by</label><br>
										<select class="searchInput" >
										    <option selected>Choose...</option>
										    <option value="1">One</option>
										    <option value="2">Two</option>
										    <option value="3">Three</option>
										  </select>
									</div>
								</form>
						</div>
					</div>
					<div class="col-12 my-5">
						<div class="text-center result">
							<p><span id="onpage"><?php echo ($prdrows <= 12) ? $prdrows : 12 ;  ?></span>/<span><?php echo $prdrows; ?></span> results</p>
						</div>
					</div>
					<div class="col-12">
						<div class="row" id="grid">
                        <?php 
                        if(!empty($product)){ 
                            echo $product;
                        }else{
                            echo $error;
                        }
                        ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php 
include_once"helpers/footer.php";
?>