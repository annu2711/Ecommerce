<?php 
include_once "helpers/index.php"; 
	$email = "";
	if(empty($id) && empty($usertoken)){
	if(isset($_COOKIE['guest'])){
		$email = stripcslashes($_COOKIE['guest']);
		$user = getSingleValue('clients', 'email', $email, 0);
		$sql = mysqli_query($conn, "SELECT * FROM address_book WHERE user='$user' ORDER BY ab_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$addressarr = mysqli_fetch_array($sql); 
			}
		}else{
			echo mysqli_error($conn);
		}
	}
	}


?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
			<form action="<?php $_SERVER['PHP_SELF'] ?>" name="usersave" method="post">
				<div class="information7 p-0 px-lg-5 px-md-5">
					<?php if(empty($id) && empty($usertoken)){ ?>
					<div class="contactInformationForm mt-5">
						<div class="row align-items-center">
							<div class="col-md-6">
								<div class="formHeading">
									<h2>Contact information</h2>
								</div>
							</div>
							<div class="col-md-6">
								<div class="alreadyAccount text-right">
									<p>Already have an account? <span><a href="login.php">Log in</a></span></p>
								</div>
							</div>
						</div>
						<div class="formGroupInput">
							<label>Email</label>
							<input type="email" class="form-control" name="email" value="<?php echo $email ?>" required>
						</div>
						<div class="exclusiveOffers">
							<input type="checkbox" name="newsletter" value="1">
							<p class="mb-0 ml-1">Keep me up to date on news and exclusive offers</p>
						</div>
					</div>
					<?php }else{ ?>
						<input type="hidden" class="form-control" name="email" value="<?php echo $userdata[5] ?>" required>
					<?php } ?>
					<div class="contactInformationForm contactInformationForm2 mt-5">
						<div class="row">
							<div class="col-md-12">
								<div class="formHeading">
									<h2>Shipping address</h2>
								</div>
							</div>
							
					<div class="col-md-12">
						<!-- <input type="hidden" name="newaddress" id="newaddress" value="<?php // echo (empty($id)) ? '1' : '0' ?>"> -->
						<div class="row enterNewAddress">
							<div class="col-md-6 mt-3">
								<div class="formGroupInput">
									<label>First Name<sup class="text-danger">*</sup></label>
									<input type="text" class="form-control required" id="first-name" name="fname" value="<?php echo (!empty($addressarr)) ? $addressarr[4] : '' ; ?>" required>
								</div>
							</div>
							<div class="col-md-6 mt-3">
								<div class="formGroupInput">
									<label>Last Name</label>
									<input type="text" class="form-control" id="last-name" name="lname" value="<?php echo (!empty($addressarr)) ? $addressarr[5] : '' ; ?>">
								</div>
							</div>
							<div class="col-md-12 mt-3">
								<div class="formGroupInput">
									<label>Address<sup class="text-danger">*</sup></label>
									<input type="text" class="form-control required" id="line-a" name="address" value="<?php echo (!empty($addressarr)) ? $addressarr[7] : '' ; ?>" required>
								</div>
							</div>
							<div class="col-md-6 mt-3">
								<div class="formGroupInput">
									<label>City<sup class="text-danger">*</sup></label>
									<input type="text" class="form-control required" id="add-city" name="city" value="<?php echo (!empty($addressarr)) ? $addressarr[8] : '' ; ?>" required>
								</div>
							</div>
							<div class="col-md-6 mt-3">
								<div class="formGroupInput">
									<label>Country/Region<sup class="text-danger">*</sup></label>
									<select class="form-control required" name="country" required>
										<option value="india">India</option>
									</select>
								</div>
							</div>
							<div class="col-md-4 mt-3">
								<div class="formGroupInput">
									<label>States<sup class="text-danger">*</sup></label>
									<select class="form-control required" id="add-state" name="state" required>
										<?php 
										$statevalue = (!empty($addressarr)) ? $addressarr[9] : '' ;
										?>
										<?php $statearr = [ "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttarakhand", "Uttar Pradesh", "West Bengal", "Andaman and Nicobar Islands", "Chandigarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Lakshadweep", "Puducherry"]; 
										for($i = 0; $i <= count($statearr) - 1; $i++){ ?>
											<option value="<?php echo $statearr[$i] ?>" <?php echo ($statevalue == $statearr[$i]) ? 'selected="selected"' : '' ; ?>><?php echo $statearr[$i] ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-4 mt-3">
								<div class="formGroupInput">
									<label>Zip Code<sup class="text-danger">*</sup></label>
									<?php // echo ($update) ? 'value="'.$shopping_data['pincode'].'"' : 'placeholder="zip code"'; ?>
									<input type="number" class="form-control required" id="add-pincode" name="pincode" value="<?php echo (!empty($addressarr)) ? $addressarr[10] : '' ; ?>" required>
								</div>
							</div>
							<div class="col-md-4 mt-3">
								<div class="formGroupInput">
									<label>Phone<sup class="text-danger">*</sup></label>
									<input type="number" class="form-control required" id="mobile" name="mobile" value="<?php echo (!empty($addressarr)) ? $addressarr[6] : '' ; ?>" required>
								</div>
							</div>
							<div class="col-md-12"><p class="text-danger add-error"></p></div>
						</div>

					</div>


						</div>
					</div>

					<div class="continueShipping mt-4 mb-5">
						<div class="row align-items-center">
							<div class="col-md-6">
								<div class="returnToCart">
									<a href="shopping-cart.php"><img src="img/sizeGuide.svg" alt="">&nbsp; Return to cart</a>
								</div>
							</div>
							<div class="col-md-6">
								<div class="alreadyAccount text-right">
									<button type="button" class="default-btn" onclick="saveAddress('0')">Submit</button>
								</div>
							</div>
						</div>
					</div>

				</div>
				</form>
			</div>


			<div class="col-md-5 pt-5 mb-4 px-5">
                <div class="order-summary">
                    <div class="row checkoutOut px-3 align-items-center">
                        <div class="orderSummary">
                            <h2>Order summary</h2>
                        </div>
                        <div class="editSummary">
                            <a href="shopping-cart.php"><span><i class="fas fa-pencil-alt"></i> </span>Edit</a>
                        </div>
                    </div>

                    <div class="order-summary-inn">
                        <div class="row">
                            <div class="col-8">
                                <p>My bag (<?php echo (empty($directbuy)) ? $totalcartquantity : $dtbuy[6] ; ?> items)</p>
                            </div>
                            <div class="col-4">
                                <div class="viewToggle">
                                    <p class="mb-0 viewtag">View <span><i class="fa fa-angle-down"></i></span></p>
	                                <p class="mb-0 hidetag">Hide <span><i class="fa fa-angle-up"></i></span></p>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
						<?php  if(!empty($cart_data)){
                            			for($i=0; $i < count($cart_data); $i++){ ?>
										<div class="wishlistmain slideUpDown border-0">
											<div class="wishlistImg checkoutImg">
												<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $cart_data[$i]['product'], 2)) ?>">
											</div>
											<div class="wishlistCont checkoutCont">
												<P class="mb-0 mt-2"><?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 4) ?> <?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 21) ?></P>
												<p class="mb-0">Quantity: <?php echo $cart_data[$i]['qty'] ?></p>
												<p class="mb-0"><i class="fas fa-rupee-sign"></i> 
												<?php 
												echo $prc = getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10);
												
												?>
												</p>
											</div>
										</div>
							<?php 
										}
									}
							?>
							<div class="col-md-6">
								<div class="totalPrice">
									<p class="mb-0">Subtotal</p>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<div class="totalPrice">
									<p class="mb-0"><i class="fa fa-inr"></i>
										<?php
											echo $totalcartamount;
										?>
									</p>
								</div>
							</div>
										
                            <div class="col-md-6">
								<div class="totalPrice">
									<p class="mb-0">Total</p>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<div class="totalPrice">
									<p class="mb-0"><i class="fa fa-inr"></i>
                                    <?php echo $totalcartamount ?>
                                    </p>
								</div>
						    </div>
                </div>
            </div>

		</div>
		</div>
	</div>
<?php 

include_once "helpers/footer.php";
flash_session_web();

function getUserByEmail($email){
	$conn = $GLOBALS['conn'];
	if(!empty($email)){
		$sql = mysqli_query($conn, "SELECT client_id FROM clients WHERE email='$email' ORDER BY client_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$row = mysqli_fetch_assoc($sql);
				return $row['client_id'];
			}else{
				return null;
			}
		}
	}
}

function addAdress($type, $user, $name, $lname, $mobile, $addresss, $city, $state, $pincode){
	$conn = $GLOBALS['conn'];
if($type == '0'){
	$key = rand_char(5);
	try{
		$stmt = $conn->prepare("INSERT INTO `address_book`(`ab_key`, `user`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('ssssssssss', $key, $user, $name, $lname, $mobile, $addresss, $city, $state, $pincode, $created_at);
		if($stmt->execute()){
			return 200;
		}else{
			throw new exception($conn->error);
		}
	}catch(Exception $e){
		return $e->getMessage();
	}
}else{
	try{
		$stmt = $conn->prepare("UPDATE address_book set name=?, last_name=?, mobile=?, address=?, town=?, state=?, pincode=? WHERE user=?");
		$stmt->bind_param('ssssssss', $name, $lname, $mobile, $addresss, $city, $state, $pincode, $user);
		if($stmt->execute()){
			return 200;
		}else{
			throw new exception($conn->error);
		}
	}catch(Exception $e){
		return $e->getMessage();
	}
}
}

if(isset($_POST['email'])){
	extract($_POST);
	// echo $email."=".$fname."=".$address."=".$city."=".$state."=".$country."=".$pincode."=".$mobile;
	if(!empty($email) && !empty($fname) && !empty($address) && !empty($city) && !empty($state) && !empty($country) && !empty($pincode) && !empty($mobile)){
		if(empty($id) && empty($usertoken)){
			$user = getUserByEmail($email);
			if(!empty($user)){
				$checkadd = check_duplicate('address_book', 'user', $user);
				$type = empty($checkadd) ? 0 : 1 ;
				$addresss = addAdress($type, $user, $fname, $lname, $mobile, $address, $city, $state, $pincode);
				if($addresss == 200){
					$saveaddress = setcookie('guest', $email, time() + (2592000 * 30)); // 86400 = 1 day
					header('location: checkout.php');
				}else{
					echo '<script>$.notify("Address not saved", "error")</script>';
				}
			}else{
				try{
					$stmt = $conn->prepare("INSERT INTO clients (email, created_at) VALUES (?, ?)");
					$stmt->bind_param('ss', $email, $created_at);
					if($stmt->execute()){
						$user = mysqli_insert_id($conn);
						// echo $user;
						$address = addAdress(0, $user, $fname, $lname, $mobile, $address, $city, $state, $pincode);
						if($address == 200){
							echo $address;
							$saveaddress = setcookie('guest', $email, time() + (2592000 * 30)); // 86400 = 1 day
							header('location: checkout.php');
						}else{
							echo '<script>$.notify("Address not saved", "error")</script>';
						}
					}else{
						throw new exception($conn->error);
					}
				}catch(Exception $e){
					echo mysqli_error($conn);
					echo '<script>$.notify("Server Error", "error")</script>';
				}
			}
		}else{
				$checkadd = check_duplicate('address_book', 'user', $userdata[0]);
				$type = empty($checkadd) ? 0 : 1 ;
				$addresss = addAdress($type, $userdata[0], $fname, $lname, $mobile, $address, $city, $state, $pincode);
				if($addresss == 200){
					$saveaddress = setcookie('guest', $email, time() + (2592000 * 30)); // 86400 = 1 day
					header('location: checkout.php');
				}else{
					echo '<script>$.notify("Address not saved", "error")</script>';
				}
		}
	}else{
		echo '<script>alert("Fields are empty")</script>';
	}
}
?>