<?php include_once"helpers/index.php" ?>
<section class="middle_part py-5">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="heading w-100 text-center">
               <h2>Return to store</h2>
            </div>
            <div class='main-cont'>
               <p><span style="font-size: small;"><strong>1. How do I return a product I don&rsquo;t want any more?</strong></span></p>
               <p><span style="font-size: small;">You can return the product either by placing a request on the lSmart website, by self-couriering it, or take the product to the nearest partner store that accepts returns.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>2. How do I return my products to a lSmart partner store?</strong></span></p>
               <p><span style="font-size: small;">Find a store near you that accepts returns. You can also&nbsp;call&nbsp;customer care to find a store near you.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>3. If I choose the return to store option, who will I get my refund?</strong></span></p>
               <p><span style="font-size: small;">You will be issued a credit note immediately (this is, of course, subject to seller returns/credit note policy). Use it to buy anything else you like in the store. Unfortunately, for products returned to store, we cannot refund the money back to source (read: your credit card or bank account).</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>4. How long is a store-issued credit note valid for?</strong></span></p>
               <p><span style="font-size: small;">It depends on from seller to seller. Also note that the existing policy/terms and conditions of the seller will be applicable when it comes to redeeming store credit as well.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>5. Can I use the seller credit note to buy something online on lSmart?</strong></span></p>
               <p><span style="font-size: small;">The credit note can be redeemed in the seller&rsquo;s store only. It cannot be used to buy a product on the lSmart website.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>6. How do I return something I bought online in a store?</strong></span></p>
               <p><span style="font-size: small;">You should return the product in its original and unused condition, along with all the original price tags, labels, packing, barcodes, user manuals, warranty cards and invoices (physical/digital copy), accessories, freebies and boxes received.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>7. What if I walk into the store without proof of purchase?</strong></span></p>
               <p><span style="font-size: small;">It is better if you can carry the proof of purchase. However if you don&rsquo;t have it on you, the refund through credit note will be issued subject to the seller&rsquo;s credit note policy.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>8. When are in-store returns not possible?</strong></span></p>
               <p><span style="font-size: small;">While do accept most products, do read the returns policy for both lSmart and the seller. A quality check of the product will be carried out by the store staff before a return is accepted.</span></p>
               <p><span style="font-size: small;">We don&rsquo;t want you to settle with something you don&rsquo;t like. But there are certain products we can&rsquo;t take back. Here are some of them:</span></p>
               <p><span style="font-size: small;">Innerwear, lingerie, socks, clothing freebies and swimsuits</span></p>
               <p><span style="font-size: small;">Perfumes, personal and beauty care products</span></p>
               <p><span style="font-size: small;">Products that have already been used or installed</span></p>
               <p><span style="font-size: small;">Products that have been tampered with or are missing serial numbers</span></p>
               <p><span style="font-size: small;">Personalised or engraved items</span></p>
               <p><span style="font-size: small;">Heads up! We can only accept returns within the first 30 days (for non-electronic items) of you having received the product for the first time. So make sure you do it as soon as you can.</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>9. Can I return a part of my order?</strong></span></p>
               <p><span style="font-size: small;">You sure can! Just select the product(s) you want to return from your order and initiate the return right away. You should know that we can&rsquo;t accept returns of incomplete product combos (Example: &ldquo;Buy one get one free&rdquo;, free gifts, etc.).</span></p>
               <p><br /> </p>
               <p><span style="font-size: small;"><strong>10. When I&rsquo;m returning a product, do I have to return the free gift that I got with it as well?</strong></span></p>
               <p><span style="font-size: small;">Yes, you have to. Any freebies that you got with a product will also need to be returned along with the original product.</span></p>
               <p><br /> </p>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include_once"helpers/footer.php" ?>