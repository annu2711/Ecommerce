<?php 
include_once "helpers/index.php"; 
include_once "helpers/shiprocket.php";
if(empty($id) && empty($usertoken)){
	$_SESSION['url'] = 'checkout';
	// header('location: login');
}else{	
	if(isset($_SESSION['url'])){
		unset($_SESSION['url']);
	}
}
if($totalcartquantity < 1){
	header('location: index');
}
?>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
<section class="shopping-cart my-5">
        <div class="main-content-area">
            <div class="container">
                <div class="shipping-area">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row no-gutters align-items-center shopping-bag shopping-bag-sec">
                                <div class="col-md-8 col-10">
                                    <h2>Secure checkout</h2>
                                </div>
                                <div class="col-md-4 col-2 text-right">
                                    <p class="mb-0">*Required fields</p>
                                </div>
                            </div>
                            <div class="shipping-content border-0">
                                <div class="row">
                                    <div class="checkoutProcess">
                                    	<p>Delivery</p>
                                    </div>
									<?php if(empty($usertoken) && empty($id)){ ?>
									<div class="chooseMethod">
                                    	<p>Checkout as guest</p>
                                    </div>
									<div class="col-md-12">
										<input type="email" class="form-control" name="guest" placeholder="Your Email Address" required>
										<p class="float-right">If you are already a customer so <a href="login.php">Login</a></p>
									</div>
									<?php } ?>
                                    <div class="chooseMethod">
                                    	<p>Choose your delivery method</p>
                                    </div>
                                    <div class="col-md-6">
                                    	<div class="deliveryMethod">
	                                    	<label class="mb-0"><input type="radio" class="mr-3" value="homedelivery" name="delivery_type" checked /><img src="img/van.svg" class="mr-3">Delivery</label>
	                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    	<div class="deliveryMethod">
	                                    	
	                                    	<label class="mb-0"><input type="radio" disabled="disabled" class="mr-3" value="collect" name="delivery_type"/><img src="img/hand.svg" class="mr-3">Click & Collect</label>
	                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                    	<div class="addressBookPopup deliverRadio deliverMethodPopup">
											<!-- Previously added addresses starts -->
											<ul>
											<?php 
											if(!empty($usertoken) && !empty($id)){
												$sql = mysqli_query($conn, "SELECT * FROM address_book WHERE user='$id' AND status=1");
												if($sql){
													if(mysqli_num_rows($sql) > 0){
														$exnum = 0;
														while($rows = mysqli_fetch_assoc($sql)){
															$ex_r = $exnum++;
															// echo $rows['user'];
															?>
														<li class="addressList">
														<label>
															<div class="checkoutBorder" <?php echo ($ex_r == 0) ? 'style="border-color: #ffe256"' : ""; ?>>
																<input type="radio" <?php echo ($ex_r == 0) ? 'checked="checked"' : ""; ?> name="ship_address" value="<?php echo $rows['ab_key'] ?>"><br>
																<p class="fontWeight"><?php echo $rows['title']." ".$rows['name']." ".$rows['last_name']?></p>
																<p><?php echo "Contact No.".$rows['mobile']."<br>".$rows['address']." ".$rows['town']." ".$rows['state']." Pincode:".$rows['pincode'] ?></p>
																<a href="javascript:void()" class="ad-btn" data-toggle="modal" data-target="#exampleModal" onclick="getAddress('<?php echo $rows['ab_key'] ?>')"><i class="fa fa-pencil"></i> Edit</a>
																<a href="lsmartbk/delete?address=<?php echo $rows['ab_key'] ?>" class="ad-btn"><i class="fa fa-trash"></i> Delete</a>
															</div>
														</label>
														
														</li>
											<?php		} ?>
											</ul>
											<label href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" onclick="formReset()">Add Another Address</label><br>
											<div class="addressform">
											</div>
											
											<?php }else{ ?>
											<div class="checkoutProcess">
                                    			<p>Shipping Address</p>
                                    		</div>
											<?php include "helpers/address-form.php";
													}
												}
											}else{ 
												include "helpers/address-form.php";
											}
											?>
											<label><input type="checkbox" id="bill_add" onclick="openBillingBlock('bill_add', 'billing-div')" name="bill_add" value="same" checked> Billing Address same as Shipping</label>
											


											<!-- Previously added addresses ends -->
								        		
								        	</div>
								        
                                    </div>
									<div class="billing-div w-100"></div>
                                    <div class="checkoutProcess mt-5">
                                    	<p>2. Payment</p>
                                    </div>
                                     <div class="col-md-6">
                                    	<div class="paymentMode">
	                                    	
	                                    	<label class="mb-0"><input type="radio" class="mr-3" name="payment" value="COD" onclick="orderBlock('confirmOrder', 'paymentNow')"/><img src="img/payment.svg" class="mr-3">Card on Delivery</label>
	                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    	<div class="paymentMode">
	                                    	<label class="mb-0"><input type="radio" class="mr-3" value="paynow" name="payment" onclick="orderBlock('confirmOrder', 'paymentNow')"/><img src="img/money.svg" class="mr-3">Pay now</label>
	                                    </div>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="order-summary">
                                <div class="row checkoutOut px-3">
                                    <div class="orderSummary">
                                        <h2>Order summary</h2>
                                    </div>
                                    <div class="editSummary">
                                    	<a href="shopping-cart"><span><img src="img/edit.svg" class="mr-1"></span>Edit</a>
                                    </div>
                                </div>
                                <div class="order-summary-inn">
                                    <div class="row">
                                    	<div class="col-4">
                                    		 <p>My bag (<?php echo $totalcartquantity; ?> items)</p>
                                    	</div>
                                    	<div class="col-4">
                                    		 <p><i class="fa fa-inr"></i> <?php echo $totalcartamount ?></p>
                                    	</div>
                                    	<div class="col-4">
                                    		 <div class="viewToggle">
	                                    		 <p class="mb-0 viewtag">View <span><i class="fa fa-angle-down"></i></span></p>
	                                    		 <p class="mb-0 hidetag">Hide <span><i class="fa fa-angle-up"></i></span></p>
                                    		 </div>
                                    	</div>
                                    </div>
                                    <div class="row mb-3">
										<?php 
										if(!empty($cart_data)){
                            			for($i=0; $i <= count($cart_data) - 1; $i++){ ?>
										<div class="wishlistmain slideUpDown border-0">
											<div class="wishlistImg checkoutImg">
												<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $cart_data[$i][3], 2)) ?>">
											</div>
											<div class="wishlistCont checkoutCont">
												<h6><?php echo getSinglevalue('brands', 'brand_key', getSinglevalue('product', 'sku', $cart_data[$i][3], 3), 3) ?></h6>
												<P class="mb-0"><?php echo getSinglevalue('product', 'sku', $cart_data[$i][3], 4) ?></P>
												<p>Quantity: <?php echo $cart_data[$i][6] ?></p>
												<p class="mb-0"><i class="fa fa-inr"></i> <?php echo getSinglevalue('product', 'sku', $cart_data[$i][3], 10) ?>.00</p>
											</div>
										</div>
										<?php } 
										} ?>


										<div class="col-md-6">
											<div class="totalPrice">
												<p class="mb-0">Total</p>
											</div>
										</div>
										<div class="col-md-6 text-right">
											<div class="totalPrice">
												<p class="mb-0"><i class="fa fa-inr"></i> <?php echo $totalcartamount ?>.00</p>
											</div>
										</div>	
										<div class="col-12">
											<div class="paymantOption">
												<input type="submit" class="confirmOrder" name="place_order" value="Place Order">
												<input type="submit" class="paymentNow" name="place_order" value="Payemnt Now">
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- shipping area-end -->
            </div>
        </div>
</section>
</form>

<?php include_once "helpers/footer.php";



function getUserByEmail($email){
	$conn = $GLOBALS['conn'];
	if(!empty($email)){
		$sql = mysqli_query($conn, "SELECT client_key FROM clients WHERE email='$email' ORDER BY client_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$row = mysqli_fetch_assoc($sql);
				return $row['client_key'];
			}else{
				return "null";
			}
		}
	}
}

function addAdress($type, $user, $name, $lname, $mobile, $addresss, $city, $state, $pincode){
	$conn = $GLOBALS['conn'];
if($type == '0'){
	$key = rand_char(5);
	try{
		$stmt = $conn->prepare("INSERT INTO `address_book`(`ab_key`, `user`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('ssssssssss', $key, $user, $name, $lname, $mobile, $addresss, $city, $state, $pincode, $created_at);
		if($stmt->execute()){
			return 200;
		}else{
			throw new exception($conn->error);
		}
	}catch(Exception $e){
		return $e->getMessage();
	}
}else{
	try{
		$stmt = $conn->prepare("UPDATE address_book set name=?, last_name=?, mobile=?, address=?, town=?, state=?, pincode=? WHERE user=?");
		$stmt->bind_param('ssssssss', $name, $lname, $mobile, $addresss, $city, $state, $pincode, $user);
		if($stmt->execute()){
			return 200;
		}else{
			throw new exception($conn->error);
		}
	}catch(Exception $e){
		return $e->getMessage();
	}
}
}



if(isset($_POST['place_order'])){
	
	extract($_POST);
	$checkoutuser = false;
	mysqli_autocommit($conn, FALSE);
	if(empty($usertoken) && empty($id)){
		echo "Hii1";
		if(empty($guest)){
			echo '<script>$.notify("Please fll your email as guest user", "error");</script>';
		}else{
			$user = getUserByEmail($guest);
			echo $user."<br>";
			if(!empty($user)){
				$checkadd = check_duplicate('address_book', 'user', $user);
				echo $checkadd."<br>";
				$type = empty($checkadd) ? 0 : 1 ;
				$addresss = addAdress($type, $user, $fname, $lname, $mobile, $address, $city, $state, $pincode);
				echo $addresss."<br>";
				if($addresss == 200){
					$saveaddress = setcookie('guest', $email, time() + (2592000 * 30)); // 86400 = 1 day
					header('location: checkout.php');
				}else{
					echo '<script>$.notify("Address not saved", "error")</script>';
				}
			}else{
				$key = rand_char(5);
				try{
					$stmt = $conn->prepare("INSERT INTO clients (client_key, email, created_at) VALUES (?, ?, ?)");
					$stmt->bind_param('sss', $key, $email, $created_at);
					if($stmt->execute()){
						$user = $key;
						// echo $user;
						$address = addAdress(0, $user, $fname, $lname, $mobile, $address, $city, $state, $pincode);
						if($address == 200){
							$saveaddress = setcookie('guest', $email, time() + (2592000 * 30)); // 86400 = 1 day
							header('location: checkout.php');
						}else{
							echo $address;
							echo '<script>$.notify("Address not saved", "error")</script>';
						}
					}else{
						throw new exception($conn->error);
					}
				}catch(Exception $e){
					echo '<script>$.notify("Server Error", "error")</script>';
				}
			}
		}
	}else{
		$checkoutuser = true;
	}
	echo  "Hii2";
	// $billing_address = "";
	if(!empty($delivery_type) && !empty($payment) && !empty($checkoutuser)){
		if($delivery_type == 'homedelivery'){

			// billing address starts
			if($bill_add == 'same'){
				$billing_address = $ship_address;
			}else{
				// echo "<script>alert('m in')</script>";
				$address = $add_line_1." ".$add_line_2;
				$billkey = rand_char(5);
				if(!empty($name) && !empty($mobile) && !empty($city) && !empty($state) && !empty($address) && !empty($pincode)){
					try{
						$stmt = $conn->prepare("INSERT INTO billing_addresses (ba_key, user, title, name, last_name, email, mobile, address, town, state, pincode, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
						$stmt->bind_param("ssssssssssss", $billkey, $id, $title, $name, $last_name, $email, $mobile, $address, $city, $state, $pincode, $created_at);
						if ($stmt->execute()) {
							$billing_address = $billkey;
						}else{
							// echo $conn->error;
							mysqli_rollback($conn);
							throw new exception($conn->error);
						}
						$stmt->close();
					}catch(Exception $e){
						
						echo "Error: " . $e->getMessage();
					}
				}else{
					echo '<script>$.notify("Please fill all fields of billing address.", "error");</script>';
				}
			}
			// billing address ends
			// save order here starts
			$order_key = rand_char(5);
			try{
				$payment_status = 'pending';
				$stmt = $conn->prepare("INSERT INTO orders (order_key, user, total_items, amount, billing_address, shipping_address, payment, payment_status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
				$stmt->bind_param("sssssssss", $order_key, $id, $totalcartquantity, $totalcartamount, $billing_address, $ship_address, $payment, $payment_status, $created_at);
				if ($stmt->execute()) {
					// echo "Order Done<br>";
					$success = 0;
						$sql = mysqli_query($conn, "SELECT * FROM user_cart WHERE user='$id'");
						if($sql){
							if(mysqli_num_rows($sql) > 0){
								$success = 0;
								while($rows = mysqli_fetch_assoc($sql)){

								$savecart = false;
								if(!empty($guest_cookie_cart_array)){
									if(in_array($rows['uc_key'], $guest_cookie_cart_array)){
										$savecart = true;
									}
								}else{
									$savecart = true;
								}
								if($savecart){
									
									$challanno = "";
									$chn = "";
									// fetch po no starts
									$productid = $rows['product'];
									$soldquantity = orderedQty($productid) +  $rows['qty'];
									$query = mysqli_query($conn, "SELECT challan, qty FROM product_stock WHERE product='$productid'");
									if($query){
										if(mysqli_num_rows($query) > 0){
											while($porows = mysqli_fetch_assoc($query)){
												echo $challanno;
												if(empty($challanno)){
												if($soldquantity > $porows['qty']){
													$chn = $soldquantity - $porows['qty'];	
												}else{
													$chn = $porows['qty'] - $soldquantity;
												}	
												if($chn > 0){
													
													$challanno = $porows['challan'];
													echo "challan=>".$challanno;
												}
											}
											}
										}
									}else{
										echo mysqli_error($conn);
									}

									// fetch po no ends
									$cart_key = $rows['uc_key'];
									$price = getSinglevalue('product', 'sku', $rows['product'], 10);
									$stmt = $conn->prepare("INSERT INTO order_book (order_key, user, product, color, size, qty, price, pono, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
									$stmt->bind_param("sssssssss", $order_key, $id, $rows['product'], $rows['color'], $rows['size'], $rows['qty'], $price, $challanno, $created_at);
									if($stmt->execute()){
										if($payment == 'COD'){
											$subquery = mysqli_query($conn, "DELETE FROM user_cart WHERE uc_key='$cart_key'");
											if($subquery){
												$success = 1;
											}
										}else{
											$success = 1;
										}
									}else{
										// echo $conn->error;
										throw new exception($conn->error);
										$success = 0;
									}

								}

								}
								if($success == 1){
									mysqli_commit($conn);
									if(empty($usertoken)){
										$rcemail = getSinglevalue('clients', 'client_key', $id, 3);
										$rcmobile = getSinglevalue('clients', 'client_key', $id, 6);
									}else{
										$rcemail = $useremail;
										$rcmobile = $usermobile;
									}
									// paynow handling
									if($payment == 'paynow'){
										// unset($_SESSION['buythis']);
										if($totalcartamount > 0){
											header('location: helpers/payment.php?orderid='.$order_key);
										}else{
											header('location: order-success.php?orderid='.$order_key);
										}
									}
									// paynow handling

									if($payment == 'COD'){
										$ship = ShipOrder($order_key);
										$shipstatus = "";
            							foreach ($ship as $key => $value){
                							$shipstatus = $shipstatus.", ".$key."=>".$value;
            							}
            							$q = mysqli_query($conn, "INSERT INTO api_response(response, api, orderid) VALUES ('$shipstatus', 'shiprocket', '$order_key')");
										// print_r($ship);
										if(!empty($ship)){
											$shiporderid = $ship['order_id'];
											$shipmentid = $ship['shipment_id'];
											$shipstatus = 'success';
											if(!empty($shiporderid)){
												$sql = mysqli_query($conn, "INSERT INTO order_shipping (order_id, shiprocket_order_id, shipment_id, ship_status) VALUES ('$order_key', '$shiporderid', '$shipmentid', '$shipstatus')");
												if($sql){
													mysqli_commit($conn);
												}
											}
										}
										
										$smscontent = "Thanks for shopping with Bquest India! Your order is confirmed and will be shipped shortly.";
										$msg = mobilesms($rcmobile, $smscontent);
										$content = 'Hi,<br>
										We are pleased to confirm your order no '.$order_key.'.
										Thank you for shopping with Bquest India.<br>
										Check your order <a href="https://bquestindia.com/my-account">here</a>';
										$mail = sendEmail('orders@bquestindia.com', $rcemail, 'Order Successfully', $content);
								// 		if($mail!= 0){
										if(!empty($guest)){
											$data = "";
            								setcookie('shopping_cart', $data, time() - 3600);
										}
										header('location: order-success?orderid='.$order_key);
									}
								}else{

								}

							}
						}
				}else{
					// echo $conn->error;
					echo mysqli_error($conn);
					mysqli_rollback($conn);
					throw new exception($conn->error);
				}
				$stmt->close();
			}catch(Exception $e){

					echo '<script>$.notify("Order Not Successfully", "error");</script>';
			}

			// save order here ends
			

		}
	}else{
		echo '<script>$.notify("Please select Delivery & Payment Method", "error");</script>';
	}
}
?>