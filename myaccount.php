<?php 
include_once"helpers/index.php"; 
client_session($id, $usertoken);

?>
<style>
#ac-one {
    background: #fff;
    font-weight: bold;
}
</style>
<section >
	<div class="container-fluid px-0">
		<div class="accountMainDiv">
			<div id="breadcrumb">
			    <ul class="mb-0">
					<li class="d-inline-block"><a href="index">Home <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block"><a href="#">My account <span class="mx-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li class="d-inline-block">Change my details</li>
			    </ul>
			</div>
			<?php include_once"helpers/accountmenu.php"; ?>
			<div class="accountContent py-5">
					<div class="heading w-100 text-center mt-4">
						<h2>My Details</h2>
					</div>
					<div class="accountCont mx-3 p-4">
						<div class="loginDiv accountDiv">
						
							<form class="formSection" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
									<div class="bussinessToggle mb-4">
										<label class="switch">
										  <input type="checkbox" id="myCheck" onclick="bussinessPurpose()">
										  <span class="slider round"></span>
										</label>
										<p class="mb-0">For bussiness purpose</p>
									</div>
								<div class="bussinessPurpose" id="bussinessInputDiv" style="display:none">
									<div class="fieldSection">
										<label>Company Name *</label>
										<input type="text" name="">
									</div>
									<div class="fieldSection">
										<label>GST Number *</label>
										<input type="text" name="">
									</div>
									<div class="fieldSection">
										<label>Company Address *</label>
										<input type="text" name="">
									</div>
								</div>
								<div class="fieldSection">
									<label>Title</label>
									<select name="title">
									<option value="Mr" <?php echo ($userdata[2] == 'Mr') ? 'selected="selected"' : ''; ?>>Mr.</option>
									<option value="Ms" <?php echo ($userdata[2] == 'Ms') ? 'selected="selected"' : ''; ?>>Ms.</option>
									<option value="Mrs" <?php echo ($userdata[2] == 'Mrs') ? 'selected="selected"' : ''; ?>>Mrs.</option>
									</select>
								</div>
								<div class="fieldSection">
									<label>First Name *</label>
									<input type="text" name="name" value="<?php echo $userdata[3] ?>" required>
								</div>
								<div class="fieldSection">
									<label>Last Name *</label>
									<input type="text" name="last_name" value="<?php echo $userdata[4] ?>">
								</div>
								<div class="fieldSection">
									<label>Email Address *</label>
									<input type="text" name="email" value="<?php echo $userdata[5] ?>" required>
								</div>
								<div class="fieldSection">
									<label>Mobile number *</label>
									<input type="number" id="mobile" name="mobile" value="<?php echo $userdata[6] ?>" required onkeyup="check(); return false;">
									<span id="message"></span>
								</div>
							<p class="mx-auto accountDetail mt-5"><input type="submit" name="save_changes" class="button" value="Save Detail"></p>

							</form>
						</div>
					</div>

					<div class="heading w-100 mt-4 pl-5">
						<h2>Change Password</h2>
					</div>
					<div class="accountCont mx-3 p-4">
						<div class="loginDiv accountDiv">
						
							<form class="formSection" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<div class="fieldSection">
									<label>Current Password *</label>
									<input type="Password" name="current">
								</div>
								<div class="fieldSection">
									<label>Password</label>
									<input type="Password" id="password" onkeyup="password_check()" name="new">
								</div>
								<div class="fieldSection">
									<label>Confirm Password *</label>
									<input type="Password" id="confirm-password" onkeyup="checkPassword()" name="confirm">
								</div>
								<p class="text-danger error-password" style="display: none">Password Invalid Format.</p>
								<p class="text-danger error-match-password" style="display: none">Password or Change Password Not Matched.</p>
								
							<p class="mx-auto accountDetail mt-5">
								<input type="submit" class="button" name="change_password" value="Change Password">
							</p>

							</form>
						</div>
					</div>
			</div>
		</div>
	</div>
</section>

<?php include_once"helpers/footer.php";

// change Password
if(isset($_POST['change_password'])){
	extract($_POST);
	if(!empty($current) && !empty($new) && !empty($confirm)){
		$crt = md5($current);
		$newpassword = md5($new);
		$userpassword = $userdata[7];
	if($crt == $userpassword){
		if($new == $confirm){
			$query = mysqli_query($conn, "UPDATE clients SET password='$newpassword' WHERE client_key='$id'");
			if($query){
				echo '<script>$.notify("Password Changed Successfully", "success");</script>';		
			}else{
				echo '<script>$.notify("Password Not Changed Successfully", "error");</script>';
			}
		}else{
			echo '<script>$.notify("New Password and Old Password Not Matched", "error");</script>';
		}
	}else{
		echo '<script>$.notify("You entered wrong old password", "error");</script>';
	}

	}else{
		echo '<script>$.notify("Please fill all fields", "error");</script>';
	}
}

// save user detail
if(isset($_POST['save_changes'])){
	extract($_POST);
	if(!empty($name) && !empty($email)){
		$sql = mysqli_query($conn, "SELECT * FROM `clients` WHERE  email='$email' OR mobile='$mobile'");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$elseuser = [];
				while($rows = mysqli_fetch_assoc($sql)){
					if($rows['client_key']!= $id){
						$elseuser[] = $rows['client_key'];
					}
				}
				if(empty(count($elseuser))){
					$query = mysqli_query($conn, "UPDATE clients SET title='$title', name='$name', last_name='$last_name', email='$email', mobile='$mobile' WHERE client_key='$id'");
					if($query){
						echo '<script>$.notify("Changes Update Successfully", "success");</script>';
						header('refresh: 1');
					}else{
						echo '<script>$.notify("Changes Not Update Successfully", "error");</script>';
					}
				}else{
					echo '<script>$.notify("Mobile No and Email ID Already Exists", "error");</script>';
				}
			}else{
				$query = mysqli_query($conn, "UPDATE clients SET title='$title', name='$name', last_name='$last_name', email='$email', mobile='$mobile' WHERE client_key='$id'");
				if($query){
					echo '<script>$.notify("Changes Update Successfully", "success");</script>';
				}else{
					echo '<script>$.notify("Changes Not Update Successfully", "error");</script>';
				}
			}
		}
	}
}

?>