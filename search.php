<?php include_once "helpers/index.php";
   if(!empty($searchval)){
       $searchval = "'%".$searchval."%'";
       $sql = mysqli_query($conn, "SELECT t1.p_id, t1.category, t1.brand, t1.subcategory, t1.p_key, t1.product_name, t1.price, t1.sku, t1.groupid, t2.brand_name FROM product as t1 join brands as t2 on t1.brand=t2.brand_key join category as t3 on t1.category=t3.cat_key join subcategory as t4 on t1.subcategory=t4.subcat_key WHERE t1.publish=1 AND t1.product_name LIKE $searchval OR t2.brand_name LIKE $searchval OR t3.category LIKE $searchval OR t4.subcategory LIKE $searchval ORDER BY t1.p_id DESC");
       if($sql){
           $productarr = [];
           while($row = mysqli_fetch_array($sql)){
               $productarr[] = $row;
           }
       }else{
           echo mysqli_error($conn);
       }
   }else{
       header('location: index.php');
   }
   ?>
<input type="hidden" id="filter" value="0">
<section class="category pb-5 pt-0 pt-lg-5">
   <div class="container">
      <div class="row">
         <?php if(!empty($productarr)){ ?>
         <div class="col-lg-3 d-none d-lg-block">
            <div class="filterMainDiv">
               <p><span class="pd-count"><?php echo count($productarr); ?></span> Results</p>
               <div class="accordion">
                  <!-- brand starts here -->
                  <?php 
                     $brandarr = [];
                     for ($i=0; $i < count($productarr); $i++){
                         $brandarr[] = $productarr[$i]['brand'];
                     }
                         $query = mysqli_query($conn, "SELECT DISTINCT(t2.brand_name) as brand FROM brands as t2 join product as t1 on t1.sku=t2.p_id join category as t3 on t1.category=t3.cat_key join subcategory as t4 on t1.subcategory=t4.subcat_key WHERE t1.publish=1 AND t1.product_name LIKE $searchval OR t2.brand_name LIKE $searchval OR t3.category LIKE $searchval OR t4.subcategory LIKE $searchval");
                         if($query){
                             if(mysqli_num_rows($query) > 0){ ?>
                  <div class="accordion-toggle">
                     <h4 class="mb-0">Brands</h4>
                     <p class="mb-0">All</p>
                  </div>
                  <div class="accordion-content">
                     <div class="refinedBrand">
                        <form>
                           <div class="scrolly">
                              <?php while($rows = mysqli_fetch_assoc($query)){ ?>
                              <div class="checkboxGroup my-lg-2">
                                 <input type="checkbox" aria-label="Checkbox for following text input" name="brand[]" value="<?php echo $rows['brand'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
                                 <p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['brand_name']; ?>
                                    <span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                 </p>
                              </div>
                              <?php } ?>
                           </div>
                        </form>
                     </div>
                  </div>
                  <?php }
                     }else{
                     echo mysqli_error($conn);
                     }
                     
                     
                     ?>
                  <!-- Brands Ends here -->
               </div>
            </div>
         </div>
         <div class="col-lg-9 textTab">
            <div class="row justify-content-end">
               <div class="col-lg-12 col-6 mt-0 mt-md-2 mt-lg-4 mb-2 d-block d-lg-none">
                  <div class="mobileFilterDiv">
                     <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                        <p><img src="img/filter.svg" alt=""> filter <span>107</span> result</p>
                     </a>
                  </div>
               </div>
               <div class="col-lg-12 col-6 text-right mt-0 mt-md-2 mt-lg-4 mb-2">
                  <div class="categoryDropdown">
                     <form>
                        <select class="searchInput" id="sorting" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
                           <option selected value="0">Price Low to High</option>
                           <option value="1">Price High to Low</option>
                        </select>
                     </form>
                  </div>
               </div>
               <div class="col-12 my-4" style="display: none">
                  <div class="closeTab">
                     <p>Clothing</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing1</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing2</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing3</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing4</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing5</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing6</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
                  <div class="closeTab">
                     <p>Clothing7</p>
                     <button type="button" class="closeBtn">
                     <img src="img/close.svg" alt="">
                     </button>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row" id="grid">
                     <?php 
                        if(!empty($productarr)){ 
                            for ($i=0; $i < count($productarr) - 1; $i++){ 
                        $sku = $productarr[$i]['sku'];
                        $imgsql = mysqli_query($conn, "SELECT image FROM product_images WHERE product='$sku' ORDER BY pi_id ASC LIMIT 2");
                        if($imgsql){
                        if(mysqli_num_rows($imgsql) > 0){
                        $imgarr = [];
                        while($imgrow = mysqli_fetch_assoc($imgsql)){
                        $imgarr[] = $imgrow['image'];
                        }
                        }
                        }
                                ?>
                     <div class="col-xl-4 col-6">
                        <div class="categoryOuter">
                           <div class="categoryOuterImg">
                              <a href="single-product?p=<?php echo $products['sku'] ?>">
                              <img src="<?php echo PRODUCT_IMAGE_URL.checkImage($imgarr[0]) ?>" class="imgWithOutHover" class="img-fluid" alt="">
                              <?php if(empty($imgarr[1])){ ?>
                              <img src="<?php echo PRODUCT_IMAGE_URL.checkImage($imgarr[0]) ?>" class="imgWithHover" class="img-fluid" alt="">
                              <?php }else{ ?>
                              <img src="<?php echo PRODUCT_IMAGE_URL.checkImage($imgarr[1]) ?>" class="imgWithHover" class="img-fluid" alt="">
                              <?php } ?>
                              </a>
                           </div>
                           <div class="categoryButton">
                              <div class="categoryContent">
                                 <h6><?php echo $productarr[$i]['brand_name'] ?></h6>
                                 <p><?php echo $productarr[$i]['product_name'] ?></p>
                                 <p><i class="fa fa-inr"></i><?php echo str_replace('INR', '',money_format('%i', $productarr[$i]['price'])) ?></p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php   }
                        }else{
                            echo 'No Result Available For "'.$searchval;
                        }
                        ?>
                  </div>
               </div>
            </div>
            <div class="loaderCenter text-center"  id="loaderOuterProduct" style="display: none">
               <div class="loaderOuter2 text-center">
                  <div class="categoryLoader"></div>
                  <p><strong>Loading.....</strong></p>
               </div>
            </div>
         </div>
         <?php }else{ ?>
         <div class="col-md-12 text-center noProduct">
            <p><strong>No products available in this category.</strong></p>
         </div>
         <?php } ?>
      </div>
   </div>
</section>
<?php include_once "helpers/footer.php"; ?>