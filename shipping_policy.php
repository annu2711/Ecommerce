<?php include_once"helpers/index.php" ?>

<section class="middle_part py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="heading w-100 text-center">
                    <h2>Shipping policy</h2>
                </div>
                <div class='main-cont'>
                    <p><span style="font-size: small;"><strong>Why do I see different prices for the same product?</strong></span></p>
                    <p><span style="font-size: small;">We are a market place and give multiple brand/sellers a platform to list their products. This is why you will see different prices for the same product. To see the best price available, click the Buy Now button.</span></p>
                    <p><span style="font-size: small;"><strong>What does the term pre-order mean?</strong></span></p>
                    <p><span style="font-size: small;">At times, we're able to give you the chance to buy a much-wanted product before it is available in the market. This is when we list the product for pre-order&mdash;so you can pre-booked and pay for it in advance. The product will be shipped by the brand/seller/brand soon after the official launch.</span></p>
                    <p><span style="font-size: small;"><strong>Can I cancel a pre-ordered item?</strong></span></p>
                    <p><span style="font-size: small;">You can cancel your order until the seller process it. To cancel your order, visit My Orders in the&nbsp;My Account section. However, please check for specific terms and conditions at the time of pre-ordering.</span></p>
                    <p><span style="font-size: small;"><strong>How do I know if a product will fit me?</strong></span></p>
                    <p><span style="font-size: small;">Please refer to the size guide provided; you can view size specifications and select the right product for you.</span></p>
                    <p><span style="font-size: small;"><strong>Can you notify me once a product is back in stock?</strong></span></p>
                    <p><span style="font-size: small;">As the the inventory is refreshed at intervals, we would suggest that you add the product you want to purchase to your wish list and check in on it from time to time.</span></p>
                    <p><span style="font-size: small;"><strong>My shipping address and billing address may be different. Will that be a problem?</strong></span></p>
                    <p><span style="font-size: small;">Not at all. You can choose to have separate shipping and billing addresses. Our team will deliver the order to your shipping address.</span></p>
                    <p><span style="font-size: small;"><strong>What are the modes of delivery available?</strong></span></p>
                    <p><span style="font-size: small;">We offer 3 delivery modes:<br /> Standard Delivery: Delivered in 3-6 business days.<br /> Express Delivery: Delivered in 1-2 business days.<br /> QUiQ PiQ: Order online &amp; collect from store in 1-2 business days.<br /> <br /> The delivery options available to you will depend on your PIN code, as well as the product you choose to buy and the brand/brand/seller's policies.</span></p>
                    <p><span style="font-size: small;"><strong>How do I find out if lSmart delivers to my location?</strong></span></p>
                    <p><span style="font-size: small;">When you enter your PIN code while placing your order, we will instantly be able to tell you if we can deliver to your location.</span></p>
                    <p><span style="font-size: small;"><strong>How do I find out about delivery charges?</strong></span></p>
                    <p><span style="font-size: small;">Delivery charges, where applicable, will be added to your total at Checkout.</span></p>
                    <p><span style="font-size: small;"><strong>Will all brand/sellers on lSmart ship to the area I live in?</strong></span></p>
                    <p><span style="font-size: small;">Whether a brand/seller can ship to your area or not depends on the brand/seller&rsquo;s capability to service your area through self or partner logistics networks, and legal restrictions, if any. brand/sellers may choose to not service certain areas based on their business policies or discretion.</span></p>
                    <p><span style="font-size: small;"><strong>Why does the delivery time vary from brand/seller to brand/seller?</strong></span></p>
                    <p><span style="font-size: small;">The delivery time depends upon the availability of the product purchased, and the handling time for the brand/seller and logistics partner.</span></p>
                    <p><span style="font-size: small;"><strong>Do you deliver internationally?</strong></span></p>
                    <p><span style="font-size: small;">We don&rsquo;t deliver internationally at the moment. Right now, you can place your orders from anywhere in the world as long as the shipping address is in India.</span></p>
                    <p><span style="font-size: small;"><strong>Can I request different items from my bag to be delivered separately?</strong></span></p>
                    <p><span style="font-size: small;">Unfortunately, at the moment we can&rsquo;t ship different products in the same bag to separate addresses. We hope we can make this happen for you soon.</span></p>
                    <p><span style="font-size: small;"><strong>My schedule is fully booked. Can I choose a specific date and time for delivery?</strong></span></p>
                    <p><span style="font-size: small;">We always show you the estimated delivery date for all the items in your shopping bag. We advise you to place your order keeping in mind when they will be delivered, and if that works for you. Unfortunately, you can&rsquo;t choose a specific date and time for delivery on lSmart right now. But soon, maybe.</span></p>
                    <p><span style="font-size: small;"><strong>Can I choose a specific date and time to collect a lSmart order from the store?</strong></span></p>
                    <p><span style="font-size: small;">We offer a 7-day window within which to collect the product from the store; this starts from the date the product has been made available in-store.</span></p>
                    <p><span style="font-size: small;"><strong>Why is the Cash on Delivery (COD) payment option not available at my location?</strong></span></p>
                    <p><span style="font-size: small;">The Cash on Delivery (COD) payment option is only available if the logistics partner delivering to your location offers the option. You can check if this payment option is available to you at Checkout. Please note that logistics companies also place limits on the amount you can pay through cash on delivery based on the destination, and your order could have exceeded this limit.</span></p>
                    <p><span style="font-size: small;"><strong>How do I place an order?</strong></span></p>
                    <p><span style="font-size: small;">To place an order, please follow these steps:<br /> 1. Select the product you want to buy and check availability and delivery options at your preferred PIN code. If you are buying clothes or shoes please select the desired size.<br /> 2. Add products to your bag and then click on the Buy Now button.<br /> 3. If this is the first time you are shopping with us, add a delivery address; if you've shopped with us before, select a saved address.<br /> 4. Use a preferred payment mode and confirm the order.</span></p>
                    <p><br /> </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once"helpers/footer.php" ?>