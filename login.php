<?php 
include_once "helpers/index.php"; 
if(!empty($id) && !empty($usertoken)){
	header('location: index');
}

$user = "";
if(isset($_GET['user'])){
	$user = $_GET['user'];
	$otpbtn = 0;
	if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
		$otpbtn = 0;
	  }else{
		  $otpbtn = 1;
	  }
}
?>

<section class="loginSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-md-12 mt-3 md-sm-4 mg-mb-5">
				<div class="row no-gutters">
					<div class="col-md-6 px-0 px-sm-4 px-md-5">
						<?php if(empty($user)){ ?>
						<div class="loginDiv" id="mobileDiv">
							<p>Mobile</p>
							<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<input type="number" placeholder="Mobile" onkeyup="mobileValidations('shipmobile', 'shipmobileerror')" id="shipmobile" name="mobile" required>
								<p class="text-danger" style="border: none; display:none" id="shipmobileerror">Please Enter 10 Digit Valid Mobile Number</p>
								<a href="javascript:;" onclick="openDiv('emailDiv')">Use Email</a>
							
							<p class="mx-auto mt-2">
								<input type="submit" name="mobilelogin" class="button signup-btn" value="Submit">
							</p> 
							</form>
						</div>

						<div class="loginDiv" id="emailDiv" style="display: none">
							<p>Email</p>
							<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<input type="text" placeholder="Email address" name="email" required>
								<a href="javascript:;" onclick="openDiv('mobileDiv')">use Mobile</a>
							
							<p class="mx-auto mt-2">
								<input type="submit" name="emaillogin" class="button signup-btn" value="Submit">
							</p> 
							</form>
						</div>

						<?php }else{ ?>

						<div class="loginDiv" id="passwordDiv">
							<p>Password</p>
							<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<input type="password" placeholder="Password" name="password">
								<button <?php echo (empty($otpbtn)) ? 'type="button"' : 'type="submit" name="sendotp" value="sendotp"' ; ?>>Use OTP</button>
							
							<p class="mx-auto mt-2">
								<input type="submit" name="submitpassword" class="button signup-btn" value="Submit">
							</p> 
							</form>

						</div>
						<?php } ?>

					</div>
			
					<div class="col-md-6 borderLogin mt-4 mt-sm-4 mt-md-0 pt-5 pt-md-0 px-0 px-sm-4 px-md-5">
						<div class="loginDiv signupDiv">
							<p>New to Bquestindia.com</p>
							<p class="mb-5">Creating an account is quick and simple and allows you to track, change or return your order.</p>
							<form class="formSect" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<select class="title" name="title" required>
									<option value="Mr">Mr.</option>
									<option value="Ms">Ms.</option>
									<option value="Mrs">Mrs.</option>
								</select>
								<input type="text" placeholder="First name*" name="name" required>
								<input type="text" placeholder="last name*" name="last_name" required>
								<input type="text" pattern="[1-9]{1}[0-9]{9}" name="mobile" placeholder="9999999999*" onkeyup="check();" class="mb-0" required>
								<p class="text-danger">10 Digit Valid Mobile No.</p>
								<!-- <span id="message"></span> -->
								<input type="email" placeholder="Email address*" name="email">
									
								<p class="rememberMe">Want to hear from us?*</p>

								<ul class="list-group">
									<li class="list-group-item">
										<input type="radio" class="form-check-input w-auto" name="newsletter" value="1" checked>Yes please
									</li>
									<li class="list-group-item">
										<input type="radio" class="form-check-input w-auto" name="newsletter" alue="0">No thanks
									</li>
								</ul>

								<p class="onClickToggle rememberMe mt-3 text-right"><strong>Show me more <i class="fa fa-angle-down" aria-hidden="true"></i></strong></p>
								<p class="toggleDiv">Our emails keep you in the loop with all our latest fashion drops, collections, events and offers here at Selfridges. Note: we may tailor our emails and online advertising according to your location and purchases. You can unsubscribe at any time by clicking the link in any of our emails.</p>
								<input type="password" id="password" placeholder="Password*" name="password" required><span class=" field-icon"></span>
								<input type="password" placeholder="Confirm Password*" name="confirm_password" id="confirm-password" required><span class="field-icon"></span>
								<p class="text-danger error-password" style="display: none">Password Invalid Format.</p>
								<p class="text-danger error-match-password" style="display: none">Password or Change Password Not Matched.</p>
								<p class="term-and-Cond my-5">By clicking Create an account, you are accepting our <a href="javascript:void(0)">Terms & Conditions</a> and <a href="privacy_policy">Privacy & Cookie Policy</a>.</p> 
								<input type="submit" id="signup" class="button signup-btn" name="signup" value="Create an Account">
							</form>
							<p class="mx-auto openSignUp"><a class="button" href="javascript:void(0)">Create an Account </a></p>
						</div>
					</div>




				</div>
			</div>
		</div>
	</div>
</section>

<?php include_once "helpers/footer.php";

// print_r($_SESSION['usermobile']);


if(isset($_POST['signup'])){
    // echo '<script>alert("Please Fill All Fields")</script>';
     extract($_POST);
     if(!empty($name) && !empty($email) && !empty($password) && !empty($confirm_password)){
		 if($password == $confirm_password){
        $chck_dup = check_duplicate('clients', 'email', $email);
        if(empty($chck_dup)){
            // $query = mysqli_query
            $key = rand_char(5);
            $token = rand_char(32);
            $pass = md5($password);
			$status = 1;
			
            try{
                $stmt = $conn->prepare("INSERT INTO clients (client_key, title, name, last_name, mobile, email, password, token, status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param('ssssssssss', $key, $title, $name, $last_name, $mobile, $email, $pass, $token, $status, $created_at);
                if ($stmt->execute()){
                    $_SESSION['user'] = $key;
                    $_SESSION['token']= $token;
                    header('location: myaccount');
                    }else{
						// echo $conn->error;
                        throw new exception($conn->error);
                    }
                    $stmt->close();
            }catch(Exception $e){
                echo "Error: " . $e->getMessage();
                echo '<script>$.notify("User Not Registered Successfully", "error");</script>';
            }
        }else{
			echo '<script>$.notify("User Already Registered", "error");</script>';
			header('refresh: 0.5');
		}
		}else{
			echo '<script>$.notify("Old Password & Confirm Password No Matched", "error");</script>';
		}
     }else{
        echo '<script>$.notify("Please Fill All Fields", "error");</script>';
     }
 }

 if(isset($_POST['submitpassword'])){
	extract($_POST);
	$username = $user;
	if(!empty($username) && !empty($password)){
		$pass = md5($password);
		$sql = mysqli_query($conn, "SELECT * FROM clients WHERE password='$pass' AND email='$username' or mobile='$username' ORDER BY client_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$data = mysqli_fetch_assoc($sql);
				$userkey = $data['client_key'];
				$token = rand_char(32);
				$query = mysqli_query($conn, "UPDATE clients SET token='$token' WHERE client_key='$userkey'");
				if($query){
					$_SESSION['user'] = $userkey;
					$_SESSION['token'] = $token;
					if(isset($_SESSION['url'])){
						header('location:'.$_SESSION['url']);
					}else{
						header('location: index');
					}
				}
			}else{
				echo '<script>$.notify("Invalid Credentials", "error");</script>';
			}
		}
	}
}


if(isset($_POST['mobilelogin'])){
	extract($_POST);
	if(!empty($mobile)){
		$checkmobile = check_duplicate('clients', 'mobile', $mobile);
		if(empty($checkmobile)){
			echo '<script>$.notify("Mobile Number not registered", "error");</script>';	
		}else{
			header('location: login.php?user='.$mobile);
		}
	}else{
		echo '<script>$.notify("Please Fill Mobile No.", "error");</script>';
	}
}

if(isset($_POST['emaillogin'])){
	extract($_POST);
	if(!empty($email)){
		$checkmobile = check_duplicate('clients', 'email', $email);
		if(empty($checkmobile)){
			echo '<script>$.notify("Mobile Number not registered", "error");</script>';	
		}else{
			header('location: login.php?user='.$email);
		}
	}else{
		echo '<script>$.notify("Please Fill Email ID", "error");</script>';
	}
}



if(isset($_POST['sendotp'])){
	if(!empty($user)){
		$otp = rand(1000, 9999);
		$mobile = 0;
		if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
			$mobile = 0;
		  }else{
			  $mobile = 1;
		  }
		if($mobile){
		try{
			$stmt = $conn->prepare("INSERT INTO otp (mobile_email, otp, created_at) VALUES (? ,? ,?)");
			$stmt->bind_param('sss', $user, $otp, $created_at);
			if($stmt->execute()){
				$content = 'Your Otp to login in bequest india is '.$otp;
				$sms = mobilesms($user, $content);
				if($sms){
					$smsresponse = "";
					foreach($sms as $key => $value){
						$smsresponse = $smsresponse.", ".$key."=".$value;
					}
					if(!empty($smsresponse)){
						$api = "sms";
						try{
							$stmt2 = $conn->prepare("INSERT INTO api_response(api, sentto, response, created_at) VALUE (?, ?, ?, ?)");
							$stmt2->bind_param('ssss', $api, $user, $smsresponse, $created_at);
							if($stmt2->execute()){
								header('location: otp.php?user='.$user);
							}else{
								throw new exception($conn->error);
							}
						}catch(Exception $e){
							echo '<script>$.notify("Server Error2", "error");</script>';
						}
					}
				}
			}else{
				throw new exception($conn->error);
			}
		}catch(Exception $e){
			echo '<script>$.notify("Server Error1", "error");</script>';
		}
	}
	}
}



?>