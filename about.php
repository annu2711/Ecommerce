<?php include_once"helpers/index.php" ?>
<section class="middle_part py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="heading w-100 text-center">
							<h2>About lSmart</h2>
						</div>
                        <div class='main-cont'>
                           <p>lSmart, is known as the "Class Technology Retailer" in India for its customer service and retail metrics has 13 stores operational in Northern India with plans for further expansion in the coming years.</p>


                            <p>Headquartered in New Delhi, with its Corporate office in Ansal Plaza, Khel Gaon Marg, the Firm has a dominant position and amongst the APRs. The Company was founded in 2005 by Mr. Tript Singh as an Apple Authorized Reseller (AAR), and soon became an Apple Premium Reseller (APR).</p>


                            <p>lSmart – launched its flagship store in a premium South Delhi Mall in Ansal Plaza in 2005. The Store is located in the "B – Block" ground floor.</p>


                            <p>Subsequently the second store was opened in "DLF Promenade", Vasant Kunj, post which to establish its presence amidst the student community, lSmart, opened the third store in Bunglow Road, Kamla Nagar. lSmart Store in Ambience Mall Gurgaon is centrally located on the first floor and opened its operations in 2012. As the firm decided to have its presence in the North of India, Punjab, MBD Neopolis Mall was the first store in Ludhiana, by lSmart which opened in 2013. In 2014 the Galleria Store, in DLF Phase –IV, was the 6th addition to lSmart's list of Stores, with Akhil Sachdeva performing at the Store Launch.</p>
                             <p>With a positive response and seeing the growing need for Apple, lSmart opened its 7th store in Pavilion Mall, Ludhiana in January, 2015. Ambience Vasant Kunj was inaugurated in August, 2015 with great pomp and show, with once again, Akhil Sachdeva, from the very popular Nasha band performing at the launch at the Ambience Vasant Kunj Atrium. Being an APR – Apple Premium Reseller, the lSmart Stores showcase an entire range of Apple Products – all models of iMac, MacBook, iPad, iPhone or an iPod.</p>


                            <p>In the same year in November 2007, an AAR was launched in the Buzzing High Streets of Nehru Place. The AAR, eWorld is situated right at the enterance of the Nehru Place market, right behind Satyam Cineplexes, now known as INOX Cineplexes.</p>

                            <p>In 2016 and 2017, a row of stores were inaugurated.</p>


                            <p>In November'2016 the South Extension store, a MONO AAR store was inagurated, Logix Mall in Noida, in February’2017 , Aerocity, World Mark in March’2017, Khan Market in April’2017 & in May’2017 the Chandigarh Store was launched.</p>


                            <p>The team at lSmart is technically competent, customer focused, motivated, competent and passionate and believe in the vision and mission of the company. Hence, the Stores enjoy a relatively higher customer reference.</p>


                            <p>Acknowledged as one of the earliest and most successful APR partners in India, which has been achieved through excellent service and consistent support. At lSmart we believe in contributing and adding value to the youth of today and making technology accessible to all. Various Workshops and Sponsorship program in Schools and colleges have been initiated by the firm, like at Guru Harkrishan Public School, Venkateshwara College, Gargi College & Maharaja Agrasen Institute of Technology, Rohini to name a few, respectively. What sets lSmart apart are also the marketing activities and initiatives. Previously the firm had tied up with Barista during the Valentine's season for various promotional activities.</p>


                            <p>In addition to its portfolio, of being an APR, lSmart offers a strong mix of accessories, of lightening cables, tri-pod stand, cases, battery backups, sleeves for iPads, MacBook Air, MacBook Pro, Bagpack, Stylus & Cleaning Kits from brands like Belkin, Mophie, Case Logic, Thule, iWires, Joby, Keep it Clean.</p>


                            <p>lSmart opened first store for Apple in 2005 and also 100th store in Ambience, Vasant Kunj, Delhi last year August, 2015. The Firm, has won accolades, which include the Best Manager Award, Ist Rank in the iPhone Hall of Fame – Store Awards, Vasant Kunj & the Ist Rank in the iPhone Hall of Fame – Partner Awards, Best Sales across all APRs in iPhone in 2014, Best APR award in September, 2015 across India for Revenue & Quality. Offering a 360 degree of Technology experience, lSmart never fails to offer you, your next moment of Delight !!</p>
                        </div>
                    </div>
                </div>
            </div>
      
</section>
<?php include_once"helpers/footer.php" ?>