<?php 
include_once "helpers/index.php";
include_once "helpers/shiprocket.php"; 
if(empty($cart_data)){
	header('location: index.php');
}
if(empty($id) && empty($usertoken)){
if(isset($_COOKIE['guest'])){
	$email = stripcslashes($_COOKIE['guest']);
	$id = getSingleValue('clients', 'email', $email, 0);
	$sql = mysqli_query($conn, "SELECT * FROM address_book WHERE user='$id' ORDER BY ab_id DESC LIMIT 1");
	if($sql){
		if(mysqli_num_rows($sql) > 0){
			$addressarr = mysqli_fetch_array($sql); 
			// print_r($addressarr);
		}
	}else{
		echo mysqli_error($conn);
	}
}
}else{
	$id = $userdata[1];
}

$coupondiscount = 0;
$couponarray = "";
if(isset($_SESSION['coupon'])){
	$coupondiscount = $_SESSION['coupon'][0];
	$couponarray = $_SESSION['coupon'];
}

// print_r($cart_data);
?>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="m-0">
<section class="middle-div auth-forms">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                        <div class="expressCheckoutInn mt-4 p-4">
							<div class="row borderBottomGray mb-2 pb-2 no-gutters" style="border-bottom: 2px solid #444943;">
								<div class="col-md-2">
									<div class="contactLight">
										<p class="mb-0">Contact</p>
									</div>
								</div>
								<div class="col-md-8">
									<div class="contactDark">
									<p class="mb-0">
									<?php 
										if(empty($usertoken)){
											echo $email;
										}else{
											echo $userdata[5];
										}
										?>
									</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="contactAnchor float-right">
										<a href="shopping-information.php" class="default-link-color">Change</a>
									</div>
								</div>
							</div>
							<div class="row justify-content-space-between mb-2 pt-2 no-gutters">
								<div class="col-md-2">
									<div class="contactLight">
										<p class="mb-0">Ship to</p>
									</div>
								</div>
								<div class="col-md-8">
									<div class="contactDark">
									<p class="mb-0">
										<strong><?php echo $addressarr[4]." ".$addressarr[5] ?></strong><br>
										<span><b>Mo: </b> <?php echo $addressarr[6] ?></span><br>
										<span><?php echo $addressarr[7]." ".$addressarr[8].", ".$addressarr[9]."-".$addressarr[10] ?></span>
										
										</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="contactAnchor float-right">
										<a href="shopping-information.php" class="default-link-color">Change</a>
									</div>
								</div>
							</div>
                        </div>
                        

                        <div class="contactInformationForm mt-5">
								<div class="row">
									<div class="col-md-12">
										<div class="formHeading">
											<h2 class="mb-0">Billing method</h2>
											<p>Select the address that matches your card or payment method.</p>
										</div>
									</div>
								</div>
                        </div>
                        

                        <div class="expressCheckoutInn">
							<div class="billingAddress borderBottomGray p-3">
								<input type="radio" name="billingaddress" checked="" value="0" id="ssa" required>
								<label class="mb-0" for="ssa">Same as shipping address</label>
							</div>
							<div class="billingAddress borderBottomGray p-3">
								<input type="radio" name="billingaddress" value="1" id="dba" required>
								<label class="mb-0" for="dba">Use a different billing address</label>
							</div>
							<div class="diffBillAdd form-border grayBack p-3 pb-4" style="display: none">
								<div class="row">
									<div class="col-md-6 mt-0">
										<div class="formGroupInput">
											<label>First Name <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="fname" placeholder="First name">
										</div>
									</div>
									<div class="col-md-6 mt-0">
										<div class="formGroupInput">
											<label>Last Name <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="lname" placeholder="Last name">
										</div>
									</div>
									<div class="col-md-12 mt-3">
										<div class="formGroupInput">
											<label>Address <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="address" placeholder="Address">
										</div>
									</div>
									<div class="col-md-6 mt-3">
										<div class="formGroupInput">
											<label>Country/Region</label>
											<select class="form-control" name="country">
												<option value="india">India</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 mt-3">
										<div class="formGroupInput">
											<label>City <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="city" value="">
										</div>
									</div>
									<div class="col-md-6 mt-3">
										<div class="formGroupInput">
											<label>States <sup class="text-danger">*</sup></label>
											<?php // echo ($statevalue == $statearr[$i]) ? 'selected="selected"' : '' ; ?>
											<select class="form-control" name="state">
											<?php $statearr = [ "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttarakhand", "Uttar Pradesh", "West Bengal", "Andaman and Nicobar Islands", "Chandigarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Lakshadweep", "Puducherry"]; 
										for($i = 0; $i <= count($statearr) - 1; $i++){ ?>
											<option value="<?php echo $statearr[$i] ?>"><?php echo $statearr[$i] ?></option>
										<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-md-6 mt-3">
										<div class="formGroupInput">
											<label>Zip Code <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="pincode" placeholder="zip code">
										</div>
									</div>
									<div class="col-md-6 mt-3">
										<div class="formGroupInput">
											<label>Phone <sup class="text-danger">*</sup></label>
											<input type="text" class="form-control" name="mobile" placeholder="Phone">
										</div>
									</div>
								</div>
							</div>
                        </div>
                        


                        <div class="contactInformationForm mt-5 mb-3">
								<div class="row">
									<div class="col-md-12">
										<div class="formHeading">
											<h2 class="mb-0">Payment method</h2>
										</div>
									</div>
								</div>
							</div>

						<div class="shipping-content">
							<div class="row no-gutters">

							<?php if(!isset($_SESSION['coupon'])){ ?>
	                                    <div class="col-md-6">
	                                    	<div class="deliveryMethod mr-2">
		                                    	<label class="mb-0"><input type="radio" class="mr-3" value="cod" name="payment" /><img src="img/payment.svg" class="mr-3">Cash on Delivery</label>
		                                    </div>
	                                    </div>
							<?php } ?>

	                                    <div class="col-md-6">
	                                    	<div class="deliveryMethod ml-2">

		                                    	<label class="mb-0"><input type="radio" class="mr-3" value="paynow" name="payment" checked="checked"/><img src="img/money.svg" class="mr-3">Pay now</label>
		                                    </div>
	                                    </div>
									</div>
                        </div>
                        

                        <div class="continueShipping my-4">
						    <div class="row align-items-center">
							    <div class="col-md-6">
								    <div class="returnToCart">
									    <a href="shopping-cart.php" class="default-link-color"><i class="fa fa-chevron-left mr-2"></i> Return to cart</a>
								    </div>
							    </div>
							    <div class="col-md-6">
								    <div class="alreadyAccount text-right">
									    <button type="submit" class="default-btn" name="confirm_order" onclick="disableIt('pcorder')" id="pcorder" value="Confirm Order">Confirm Order</button>
								    </div>
							    </div>
						    </div>
					    </div>

            </div>
            <div class="col-md-5  mb-4">
                <div class="order-summary">
                    <div class="row checkoutOut px-3">
                        <div class="orderSummary">
                            <h2>Order summary</h2>
                        </div>
                        <div class="editSummary">
                            <a href="shopping-cart.php"><span><i class="fa fa-pencil-alt"></i> </span>Edit</a>
                        </div>
                    </div>

                    <div class="order-summary-inn">
                        <div class="row">
                            <div class="col-8">
                                <p>My bag (<?php echo (empty($directbuy)) ? $totalcartquantity : $dtbuy[6] ; ?> items)</p>
                            </div>
                            <div class="col-4">
                                <div class="viewToggle">
                                    <p class="mb-0 viewtag">View <span><i class="fa fa-angle-down"></i></span></p>
	                                <p class="mb-0 hidetag">Hide <span><i class="fa fa-angle-up"></i></span></p>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
						<?php  if(!empty($cart_data)){
                            			for($i=0; $i < count($cart_data); $i++){ ?>
										<div class="wishlistmain slideUpDown border-0">
											<div class="wishlistImg checkoutImg">
												<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $cart_data[$i]['product'], 2)) ?>">
											</div>
											<div class="wishlistCont checkoutCont">
												<P class="mb-0 mt-2"><?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 4) ?> <?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 21) ?></P>
												<p class="mb-0">Quantity: <?php echo $cart_data[$i]['qty'] ?></p>
												<p class="mb-0"><i class="fas fa-rupee-sign"></i> 
												<?php 
												echo $prc = getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10);
												
												?>
												</p>
											</div>
										</div>
							<?php 
										}
									}
							?>
							<div class="col-md-6">
								<div class="totalPrice">
									<p class="mb-0">Subtotal</p>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<div class="totalPrice">
									<p class="mb-0"><i class="fa fa-rupee-sign"></i>
										<?php
											echo $totalcartamount;
										?>
									</p>
								</div>
							</div>
										
                            <div class="col-md-6">
								<div class="totalPrice">
									<p class="mb-0">Total</p>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<div class="totalPrice">
									<p class="mb-0"><i class="fa fa-rupee-sign"></i> 
                                    <?php echo $totalcartamount ?>
                                    </p>
								</div>
						    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
	function disableIt(id){
		$('.loading-page').show();
	}
</script>
<?php 
include_once "helpers/footer.php"; 

function saveBillingAddress($user, $name, $lname, $mobile, $addresss, $city, $state, $pincode){
	$conn = $GLOBALS['conn'];
	$key = rand_char(5);
	try{
		$stmt = $conn->prepare("INSERT INTO `address_book`(`ab_key`, `user`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('ssssssssss', $key, $user, $name, $lname, $mobile, $addresss, $city, $state, $pincode, $created_at);
		if($stmt->execute()){
			return $key;
		}else{
			throw new exception($conn->error);
		}
	}catch(Exception $e){
		echo $e->getMessage();
		return 400;
	}
}

if(isset($_POST['confirm_order'])){
	extract($_POST);
	if($billingaddress != "" && !empty($payment)){
		if($billingaddress == 1){
			echo "Hii";
			$billing_address = saveBillingAddress($id, $fname, $lname, $mobile, $address, $city, $state, $pincode);
			echo $billing_address;
		}else{
			$billing_address = $addressarr[1];
		}
	}
	if(!empty($billing_address) && $billing_address != 400){
		mysqli_autocommit($conn, FALSE);
		$order_key = rand_char(5);
		$payment_status = ($payment == 'cod') ? 'Uncomplete' : 'Uncomplete';
		$order_key = rand_char(5);
		try{
			$stmt = $conn->prepare("INSERT INTO orders (order_key, user, total_items, amount, billing_address, shipping_address, payment, payment_status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("sssssssss", $order_key, $id, $totalcartquantity, $totalcartamount, $billing_address, $addressarr[1], $payment, $payment_status, $created_at);
			if ($stmt->execute()) {
								$success = 0;
								$qtyformsg = $totalcartquantity;
								$amtformsg = $totalcartamount;
								for($i = 0; $i < count($cart_data); $i++){
									$challanno = "";
									$chn = "";
									// fetch po no starts
									$productid = $cart_data[$i]['product'];
									$soldquantity = orderedQty($productid) +  $cart_data[$i]['qty'];
									$query = mysqli_query($conn, "SELECT challan, qty FROM product_stock WHERE product='$productid'");
									if($query){
										if(mysqli_num_rows($query) > 0){
											while($porows = mysqli_fetch_assoc($query)){
												echo $challanno;
												if(empty($challanno)){
												if($soldquantity > $porows['qty']){
													$chn = $soldquantity - $porows['qty'];	
												}else{
													$chn = $porows['qty'] - $soldquantity;
												}	
												if($chn > 0){
													
													$challanno = $porows['challan'];
													echo "challan=>".$challanno;
												}
											}
											}
										}
									}else{
										echo mysqli_error($conn);
									}

									// fetch po no ends
									$cart_key = $cart_data[$i]['cartid'];
									$price = getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10);
									$stmt = $conn->prepare("INSERT INTO order_book (order_key, user, product, qty, price, pono, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
									$stmt->bind_param("sssssss", $order_key, $id, $cart_data[$i]['product'], $cart_data[$i]['qty'], $price, $challanno, $created_at);
									if($stmt->execute()){
										if($payment == 'COD'){
											if(!empty($usertoken)){
												$subquery = mysqli_query($conn, "DELETE FROM user_cart WHERE uc_key='$cart_key'");
												if($subquery){
													$success = 1;
												}
											}
											$success = 1;
										}else{
											$success = 1;
										}
									}else{
										echo mysqli_error($conn);
										throw new exception($conn->error);
										$success = 0;
									}
								}



								echo "Success =>".$success;
								if($success == 1){
									mysqli_commit($conn);
									if(empty($usertoken)){
										$rcemail = getSinglevalue('address_book', 'ab_id', $addressarr[0], 6);
										$rcmobile = getSinglevalue('clients', 'client_id', $id, 6);
									}else{
										$rcemail = $useremail;
										$rcmobile = $usermobile;
									}
									// paynow handling
									if($payment == 'paynow'){
										if($totalcartamount > 0){
											header('location: helpers/payment.php?orderid='.$order_key);
										}else{
											header('location: order-success.php?orderid='.$order_key);
										}
									}
									// paynow handling

									if($payment == 'cod'){
										$ship = ShipOrder($order_key);
										print_r($ship);
										$shipstatus = "";
            							foreach ($ship as $key => $value){
                							$shipstatus = $shipstatus.", ".$key."=>".$value;
            							}
            							$q = mysqli_query($conn, "INSERT INTO api_response(response, api, orderid) VALUES ('$shipstatus', 'shiprocket', '$order_key')");
										// print_r($ship);
										if(!empty($ship)){
											$shiporderid = $ship['order_id'];
											$shipmentid = $ship['shipment_id'];
											$shipstatus = 'success';
											if(!empty($shiporderid)){
												$sql = mysqli_query($conn, "INSERT INTO order_shipping (order_id, shiprocket_order_id, shipment_id, ship_status) VALUES ('$order_key', '$shiporderid', '$shipmentid', '$shipstatus')");
												if($sql){
													mysqli_commit($conn);
												}
											}
										}
										
										$smscontent = "Thanks for shopping with Bquest India! Your order is confirmed and will be shipped shortly.";
										$msg = mobilesms($rcmobile, $smscontent);
										$content = 'Hi,<br>
										We are pleased to confirm your order no '.$order_key.'.
										Thank you for shopping with Bquest India.<br>
										Check your order <a href="https://bquestindia.com/my-account">here</a>';
										$mail = sendEmail('orders@bquestindia.com', $rcemail, 'Order Successfully', $content);
										if(empty($usertoken)){
											$data = "";
            								setcookie('shopping_cart', $data, time() - 3600);
										}
										header('location: order-success?orderid='.$order_key);
									}
								}else{

								}
				}else{
					echo mysqli_error($conn);
					mysqli_rollback($conn);
					throw new exception($conn->error);
				}
				$stmt->close();
			}catch(Exception $e){

					echo '<script>$.notify("Order Not placed", "error");</script>';
			}
	}
}

?>