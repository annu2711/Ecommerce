-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2020 at 11:44 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moonraker_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

CREATE TABLE `address_book` (
  `ab_id` int(255) NOT NULL,
  `ab_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `town` varchar(200) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` int(10) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address_book`
--

INSERT INTO `address_book` (`ab_id`, `ab_key`, `user`, `title`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rkxyt', 'xhqgm', 'Mr', 'Anand', 'Verma', '09718190486', 'A-41, Ram Vihar, Loni ', 'Ghaziabad', 'Uttar Pradesh', 201102, 1, '2020-06-11 01:57:19', '2020-06-10 13:27:20'),
(2, 'ghvej', 'jqrue', 'Mr', 'Manreet', 'Rathore', '09999910363', 'Plot No 10 Rohini Sector 10 ', 'Delhi', 'Delhi', 110085, 1, '2020-06-11 03:11:16', '2020-06-10 14:41:16'),
(3, 'bqphy', 'xhqgm', 'Mr', 'Ajay', 'Verma', '07011761969', 'xyz ', 'Ganganagar', 'Rajasthan', 526398, 0, '2020-08-20 10:19:03', '2020-08-20 10:19:03'),
(4, 'mrsln', 'xhqgm', 'Mr', 'Anand', 'Verma', '09718190486', 'Dummy ', 'Mujjafarpur', 'Bihar', 256235, 0, '2020-08-20 10:20:43', '2020-08-20 10:20:43'),
(5, 'rsmbi', 'xhqgm', 'Mr', 'Ajay', 'Verma', '07011761969', 'xyz ', 'Ganganagar', 'Rajasthan', 526398, 0, '2020-08-20 10:21:49', '2020-08-20 10:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `au_id` int(255) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `token` text DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `deg` int(20) NOT NULL,
  `status` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`au_id`, `username`, `password`, `token`, `name`, `email`, `mobile`, `deg`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'e1b4755403710e0deb7aa5d45e43996d', 'jbsmudxranewzkylqptofcvigh', '', '', '', 0, 1, '2020-09-02 05:56:34', '2020-09-02 05:56:34'),
(2, 'anand', 'Anand@123#', 'woxjhpuyvrsgkqlazdmbtncief', 'Anand Verma', 'anandverma137@gmail.com', '9718190486', 1, 1, '2020-08-12 11:45:42', '2020-08-12 11:45:42'),
(3, 'shikha', 'Shikha@123#', 'rgpnwzjtaecudlihofvxskmbyq', 'Shikha Kothari', 'shikha.k@inovoteq.com', '8826045188', 1, 1, '2020-08-12 11:27:48', '2020-08-12 11:27:48'),
(4, 'sachin', 'Sachin@123#', NULL, 'Sachin Jangid', 'sachin.jangid26@gmail.com', '9013082073', 2, 1, '2020-08-12 11:25:36', '2020-08-12 11:25:36'),
(5, 'anurag', 'e1b4755403710e0deb7aa5d45e43996d', '0', 'Anurag Yadav', 'ak@gmail.com', '8750331669', 3, 1, '2020-08-22 09:16:15', '2020-08-22 09:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `apple`
--

CREATE TABLE `apple` (
  `aid` int(255) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `npi` varchar(50) DEFAULT NULL,
  `marketing_flag` varchar(50) DEFAULT NULL,
  `upc_ean` varchar(50) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `item_group` varchar(50) DEFAULT NULL,
  `subproduct_group` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apple`
--

INSERT INTO `apple` (`aid`, `p_id`, `npi`, `marketing_flag`, `upc_ean`, `barcode`, `item_group`, `subproduct_group`, `manufacturer`, `created_at`, `updated_at`) VALUES
(1, 'MOON00103fghf', 'New', 'dh', 'hkk', 'fsfweefewf', 'dfgdfg', 'lkjlk', 'jkl', '2020-08-24 12:27:08', '2020-08-25 06:29:22'),
(2, 'MOON-LB-002sdfsdf', 'New', 'sdfdf', 'ddfsd', 'dsfds', 'sdfsdf', 'sdfsdf', 'sdffdsf', '2020-08-25 07:28:40', '2020-08-25 07:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `assigned_attributes`
--

CREATE TABLE `assigned_attributes` (
  `aaid` int(255) NOT NULL,
  `brand` varchar(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `subcategory` varchar(10) NOT NULL,
  `attribute` varchar(20) NOT NULL,
  `attribute_type` varchar(20) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assigned_attributes`
--

INSERT INTO `assigned_attributes` (`aaid`, `brand`, `category`, `subcategory`, `attribute`, `attribute_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'wxfiq', 'saqem', 'ahyxc', 'COLOR', 'text', 1, '2020-08-26 10:03:03', '2020-08-26 10:03:03'),
(2, 'wxfiq', 'nbawm', 'brnpl', 'SIZE', 'text', 1, '2020-08-26 10:28:30', '2020-08-26 10:28:30'),
(3, 'jdmnb', 'njchx', 'syodq', 'STORAGE', 'dd', 1, '2020-08-26 10:29:02', '2020-08-26 10:44:50'),
(4, 'jdmnb', 'njchx', 'ckmvp', 'DISPLAY', 'dd', 1, '2020-08-26 10:46:09', '2020-08-26 10:46:09'),
(5, 'jdmnb', 'njchx', 'syodq', 'COLOR', 'text', 1, '2020-08-26 11:51:53', '2020-08-26 11:51:53'),
(6, 'jdmnb', 'njchx', 'ckmvp', 'COLOR', 'text', 1, '2020-08-26 11:52:23', '2020-08-26 11:52:23'),
(7, 'jdmnb', 'njchx', 'ckmvp', 'PROCESSOR', 'dd', 1, '2020-08-26 11:53:36', '2020-08-26 11:53:36'),
(8, 'wxfiq', 'nbawm', 'tfkum', 'SIZE', 'text', 1, '2020-08-26 11:54:14', '2020-08-26 11:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_dd`
--

CREATE TABLE `attribute_dd` (
  `id` int(255) NOT NULL,
  `aaid` int(255) NOT NULL,
  `dd_val` varchar(50) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute_dd`
--

INSERT INTO `attribute_dd` (`id`, `aaid`, `dd_val`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, '32GB', 1, '2020-08-26 11:12:39', '2020-08-26 11:12:39'),
(2, 3, '64GB', 1, '2020-08-26 11:48:24', '2020-08-26 11:48:24'),
(3, 3, '128GB', 1, '2020-08-26 11:48:33', '2020-08-26 11:48:33'),
(4, 3, '256GB', 1, '2020-08-26 11:48:53', '2020-08-26 11:48:53'),
(5, 4, '13.5INCH', 1, '2020-08-26 11:50:59', '2020-08-26 11:50:59'),
(6, 4, '16', 1, '2020-08-26 11:51:14', '2020-08-26 11:51:14'),
(7, 7, 'I3', 1, '2020-08-26 11:54:45', '2020-08-26 11:54:45'),
(8, 7, 'I5', 1, '2020-08-26 11:54:56', '2020-08-26 11:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_tbl`
--

CREATE TABLE `attribute_tbl` (
  `at_id` int(255) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int(2) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute_tbl`
--

INSERT INTO `attribute_tbl` (`at_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'COLOR', 1, '2020-07-01 05:48:04', '2020-08-26 07:04:48'),
(2, 'SIZE', 1, '2020-07-01 05:48:04', '2020-08-26 07:04:48'),
(3, 'STORAGE', 1, NULL, '2020-08-26 07:04:48'),
(6, 'GENDER', 1, '2020-07-01 06:36:56', '2020-08-26 07:04:48'),
(7, 'DISPLAY', 1, '2020-08-26 07:21:25', '2020-08-26 07:21:25'),
(8, 'DISK', 1, '2020-08-26 07:22:29', '2020-08-26 07:27:46'),
(9, 'PROCESSOR', 1, '2020-08-26 11:52:54', '2020-08-26 11:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `berluti`
--

CREATE TABLE `berluti` (
  `id` int(255) NOT NULL,
  `p_id` varchar(20) NOT NULL,
  `collection_tag` varchar(10) DEFAULT NULL,
  `line_of_business` varchar(50) DEFAULT NULL,
  `vpn` varchar(50) DEFAULT NULL,
  `style_name` varchar(50) DEFAULT NULL,
  `style_no` varchar(50) DEFAULT NULL,
  `serial_no` varchar(50) DEFAULT NULL,
  `color_code` varchar(50) DEFAULT NULL,
  `material_code` varchar(50) DEFAULT NULL,
  `fit` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `hsncode` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `billing_addresses`
--

CREATE TABLE `billing_addresses` (
  `ba_id` int(255) NOT NULL,
  `ba_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `town` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(255) NOT NULL,
  `brand_key` varchar(10) NOT NULL,
  `store_key` varchar(10) NOT NULL,
  `brand_name` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_key`, `store_key`, `brand_name`, `status`, `created_at`, `updated_at`) VALUES
(26, 'wugsl', 'vrbsh', 'POLO', 1, '2020-07-14 08:10:50', '2020-07-14 08:10:50'),
(27, 'rydle', 'vrbsh', 'Saint Laurant', 1, '2020-07-22 06:46:48', '2020-07-22 06:46:48'),
(28, 'nxzhs', 'vrbsh', 'Brunello Cucinelli', 1, '2020-07-24 16:49:40', '2020-07-24 04:19:40'),
(29, 'cheoa', 'vrbsh', 'Molten Brown', 1, '2020-08-07 04:07:34', '2020-08-06 15:37:34'),
(30, 'jproh', 'vrbsh', 'HERSCHEL SUPPLY CO', 1, '2020-08-09 15:31:26', '2020-08-09 15:31:26'),
(31, 'hfoak', 'vrbsh', 'MLM', 1, '2020-08-09 15:42:01', '2020-08-09 15:42:01'),
(32, 'vthpi', 'vrbsh', 'REISS', 1, '2020-08-09 15:51:26', '2020-08-09 15:51:26'),
(33, 'gkpfe', 'vrbsh', 'OLIVIA ROSE THE LABEL', 1, '2020-08-10 06:17:02', '2020-08-10 06:17:02'),
(34, 'wxfiq', 'vrbsh', 'Berluti', 1, '2020-08-24 09:50:15', '2020-08-24 09:50:15'),
(35, 'jdmnb', 'vrbsh', 'apple', 1, '2020-08-24 09:52:18', '2020-08-24 09:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `brand_profile`
--

CREATE TABLE `brand_profile` (
  `bf_id` int(255) NOT NULL,
  `brand_key` varchar(10) NOT NULL,
  `title` text DEFAULT NULL,
  `subtitle` text NOT NULL,
  `image` text NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brand_profile`
--

INSERT INTO `brand_profile` (`bf_id`, `brand_key`, `title`, `subtitle`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'zsaqw', 'NEW COLLENTION 35% OFFER MEN', '<p>asjdal akshd hf hsfj&nbsp; lf afla haklalha a a ga as gasg asg aslk gasl gldg a dqyiuq yiu yiwy egiu galgaagzwoigaioteuggf</p>', 'brandpic_20200615160522.jpg', 0, '2020-06-15 10:35:22', '2020-06-15 11:01:39'),
(2, 'hvcmb', NULL, '<p>lkjljl</p>', 'brandpic_20200615173914.jpg', 0, '2020-06-15 12:09:14', '2020-06-15 12:16:09'),
(3, 'rydle', NULL, '<p>The illustrious label founded by Mr Yves Saint Laurent in 1961 still retains its French irreverence and sleek style under Creative Director Mr Anthony Vaccarello. Expect sharp tailoring, skinny-fit denim and edgy accessories.</p>', 'brandpic_20200722122019.jpg', 1, '2020-07-22 06:50:19', '2020-07-22 06:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(255) NOT NULL,
  `cat_key` varchar(10) NOT NULL,
  `category` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_key`, `category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fancy', 'Bags', 1, '2020-06-10 06:30:10', '2020-06-09 18:00:10'),
(2, 'nbawm', 'Shoes', 1, '2020-06-10 06:41:50', '2020-06-09 18:11:50'),
(3, 'njchx', 'Home & Tech', 1, '2020-06-09 18:46:50', '2020-06-09 18:46:50'),
(4, 'bedsz', 'Beauty', 1, '2020-06-09 18:53:37', '2020-06-09 18:53:37'),
(5, 'kxypo', 'BABY BOYS (0â€“3 YEARS)', 0, '2020-06-09 18:45:44', '2020-06-09 18:45:44'),
(6, 'qczhu', 'GIRLS (3â€“16 YEARS)', 0, '2020-06-09 18:45:36', '2020-06-09 18:45:36'),
(7, 'zdvfl', 'BOYS (3â€“16 YEARS)', 0, '2020-06-09 18:45:27', '2020-06-09 18:45:27'),
(8, 'xkmiv', 'Kids', 1, '2020-06-10 07:16:06', '2020-06-09 18:46:06'),
(9, 'zisqv', 'Jewellery & Watches', 1, '2020-06-10 07:43:46', '2020-06-09 19:13:46'),
(10, 'saqem', 'Clothing', 1, '2020-06-10 07:51:14', '2020-06-09 19:21:14'),
(11, 'lcgpa', 'Accessories', 1, '2020-06-10 07:55:09', '2020-06-09 19:25:09');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(255) NOT NULL,
  `client_key` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `newsletter` int(2) NOT NULL DEFAULT 1,
  `token` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_key`, `title`, `name`, `last_name`, `email`, `mobile`, `password`, `newsletter`, `token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'xhqgm', 'Mr', 'Anand', 'Verma', 'anandverma137@gmail.com', '09718190486', '05948c73ecc96c0a9e42dd5b3292dc7c', 1, 'sleckixrdtnhvgawqfupjmoyzb', 1, '2020-06-11 01:56:36', '2020-09-02 12:49:17'),
(2, 'jqrue', 'Mr', 'Manreet', 'Rathore', 'manreetrathore22@gmail.com', '9999910363', '8a1b2199934cc955b962c8d04e3a3aa5', 1, 'kwloefuyszqcnpritvahmjdbxg', 1, '2020-06-11 03:07:41', '2020-06-11 13:17:24'),
(3, 'uxael', 'Mr', 'Sachin', 'Jangid', 'sachin.jangid26@gmail.com', '9873987305', 'fdd07de9aea3b8730ce9569446b50522', 1, 'wjcbvtfoqmrlnguxikdzashpye', 1, '2020-06-11 23:28:29', '2020-06-11 10:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `color_id` int(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`color_id`, `color`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Weekly Featured', 1, '2020-08-20 05:36:52', '2020-08-20 05:36:52'),
(2, 'New This Week', 1, '2020-08-09 15:48:41', '2020-08-09 15:48:41'),
(3, 'New Season', 1, '2020-08-10 06:17:21', '2020-08-10 06:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `creed`
--

CREATE TABLE `creed` (
  `id` int(255) NOT NULL,
  `p_id` int(255) NOT NULL,
  `upc` varchar(100) DEFAULT NULL,
  `style_name` varchar(100) DEFAULT NULL,
  `style_no` varchar(100) DEFAULT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `ceated_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `deg_id` int(20) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`deg_id`, `designation`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SUPERADMIN', 1, NULL, '2020-08-12 10:34:56'),
(2, 'MANAGER', 1, NULL, '2020-08-12 10:34:56'),
(3, 'EDITOR', 1, NULL, '2020-08-12 10:35:17');

-- --------------------------------------------------------

--
-- Table structure for table `fit`
--

CREATE TABLE `fit` (
  `fit_id` int(255) NOT NULL,
  `fit` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fit`
--

INSERT INTO `fit` (`fit_id`, `fit`, `status`, `created_at`, `updated_at`) VALUES
(1, 'M', 1, '2020-08-07 04:09:35', '2020-08-06 15:39:35'),
(2, 'asklaSKVALKDV SDSADjklasdjas nkdsa knn', 0, '2020-08-19 05:41:26', '2020-08-19 05:41:26'),
(3, '5465456sad4s4fdsd', 1, '2020-08-19 05:39:55', '2020-08-19 05:39:55'),
(4, 'mkl jkfff dfkjgdkjd ddjfkfjk', 1, '2020-08-19 05:41:13', '2020-08-19 05:41:13');

-- --------------------------------------------------------

--
-- Table structure for table `home_section`
--

CREATE TABLE `home_section` (
  `hs_id` int(255) NOT NULL,
  `section` int(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` text NOT NULL,
  `link` text DEFAULT NULL,
  `image_one` text NOT NULL,
  `image_two` text NOT NULL,
  `image_three` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `slider_id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `image` text NOT NULL,
  `block_type` varchar(100) DEFAULT NULL,
  `text_align` varchar(200) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`slider_id`, `title`, `subtitle`, `link`, `image`, `block_type`, `text_align`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Shop', '', '', 'slider_20200610003015.jpg', 'top', 'center', 0, '2020-06-09 19:02:39', '2020-06-09 19:02:39'),
(2, 'Shop', '', '', 'slider_20200610005611.jpg', 'top', 'center', 0, '2020-06-10 14:35:34', '2020-06-10 14:35:34'),
(3, 'Shop', '', '', 'slider_20200610010051.jpg', 'top', 'center', 1, '2020-06-10 08:00:51', '2020-06-09 19:30:51'),
(4, 'Shop now', '', '', 'slider_20200610012835.jpg', 'top', 'center', 0, '2020-06-09 19:59:20', '2020-06-09 19:59:20'),
(5, 'Shop now', '', '', 'slider_20200610013002.jpg', 'top', 'right', 1, '2020-06-09 20:00:58', '2020-06-09 20:00:58'),
(6, '.', '', '', 'slider_20200610182035.png', 'top', 'center', 1, '2020-06-10 12:50:35', '2020-06-10 12:50:35'),
(7, '.', '', '', 'slider_20200610014406.jpg', 'top', 'center', 0, '2020-06-09 20:14:49', '2020-06-09 20:14:49'),
(8, 'Thoughtful Gifts at LSMART', '', '', 'slider_20200611152126.jpg', 'middle', 'center', 1, '2020-06-11 09:56:47', '2020-06-11 09:56:47'),
(9, 'Stuff to make your world brighter', '', '', 'slider_20200611152918.jpeg', 'bottom', 'center', 1, '2020-06-11 22:29:18', '2020-06-11 09:59:18'),
(10, 'Slider', '', '', 'slider_20200611170301.png', 'top', 'center', 1, '2020-06-12 00:03:01', '2020-06-11 11:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(100) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `category` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bags', 'fancy', 1, '2020-06-10 06:33:04', '2020-06-09 18:03:04'),
(2, 'Shoes', 'nbawm', 1, '2020-06-10 06:49:27', '2020-06-09 18:19:27'),
(3, 'Home & Tech', 'njchx', 1, '2020-06-10 07:05:06', '2020-06-09 18:35:06'),
(4, 'Kids', 'xkmiv', 1, '2020-06-10 07:19:23', '2020-06-09 18:49:23'),
(5, 'Beauty ', 'bedsz', 1, '2020-06-10 07:27:26', '2020-06-09 18:57:26'),
(6, 'Jewellery & Watches', 'zisqv', 1, '2020-06-10 07:47:16', '2020-06-09 19:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(255) NOT NULL,
  `order_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `total_items` int(100) NOT NULL,
  `amount` float NOT NULL,
  `billing_address` varchar(20) NOT NULL,
  `shipping_address` varchar(20) NOT NULL,
  `d_status` int(2) DEFAULT 0,
  `payment` varchar(100) NOT NULL DEFAULT 'COD',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_key`, `user`, `total_items`, `amount`, `billing_address`, `shipping_address`, `d_status`, `payment`, `created_at`, `updated_at`) VALUES
(12, 'epaym', 'xhqgm', 1, 15000, 'rkxyt', 'rkxyt', 0, 'COD', '2020-08-11 10:07:46', '2020-08-11 10:07:46'),
(13, 'bldmi', 'xhqgm', 1, 15000, 'rkxyt', 'rkxyt', 0, 'COD', '2020-08-11 10:09:54', '2020-08-11 10:09:54'),
(14, 'svnia', 'xhqgm', 2, 68000, 'rkxyt', 'rkxyt', 0, 'COD', '2020-08-11 10:16:50', '2020-08-11 10:16:50'),
(15, 'zdkiw', 'xhqgm', 2, 55000, 'rkxyt', 'rkxyt', 0, 'COD', '2020-08-20 10:50:47', '2020-08-20 10:50:47'),
(16, 'nwgbp', 'xhqgm', 1, 4200, 'rkxyt', 'rkxyt', 0, 'COD', '2020-08-20 10:52:19', '2020-08-20 10:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `orders_status`
--

CREATE TABLE `orders_status` (
  `os_id` int(255) NOT NULL,
  `orderid` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `status_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_book`
--

CREATE TABLE `order_book` (
  `ob_id` int(255) NOT NULL,
  `order_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `qty` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL,
  `pono` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_book`
--

INSERT INTO `order_book` (`ob_id`, `order_key`, `user`, `product`, `color`, `size`, `qty`, `price`, `pono`, `created_at`, `updated_at`) VALUES
(11, 'epaym', 'xhqgm', 'MOON-K-103', 'Red', 'L', '1', '15000', '3158', '2020-08-11 10:07:46', '2020-08-11 10:07:46'),
(12, 'bldmi', 'xhqgm', 'MOON-K-101', 'Red', 'L', '1', '15000', '1202', '2020-08-11 10:09:54', '2020-08-11 10:09:54'),
(13, 'svnia', 'xhqgm', 'MOON-B-002', 'COGNAC', '', '1', '40000', '2311121', '2020-08-11 10:16:50', '2020-08-11 10:16:51'),
(14, 'svnia', 'xhqgm', 'MOON-T-001', 'GREEN', '6', '1', '28000', '3158', '2020-08-11 10:16:50', '2020-08-11 10:16:51'),
(15, 'zdkiw', 'xhqgm', 'MOON-B-002', 'COGNAC', '', '1', '40000', '2311121', '2020-08-20 10:50:47', '2020-08-20 10:50:47'),
(16, 'zdkiw', 'xhqgm', 'MOON-K-102', 'Black', 'L', '1', '15000', '', '2020-08-20 10:50:47', '2020-08-20 10:50:47'),
(17, 'nwgbp', 'xhqgm', 'MOON-B-001', 'Navy', '', '1', '4200', '', '2020-08-20 10:52:19', '2020-08-20 10:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` int(255) NOT NULL,
  `p_key` varchar(10) NOT NULL,
  `store` varchar(10) NOT NULL,
  `brand` varchar(10) NOT NULL,
  `product_name` text NOT NULL,
  `sku` varchar(255) NOT NULL,
  `category` varchar(10) NOT NULL,
  `subcategory` varchar(10) NOT NULL,
  `add_cat` varchar(255) DEFAULT NULL,
  `mrp` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `specification` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `features` text DEFAULT NULL,
  `review` int(2) NOT NULL DEFAULT 1,
  `publish` int(2) NOT NULL DEFAULT 1,
  `status` int(2) NOT NULL DEFAULT 1,
  `weight` varchar(200) DEFAULT NULL,
  `img` int(255) DEFAULT NULL,
  `type` int(2) DEFAULT 0,
  `groupid` varchar(100) DEFAULT NULL,
  `product_code` varchar(200) DEFAULT NULL,
  `collection_tag` varchar(50) DEFAULT NULL,
  `order_start_date` date DEFAULT NULL,
  `line_of_business` varchar(100) DEFAULT NULL,
  `vpn` varchar(100) DEFAULT NULL,
  `style_name` varchar(100) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `material_code` varchar(100) DEFAULT NULL,
  `fit` varchar(100) DEFAULT NULL,
  `batch_no` varchar(100) DEFAULT NULL,
  `min_sale_quantity` varchar(10) DEFAULT NULL,
  `stock` varchar(50) DEFAULT '0',
  `low_stock` varchar(50) DEFAULT NULL,
  `styleno` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `status`, `weight`, `img`, `type`, `groupid`, `product_code`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `styleno`, `hsncode`, `created_at`, `updated_at`) VALUES
(31, 'bmnhp', 'vrbsh', 'jproh', 'Settlement backpack', 'MOON-B-001', 'fancy', 'rihfu', '', '', 4200, '<ul>\r\n<li><strong>Herschel</strong>&nbsp;canvas backpack</li>\r\n<li>Zipped closure</li>\r\n<li>Adjustable shoulder straps, external zipped pocket with key clip, internal 15\" sleeve pocket, internal media pocket, coated cotton-poly fabric lining</li>\r\n<li>Suitable for laptops up to 15 inches</li>\r\n<li>Textile</li>\r\n<li>Wipe clean with a damp cloth</li>\r\n<li>Capacity 20 litres</li>\r\n<li>Height 45cm, width 30.5cm, depth 14.5cm</li>\r\n</ul>', '<ul>\r\n<li><strong>Herschel</strong>&nbsp;canvas backpack</li>\r\n<li>Zipped closure</li>\r\n<li>Adjustable shoulder straps, external zipped pocket with key clip, internal 15\" sleeve pocket, internal media pocket, coated cotton-poly fabric lining</li>\r\n<li>Suitable for laptops up to 15 inches</li>\r\n<li>Textile</li>\r\n<li>Wipe clean with a damp cloth</li>\r\n<li>Capacity 20 litres</li>\r\n<li>Height 45cm, width 30.5cm, depth 14.5cm</li>\r\n</ul>', '', 0, 1, 1, NULL, NULL, 0, 'MOON-B-001', NULL, 'PE', '2020-08-09', '', '001', 'S4918 KNIT + VN NAVY', 'S4918-001', 'B72', '001', '', '001', '1', '10', '3', NULL, NULL, '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(32, 'ztuks', 'vrbsh', 'hfoak', 'Stark studded Visetos coated canvas mini backpack', 'MOON-B-002', 'fancy', 'rihfu', '', '', 40000, '<ul>\r\n<li><strong>MCM</strong>&nbsp;coated canvas backpack</li>\r\n<li>Zip fastening</li>\r\n<li>Top handle, detachable shoulder straps, external zipped pocket, side pockets, Visetos-printed, studded, two internal card slots, engraved logo plaque</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Height 20.5cm, width 16cm, depth 11cm</li>\r\n</ul>', '<ul>\r\n<li><strong>MCM</strong>&nbsp;coated canvas backpack</li>\r\n<li>Zip fastening</li>\r\n<li>Top handle, detachable shoulder straps, external zipped pocket, side pockets, Visetos-printed, studded, two internal card slots, engraved logo plaque</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Height 20.5cm, width 16cm, depth 11cm</li>\r\n</ul>', '', 0, 1, 1, NULL, NULL, 1, 'MOON-B-002', NULL, '', '0000-00-00', 'S4918 KNIT + VN NAVY', '002', 'S4918 KNIT + VN NAVY', '002', 'B72', '002', '', '002', '1', '10', '2', NULL, NULL, '2020-08-09 15:45:01', '2020-08-09 15:47:07'),
(33, 'pfzrv', 'vrbsh', 'vthpi', 'Rose ruffle woven top', 'MOON-T-001', 'saqem', 'ltpsw', '', '', 28000, '<ul>\r\n<ul>\r\n<li><strong>Reiss</strong>&nbsp;woven top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% polyester</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Halterneck, sleeveless, ruffled trim at shoulders and hems, all-over pintuck pleats</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>', '<ul>\r\n<ul>\r\n<li><strong>Reiss</strong>&nbsp;woven top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% polyester</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Halterneck, sleeveless, ruffled trim at shoulders and hems, all-over pintuck pleats</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>', '', 0, 1, 2, NULL, NULL, 0, 'MOON-T-001', NULL, 'SE', '2020-08-09', 'S4918 KNIT + VN NAVY', '003', 'S4918 KNIT + VN NAVY', '003', 'B72', '003', '', '003', '1', '10', '3', NULL, NULL, '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(34, 'cugzv', 'vrbsh', 'vthpi', 'Rose ruffle woven top', 'MOON-T-002', 'saqem', 'ltpsw', '', '', 28000, '<ul>\r\n<ul>\r\n<li><strong>Reiss</strong>&nbsp;woven top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% polyester</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Halterneck, sleeveless, ruffled trim at shoulders and hems, all-over pintuck pleats</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>', '<ul>\r\n<ul>\r\n<li><strong>Reiss</strong>&nbsp;woven top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% polyester</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Halterneck, sleeveless, ruffled trim at shoulders and hems, all-over pintuck pleats</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>', '', 0, 1, 2, NULL, NULL, 1, 'MOON-T-001', NULL, 'SE', '2020-08-09', '', '004', '', '004', 'b73', '', '', '004', '1', '10', '3', NULL, NULL, '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(35, 'fqgki', 'vrbsh', 'gkpfe', 'Esmeralda puffed-sleeve organic cotton-poplin top', 'MOON-K-101', 'saqem', 'ltpsw', '', '', 15000, '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '', 0, 1, 3, NULL, NULL, 0, 'MOON-K-101', NULL, 'SE', '2020-08-10', 'S4918 KNIT + VN NAVY', '005', 'S4918 KNIT + VN NAVY', '005', 'B72', 'Fabric', '', '005', '1', '5', '2', NULL, NULL, '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(36, 'jkxbi', 'vrbsh', 'gkpfe', 'Esmeralda puffed-sleeve organic cotton-poplin top', 'MOON-K-102', 'saqem', 'ltpsw', '', '', 15000, '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '', 0, 1, 3, NULL, NULL, 1, 'MOON-K-101', NULL, 'SE', '2020-08-10', '', '006', '', '006', 'b73', 'fabric', '', '006', '1', '5', '2', NULL, NULL, '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(37, 'rwyjp', 'vrbsh', 'gkpfe', 'Esmeralda puffed-sleeve organic cotton-poplin top', 'MOON-K-103', 'saqem', 'ltpsw', '', '', 15000, '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '<ul>\r\n<ul>\r\n<li><strong>Olivia Rose The Label</strong>&nbsp;organic-cotton poplin top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% organic cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square neck, elasticated neckline and shoulders, puff shoulders, long sleeves, shirred bodice and panels at sleeves, flared trims</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size small: length 20in/ 51cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Model is 5ft 10in/1.78m and wears a size small</li>\r\n</ul>', '', 0, 1, 3, NULL, NULL, 1, 'MOON-K-101', NULL, '', '0000-00-00', '', '006', '002', '006', 'B72', '', '', '006', '1', '5', '2', NULL, NULL, '2020-08-10 06:28:46', '2020-08-13 09:05:49'),
(39, 'dnzwv', 'vrbsh', 'jdmnb', 'Typical Miracle', 'MOON00103fghf', 'njchx', 'syodq', '', NULL, 45000, '<p>jjk</p>', '<p>jlk</p>', '<p>j</p>', 0, 1, 2, NULL, NULL, 1, 'MOON00103fghf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-24 12:27:08', '2020-08-25 06:27:45'),
(40, 'tzsib', 'vrbsh', 'jdmnb', 'Typical Miracle', 'MOON-LB-002sdfsdf', 'njchx', 'syodq', '', NULL, 45000, '<p>jjk</p>', '<p>jlk</p>', '<p>j</p>', 0, 1, 2, NULL, NULL, 1, 'MOON00103fghf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '0', '1', NULL, NULL, '2020-08-25 07:28:40', '2020-08-25 07:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `pa_id` int(255) NOT NULL,
  `p_id` varchar(255) NOT NULL,
  `attribute` varchar(255) DEFAULT NULL,
  `storage` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `imgid` int(255) DEFAULT NULL,
  `sample` text DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`pa_id`, `p_id`, `attribute`, `storage`, `color`, `size`, `groupid`, `imgid`, `sample`, `gender`, `created_at`, `updated_at`) VALUES
(63, 'MOON-B-001', NULL, NULL, 'Navy', '', 'MOON-B-001', NULL, 'clrsample_20200809210823.jpg', '', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(64, 'MOON-B-002', NULL, NULL, 'COGNAC', '', 'MOON-B-002', NULL, 'clrsample_20200809211501.jpg', '', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(65, 'MOON-T-001', NULL, NULL, 'GREEN', '6', 'MOON-T-001', NULL, 'clrsample_20200809212850.jpg', 'Women', '2020-08-09 15:58:50', '2020-08-09 16:28:00'),
(66, 'MOON-T-002', NULL, NULL, 'GREEN', '8', 'MOON-T-001', NULL, 'clrsample_20200809212850.jpg', 'Women', '2020-08-09 16:26:10', '2020-08-09 16:28:00'),
(67, 'MOON-K-101', NULL, NULL, 'Red', 'XS', 'MOON-K-101', NULL, 'clrsample_20200810115258.jpg', 'Women', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(68, 'MOON-K-102', NULL, NULL, 'Black', 'L', 'MOON-K-101', NULL, 'clrsample_20200810115641.jpg', 'Women', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(69, 'MOON-K-103', NULL, NULL, 'Red', 'L', 'MOON-K-101', NULL, 'clrsample_20200810115258.jpg', 'Women', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(70, 'MOON00103fghf', NULL, NULL, '', 'jh', 'MOON00103fghf', NULL, '', '', '2020-08-24 12:27:08', '2020-08-24 12:27:08'),
(71, 'MOON-LB-002sdfsdf', NULL, NULL, '', 'jh', 'MOON00103fghf', NULL, NULL, '', '2020-08-25 07:28:40', '2020-08-25 07:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `pi_id` int(255) NOT NULL,
  `product` varchar(200) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `product`, `image`, `created_at`, `updated_at`) VALUES
(3, 'MOON-B-001', 'product_MOON-B-001_1_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(4, 'MOON-B-001', 'product_MOON-B-001_2_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(5, 'MOON-B-001', 'product_MOON-B-001_3_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(6, 'MOON-B-001', 'product_MOON-B-001_4_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(7, 'MOON-B-002', 'product_MOON-B-002_1_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(8, 'MOON-B-002', 'product_MOON-B-002_2_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(9, 'MOON-B-002', 'product_MOON-B-002_3_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(10, 'MOON-B-002', 'product_MOON-B-002_4_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(11, 'MOON-B-002', 'product_MOON-B-002_1_20200809211707.jpg', '2020-08-09 15:47:07', '2020-08-09 15:47:07'),
(12, 'MOON-T-001', 'product_MOON-T-001_1_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(13, 'MOON-T-001', 'product_MOON-T-001_2_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(14, 'MOON-T-001', 'product_MOON-T-001_3_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(15, 'MOON-T-002', 'product_MOON-T-001_1_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(16, 'MOON-T-002', 'product_MOON-T-001_2_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(17, 'MOON-T-002', 'product_MOON-T-001_3_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(18, 'MOON-K-101', 'product_MOON-K-101_1_20200810115258.jpg', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(19, 'MOON-K-101', 'product_MOON-K-101_2_20200810115258.jpg', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(20, 'MOON-K-101', 'product_MOON-K-101_3_20200810115258.jpg', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(21, 'MOON-K-101', 'product_MOON-K-101_4_20200810115258.jpg', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(22, 'MOON-K-101', 'product_MOON-K-101_5_20200810115258.jpg', '2020-08-10 06:22:58', '2020-08-10 06:22:58'),
(23, 'MOON-K-102', 'product_MOON-K-101_1_20200810115641.jpg', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(24, 'MOON-K-102', 'product_MOON-K-101_2_20200810115641.jpg', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(25, 'MOON-K-102', 'product_MOON-K-101_3_20200810115641.jpg', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(26, 'MOON-K-102', 'product_MOON-K-101_4_20200810115641.jpg', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(27, 'MOON-K-102', 'product_MOON-K-101_5_20200810115641.jpg', '2020-08-10 06:26:40', '2020-08-10 06:26:41'),
(28, 'MOON-K-103', 'product_MOON-K-101_1_20200810115258.jpg', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(29, 'MOON-K-103', 'product_MOON-K-101_2_20200810115258.jpg', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(30, 'MOON-K-103', 'product_MOON-K-101_3_20200810115258.jpg', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(31, 'MOON-K-103', 'product_MOON-K-101_4_20200810115258.jpg', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(32, 'MOON-K-103', 'product_MOON-K-101_5_20200810115258.jpg', '2020-08-10 06:28:46', '2020-08-10 06:28:46'),
(33, 'MOON-K-103', 'product_MOON-K-103_1_20200813143549.jpg', '2020-08-13 09:05:48', '2020-08-13 09:05:51'),
(34, 'MOON00103fghf', 'product_MOON00103fghf_1_20200824175708.jpg', '2020-08-24 12:27:08', '2020-08-24 12:27:08'),
(35, 'MOON-LB-002sdfsdf', 'product_MOON00103fghf_1_20200824175708.jpg', '2020-08-25 07:28:40', '2020-08-25 07:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `ps_id` int(255) NOT NULL,
  `product` varchar(100) NOT NULL,
  `challan` varchar(100) NOT NULL,
  `date` date DEFAULT NULL,
  `qty` int(100) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `place_order_date` date DEFAULT NULL,
  `purchase_price` float DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`ps_id`, `product`, `challan`, `date`, `qty`, `start_date`, `end_date`, `place_order_date`, `purchase_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'MOON-T-002', '1201', '2020-08-10', 10, NULL, NULL, NULL, NULL, 1, '2020-08-10 11:04:01', '2020-08-10 11:04:01'),
(2, 'MOON-T-002', '1202', '2020-08-10', 5, NULL, NULL, NULL, NULL, 1, '2020-08-10 11:15:14', '2020-08-10 11:15:14'),
(3, 'MOON-T-002', '1202', '2020-08-10', 5, NULL, NULL, NULL, NULL, 1, '2020-08-10 11:15:31', '2020-08-10 11:15:31'),
(4, 'MOON-T-002', '1205', '2020-08-10', 5, NULL, NULL, NULL, NULL, 1, '2020-08-10 11:17:47', '2020-08-10 11:17:47'),
(5, 'MOON-T-002', '1206', '2020-10-10', 2, NULL, NULL, NULL, NULL, 1, '2020-08-10 11:21:31', '2020-08-10 11:21:31'),
(6, 'MOON-T-001', '3158', '2020-08-12', 5, NULL, NULL, NULL, NULL, 1, '2020-08-11 11:06:22', '2020-08-11 11:06:22'),
(7, 'MOON-K-103', '3158', '2020-08-11', 10, NULL, NULL, NULL, NULL, 1, '2020-08-11 15:13:34', '2020-08-11 15:13:34'),
(8, 'MOON-K-101', '1202', '2020-08-10', 15, NULL, NULL, NULL, NULL, 1, '2020-08-11 15:39:34', '2020-08-11 15:39:34'),
(9, 'MOON-B-002', '2311121', '2020-08-10', 15, NULL, NULL, NULL, NULL, 1, '2020-08-11 15:44:46', '2020-08-11 15:44:46'),
(10, 'MOON-T-002', '2131', '2020-08-10', 10, NULL, NULL, NULL, NULL, 1, '2020-08-11 15:45:58', '2020-08-11 15:45:58'),
(11, 'MOON-K-103', '3158', '2020-08-19', 10, '0000-00-00', '0000-00-00', '0000-00-00', 12000, 1, '2020-08-19 16:58:29', '2020-08-19 16:58:29'),
(12, 'MOON-K-103', '3158', '2020-08-12', 15, '2020-08-19', '2020-08-22', '2020-08-31', 12000, 1, '2020-08-19 17:00:57', '2020-08-19 17:00:57'),
(13, 'MOON-K-103', '2311121', '2020-08-12', 12, '2020-08-20', '2020-08-24', '2020-08-27', 12000, 1, '2020-08-19 17:30:44', '2020-08-19 17:30:44'),
(14, 'MOON-K-103', '2311121', '2020-08-12', 12, '2020-08-20', '2020-08-24', '2020-08-27', 12000, 1, '2020-08-19 17:31:27', '2020-08-19 17:31:27'),
(15, 'MOON-K-103', '2311121', '2020-08-12', 12, '2020-08-20', '2020-08-24', '2020-08-27', 12000, 1, '2020-08-19 17:32:18', '2020-08-19 17:32:18'),
(16, 'MOON-K-103', '2311121', '2020-08-20', 10, '2020-08-12', '2020-08-18', '2020-09-01', 12000, 1, '2020-08-19 17:46:55', '2020-08-20 17:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `shop_by_cat`
--

CREATE TABLE `shop_by_cat` (
  `sb_id` int(255) NOT NULL,
  `title` text NOT NULL,
  `link` text DEFAULT NULL,
  `img` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `type` int(2) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shop_by_cat`
--

INSERT INTO `shop_by_cat` (`sb_id`, `title`, `link`, `img`, `status`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Trending: Shoes', 'https://demo.inovoteq.com/cat?cat=nbawm-_st!_Men', 'slider_20200610000748.jpg', 1, 1, '2020-06-11 11:17:51', '2020-06-11 11:17:51'),
(2, 'Trending: Men\'s Collection', 'https://demo.inovoteq.com/cat?cat=saqem-_st!_Men', 'slider_20200610000904.jpg', 1, 1, '2020-06-11 11:16:38', '2020-06-11 11:16:38'),
(3, 'Trending: Shoes', 'https://demo.inovoteq.com/cat?cat=nbawm-_st!_Men', 'slider_20200610000957.jpg', 1, 1, '2020-06-11 11:13:27', '2020-06-11 11:13:27'),
(4, 'Trending: Fragrances', 'https://demo.inovoteq.com/cat?cat=bedsz-gfqra_st!_', 'slider_20200610001054.jpg', 1, 1, '2020-06-11 11:12:08', '2020-06-11 11:12:08'),
(5, 'Shop Men\'s', 'https://demo.inovoteq.com/cat?cat=saqem-_st!_Men', 'slider_20200610092745.jpeg', 1, 0, '2020-06-11 11:08:19', '2020-06-11 11:08:19'),
(6, 'Shop Women\'s ', 'https://demo.inovoteq.com/cat?cat=saqem-_st!_Women', 'slider_20200610092822.jpg', 1, 0, '2020-06-11 11:07:35', '2020-06-11 11:07:35'),
(7, 'Shop Beauty', 'https://demo.inovoteq.com/cat?cat=bedsz-_st!_', 'slider_20200610092909.jpeg', 1, 0, '2020-06-11 11:05:25', '2020-06-11 11:05:25'),
(8, 'Shop Home &Tech', 'https://demo.inovoteq.com/cat?cat=njchx-_st!_', 'slider_20200610093050.jpg', 1, 0, '2020-06-11 11:04:26', '2020-06-11 11:04:26'),
(9, 'Shop Fragrance', 'https://demo.inovoteq.com/cat?cat=bedsz-gfqra_st!_', 'slider_20200610093428.jpg', 1, 0, '2020-06-11 11:02:30', '2020-06-11 11:02:30'),
(10, 'Shop Kid\'s', 'https://demo.inovoteq.com/cat?cat=xkmiv-_st!_', 'slider_20200610093625.jpg', 1, 0, '2020-06-11 11:03:26', '2020-06-11 11:03:26'),
(11, 'Women\'s Shoulder bags', 'https://demo.inovoteq.com/cat?cat=fancy-esowr_st!_', 'slider_20200611153743.png', 1, 2, '2020-06-11 10:47:07', '2020-06-11 10:47:07'),
(12, 'Women\'s Sandals', 'https://demo.inovoteq.com/brand?brand=Gucci__nbawm', 'slider_20200611154413.png', 0, 1, '2020-06-11 10:15:32', '2020-06-11 10:15:32'),
(13, 'Men\'s Loafers', 'https://demo.inovoteq.com/cat?cat=nbawm-tyfcl_st!_Men', 'slider_20200611154732.png', 1, 2, '2020-06-11 10:29:39', '2020-06-11 10:29:39'),
(14, 'Women\'s Skirt', 'https://demo.inovoteq.com/cat?cat=saqem-wifle_st!_Women', 'slider_20200611155705.png', 1, 2, '2020-06-11 22:57:05', '2020-06-11 10:27:05'),
(15, 'Women\'s Fragrance', 'https://demo.inovoteq.com/cat?cat=bedsz-gfqra_st!_Women', 'slider_20200611160551.png', 1, 2, '2020-06-11 23:05:51', '2020-06-11 10:35:51'),
(16, 'Women\'s Shirt', 'https://demo.inovoteq.com/cat?cat=saqem-xplro_st!_Women', 'slider_20200611162606.png', 0, 2, '2020-06-11 11:00:31', '2020-06-11 11:00:31'),
(17, 'Women\'s Makeup', 'https://demo.inovoteq.com/brand?brand=Charlotte%20Tilbury_Women_', 'slider_20200611163007.png', 1, 2, '2020-06-11 23:30:07', '2020-06-11 11:00:07');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `store_id` int(255) NOT NULL,
  `store_key` varchar(10) NOT NULL,
  `store_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `gst` varchar(100) DEFAULT NULL,
  `password` text NOT NULL,
  `address` text NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`store_id`, `store_key`, `store_name`, `email`, `mobile`, `gst`, `password`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vrbsh', 'Bequest Group', 'gaganmeet@bequestgroup.com', '9899123123', '', 'e1b4755403710e0deb7aa5d45e43996d', '30/1 East Patel Nagar, Delhi ', 1, '2020-06-10 06:47:35', '2020-06-09 18:17:35');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `subcat_id` int(255) NOT NULL,
  `subcat_key` varchar(10) NOT NULL,
  `cat_key` varchar(10) NOT NULL,
  `subcategory` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`subcat_id`, `subcat_key`, `cat_key`, `subcategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rihfu', 'fancy', 'Backpacks', 1, '2020-06-10 06:30:38', '2020-06-09 18:00:38'),
(2, 'kxryd', 'fancy', 'Belt Bags', 1, '2020-06-10 06:30:51', '2020-06-09 18:00:51'),
(3, 'ihqtv', 'fancy', 'Bucket Bags', 1, '2020-06-10 06:31:11', '2020-06-09 18:01:11'),
(4, 'treia', 'fancy', 'Clutch Bags', 1, '2020-06-10 06:31:29', '2020-06-09 18:01:29'),
(5, 'mdokh', 'fancy', 'Cross Body Bags', 1, '2020-06-10 06:31:41', '2020-06-09 18:01:41'),
(6, 'exhwn', 'fancy', 'Evening Bags', 1, '2020-06-10 06:31:56', '2020-06-09 18:01:56'),
(7, 'puqjh', 'fancy', 'Luggage and Travel', 1, '2020-06-10 06:32:07', '2020-06-09 18:02:07'),
(8, 'esowr', 'fancy', 'Shoulder Bags', 1, '2020-06-10 06:32:18', '2020-06-09 18:02:18'),
(9, 'xrlas', 'fancy', 'Tote Bags', 1, '2020-06-10 06:32:29', '2020-06-09 18:02:29'),
(10, 'draim', 'nbawm', 'Ankle Boots', 1, '2020-06-10 06:42:23', '2020-06-09 18:12:23'),
(11, 'brnpl', 'nbawm', 'Boots', 1, '2020-06-10 06:42:35', '2020-06-09 18:12:35'),
(12, 'dgzfq', 'nbawm', 'Espadrilles', 1, '2020-06-10 06:42:44', '2020-06-09 18:12:44'),
(13, 'tfkum', 'nbawm', 'Evening Shoes', 1, '2020-06-10 06:43:13', '2020-06-09 18:13:13'),
(14, 'mwbki', 'nbawm', 'Flat Shoes', 1, '2020-06-10 06:43:25', '2020-06-09 18:13:25'),
(15, 'erblu', 'nbawm', 'Mules', 1, '2020-06-10 06:43:42', '2020-06-09 18:13:42'),
(16, 'hoplq', 'nbawm', 'Pumps', 1, '2020-06-10 06:43:55', '2020-06-09 18:13:55'),
(17, 'gmyjl', 'nbawm', 'Sandals', 1, '2020-06-10 06:44:08', '2020-06-09 18:14:08'),
(18, 'rewil', 'nbawm', 'Sneakers', 1, '2020-06-10 06:44:15', '2020-06-09 18:14:15'),
(19, 'qldej', 'nbawm', 'Boat Shoes ', 1, '2020-06-10 06:44:27', '2020-06-09 18:14:27'),
(20, 'qrckp', 'nbawm', 'Brogues ', 1, '2020-06-10 06:44:37', '2020-06-09 18:14:37'),
(21, 'mgqaj', 'nbawm', 'Chelsea Boots ', 1, '2020-06-10 06:44:50', '2020-06-09 18:14:50'),
(22, 'wlpdo', 'nbawm', 'Derby Shoes', 1, '2020-06-10 06:45:00', '2020-06-09 18:15:00'),
(23, 'ikvzp', 'nbawm', 'Driving Shoes ', 1, '2020-06-10 06:45:08', '2020-06-09 18:15:08'),
(24, 'elvku', 'nbawm', 'House Shoes ', 1, '2020-06-10 06:45:30', '2020-06-09 18:15:30'),
(25, 'ybmkr', 'nbawm', 'Lace-Up Boots ', 1, '2020-06-10 06:45:39', '2020-06-09 18:15:39'),
(26, 'tyfcl', 'nbawm', 'Loafers ', 1, '2020-06-10 06:46:44', '2020-06-09 18:16:44'),
(27, 'cseyg', 'nbawm', 'Monk Strap Shoes', 1, '2020-06-10 06:46:55', '2020-06-09 18:16:55'),
(28, 'hzvim', 'nbawm', 'Oxford Shoes ', 1, '2020-06-10 06:47:02', '2020-06-09 18:17:02'),
(29, 'ytzix', 'nbawm', 'Slides ', 1, '2020-06-10 06:47:10', '2020-06-09 18:17:10'),
(30, 'krhib', 'nbawm', 'Slippers ', 1, '2020-06-10 06:47:19', '2020-06-09 18:17:19'),
(31, 'alptd', 'nbawm', 'Summer Shoes ', 1, '2020-06-10 06:47:37', '2020-06-09 18:17:37'),
(32, 'ncotb', 'nbawm', 'Hiking Boots', 1, '2020-06-10 06:47:46', '2020-06-09 18:17:46'),
(33, 'ckmvp', 'njchx', 'Laptop', 1, '2020-06-10 07:03:05', '2020-06-09 18:33:05'),
(34, 'syodq', 'njchx', 'iPhone', 1, '2020-06-10 07:03:22', '2020-06-09 18:33:22'),
(35, 'cjwrx', 'njchx', 'iPad', 1, '2020-06-10 07:03:35', '2020-06-09 18:33:35'),
(36, 'pmwzx', 'njchx', 'Accessories', 1, '2020-06-10 07:03:49', '2020-06-09 18:33:49'),
(37, 'kziam', 'bedsz', 'Rompers & Sleepsuits', 0, '2020-06-09 19:01:47', '2020-06-09 19:01:47'),
(38, 'lqoew', 'bedsz', 'Dresses', 0, '2020-06-09 19:02:28', '2020-06-09 19:02:28'),
(39, 'pdlfn', 'bedsz', 'Tops', 0, '2020-06-09 19:02:14', '2020-06-09 19:02:14'),
(40, 'jdeix', 'bedsz', 'Coats', 0, '2020-06-09 19:02:02', '2020-06-09 19:02:02'),
(41, 'wuvbm', 'bedsz', 'Accessories', 1, '2020-06-10 07:08:06', '2020-06-09 18:38:06'),
(42, 'bmpzk', 'bedsz', 'Shoes', 0, '2020-06-09 19:01:12', '2020-06-09 19:01:12'),
(43, 'qlvxy', 'xkmiv', 'Rompers & Sleepsuits', 1, '2020-06-10 07:17:12', '2020-06-09 18:47:12'),
(44, 'ikgof', 'xkmiv', 'Dresses', 1, '2020-06-10 07:17:21', '2020-06-09 18:47:21'),
(45, 'fmtvc', 'xkmiv', 'Tops', 1, '2020-06-10 07:17:38', '2020-06-09 18:47:38'),
(46, 'gljqc', 'xkmiv', 'Coats', 1, '2020-06-10 07:17:48', '2020-06-09 18:47:48'),
(47, 'ajinq', 'xkmiv', 'Shoes', 1, '2020-06-10 07:18:13', '2020-06-09 18:48:13'),
(48, 'cohkq', 'xkmiv', 'T-Shirts', 1, '2020-06-10 07:18:31', '2020-06-09 18:48:31'),
(49, 'hceub', 'xkmiv', 'Trousers', 1, '2020-06-10 07:18:44', '2020-06-09 18:48:44'),
(50, 'xfqbo', 'xkmiv', 'Knitwear', 1, '2020-06-10 07:18:54', '2020-06-09 18:48:54'),
(51, 'gmpuo', 'xkmiv', 'Accessories', 1, '2020-06-10 07:19:04', '2020-06-09 18:49:04'),
(52, 'qpfel', 'bedsz', 'Bath and Body', 1, '2020-06-10 07:24:13', '2020-06-09 18:54:13'),
(53, 'gfqra', 'bedsz', 'Fragrance', 1, '2020-06-10 07:24:22', '2020-06-09 18:54:22'),
(54, 'dkoqc', 'bedsz', 'Haircare Sets ', 1, '2020-06-10 07:24:35', '2020-06-09 18:54:35'),
(55, 'voefn', 'bedsz', 'Shave ', 1, '2020-06-10 07:24:47', '2020-06-09 18:54:47'),
(56, 'wmzkj', 'bedsz', 'Skincare ', 1, '2020-06-10 07:24:57', '2020-06-09 18:54:57'),
(57, 'uzbqa', 'bedsz', 'Beauty Sets', 1, '2020-06-10 07:25:10', '2020-06-09 18:55:10'),
(58, 'xzpdg', 'bedsz', 'Candles', 1, '2020-06-10 07:25:19', '2020-06-09 18:55:19'),
(59, 'phbvo', 'bedsz', 'Cosmetic Cases', 1, '2020-06-10 07:25:30', '2020-06-09 18:55:30'),
(60, 'gwtef', 'bedsz', 'Handcare', 1, '2020-06-10 07:25:47', '2020-06-09 18:55:47'),
(61, 'srgfy', 'bedsz', 'Makeup', 1, '2020-06-10 07:25:55', '2020-06-09 18:55:55'),
(62, 'pkgix', 'bedsz', 'Tools and Devices', 1, '2020-06-10 07:26:07', '2020-06-09 18:56:07'),
(63, 'dklgu', 'zisqv', 'Fine Watches', 1, '2020-06-10 07:44:40', '2020-06-09 19:14:40'),
(64, 'ctklh', 'zisqv', 'Earrings', 1, '2020-06-10 07:45:52', '2020-06-09 19:15:52'),
(65, 'wvbly', 'zisqv', 'Necklaces', 1, '2020-06-10 07:46:05', '2020-06-09 19:16:05'),
(66, 'lpvuo', 'zisqv', 'Bracelets', 1, '2020-06-10 07:46:16', '2020-06-09 19:16:16'),
(67, 'whznl', 'zisqv', 'Rings', 1, '2020-06-10 07:46:25', '2020-06-09 19:16:25'),
(68, 'ahyxc', 'saqem', 'Blazers', 1, '2020-06-10 07:51:35', '2020-06-09 19:21:35'),
(69, 'gjvqf', 'saqem', 'Coats', 1, '2020-06-10 07:51:48', '2020-06-09 19:21:48'),
(70, 'vktjn', 'saqem', 'Dresses', 1, '2020-06-10 07:51:58', '2020-06-09 19:21:58'),
(71, 'pneht', 'saqem', 'Gowns', 1, '2020-06-10 07:52:07', '2020-06-09 19:22:07'),
(72, 'wiflq', 'saqem', 'Jackets', 1, '2020-06-10 07:52:20', '2020-06-09 19:22:20'),
(73, 'iolzh', 'saqem', 'Jeans', 1, '2020-06-10 07:52:29', '2020-06-09 19:22:29'),
(74, 'lcxrf', 'saqem', 'Knitwear', 1, '2020-06-10 07:52:40', '2020-06-09 19:22:40'),
(75, 'etcmz', 'saqem', 'Lingerie', 1, '2020-06-10 07:52:49', '2020-06-09 19:22:49'),
(76, 'lpten', 'saqem', 'Nightwear', 1, '2020-06-10 07:52:58', '2020-06-09 19:22:58'),
(77, 'wifle', 'saqem', 'Skirts', 1, '2020-06-10 07:53:10', '2020-06-09 19:23:10'),
(78, 'cmzha', 'saqem', 'Sportswear', 1, '2020-06-10 07:53:18', '2020-06-09 19:23:18'),
(79, 'txmvd', 'saqem', 'Swimwear', 1, '2020-06-10 07:53:27', '2020-06-09 19:23:27'),
(80, 'ltpsw', 'saqem', 'Tops', 1, '2020-06-10 07:53:36', '2020-06-09 19:23:36'),
(81, 'mdrvj', 'saqem', 'Trousers', 1, '2020-06-10 07:53:45', '2020-06-09 19:23:45'),
(82, 'goyec', 'saqem', 'Polo Shirts', 1, '2020-06-10 07:53:59', '2020-06-09 19:23:59'),
(83, 'xplro', 'saqem', 'Shirts', 1, '2020-06-10 07:54:08', '2020-06-09 19:24:08'),
(84, 'nkbhl', 'saqem', 'Suits', 1, '2020-06-10 07:54:17', '2020-06-09 19:24:17'),
(85, 'ausei', 'saqem', 'Sweatshirts', 1, '2020-06-10 07:54:28', '2020-06-09 19:24:28'),
(86, 'ygtuj', 'saqem', 'T-Shirts', 1, '2020-06-10 07:54:39', '2020-06-09 19:24:39'),
(87, 'fgmdy', 'saqem', 'Underwear', 1, '2020-06-10 07:54:48', '2020-06-09 19:24:48'),
(88, 'ljcnx', 'lcgpa', 'Belts', 1, '2020-06-10 07:55:51', '2020-06-09 19:25:51'),
(89, 'vqosu', 'lcgpa', 'Hats', 1, '2020-06-10 07:55:59', '2020-06-09 19:25:59'),
(90, 'unqml', 'lcgpa', 'Luggage', 0, '2020-06-09 19:26:40', '2020-06-09 19:26:40'),
(91, 'bnxkh', 'lcgpa', 'Sunglasses', 1, '2020-06-10 07:56:23', '2020-06-09 19:26:23'),
(92, 'fmula', 'lcgpa', 'Wallets & Purses', 1, '2020-06-10 07:56:48', '2020-06-09 19:26:48'),
(93, 'pcyzr', 'lcgpa', 'Ties', 1, '2020-06-10 07:56:59', '2020-06-09 19:26:59'),
(94, 'dmfcj', 'lcgpa', 'Cufflinks', 1, '2020-06-10 07:57:12', '2020-06-09 19:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `submenu_id` int(255) NOT NULL,
  `menu` int(255) NOT NULL,
  `submenu` varchar(100) NOT NULL,
  `subcategory` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`submenu_id`, `menu`, `submenu`, `subcategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Backpacks', 'rihfu', 1, '2020-06-10 06:50:09', '2020-06-09 18:20:09'),
(2, 1, 'Belt Bags', 'kxryd', 1, '2020-06-10 06:50:32', '2020-06-09 18:20:32'),
(3, 1, 'Bucket Bags', 'ihqtv', 1, '2020-06-10 06:50:43', '2020-06-09 18:20:43'),
(4, 1, 'Clutch Bags', 'treia', 1, '2020-06-10 06:50:56', '2020-06-09 18:20:56'),
(5, 1, 'Cross Body Bags', 'mdokh', 1, '2020-06-10 06:51:17', '2020-06-09 18:21:17'),
(6, 1, 'Evening Bags', 'exhwn', 1, '2020-06-10 06:51:36', '2020-06-09 18:21:36'),
(7, 1, 'Luggage and Travel', 'puqjh', 1, '2020-06-10 06:51:50', '2020-06-09 18:21:50'),
(8, 1, 'Shoulder Bags', 'esowr', 1, '2020-06-10 06:52:15', '2020-06-09 18:22:15'),
(9, 1, 'Tote Bags', 'xrlas', 1, '2020-06-10 06:52:39', '2020-06-09 18:22:39'),
(10, 2, 'Ankle Boots', 'draim', 1, '2020-06-10 06:53:07', '2020-06-09 18:23:07'),
(11, 2, 'Boots', 'brnpl', 1, '2020-06-10 06:53:21', '2020-06-09 18:23:21'),
(12, 2, 'Espadrilles', 'dgzfq', 1, '2020-06-10 06:53:35', '2020-06-09 18:23:35'),
(13, 2, 'Evening Shoes', 'tfkum', 1, '2020-06-10 06:53:49', '2020-06-09 18:23:49'),
(14, 2, 'Flat Shoes', 'mwbki', 1, '2020-06-10 06:54:07', '2020-06-09 18:24:07'),
(15, 2, 'Mules', 'erblu', 1, '2020-06-10 06:54:24', '2020-06-09 18:24:24'),
(16, 2, 'Pumps', 'hoplq', 1, '2020-06-10 06:54:45', '2020-06-09 18:24:45'),
(17, 2, 'Sandals', 'gmyjl', 1, '2020-06-10 06:55:02', '2020-06-09 18:25:02'),
(18, 2, 'Sneakers', 'rewil', 1, '2020-06-10 06:55:19', '2020-06-09 18:25:19'),
(19, 2, 'Boat Shoes', 'qldej', 1, '2020-06-10 06:55:46', '2020-06-09 18:25:46'),
(20, 2, 'Brogues', 'qrckp', 1, '2020-06-10 06:56:03', '2020-06-09 18:26:03'),
(21, 2, 'Chelsea Boots', 'mgqaj', 1, '2020-06-10 06:56:25', '2020-06-09 18:26:25'),
(22, 2, 'Derby Shoes', 'wlpdo', 1, '2020-06-10 06:56:50', '2020-06-09 18:26:50'),
(23, 2, 'Driving Shoes', 'ikvzp', 1, '2020-06-10 06:57:20', '2020-06-09 18:27:20'),
(24, 2, 'House Shoes', 'elvku', 1, '2020-06-10 06:57:36', '2020-06-09 18:27:36'),
(25, 2, 'Lace-Up Boots', 'ybmkr', 1, '2020-06-10 06:57:54', '2020-06-09 18:27:54'),
(26, 2, 'Loafers', 'tyfcl', 1, '2020-06-10 06:58:14', '2020-06-09 18:28:14'),
(27, 2, 'Monk Strap Shoes', 'cseyg', 1, '2020-06-10 06:58:33', '2020-06-09 18:28:33'),
(28, 2, 'Oxford Shoes', 'hzvim', 1, '2020-06-10 06:58:50', '2020-06-09 18:28:50'),
(29, 2, 'Slides ', 'ytzix', 1, '2020-06-10 06:59:11', '2020-06-09 18:29:11'),
(30, 2, 'Slippers', 'krhib', 1, '2020-06-10 06:59:43', '2020-06-09 18:29:43'),
(31, 2, 'Summer Shoes ', 'alptd', 1, '2020-06-10 07:00:27', '2020-06-09 18:30:27'),
(32, 2, 'Hiking Boots', 'ncotb', 1, '2020-06-10 07:00:39', '2020-06-09 18:30:39'),
(33, 3, 'Laptop', 'ckmvp', 1, '2020-06-10 07:05:26', '2020-06-09 18:35:26'),
(34, 3, 'iPhone', 'syodq', 1, '2020-06-10 07:05:38', '2020-06-09 18:35:38'),
(35, 3, 'iPad', 'cjwrx', 1, '2020-06-10 07:05:49', '2020-06-09 18:35:49'),
(36, 3, 'Accessories', 'pmwzx', 1, '2020-06-10 07:06:03', '2020-06-09 18:36:03'),
(37, 4, 'Rompers & Sleepsuits', 'qlvxy', 1, '2020-06-10 07:19:48', '2020-06-09 18:49:48'),
(38, 4, 'Dresses', 'ikgof', 1, '2020-06-10 07:20:01', '2020-06-09 18:50:01'),
(39, 4, 'Tops', 'fmtvc', 1, '2020-06-10 07:20:14', '2020-06-09 18:50:14'),
(40, 4, 'Coats', 'gljqc', 1, '2020-06-10 07:20:24', '2020-06-09 18:50:24'),
(41, 4, 'Shoes', 'ajinq', 1, '2020-06-10 07:20:46', '2020-06-09 18:50:46'),
(42, 4, 'T-Shirts', 'cohkq', 1, '2020-06-10 07:21:03', '2020-06-09 18:51:03'),
(43, 4, 'Trousers', 'hceub', 1, '2020-06-10 07:21:26', '2020-06-09 18:51:26'),
(44, 4, 'Knitwear', 'xfqbo', 1, '2020-06-10 07:21:38', '2020-06-09 18:51:38'),
(45, 5, 'Bath and Body', 'qpfel', 1, '2020-06-10 07:33:00', '2020-06-09 19:03:00'),
(46, 5, 'Fragrance ', 'gfqra', 1, '2020-06-10 07:33:11', '2020-06-09 19:03:11'),
(47, 5, 'Haircare Sets ', 'dkoqc', 1, '2020-06-10 07:33:28', '2020-06-09 19:03:28'),
(48, 5, 'Shave', 'voefn', 1, '2020-06-10 07:33:56', '2020-06-09 19:03:56'),
(49, 5, 'Skincare', 'wmzkj', 1, '2020-06-10 07:34:09', '2020-06-09 19:04:09'),
(50, 5, 'Beauty Sets', 'uzbqa', 1, '2020-06-10 07:34:24', '2020-06-09 19:04:24'),
(51, 5, 'Candles', 'xzpdg', 1, '2020-06-10 07:34:35', '2020-06-09 19:04:35'),
(52, 5, 'Cosmetic Cases', 'phbvo', 1, '2020-06-10 07:34:47', '2020-06-09 19:04:47'),
(53, 5, 'Handcare', 'gwtef', 1, '2020-06-10 07:35:01', '2020-06-09 19:05:01'),
(54, 5, 'Makeup', 'srgfy', 1, '2020-06-10 07:35:18', '2020-06-09 19:05:18'),
(55, 5, 'Tools and Devices', 'pkgix', 1, '2020-06-10 07:35:35', '2020-06-09 19:05:35'),
(56, 6, 'Earrings', 'ctklh', 1, '2020-06-10 07:47:44', '2020-06-09 19:17:44'),
(57, 6, 'Necklaces', 'wvbly', 1, '2020-06-10 07:47:58', '2020-06-09 19:17:58'),
(58, 6, 'Bracelets', 'lpvuo', 1, '2020-06-10 07:48:38', '2020-06-09 19:18:38'),
(59, 6, 'Rings', 'whznl', 1, '2020-06-10 07:48:57', '2020-06-09 19:18:57'),
(60, 6, 'Fine Watches', 'dklgu', 1, '2020-06-10 07:49:12', '2020-06-09 19:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_cart`
--

CREATE TABLE `user_cart` (
  `uc_id` int(255) NOT NULL,
  `uc_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_cart`
--

INSERT INTO `user_cart` (`uc_id`, `uc_key`, `user`, `product`, `color`, `size`, `qty`, `created_at`, `updated_at`) VALUES
(22, 'lzcey', 'xhqgm', 'MOON-T-001', 'GREEN', '6', 2, '2020-09-02 12:49:18', '2020-09-02 12:49:18');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `w_id` int(255) NOT NULL,
  `w_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`w_id`, `w_key`, `user`, `product`, `color`, `size`, `qty`, `created_at`, `updated_at`) VALUES
(1, 'atxmf', 'jqrue', 'gdtfb', 'BROWN', '', '0', '2020-06-11 23:24:12', '2020-06-11 10:54:12'),
(2, 'hbdgf', 'jqrue', 'kmvta', 'BLACK', '40', '0', '2020-06-12 01:47:24', '2020-06-11 13:17:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`ab_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`au_id`);

--
-- Indexes for table `apple`
--
ALTER TABLE `apple`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `assigned_attributes`
--
ALTER TABLE `assigned_attributes`
  ADD PRIMARY KEY (`aaid`);

--
-- Indexes for table `attribute_dd`
--
ALTER TABLE `attribute_dd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  ADD PRIMARY KEY (`at_id`);

--
-- Indexes for table `berluti`
--
ALTER TABLE `berluti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `brand_profile`
--
ALTER TABLE `brand_profile`
  ADD PRIMARY KEY (`bf_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `creed`
--
ALTER TABLE `creed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`deg_id`);

--
-- Indexes for table `fit`
--
ALTER TABLE `fit`
  ADD PRIMARY KEY (`fit_id`);

--
-- Indexes for table `home_section`
--
ALTER TABLE `home_section`
  ADD PRIMARY KEY (`hs_id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `orders_status`
--
ALTER TABLE `orders_status`
  ADD PRIMARY KEY (`os_id`);

--
-- Indexes for table `order_book`
--
ALTER TABLE `order_book`
  ADD PRIMARY KEY (`ob_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `sku` (`sku`),
  ADD KEY `product_name` (`product_name`(768)),
  ADD KEY `brand` (`brand`),
  ADD KEY `category` (`category`),
  ADD KEY `subcategory` (`subcategory`),
  ADD KEY `mrp` (`mrp`),
  ADD KEY `price` (`price`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`pa_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `shop_by_cat`
--
ALTER TABLE `shop_by_cat`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`),
  ADD KEY `store_mobile` (`mobile`),
  ADD KEY `store` (`store_key`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`submenu_id`);

--
-- Indexes for table `user_cart`
--
ALTER TABLE `user_cart`
  ADD PRIMARY KEY (`uc_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`w_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address_book`
--
ALTER TABLE `address_book`
  MODIFY `ab_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `au_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `apple`
--
ALTER TABLE `apple`
  MODIFY `aid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assigned_attributes`
--
ALTER TABLE `assigned_attributes`
  MODIFY `aaid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `attribute_dd`
--
ALTER TABLE `attribute_dd`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  MODIFY `at_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `berluti`
--
ALTER TABLE `berluti`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  MODIFY `ba_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `brand_profile`
--
ALTER TABLE `brand_profile`
  MODIFY `bf_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `creed`
--
ALTER TABLE `creed`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `deg_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fit`
--
ALTER TABLE `fit`
  MODIFY `fit_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_section`
--
ALTER TABLE `home_section`
  MODIFY `hs_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `slider_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders_status`
--
ALTER TABLE `orders_status`
  MODIFY `os_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_book`
--
ALTER TABLE `order_book`
  MODIFY `ob_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `p_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `pa_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `pi_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `ps_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `shop_by_cat`
--
ALTER TABLE `shop_by_cat`
  MODIFY `sb_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `subcat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `submenu_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `user_cart`
--
ALTER TABLE `user_cart`
  MODIFY `uc_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `w_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
