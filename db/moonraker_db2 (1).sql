-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2020 at 11:44 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moonraker_db2`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

CREATE TABLE `address_book` (
  `ab_id` int(255) NOT NULL,
  `ab_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `town` varchar(200) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` int(10) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address_book`
--

INSERT INTO `address_book` (`ab_id`, `ab_key`, `user`, `title`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rkxyt', 'xhqgm', 'Mr', 'Anand', 'Verma', '09718190486', 'A-41, Ram Vihar, Loni ', 'Ghaziabad', 'Uttar Pradesh', 201102, 1, '2020-06-11 01:57:19', '2020-06-10 13:27:20'),
(2, 'ghvej', 'jqrue', 'Mr', 'Manreet', 'Rathore', '09999910363', 'Plot No 10 Rohini Sector 10 ', 'Delhi', 'Delhi', 110085, 1, '2020-06-11 03:11:16', '2020-06-10 14:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `au_id` int(255) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `token` text DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `deg` int(20) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`au_id`, `username`, `password`, `token`, `name`, `email`, `mobile`, `deg`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'e1b4755403710e0deb7aa5d45e43996d', 'rwvxiqchakzotsejbupdlmygnf', NULL, NULL, NULL, NULL, 1, '2020-09-08 05:52:52', '2020-09-08 05:52:52'),
(2, 'anand', 'e1b4755403710e0deb7aa5d45e43996d', 'klovryqhxfejcgstbuwmpdazni', NULL, NULL, NULL, NULL, 1, '2020-08-31 09:12:46', '2020-08-31 09:12:46'),
(3, 'shikha', 'e1b4755403710e0deb7aa5d45e43996d', 'qvowygezailurdmfpcsxthnbkj', NULL, NULL, NULL, NULL, 1, '2020-09-04 04:37:32', '2020-09-04 04:37:32'),
(4, 'manreetr', 'admin@123#', '0', 'Manreet', 'manreetrathore22@gmail.com', '9999910363', 1, 1, '2020-08-17 07:23:17', '2020-08-17 07:23:17');

-- --------------------------------------------------------

--
-- Table structure for table `apple`
--

CREATE TABLE `apple` (
  `aid` int(255) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `npi` varchar(50) DEFAULT NULL,
  `marketing_flag` varchar(50) DEFAULT NULL,
  `upc_ean` varchar(50) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `item_group` varchar(50) DEFAULT NULL,
  `subproduct_group` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apple`
--

INSERT INTO `apple` (`aid`, `p_id`, `npi`, `marketing_flag`, `upc_ean`, `barcode`, `item_group`, `subproduct_group`, `manufacturer`, `created_at`, `updated_at`) VALUES
(1, 'MOON00103fghf', 'New', 'dh', 'hkk', 'fsfweefewf', 'dfgdfg', 'lkjlk', 'jkl', '2020-08-24 12:27:08', '2020-08-25 06:29:22'),
(2, 'MOON-LB-002sdfsdf', 'New', 'sdfdf', 'ddfsd', 'dsfds', 'sdfsdf', 'sdfsdf', 'sdffdsf', '2020-08-25 07:28:40', '2020-08-25 07:28:41'),
(4, 'MWHD2HN/A', 'New', 'iPhone 11 Max', '190199381032', '', '', '', 'Apple', '2020-08-25 21:32:26', '2020-08-25 09:02:26'),
(5, 'MVH22HN/A', 'New', 'Macbook Air', '190199244498', '', '', '', 'Apple', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(6, 'Mac0001', 'New', 'MacBook Pro', '190199368262', '', '', '', '', '2020-08-27 18:06:04', '2020-08-27 05:36:04'),
(7, 'GRU0022', 'New', '', '', '', '', '', '', '2020-08-31 08:39:30', '2020-08-30 20:09:30'),
(8, 'PR0021', 'New', '', '', '', '', '', '', '2020-08-31 08:47:41', '2020-08-30 20:17:41'),
(9, 'PR0022', 'New', '', 'PR0022', '', '', 'PR0022', '', '2020-08-31 08:49:36', '2020-08-30 20:19:36'),
(10, 'PR0023', 'New', '', 'PR0022', '', '', 'PR0022', '', '2020-08-31 09:05:04', '2020-08-30 20:35:04'),
(11, 'GRU0023', 'New', '', '', '', '', '', '', '2020-08-31 09:05:25', '2020-08-30 20:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_tbl`
--

CREATE TABLE `attribute_tbl` (
  `at_id` int(255) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute_tbl`
--

INSERT INTO `attribute_tbl` (`at_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'COLOR', NULL, '2020-07-01 05:48:04', '2020-07-01 05:48:39'),
(2, 'SIZE', NULL, '2020-07-01 05:48:04', '2020-07-01 05:48:39'),
(4, 'STORAGE', NULL, NULL, '2020-08-25 08:41:08'),
(6, 'GENDER', NULL, '2020-07-01 06:36:56', '2020-07-01 06:37:08'),
(7, 'DISPLAY', NULL, NULL, '2020-08-26 06:08:49'),
(8, 'DISK', NULL, NULL, '2020-08-26 06:08:49'),
(9, 'RAM', NULL, NULL, '2020-08-26 06:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `berluti`
--

CREATE TABLE `berluti` (
  `id` int(255) NOT NULL,
  `p_id` varchar(20) NOT NULL,
  `collection_tag` varchar(10) DEFAULT NULL,
  `line_of_business` varchar(50) DEFAULT NULL,
  `vpn` varchar(50) DEFAULT NULL,
  `style_name` varchar(50) DEFAULT NULL,
  `style_no` varchar(50) DEFAULT NULL,
  `serial_no` varchar(50) DEFAULT NULL,
  `color_code` varchar(50) DEFAULT NULL,
  `material_code` varchar(50) DEFAULT NULL,
  `fit` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `hsncode` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `berluti`
--

INSERT INTO `berluti` (`id`, `p_id`, `collection_tag`, `line_of_business`, `vpn`, `style_name`, `style_no`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `hsncode`, `created_at`, `updated_at`) VALUES
(1, '1105032', 'PE', '', '1105032', '50ml Millesime Green Irish Tweed', '1105032', '1105032', 'A3217Q01', '', '', '', NULL, '2020-08-27 23:16:54', '2020-08-27 10:46:54'),
(2, '1105036', 'PE', '', '1105036', '50ml Millesime Royal Water', '1105036', '1105036', '', '', '', '', NULL, '2020-08-27 23:30:57', '2020-08-27 11:19:36'),
(3, '1105039', 'PE', '', '1105039', ' Millesime Himalaya', '1105039', '1105039', '', '', '', '', NULL, '2020-08-28 00:03:34', '2020-08-27 11:33:34'),
(4, '1105040', 'PE', '', '1105040', ' 50ml Millesime Original Vetiver', '1105040', '1105040', '', '', '', '', NULL, '2020-08-28 00:22:17', '2020-08-27 11:52:17'),
(5, '1105041', 'PE', '', '1105041', 'Millesime Original Santal 50ml', '1105041', '1105041', 'CM4117T01', '', '', '', NULL, '2020-08-28 00:42:37', '2020-08-27 12:12:37'),
(6, '1320032', 'PE', '', '1320032', 'Candle Ambiance Green Irish Tweed  200g', '', '1320032', '', '', '', '', NULL, '2020-08-28 01:14:37', '2020-08-27 12:44:37'),
(7, 'SK001', 'PE', '', 'SK001', 'leather pouch', '', 'SK001', '', '', '', '', NULL, '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(8, 'PR001', 'PE', '', 'PR001', 'tote bag', '', 'PR001', '', '', '', '', NULL, '2020-08-31 02:13:31', '2020-08-30 13:43:31'),
(9, 'GRU001', 'PE', '', 'GRU001', 'puffer shoulder bag', '', 'GRU001', '', '', '', '', NULL, '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(10, 'GRU002', 'PE', '', 'GRU002', 'suede driving shoes', '', 'GRU002', '', '', '', '', NULL, '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(11, 'PR002', 'PE', '', 'PR002', 'hiking boots', '', 'PR002', '', '', '', '', NULL, '2020-08-31 03:02:23', '2020-08-30 14:32:23'),
(12, 'DIM002', 'PE', '', 'DIM002', 'cotton-blend shirt', '', 'DIM002', '', '', '', '', NULL, '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(13, 'DIM003', 'PE', '', 'DIM003', 'Bomber Jacket', '', 'DIM003', '', '', '', '', NULL, '2020-08-31 04:24:26', '2020-08-30 15:54:26'),
(14, 'GRU003', 'PE', '', 'GRU003', 'Wool Trousers', '', 'GRU003', '', '', '', '', NULL, '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(15, 'GRU004', 'PE', '', 'GRU004', 'Wool trousers', '', 'GRU004', '', '', '', '', NULL, '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(16, 'DIM004', 'PE', '', 'DIM004', 'Bomber Jacket', '', 'DIM004', '', '', '', '', NULL, '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(17, 'DIM005', 'PE', '', 'DIM005', 'Bomber Jacket', '', 'DIM005', '', '', '', '', NULL, '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(18, 'DIM006', 'PE', '', 'DIM006', ' Leather Sneakers', '', 'DIM006', '', '', '', '', NULL, '2020-08-31 04:41:21', '2020-08-30 16:11:21'),
(19, 'DIM007', 'PE', '', 'DIM007', 'Leather Sneakers', '', 'DIM007', '', '', '', '', NULL, '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(20, 'PR003', 'PE', '', 'PR003', 'Selvedge Denim Jeans', '', 'PR003', '', '', '', '', NULL, '2020-08-31 05:12:21', '2020-08-30 16:42:21'),
(21, 'PR004', 'PE', '', 'PR004', 'Denim Jeans', '', 'PR004', '', '', '', '', NULL, '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(22, 'GRU005', 'PE', '', 'GRU005', 'Eau De Parfum', '', 'GRU005', '', '', '', '', NULL, '2020-08-31 05:49:27', '2020-08-30 17:19:27'),
(23, 'PR006', 'PE', '', 'PR005', 'Jersey Polo Shirt', '', 'PR005', '', '', '', '', NULL, '2020-08-31 05:58:37', '2020-08-30 17:28:37'),
(24, 'GRUOO6', 'PE', '', 'GRU006', 'eau de parfum splash', '', 'GRU006', '', '', '', '', NULL, '2020-08-31 06:00:56', '2020-08-30 17:30:56'),
(25, 'PR005', 'PE', '', 'PR005', 'Twill Shirt', '', 'PR005', '', '', '', '', NULL, '2020-08-31 06:13:23', '2020-08-30 17:43:23'),
(26, 'GRU007', 'PE', '', 'GRU007', 'gold tone', '', 'GRU007', '', '', '', '', NULL, '2020-08-31 06:13:38', '2020-08-30 17:43:38'),
(27, 'GRU008', 'PE', '', 'GRU008', ' Eau de Parfum', '', 'GRU008', '', '', '', '', NULL, '2020-08-31 06:18:51', '2020-08-30 17:48:51'),
(28, 'GRU009', 'PE', '', 'GRU009', 'eau de parfum', '', 'GRU009', '', '', '', '', NULL, '2020-08-31 06:21:40', '2020-08-30 17:51:40'),
(29, 'GRU0010', 'PE', '', 'GRU0010', 'eau de parfum', '', 'GRU0010', '', '', '', '', NULL, '2020-08-31 06:23:18', '2020-08-30 17:53:18'),
(30, 'PR007', 'PE', '', 'PR007', 'Blend Suit Jacket', '', 'PR007', '', '', '', '', NULL, '2020-08-31 06:31:37', '2020-08-30 18:01:37'),
(31, 'PR008', 'PE', '', 'PR008', 'Silk-Blend Suit Jacket', '', 'PR008', '', '', '', '', NULL, '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(32, 'PR009', 'PE', '', 'PR009', 'Silk-Blend Suit Jacket', '', 'PR009', '', '', '', '', NULL, '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(33, 'GRU0011', 'PE', '', 'GRU0011', 'leather atomiser', '', 'GRU0011', '', '', '', '', NULL, '2020-08-31 06:35:03', '2020-08-30 18:05:03'),
(34, 'PR0010', 'PE', '', 'PR0010', 'Silk-Blend Suit Jacket', '', 'PR0010', '', '', '', '', NULL, '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(35, 'PR0011', 'PE', '', 'PR0011', 'Silk-Blend Suit Jacket', '', 'PR0011', '', '', '', '', NULL, '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(36, 'PR0012', 'PE', '', 'PR0012', 'MÃ©lange Virgin Wool Tie', '', 'PR0012', '', '', '', '', NULL, '2020-08-31 06:47:29', '2020-08-30 18:17:29'),
(37, 'GRU0012', 'PE', '', 'GRU0012', 'leather tote bag', '', 'GRU0012', '', '', '', '', NULL, '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(38, 'PR0013', 'PE', '', 'PR0013', 'Leather Carry-On Suitcase', '', 'PR0013', '', '', '', '', NULL, '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(39, 'GRU0013', 'PE', '', 'GRU0013', 'Bucket Bag', '', 'GRU0013', '', '', '', '', NULL, '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(40, 'PR0015', 'PE', '', 'PR0015', 'Grain Leather Billfold Wallet', '', 'PR0015', '', '', '', '', NULL, '2020-08-31 07:19:02', '2020-08-30 18:49:02'),
(41, 'GRU0015', 'PE', '', 'GRU0015', 'Belt Bag', '', 'GRU0015', '', '', '', '', NULL, '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(42, 'GRU0016', 'PE', '', 'GRU0016', 'wool-blend coat', '', 'GRU0016', '', '', '', '', NULL, '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(43, 'PR0016', 'PE', '', 'PR0016', 'Corduroy Trousers', '', 'PR0016', '', '', '', '', NULL, '2020-08-31 07:32:00', '2020-08-30 19:02:00'),
(44, 'PR0017', 'PE', '', 'PR0017', 'Corduroy Trousers', '', 'PR0017', '', '', '', '', NULL, '2020-08-31 07:36:53', '2020-08-30 19:06:53'),
(45, 'GRU0017', 'PE', '', 'GRU0017', 'wool-blend coat', '', 'GRU0017', '', '', '', '', NULL, '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(46, 'GRU0018', 'PE', '', 'GRU0018', 'cotton-jersey T-shirt', '', 'GRU0018', '', '', '', '', NULL, '2020-08-31 07:54:19', '2020-08-30 19:24:19'),
(47, 'PR0018', 'PE', '', 'PR0018', 'Full-Grain Leather Backpack', '', 'PR0018', '', '', '', '', NULL, '2020-08-31 07:56:46', '2020-08-30 19:26:46'),
(48, 'GRU0019', 'PE', '', 'GRU0019', 'cotton-jersey T-shirt', '', 'GRU0019', '', '', '', '', NULL, '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(49, 'PR0019', 'PE', '', 'PR0019', 'Herringbone Coat', '', 'PR0019', '', '', '', '', NULL, '2020-08-31 08:08:47', '2020-08-30 19:38:47'),
(50, 'GRU0020', 'PE', '', 'GRU0020', 'Naima square-toe ', '', 'GRU0020', '', '', '', '', NULL, '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(51, 'GRU0021', 'PE', '', 'GRU0021', 'Naima square-toe ', '', 'GRU0021', '', '', '', '', NULL, '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(52, 'PR0020', 'PE', '', 'PR0020', 'Herringbone Coat', '', 'PR0020', '', '', '', '', NULL, '2020-08-31 08:16:27', '2020-08-30 19:46:27'),
(53, 'GRU0024', 'PE', '', 'GRU0024', 'square-frame sunglasses', '', 'GRU0024', '', '', '', '', NULL, '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(54, 'PR0024', 'PE', '', 'PR0024', 'Palladium-Plated Cufflinks', '', 'PR0024', '', '', '', '', NULL, '2020-08-31 09:25:47', '2020-08-30 20:55:47'),
(55, 'PR0025', 'PE', '', 'PR0025', 'plaque leather bracelet', '', 'PR0025', '', '', '', '', NULL, '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(56, 'DIM008', 'PE', '', 'DIM008', 'pendant necklace', '', 'DIM008', '', '', '', '', NULL, '2020-08-31 18:20:25', '2020-08-31 05:50:25'),
(57, 'DIM009', 'PE', '', 'DIM009', 'jacquard mini dress', '', 'DIM009', '', '', '', '', NULL, '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(58, 'DIM10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-31 23:57:07', '2020-08-31 11:27:07'),
(59, 'DIM11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(60, 'DIM12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(61, 'DIM0013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(62, 'DIM14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(63, 'DIM15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 01:21:28', '2020-08-31 12:51:28'),
(64, 'DMI17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 01:28:18', '2020-08-31 12:58:18'),
(65, 'PR0026', 'PE', '', 'PR0026', 'square-frame sunglasses', '', 'PR0026', '', '', '', '', NULL, '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(66, 'GRU0025', 'PE', '', 'GRU0025', 'leather belt', '', 'GRU0025', '', '', '', '', NULL, '2020-09-01 06:07:39', '2020-08-31 17:37:39'),
(67, 'GRU0026', 'PE', '', 'GRU0026', ' billfold wallet', '', 'GRU0026', '', '', '', '', NULL, '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(68, 'PR0027', 'PE', '', 'PR0027', ' suede tote bag', '', 'PR0027', '', '', '', '', NULL, '2020-09-01 06:46:35', '2020-09-01 18:38:46'),
(69, 'GRU0027', 'PE', '', 'GRU0027', 'money clip wallet', '', 'GRU0027', '', '', '', '', NULL, '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(70, 'PR0028', 'PE', '', 'PR0028', 'frame optical glasses', '', 'PR0028', '', '', '', '', NULL, '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(71, 'GRU0028', 'PE', '', 'GRU0028', 'Silk-satin tie', '', 'GRU0028', '', '', '', '', NULL, '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(72, 'GRU0029', 'PE', '', 'GRU0029', ' cotton-pique polo shirt', '', 'GRU0029', '', '', '', '', NULL, '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(73, 'PR0029', 'PE', '', 'PR0029', 'frame acetate sunglasses', '', 'PR0029', '', '', '', '', NULL, '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(74, 'GRU0030', 'PE', '', 'GRU0030', 'cotton-jersey T-shirt', '', 'GRU0030', '', '', '', '', NULL, '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(75, 'PR0030', 'PE', '', 'PR0030', 'square glasses', '', 'PR0030', '', '', '', '', NULL, '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(76, 'GRU0031', 'PE', '', 'GRU0031', 'silk-satin tie', '', 'GRU0031', '', '', '', '', NULL, '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(77, 'PR0031', 'PE', '', 'PR0031', ' square-frame sunglasses', '', 'PR0031', '', '', '', '', NULL, '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(78, 'GRU0032', 'PE', '', 'GRU0032', 'shell-down jacket', '', 'GRU0032', '', '', '', '', NULL, '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(79, 'PR0032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 08:02:37', '2020-09-01 18:28:38'),
(80, 'GRU0033', 'PE', '', 'GRU0033', ' cotton-piquÃ© polo shirt', '', 'GRU0033', '', '', '', '', NULL, '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(81, 'PR0033', 'PE', '', 'PR0033', 'frame sunglasses', '', 'PR0033', '', '', '', '', NULL, '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(82, 'PR0034', 'PE', '', 'PR0034', 'round glasses', '', 'PR0034', '', '', '', '', NULL, '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(83, 'GRU0034', 'PE', '', 'GRU0034', ' leather wallet', '', 'GRU0034', '', '', '', '', NULL, '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(84, 'PR0035', 'PE', '', 'PR0035', 'metal glasses', '', 'PR0035', '', '', '', '', NULL, '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(85, 'GRU0035', 'PE', '', 'GRU0035', 'leather backpack', '', 'GRU0035', '', '', '', '', NULL, '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(86, 'PR0036', 'PE', '', 'PR0036', 'leather platform sandals', '', 'PR0036', '', '', '', '', NULL, '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(87, 'PR0037', 'PE', '', 'PR0037', 'leather wedge-heeled sandals', '', 'PR0037', '', '', '', '', NULL, '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(88, 'PR0038', 'PE', '', 'PR0038', ' patent leather courts', '', 'PR0038', '', '', '', '', NULL, '2020-09-01 20:48:54', '2020-09-01 08:18:54'),
(89, 'PR0039', 'PE', '', 'PR0039', 'leather sandal', '', 'PR0039', '', '', '', '', NULL, '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(90, 'PR0040', 'PE', '', 'PR0040', 'leather heeled sandals', '', 'PR0040', '', '', '', '', NULL, '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(91, 'GRU0036', 'PE', '', 'GRU0036', 'silk-satin tie', '', 'GRU0036', '', '', '', '', NULL, '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(92, 'GRU0037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 21:44:41', '2020-09-01 09:23:39'),
(93, 'GRU0038', 'PE', '', 'GRU0038', 'cotton shirt', '', 'GRU0038', '', '', '', '', NULL, '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(94, 'GRU0039', 'PE', '', 'GRU0039', 'cotton shirt', '', 'GRU0039', '', '', '', '', NULL, '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(95, 'GRU0040', 'PE', '', 'GRU0040', 'wool and silk-blend trousers', '', 'GRU0040', '', '', '', '', NULL, '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(96, 'GRU0041', 'PE', '', 'GRU0041', 'wool and silk-blend trousers', '', 'GRU0041', '', '', '', '', NULL, '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(97, 'PR0041', 'PE', '', 'PR0041', 'metallic-leather sandals', '', 'PR0041', '', '', '', '', NULL, '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(98, 'PR0042', 'PE', '', 'PR0042', ' belt bag', '', 'PR0042', '', '', '', '', NULL, '2020-09-02 04:59:45', '2020-09-01 16:29:45'),
(99, 'PR0043', 'PE', '', 'PR0043', 'leather tote bag', '', 'PR0043', '', '', '', '', NULL, '2020-09-02 05:08:13', '2020-09-01 16:38:13'),
(100, 'PR0044', 'PE', '', 'PR0044', ' shoulder bag', '', 'PR0044', '', '', '', '', NULL, '2020-09-02 05:20:45', '2020-09-01 16:50:45'),
(101, 'GRU0042', 'PE', '', 'GRU0042', 'penny loafers', '', 'GRU0042', '', '', '', '', NULL, '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(102, 'PR0045', 'PE', '', 'PR0045', 'leather espadrilles', '', 'PR0045', '', '', '', '', NULL, '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(103, 'GRU0043', 'PE', '', 'GRU0043', ' driving shoes', '', 'GRU0043', '', '', '', '', NULL, '2020-09-02 06:05:47', '2020-09-01 17:35:47'),
(104, 'GRU0044', 'PE', '', 'GRU0044', 'driving shoes', '', 'GRU0044', '', '', '', '', NULL, '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(105, 'PR0046', 'PE', '', 'PR0046', ' espadrilles', '', 'PR0046', '', '', '', '', NULL, '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(106, 'GRU0045', 'PE', '', 'GRU0045', 'leather loafers', '', 'GRU0045', '', '', '', '', NULL, '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(107, 'GRU0046', 'PE', '', 'GRU0046', 'leather loafers', '', 'GRU0046', '', '', '', '', NULL, '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(108, 'PR0047', 'PE', '', 'PR0047', ' leather tote bag', '', 'PR0047', '', '', '', '', NULL, '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(109, 'PR0048', 'PE', '', 'PR0048', ' leather tote bag', '', 'PR0048', '', '', '', '', NULL, '2020-09-02 06:35:43', '2020-09-01 18:05:43'),
(110, 'PR0049', 'PE', '', 'PR0049', ' cotton tote bag', '', 'PR0049', '', '', '', '', NULL, '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(111, 'PR0050', 'PE', '', 'PR0050', 'leather tote bag', '', 'PR0050', '', '', '', '', NULL, '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(112, 'GRU0048', 'PE', '', 'GRU0048', 'esperadrille wedges', '', 'GRU0048', '', '', '', '', '', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(113, 'GRU0049', 'PE', '', 'GRU0049', 'leather platform loafers', '', 'GRU0049', '', '', '', '', '', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(114, 'GRU0050', 'PE', '', 'GRU0050', ' leather loafers', '', 'GRU0050', '', '', '', '', '', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(115, 'GRU0051', 'PE', '', 'GRU0051', 'leather loafers', '', 'GRU0051', '', '', '', '', NULL, '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(116, 'GRU0052', 'PE', '', 'GRU0052', 'rubber sliders', '', 'GRU0052', '', '', '', '', NULL, '2020-09-04 00:21:57', '2020-09-03 12:00:08'),
(117, 'GRU0053', 'PE', '', 'GRU0053', 'leather loafers', '', 'GRU0053', '', '', '', '', '', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(118, 'GRU0054', 'PE', '', 'GRU0054', 'leather loafers', '', 'GRU0054', '', '', '', '', NULL, '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(119, 'GRU0055', 'PE', '', 'GRU0055', 'ankle boots', '', 'GRU0055', '', '', '', '', '', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(120, 'GRU0056', 'PE', '', 'GRU0056', 'ankle boots', '', 'GRU0056', '', '', '', '', NULL, '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(121, 'GRU0057', 'PE', '', 'GRU0057', 'leather loafers', '', 'GRU0057', '', '', '', '', '', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(122, 'GRU0058', 'PE', '', 'GRU0058', 'leather loafers', '', 'GRU0058', '', '', '', '', NULL, '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(123, 'GRU0059', 'PE', '', 'GRU0059', ' Derby shoes', '', 'GRU0059', '', '', '', '', '', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(124, 'GRU0060', 'PE', '', 'GRU0060', '  Derby Shoe', '', 'GRU0060', '', '', '', '', NULL, '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(125, 'PR0051', 'PE', '', 'PR0051', 'metallic-leather sandals', '', 'PR0051', '', '', '', '', NULL, '2020-09-04 06:42:18', '2020-09-03 18:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `billing_addresses`
--

CREATE TABLE `billing_addresses` (
  `ba_id` int(255) NOT NULL,
  `ba_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `town` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(255) NOT NULL,
  `brand_key` varchar(10) NOT NULL,
  `store_key` varchar(10) NOT NULL,
  `brand_name` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_key`, `store_key`, `brand_name`, `status`, `created_at`, `updated_at`) VALUES
(26, 'wugsl', 'vrbsh', 'POLO', 0, '2020-08-18 06:37:59', '2020-08-18 06:37:59'),
(27, 'rydle', 'vrbsh', 'Saint Laurant', 0, '2020-08-18 06:36:58', '2020-08-18 06:36:58'),
(28, 'nxzhs', 'vrbsh', 'Brunello Cucinelli', 1, '2020-07-24 16:49:40', '2020-07-24 04:19:40'),
(29, 'cheoa', 'vrbsh', 'Molten Brown', 1, '2020-08-07 04:07:34', '2020-08-06 15:37:34'),
(30, 'jproh', 'vrbsh', 'HERSCHEL SUPPLY CO', 0, '2020-08-18 06:38:08', '2020-08-18 06:38:08'),
(31, 'hfoak', 'vrbsh', 'MLM', 0, '2020-08-18 06:38:59', '2020-08-18 06:38:59'),
(32, 'vthpi', 'vrbsh', 'REISS', 0, '2020-08-18 06:39:18', '2020-08-18 06:39:18'),
(33, 'yoahe', 'vrbsh', 'Berluti', 1, '2020-08-11 01:33:04', '2020-08-10 13:03:04'),
(34, 'yluqx', 'vrbsh', 'Saint Laurent', 1, '2020-08-18 18:23:09', '2020-08-18 05:53:09'),
(35, 'gcwzi', 'vrbsh', 'Alexander McQueen', 1, '2020-08-18 18:23:22', '2020-08-18 05:53:22'),
(36, 'kldfc', 'vrbsh', 'Chloe', 1, '2020-08-18 18:23:35', '2020-08-18 05:53:35'),
(37, 'tovmx', 'vrbsh', 'Tods', 1, '2020-08-18 18:24:29', '2020-08-18 05:54:29'),
(38, 'gtsyz', 'vrbsh', 'Tom Ford', 1, '2020-08-18 18:24:39', '2020-08-18 05:54:39'),
(39, 'etrwh', 'vrbsh', 'Roberto Cavalli', 1, '2020-08-18 18:24:47', '2020-08-18 05:54:47'),
(40, 'rjbtx', 'vrbsh', 'Missoni', 1, '2020-08-18 18:24:54', '2020-08-18 05:54:54'),
(41, 'tcjei', 'vrbsh', 'Brioni', 1, '2020-08-18 18:25:18', '2020-08-18 05:55:18'),
(42, 'lzrgj', 'vrbsh', 'Corneliani', 1, '2020-08-18 18:25:28', '2020-08-18 05:55:28'),
(43, 'hiowx', 'vrbsh', 'Gucci', 1, '2020-08-18 18:25:38', '2020-08-18 05:55:38'),
(44, 'paeyg', 'vrbsh', 'Off-White', 1, '2020-08-18 18:25:47', '2020-08-18 05:55:47'),
(45, 'bhykn', 'vrbsh', 'Balmain', 1, '2020-08-18 18:25:59', '2020-08-18 05:55:59'),
(46, 'fdatq', 'vrbsh', 'Bottega Veneta', 1, '2020-08-18 18:26:12', '2020-08-18 05:56:12'),
(47, 'crgft', 'vrbsh', 'Dolce & Gabbana', 1, '2020-08-18 18:26:20', '2020-08-18 05:56:20'),
(48, 'ityha', 'vrbsh', 'Loewe', 1, '2020-08-18 18:26:27', '2020-08-18 05:56:27'),
(49, 'rcdjp', 'vrbsh', 'Balenciaga', 1, '2020-08-18 18:26:45', '2020-08-18 05:56:45'),
(50, 'wcqtk', 'vrbsh', 'Molton Brown', 1, '2020-08-18 18:26:55', '2020-08-18 05:56:55'),
(51, 'bgafk', 'vrbsh', 'Charlotte Tilbury', 1, '2020-08-18 18:27:13', '2020-08-18 05:57:13'),
(52, 'vfjla', 'vrbsh', 'ClÃ© De Peau BeautÃ©', 1, '2020-08-18 18:27:22', '2020-08-18 05:57:22'),
(53, 'vepgo', 'vrbsh', 'Creed', 1, '2020-08-18 18:27:32', '2020-08-18 05:57:32'),
(54, 'zfbgh', 'vrbsh', 'HermÃ¨s', 1, '2020-08-18 18:27:51', '2020-08-18 05:57:51'),
(55, 'nvisa', 'vrbsh', 'La Mer', 1, '2020-08-18 18:27:59', '2020-08-18 05:57:59'),
(56, 'iwtjd', 'vrbsh', 'Burberry', 1, '2020-08-18 18:28:34', '2020-08-18 05:58:34'),
(57, 'ydilm', 'vrbsh', 'Givenchy', 1, '2020-08-18 18:28:55', '2020-08-18 05:58:55'),
(58, 'isowy', 'vrbsh', 'Jimmy Choo', 1, '2020-08-18 18:30:36', '2020-08-18 06:00:36'),
(59, 'piebg', 'vrbsh', 'Max Mara', 1, '2020-08-18 18:30:48', '2020-08-18 06:00:48'),
(60, 'kjezg', 'vrbsh', 'Moncler', 1, '2020-08-18 18:31:21', '2020-08-18 06:01:21'),
(61, 'fbjux', 'vrbsh', 'Roger Vivier', 1, '2020-08-18 18:31:49', '2020-08-18 06:01:49'),
(62, 'fjeso', 'vrbsh', 'Valentino', 1, '2020-08-18 18:32:18', '2020-08-18 06:02:18'),
(63, 'mgrjf', 'vrbsh', 'apple', 1, '2020-08-25 21:22:33', '2020-08-25 08:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `brand_profile`
--

CREATE TABLE `brand_profile` (
  `bf_id` int(255) NOT NULL,
  `brand_key` varchar(10) NOT NULL,
  `title` text DEFAULT NULL,
  `subtitle` text NOT NULL,
  `image` text NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brand_profile`
--

INSERT INTO `brand_profile` (`bf_id`, `brand_key`, `title`, `subtitle`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'zsaqw', 'NEW COLLENTION 35% OFFER MEN', '<p>asjdal akshd hf hsfj&nbsp; lf afla haklalha a a ga as gasg asg aslk gasl gldg a dqyiuq yiu yiwy egiu galgaagzwoigaioteuggf</p>', 'brandpic_20200615160522.jpg', 0, '2020-06-15 10:35:22', '2020-06-15 11:01:39'),
(2, 'hvcmb', NULL, '<p>lkjljl</p>', 'brandpic_20200615173914.jpg', 0, '2020-06-15 12:09:14', '2020-06-15 12:16:09'),
(3, 'yluqx', NULL, '<p>The illustrious label founded by Mr Yves Saint Laurent in 1961 still retains its French irreverence and sleek style under Creative Director Mr Anthony Vaccarello. Expect sharp tailoring, skinny-fit denim and edgy accessories.</p>', 'brandpic_20200722122019.jpg', 1, '2020-07-22 06:50:19', '2020-08-18 06:37:34'),
(4, 'yoahe', NULL, '<div class=\"Header5__headingsContainer\">\r\n<h1 class=\"Header5__title\"><span style=\"font-size: 14px;\">Berluti believes \"you cannot be elegant if you are not comfortable and well shod.\" Founded in 1895 and helmed by Mr Kris Van Assche, the house began with bespoke footwear but today offers accessories and ready-to-wear that mix classic craftsmanship with contemporary sensibility.</span></h1>\r\n</div>', 'brandpic_20200814121821.jpg', 1, '2020-08-14 19:18:21', '2020-08-14 06:48:21'),
(5, 'nxzhs', NULL, '<p>At 25 years old in the mid \'80s, Mr Brunello Cucinelli had an innovative vision for dyed cashmere rather than the prevalent neutral shades and he began his business in a small workshop. Today the company represents the pinnacle of Italian luxury.</p>', 'brandpic_20200818114237.jpg', 1, '2020-08-18 18:42:37', '2020-08-18 06:12:37'),
(6, 'gtsyz', NULL, '<p>&ldquo;Menswear is a detail-driven business,&rdquo; says Mr Tom Ford, who brings his eye for precision to a highly acclaimed offering of immaculately tailored suits, plush cashmere and refined accessories. Shop the exclusive underwear collection at MR PORTER.</p>', 'brandpic_20200818114345.jpg', 1, '2020-08-18 18:43:45', '2020-08-18 06:13:45'),
(7, 'rcdjp', NULL, '<p>Helmed by Mr Demna Gvasalia, Balenciaga offers an unexpected reassessment of structure, while referencing traditional tailoring techniques. Look out for unorthodox basics, cool accessories and noteworthy footwear.</p>', 'brandpic_20200818114642.jpg', 1, '2020-08-18 18:46:42', '2020-08-18 06:16:42'),
(8, 'ydilm', NULL, '<p>French luxury brand Givenchy creates stylish collections that combine dark romanticism with progressive streetwear. Notable fans include Jay Z, Mr Rami Malek and Prince Harry.</p>', 'brandpic_20200818114748.jpg', 1, '2020-08-18 18:47:48', '2020-08-18 06:17:48'),
(9, 'hiowx', NULL, '<p>Under the creative direction of Mr Alessandro Michele, Gucci is redefining luxury for the 21st century. The label is renowned for its Italian craftsmanship, attention to detail and an eclectic aesthetic. Season after season we expect superb bags and accessories, and of course, new versions of the iconic horsebit loafers.</p>', 'brandpic_20200818114857.jpg', 1, '2020-08-18 18:48:57', '2020-08-18 06:18:57'),
(10, 'gcwzi', NULL, '<p>Now designed by Mr McQueen&rsquo;s long-time collaborator, Ms Sarah Burton, Alexander McQueen collections are characterised by immaculate tailoring and a dark, dramatic edge. Look out for sharp suiting and the label\'s signature skulls.&nbsp;</p>', 'brandpic_20200818115045.jpg', 1, '2020-08-18 18:50:45', '2020-08-18 06:20:45'),
(11, 'iwtjd', NULL, '<p>Founded in 1856, Burberry made its name as the original purveyor of the trench coat. Today, the brand is led by Chief Creative Officer Mr Riccardo Tisci, combining a rich design pedigree with a modern approach.</p>', 'brandpic_20200818115322.jpg', 1, '2020-08-18 18:53:22', '2020-08-18 06:23:22'),
(12, 'bhykn', NULL, '<p>Balmain, which was founded in 1945, has recently become known for its rock star glamour and amped-up military style, with sophisticated quality and finishing. In the men\'s collection, the biker jackets, waxed jeans and leather boots are signature pieces this season.</p>', 'brandpic_20200818115550.jpg', 1, '2020-08-18 18:55:50', '2020-08-18 06:25:50'),
(13, 'tcjei', NULL, '<p>Brioni held the first ever men\'s fashion show in Florence&rsquo;s Palazzo Pitti in 1952 and has since become one of the world&rsquo;s leading tailors. The label creates refined basics, accessories and exceptional suits - it takes 22 hours to make just one. All production takes place in Italy.</p>', 'brandpic_20200818115811.jpg', 1, '2020-08-18 18:58:11', '2020-08-18 06:28:11'),
(14, 'tovmx', NULL, '<p>Well known for its handmade footwear and accessories, Tod\'s also turns out impeccable clothing. Combining a modernist aesthetic with high-quality construction, the brand creates staple pieces that slot into a modern wardrobe.</p>', 'brandpic_20200818120438.jpg', 1, '2020-08-18 19:04:38', '2020-08-18 06:34:38'),
(15, 'etrwh', NULL, '<p>Established by Italian athletic champion Mr Ottavio Missoni in 1953, this Italian label is today world-renowned for its luxurious patterned knitwear. Under the creative directorship of the founder&rsquo;s daughter, the men\'s collections have gone from strength to strength in recent seasons.</p>', 'brandpic_20200818121445.jpg', 1, '2020-08-18 19:14:45', '2020-08-18 06:44:45'),
(16, 'fdatq', NULL, '<p>Founded in the Veneto region in 1966, Bottega Veneta has long been synonymous with premium Italian hand-craftsmanship. Today, the brand maintains a deep connection to its heritage by championing the collaboration between artisan and designer.</p>', 'brandpic_20200818121849.jpg', 1, '2020-08-18 19:18:49', '2020-08-18 06:48:49'),
(17, 'crgft', NULL, '<p>Dolce&nbsp;&amp;&nbsp;Gabbana was launched in Milan in 1985 when Mr Domenico Dolce and Mr Stefano Gabbana showed their debut collection. Since then, the label has exploded into a global fashion phenomenon, winning legions of fans for its sleek, luxurious and distinctly Italian collections.</p>', 'brandpic_20200818122040.jpg', 1, '2020-08-18 19:20:40', '2020-08-18 06:50:40'),
(18, 'ityha', NULL, '<p>LOEWE was founded in 1846 in Madrid, and continues to be a world leader in luxurious bags and accessories. The Spanish heritage label, now helmed by Mr Jonathan Anderson, also makes clothes and is renowned for its collaborations with architects.</p>', 'brandpic_20200818122222.jpg', 1, '2020-08-18 19:22:22', '2020-08-18 06:52:22'),
(19, 'nvisa', NULL, '<p>La Mer&rsquo;s Miracle Broth&trade; was originally developed by Dr Max Huber to treat burns from a lab accident. The unique blend of Sea Kelp, vitamins and oils powers each product in the range, including the indulgent &lsquo;Cr&egrave;me de la Mer&rsquo;.</p>', 'brandpic_20200818122611.jpg', 1, '2020-08-18 19:26:11', '2020-08-18 06:56:11'),
(20, 'kjezg', NULL, '<p>Named after the French alpine village Monestier-de-Clermont, Moncler is a leader in the field of technical outerwear. The iconic down-filled jackets cater for life both on and off the slopes, making them a hit with snow-sport enthusiasts and urban dwellers.</p>', 'brandpic_20200818123008.jpg', 1, '2020-08-18 19:30:08', '2020-08-18 07:00:08'),
(21, 'fjeso', NULL, '<p>Under the design direction of Mr Pierpaolo Piccioli, former assistant to Mr Valentino Garavani himself, this illustrious label produces its contemporary range of menswear and accessories with a strong focus on tradition and innovation. While there\'s an emphasis on tailoring and outerwear, studs, camouflage and bold graphics are modern brand signatures.</p>', 'brandpic_20200818123226.jpg', 1, '2020-08-18 19:32:26', '2020-08-18 07:02:26'),
(22, 'cheoa', NULL, '<section class=\"c-self-says o-fs-self-says\" data-ts-self-says=\"Selfridges Says\"><q class=\"c-self-says__content\"><strong><a href=\"http://www.selfridges.com/GB/en/cat/molton-brown/\"><span style=\"color: black;\">Molton Brown</span></a></strong>&nbsp;whisks your morning shower or post-workout bath to a spot in the fresh sea air, metaphorically of course. Inspired by the coast, the label blends Australian sea fennel, salted cypress, spicy cardamom with a hint of jasmine to create the&nbsp;<strong>Cypress &amp; Sea Fennel bath &amp; shower gel</strong>. Formulated without parabens, it lathers into a nourishing, cleansing foam, kick-starting your day or evening wind-down routine.</q></section>\r\n<div class=\"more-from\">&nbsp;</div>', 'brandpic_20200818124136.png', 1, '2020-08-18 19:41:36', '2020-08-18 07:11:36'),
(23, 'kldfc', NULL, '<p>Toughening up the&nbsp;<strong><a href=\"http://www.selfridges.com/GB/en/cat/chloe/bags/womens/shoulder-bags/?llc=sn\"><span style=\"color: black;\">Chlo&eacute;</span></a></strong>&nbsp;girl for AW18, Natacha Ramsay-Levi took inspiration from the \'70s, in a collection that gave a harder edge to the Maison\'s signature romanticised aesthetic. Contoured to a round silhouette in leather and suede, the Tess shoulder bag is defined by the brand\'s signature \'O\' ring that adorns the front flap, while there is a choice of both wide and slim carry straps. (Height 18cm, width 26cm, depth 8cm)</p>', 'brandpic_20200818124843.png', 1, '2020-08-18 19:48:43', '2020-08-18 07:18:43'),
(24, 'lzrgj', NULL, '<p>Expert craftsmanship has been at the heart of&nbsp;<strong><a href=\"https://www.selfridges.com/GB/en/cat/corneliani/\">Corneliani</a></strong>\'s ethos ever since the beginning, and nowadays it can be seen in the form of casualwear&mdash;not just sharp tailoring. This long-sleeved sweatshirt seems like a good place to start, featuring sumptuous cotton-jersey which envelops your form keeping you warm and cosy on chilly days. It\'s topped with a funnel neckline, too, while a brand badge at the sleeve is a sure-fire sign of its made-in-Italy quality.</p>', 'brandpic_20200818125232.png', 1, '2020-08-18 19:52:32', '2020-08-18 07:22:32'),
(25, 'paeyg', NULL, '<p>Ever conceptual in his designs, Virgil Abloh pulls out the stops for his AW20 collection. For the&nbsp;<strong>Trestle T-shirt</strong>, workmanship bridges the gap between something a nothing &ndash; between existence and not existing &ndash; just as&nbsp;<strong><a href=\"https://www.selfridges.com/GB/en/cat/off-white-c-o-virgil-abloh/\">Off-White c/o Virgil Abloh</a></strong>&nbsp;bridges the gap between black and white. Crafted with cotton-jersey and shaped with a crewneck, it&rsquo;s branded with a logo at the back which features an illustrated labourer emblem.</p>', 'brandpic_20200818125536.png', 1, '2020-08-18 19:55:36', '2020-08-18 07:25:36'),
(26, 'bgafk', NULL, '<p>Charlotte Tilbury, the world-renowned make-up artist, wins awards for her namesake brand. Discover the collection of beauty must-haves today, including Charlotte Tilbury make-up and beauty gift sets.</p>', 'brandpic_20200818130203.png', 1, '2020-08-18 20:02:03', '2020-08-18 07:32:03'),
(27, 'vfjla', NULL, '<p>The key to unlocking a new world of beauty. Discover the lastest Skincare and Makeup collection for pure radiance.</p>', 'brandpic_20200818131650.png', 1, '2020-08-18 20:16:50', '2020-08-18 07:46:50'),
(28, 'zfbgh', NULL, '<p><strong>Herm&egrave;s</strong>&nbsp;is a French luxury goods house founded in 1837 by Thierry Hermes. Renowned for its artistic approach, Herm&egrave;s perfumery is today represented by Christine Nagel, and still maintains the brand\'s core values of a free spirited, modern take on their elegant fragrances for men and women.</p>', 'brandpic_20200818132130.png', 1, '2020-08-18 20:21:30', '2020-08-18 07:51:30'),
(29, 'vepgo', NULL, '<p>Handcrafting uncommon scents for over 250 years, London-based label&nbsp;<strong><a href=\"http://www.selfridges.com/cat/creed/\"><span style=\"color: black;\">Creed</span></a></strong>&nbsp;continuously hits the mark in the fragrance world. The&nbsp;<strong>Women&rsquo;s Discovery set</strong>&nbsp;is comprised of three innovative eau de parfums to help you fall in love with the label (if you hadn&rsquo;t already): Love In White, Adventus For Her and Acqua Florentina. Arriving with a pocket atomizer &nbsp;and funnel, these aromas are handbag-ready so they&rsquo;re the ultimate travel companion.</p>', 'brandpic_20200818132526.png', 1, '2020-08-18 20:25:26', '2020-08-18 07:55:26'),
(30, 'piebg', NULL, '<p>Linear tailoring will always be intrinsic to&nbsp;<strong><a href=\"https://www.selfridges.com/GB/en/cat/max-mara/\">Max Mara</a></strong>&mdash;the Italian brand is globally renowned for its sharp cuts, after all. For AW20, Ian Griffiths take a slightly off-kilter route into the workwear realm, by introducing an element of seaside charm to the collection. The&nbsp;<strong>Daphe jacket&nbsp;</strong>is a classic piece &ndash; shaped with padded shoulders and notch lapels &ndash; that\'s barnacled with ruffles at the cuffs that evoke a seaside romance.</p>', 'brandpic_20200818133146.png', 1, '2020-08-18 20:31:46', '2020-08-18 08:01:46'),
(31, 'fbjux', NULL, '<p>Famed for being the creator of the stiletto,&nbsp;<a class=\"analytics-tracked\" href=\"https://www.selfridges.com/GB/en/cat/roger-vivier/shoes/womens/heels/\">Roger Vivier&rsquo;s heels</a>&nbsp;are iconic. Continuing his legacy, the label introduces women&rsquo;s trainers. Think: unrivalled comfort and the brand&rsquo;s signature finishes for kicks you&rsquo;ll want to wear with everything from jeans to dresses.</p>', 'brandpic_20200818135041.png', 1, '2020-08-18 20:50:41', '2020-08-18 08:20:41'),
(32, 'rjbtx', NULL, '<p>Established by Italian athletic champion Mr Ottavio Missoni in 1953, this Italian label is today world-renowned for its luxurious patterned knitwear. Under the creative directorship of the founder&rsquo;s daughter, the men\'s collections have gone from strength to strength in recent seasons.</p>', 'brandpic_20200818140043.jpg', 1, '2020-08-18 21:00:43', '2020-08-18 08:30:43'),
(33, 'isowy', NULL, '<p>British high fashion house specialising in luxury shoes, handbags, accessories and fragrances. The company, J. Choo Limited, was founded in 1996 by couture shoe designer Jimmy Choo and Vogue accessories editor Tamara Mellon. The brand claims to have been a favourite of Diana, Princess of Wales.</p>', 'brandpic_20200818141326.png', 1, '2020-08-18 21:13:26', '2020-08-18 08:43:26');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(255) NOT NULL,
  `cat_key` varchar(10) NOT NULL,
  `category` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_key`, `category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fancy', 'Bags', 1, '2020-06-10 06:30:10', '2020-06-09 18:00:10'),
(2, 'nbawm', 'Shoes', 1, '2020-06-10 06:41:50', '2020-06-09 18:11:50'),
(3, 'njchx', 'Home & Tech', 1, '2020-06-09 18:46:50', '2020-06-09 18:46:50'),
(4, 'bedsz', 'Beauty', 1, '2020-06-09 18:53:37', '2020-06-09 18:53:37'),
(5, 'kxypo', 'BABY BOYS (0â€“3 YEARS)', 0, '2020-06-09 18:45:44', '2020-06-09 18:45:44'),
(6, 'qczhu', 'GIRLS (3â€“16 YEARS)', 0, '2020-06-09 18:45:36', '2020-06-09 18:45:36'),
(7, 'zdvfl', 'BOYS (3â€“16 YEARS)', 0, '2020-06-09 18:45:27', '2020-06-09 18:45:27'),
(8, 'xkmiv', 'Kids', 1, '2020-06-10 07:16:06', '2020-06-09 18:46:06'),
(9, 'zisqv', 'Jewellery ', 1, '2020-08-18 11:55:34', '2020-08-18 11:55:34'),
(10, 'saqem', 'Clothing', 1, '2020-06-10 07:51:14', '2020-06-09 19:21:14'),
(11, 'lcgpa', 'Accessories', 1, '2020-06-10 07:55:09', '2020-06-09 19:25:09'),
(12, 'tzylx', 'Watches', 1, '2020-08-19 00:25:11', '2020-08-18 11:55:11');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(255) NOT NULL,
  `client_key` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `newsletter` int(2) NOT NULL DEFAULT 1,
  `token` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_key`, `title`, `name`, `last_name`, `email`, `mobile`, `password`, `newsletter`, `token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'xhqgm', 'Mr', 'Anand', 'Verma', 'anandverma137@gmail.com', '09718190486', '05948c73ecc96c0a9e42dd5b3292dc7c', 1, 'wdmfksgtaqirxuylezhopvcjbn', 1, '2020-06-11 01:56:36', '2020-09-04 04:41:34'),
(2, 'jqrue', 'Mr', 'Manreet', 'Rathore', 'manreetrathore22@gmail.com', '9999910363', '2dbb64162aa2410c1e66acb2c7a422d2', 1, 'injaperokzwvgutdlsmhfyxcbq', 1, '2020-06-11 03:07:41', '2020-09-04 06:30:38'),
(3, 'uxael', 'Mr', 'Sachin', 'Jangid', 'sachin.jangid26@gmail.com', '9873987305', 'b483ff0bf8ef1dc1e7428f3c5033a83e', 1, 'wjcbvtfoqmrlnguxikdzashpye', 1, '2020-06-11 23:28:29', '2020-09-03 10:31:43'),
(4, 'ywifr', 'Ms', 'Shikha ', 'Kothari', 'Shikhakothari2016@gmail.com', '8826045188', '15085ce0cc584ddc0fd70dcd8fd697df', 1, 'irhfmtkvjbnawxuyosgdqpelcz', 1, '2020-08-14 17:55:54', '2020-09-03 10:18:10'),
(5, 'oumkh', 'Mr', 'sachin', 'jangid', 'sachin.jangid@inovoteq.com', '9013082073', 'e59f6525791d7cfddf6d587c178b765d', 1, 'wgnrvmaptofhlcjxsyiqbuedzk', 1, '2020-08-28 21:45:49', '2020-09-03 05:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `color_id` int(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`color_id`, `color`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Weekly Featured', 1, '2020-07-22 07:29:39', '2020-07-22 07:29:39'),
(2, 'New This Week', 1, '2020-08-09 15:48:41', '2020-08-09 15:48:41'),
(3, 'Spring Collection', 1, '2020-08-18 19:55:42', '2020-08-18 07:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(200) NOT NULL,
  `name` text NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` int(8) NOT NULL,
  `gst` varchar(100) NOT NULL,
  `pancard` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `name`, `address`, `city`, `state`, `pincode`, `gst`, `pancard`, `created_at`, `updated_at`) VALUES
(1, 'LSMART', 'F-110, Ist Floor, Ambience Mall,Ambience Island, Near Toll Plaza, NH-8 ', 'GURGAON', 'HARYANA', 122001, '06AABCI4305A2ZC ', 'AABCI4305A', NULL, '2020-09-04 05:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `deg_id` int(20) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`deg_id`, `designation`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SUPERADMIN', 1, NULL, '2020-08-12 10:34:56'),
(2, 'MANAGER', 1, NULL, '2020-08-12 10:34:56'),
(3, 'EDITOR', 1, NULL, '2020-08-12 10:35:17');

-- --------------------------------------------------------

--
-- Table structure for table `fit`
--

CREATE TABLE `fit` (
  `fit_id` int(255) NOT NULL,
  `fit` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fit`
--

INSERT INTO `fit` (`fit_id`, `fit`, `status`, `created_at`, `updated_at`) VALUES
(1, 'M', 1, '2020-08-07 04:09:35', '2020-08-06 15:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `home_section`
--

CREATE TABLE `home_section` (
  `hs_id` int(255) NOT NULL,
  `section` int(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` text NOT NULL,
  `link` text DEFAULT NULL,
  `image_one` text NOT NULL,
  `image_two` text NOT NULL,
  `image_three` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `slider_id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `image` text NOT NULL,
  `block_type` varchar(100) DEFAULT NULL,
  `text_align` varchar(200) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`slider_id`, `title`, `subtitle`, `link`, `image`, `block_type`, `text_align`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Shop', '', '', 'slider_20200610003015.jpg', 'top', 'center', 0, '2020-06-09 19:02:39', '2020-06-09 19:02:39'),
(2, 'Shop', '', '', 'slider_20200610005611.jpg', 'top', 'center', 0, '2020-06-10 14:35:34', '2020-06-10 14:35:34'),
(3, 'Shop', '', '', 'slider_20200610010051.jpg', 'top', 'center', 1, '2020-06-10 08:00:51', '2020-06-09 19:30:51'),
(4, 'Shop now', '', '', 'slider_20200610012835.jpg', 'top', 'center', 0, '2020-06-09 19:59:20', '2020-06-09 19:59:20'),
(5, 'Shop now', '', '', 'slider_20200610013002.jpg', 'top', 'right', 1, '2020-06-09 20:00:58', '2020-06-09 20:00:58'),
(6, '.', '', '', 'slider_20200610182035.png', 'top', 'center', 1, '2020-06-10 12:50:35', '2020-06-10 12:50:35'),
(7, '.', '', '', 'slider_20200610014406.jpg', 'top', 'center', 0, '2020-06-09 20:14:49', '2020-06-09 20:14:49'),
(8, 'Thoughtful Gifts at LSMART', '', '', 'slider_20200611152126.jpg', 'middle', 'center', 1, '2020-06-11 09:56:47', '2020-06-11 09:56:47'),
(9, 'Stuff to make your world brighter', '', '', 'slider_20200611152918.jpeg', 'bottom', 'center', 1, '2020-06-11 22:29:18', '2020-06-11 09:59:18'),
(10, 'Slider', '', '', 'slider_20200611170301.png', 'top', 'center', 1, '2020-06-12 00:03:01', '2020-06-11 11:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `inv_id` int(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `invno` varchar(255) NOT NULL,
  `qty` varchar(50) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `ceated_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(100) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `category` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bags', 'fancy', 1, '2020-06-10 06:33:04', '2020-06-09 18:03:04'),
(2, 'Shoes', 'nbawm', 1, '2020-06-10 06:49:27', '2020-06-09 18:19:27'),
(3, 'Home & Tech', 'njchx', 1, '2020-06-10 07:05:06', '2020-06-09 18:35:06'),
(4, 'Kids', 'xkmiv', 1, '2020-06-10 07:19:23', '2020-06-09 18:49:23'),
(5, 'Beauty ', 'bedsz', 1, '2020-06-10 07:27:26', '2020-06-09 18:57:26'),
(6, 'Jewellery & Watches', 'zisqv', 1, '2020-06-10 07:47:16', '2020-06-09 19:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(255) NOT NULL,
  `order_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `total_items` int(100) NOT NULL,
  `amount` float NOT NULL,
  `billing_address` varchar(20) NOT NULL,
  `shipping_address` varchar(20) NOT NULL,
  `d_status` int(2) DEFAULT 0,
  `payment` varchar(100) NOT NULL DEFAULT 'COD',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_key`, `user`, `total_items`, `amount`, `billing_address`, `shipping_address`, `d_status`, `payment`, `created_at`, `updated_at`) VALUES
(2, 'fxkzo', 'xhqgm', 4, 172717, 'rkxyt', 'rkxyt', 0, 'COD', '2020-09-04 17:29:18', '2020-09-04 04:59:18'),
(3, 'oedyj', 'xhqgm', 1, 2148, 'rkxyt', 'rkxyt', 0, 'COD', '2020-09-04 18:30:54', '2020-09-04 06:00:54'),
(4, 'fvrps', 'jqrue', 1, 619850, 'ghvej', 'ghvej', 0, 'COD', '2020-09-04 19:01:34', '2020-09-04 06:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `orders_status`
--

CREATE TABLE `orders_status` (
  `os_id` int(255) NOT NULL,
  `orderid` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `status_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_book`
--

CREATE TABLE `order_book` (
  `ob_id` int(255) NOT NULL,
  `order_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `qty` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL,
  `pono` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_book`
--

INSERT INTO `order_book` (`ob_id`, `order_key`, `user`, `product`, `color`, `size`, `qty`, `price`, `pono`, `created_at`, `updated_at`) VALUES
(2, 'fxkzo', 'xhqgm', 'MOON0013', 'DEEP TDM & BLACK', 'TU', '1', '61039', '00022', '2020-09-04 17:29:18', '2020-09-04 04:59:18'),
(3, 'fxkzo', 'xhqgm', 'PR0028', 'PINK', '', '1', '20125', '', '2020-09-04 17:29:18', '2020-09-04 04:59:18'),
(4, 'fxkzo', 'xhqgm', 'PR001', 'NATURALTAN', '', '1', '65302.8', '', '2020-09-04 17:29:18', '2020-09-04 04:59:18'),
(5, 'fxkzo', 'xhqgm', 'PR0033', 'BLACK', '', '1', '26250', 'INV001', '2020-09-04 17:29:18', '2020-09-04 04:59:18'),
(6, 'oedyj', 'xhqgm', 'DIM10', '', '300 ML', '1', '2148', 'INVOICE001', '2020-09-04 18:30:54', '2020-09-04 06:00:54'),
(7, 'fvrps', 'jqrue', 'MOON0092', 'Red', 'R48', '1', '619850', '00030', '2020-09-04 19:01:34', '2020-09-04 06:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` int(255) NOT NULL,
  `p_key` varchar(10) NOT NULL,
  `store` varchar(10) NOT NULL,
  `brand` varchar(10) NOT NULL,
  `product_name` text NOT NULL,
  `sku` varchar(255) NOT NULL,
  `category` varchar(10) NOT NULL,
  `subcategory` varchar(10) NOT NULL,
  `add_cat` varchar(255) DEFAULT NULL,
  `mrp` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `specification` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `features` text DEFAULT NULL,
  `review` int(2) NOT NULL DEFAULT 1,
  `publish` int(2) NOT NULL DEFAULT 1,
  `status` varchar(20) NOT NULL DEFAULT '1',
  `weight` varchar(200) DEFAULT NULL,
  `img` int(255) DEFAULT NULL,
  `type` int(2) DEFAULT 0,
  `groupid` varchar(100) DEFAULT NULL,
  `product_code` varchar(200) DEFAULT NULL,
  `collection_tag` varchar(50) DEFAULT NULL,
  `order_start_date` varchar(255) DEFAULT NULL,
  `line_of_business` varchar(100) DEFAULT NULL,
  `vpn` varchar(100) DEFAULT NULL,
  `style_name` varchar(100) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `material_code` varchar(100) DEFAULT NULL,
  `fit` varchar(100) DEFAULT NULL,
  `batch_no` varchar(100) DEFAULT NULL,
  `min_sale_quantity` varchar(10) DEFAULT NULL,
  `stock` varchar(50) DEFAULT '0',
  `low_stock` varchar(50) DEFAULT NULL,
  `styleno` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `status`, `weight`, `img`, `type`, `groupid`, `product_code`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `styleno`, `hsncode`, `created_at`, `updated_at`) VALUES
(35, 'igsch', 'vrbsh', 'vthpi', 'Esmeralda puffed-sleeve organic cotton-poplin top', 'MOON0058', 'saqem', 'ltpsw', '', '', 209, '<p>duymy</p>', '<p>dsummy</p>', '', 0, 0, '2', NULL, NULL, 1, 'MOON0058', NULL, '', '', 'S4918 KNIT + VN NAVY', '001', 'S4918 KNIT + VN NAVY', '001', 'B72', 'Fabric', 'kl', '001', '10', '0', '2', NULL, NULL, '2020-08-11 02:50:03', '2020-08-17 05:20:13'),
(36, 'girqo', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON-AN-001', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, '', '', 'Shoes - FW20 Main', '213029', 'S3873 VENEZIA NERO GRIGIO', '002', 'K02', '', '', '002', '1', '0', '3', NULL, NULL, '2020-08-12 00:00:46', '2020-08-16 08:44:02'),
(37, 'yivkx', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather Details', 'MOON0090', 'nbawm', 'rewil', '', '90000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<div class=\"fit-advisor\">\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>\r\n</div>\r\n<div class=\"sizechart-container\">&nbsp;</div>', '', 0, 0, '', NULL, NULL, 1, 'MOON0090', NULL, '', '', '', '182639', 'S4918 KNIT + VN NAVY', 'SRL001', 'B72', 'Leather', '7', '1', '576', '576', '2', NULL, NULL, '2020-08-13 05:25:29', '2020-08-14 04:56:33'),
(38, 'xhucw', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0091', 'nbawm', 'rewil', '', '90000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, '', '', '', '182639', 'S4918 KNIT + VN NAVY', 'SRL002', 'B72', 'Leather', '7', '2', '1', '2', '', NULL, NULL, '2020-08-13 05:51:08', '2020-09-03 18:50:41'),
(39, 'scqtb', 'vrbsh', 'yoahe', 'Grain Patina Leather Blazer', 'MOON0092', 'saqem', 'ahyxc', '', '', 619850, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>2 buttons fastenings<br />Buttoned cuffs<br />Two front flap pockets<br />Chest pocket<br />Hand-stitched details at chest, lapel and cuff<br />Patine effect</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features</li>\r\n</ul>\r\n<p>Fully lined in Scritto jacquard<br />Leather label with embossed logo</p>\r\n</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Reference : R18LJL40-KAIP</div>', '<p>The model is 189 cm / 6&rsquo;20&Prime; tall and is wearing a size 48/M</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0092', NULL, '', '', '', '214366', 'LJL39 KAIP DUSTY BLUE', 'SRL003', 'Red', 'Leather', 'R48', '3', '', '2013', '', NULL, NULL, '2020-08-13 20:52:05', '2020-08-14 04:56:33'),
(40, 'tgmdi', 'vrbsh', 'yoahe', 'Tersio Alligator Leather Wallet', 'MOON0093', 'lcgpa', 'fmula', '', '', 758972, '<p>&nbsp;</p>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer Features :</li>\r\n</ul>\r\n<p>One hand strap</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner Features :</li>\r\n</ul>\r\n<p>One zipped pocket<br />One compartment with flat pocket<br />One compartment with 8 credit cards slots</p>\r\n</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Alligator</div>\r\n<div>Reference : TERSIO-ALLIGATOR-E2</div>', '<div>Line : Alligator</div>\r\n<ul>\r\n<li>Dimensions :</li>\r\n</ul>\r\n<p>Height : 23 cm<br />Width : 16 cm<br />Thickness : 2 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0093', NULL, '', '', '', '143952', 'TERSIO ALLIGATOR TOBACCO BIS', 'SRL004', 'M04', 'Leather', 'TU', '4', '1', '', '', NULL, NULL, '2020-08-13 23:34:58', '2020-09-04 06:33:14'),
(41, 'wkitm', 'vrbsh', 'yoahe', 'Makore Alligator Leather Compact Wallet', 'MOON0094', 'lcgpa', 'fmula', '', '', 315366, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner Features :</li>\r\n</ul>\r\n<p>Eight credit cards slots<br />Two note compartments<br />Two patch pockets</p>\r\n</div>\r\n<div>Construction : SIMPLE</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Alligator</div>\r\n<div>Reference : MAKORE-ALLIGATOR-E2</div>', '<div>Line : Alligator</div>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 9,5 cm<br />Width : 11 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0094', NULL, '', '', '', '168169', 'MAKORE ALLIGATOR TOBACCO BIS', 'SRL005', 'M04', 'Leather', 'TU', '5', '1', '1', '', NULL, NULL, '2020-08-14 00:07:46', '2020-09-04 06:48:55'),
(42, 'qyaje', 'vrbsh', 'yoahe', 'Miles Leather And Alligator Messenger Bag', 'MOON0095', 'fancy', 'dmsie', '', '', 315259, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>Two zipped pockets<br />One flat pocket</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : ODYSSEE</div>\r\n<div>Reference : MILES_PM-VC</div>', '<div>Line : ODYSSEE</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>Cross-body wear: Adjustable leather shoulder strap</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 13 cm<br />Width : 16 cm<br />Thickness : 7 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0095', NULL, '', '', '', '214267', 'MILES PM VN+ALLIGATOR ICE', 'SRL006', 'MJ3', 'Leather', '', '6', '1', '2', '', NULL, NULL, '2020-08-14 00:24:48', '2020-09-04 07:09:57'),
(43, 'zyntj', 'vrbsh', 'yoahe', 'MILES PM TOILE+VN BLACK+T', 'MOON0096', 'fancy', 'dmsie', '', '', 101855, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>Two zipped pockets<br />One flat pocket</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : ODYSSEE</div>\r\n<div>Reference : MILES_PM-T9</div>', '<p>&nbsp;</p>\r\n<div>Line : ODYSSEE</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>Cross-body wear: Adjustable leather shoulder strap</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 13 cm<br />Width : 16 cm<br />Thickness : 7 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0096', NULL, '', '', '', '213272', 'MILES PM TOILE+VN BLACK+T', 'SRL007', 'KA2', 'Leather', '', '7', '1', '2', '', NULL, NULL, '2020-08-14 01:03:30', '2020-09-04 05:22:22'),
(44, 'bnkug', 'vrbsh', 'yoahe', 'E\'Mio Scritto Leather Briefcase', 'MOON0097', 'fancy', 'pqmif', '', '', 345753, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features :</li>\r\n</ul>\r\n<p>One flat pocket<br />Flap with clasp that opens when both snap buttons are pressed (leather clasp tab for better handling)</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features :</li>\r\n</ul>\r\n<p>Two independent compartments<br />One zipped pocket<br />Three flat pockets<br />Two pen-holders</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : E Chiuso</div>\r\n<div>Reference : E_MIO-V2</div>', '<div>Line : E Chiuso</div>\r\n<div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n</div>\r\n<p>In the hand : two top handles<br />On the Shoulder: adjustable and removable nylon shoulder strap, included</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 29 cm<br />Width : 40 cm<br />Thickness : 12,5 cm</p>\r\n<ul>\r\n<li>Fits a 13-inch laptop</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0097', NULL, 'PE', '2020-07-01', '', '124253', 'E MIO III VN SCRITTO EBANO', 'SRL008', 'MF6', 'Leather', '', '2020-09-30', '1', '2', '', NULL, NULL, '2020-08-14 04:15:28', '2020-09-04 06:21:28'),
(45, 'krpth', 'vrbsh', 'yoahe', 'Makore 2 In 1 Scritto Leather Wallet', 'MOON0099', 'lcgpa', 'fmula', '', '', 76045, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Wallet section :</li>\r\n</ul>\r\n<p>Eight credit card slots<br />One note compartment<br />Four patch pockets</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Card holder section :</li>\r\n</ul>\r\n<p>Two credit card slots<br />One flat pocket</p>\r\n</div>\r\n<div>Construction : RAW EDGE</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Scritto</div>\r\n<div>Reference : MAKORE_2IN1-SCRITTO-V2</div>', '<div>Line : Scritto</div>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 9,5 cm<br />Width : 11,5 cm<br />Thickness : 2 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0099', NULL, 'PE', '2020-07-01', '', '96250', 'MAKORE 2IN1 VN SCRITTO TOBACCO BIS', 'SRL009', 'M04', 'Leather', '', '9', '1', '5', '', NULL, NULL, '2020-08-14 04:43:55', '2020-09-03 18:11:20'),
(46, 'mawrg', 'vrbsh', 'yoahe', 'Koa Maxi Scritto Leather Zipped Card Holder', 'MOON0011', 'lcgpa', 'fmula', '', '', 49515, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer feature :</li>\r\n</ul>\r\n<p>Four credit card slots<br />One patch pocket for papers or receipts<br />One zipped compartment</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner feature :</li>\r\n</ul>\r\n<p>One brass key ring</p>\r\n</div>\r\n<div>Construction : SIMPLE</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Scritto</div>\r\n<div>Reference : KOA_MAXI-SCRITTO-V2</div>', '<div>Line : Scritto</div>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 8 cm<br />Width : 14 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0011', NULL, 'PE', '2020-07-01', '', '96242', 'KOA MAXI VN SCRITTO TOBACCO BIS', 'SRL0010', 'M04', 'Leather', '', '10', '1', '2', '', NULL, NULL, '2020-08-14 04:51:55', '2020-09-03 18:17:32'),
(47, 'rktoi', 'vrbsh', 'yoahe', 'Essentiel Leather Wallet', 'MOON0012', 'lcgpa', 'fmula', '', '', 61043, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features :&nbsp;</li>\r\n</ul>\r\n<p>Eight credit card slots<br />Two flat pockets<br />One banknote compartment</p>\r\n</div>\r\n<div>Construction : SIMPLE</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Epure</div>\r\n<div>Reference : ESSENTIEL-EPURE-V1</div>', '<div>Line : Epure</div>\r\n<ul>\r\n<li>Dimensions:</li>\r\n</ul>\r\n<p>Height: 9 cm</p>\r\n<div>Width: 12 cm</div>', '', 0, 1, '', NULL, NULL, 1, 'MOON0012', NULL, 'PE', '2020-07-01', '', '152797', 'ESSENTIEL VENEZIA MOGANO', 'SRL0012', 'MF4', 'Leather', '', '12', '1', '2', '', NULL, NULL, '2020-08-14 05:20:39', '2020-09-03 18:14:19'),
(48, 'opymg', 'vrbsh', 'yoahe', 'Essentiel Leather and Alligator Details Wallet', 'MOON0013', 'lcgpa', 'fmula', '', '', 61039, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features</li>\r\n</ul>\r\n<p>Eight credit cards slots<br />Two side pockets for receipts<br />One compartment for banknotes and tickets</p>\r\n</div>\r\n<div>Construction : SIMPLE</div>\r\n<div>Spring 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : PATCHWORK</div>\r\n<div>Reference : ESSENTIEL-PATCHWORK-VC</div>', '<div>Line : PATCHWORK</div>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 9 cm<br />Width : 12 cm</p>', '', 0, 1, '', NULL, NULL, 1, 'MOON0013', NULL, 'PE', '2020-07-01', '', '211843', 'ESSENTIEL VENEZIA TDM INT', 'SRL0013', 'MI5', 'Leather', '', '13', '1', '2', '', NULL, NULL, '2020-08-14 05:26:32', '2020-09-04 05:27:14'),
(49, 'derfy', 'vrbsh', 'yoahe', 'Formula 1004 Scritto Leather Rolling Suitcase', 'MOON0014', 'fancy', 'vuotm', '', '', 690739, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features :&nbsp;</li>\r\n</ul>\r\n<p>One flat pocket<br />One zippered pocket<br />One luggage tag, one padlock and one key pouch<br />One ergonomic telescopic pull handle with two heights<br />One logoed pushbutton to lock and unlock handle<br />One leather top handle<br />Four moulded polyurethane wheels featuring the Berluti &ldquo;B\". Silent and stable with excellent impact resistance<br />Double metal zip closure</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features :</li>\r\n</ul>\r\n<p>Two zippered pockets<br />System for securing garments with two interlocking rings<br />One shoehorn compartment with shoehorn included<br />One cloth shoe bag<br />One zippered opening (to access handle mechanism)</p>\r\n</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Venezia</div>\r\n<div>Reference : FORMULA_1004-SCRITTO_SWIFT-V2</div>', '<div>Line : Venezia</div>\r\n<p>Rolling bag</p>\r\n<ul>\r\n<li>Dimensions.&nbsp;</li>\r\n</ul>\r\n<p>Height: 55 cm</p>\r\n<div>Width: 35 cm</div>\r\n<div>Thickness: 22 cm</div>\r\n<div>Weight: 4.8 kg</div>', '', 0, 1, '', NULL, NULL, 0, 'MOON0014', NULL, 'PE', '2020-07-01', '', '149252', 'FORMULA 1004 II SCR SWIFT', 'SRL0014', 'MG1', 'Leather', '', '13', '', '4', '', NULL, NULL, '2020-08-14 05:34:48', '2020-08-14 04:56:33'),
(50, 'kuyfe', 'vrbsh', 'yoahe', 'Deux Jours Leather Briefcase', 'MOON0016', 'fancy', 'pqmif', '', '', 326561, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features :&nbsp;</li>\r\n</ul>\r\n<p>Two zippered compartments<br />One zippered front pocket<br />One trolley strap on the back<br />Double metal zip closure</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features:&nbsp;</li>\r\n</ul>\r\n<p>First compartment :<br />One zippered pocket<br />Two pen holders<br />One flat pocket<br />One large flat leather-edged pocket<br />Seconf compartment :<br />One padded computer slot<br />One small flat pocket</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Venezia</div>\r\n<div>Reference : 2_JOURS-V1</div>', '<div>Line : Venezia</div>\r\n<p>Hand-carry: two handles. Shoulder: Adjustable and removable nylon shoulder strap included.</p>\r\n<ul>\r\n<li>Dimensions.&nbsp;</li>\r\n</ul>\r\n<p>Height: 30.5 cm</p>\r\n<div>Width: 42 cm</div>\r\n<div>Thickness: 11 cm</div>\r\n<div>\r\n<ul>\r\n<li>Compatible with 15-inch laptop</li>\r\n</ul>\r\n</div>', '', 0, 1, '', NULL, NULL, 1, 'MOON0016', NULL, '', '', '', '149262', '2 JOURS IV VENEZIA MOGANO', 'SRL0016', 'MF4', 'Leather', '', '16', '1', '', '', NULL, NULL, '2020-08-14 06:00:33', '2020-09-04 06:27:14'),
(51, 'msjwt', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0017', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, '', '', '', '182640', 'S4918 KNIT + VN NAVY', '17', 'B72', '', '', '17', '1', '2', '', NULL, NULL, '2020-08-14 17:11:58', '2020-09-04 05:02:28'),
(52, 'jyndb', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0018', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, 'PE', '2020-07-01', '', '182641', 'S4918 KNIT + VN NAVY', '18', 'B72', '', '', '18', '', '2', '', NULL, NULL, '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(53, 'plbdj', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0019', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, 'PE', '2020-07-01', '', '182642', 'S4918 KNIT + VN NAVY', '19', 'B72', '', '', '19', '', '2', '', NULL, NULL, '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(54, 'efosz', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0020', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, 'PE', '2020-07-01', '', '182643', 'S4918 KNIT + VN NAVY', '20', 'B72', '', '', '20', '', '2', '', NULL, NULL, '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(55, 'ipgyx', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0021', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, 'PE', '2020-07-01', '', '182644', 'S4918 KNIT + VN NAVY', '21', 'B72', '', '', '21', '', '1', '', NULL, NULL, '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(56, 'ltwfi', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0022', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, '', '', '', 'testing', 'S4918 KNIT + VN NAVY', '22', '', '', '', '22', '', '0', '', NULL, NULL, '2020-08-14 18:38:09', '2020-08-18 12:44:39'),
(57, 'upgam', 'vrbsh', 'yoahe', 'Formula 1004 Leather Rolling Suitcase', 'MOON0023', 'fancy', 'vuotm', '', '', 670163, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features :&nbsp;</li>\r\n</ul>\r\n<p>One zipped pocket, one flat pocket<br />One luggage tag, one padlock and one key pouch containing two keys<br />One ergonomic telescoping pull handle with two Eights<br />One initialled button to lock and unlock handle &nbsp;One leather top handle<br />One leather side handle<br />Four moulded polyurethane wheels featuring Venezia leather pieces<br />Silent and stable with &nbsp;excellent impact resistance<br />Double metal zipped closure</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features:&nbsp;</li>\r\n</ul>\r\n<p>Two zipped pockets<br />Garment securing system with two linking rings<br />One shoehorn compartment (shoehorn included) &nbsp;One protective shoe bag<br />One zipped opening (access to handle mechanism)</p>\r\n</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : Venezia</div>\r\n<div>Reference : FORMULA_1004-V1</div>', '<div>Line : Venezia</div>\r\n<p>Rolling bag</p>\r\n<ul>\r\n<li>Dimensions:&nbsp;</li>\r\n</ul>\r\n<p>Height: 55 cm</p>\r\n<div>Width: 35 cm&nbsp;</div>\r\n<div>Thickness: 22 cm</div>\r\n<div>Weight: 4.8 Kg</div>', '', 0, 1, '', NULL, NULL, 1, 'MOON0023', NULL, '', '', '', '149267', 'FORMULA 1004 II VN MOGANO', 'SRL0023', 'MF4', '', '', '23', '', '0', '', NULL, NULL, '2020-08-16 06:03:40', '2020-08-15 17:38:00'),
(58, 'hxgcl', 'vrbsh', 'yoahe', 'Toujours Scritto Leather Tote Bag', 'MOON0024', 'fancy', 'xrlas', '', '', 252640, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>One flat pocket<br />One D-ring for accessories</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features</li>\r\n</ul>\r\n<p>One zipped pocket<br />One flat pocket<br />One mobile phone pocket<br />One key ring</p>\r\n</div>\r\n<div>Permanent Collection</div>\r\n<div>Made in Italy</div>\r\n<div>Line : JOUR</div>\r\n<div>Reference : TOUJOURS-V2</div>', '<div>Line : JOUR</div>\r\n<div>\r\n<ul>\r\n<li>Carry:</li>\r\n</ul>\r\n</div>\r\n<p>In the hand: Two handles<br />On the shoulder: Two handles</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 30 cm<br />Width : 44 cm<br />Thickness : 13 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0024', NULL, 'PE', '2020-07-01', '', '190644', 'TOUJOURS II VN SCRITTO MOGANO', 'SRL0024', 'MF4', '', '', '24', '', '0', '', NULL, NULL, '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(59, 'bdhie', 'vrbsh', 'yoahe', 'NINO GM TOILE+VN BLACK+TD', 'MOON0025', 'fancy', 'treia', '', '', 66477, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>One D-ring for accessorization<br />Wide opening</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner features</li>\r\n</ul>\r\n<p>Two flat pockets, including one with six credit cards slots</p>\r\n</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : ODYSSEE</div>\r\n<div>Reference : NINO_GM-T9</div>', '<div>Line : ODYSSEE</div>\r\n<ul>\r\n<li>Carry:</li>\r\n</ul>\r\n<p>In the hand</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 21 cm<br />Width : 29.5 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0025', NULL, 'PE', '2020-07-01', '', '207348', 'NINO GM TOILE+VN BLACK+TD', 'SRL0025', 'KA2', '', '', '25', '', '0', '', NULL, NULL, '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(60, 'thkfq', 'vrbsh', 'yoahe', 'Take Off Small Leather Briefcase', 'MOON0026', 'fancy', 'pqmif', '', '', 222501, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>Zip closure<br />One strap<br />One D-ring for accessorization</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : ODYSSEE</div>\r\n<div>Reference : TAKE_OFF_PM-V1</div>', '<div>Line : ODYSSEE</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>In the hand: One handle<br />Cross-body wear: Adjustable leather shoulder strap</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 23 cm<br />Width : 17 cm<br />Thickness : 6 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0026', NULL, 'PE', '2020-07-01', '', '213262', 'TAKE OFF PM VENEZIA CAMEL', 'SRL0026', 'MJ4', '', '', '26', '', '0', '', NULL, NULL, '2020-08-16 06:51:07', '2020-08-15 18:21:07'),
(61, 'hzlej', 'vrbsh', 'yoahe', 'Gravity Leather Sneaker', 'MOON0027', 'nbawm', 'rewil', '', '', 128536, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Gravity Vitello Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>White edges</li>\r\n<li>Mesh nylon lining and black leather</li>\r\n<li>Padded insole</li>\r\n<li>Playful color block</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : GRAVITY 1</div>\r\n<div>Reference : S4781-CK</div>', '<div>Last : GRAVITY 1</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0027', NULL, '', '', '', 'Test', 'Test', 'SRL0027', 'MJ4', '', '', '27', '', '0', '', NULL, NULL, '2020-08-16 07:06:20', '2020-08-16 07:45:44'),
(62, 'szmlj', 'vrbsh', 'yoahe', 'Globe-Trotter Attache Case Canvas And Leather Briefcase', 'MOON0028', 'fancy', 'pqmif', '', '', 310260, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>One removable leather shoulder strap with shoulder patch<br />One leather handle<br />One lock closure</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner feature</li>\r\n</ul>\r\n<p>15L capacity</p>\r\n</div>\r\n<div>Berluti x Globe-Trotter</div>\r\n<div>UK</div>\r\n<div>Line : GLOBE-TROTTER</div>\r\n<div>Reference : ATTACHE_CASE-T9</div>', '<div>Line : GLOBE-TROTTER</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>In the hand: one handle<br />On the shoulder: Leather shoulder strap<br />Cross-body wear: Leather shoulder strap</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 35,5 cm<br />Width : 28 cm<br />Thickness : 11 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0028', NULL, 'PE', '2020-07-01', '', '212759', 'ATTACHE CASE TOILE+VN BLA', 'SRL0028', 'KA2', '', '', '28', '', '0', '', NULL, NULL, '2020-08-16 07:15:10', '2020-08-15 18:45:10'),
(63, 'ckdqz', 'vrbsh', 'yoahe', 'Globe-Trotter Carry On Canvas And Leather Rolling Suitcase', 'MOON0029', 'fancy', 'vuotm', '', '', 5690, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>Two leather handles<br />Two leather closing straps<br />One lock closure<br />Two wheels<br />One ergonomic telescopic pull handle<br />One name tag<br />One key bell</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner feature</li>\r\n</ul>\r\n<p>34L capacity<br />Adjustable webbing compression straps</p>\r\n</div>\r\n<div>Berluti x Globe-Trotter</div>\r\n<div>UK</div>\r\n<div>Line : GLOBE-TROTTER</div>\r\n<div>Reference : CARRY_ON-T9</div>', '<div>Line : GLOBE-TROTTER</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>Rolling</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 53 cm<br />Width : 38 cm<br />Thickness : 17 cm<br />Weight : 4 kg</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0029', NULL, 'PE', '2020-07-01', '', '212761', 'CARRY ON TOILE+VN BLACK+T', '29', 'KA2', '', '', '29', '', '0', '', NULL, NULL, '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(64, 'luqoh', 'vrbsh', 'yoahe', ' Take Off Small Canvas And Leather Briefcase', 'MOON0030', 'fancy', 'pqmif', '', '', 166654, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>Zip closure<br />One strap<br />One D-ring for accessorization</p>\r\n</div>\r\n<div>Strap Included</div>\r\n<div>Winter 20</div>\r\n<div>Made in Italy</div>\r\n<div>Line : ODYSSEE</div>\r\n<div>Reference : TAKE_OFF_PM-T9</div>', '<div>Line : ODYSSEE</div>\r\n<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>In the hand: One handle<br />Cross-body wear: Adjustable leather shoulder strap</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 23 cm<br />Width : 17 cm<br />Thickness : 6 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0030', NULL, 'PE', '2020-07-01', '', '213261', 'TAKE OFF PM TOILE+VN BLAC', 'SRL0030', 'KA2', '', '', '30', '', '0', '', NULL, NULL, '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(65, 'tabnk', 'vrbsh', 'yoahe', 'Globe-Trotter Watch Box Canvas And Leather Travel Accessory', 'MOON0032', 'lcgpa', 'kasfb', '', '', 348378, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Outer features</li>\r\n</ul>\r\n<p>One leather handle<br />3 digits combination lock</p>\r\n</div>\r\n<div class=\"level-1-item\">\r\n<ul>\r\n<li>Inner feature</li>\r\n</ul>\r\n<p>One removable tray with six storages for watches</p>\r\n</div>\r\n<div>Berluti x Globe-Trotter</div>\r\n<div>UK</div>\r\n<div>Reference : WATCH_BOX-T9</div>', '<ul>\r\n<li>Carry</li>\r\n</ul>\r\n<p>In the hand: one handle</p>\r\n<ul>\r\n<li>Dimensions</li>\r\n</ul>\r\n<p>Height : 14 cm<br />Width : 24 cm<br />Thickness : 21 cm</p>', '', 0, 1, '', NULL, NULL, 0, 'MOON0032', NULL, 'PE', '2020-07-01', '', '214336', 'WATCH BOX TOILE + VN BLAC', 'SRL0031', '31', '', '', '31', '', '0', '', NULL, NULL, '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(66, 'rkpnl', 'vrbsh', 'yoahe', 'Shadow Knit Sneaker With Leather', 'MOON0033', 'nbawm', 'rewil', '', '90,000', 88122, '<p class=\"pa1-rmm product-description\">With brown leather details, these navy sneakers from Berluti are made from a lightweight knit construction with an embroidered logo on the upper, and embossed branding on the tongue.</p>\r\n<p>&nbsp;</p>\r\n<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">upper: leather, fabric</li>\r\n<li class=\"pa1-rmm\">sole: fabric insole, rubber sole</li>\r\n<li class=\"pa1-rmm\">round toe</li>\r\n<li class=\"pa1-rmm\">lace-up</li>\r\n</ul>', '<ul class=\"disc featurepoints\">\r\n<li class=\"pa1-rmm\">True to size</li>\r\n<li class=\"pa1-rmm\">Half sizes please take the next size up</li>\r\n<li class=\"pa1-rmm\">UK sizes</li>\r\n<li class=\"pa1-rmm\">2,5cm-1\" platform (size EU 38)</li>\r\n<li class=\"pa1-rmm\">4,5cm-2\" wedge heel (size EU 38)</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON0091', NULL, 'PE', '2020-07-01', '', '182645', 'S4918 KNIT + VN NAVY', '33', 'B72', '', '', '33', '', '0', '2', NULL, NULL, '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(67, 'qmloh', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON0035', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, 'PE', '2020-07-01', '', '213030', 'S3873 VENEZIA NERO GRIGIO', '35', 'K02', '', '', '35', '', '0', '', NULL, NULL, '2020-08-16 21:16:12', '2020-08-16 08:46:12'),
(68, 'otbyz', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON0036', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, 'PE', '2020-07-01', '', '213031', 'S3873 VENEZIA NERO GRIGIO', '36', 'K02', '', '', '36', '', '0', '', NULL, NULL, '2020-08-16 21:17:55', '2020-08-16 08:47:55'),
(69, 'ayjtx', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON0037', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, 'PE', '2020-07-01', '', '213032', 'S3873 VENEZIA NERO GRIGIO', '37', 'K02', '', '', '37', '', '0', '', NULL, NULL, '2020-08-16 21:20:06', '2020-08-16 08:50:06'),
(70, 'ecody', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON0038', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, 'PE', '2020-07-01', '', '213033', 'S3873 VENEZIA NERO GRIGIO', '38', 'K02', '', '', '38', '', '0', '', NULL, NULL, '2020-08-16 21:23:00', '2020-08-16 08:53:00'),
(71, 'ejqry', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', 'MOON0039', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, 'PE', '2020-07-01', '', '213034', 'S3873 VENEZIA NERO GRIGIO', '39', 'K02', '', '', '39', '', '0', '', NULL, NULL, '2020-08-16 21:24:30', '2020-08-16 08:54:30'),
(72, 'jonic', 'vrbsh', 'yoahe', 'Fast Track Leather Sneaker', '213035', 'nbawm', 'mwbki', '', '', 109920, '<div class=\"level-1-item\">\r\n<ul>\r\n<li>Fast Track Low-Cut Sneaker</li>\r\n<li>Incollato construction</li>\r\n<li>New color variation: Ice Brown</li>\r\n<li>Two-eyelet lace-up shoe</li>\r\n<li>Perforations and decorative toe</li>\r\n<li>Rubber outsole</li>\r\n</ul>\r\n</div>\r\n<div>Construction : Incollato</div>\r\n<div>Fall 20</div>\r\n<div>Made in Italy</div>\r\n<div>Last : Torino</div>\r\n<div>Reference : S3873-V1</div>', '<div>Last : Torino</div>\r\n<ul>\r\n<li>Model available in sizes 5 to 12,5</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'MOON-AN-001', NULL, '', '', '', '213035', 'S3873 VENEZIA NERO GRIGIO', '39', 'K02', '', '', '39', '', '0', '', NULL, NULL, '2020-08-16 21:26:32', '2020-08-18 12:23:39'),
(74, 'rgnip', 'vrbsh', 'mgrjf', 'iPhone 11 Pro 64GB Space Grey', 'MWHD2HN/A', 'njchx', 'syodq', '', NULL, 117100, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\">\r\n<li><span class=\"a-list-item\">6.5-inch (16.5 cm) Super Retina XDR OLED display</span></li>\r\n<li><span class=\"a-list-item\">Water and dust resistant (4 meters for up to 30 minutes, IP68)</span></li>\r\n<li><span class=\"a-list-item\">Triple-camera system with 12MP Ultra Wide, Wide, and Telephoto cameras; Night mode, Portrait mode, and 4K video up to 60fps</span></li>\r\n<li><span class=\"a-list-item\">12MP TrueDepth front camera with Portrait Mode, 4K video, and Slo-Mo</span></li>\r\n<li><span class=\"a-list-item\">Face ID for secure authentication and Apple Pay</span></li>\r\n<li><span class=\"a-list-item\">A13 Bionic chip with third-generation Neural Engine</span></li>\r\n<li><span class=\"a-list-item\">Fast charge with 18W adapter included</span></li>\r\n</ul>', '', '', 0, 0, '', NULL, NULL, 1, 'MWHD2HN/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '2', NULL, NULL, '2020-08-25 21:32:26', '2020-09-03 11:08:31'),
(75, 'iaxeb', 'vrbsh', 'mgrjf', '13-inch MacBook Air: 1.1GHz quad-core 10th-generation Intel Core i5 processor, 512GB - Space Grey', 'MVH22HN/A', 'njchx', 'ckmvp', '', NULL, 122990, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\">\r\n<li><span class=\"a-list-item\">Dual-core 5th-generation Intel Core i5 processor</span></li>\r\n<li><span class=\"a-list-item\">Intel HD Graphics 6000</span></li>\r\n<li><span class=\"a-list-item\">SSD storage</span></li>\r\n<li><span class=\"a-list-item\">8GB memory</span></li>\r\n<li><span class=\"a-list-item\">Two USB 3 ports</span></li>\r\n<li><span class=\"a-list-item\">Thunderbolt 2 port</span></li>\r\n<li><span class=\"a-list-item\">SDXC port</span></li>\r\n</ul>\r\n<div class=\"a-row a-expander-container a-expander-inline-container\" aria-live=\"polite\">\r\n<div class=\"a-expander-content a-expander-extend-content a-expander-content-expanded\" aria-expanded=\"true\">\r\n<ul class=\"a-unordered-list a-vertical a-spacing-none\">\r\n<li><span class=\"a-list-item\">Up to 12 hours of battery life</span></li>\r\n<li><span class=\"a-list-item\">Manufacturer Detail: Apple Inc, One Apple Park way, Cupertino, CA95014,USA.</span></li>\r\n<li><span class=\"a-list-item\">Packer Detail: Apple India Private Limited No.24 19th Floor, Concorde Tower C, UB city, Vittal Mallya Road Bangalore-560 001.</span></li>\r\n<li><span class=\"a-list-item\">Importer Details: Apple India Private Limited No.24 19th Floor, Concorde Tower C, UB city, Vittal Mallya Road Bangalore-560 001.</span></li>\r\n<li><span class=\"a-list-item\">Country of Origin: China</span></li>\r\n</ul>\r\n</div>\r\n</div>', '', '', 0, 1, '', NULL, NULL, 1, 'MVH22HN/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '2', NULL, NULL, '2020-08-27 01:28:06', '2020-08-26 13:00:11'),
(76, 'kyuao', 'vrbsh', 'mgrjf', '16-inch MacBook Pro with Touch Bar: 2.6GHz 6-core 9th-generation Intel Core i7 processor, 512GB - Space Grey', 'Mac0001', 'njchx', 'aembv', '', NULL, 199900, '<p>Expand your view of everything on MacBook Pro thanks to a larger 16-inch Retina display with sharper<br />pixel resolution and support for millions of colours.(1) Harness the power of 6- or 8-core processors and AMD Radeon Pro 5000M series graphics with up to 8GB of GDDR6 memory, together with an optimised thermal architecture for groundbreaking performance. Featuring up to 64GB of 2666MHz memory and up to 8TB of storage.(3) Touch ID and the Touch Bar. And all-day battery life.(2) Designed for pros who put performance above all else, MacBook Pro gives you the power to accomplish anything, anywhere.</p>', '', '', 0, 0, '', NULL, NULL, 1, 'Mac0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-27 18:06:04', '2020-08-31 07:29:14'),
(78, 'qdvmh', 'vrbsh', 'vepgo', 'Green Irish Tweed', '1105032', 'bedsz', 'gfqra', '', NULL, 15500, '', '', '', 0, 1, '3', NULL, NULL, 0, '1105032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-27 23:16:54', '2020-08-27 10:46:54'),
(79, 'dnjiu', 'vrbsh', 'vepgo', ' Millesime Royal Water 50ml', '1105036', 'bedsz', 'gfqra', '', NULL, 15500, '', '', '', 0, 1, '3', NULL, NULL, 1, '1105036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-27 23:30:57', '2020-08-27 11:56:01'),
(80, 'gkinc', 'vrbsh', 'vepgo', ' Millesime Himalaya', '1105039', 'bedsz', 'gfqra', '', NULL, 15312, '', '', '', 0, 1, '3', NULL, NULL, 1, '1105039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-28 00:03:34', '2020-08-27 11:55:43'),
(81, 'mgjyq', 'vrbsh', 'vepgo', ' Millesime Original Vetiver', '1105040', 'bedsz', 'gfqra', '', NULL, 15500, '', '', '', 0, 1, '3', NULL, NULL, 1, '1105040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-28 00:22:17', '2020-08-27 11:55:05'),
(82, 'ygckv', 'vrbsh', 'vepgo', 'Millesime Original Santal 50ml', '1105041', 'bedsz', 'gfqra', '', NULL, 15500, '', '', '', 0, 1, '3', NULL, NULL, 1, '1105041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-28 00:42:37', '2020-08-27 12:13:44'),
(83, 'vyibq', 'vrbsh', 'vepgo', 'Candle Ambiance Green Irish Tweed  200g', '1320032', 'bedsz', 'xzpdg', '', NULL, 11416.3, '', '', '', 0, 1, '3', NULL, NULL, 1, '1320032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-28 01:14:37', '2020-08-27 13:15:45'),
(84, 'ulrhp', 'vrbsh', 'yluqx', 'Monogram quilted leather pouch', 'SK001', 'fancy', 'esowr', '', NULL, 42229.1, '                                <ul>\r\n<li>Saint Laurent leather pouch</li>\r\n<li>Top zip closure</li>\r\n<li>Detachable wrist strap, gold-toned hardware, monogram logo at front, two interior slip pockets, six card slots, quilted finish</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Height 17cm, width 24cm, depth 1.5cm</li>\r\n</ul>                                ', '                                                                ', '                                                                ', 0, 1, '', NULL, NULL, 1, 'SK001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 01:48:49', '2020-08-31 08:29:56'),
(85, 'cxqtu', 'vrbsh', 'yluqx', 'Panier raffie tote bag', 'PR001', 'fancy', 'xrlas', '', NULL, 65302.8, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;raffia tote</li>\r\n<li>Open top</li>\r\n<li>Leather twin handles, woven, YSL branded leather keyholder, one main compartment, interior zipped pocket, gold-toned hardware</li>\r\n<li>100% viscose</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Height 30cm, width 30cm, depth 27cm</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 02:13:31', '2020-08-30 13:50:19'),
(86, 'ulcxm', 'vrbsh', 'yluqx', 'Loulou mini monogram puffer shoulder bag', 'GRU001', 'fancy', 'esowr', '', NULL, 91423.9, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;shoulder bag</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Magnetic clasp</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Fold over top, detachable chain and leather strap, internal zip pocket, silver-tone monogram plaque, quilted, silver-tone hardware</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Leather</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Use specialist cleaner</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Height 15.5cm, width 23cm, depth 8.5cm</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Maximum shoulder strap drop 48cm</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<li>Minimum shoulder strap drop 28cm</li>\r\n</ul>\r\n</div>\r\n</article>\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</article>\r\n<article id=\"content3\" class=\"c-tabs__content \" role=\"tabpanel\">\r\n<div class=\"c-self-plus \">\r\n<article class=\"c-self-plus__section\">\r\n<div class=\"c-self-plus__footer\">\r\n<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</article>\r\n</section>\r\n</section>\r\n<section class=\"c-product-info__sku\"></section>\r\n</div>\r\n</article>\r\n</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 02:14:16', '2020-08-31 07:48:24'),
(87, 'ujyon', 'vrbsh', 'tovmx', 'City tie-detail suede driving shoes', 'GRU002', 'nbawm', 'ikvzp', '', NULL, 28733.2, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">Tod&rsquo;s suede driving shoes<br />Round toe, tie detail, pebbling at heel<br />Suede upper, leather lining, rubber sole<br />Made in Italy</div>\r\n</article>\r\n<article id=\"content3\" class=\"c-tabs__content \" role=\"tabpanel\">\r\n<div class=\"c-self-plus \">\r\n<article class=\"c-self-plus__section\">\r\n<div class=\"c-self-plus__footer\">&nbsp;</div>\r\n</article>\r\n</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 02:47:57', '2020-08-30 14:17:57');
INSERT INTO `product` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `status`, `weight`, `img`, `type`, `groupid`, `product_code`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `styleno`, `hsncode`, `created_at`, `updated_at`) VALUES
(88, 'angbq', 'vrbsh', 'tovmx', 'Mountain suede hiking boots', 'PR002', 'nbawm', 'ncotb', '', NULL, 35698.9, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<ul>\r\n<ul>\r\n<li><strong>Tod&rsquo;s&nbsp;</strong>suede hiking boots</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Lace-up fastening</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Round toe, metal eyelets, logo to tongue, sculpted midsole, logo to heel, pull-tab to the back</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<li>Suede upper, rubber sole</li>\r\n</ul>\r\n</div>\r\n</article>\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</article>\r\n<article id=\"content3\" class=\"c-tabs__content \" role=\"tabpanel\">\r\n<div class=\"c-self-plus \">&nbsp;</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 1, 'PR002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 03:02:23', '2020-08-30 14:34:40'),
(91, 'smuix', 'vrbsh', 'nxzhs', 'Metallic-trimmed cotton-blend shirt', 'DIM002', 'saqem', 'xplro', '', NULL, 74880.5, '<ul>\r\n<ul>\r\n<li><strong>Brunello Cucinelli&nbsp;</strong>cotton-blend shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>72% cotton, 23% polyamide, 5% elastane</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed and concealed button fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Spread collar</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Buttoned cuffs</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Long sleeves, pearlescent buttons, metallic trim at front, tonal stitching, curved hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(92, 'wocun', 'vrbsh', 'nxzhs', 'Shearling-Trimmed Cashmere Bomber Jacket', 'DIM003', 'saqem', 'wiflq', '', NULL, 582466, '<p>In muted neutrals, Brunello Cucinelli\'s bomber jacket has timeless appeal. It\'s made from soft cashmere, padded for warmth and has a detachable shearling collar. Fasten the throat latch on particularly cold days.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Cut for a loose fit with a thick lining for a close internal fit</li>\r\n<li>Ribbed hem</li>\r\n<li>Padded, non-stretchy fabric</li>\r\n<li>Model wears a M</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'2\"/ 188cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 0, 'DIM003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:24:26', '2020-08-30 15:54:26'),
(93, 'htorm', 'vrbsh', 'nxzhs', 'Wide-leg high-rise wool trousers', 'GRU003', 'saqem', 'mdrvj', '', NULL, 82716.9, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<ul>\r\n<ul>\r\n<li><strong>Brunello Cucinelli</strong>&nbsp;wool trousers</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>100% virgin wool</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Concealed button, hook-and-eye and zip-fly fastening</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>High rise, wide leg, straight, belt loops, pleated at front, slip pockets at sides, all-over check t back yoke, two welt pockets at back</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Size UK 8: Rise 14in / 36cm, inside leg 32in / 81cm, leg opening 16in / 40.5cm</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Model is 5ft 9in/1.75m and wears a size UK 8</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<li>Midweight, non-stretch</li>\r\n</ul>\r\n</div>\r\n</article>\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(94, 'yerwj', 'vrbsh', 'nxzhs', 'Wide-leg high-rise wool trousers', 'GRU004', 'saqem', 'mdrvj', '', NULL, 82716.9, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<ul>\r\n<ul>\r\n<li><strong>Brunello Cucinelli</strong>&nbsp;wool trousers</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>100% virgin wool</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Concealed button, hook-and-eye and zip-fly fastening</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>High rise, wide leg, straight, belt loops, pleated at front, slip pockets at sides, all-over check t back yoke, two welt pockets at back</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Size UK 8: Rise 14in / 36cm, inside leg 32in / 81cm, leg opening 16in / 40.5cm</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<ul>\r\n<li>Model is 5ft 9in/1.75m and wears a size UK 8</li>\r\n</ul>\r\n</ul>\r\n<br />\r\n<ul>\r\n<li>Midweight, non-stretch</li>\r\n</ul>\r\n</div>\r\n</article>\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '1', NULL, NULL, '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(95, 'sdbeo', 'vrbsh', 'nxzhs', 'Shearling-Trimmed Cashmere Bomber Jacket', 'DIM004', 'saqem', 'wiflq', '', NULL, 582466, '<p>In muted neutrals, Brunello Cucinelli\'s bomber jacket has timeless appeal. It\'s made from soft cashmere, padded for warmth and has a detachable shearling collar. Fasten the throat latch on particularly cold days.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Cut for a loose fit with a thick lining for a close internal fit</li>\r\n<li>Ribbed hem</li>\r\n<li>Padded, non-stretchy fabric</li>\r\n<li>Model wears a M</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'2\"/ 188cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'DIM003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(96, 'awvlz', 'vrbsh', 'nxzhs', 'Shearling-Trimmed Cashmere Bomber Jacket', 'DIM005', 'saqem', 'wiflq', '', NULL, 582466, '<p>In muted neutrals, Brunello Cucinelli\'s bomber jacket has timeless appeal. It\'s made from soft cashmere, padded for warmth and has a detachable shearling collar. Fasten the throat latch on particularly cold days.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Cut for a loose fit with a thick lining for a close internal fit</li>\r\n<li>Ribbed hem</li>\r\n<li>Padded, non-stretchy fabric</li>\r\n<li>Model wears a M</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'2\"/ 188cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'DIM003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(97, 'kzrwj', 'vrbsh', 'nxzhs', 'Suede-Trimmed Leather Sneakers', 'DIM006', 'nbawm', 'rewil', '', NULL, 60823, '<p>Inspired by traditional tennis styles, Brunello Cucinelli\'s sneakers are free from obvious branding, meaning they\'ll look good with tailoring and casual outfits alike. Set on lightweight rubber soles, they\'re made from smooth white leather and finished with contrasting tan heel tabs in suede.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>European sizing</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'DIM006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:41:21', '2020-09-03 18:46:30'),
(98, 'paogb', 'vrbsh', 'nxzhs', 'Suede-Trimmed Leather Sneakers', 'DIM007', 'nbawm', 'rewil', '', NULL, 60823, '<p>Inspired by traditional tennis styles, Brunello Cucinelli\'s sneakers are free from obvious branding, meaning they\'ll look good with tailoring and casual outfits alike. Set on lightweight rubber soles, they\'re made from smooth white leather and finished with contrasting tan heel tabs in suede.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>European sizing</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'DIM006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(99, 'yeqpz', 'vrbsh', 'nxzhs', 'Selvedge Denim Jeans', 'PR003', 'saqem', 'iolzh', '', NULL, 72094.3, '<p>Brunello Cucinelli\'s jeans have been made in Solomeo, a small medieval village that\'s home to the Italian label\'s workshop. This pair is cut from pure cotton-denim with a selvedge finish to ensure they\'re exceptionally durable. The traditional straight-leg profile looks smart with a shirt and blazer.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Straight leg, mid-rise style</li>\r\n<li>Size IT48 jeans have a hem width of approximately 6.6&rdquo;/ 17cm</li>\r\n<li>Mid-weight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: waist size 32, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Outside Leg:&nbsp;111cm&nbsp;/ 43.7in</li>\r\n<li>Inside_Leg:&nbsp;85cm&nbsp;/ 33.5in</li>\r\n<li>Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Rise:&nbsp;26cm&nbsp;/ 10.2in</li>\r\n<li>Leg Opening:&nbsp;37cm&nbsp;/ 14.6in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 05:12:21', '2020-08-30 16:54:14'),
(100, 'xuodg', 'vrbsh', 'nxzhs', 'Selvedge Denim Jeans', 'PR004', 'saqem', 'iolzh', '', NULL, 72094.3, '<p>Brunello Cucinelli\'s jeans have been made in Solomeo, a small medieval village that\'s home to the Italian label\'s workshop. This pair is cut from pure cotton-denim with a selvedge finish to ensure they\'re exceptionally durable. The traditional straight-leg profile looks smart with a shirt and blazer.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Straight leg, mid-rise style</li>\r\n<li>Size IT48 jeans have a hem width of approximately 6.6&rdquo;/ 17cm</li>\r\n<li>Mid-weight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: waist size 32, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Outside Leg:&nbsp;111cm&nbsp;/ 43.7in</li>\r\n<li>Inside_Leg:&nbsp;85cm&nbsp;/ 33.5in</li>\r\n<li>Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Rise:&nbsp;26cm&nbsp;/ 10.2in</li>\r\n<li>Leg Opening:&nbsp;37cm&nbsp;/ 14.6in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(101, 'jyxbg', 'vrbsh', 'vepgo', 'Green Irish Tweed Eau De Parfum', 'GRU005', 'bedsz', 'gfqra', '', NULL, 14366.6, '<p><strong>Creed&nbsp;</strong>Green Irish Tweed Eau De Parfum<br />Notes: Lemon, Verbena, Peppermint, Violet Leaves, Iris, Sandalwood, Ambergris</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 05:49:27', '2020-08-30 17:19:27'),
(102, 'jkgoi', 'vrbsh', 'nxzhs', 'Slim-Fit Contrast-Tipped Cotton-Jersey Polo Shirt', 'PR006', 'saqem', 'goyec', '', NULL, 42267.4, '<div id=\"EDITORS_NOTES\" class=\"AccordionSection2\">\r\n<div class=\"AccordionSection2__content AccordionSection2__content--openAnimation AccordionSection2__content--pdpAccordion content\">\r\n<div class=\"AccordionSection2__contentChildWrapper\">\r\n<div class=\"EditorialAccordion80__accordionContent\">\r\n<p>Made at Brunello Cucinelli\'s Umbrian workshop from soft cotton-jersey, this polo shirt is embroidered with a crest paying homage to its Solomeo HQ (a medieval castle, no less). Contrast tipping enhances the sporty look, while side slits keep the slim fit comfortable.</p>\r\n<br />\r\n<p>Shown here with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1208500\">Brunello Cucinelli Jeans</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1208537\">Brunello Cucinelli Gilet</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1212207\">Brunello Cucinelli Sneakers</a>.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div id=\"SIZE_AND_FIT\" class=\"AccordionSection2\"></div>', '<div id=\"SIZE_AND_FIT\" class=\"AccordionSection2\">\r\n<div class=\"AccordionSection2__content AccordionSection2__content--openAnimation AccordionSection2__content--pdpAccordion content\">\r\n<div class=\"AccordionSection2__contentChildWrapper\">\r\n<div class=\"EditorialAccordion80__accordionContent\">\r\n<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This polo shirt is designed for a slim fit</li>\r\n<li>Mid-weight, stretchy fabric</li>\r\n<li>Model wears a M</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Chest:&nbsp;108cm&nbsp;/ 42.5in</li>\r\n<li>Back Length:&nbsp;73cm&nbsp;/ 28.7in</li>\r\n<li>Sleeve Length:&nbsp;48cm&nbsp;/ 18.9in</li>\r\n<li>Shoulder Width:&nbsp;45cm&nbsp;/ 17.7in</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div id=\"DETAILS_AND_CARE\" class=\"AccordionSection2\"></div>', '', 0, 1, '', NULL, NULL, 1, 'PR006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 05:58:37', '2020-09-03 19:10:56'),
(103, 'yovzm', 'vrbsh', 'vepgo', 'Aventus for Her eau de parfum splash', 'GRUOO6', 'bedsz', 'gfqra', '', NULL, 42229.1, '<ul>\r\n<ul>\r\n<li><strong>Creed&nbsp;</strong>Aventus for Her eau de parfum splash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Top notes: Green apple, violet leaf, pink pepper, calabrian bergamot</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Heart notes: Rose, styrax, mysore sandalwood</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Base notes: Peach, cassis, amber, ylang ylang</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>250ml</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRUOO6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:00:56', '2020-08-30 17:30:56'),
(104, 'aqdzf', 'vrbsh', 'nxzhs', 'Button-Down Collar Checked Cotton-Twill Shirt', 'PR005', 'saqem', 'xplro', '', NULL, 63916.6, '<p>No matter how casual the piece, Brunello Cucinelli applies the same level of consideration as it does to its suiting, knits and outerwear. Each button on this shirt, for example, is fashioned from horn, while the checked cotton-twill has been chosen for its lightweight handle and softness.</p>\r\n<p>&nbsp;</p>\r\n<p>Shown here with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263230\">Brunello Cucinelli Jacket</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263309\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1213831\">Officine Generale T-shirt</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1165241\">TOM FORD Sneakers</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This shirt is intended for a regular cut</li>\r\n<li>Mid-weight, non-stretchy fabric</li>\r\n<li>Model wears a M</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:13:23', '2020-09-04 05:33:12'),
(105, 'udsfx', 'vrbsh', 'vepgo', 'Gold-tone leather atomiser 10ml', 'GRU007', 'bedsz', 'gfqra', '', NULL, 11754.5, '<ul>\r\n<ul>\r\n<li><strong>Creed&nbsp;</strong>leather perfume atomiser</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Gold-toned, logo embossed, refillable</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Compatible with splash fragrances in 250ml and 500ml</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>10ml</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:13:38', '2020-08-30 17:43:38'),
(106, 'uhixv', 'vrbsh', 'vepgo', 'Love In White Eau de Parfum', 'GRU008', 'bedsz', 'gfqra', '', NULL, 10883.8, '<p><strong>Creed&nbsp;</strong>Love In White Eau de Parfum<br />Notes: Orange Zest from Italy, Florence Iris, French Daffodil, Magnolia, Vanilla, Ambergris, Sandalwood from Mysore<br />Available in 30ml, 75ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:18:51', '2020-08-30 17:48:51'),
(107, 'qbycn', 'vrbsh', 'vepgo', 'Spring Flower eau de parfum', 'GRU009', 'bedsz', 'gfqra', '', NULL, 10883.8, '', '', '', 0, 1, '', NULL, NULL, 0, 'GRU009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:21:40', '2020-08-30 17:51:40'),
(108, 'kypnw', 'vrbsh', 'vepgo', 'Spring Flower eau de parfum', 'GRU0010', 'bedsz', 'gfqra', '', NULL, 10883.8, '', '', '', 0, 1, '', NULL, NULL, 1, 'GRU009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:23:18', '2020-08-30 17:53:18'),
(109, 'ocinq', 'vrbsh', 'nxzhs', 'Houndstooth Wool and Silk-Blend Suit Jacket', 'PR007', 'saqem', 'ahyxc', '', NULL, 214430, '<p>The success of a suit begins and ends with the materials used. Brunello Cucinelli\'s houndstooth jacket has been tailored in Italy from a pure wool and silk-blend of the utmost calibre. The partial lining makes it comfortably light and flexible.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263288\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1248637\">Canali Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1220480\">J.M. Weston Loafers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1210826\">Dunhill Pouch</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This blazer is tailored for a regular cut</li>\r\n<li>Lightweight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Jacket Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Chest:&nbsp;100cm&nbsp;/ 39.4in</li>\r\n<li>Back Length:&nbsp;74cm&nbsp;/ 29.1in</li>\r\n<li>Sleeve Length:&nbsp;85.5cm&nbsp;/ 33.7in</li>\r\n<li>Shoulder Width:&nbsp;45.5cm&nbsp;/ 17.9in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:31:37', '2020-09-04 06:56:00'),
(110, 'hwizj', 'vrbsh', 'nxzhs', 'Houndstooth Wool and Silk-Blend Suit Jacket', 'PR008', 'saqem', 'ahyxc', '', NULL, 214430, '<p>The success of a suit begins and ends with the materials used. Brunello Cucinelli\'s houndstooth jacket has been tailored in Italy from a pure wool and silk-blend of the utmost calibre. The partial lining makes it comfortably light and flexible.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263288\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1248637\">Canali Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1220480\">J.M. Weston Loafers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1210826\">Dunhill Pouch</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This blazer is tailored for a regular cut</li>\r\n<li>Lightweight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Jacket Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Chest:&nbsp;100cm&nbsp;/ 39.4in</li>\r\n<li>Back Length:&nbsp;74cm&nbsp;/ 29.1in</li>\r\n<li>Sleeve Length:&nbsp;85.5cm&nbsp;/ 33.7in</li>\r\n<li>Shoulder Width:&nbsp;45.5cm&nbsp;/ 17.9in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(111, 'vsjnu', 'vrbsh', 'nxzhs', 'Houndstooth Wool and Silk-Blend Suit Jacket', 'PR009', 'saqem', 'ahyxc', '', NULL, 214430, '<p>The success of a suit begins and ends with the materials used. Brunello Cucinelli\'s houndstooth jacket has been tailored in Italy from a pure wool and silk-blend of the utmost calibre. The partial lining makes it comfortably light and flexible.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263288\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1248637\">Canali Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1220480\">J.M. Weston Loafers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1210826\">Dunhill Pouch</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This blazer is tailored for a regular cut</li>\r\n<li>Lightweight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Jacket Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Chest:&nbsp;100cm&nbsp;/ 39.4in</li>\r\n<li>Back Length:&nbsp;74cm&nbsp;/ 29.1in</li>\r\n<li>Sleeve Length:&nbsp;85.5cm&nbsp;/ 33.7in</li>\r\n<li>Shoulder Width:&nbsp;45.5cm&nbsp;/ 17.9in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(112, 'ogpxw', 'vrbsh', 'vepgo', 'Gold-toned leather atomiser 50ml', 'GRU0011', 'bedsz', 'gfqra', '', NULL, 13495.9, '<p><strong>Creed&nbsp;</strong>leather perfume atomiser<br />Gold-toned, logo embossed, refillable<br />Notes: Compatible with splash fragrances in 250ml and 500ml<br />50ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:35:03', '2020-08-30 18:05:03'),
(113, 'xfsho', 'vrbsh', 'nxzhs', 'Houndstooth Wool and Silk-Blend Suit Jacket', 'PR0010', 'saqem', 'ahyxc', '', NULL, 214430, '<p>The success of a suit begins and ends with the materials used. Brunello Cucinelli\'s houndstooth jacket has been tailored in Italy from a pure wool and silk-blend of the utmost calibre. The partial lining makes it comfortably light and flexible.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263288\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1248637\">Canali Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1220480\">J.M. Weston Loafers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1210826\">Dunhill Pouch</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This blazer is tailored for a regular cut</li>\r\n<li>Lightweight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Jacket Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Chest:&nbsp;100cm&nbsp;/ 39.4in</li>\r\n<li>Back Length:&nbsp;74cm&nbsp;/ 29.1in</li>\r\n<li>Sleeve Length:&nbsp;85.5cm&nbsp;/ 33.7in</li>\r\n<li>Shoulder Width:&nbsp;45.5cm&nbsp;/ 17.9in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(114, 'mulfk', 'vrbsh', 'nxzhs', 'Houndstooth Wool and Silk-Blend Suit Jacket', 'PR0011', 'saqem', 'ahyxc', '', NULL, 214430, '<p>The success of a suit begins and ends with the materials used. Brunello Cucinelli\'s houndstooth jacket has been tailored in Italy from a pure wool and silk-blend of the utmost calibre. The partial lining makes it comfortably light and flexible.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263288\">Brunello Cucinelli Trousers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1248637\">Canali Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1220480\">J.M. Weston Loafers</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1210826\">Dunhill Pouch</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This blazer is tailored for a regular cut</li>\r\n<li>Lightweight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Jacket Waist:&nbsp;94cm&nbsp;/ 37in</li>\r\n<li>Chest:&nbsp;100cm&nbsp;/ 39.4in</li>\r\n<li>Back Length:&nbsp;74cm&nbsp;/ 29.1in</li>\r\n<li>Sleeve Length:&nbsp;85.5cm&nbsp;/ 33.7in</li>\r\n<li>Shoulder Width:&nbsp;45.5cm&nbsp;/ 17.9in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(115, 'ntvxu', 'vrbsh', 'nxzhs', '7cm MÃ©lange Virgin Wool Tie', 'PR0012', 'lcgpa', 'pcyzr', '', NULL, 16494.6, '<p>Brunell</p>\r\n<p>This item\'s measurements are:</p>\r\n<ul>\r\n<li>Length:&nbsp;149.5cm&nbsp;/ 58.9in</li>\r\n<li>Width:&nbsp;7cm&nbsp;/ 2.8in</li>\r\n</ul>\r\n<p>o Cucinelli\'s tie has been crafted in Italy from virgin wool in varying shades of grey that create a subtle checked pattern from afar. It\'s lined for a smooth drape and has a 7cm blade that works with most blazers.</p>\r\n<p>&nbsp;</p>\r\n<p>Shown here with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/429968\">Charvet Shirt</a>.</p>', '<p>This item\'s measurements are:</p>\r\n<ul>\r\n<li>Length:&nbsp;149.5cm&nbsp;/ 58.9in</li>\r\n<li>Width:&nbsp;7cm&nbsp;/ 2.8in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:47:29', '2020-09-03 18:54:52'),
(116, 'jxuov', 'vrbsh', 'yluqx', 'Baby Sac de Jour croc-embossed leather tote bag', 'GRU0012', 'fancy', 'xrlas', '', NULL, 164998, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent&nbsp;</strong>leather tote bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Open top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Glossy croc-embossed, accordion design, twin handles, optional shoulder strap, brand-embossed luggage tab, protective feet, foil branding, one main compartment.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 21cm, width 26cm, depth 13cm, handle drop 10cm, strap drop 62cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(117, 'uilqw', 'vrbsh', 'nxzhs', 'Burnished-Leather Carry-On Suitcase', 'PR0013', 'fancy', 'vuotm', '', NULL, 365974, '<p>This carry-on suitcase is exemplary of Brunello Cucinelli\'s top-notch craftsmanship and attention to detail. Made from burnished-leather that\'ll develop a nice patina over time, it has two fully lined internal compartments and plenty of organisational pockets. The spinner wheels and telescopic handle are particularly handy for navigating busy terminals.</p>\r\n<p>&nbsp;</p>\r\n<p>Shown here with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1247980\">TOM FORD Sweater</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1208503\">Brunello Cucinelli Jeans</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1213552\">TOM FORD Sneakers</a>.</p>', '<p>This item\'s measurements are:</p>\r\n<ul>\r\n<li>Handle Length:&nbsp;6cm&nbsp;/ 2.4in</li>\r\n<li>Height:&nbsp;48cm&nbsp;/ 18.9in</li>\r\n<li>Width:&nbsp;37cm&nbsp;/ 14.6in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 0, 'PR0013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(118, 'ldgex', 'vrbsh', 'yluqx', 'Teddy leather bucket bag', 'GRU0013', 'fancy', 'ihqtv', '', NULL, 50000, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather bucket bag</li>\r\n<li>Drawstring closure</li>\r\n<li>suede wallet, leather handles, chain and leather strap, silver-toned metal hardware, logo-embossed</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Made in Italy</li>\r\n<li>Height 34cm, width 35cm, depth 15.5cm, strap drop 38cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(119, 'vtrhc', 'vrbsh', 'nxzhs', 'Burnished Full-Grain Leather Billfold Wallet', 'PR0015', 'lcgpa', 'fmula', '', NULL, 36082, '<p>Whether you\'re a cash or card man, Brunello Cucinelli\'s billfold wallet will ensure you settle up in style. It\'s made from dark-tan burnished-leather and equipped with sleeves and pockets to secure all your payment essentials.</p>', '<p>This item\'s measurements are:</p>\r\n<ul>\r\n<li>Height:&nbsp;9cm&nbsp;/ 3.5in</li>\r\n<li>Width:&nbsp;11cm&nbsp;/ 4.3in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:19:02', '2020-09-03 19:15:00'),
(120, 'rhmox', 'vrbsh', 'yluqx', 'Logo-print nylon belt bag', 'GRU0015', 'fancy', 'kxryd', '', NULL, 45711.9, '<p><strong>Saint Laurent</strong>&nbsp;nylon belt bag<br />Zip fastening<br />Adjustable waist strap, leather trim with silver-toned embossed logo, single compartment with slip pocket, three card slots, silver-toned hardware<br />Main: 100% nylon<br />Use specialist cleaner<br />Height 13cm, width 35cm, depth 10cm, strap drop 35cm</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(121, 'cqzkf', 'vrbsh', 'yluqx', 'Leopard-print wool-blend coat', 'GRU0016', 'saqem', 'gjvqf', '', NULL, 152809, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;wool-blend coat</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>73% wool, 15% mohair, 1% polyamide, 1% elastane</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed button fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Notch lapels, dropped shoulders, long sleeves, branded buttons, all-over leopard print pattern</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Size small: length 34in / 86cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(122, 'wjqsl', 'vrbsh', 'nxzhs', 'Cotton-Corduroy Trousers', 'PR0016', 'saqem', 'mdrvj', '', NULL, 54638.4, '<p>All of Brunello Cucinelli\'s pieces are crafted in the brand\'s Solomeo, Italy workshop to ensure impeccable quality. Tailored from fine-wale cotton-corduroy, these grey pants have a tapered shape defined by neatly pressed creases. Their minimal design means they can easily be dressed up or down for most occasions.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263315\">Brunello Cucinelli Shirt</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1269807\">Brunello Cucinelli Sneakers</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Tapered leg, mid-rise style</li>\r\n<li>Mid-weight, slightly stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: waist size 32, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Outside Leg:&nbsp;113cm&nbsp;/ 44.5in</li>\r\n<li>Inside_Leg:&nbsp;89cm&nbsp;/ 35in</li>\r\n<li>Trouser Waist:&nbsp;84cm&nbsp;/ 33.1in</li>\r\n<li>Rise:&nbsp;25cm&nbsp;/ 9.8in</li>\r\n<li>Leg Opening:&nbsp;30cm&nbsp;/ 11.8in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:32:00', '2020-09-04 05:12:58'),
(123, 'dgjia', 'vrbsh', 'nxzhs', 'Cotton-Corduroy Trousers', 'PR0017', 'saqem', 'mdrvj', '', NULL, 54638.4, '<p>All of Brunello Cucinelli\'s pieces are crafted in the brand\'s Solomeo, Italy workshop to ensure impeccable quality. Tailored from fine-wale cotton-corduroy, these grey pants have a tapered shape defined by neatly pressed creases. Their minimal design means they can easily be dressed up or down for most occasions.</p>\r\n<p>&nbsp;</p>\r\n<p>Wear it with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1263315\">Brunello Cucinelli Shirt</a>,&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1269807\">Brunello Cucinelli Sneakers</a>.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>Tapered leg, mid-rise style</li>\r\n<li>Mid-weight, slightly stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: waist size 32, height 6\'1\"/ 185cm</li>\r\n</ul>\r\n<ul>\r\n<li>Outside Leg:&nbsp;113cm&nbsp;/ 44.5in</li>\r\n<li>Inside_Leg:&nbsp;89cm&nbsp;/ 35in</li>\r\n<li>Trouser Waist:&nbsp;84cm&nbsp;/ 33.1in</li>\r\n<li>Rise:&nbsp;25cm&nbsp;/ 9.8in</li>\r\n<li>Leg Opening:&nbsp;30cm&nbsp;/ 11.8in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:36:53', '2020-08-30 19:06:53'),
(124, 'florn', 'vrbsh', 'yluqx', 'Leopard-print wool-blend coat', 'GRU0017', 'saqem', 'gjvqf', '', NULL, 152809, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;wool-blend coat</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>73% wool, 15% mohair, 1% polyamide, 1% elastane</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed button fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Notch lapels, dropped shoulders, long sleeves, branded buttons, all-over leopard print pattern</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Size small: length 34in / 86cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(125, 'cqpzk', 'vrbsh', 'yluqx', 'Mickey Mouse-print cotton-jersey T-shirt', 'GRU0018', 'saqem', 'ygtuj', '', NULL, 24815.1, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\"><strong>Saint Laurent</strong>&nbsp;cotton-jersey T-shirt<br /><br /><strong>Details:</strong><br />Round neck<br />Ribbed trim at neck<br />Short sleeves<br />Graphic print at front<br />Stepped hem<br />Slips on<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />100% cotton<br />Lightweight, stretch-jersey<br /><br /><strong>Size &amp; Fit:</strong><br />Size small: Length 27in / 69cm<br />True to size<br />Cut to a classic T-shirt fit<br /><br /><strong>Care:</strong><br />Machine Wash</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:54:19', '2020-08-30 19:24:19'),
(126, 'hqzdm', 'vrbsh', 'nxzhs', 'Suede-Trimmed Burnished Full-Grain Leather Backpack', 'PR0018', 'fancy', 'rihfu', '', NULL, 205094, '<p>Brunello Cucinelli\'s commitment to fine fabrics and artful craftsmanship ensures this backpack is as practical as it is smart. Assembled in the label\'s Solomeo workshop from supple burnished full-grain leather and finished with a tonal suede back panel, it unzips to a spacious interior and has two side pockets for keeping essentials within easy reach.</p>\r\n<p>&nbsp;</p>\r\n<p>Shown here with:&nbsp;<a href=\"https://www.mrporter.com/en-in/mens/product/1208489\">Brunello Cucinelli Trousers</a>.</p>', '<p>This item\'s measurements are:</p>\r\n<ul>\r\n<li>Handle Length:&nbsp;8cm&nbsp;/ 3.1in</li>\r\n<li>Height:&nbsp;44cm&nbsp;/ 17.3in</li>\r\n<li>Width:&nbsp;32cm&nbsp;/ 12.6in</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 07:56:46', '2020-09-04 06:41:12'),
(127, 'epuaj', 'vrbsh', 'yluqx', 'Mickey Mouse-print cotton-jersey T-shirt', 'GRU0019', 'saqem', 'ygtuj', '', NULL, 24815.1, '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected  o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\"><strong>Saint Laurent</strong>&nbsp;cotton-jersey T-shirt<br /><br /><strong>Details:</strong><br />Round neck<br />Ribbed trim at neck<br />Short sleeves<br />Graphic print at front<br />Stepped hem<br />Slips on<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />100% cotton<br />Lightweight, stretch-jersey<br /><br /><strong>Size &amp; Fit:</strong><br />Size small: Length 27in / 69cm<br />True to size<br />Cut to a classic T-shirt fit<br /><br /><strong>Care:</strong><br />Machine Wash</div>\r\n</article>\r\n</section>\r\n</section>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(128, 'rsbah', 'vrbsh', 'nxzhs', 'Double-Breasted Wool and Cashmere-Blend Herringbone Coat', 'PR0019', 'saqem', 'gjvqf', '', NULL, 592608, '<p>While the buttons of Mr Brunello Cucinelli\'s coat are engraved with the designer\'s name, his signature techniques can be found in the relaxed silhouette and softness of the herringbone wool and cashmere-blend. It\'s been tailored at the atelier in Solomeo, lightly structured with a flexible butterfly lining.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This coat is designed for a slightly loose fit</li>\r\n<li>Mid-weight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'2\"/ 188cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:08:47', '2020-08-31 07:44:54'),
(129, 'wvbdy', 'vrbsh', 'yluqx', 'Naima square-toe leather sandals', 'GRU0020', 'nbawm', 'erblu', '', NULL, 54419, '<ul>\r\n<ul>\r\n<li><strong>Amina Muaddi</strong>&nbsp;leather sandals</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% lamb leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square toe, slender elasticated straps, flared heel</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Heel height 3.7\"</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather cleaner</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Upper: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Sole: 100% leather</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(130, 'kdinu', 'vrbsh', 'yluqx', 'Naima square-toe leather sandals', 'GRU0021', 'nbawm', 'erblu', '', NULL, 54419, '<ul>\r\n<ul>\r\n<li><strong>Amina Muaddi</strong>&nbsp;leather sandals</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% lamb leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square toe, slender elasticated straps, flared heel</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Heel height 3.7\"</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather cleaner</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Upper: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Sole: 100% leather</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(131, 'sgyhq', 'vrbsh', 'nxzhs', 'Double-Breasted Wool and Cashmere-Blend Herringbone Coat', 'PR0020', 'saqem', 'gjvqf', '', NULL, 592608, '<p>While the buttons of Mr Brunello Cucinelli\'s coat are engraved with the designer\'s name, his signature techniques can be found in the relaxed silhouette and softness of the herringbone wool and cashmere-blend. It\'s been tailored at the atelier in Solomeo, lightly structured with a flexible butterfly lining.</p>', '<ul>\r\n<li>Fits true to size. Take your normal size</li>\r\n<li>This coat is designed for a slightly loose fit</li>\r\n<li>Mid-weight, non-stretchy fabric</li>\r\n<li>Model wears an IT 48</li>\r\n<li>Model measures: chest 38\"/ 96cm, height 6\'2\"/ 188cm</li>\r\n</ul>', '', 0, 1, '', NULL, NULL, 1, 'PR0019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:16:27', '2020-08-31 07:45:16'),
(132, 'hxbsi', 'vrbsh', 'mgrjf', 'MacBook Pro 16', 'GRU0022', 'njchx', 'ckmvp', '', NULL, 208882, '<p><strong>Performance that goes beyond.</strong><br />The 16-inch MacBook Pro brings a whole new class of performance to the notebook. Thanks to a more advanced thermal design, the Intel Core i9 processor with up to 8 cores and 16 threads of processing power sustains higher performance for longer periods of time &mdash; and delivers up to 2.1 times the performance of a quad-core MacBook Pro. So whether you&rsquo;re layering dozens of tracks and effects, rendering 3D models, or compiling and testing code, you&rsquo;ll be doing it in no time flat.<br /><br /><strong>A big, beautiful workspace. For big, beautiful work.</strong><br />The new MacBook Pro features a stunning 16-inch Retina display &mdash; the largest Retina display ever in a Mac notebook. It produces 500 nits of brightness for spectacular highlights and bright whites, while delivering deep blacks thanks to the precise photo alignment of liquid crystal molecules. The P3 wide colour gamut enables brilliant, true-to-life images and video. So no matter where you are, you&rsquo;ll see your work in the best possible light.<br /><br /><strong>Working at the speed of thought.</strong><br />Thanks to a more advanced thermal design, the Intel Core i9 processor with up to 8 cores and 16 threads of processing power sustains higher performance for longer periods of time &mdash; and delivers up to 2.1 times the performance of a quad-core MacBook Pro. So whether you&rsquo;re layering dozens of tracks and effects, rendering 3D models, or compiling and testing code, you&rsquo;ll be doing it in no time flat.<br /><br /><strong>Graphics that bend reality.</strong><br />The AMD Radeon Pro 5000M series GPU delivers the most graphics horsepower ever in a MacBook Pro. The 16-inch MacBook Pro base model is over two times faster than the previous-generation base model, for seamless playback and faster rendering of ultra-high-definition video. And with the optional 8GB of GDDR6 VRAM, you&rsquo;ll get up to 80 per cent faster performance when executing tasks like colour grading in DaVinci Resolve compared with the Radeon Pro Vega 20.</p>', '', '<p>Audio features: Highâ€‘fidelity sixâ€‘speaker system with forceâ€‘cancelling woofers<br />Brightness: 500 nits brightness<br />Colours supported: support for millions of colours<br />Connectivity: Four Thunderbolt 3 (USBâ€‘C) ports, headphone jack<br />Graphics: AMD Radeon Pro 5300M with 4GB of GDDR6 memory<br />Hard drive: 512GB SSD<br />Memory (RAM): 16GB of 2666MHz DDR 4<br />Ports and interfaces: Four Thunderbolt 3 (USBâ€‘C) ports<br />Power supply: 96W USBâ€‘C Power Adapter<br />Processor: 2.6GHz 6â€‘core Intel Core i7, Turbo Boost up to 4.5GHz, with 12MB shared L3 cache<br />Screen resolution: 3072x1920 native resolution<br />Screen size: 16â€‘inch (diagonal) LEDâ€‘backlit display<br />Size (unpackaged): 16.2mm, 357.9mm, 245.9mm<br />Weight (unpackaged): 2.0kg<br />What\'s in the box: 16â€‘inch MacBook Pro, 96W USBâ€‘C Power Adapter, USBâ€‘C Charge Cable (2m)</p>', 0, 1, '', NULL, NULL, 1, 'GRU0022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:39:30', '2020-08-30 20:14:45'),
(133, 'hugvf', 'vrbsh', 'mgrjf', 'iPhone 11 Yellow', 'PR0021', 'njchx', 'syodq', '', NULL, 63460, '<ul>\r\n<li><strong>Apple</strong>&nbsp;iPhone 11 64GB Yellow</li>\r\n<li>6.1-inch Liquid Retina HD LCD display3</li>\r\n<li>Water and dust resistant (2 metres for up to 30 minutes, IP68)1</li>\r\n<li>Dual-camera system with 12MP Ultra Wide and Wide cameras, Night mode, Portrait mode, and 4K video up to 60 fps</li>\r\n<li>12MP TrueDepth front camera with Portrait mode, 4k video and slo-mo1</li>\r\n<li>Face ID for secure authentication and Apple Pay</li>\r\n<li>A13 Bionic chip with third-generation Neural Engine</li>\r\n<li>Fast-charge capable</li>\r\n<li>Wireless charging</li>\r\n<li>iOS 13 with Dark Mode, new tools for editing photos and video, and brand-new privacy features</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:47:41', '2020-08-30 20:18:36'),
(134, 'obnxd', 'vrbsh', 'mgrjf', 'iPhone 11 Yellow', 'PR0022', 'njchx', 'syodq', '', NULL, 63460, '<ul>\r\n<li><strong>Apple</strong>&nbsp;iPhone 11 64GB Yellow</li>\r\n<li>6.1-inch Liquid Retina HD LCD display3</li>\r\n<li>Water and dust resistant (2 metres for up to 30 minutes, IP68)1</li>\r\n<li>Dual-camera system with 12MP Ultra Wide and Wide cameras, Night mode, Portrait mode, and 4K video up to 60 fps</li>\r\n<li>12MP TrueDepth front camera with Portrait mode, 4k video and slo-mo1</li>\r\n<li>Face ID for secure authentication and Apple Pay</li>\r\n<li>A13 Bionic chip with third-generation Neural Engine</li>\r\n<li>Fast-charge capable</li>\r\n<li>Wireless charging</li>\r\n<li>iOS 13 with Dark Mode, new tools for editing photos and video, and brand-new privacy features</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 08:49:36', '2020-08-31 10:42:29');
INSERT INTO `product` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `status`, `weight`, `img`, `type`, `groupid`, `product_code`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `styleno`, `hsncode`, `created_at`, `updated_at`) VALUES
(135, 'omgkd', 'vrbsh', 'mgrjf', 'iPhone 11 Pro 256GB Space Grey', 'PR0023', 'njchx', 'syodq', '', NULL, 104374, '<ul>\r\n<li><strong>Please note:</strong>&nbsp;Battery life varies by use and configuration</li>\r\n<li>iPhone 11 Pro and iPhone 11 Pro Max are splash, water and dust resistant and were tested under controlled laboratory conditions; iPhone 11 Pro and iPhone 11 Pro Max have a rating of IP68 under IEC standard 60529 (maximum depth of 4 metres up to 30 minutes). Splash, water and dust resistance are not permanent conditions and resistance might decrease as a result of normal wear. Do not attempt to charge a wet iPhone; refer to the user guide for cleaning and drying instructions. Liquid damage not covered under warranty</li>\r\n<li>The display has rounded corners. When measured as a rectangle, the iPhone 11 Pro screen is 5.85 inches diagonally and the iPhone 11 Pro Max screen is 6.46 inches diagonally. Actual viewable area is less</li>\r\n<li>Qi wireless chargers sold separately</li>\r\n</ul>', '', '<p class=\"c-tabs__header\">Specifications</p>\r\n<ul>\r\n<li><strong>Apple</strong>&nbsp;iPhone 11 Pro 256GB</li>\r\n<li>5.8-inch Super Retina XDR OLED display</li>\r\n<li>Water and dust resistant (4 metres for up to 30 minutes, IP68)</li>\r\n<li>Triple-camera system with 12MP Ultra Wide, Wide and Telephoto cameras, Night mode, Portrait mode, and 4K video up to 60 fps</li>\r\n<li>12MP TrueDepth front camera with Portrait mode, 4K video and slo-mo</li>\r\n<li>Face ID for secure authentication and Apple Pay</li>\r\n<li>A13 Bionic chip with third-generation Neural Engine</li>\r\n<li>Fast charge with 18W adapter included</li>\r\n<li>Wireless charging</li>\r\n<li>iOS 13 with Dark Mode, new tools for editing photos and video, and brand-new privacy features</li>\r\n</ul>', 0, 1, '', NULL, NULL, 1, 'PR0023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 09:05:04', '2020-08-31 09:56:06'),
(136, 'etcar', 'vrbsh', 'mgrjf', 'iPad Pro 12.9\" WiFi 64GB Silver', 'GRU0023', 'njchx', 'cjwrx', '', NULL, 84371.2, '<div class=\"c-tabs__copy\"><strong>It&rsquo;s all new, all screen and all powerful</strong><br />Completely redesigned and packed with Apple&rsquo;s most advanced technology, it will make you rethink what an iPad is capable of.</div>', '', '<section class=\"c-product-info__tabs c-tabs\" data-component=\"Tabs\">\r\n<section role=\"region\" aria-live=\"polite\">\r\n<article id=\"content1\" class=\"c-tabs__content --selected --two-col o-fs-p3\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<ul>\r\n<li><strong>Apple</strong>&nbsp;iPad Pro 12.9\"</li>\r\n<li>Audio calling: iPad to any FaceTime-enabled device over Wi-Fi or a mobile network</li>\r\n<li>Battery duration (please note - reasonable usage): Up to 10 hours of surfing the web on Wi-Fi, watching video or listening to music</li>\r\n<li>Camera resolution: 12MP</li>\r\n<li>Camera type: f/1.8 aperture&nbsp;</li>\r\n<li>Display brightness: 600 nits brightness</li>\r\n<li>Display resolution: 2732x2048</li>\r\n<li>Display size: 12.9\"</li>\r\n<li>Microphones: Five microphones for calls, video recording and audio recording</li>\r\n<li>Mobile networking: WiFi</li>\r\n<li>Operating System (OS): iOS 12</li>\r\n<li>Ports: USB-C</li>\r\n<li>Power supply: 18W USB-C</li>\r\n<li>Processor/chip: A12X Bionic chip with 64bit architecture</li>\r\n<li>Sensors: Face ID, Three-axis gyro, Accelerometer, Barometer, Ambient light sensor</li>\r\n<li>Size (unpackaged): 28.1cm, 21.5cm, 0.59cm</li>\r\n<li>Smart Assistant: Siri Enabled</li>\r\n<li>Speakers: Four-speaker Audio</li>\r\n<li>Video calling: iPad to any FaceTime-enabled device over Wi-Fi</li>\r\n<li>Video resolution: 4K video recording at 30 fps or 60 fps</li>\r\n<li>Weight (unpackaged): 633 grams&nbsp;</li>\r\n<li>What\'s in the box: iPad Pro, USB-C Charge Cable (1m), 18W USB-C Power Adapter</li>\r\n</ul>\r\n</div>\r\n</article>\r\n<article id=\"content2\" class=\"c-tabs__content\" role=\"tabpanel\">\r\n<div class=\"c-tabs__copy\">\r\n<div class=\"c-delivery-left\">\r\n<div class=\"c-delivery-left__inner\">\r\n<div>\r\n<h4 data-ts-title=\"UK DELIVERY\">&nbsp;</h4>\r\n<div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n</div>\r\n<div>\r\n<h4 data-ts-title=\"INTERNATIONAL DELIVERY\">&nbsp;</h4>\r\n<p>&nbsp;</p>\r\n<div>\r\n<div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</article>\r\n</section>\r\n</section>', 0, 1, '', NULL, NULL, 0, 'GRU0023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 09:05:25', '2020-08-30 20:35:25'),
(137, 'ipnhf', 'vrbsh', 'yluqx', 'Betty Havana square-frame sunglasses', 'GRU0024', 'lcgpa', 'bnxkh', '', NULL, 24815.1, '<p><strong>Saint Laurent</strong>&nbsp;acetate sunglasses<br />Square Havana frame, gradient tinted lenses, branding<br />Frame width: 14cm<br />Soft case<br />Made in Italy</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(138, 'eokpf', 'vrbsh', 'yoahe', 'Logo-Engraved Palladium-Plated Cufflinks', 'PR0024', 'lcgpa', 'dmfcj', '', NULL, 58236.9, '<p><a class=\"\" href=\"https://www.mrporter.com/en-in/mens/Designers/Berluti\">Berluti</a>\'s silver cufflinks are palladium-plated so they\'ll last longer and retain their lustre. They\'re engraved with the label\'s moniker and year of foundation, and come with a presentation case in the style of old wax boxes.</p>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 09:25:47', '2020-09-04 05:17:38'),
(139, 'tfnde', 'vrbsh', 'yluqx', 'Logo-plaque leather bracelet', 'PR0025', 'zisqv', 'lpvuo', '', NULL, 17410.1, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather bracelet</li>\r\n<li>Magnetic clasp closure</li>\r\n<li>Metal &lsquo;YSL&rsquo; logo, engraved clasp</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Width: 1cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(140, 'pthxc', 'vrbsh', 'yluqx', 'Marine pendant necklace', 'DIM008', 'zisqv', 'wvbly', '', NULL, 18004, '<p><strong>Materials:</strong><br />Silver-toned brass<br /><br /><strong>Product Details:</strong><br />Lobster claw closure<br />Curb chain<br />Silver-toned marine pendant<br />Engraved branding label<br />Chain length 49cm<br /><br /><strong>Care:</strong><br />Wipe with a soft, dry cloth<br /><br /><strong>Country of Origin:</strong><br />Made in Italy</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 18:20:25', '2020-08-31 05:50:25'),
(141, 'vcgux', 'vrbsh', 'yluqx', 'Heart-print semi-sheer jacquard mini dress', 'DIM009', 'saqem', 'vktjn', '', NULL, 163511, '<p><strong>Details:</strong><br />Round neck<br />Long raglan sleeves<br />Semi-sheer panels at shoulders and sleeves<br />Buttoned cuffs<br />All-over heart jacquard pattern<br />Draped at waistband<br />Buttons at back neck<br />Keyhole at back<br />Fully lined<br />Concealed zip fastening at side<br />Made in France<br /><br /><strong>Fabric:</strong><br />Main: 68% viscose, 32% silk<br />Lining: 100% silk<br />Heavyweight, slight stretch<br /><br /><strong>Size &amp; Fit:</strong><br />Size UK 8: Length 33in / 84cm<br />True to size<br />Cut to an H-line silhouette, straight through the body with a slightly emphasised waist<br /><br /><strong>Care:</strong><br />Dry clean</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(142, 'fkzpg', 'vrbsh', 'cheoa', 'Orange and bergamot shower gel 300ml', 'DIM10', 'bedsz', 'qpfel', '', NULL, 2148, '<p><strong>Molton Brown</strong>&nbsp;shower gel<br />Top note: orange, bergamot and mandarin<br />Heart note: neroli, cardamom and galbanum.<br />Base notes: musk, ylang-ylang and cedarwood<br />Vegan, paraben-free, gluten-free and cruelty-free<br />300ml</p>', '', '', 0, 1, '', NULL, NULL, 1, 'DIM10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-08-31 23:57:07', '2020-08-31 11:33:01'),
(143, 'dpizv', 'vrbsh', 'cheoa', 'Oudh accord and gold shower gel 300ml', 'DIM11', 'bedsz', 'qpfel', '', NULL, 2344, '<p><strong>Molton Brown</strong>&nbsp;bath and shower gel<br />Top notes: cinnamon leaf, nutmeg and bergamot<br />Heart notes: elemi, myrrh and black tea<br />Base notes: oudh, vetiver and honey<br />Vegan, paraben-free, gluten-free and cruelty-free<br />300ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(144, 'vbwpd', 'vrbsh', 'cheoa', 'Fiery Pink Pepper liquid hand wash 300ml', 'DIM12', 'bedsz', 'qpfel', '', NULL, 1953, '<p><strong>Molton Brown</strong>&nbsp;Fiery Pink Pepper liquid hand wash<br />Top notes: Pink pepper, tangerine, elemi oil<br />Heart notes: Nutmeg, ginger<br />Base notes: Patchouli, cedarwood<br />300ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(145, 'wvxcr', 'vrbsh', 'cheoa', 'Cyprus & Sea Fennel liquid hand lotion ', 'DIM0013', 'bedsz', 'gwtef', '', NULL, 2149, '<p>Molton Brown liquid hand lotion<br />Top notes: fig leaves, cardamom and bergamot.<br />Heart notes: marine notes, jasmine and violet leaf.<br />Base notes: salted cypress, cedarwood and musks.<br />Vegan, paraben-free, gluten-free and cruelty-free<br />300ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM0013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(146, 'vzpsc', 'vrbsh', 'cheoa', 'Fiery Pink Pepper liquid hand lotion 300ml', 'DIM14', 'bedsz', 'gwtef', '', NULL, 2150, '<p>Molton Brown liquid hand lotion<br />Top notes: pink pepper, tangerine and elemi.<br />Heart notes: nutmeg, ginger and jasmine.<br />Base notes: patchouli, cedarwood and oakmoss.<br />Vegan, paraben-free, gluten-free and cruelty-free<br />300ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(147, 'brfhx', 'vrbsh', 'cheoa', 'Flora Luminare eau de parfum 100ml', 'DIM15', 'bedsz', 'gfqra', '', NULL, 10751, '<p><br />Molton Brown eau de parfum<br />Top notes: almond bitter, mandarin, petitgrain and olibanum.<br />Heart notes: absolute, neroli, jasmine absolute and rose absolute.<br />Base notes: earthy mate absolute, sandalwood, cedarwood and musk.<br />Vegan, paraben-free, gluten-free and cruelty-free<br />100ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DIM15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 01:21:28', '2020-08-31 12:51:28'),
(148, 'syrzq', 'vrbsh', 'cheoa', 'Orange & Bergamot eau de parfum 100ml', 'DMI17', 'bedsz', 'gfqra', '', NULL, 10751, '<p><br />Molton Brown eau de parfum<br />Top notes: bitter orange, sweet orange, bergamot and galbanum.<br />Heart notes: orange blossom, rose petals, ylang-ylang and clove.<br />Base notes: musk, sandalwood, vetiver and Peruvian balsam<br />Vegan, paraben-free, gluten-free and cruelty-free<br />100ml</p>', '', '', 0, 1, '', NULL, NULL, 0, 'DMI17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 01:28:18', '2020-08-31 12:58:18'),
(149, 'xabpk', 'vrbsh', 'gtsyz', 'Katrine square-frame sunglasses', 'PR0026', 'lcgpa', 'bnxkh', '', NULL, 25375, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;sunglasses</li>\r\n<li>ft0617</li>\r\n<li>Havana square-frame, brown lenses, \'T\' logo at temples, brand-embossed tips</li>\r\n<li>100% UV protection</li>\r\n<li>Frame width: 14.5cm</li>\r\n<li>Soft case</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(150, 'zthlw', 'vrbsh', 'gtsyz', 'Reversible T logo leather belt', 'GRU0025', 'lcgpa', 'ljcnx', '', NULL, 42866, '<ul>\r\n<li>Tom Ford leather belt</li>\r\n<li>Buckle fastening</li>\r\n<li>Silver-toned logo buckle, grained leather, reversible</li>\r\n<li>Adjustable fitting</li>\r\n<li>Width 4cm</li>\r\n<li>Made in Italy</li>\r\n<li>Use specialist cleaner</li>\r\n<li>Tom Ford belts run small, we suggest you go two sizes up your usual size</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 06:07:39', '2020-08-31 17:37:39'),
(151, 'qbdkf', 'vrbsh', 'gtsyz', 'Logo leather billfold wallet', 'GRU0026', 'lcgpa', 'fmula', '', NULL, 31500, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford&nbsp;</strong>leather billfold wallet</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Smooth leather, embossed T logo, gold-toned hardware, eight card slots, central note compartment</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 11.4cm, width 9.4cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 0, '', NULL, NULL, 0, 'GRU0026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(152, 'oxuhm', 'vrbsh', 'yluqx', 'Toy North/South monogrammed suede tote bag', 'PR0027', 'fancy', 'xrlas', '', NULL, 106907, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;shoulder bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Magnetic press-stud closure</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Twin top handles, detachable and adjustable leather shoulder strap, single compartment, gold-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 27cm, width 26cm, depth 12cm, handle drop 15cm, strap drop 45cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 06:46:35', '2020-09-01 18:38:46'),
(153, 'iesgh', 'vrbsh', 'gtsyz', 'T-Line leather money clip wallet', 'GRU0027', 'lcgpa', 'fmula', '', NULL, 33250, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;leather money clip wallet</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Bifold design, smooth leather, embossed T logo, gold-toned hardware, ten card slots</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 9cm, width 12cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(154, 'wgkub', 'vrbsh', 'gtsyz', 'Ft5518 butterfly-frame optical glasses', 'PR0028', 'lcgpa', 'bnxkh', '', NULL, 20125, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;optical glasses</li>\r\n<li>Butterfly-frame, clear lenses, pale gold-toned hardware, tortoiseshell coated tips, branded at ends</li>\r\n<li>Frame width: 13.5cm</li>\r\n<li>Hard case</li>\r\n<li>Made in Italy</li>\r\n<li>Please note: these glasses come with plain lenses; for prescription lenses please visit our London in-store opticians</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(155, 'pgnur', 'vrbsh', 'gtsyz', 'Houndstooth-pattern silk-satin tie', 'GRU0028', 'lcgpa', 'pcyzr', '', NULL, 15750, '<p>Tom Ford silk-satin tie<br />100% silk-satin<br />Self-tie fastening<br />Slim blade, houndstooth pattern, woven, glossy texture<br />Dry clean<br />Made in Italy</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(156, 'vgkyq', 'vrbsh', 'gtsyz', 'Short-sleeve cotton-pique polo shirt', 'GRU0029', 'saqem', 'goyec', '', NULL, 16887, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;cotton-piqu&eacute; polo shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed button fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Collar, short sleeves, ribbed collar and cuffs, pearlescent buttons, brand embroidery at front hem, stepped notched hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash cold</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Portugal</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 2in/1.88m and wears a size UK 40</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Midweight, slight stretch</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(157, 'hfbos', 'vrbsh', 'gtsyz', 'Slater cat eye-frame acetate sunglasses', 'PR0029', 'lcgpa', 'bnxkh', '', NULL, 21437, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;acetate sunglasses</li>\r\n<li>Cat eye-frame, tinted gradient lenses, gold-toned hardware, &lsquo;T&rsquo; logo, engraved logo plaque at tips</li>\r\n<li>100% UV protection</li>\r\n<li>Frame width: 15cm</li>\r\n<li>Hard case</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(158, 'gdjac', 'vrbsh', 'gtsyz', 'Pocket-embellished cotton-jersey T-shirt', 'GRU0030', 'saqem', 'ygtuj', '', NULL, 15225, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;cotton-jersey T-shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Pulls on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Crew neck, ribbed trim at neckline, short sleeves, patch pocket at chest, all-over marled texture, brand embroidery at front hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash cold</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 2in/1.88m and wears a size UK 40</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Lightweight, slight stretch</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(159, 'boxsw', 'vrbsh', 'gtsyz', 'Ft5507 square glasses', 'PR0030', 'lcgpa', 'bnxkh', '', NULL, 27125, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;acetate optical glasses</li>\r\n<li>Square frame, metal eye-rim structure, metal arms, &lsquo;T&rsquo; hinges, logo print lens, logo detail tips, tortoiseshell finish</li>\r\n<li>Frame width 14cm</li>\r\n<li>Hard case</li>\r\n<li>Made in Italy</li>\r\n<li><strong>Please note: these glasses come with plain lenses; for prescription lenses please visit our London in-store opticians</strong></li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(160, 'fbjgn', 'vrbsh', 'gtsyz', 'Solid-pattern silk-satin tie', 'GRU0031', 'lcgpa', 'pcyzr', '', NULL, 15748, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;silk-satin tie</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% silk-satin</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slim blade, solid pattern, woven, glossy texture</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(161, 'fdcmv', 'vrbsh', 'gtsyz', 'Beatrix square-frame sunglasses', 'PR0031', 'lcgpa', 'bnxkh', '', NULL, 21437, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;sunglasses</li>\r\n<li>ft0613</li>\r\n<li>Square-frame, tinted lenses, \'T\' logo insert at temples, brand-embossed tips, silver-toned hardware</li>\r\n<li>100% UV protection</li>\r\n<li>Frame width: 14.5cm</li>\r\n<li>Soft case</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(162, 'tonbi', 'vrbsh', 'gtsyz', 'Quilted wool and shell-down jacket', 'GRU0032', 'saqem', 'wiflq', '', NULL, 157064, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;wool and shell-down jacket</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% wool; 100% polyamide; padding: 90% goose down, 10% feather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed zip fastening at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>High neck, long sleeves, padded and lined panel at front, zipped pockets at front, ribbed trims, straight hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Size medium: Length 26in / 66cm, chest 46in / 117cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 3in/1.91m and wears a size medium</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Midweight, slight stretch</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(163, 'bilhg', 'vrbsh', 'gtsyz', 'Tf5528-B phantos frame optical glasses', 'PR0032', 'lcgpa', 'bnxkh', '', NULL, 21875, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;metal sunglasses</li>\r\n<li>Phantos frames, clear lenses, tortoiseshell frames, &nbsp;temple branding, round tips</li>\r\n<li>Hard case</li>\r\n<li>Frame width: 13.5cm</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:02:37', '2020-09-01 18:28:38'),
(164, 'tqukf', 'vrbsh', 'gtsyz', 'Button-up cotton-piquÃ© polo shirt', 'GRU0033', 'saqem', 'goyec', '', NULL, 16887, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;cotton-piqu&eacute; polo shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed button fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Collar, short sleeves, pearlescent buttons, brand embroidery at front hem, notched stepped hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hand wash</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Portugal</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 2in/1.88m and wears a size UK 40</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Midweight, stretchy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(165, 'mnsqb', 'vrbsh', 'gtsyz', 'Rizzo FT0730 square-frame sunglasses', 'PR0033', 'lcgpa', 'bnxkh', '', NULL, 26250, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;sunglasses</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Square-frame, tinted lenses, \'T\' logo insert at temples, brand-embossed tips, silver-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Frame width: 14.5cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Hard case</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(166, 'bhosv', 'vrbsh', 'gtsyz', 'FT5608-B round glasses', 'PR0034', 'lcgpa', 'bnxkh', '', NULL, 21875, '<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;glasses</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Metallic tips, round frame, T logo hinges</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Acetate</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Frame width 14.5cm</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Hard case</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(167, 'tjgnh', 'vrbsh', 'gtsyz', 'Burnished money-clip leather wallet', 'GRU0034', 'lcgpa', 'fmula', '', NULL, 39375, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford</strong>&nbsp;leather clip wallet</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% grained leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Eight card slots, two slots, note section</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 9.4cm, width 11.4cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(168, 'doxcq', 'vrbsh', 'gtsyz', 'Tom Ford FT0653 irregular metal glasses', 'PR0035', 'lcgpa', 'bnxkh', '', NULL, 28000, '<p><strong>Tom Ford</strong>&nbsp;metal glasses<br />Two-tone lens, slim metal frames, irregular shape<br />100% UV protection</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(169, 'sxmig', 'vrbsh', 'gtsyz', 'Buckly leather backpack', 'GRU0035', 'fancy', 'rihfu', '', NULL, 146126, '<ul>\r\n<li>Tom Ford leather backpack</li>\r\n<li>Zip closure</li>\r\n<li>Adjustable shoulder straps, roll top carry handle, external zip and slip pocket, internal slip and press stud pockets, lined interior</li>\r\n<li>Height: 46, width 36, depth 16 cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(170, 'pwxve', 'vrbsh', 'yluqx', 'Tribute 75 patent-leather platform sandals', 'PR0036', 'nbawm', 'gmyjl', '', NULL, 53632, '<p><strong>Saint Laurent&nbsp;</strong>metallic-leather platform sandals<br /><br /><strong>Details</strong>:<br />Buckle fastening<br />Open toe<br />Metallic finish<br />Caged front straps<br />Ankle strap<br />Covered platform and stiletto heel<br /><br /><strong>Fabric</strong>:<br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather</p>', '<p>Heel height 2.95&rdquo;<br /><br /><strong>Care</strong>:<br />Use specialist cleaner</p>', '', 0, 1, '', NULL, NULL, 0, 'PR0036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(171, 'hnxvp', 'vrbsh', 'yluqx', 'Cassandra 115 leather wedge-heeled sandals', 'PR0037', 'nbawm', 'gmyjl', '', NULL, 73254, '<p><strong>Saint Laurent&nbsp;</strong>leather sandals<br />Buckle fastening<br />Open toe, cross-over straps, black metal hardware, adjustable strap<br />Leather upper, leather lining, leather sole<br />100% calfskin leather<br />Heel height 4.5&rdquo;, platform height 0.4&rdquo;, arch height 4.1&rdquo;<br />Made in Italy</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(172, 'ymkrn', 'vrbsh', 'yluqx', 'Opyum logo heel patent leather courts', 'PR0038', 'nbawm', 'gmyjl', '', NULL, 73310, '<ul>\r\n<li>Saint Laurent patent-leather courts</li>\r\n<li>Slips on</li>\r\n<li>Pointed toe, low-cut vamp, patent finish, interlocking metal logo heel,</li>\r\n<li>Leather upper</li>\r\n<li>Heel height 4.3\"</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 20:48:54', '2020-09-01 08:18:54'),
(173, 'nxftr', 'vrbsh', 'yluqx', 'Tribute 75 patent leather sandals', 'PR0039', 'nbawm', 'gmyjl', '', NULL, 53673, '<ul>\r\n<li>Saint Laurent sandals</li>\r\n<li>Buckle fastening</li>\r\n<li>Open toe, patent finish, caged front strap, ankle strap, covered platform and stiletto heel</li>\r\n<li>Leather upper, leather lining, leather sole</li>\r\n<li>Heel height 4.1\"</li>\r\n<li>Platform height 1.2\"</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(174, 'flakh', 'vrbsh', 'yluqx', 'Tribute leather heeled sandals', 'PR0040', 'nbawm', 'gmyjl', '', NULL, 61091, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather heeled sandals</li>\r\n<li>Buckle fastening</li>\r\n<li>Open toe, intertwining straps, stud detailing, YSL logo engraved on buckle, stiletto heel</li>\r\n<li>Leather upper, leather lining, leather sole</li>\r\n<li>Heel height 5.3&rsquo;&rsquo;</li>\r\n<li>Platform height 1.2&rsquo;&rsquo;</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(175, 'mkahq', 'vrbsh', 'gtsyz', 'Solid-pattern silk-satin tie', 'GRU0036', 'lcgpa', 'pcyzr', '', NULL, 15709, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford&nbsp;</strong>silk-satin tie</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% silk-satin</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slim blade, solid pattern, woven, glossy texture</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(176, 'fsjwt', 'vrbsh', 'gtsyz', 'Solid-pattern silk-satin tie', 'GRU0037', 'lcgpa', 'pcyzr', '', NULL, 15709, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford&nbsp;</strong>silk-satin tie</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% silk-satin</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slim blade, solid pattern, woven, glossy texture</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Dry clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 21:44:41', '2020-09-01 09:23:39'),
(177, 'eyzxj', 'vrbsh', 'gtsyz', 'Western-check cotton shirt', 'GRU0038', 'saqem', 'xplro', '', NULL, 43287, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford&nbsp;</strong>cotton shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed popper-fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Spread collar</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Buttoned cuffs</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Long sleeves, popper-buttoned cuffs, pleated panels at cuffs, two buttoned flap pockets at front, all-over tartan pattern, v-shaped yoke at back, curved hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Collar size 15.5: length 30in / 76cm, chest 38in / 97cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 2in/1.88m and wears a collar size 15.5</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Lightweight, non-stretch</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(178, 'rpudq', 'vrbsh', 'gtsyz', 'Western-check cotton shirt', 'GRU0039', 'saqem', 'xplro', '', NULL, 43287, '<ul>\r\n<ul>\r\n<li><strong>Tom Ford&nbsp;</strong>cotton shirt</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% cotton</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Exposed popper-fastenings at front</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Spread collar</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Buttoned cuffs</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Long sleeves, popper-buttoned cuffs, pleated panels at cuffs, two buttoned flap pockets at front, all-over tartan pattern, v-shaped yoke at back, curved hem</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Machine wash mild</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>True to size</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Collar size 15.5: length 30in / 76cm, chest 38in / 97cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Model is 6ft 2in/1.88m and wears a collar size 15.5</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Lightweight, non-stretch</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(179, 'kdnzr', 'vrbsh', 'gtsyz', 'Atticus straight-leg wool and silk-blend trousers', 'GRU0040', 'saqem', 'mdrvj', '', NULL, 73048, '<p><strong>Tom Ford</strong>&nbsp;wool and silk-blend trousers<br />75% wool, 25% silk<br />Concealed hook-and-eye and zip-fly fastenings at front<br />Mid-rise, straight leg, structured waistband, waist adjusters, slip pockets at sides, darts at back, jetted pockets at back, pressed centre creases at front and back, straight hem<br />Dry clean<br />Made in Italy<br />Size UK 32: rise 12in / 31cm, inside leg 31in / 79cm, leg opening 14in / 36cm<br />Model is 6ft 2in/1.88m and wears a size UK 32</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(180, 'jhgwx', 'vrbsh', 'gtsyz', 'Atticus straight-leg wool and silk-blend trousers', 'GRU0041', 'saqem', 'mdrvj', '', NULL, 73048, '<p><strong>Tom Ford</strong>&nbsp;wool and silk-blend trousers<br />75% wool, 25% silk<br />Concealed hook-and-eye and zip-fly fastenings at front<br />Mid-rise, straight leg, structured waistband, waist adjusters, slip pockets at sides, darts at back, jetted pockets at back, pressed centre creases at front and back, straight hem<br />Dry clean<br />Made in Italy<br />Size UK 32: rise 12in / 31cm, inside leg 31in / 79cm, leg opening 14in / 36cm<br />Model is 6ft 2in/1.88m and wears a size UK 32</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(181, 'yxrko', 'vrbsh', 'yluqx', 'Nu Pieds 05 metallic-leather sandals', 'PR0041', 'nbawm', 'gmyjl', '', NULL, 41891, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather sandals</li>\r\n<li>Slip-on</li>\r\n<li>Intertwining straps, square open toe, metallic finish, contrast lining</li>\r\n<li>Leather upper, leather lining, leather sole</li>\r\n<li>Heel height 0.2\"</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(182, 'qesgu', 'vrbsh', 'yluqx', 'Kaia leather belt bag', 'PR0042', 'fancy', 'kxryd', '', NULL, 57599, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather belt bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Magnetic press-stud closure</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Adjustable leather waist strap, single compartment, gold-toned monogram logo at front, gold-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 10cm, width 13cm, depth 4cm, strap drop 40cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 04:59:45', '2020-09-03 18:03:05'),
(183, 'nafuv', 'vrbsh', 'yluqx', 'Baby Sac de Jour croc-embossed leather tote bag', 'PR0043', 'fancy', 'esowr', '', NULL, 91635, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Magnetic press-stud closure</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Curb chain and leather strap, single compartment, internal zip pocket, two card slots, all-over quilted chevron design, gold-toned monogram logo at front, gold-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 19cm, width 24cm, depth 8cm, strap drop 56cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 05:08:13', '2020-09-01 17:12:50'),
(184, 'wzcma', 'vrbsh', 'yluqx', 'Kate monogram suede shoulder bag', 'PR0044', 'fancy', 'esowr', '', NULL, 126980, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;suede reversible bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Outer: 100% lambskin</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Magnetic closure</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Curb chain strap, reversible design, internal slip pocket, gold-toned rope monogram logo at front, gold-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 20cm, width 32cm, depth 6cm, strap drop 50cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 05:20:45', '2020-09-01 16:52:13'),
(185, 'xtvei', 'vrbsh', 'tovmx', 'Carrarmato patent leather platform penny loafers', 'GRU0042', 'nbawm', 'tyfcl', '', NULL, 43177, '<ul>\r\n<ul>\r\n<li><strong>Tod&rsquo;s</strong>&nbsp;patent leather loafers</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Round toe, penny bar over toe, platform sole</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Upper: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: 100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Sole: 100% rubber</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(186, 'sjerl', 'vrbsh', 'yluqx', 'Classic logo-detail leather espadrilles', 'PR0045', 'nbawm', 'dgzfq', '', NULL, 32726, '<p><strong>Saint Laurent</strong>&nbsp;leather espadrilles<br />Slip-on<br />Almond toe, embossed branding at vamp, jute sole<br />Leather upper, rubber sole</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(187, 'hxogq', 'vrbsh', 'tovmx', 'Gommini Frangia suede driving shoes', 'GRU0043', 'nbawm', 'ikvzp', '', NULL, 34716, '<ul>\r\n<li><strong>Tod&rsquo;s</strong>&nbsp;suede driving shoes</li>\r\n<li>Slip on</li>\r\n<li>Gold-toned buckle embellishment at front, closed toe, round toe, fringe at front, exposed handmade stitching, pebble outsole</li>\r\n<li>Suede upper, leather lining, rubber sole</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:05:47', '2020-09-01 17:35:47'),
(188, 'sibtd', 'vrbsh', 'tovmx', 'Gommini Frangia suede driving shoes', 'GRU0044', 'nbawm', 'ikvzp', '', NULL, 34716, '<ul>\r\n<li><strong>Tod&rsquo;s</strong>&nbsp;suede driving shoes</li>\r\n<li>Slip on</li>\r\n<li>Gold-toned buckle embellishment at front, closed toe, round toe, fringe at front, exposed handmade stitching, pebble outsole</li>\r\n<li>Suede upper, leather lining, rubber sole</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(189, 'wksxo', 'vrbsh', 'yluqx', 'Leather espadrilles', 'PR0046', 'nbawm', 'dgzfq', '', NULL, 32726, '<p>Saint Laurent leather espadrilles<br />&bull; Slip on<br />&bull; Round toe, branding on the upper<br />&bull; Leather upper, rope midsole, rubber sole<br />&bull; Made in Spain</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(190, 'blwfk', 'vrbsh', 'tovmx', 'Gomma Para patent leather loafers', 'GRU0045', 'nbawm', 'tyfcl', '', NULL, 44486, '<p><strong>Tods&nbsp;</strong>leather loafers<br />Slip in style Round toe, tassel detail, patent leather<br />Leather upper, leather lining, rubber sole</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(191, 'yispv', 'vrbsh', 'tovmx', 'Gomma Para patent leather loafers', 'GRU0046', 'nbawm', 'tyfcl', '', NULL, 44486, '<p><strong>Tods&nbsp;</strong>leather loafers<br />Slip in style Round toe, tassel detail, patent leather<br />Leather upper, leather lining, rubber sole</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(192, 'sndhr', 'vrbsh', 'yluqx', 'Logo-print large leather tote bag', 'PR0047', 'fancy', 'xrlas', '', NULL, 65453, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather tote bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Press stud closure</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Top handles, gold-toned lettering</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Use specialist cleaner</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Height 28cm, width 37cm, depth 13cm</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(193, 'xuvbp', 'vrbsh', 'yluqx', 'Buckle branded leather tote bag', 'PR0048', 'fancy', 'xrlas', '', NULL, 73308, '<p><br /><strong>Saint Laurent</strong>&nbsp;leather tote bag<br />Open top<br />Twin carry handles, foiled branding, buckles, removable zip pouch<br />Leather<br />Use specialist cleaner<br />Length 27cm, width 34cm, depth 11cm, handle drop 25cm</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:35:43', '2020-09-01 18:05:43'),
(194, 'nxihy', 'vrbsh', 'yluqx', 'Rive Gauche cotton tote bag', 'PR0049', 'fancy', 'xrlas', '', NULL, 76362, '<p><br /><strong>Saint Laurent</strong>&nbsp;cotton tote<br />Snap button closure<br />Leopard print, logo print, bronze-tone hardware, internal zip pocket, leather top handles<br />Use specialist cleaner<br />Height 36cm, width 48cm, depth 6cm, handle drop 17cm<br />Made in Italy</p>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(195, 'jmvzd', 'vrbsh', 'yluqx', 'Buckle logo-print leather tote bag', 'PR0050', 'fancy', 'xrlas', '', NULL, 73308, '<ul>\r\n<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather tote bag</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>63% leather, 37% polyurethane</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Open top</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Twin top handles with buckle detail, single compartment, suede lined, detachable matching purse, gold-toned hardware</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Height 28cm, width 36cm, depth 14cm, handle drop 20cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'PR0050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(198, 'zjyeo', 'vrbsh', 'hiowx', 'Pilar canvas esperadrille wedges', 'GRU0048', 'nbawm', 'dgzfq', '', NULL, 42131, '<ul>\r\n<ul>\r\n<li><strong>Gucci&nbsp;</strong>canvas wedges</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>100% fabric</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Round toe, all-over logo-print, gold-toned GG motif at upper, jute sole</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Heel height 1.3\" / 3.5cm</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Upper: fabric</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: fabric</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Sole: rubber</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-03 23:45:15', '2020-09-03 11:15:15');
INSERT INTO `product` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `status`, `weight`, `img`, `type`, `groupid`, `product_code`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `styleno`, `hsncode`, `created_at`, `updated_at`) VALUES
(199, 'xwscp', 'vrbsh', 'hiowx', 'Houdan horsebit leather platform loafers', 'GRU0049', 'nbawm', 'tyfcl', '', NULL, 58202, '<ul>\r\n<ul>\r\n<li><strong>Gucci</strong>&nbsp;leather platform loafers</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Leather upper</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Slips on</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Round toe, horsebit charm, mid-block heel, platform sole, gold-toned hardware.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Heel height 4.5cm/1.77 inches</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Specialist leather clean</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Made in Italy</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Upper: Leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>Lining: Leather</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Sole: Leather</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(200, 'pwhbk', 'vrbsh', 'hiowx', 'Mid-heel leather loafers', 'GRU0050', 'nbawm', 'tyfcl', '', NULL, 69930, '<ul>\r\n<li>Gucci &nbsp;mid-heel loafers</li>\r\n<li>Slip-on</li>\r\n<li>Navy and red fabric strap, GG gold-tone detailing, square toe</li>\r\n<li>Leather</li>\r\n<li>35mm heel</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(201, 'gazrc', 'vrbsh', 'hiowx', 'Mid-heel leather loafers', 'GRU0051', 'nbawm', 'tyfcl', '', NULL, 69930, '<ul>\r\n<li>Gucci &nbsp;mid-heel loafers</li>\r\n<li>Slip-on</li>\r\n<li>Navy and red fabric strap, GG gold-tone detailing, square toe</li>\r\n<li>Leather</li>\r\n<li>35mm heel</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(202, 'tzfbv', 'vrbsh', 'hiowx', 'Logo-embroidered rubber sliders', 'GRU0052', 'nbawm', 'ytzix', '', NULL, 22586, '<p><strong>Gucci</strong>&nbsp;rubber sliders<br /><br /><strong>Details:</strong><br />Slip on<br />Open toe<br />Logo-embroidered front strap<br />Backless<br />Moulded footbed<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: rubber<br />Sole: rubber sole</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-04 00:21:57', '2020-09-03 12:00:08'),
(203, 'zexnp', 'vrbsh', 'hiowx', 'Donnie GG leather loafers', 'GRU0053', 'nbawm', 'tyfcl', '', NULL, 46909, '<p><strong>Gucci</strong>&nbsp;leather loafers<br /><br /><strong>Details:</strong><br />Slip on<br />Almond toe<br />Web stripe strap detail<br />Gold-toned double G hardware<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Care:</strong><br />Use specialist cleaner</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(204, 'xekgf', 'vrbsh', 'hiowx', 'Donnie GG leather loafers', 'GRU0054', 'nbawm', 'tyfcl', '', NULL, 46909, '<p><strong>Gucci</strong>&nbsp;leather loafers<br /><br /><strong>Details:</strong><br />Slip on<br />Almond toe<br />Web stripe strap detail<br />Gold-toned double G hardware<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Care:</strong><br />Use specialist cleaner</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(205, 'hruox', 'vrbsh', 'hiowx', 'Fria GG-woven wool ankle boots', 'GRU0055', 'nbawm', 'draim', '', NULL, 52990, '<p><strong>Gucci&nbsp;</strong>wool ankle boots<br /><br /><strong>Details:</strong><br />GG pattern<br />Gold-toned hardware<br />Horsebit embellished<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% wool<br />Lining: 100% wool<br />Sole: 100% rubber<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 0.59&rdquo;<br /><br /><strong>Care:</strong><br />Spot clean</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(206, 'dwfut', 'vrbsh', 'hiowx', 'Fria GG-woven wool ankle boots', 'GRU0056', 'nbawm', 'draim', '', NULL, 52990, '<p><strong>Gucci&nbsp;</strong>wool ankle boots<br /><br /><strong>Details:</strong><br />GG pattern<br />Gold-toned hardware<br />Horsebit embellished<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% wool<br />Lining: 100% wool<br />Sole: 100% rubber<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 0.59&rdquo;<br /><br /><strong>Care:</strong><br />Spot clean</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(207, 'pdyjf', 'vrbsh', 'hiowx', 'Phyllis GG-embellished leather loafers', 'GRU0057', 'nbawm', 'tyfcl', '', NULL, 52990, '<p><strong>Gucci&nbsp;</strong>GG-embellished leather loafers<br /><br /><strong>Details:</strong><br />Slip on<br />Interlocking G embellishment<br />Gold-toned metal hardware<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 1.18&rdquo;<br /><br /><strong>Care:</strong><br />Wipe clean with a soft cloth</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(208, 'pjtnk', 'vrbsh', 'hiowx', 'Phyllis GG-embellished leather loafers', 'GRU0058', 'nbawm', 'tyfcl', '', NULL, 52990, '<p><strong>Gucci&nbsp;</strong>GG-embellished leather loafers<br /><br /><strong>Details:</strong><br />Slip on<br />Interlocking G embellishment<br />Gold-toned metal hardware<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 1.18&rdquo;<br /><br /><strong>Care:</strong><br />Wipe clean with a soft cloth</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(209, 'fmhtv', 'vrbsh', 'hiowx', 'Dracma GG-detailed leather Derby shoes', 'GRU0059', 'nbawm', 'wlpdo', '', NULL, 52990, '<p><strong>Gucci</strong>&nbsp;leather Derby shoes<br /><br /><strong>Details:</strong><br />Lace-up fastening<br />Panelled construction<br />Interlocking G brogue detail<br />Square, closed toe<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 1.4&rdquo;<br /><br /><strong>Care:</strong><br />Use specialist cleaner</p>', '', '', 0, 1, '', NULL, NULL, 0, 'GRU0059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '', NULL, NULL, '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(210, 'qrenm', 'vrbsh', 'hiowx', 'Dracma GG-detailed leather Derby shoes', 'GRU0060', 'nbawm', 'wlpdo', '', NULL, 52990, '<p><strong>Gucci</strong>&nbsp;leather Derby shoes<br /><br /><strong>Details:</strong><br />Lace-up fastening<br />Panelled construction<br />Interlocking G brogue detail<br />Square, closed toe<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather<br /><br /><strong>Size &amp; Fit:</strong><br />Heel height 1.4&rdquo;<br /><br /><strong>Care:</strong><br />Use specialist cleaner</p>', '', '', 0, 1, '', NULL, NULL, 1, 'GRU0059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(211, 'xwadn', 'vrbsh', 'yluqx', 'Nu Pieds 05 metallic-leather sandals', 'PR0051', 'nbawm', 'gmyjl', '', NULL, 41891, '<ul>\r\n<li><strong>Saint Laurent</strong>&nbsp;leather sandals</li>\r\n<li>Slip-on</li>\r\n<li>Intertwining straps, square open toe, metallic finish, contrast lining</li>\r\n<li>Leather upper, leather lining, leather sole</li>\r\n<li>Heel height 0.2\"</li>\r\n<li>Made in Italy</li>\r\n</ul>', '', '', 0, 1, '', NULL, NULL, 1, 'PR0041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '', NULL, NULL, '2020-09-04 06:42:18', '2020-09-03 18:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `products_db`
--

CREATE TABLE `products_db` (
  `p_id` int(255) NOT NULL,
  `p_key` varchar(10) NOT NULL,
  `store` varchar(100) NOT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `product_name` text NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `category` varchar(100) NOT NULL,
  `subcategory` varchar(100) DEFAULT NULL,
  `add_cat` varchar(255) DEFAULT NULL,
  `regular_price` varchar(100) DEFAULT NULL,
  `price` float NOT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `min_qty` varchar(255) DEFAULT NULL,
  `max_qty` varchar(255) DEFAULT NULL,
  `offer` varchar(20) DEFAULT NULL,
  `offer_start` varchar(100) DEFAULT NULL,
  `offer_end` varchar(100) DEFAULT NULL,
  `prf` varchar(20) DEFAULT NULL,
  `specification` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `features` text DEFAULT NULL,
  `reviews` varchar(100) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `visible` int(2) NOT NULL DEFAULT 1,
  `p_img` text NOT NULL,
  `s_img` text DEFAULT NULL,
  `o_img` text DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL,
  `width` varchar(100) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `create_by` int(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_db`
--

INSERT INTO `products_db` (`p_id`, `p_key`, `store`, `brand`, `product_name`, `code`, `category`, `subcategory`, `add_cat`, `regular_price`, `price`, `stock`, `min_qty`, `max_qty`, `offer`, `offer_start`, `offer_end`, `prf`, `specification`, `description`, `features`, `reviews`, `status`, `visible`, `p_img`, `s_img`, `o_img`, `weight`, `width`, `height`, `create_by`, `created_at`, `updated_at`) VALUES
(1, 'bodzx', 'vrbsh', 'xueqs', 'Metallic high-rise lamÃ© silk-blend trousers', '01', 'saqem', 'mdrvj', '', '', 109579, NULL, NULL, NULL, '', '', '', 'Women', '<p><strong>Saint Laurent</strong>&nbsp;lam&eacute; silk-blend trousers<br /><br /><strong>Details:</strong><br />High rise<br />Elasticated waistband<br />Two concealed side pockets<br />Tapered legs<br />Turned-up cuffs<br />Elasticated cuffs<br />Slip on<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />71% silk, 29% metallic polyester<br />Midweight, non-stretch</p>', '<p><strong>Size &amp; Fit:</strong><br />Size UK 8: chest 33.1in/84cm, waist 25.2in/64cm, hip 35in/89cm<br />True to size<br />Cut to a tailored fit<br /><br /><strong>Care:</strong><br />Dry clean only</p>', '', '0', '', 1, 'primary_20200610012551.jpg', 'second_20200610012551.jpg', 'other1_20200610012551.jpg,', '', '', '', 1, '2020-06-10 08:25:51', '2020-06-09 19:55:51'),
(2, 'gdtfb', 'vrbsh', 'xueqs', 'Leopard-print tie-front wool shirt', '02', 'saqem', 'xplro', 'Wool Shirt', '', 84076, NULL, NULL, NULL, '', '', '', 'Women', '<p><strong>Saint Laurent</strong>&nbsp;wool shirt<br /><br /><strong>Details:</strong><br />Mandarin collar<br />Long sleeves<br />Buttoned cuffs<br />All-over leopard print<br />Self-tying front<br />Oversized<br />Curved hem<br />Exposed button fastenings at front<br /><br /><strong>Fabric:</strong><br />100% virgin wool<br />Lightweight, non-stretch</p>', '<p><strong>Size &amp; Fit:</strong><br />True to size<br />Cut to an oversized fit<br /><br /><strong>Care:</strong><br />Dry clean</p>', '', '0', '', 0, 'primary_20200610013330.png', 'second_20200610013330.png', 'other1_20200610013330.png,', '', '', '', 1, '2020-06-11 10:57:24', '2020-06-11 10:57:24'),
(3, 'weqnd', 'vrbsh', 'isfau', 'Brand-print crewneck cotton-jersey T-shirt', '03', 'saqem', 'ygtuj', 'cotton-jersey T-shirt', '', 18497, NULL, NULL, NULL, '', '', '', 'Men', '<p><strong>Balmain&nbsp;</strong>cotton-jersey T-shirt<br /><br /><strong>Details:</strong><br />Crewneck<br />Short sleeves<br />Printed branding at chest<br />Straight hem<br />Pulls on<br /><br /><strong>Fabric:</strong><br />100% cotton<br />Midweight, stretch-jersey</p>', '<p><strong>Size &amp; Fit:</strong><br />Model is 6ft 2in / 1.88m and wears a size medium<br />Cut to a regular fit<br /><br /><strong>Care:</strong><br />Specialist dry clean</p>', '', '0', '', 0, 'primary_20200610014407.png', 'second_20200610014407.png', 'other1_20200610014407.png,', '', '', '', 1, '2020-06-11 10:53:32', '2020-06-11 10:53:32'),
(4, 'uwbqo', 'vrbsh', 'lxeid', 'Brixton collapsible leather loafers', '04', 'nbawm', 'tyfcl', '', '', 51890, NULL, NULL, NULL, '', '', '', 'Men', '<ul>\r\n<li><strong>Gucci&nbsp;</strong>suede loafers<br /><br /><strong>Details:</strong><br />Slip on<br />Leather trim<br />Gold-toned hardware<br />Horsebit embellishment<br />Closed toe<br />Almond toe<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />Upper: 100% leather<br />Lining: 100% leather<br />Sole: 100% leather</li>\r\n</ul>', '<p><strong>Size &amp; Fit:</strong><br />Heel height 1.18&rdquo;<br /><br /><strong>Care:</strong><br />Use specialist cleaner</p>', '', '0', '', 1, 'primary_20200610021338.png', 'second_20200610021338.png', 'other1_20200610021338.png,', '', '', '', 1, '2020-06-10 09:13:38', '2020-06-09 20:43:38'),
(5, 'bjkfm', 'vrbsh', 'pveuh', 'Double T suede driving shoes', '04', 'nbawm', 'ikvzp', '', '', 26292, NULL, NULL, NULL, '', '', '', 'Men', '<section class=\"c-self-says o-fs-self-says\" data-ts-self-says=\"Selfridges Says\"><q class=\"c-self-says__content\"><a href=\"http://www.selfridges.com/GB/en/cat/tods/\"><span style=\"color: black;\"><strong>Tod\'s</strong></span></a>&nbsp;Double T collection offers a refined new take on the company\'s classic&nbsp;<strong>driving shoes</strong>. This design is crafted from superbly comfortable suede and finished with pebbled sole and heel panel (giving great grip for driving in, as the name suggests). The new twist comes in the form of a smart double T feature at the front. Team with chinos and a gingham shirt &ndash; because with this kind of quality, style really is that easy.</q></section>\r\n<div class=\"more-from\">&nbsp;</div>', '<ul>\r\n<li><strong>Tod\'s</strong>&nbsp;suede driving shoes</li>\r\n<li>Slip on</li>\r\n<li>Square toe, apron front, smooth texture, double T feature at the front</li>\r\n<li>100% leather &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Made in Italy&nbsp;</li>\r\n</ul>', '', '0', '', 1, 'primary_20200610122538.png', 'second_20200610122539.png', 'other1_20200610122539.png,', '', '', '', 1, '2020-06-10 19:25:38', '2020-06-10 06:55:39'),
(6, 'zjavh', 'vrbsh', 'pveuh', 'Double T suede driving shoes', '04', 'nbawm', 'ikvzp', '', '', 26292, NULL, NULL, NULL, '', '', '', 'Men', '<section class=\"c-self-says o-fs-self-says\" data-ts-self-says=\"Selfridges Says\"><q class=\"c-self-says__content\"><a href=\"http://www.selfridges.com/GB/en/cat/tods/\"><span style=\"color: black;\"><strong>Tod\'s</strong></span></a>&nbsp;Double T collection offers a refined new take on the company\'s classic&nbsp;<strong>driving shoes</strong>. This design is crafted from superbly comfortable suede and finished with pebbled sole and heel panel (giving great grip for driving in, as the name suggests). The new twist comes in the form of a smart double T feature at the front. Team with chinos and a gingham shirt &ndash; because with this kind of quality, style really is that easy.</q></section>\r\n<div class=\"more-from\">&nbsp;</div>', '<ul>\r\n<li><strong>Tod\'s</strong>&nbsp;suede driving shoes</li>\r\n<li>Slip on</li>\r\n<li>Square toe, apron front, smooth texture, double T feature at the front</li>\r\n<li>100% leather &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Made in Italy&nbsp;</li>\r\n</ul>', '', '0', '', 1, 'primary_20200610130727.png', 'second_20200610130727.png', 'other1_20200610130727.png,', '', '', '', 1, '2020-06-10 20:07:27', '2020-06-10 07:37:27'),
(7, 'vprfd', 'vrbsh', 'shipa', 'Cypress & Sea Fennel bath & shower gel 300ml', '06', 'bedsz', 'qpfel', '', '', 2121, NULL, NULL, NULL, '', '', '', '', '<p><strong>Molton Brown</strong>&nbsp;bath &amp; shower gel<br />Scent: Cypress &amp; Sea Fennel<br />Top note: Australian sea fennel.<br />Heart note: cypress.<br />Base notes: cardamom and jasmine.<br />Paraben-free, gluten-free, cruelty-free and vegan*<br />*No ingredient of animal origin</p>', '<p>300ml</p>', '', '0', '', 1, 'primary_20200610195640.png', 'second_20200610195640.png', 'other1_20200610195641.png,', '', '', '', 1, '2020-06-11 02:56:40', '2020-06-10 14:26:41'),
(8, 'wcdls', 'vrbsh', 'xueqs', 'Kate medium leather shoulder bag', '07', 'fancy', 'esowr', '', '', 126847, NULL, NULL, NULL, '', '', '', 'Women', '<p><strong>Saint Laurent</strong>&nbsp;leather shoulder bag<br />Fold-over press-stud fastening<br />Monogrammed logo, textured leather, chain shoulder strap, interior leather branded plaque and slip pocket, fully lined<br />100% leather<br />Use specialist cleaner</p>\r\n<p>Made in Italy<br />Comes with dust bag</p>', '<p>Height 14.5cm, width 24cm, depth 5.5cm</p>', '', '0', '', 1, 'primary_20200610200422.jpg', 'second_20200610200422.jpg', 'other1_20200610200423.jpg,other2_20200610200423.jpg,', '', '', '', 1, '2020-06-11 03:04:22', '2020-06-10 14:34:23'),
(9, 'kmvta', 'vrbsh', 'xueqs', 'Teddy Mick striped-appliquÃ© satin jacket', '08', 'saqem', 'wiflq', '', '', 187432, NULL, NULL, NULL, '', '', '', 'Men', '<p><strong>Saint Laurent</strong>&nbsp;satin jacket<br /><br /><strong>Details:</strong><br />Shawl collar<br />Epaulettes<br />Long sleeves<br />Striped appliqu&eacute; at front<br />Two slip pockets at front<br />Striped ribbed trims<br />Fully lined<br />Two interior pockets<br />Exposed zip fastening at front<br />Made in Italy<strong>Saint Laurent</strong>&nbsp;satin jacket<br /><br /><strong>Details:</strong><br />Shawl collar<br />Epaulettes<br />Long sleeves<br />Striped appliqu&eacute; at front<br />Two slip pockets at front<br />Striped ribbed trims<br />Fully lined<br />Two interior pockets<br />Exposed zip fastening at front<br />Made in Italy</p>\r\n<p><strong>Fabric:</strong><br />Main: 55% viscose, 45% cotton<br />Lining: 60% cupro, 40% cotton<br />Heavyweight, non-stretch</p>\r\n<p><strong>Care:</strong><br />Dry clean</p>', '<p><strong>Size &amp; Fit:</strong><br />Model is 6ft 2in / 1.88m and wears a size UK 36<br />Size UK 36: Length 23in / 58cm, chest 42in / 107cm<br />Cut to a slim fit</p>', '', '0', '', 1, 'primary_20200610201439.png', 'second_20200610201439.png', 'other1_20200610201439.png,other2_20200610201440.png,other3_20200610201440.png,', '', '', '', 1, '2020-06-11 03:14:39', '2020-06-10 14:44:40'),
(10, 'yclab', 'vrbsh', 'xueqs', 'Buckle leather tote bag', '08', 'fancy', 'xrlas', '', '', 72354, NULL, NULL, NULL, '', '', '', 'Women', '<p><strong>Saint Laurent</strong>&nbsp;tote bag<br />Open top<br />Twin carry handles, foiled branding, buckles, removable zip pouch<br />Leather<br />Use specialist cleaner</p>', '<p>Length 27cm, width 34cm, depth 11cm</p>', '', '0', '', 1, 'primary_20200610203705.png', 'second_20200610203705.png', 'other1_20200610203705.png,other2_20200610203705.png,', '', '', '', 1, '2020-06-11 03:37:05', '2020-06-10 15:07:05'),
(11, 'gyare', 'vrbsh', 'xueqs', 'Tribute 105 patent-leather heeled sandals', '09', 'nbawm', 'gmyjl', '', '', 59302, NULL, NULL, NULL, '', '', '', 'Women', '<ul>\r\n<li>Heel height 5.5\"</li>\r\n<li>Platform height 1\"</li>\r\n</ul>', '<ul>\r\n<li>Saint Laurent patent-leather heeled sandals</li>\r\n<li>Buckle fastening at ankle</li>\r\n<li>Peep toe, cross-over straps, sculpted platform heel, adjustable ankle strap</li>\r\n</ul>', '', '0', '', 1, 'primary_20200610204659.jpg', 'second_20200610204659.png', 'other1_20200610204659.png,other2_20200610204659.png,other3_20200610204659.png,', '', '', '', 1, '2020-06-11 03:46:59', '2020-06-10 15:16:59'),
(12, 'svwxz', 'vrbsh', 'pveuh', 'Mountain suede hiking boots', '012', 'nbawm', 'ncotb', '', '', 39367, NULL, NULL, NULL, '', '', '', 'Men', '<section class=\"c-self-says o-fs-self-says\" data-ts-self-says=\"Selfridges Says\"><q class=\"c-self-says__content\">Originally designed for tackling long walks on tough terrain, the hiking boot is on its own off-road adventure these days &ndash; this once-humble silhouette has become the season&rsquo;s most sought-after footwear trend.&nbsp;<strong><a href=\"http://www.selfridges.com/GB/en/cat/tods/\"><span style=\"color: black;\">Tod&rsquo;s</span></a></strong>&nbsp;offers us a characteristically refined iteration with this smooth suede pair. A satisfyingly chunky rubber sole might make them comfy enough for climbing, but they&rsquo;re just too good to get muddy.</q></section>\r\n<div class=\"more-from\">&nbsp;</div>', '<ul>\r\n<li>Tod&rsquo;s&nbsp;suede hiking boots</li>\r\n<li>Lace-up fastening</li>\r\n<li>Round toe, metal eyelets, logo to tongue, sculpted midsole, logo to heel, pull-tab to the back</li>\r\n<li>Suede upper, rubber sole</li>\r\n</ul>', '', '0', '', 1, 'primary_20200611112323.png', 'second_20200611112323.png', 'other1_20200611112323.png,other2_20200611112324.png,', '', '', '', 1, '2020-06-11 18:23:23', '2020-06-11 05:53:24'),
(13, 'crtxy', 'vrbsh', 'hopvn', 'TFT002 013 stainless steel and yellow-gold quartz watch', '012', 'zisqv', 'dklgu', '', '', 251789, NULL, NULL, NULL, '', '', '', 'Both', '<p><strong><a href=\"http://www.selfridges.com/GB/en/cat/tom-ford/\"><span style=\"color: black;\">Tom Ford</span></a></strong>&nbsp;is a man of many talents.Having dabbled in acting, architectural pursuits and, of course, fashion design, the American native fuses all three to create the&nbsp;<strong>002 quartz watch</strong>. Polished 18-carat gold and stainless-steel forms the round face that houses steel diamond-cut hands, while quartz movement drives the inner workings. Pair yours with a croc-embossed leather strap and your favourite suit &ndash; a look we can easily imagine Tom would don himself.</p>', '<p><br /><strong>Tom Ford</strong>&nbsp;stainless steel and yellow-gold quartz watch<br />Style No: TFT002 013<br />Polished case, round face, steel diamond-cut hands, sapphire crystal, quartz movement<br />Case width 34mm, case height 40mm<br />Water resistant to 30m<br />Two-year warranty<br />Made in Switzerland</p>', '', '0', '', 1, 'primary_20200611113608.jpg', '', ',', '', '', '', 1, '2020-06-11 18:36:08', '2020-06-11 06:06:08'),
(14, 'cnypt', 'vrbsh', 'hopvn', '002 stainless steel watch', '013', 'zisqv', 'dklgu', '', '', 251697, NULL, NULL, NULL, '', '', '', 'Both', '<p><strong><a href=\"http://www.selfridges.com/GB/en/cat/tom-ford/\"><span style=\"color: black;\">Tom Ford</span></a></strong>&nbsp;is a traditionalist when it comes to watches, believing the colour of the strap should match your shoes. That&rsquo;s why, for his debut collection of automatic timepieces, dubbed 002, you can swap the bracelets. Clever. Now you can keep this classic round-case style at your wrist from dawn till dusk, its polished stainless steel glinting against white diamond-cut hands, double-digit indices and a sunray blue dial.</p>', '<p><strong>Tom Ford</strong>&nbsp;stainless steel watch<br />Style No: TFT002017<br />Round stainless steel case, stainless steel crown with onyx inlay, automatic movement, sunray blue dial, white diamond-cut hands<br />Case diameter 40mm<br />Water-resistant to 30m<br />Made in Switzerland</p>', '', '0', '', 1, 'primary_20200611113904.jpg', '', ',', '', '', '', 1, '2020-06-11 18:39:03', '2020-06-11 06:09:04'),
(15, 'cdxgn', 'vrbsh', 'xueqs', 'Cassandra 115 leather wedge-heeled sandals', '013', 'nbawm', 'gmyjl', '', '', 80710, NULL, NULL, NULL, '', '', '', 'Women', '', '<p><strong>Saint Laurent&nbsp;</strong>leather sandals<br />Buckle fastening<br />Open toe, cross-over straps, black metal hardware, adjustable strap<br />Leather upper, leather lining, leather sole<br />100% calfskin leather<br />Heel height 4.5&rdquo;, platform height 0.4&rdquo;, arch height 4.1&rdquo;<br />Made in Italy</p>', '', '0', '', 1, 'primary_20200611115146.png', 'second_20200611115146.png', 'other1_20200611115146.png,other2_20200611115146.png,', '', '', '', 1, '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(16, 'rdabs', 'vrbsh', 'hopvn', 'TFT00100606 001 stainless-steel watch', '014', 'zisqv', 'dklgu', '', '', 251737, NULL, NULL, NULL, '', '', '', 'Both', '', '<p><strong>Tom Ford</strong>&nbsp;stainless steel watch<br />Style no: TFT00100606<br />Rectangular case, stainless steel black DLC case, sapphire crystal glass, Black Matte Velvet dial, Quartz movement<br />Case diameter: 30mm<br />Water resistance: 30m / 3 ATM<br />2 Years Manufacturer Warranty<br />Made in Switzerland</p>', '', '0', '', 1, 'primary_20200611121706.png', '', ',', '', '', '', 1, '2020-06-11 19:17:06', '2020-06-11 06:47:06'),
(17, 'hjmry', 'vrbsh', 'hopvn', 'TFT00100606 001 stainless-steel watch', '014', 'zisqv', 'dklgu', '', '', 251737, NULL, NULL, NULL, '', '', '', 'Both', '', '<p><strong>Tom Ford</strong>&nbsp;stainless steel watch<br />Style no: TFT00100606<br />Rectangular case, stainless steel black DLC case, sapphire crystal glass, Black Matte Velvet dial, Quartz movement<br />Case diameter: 30mm<br />Water resistance: 30m / 3 ATM<br />2 Years Manufacturer Warranty<br />Made in Switzerland</p>', '', '0', '', 1, 'primary_20200611125715.png', '', ',', '', '', '', 1, '2020-06-11 19:57:15', '2020-06-11 07:27:15'),
(18, 'juxvs', 'vrbsh', 'hopvn', 'TFT001003 001 stainless-steel watch', '014', 'zisqv', 'dklgu', '', '', 171118, NULL, NULL, NULL, '', '', '', '', '', '<p><strong>Tom Ford</strong>&nbsp;stainless-steel watch<br />Style no: TFT001003<br />Rectangular case, sapphire crystal glass, Black Glossy dial, Quartz movement<br />Case diameter: 30mm<br />Water resistance: 30m / 3 ATM<br />2 Years Manufacturer Warranty<br />Made in Switzerland</p>', '', '0', '', 1, 'primary_20200611131749.png', '', ',', '', '', '', 3, '2020-06-11 20:17:49', '2020-06-11 07:47:49'),
(19, 'bslno', 'vrbsh', 'itbnq', 'Pleated woven midi skirt', '017', 'saqem', 'wifle', '', '', 84854, NULL, NULL, NULL, '', '', '', 'Women', '<p>When he isn&rsquo;t spinning magic from wool, cashmere and the like (or tending to his own town &ndash; yep, that&rsquo;s real) you&rsquo;ll find&nbsp;<a href=\"https://www.selfridges.com/GB/en/cat/brunello-cucinelli/\"><strong>Brunello Cucinelli</strong></a>&nbsp;whipping up complementary pieces like this skirt. Crafted in Italy, it&rsquo;s shaped with rows and rows of neatly pressed pleats falling languidly to an asymmetric calf-swinging hem, topped with a self-tie belt at the waist. Slip it on with one of his shirts or plush knits, we say.</p>', '<p><strong>Brunello Cucinelli</strong>&nbsp;woven skirt<br /><br /><strong>Details:</strong><br />High waist<br />Elasticated waistband<br />Self-tie belt at waist<br />All-over pleated<br />Slips on<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />65% polyester, 35% cotton<br />Midweight, non-stretch<br /><br /><strong>Size &amp; Fit:</strong><br />Model is 5ft 9in / 1.75m and wears a size UK 8<br />Size UK 8: Length 39in / 99cm<br />True to size<br />Cut to a relaxed fit<br /><br /><strong>Care:</strong><br />Dry clean</p>', '', '0', '', 1, 'primary_20200611133311.png', 'second_20200611133311.png', 'other1_20200611133311.png,other2_20200611133312.png,other3_20200611133312.png,', '', '', '', 3, '2020-06-11 20:33:11', '2020-06-11 08:03:12'),
(20, 'togbz', 'vrbsh', 'jsrob', 'The Air-Brush kabuki bronzing brush', '018', 'bedsz', 'srgfy', '', '', 3080.79, NULL, NULL, NULL, '', '', '', 'Women', '<p>British beauty buff&nbsp;<strong><a href=\"https://www.selfridges.com/GB/en/cat/charlotte-tilbury/\">Charlotte Tilbury</a></strong>&nbsp;always says &ldquo;the precision of the artist&rsquo;s brush is key&rdquo;. That&rsquo;s why the brand created&nbsp;<strong>The Air-Brush kabuki brush</strong>. Crafted from super-soft, cruelty-free synthetic bristles, this hand-made retractable tool helps to glide bronzer over the skin for an airbrushed, sun-kissed finish. What&rsquo;s more, the rose-gold handle is ergonomically designed for optimum control. Easy to use? Check.</p>', '<p><strong>Charlotte Tilbury</strong>&nbsp;The Air-Brush kabuki brush<br />Application: Simply swirl the brush in the powder, tap off the excess and apply in a sweeping motion for an air-brush finish<br />Ergonomic hand, retractable, rose-gold design<br />100% cruelty-free synthetic bristles</p>', '', '0', '', 1, 'primary_20200611135204.png', 'second_20200611135204.png', 'other1_20200611135204.png,', '', '', '', 3, '2020-06-11 20:52:04', '2020-06-11 08:22:04'),
(21, 'gjcrx', 'vrbsh', 'lazyx', 'Love In White Eau de Parfum', '019', 'bedsz', 'gfqra', '', '', 12023, NULL, NULL, NULL, '', '', '', 'Women', '<section class=\"c-self-says o-fs-self-says\" data-ts-self-says=\"Selfridges Says\"><q class=\"c-self-says__content\"><a href=\"https://www.selfridges.com/GB/en/cat/creed/\"><strong>Creed</strong></a>\'s&nbsp;<strong>Love In White Eau de Parfum</strong>&nbsp;is a gorgeous bouquet of white flowers combining the first blooms of spring (Magnolia, Narcissus and White Hyacinth) with Fresh Grass on a base of White Iris from Florence, Rice Husk, and Vanilla to add depth and longevity. The result is a sensationally fresh, creamy, floral composition, full of energy and optimism.</q></section>\r\n<div class=\"more-from\">&nbsp;</div>', '<p><strong>Creed&nbsp;</strong>Love In White Eau de Parfum<br />Notes: Orange Zest from Italy, Florence Iris, French Daffodil, Magnolia, Vanilla, Ambergris, Sandalwood from Mysore<br />Available in 30ml, 75ml</p>', '', '0', '', 1, 'primary_20200611135906.png', '', ',', '', '', '', 3, '2020-06-11 20:59:05', '2020-06-11 08:29:06'),
(22, 'kfexh', 'vrbsh', 'lazyx', 'Love in Black eau de parfum 75ml', '020', 'bedsz', 'gfqra', '', '', 20179, NULL, NULL, NULL, '', '', '', 'Women', '<p>In 2006 the House of&nbsp;<a href=\"https://www.selfridges.com/GB/en/cat/creed/\">Creed&nbsp;</a>released its most successful fragrance to date, Love in White. Love in Black is a mirror image, with the emphasis firmly on seduction rather than innocence. Love in Black is a lush floral oriental with an intense dark and musky heart built around violets, iris, blackcurrant and rose.</p>', '<ul>\r\n<li>Creed Love in Black eau de parfum</li>\r\n<li>Notes: Violet, iris, blackcurrant, rose</li>\r\n<li>Available in 30ml, 75ml</li>\r\n</ul>', '', '0', '', 1, 'primary_20200611140641.png', '', ',', '', '', '', 3, '2020-06-11 21:06:41', '2020-06-11 08:36:41'),
(23, 'grliw', 'vrbsh', 'lazyx', 'Viking Eau de Parfum', '022', 'bedsz', 'gfqra', '', '', 18747, NULL, NULL, NULL, '', '', '', 'Women', '<p>Bold, fearless and determined, the house of&nbsp;<a href=\"https://www.selfridges.com/GB/en/cat/creed/\"><strong>Creed&nbsp;</strong></a>introduces the masterfully crafted&nbsp;<strong>Viking Eau de Parfum</strong>&nbsp;&ndash; a valiant fragrance that bottles the fearless spirit of the modern man. Enthused by longships, a masterpiece of the Viking age and one of the greatest feats of the ninth century, it&rsquo;s a symbol of perseverance for the determined conqueror. Notes include natural accords of Indian sandalwood, Haitian veviter, alongside patchouli and Bulgarian rose.</p>', '<p><strong>Creed&nbsp;</strong>Viking Eau de Parfum<br />Top notes: Calabrian bergamot Sicilian lemon La Reunion baie rose (pink peppercorn)<br />Heart notes: Peppercorn Bulgarian rose Peppermint<br />Base notes: Indian Sandalwood Haitian Vetiver Indian Patchouli Lavandin Absolute<br />Available in: 50ml, 100ml</p>', '', '0', '', 1, 'primary_20200611141104.png', '', ',', '', '', '', 3, '2020-06-11 21:11:04', '2020-06-11 08:41:04'),
(24, 'zkqal', 'vrbsh', 'lazyx', 'Original Vetiver Eau de Parfum', '022', 'bedsz', 'gfqra', '', '', 15863, NULL, NULL, NULL, '', '', '', 'Women', '<p>True to its name,<a href=\"https://www.selfridges.com/GB/en/cat/creed/\"><strong>&nbsp;Creed</strong></a>&rsquo;s<strong>&nbsp;Original Vetiver Eau de Parfum</strong>&nbsp;dramatically reinvents the traditional vetiver scent. Before Original Vetiver, only one part of the vetiver plant was used in a fragrance. The House of Creed infuses all three parts of the plant: the earthy root, the verdant leaves, and the rich heart to freshen the blend. The result is a scent reminiscent of lingering summers, invigorating and vivacious - it leaves an alluring air of freshness.</p>', '<p><strong>Creed&nbsp;</strong>Original Vetiver Eau de Parfum<br />Top notes: Bergamot, bitter orange, mandarin, vetiver leaves<br />Heart notes: White pepper, coriander, pink pepper<br />Base notes: Mysore sandalwood, vetiver, ambergris, tonkin musk<br />Available in: 50ml, 100ml</p>', '', '0', '', 1, 'primary_20200611142053.png', '', ',', '', '', '', 3, '2020-06-11 21:20:53', '2020-06-11 08:50:53'),
(25, 'quofj', 'vrbsh', 'xueqs', 'Sequin-embellished jersey mini dress', '1190', 'saqem', 'vktjn', '', '', 124660, NULL, NULL, NULL, '', '', '', 'Women', '<p>Anthony Vaccarello transformed the runway set up opposite the Eiffel Tower for&nbsp;<a href=\"https://www.selfridges.com/GB/en/cat/saint-laurent/\"><strong>Saint Laurent</strong></a>&rsquo;s Spring &rsquo;20 show into, according to Vogue Runway, &ldquo;something between an open-air city spectacle, a rock concert, and a brand power rally&rdquo;. With that in mind, it makes sense this mini dress calls for a dancefloor. Whipped up from soft jersey scattered with mini sequins that work their way around the body-hugging silhouette, this thigh-skimming number sparkles like the sky at night.</p>', '<p><strong>Saint Laurent</strong>&nbsp;jersey mini dress<br /><br /><strong>Details:</strong><br />Round neck<br />Long sleeves<br />All-over sequin embellishment<br />Concealed back zip closure<br />Made in Italy<br /><br /><strong>Fabric:</strong><br />100% polyamide<br />Midweight, non-stretch<br /><br /><strong>Size &amp; Fit:</strong><br />Size UK 8: Length 31in / 73cm<br />True to size<br />Cut to a bodycon silhouette, fitted through the full length<br /><br /><strong>Care:</strong><br />Specialist dry clean only</p>', '', '0', '', 1, 'primary_20200619123446.png', 'second_20200619123446.png', '=other1_20200619123446.png=products/other1_20200619123446.png,', '', '', '', 3, '2020-06-19 19:34:46', '2020-06-19 07:04:46');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `pa_id` int(255) NOT NULL,
  `p_id` varchar(255) NOT NULL,
  `attribute` varchar(255) DEFAULT NULL,
  `storage` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `imgid` int(255) DEFAULT NULL,
  `sample` text DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`pa_id`, `p_id`, `attribute`, `storage`, `color`, `size`, `groupid`, `imgid`, `sample`, `gender`, `created_at`, `updated_at`) VALUES
(63, 'MOON-B-001', NULL, NULL, 'Navy', '', 'MOON-B-001', NULL, 'clrsample_20200809210823.jpg', '', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(64, 'MOON-B-002', NULL, NULL, 'COGNAC', '', 'MOON-B-002', NULL, 'clrsample_20200809211501.jpg', '', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(65, 'MOON-T-001', NULL, NULL, 'GREEN', '6', 'MOON-T-001', NULL, 'clrsample_20200809212850.jpg', 'Women', '2020-08-09 15:58:50', '2020-08-09 16:28:00'),
(66, 'MOON-T-002', NULL, NULL, 'GREEN', '8', 'MOON-T-001', NULL, 'clrsample_20200809212850.jpg', 'Women', '2020-08-09 16:26:10', '2020-08-09 16:28:00'),
(67, 'MOON0058', NULL, NULL, 'Red', 'L', 'MOON0058', NULL, 'clrsample_20200810195003.jpg', 'Women', '2020-08-11 02:50:03', '2020-08-10 14:20:03'),
(68, 'MOON-AN-001', NULL, NULL, 'NERO GRIGIO', '7', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-12 00:00:46', '2020-08-16 08:33:47'),
(69, 'MOON0090', NULL, NULL, '', '', 'MOON0090', NULL, '', '', '2020-08-13 05:25:29', '2020-08-12 16:55:29'),
(70, 'MOON0091', NULL, '', 'NAVY', '7', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-13 05:51:08', '2020-09-03 18:50:41'),
(71, 'MOON0092', NULL, NULL, 'Red', 'R48', 'MOON0092', NULL, 'clrsample_20200813135205.jpg', 'Men', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(72, 'MOON0093', NULL, '', 'TOBACCOBIS', 'TU', 'MOON0093', NULL, 'clrsample_20200813163458.jpg', 'Men', '2020-08-13 23:34:58', '2020-09-04 06:33:14'),
(73, 'MOON0094', NULL, '', 'TOBACCOBIS', 'TU', 'MOON0094', NULL, 'clrsample_20200813170746.jpg', 'Men', '2020-08-14 00:07:46', '2020-09-04 06:48:55'),
(74, 'MOON0095', NULL, '', 'EARTHBROWN', 'TU', 'MOON0095', NULL, 'clrsample_20200813172448.jpg', 'Men', '2020-08-14 00:24:48', '2020-09-04 07:09:57'),
(75, 'MOON0096', NULL, '', 'BLACK+TDMINTENSO', 'TU', 'MOON0096', NULL, 'clrsample_20200813180439.jpg', 'Men', '2020-08-14 01:03:30', '2020-09-04 05:22:22'),
(76, 'MOON0097', NULL, '', 'EBANO', 'TU', 'MOON0097', NULL, 'clrsample_20200813211528.jpg', 'Men', '2020-08-14 04:15:28', '2020-09-04 06:21:28'),
(77, 'MOON0099', NULL, '', 'TOBACCOBIS', 'TU', 'MOON0099', NULL, 'clrsample_20200813214355.jpg', 'Men', '2020-08-14 04:43:55', '2020-09-03 18:11:20'),
(78, 'MOON0011', NULL, '', 'TOBACCOBIS', 'TU', 'MOON0011', NULL, 'clrsample_20200813215155.jpg', 'Men', '2020-08-14 04:51:55', '2020-09-03 18:17:32'),
(79, 'MOON0012', NULL, '', 'MOGANO', 'TU', 'MOON0012', NULL, 'clrsample_20200813222039.jpg', 'Men', '2020-08-14 05:20:39', '2020-09-03 18:14:19'),
(80, 'MOON0013', NULL, '', 'DEEPTDM&BLACK', 'TU', 'MOON0013', NULL, 'clrsample_20200813222632.jpg', 'Men', '2020-08-14 05:26:32', '2020-09-04 05:27:14'),
(81, 'MOON0014', NULL, NULL, 'TDM SCURO', 'TU', 'MOON0014', NULL, 'clrsample_20200813223448.jpg', 'Men', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(82, 'MOON0016', NULL, '', 'MOGANO', 'TU', 'MOON0016', NULL, 'clrsample_20200813230034.jpg', 'Men', '2020-08-14 06:00:33', '2020-09-04 06:27:14'),
(83, 'MOON0017', NULL, '', 'NAVY', '7.5', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-14 17:11:58', '2020-09-04 05:02:28'),
(84, 'MOON0018', NULL, NULL, 'NAVY', '8', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(85, 'MOON0019', NULL, NULL, 'NAVY', '8.5', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(86, 'MOON0020', NULL, NULL, 'NAVY', '9', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(87, 'MOON0021', NULL, NULL, 'NAVY', '9.5', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(88, 'MOON0022', NULL, NULL, 'White', '8', 'MOON0091', NULL, 'clrsample_20200814113809.jpg', 'Men', '2020-08-14 18:38:09', '2020-08-14 06:08:09'),
(89, 'MOON0023', NULL, NULL, 'MOGANO', 'TU', 'MOON0023', NULL, 'clrsample_20200815230800.jpg', '', '2020-08-16 06:03:40', '2020-08-15 17:38:00'),
(90, 'MOON0024', NULL, NULL, 'MOGANO', 'TU', 'MOON0024', NULL, 'clrsample_20200815232350.jpg', 'Women', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(91, 'MOON0025', NULL, NULL, 'BLACK+TDM INTENSO', 'TU', 'MOON0025', NULL, NULL, 'Women', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(92, 'MOON0026', NULL, NULL, 'CAMEL', 'TU', 'MOON0026', NULL, 'clrsample_20200815235107.jpg', '', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(93, 'MOON0027', NULL, NULL, 'LIME/BLUE', '7', 'MOON0027', NULL, 'clrsample_20200816000620.jpg', 'Men', '2020-08-16 07:06:20', '2020-08-16 07:45:44'),
(94, 'MOON0028', NULL, NULL, 'BLACK+TDM INTENSO', 'TU', 'MOON0028', NULL, 'clrsample_20200816001510.jpg', '', '2020-08-16 07:15:10', '2020-08-15 18:45:10'),
(95, 'MOON0029', NULL, NULL, 'BLACK+TDM INTENSO', 'TU', 'MOON0029', NULL, 'clrsample_20200816002239.jpg', '', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(96, 'MOON0030', NULL, NULL, 'BLACK+TDM INTENSO', 'TU', 'MOON0030', NULL, 'clrsample_20200816002813.jpg', '', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(97, 'MOON0032', NULL, NULL, 'BLACK+TDM INTENSO', 'TU', 'MOON0032', NULL, 'clrsample_20200816134111.jpg', '', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(98, 'MOON0033', NULL, NULL, 'NAVY', '10', 'MOON0091', NULL, 'clrsample_20200812225108.jpg', 'Men', '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(99, 'MOON0035', NULL, NULL, 'NERO GRIGIO', '7.5', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:16:12', '2020-08-16 08:46:12'),
(100, 'MOON0036', NULL, NULL, 'NERO GRIGIO', '8', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:17:55', '2020-08-16 08:47:55'),
(101, 'MOON0037', NULL, NULL, 'NERO GRIGIO', '8.5', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:20:06', '2020-08-16 08:50:06'),
(102, 'MOON0038', NULL, NULL, 'NERO GRIGIO', '9', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:23:00', '2020-08-16 08:53:00'),
(103, 'MOON0039', NULL, NULL, 'NERO GRIGIO', '9.5', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:24:30', '2020-08-16 08:54:30'),
(104, '213035', NULL, NULL, 'NERO GRIGIO', '10', 'MOON-AN-001', NULL, 'clrsample_20200811170046.jpg', 'Men', '2020-08-16 21:26:32', '2020-08-16 08:56:32'),
(105, 'MWHD2HN/A', NULL, '', 'SPACEGREY', '', 'MWHD2HN/A', NULL, 'clrsample_20200825143226.jpg', '', '2020-08-25 21:32:26', '2020-08-31 10:12:59'),
(106, 'MVH22HN/A', NULL, '512GB', 'Space Grey', '', 'MVH22HN/A', NULL, 'clrsample_20200826182806.jpg', '', '2020-08-27 01:28:06', '2020-08-26 13:04:25'),
(107, 'Mac0001', NULL, '512GB', 'SPACEGREY', '', 'Mac0001', NULL, 'clrsample_20200831130223.png', '', '2020-08-27 18:06:04', '2020-08-31 09:21:43'),
(108, '1105032', NULL, NULL, '', '50ML', '1105032', NULL, '', 'Both', '2020-08-27 23:16:54', '2020-08-27 10:50:55'),
(109, '1105036', NULL, '', '', '50ML', '1105036', NULL, '', 'Both', '2020-08-27 23:30:57', '2020-08-27 11:56:01'),
(110, '1105039', NULL, '', '', '50ML', '1105039', NULL, '', 'Both', '2020-08-28 00:03:34', '2020-08-27 11:55:43'),
(111, '1105040', NULL, '', '', '50ML', '1105040', NULL, '', 'Both', '2020-08-28 00:22:17', '2020-08-27 11:57:10'),
(112, '1105041', NULL, '', '', '50ML', '1105041', NULL, '', 'Both', '2020-08-28 00:42:37', '2020-08-27 12:13:44'),
(113, '1320032', NULL, '', '', '200GM', '1320032', NULL, '', '', '2020-08-28 01:14:37', '2020-08-27 12:44:37'),
(114, 'SK001', NULL, '', 'BLACK', '', 'SK001', NULL, 'clrsample_20200830184849.jpg', 'Women', '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(115, 'PR001', NULL, '', 'NATURALTAN', '', 'PR001', NULL, 'clrsample_20200830192019.jpg', 'Women', '2020-08-31 02:13:31', '2020-08-30 13:50:19'),
(116, 'GRU001', NULL, '', 'WHITE', '', 'GRU001', NULL, 'clrsample_20200830191416.jpg', 'Women', '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(117, 'GRU002', NULL, '', 'BLACK', 'EUR39/5UKMEN', 'GRU002', NULL, 'clrsample_20200830194757.jpg', 'Men', '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(118, 'PR002', NULL, '', 'DARKBROWN', 'EUR41/7UKMEN', 'PR002', NULL, 'clrsample_20200830200223.png', 'Men', '2020-08-31 03:02:23', '2020-08-30 14:32:23'),
(119, 'DIM002', NULL, '', 'WHITE', 'S', 'DIM002', NULL, 'clrsample_20200830211203.jpg', 'Women', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(120, 'DIM003', NULL, '', 'BEIGE', 'S', 'DIM003', NULL, 'clrsample_20200830212426.jpg', 'Men', '2020-08-31 04:24:26', '2020-08-30 15:54:26'),
(121, 'GRU003', NULL, '', 'PEARL', '8', 'GRU003', NULL, 'clrsample_20200830212442.jpg', 'Women', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(122, 'GRU004', NULL, '', 'PEARL', '12', 'GRU003', NULL, 'clrsample_20200830212442.jpg', 'Women', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(123, 'DIM004', NULL, '', 'BEIGE', 'L', 'DIM003', NULL, 'clrsample_20200830212426.jpg', 'Men', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(124, 'DIM005', NULL, '', 'BEIGE', 'XL', 'DIM003', NULL, 'clrsample_20200830212426.jpg', 'Men', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(125, 'DIM006', NULL, '', 'WHITE', '40', 'DIM006', NULL, 'clrsample_20200830214121.jpg', 'Men', '2020-08-31 04:41:21', '2020-08-30 16:11:21'),
(126, 'DIM007', NULL, '', 'WHITE', '42', 'DIM006', NULL, 'clrsample_20200830214121.jpg', 'Men', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(127, 'PR003', NULL, '', 'BLUE', 'IT46', 'PR003', NULL, 'clrsample_20200830221221.jpg', 'Men', '2020-08-31 05:12:21', '2020-08-30 16:42:21'),
(128, 'PR004', NULL, '', 'BLUE', 'IT47', 'PR003', NULL, 'clrsample_20200830221221.jpg', 'Men', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(129, 'GRU005', NULL, '', '', '50ML', 'GRU005', NULL, '', 'Both', '2020-08-31 05:49:27', '2020-08-30 17:19:27'),
(130, 'PR006', NULL, '', 'WHITE', 'L', 'PR006', NULL, 'clrsample_20200830225837.jpg', 'Men', '2020-08-31 05:58:37', '2020-08-30 17:28:37'),
(131, 'GRUOO6', NULL, '', '', '250ML', 'GRUOO6', NULL, '', 'Women', '2020-08-31 06:00:56', '2020-08-30 17:30:56'),
(132, 'PR005', NULL, '', 'GRAY', 'S', 'PR005', NULL, 'clrsample_20200830231323.jpg', 'Men', '2020-08-31 06:13:23', '2020-08-30 17:43:23'),
(133, 'GRU007', NULL, '', '', '10ML', 'GRU007', NULL, '', 'Women', '2020-08-31 06:13:38', '2020-08-30 17:43:38'),
(134, 'GRU008', NULL, '', '', '30ML', 'GRU008', NULL, '', 'Women', '2020-08-31 06:18:51', '2020-08-30 17:48:51'),
(135, 'GRU009', NULL, '', '', '30ML', 'GRU009', NULL, '', 'Women', '2020-08-31 06:21:40', '2020-08-30 17:51:40'),
(136, 'GRU0010', NULL, '', '', '75ML', 'GRU009', NULL, NULL, 'Women', '2020-08-31 06:23:18', '2020-08-30 17:53:18'),
(137, 'PR007', NULL, '', 'GRAY', 'IT46', 'PR007', NULL, 'clrsample_20200830233137.jpg', 'Men', '2020-08-31 06:31:37', '2020-08-30 18:01:38'),
(138, 'PR008', NULL, '', 'GRAY', 'IT48', 'PR007', NULL, 'clrsample_20200830233137.jpg', 'Men', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(139, 'PR009', NULL, '', 'GRAY', 'IT50', 'PR007', NULL, 'clrsample_20200830233137.jpg', 'Men', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(140, 'GRU0011', NULL, '', '', '50ML', 'GRU0011', NULL, '', 'Men', '2020-08-31 06:35:03', '2020-08-30 18:05:03'),
(141, 'PR0010', NULL, '', 'GRAY', 'IT52', 'PR007', NULL, 'clrsample_20200830233137.jpg', 'Men', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(142, 'PR0011', NULL, '', 'GRAY', 'IT56', 'PR007', NULL, 'clrsample_20200830233137.jpg', 'Men', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(143, 'PR0012', NULL, '', 'GRAY', 'ONESIZE', 'PR0012', NULL, 'clrsample_20200830234729.jpg', 'Men', '2020-08-31 06:47:29', '2020-08-30 18:17:29'),
(144, 'GRU0012', NULL, '', 'BLACK', '', 'GRU0012', NULL, 'clrsample_20200830235437.jpg', 'Women', '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(145, 'PR0013', NULL, '', 'TAN', 'ONESIZE', 'PR0013', NULL, 'clrsample_20200831000247.jpg', 'Both', '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(146, 'GRU0013', NULL, '', 'BLACK', '', 'GRU0013', NULL, 'clrsample_20200831000811.jpg', 'Women', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(147, 'PR0015', NULL, '', 'TAN', 'ONESIZE', 'PR0015', NULL, 'clrsample_20200831001902.jpg', 'Men', '2020-08-31 07:19:02', '2020-08-30 18:49:02'),
(148, 'GRU0015', NULL, '', 'BLACK', '', 'GRU0015', NULL, 'clrsample_20200831002319.jpg', 'Women', '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(149, 'GRU0016', NULL, '', 'NOIR/CAMEL/MARRON', 'M', 'GRU0016', NULL, 'clrsample_20200831002937.jpg', 'Women', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(150, 'PR0016', NULL, '', 'GRAY', 'IT46', 'PR0016', NULL, 'clrsample_20200831003200.jpg', 'Men', '2020-08-31 07:32:00', '2020-08-30 19:02:00'),
(151, 'PR0017', NULL, '', 'ARMYGREEN', 'IT44', 'PR0016', NULL, 'clrsample_20200831003653.jpg', 'Men', '2020-08-31 07:36:53', '2020-08-30 19:06:53'),
(152, 'GRU0017', NULL, '', 'NOIR/CAMEL/MARRON', 'L', 'GRU0016', NULL, 'clrsample_20200831002937.jpg', 'Women', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(153, 'GRU0018', NULL, '', 'NOIR/MULTICOLORE', 'S', 'GRU0018', NULL, 'clrsample_20200831005419.jpg', 'Men', '2020-08-31 07:54:19', '2020-08-30 19:24:19'),
(154, 'PR0018', NULL, '', 'BROWN', 'ONESIZE', 'PR0018', NULL, 'clrsample_20200831005646.jpg', 'Both', '2020-08-31 07:56:46', '2020-08-30 19:26:46'),
(155, 'GRU0019', NULL, '', 'NOIR/MULTICOLORE', 'L', 'GRU0018', NULL, 'clrsample_20200831005419.jpg', 'Men', '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(156, 'PR0019', NULL, '', 'BROWN', 'IT46', 'PR0019', NULL, 'clrsample_20200831010847.jpg', 'Men', '2020-08-31 08:08:47', '2020-08-31 07:44:54'),
(157, 'GRU0020', NULL, '', 'BLUE', 'EUR36/3UKWOMEN', 'GRU0020', NULL, 'clrsample_20200831011100.jpg', 'Women', '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(158, 'GRU0021', NULL, '', 'BLUE', 'EUR41/8UKWOMEN', 'GRU0020', NULL, 'clrsample_20200831011100.jpg', 'Women', '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(159, 'PR0020', NULL, '', 'BROWN', 'IT48', 'PR0019', NULL, 'clrsample_20200831010847.jpg', 'Men', '2020-08-31 08:16:27', '2020-08-31 07:45:16'),
(160, 'GRU0022', NULL, '', 'SPACEGREY', '', 'GRU0022', NULL, 'clrsample_20200831013930.jpg', '', '2020-08-31 08:39:30', '2020-08-31 09:25:46'),
(161, 'PR0021', NULL, '64GB', 'YELLOW', '', 'PR0021', NULL, 'clrsample_20200831014741.jpg', '', '2020-08-31 08:47:41', '2020-08-31 09:21:43'),
(162, 'PR0022', NULL, '256GB', 'YELLOW', '', 'PR0021', NULL, 'clrsample_20200831014741.jpg', '', '2020-08-31 08:49:36', '2020-08-31 10:42:45'),
(163, 'PR0023', NULL, '256GB', 'SPACEGREY', '', 'PR0023', NULL, 'clrsample_20200831020504.jpg', '', '2020-08-31 09:05:04', '2020-09-03 11:04:27'),
(164, 'GRU0023', NULL, '64GB', 'SILVER', '', 'GRU0023', NULL, 'clrsample_20200831020525.jpg', '', '2020-08-31 09:05:25', '2020-08-31 09:21:43'),
(165, 'GRU0024', NULL, '', 'BROWN', '', 'GRU0024', NULL, 'clrsample_20200831022255.jpg', 'Women', '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(166, 'PR0024', NULL, '', 'SILVER', '', 'PR0024', NULL, 'clrsample_20200831022547.jpg', 'Both', '2020-08-31 09:25:47', '2020-08-30 20:55:47'),
(167, 'PR0025', NULL, '', 'BLACKSILVER', '', 'PR0025', NULL, 'clrsample_20200831023924.jpg', 'Both', '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(168, 'DIM008', NULL, '', 'VIEILORLAITON', '', 'DIM008', NULL, 'clrsample_20200831112025.jpg', 'Women', '2020-08-31 18:20:25', '2020-08-31 05:50:26'),
(169, 'DIM009', NULL, '', 'NOIR', '8', 'DIM009', NULL, 'clrsample_20200831120244.jpg', 'Women', '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(170, 'DIM10', NULL, '', '', '300 ML', 'DIM10', NULL, '', 'Both', '2020-08-31 23:57:07', '2020-08-31 11:33:01'),
(171, 'DIM11', NULL, '', '', '300 ML', 'DIM11', NULL, '', 'Both', '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(172, 'DIM12', NULL, '', '', '300 ML', 'DIM12', NULL, '', 'Both', '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(173, 'DIM0013', NULL, '', '', '300ML', 'DIM0013', NULL, '', 'Both', '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(174, 'DIM14', NULL, '', '', '300 ML', 'DIM14', NULL, '', 'Both', '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(175, 'DIM15', NULL, '', '', '100 ML', 'DIM15', NULL, '', 'Both', '2020-09-01 01:21:28', '2020-08-31 12:51:28'),
(176, 'DMI17', NULL, '', '', '100 ML', 'DMI17', NULL, '', 'Both', '2020-09-01 01:28:18', '2020-08-31 12:58:18'),
(177, 'PR0026', NULL, '', 'HAVANA', '', 'PR0026', NULL, NULL, 'Women', '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(178, 'GRU0025', NULL, '', 'BLACK/BROWNGLD', '', 'GRU0025', NULL, 'clrsample_20200831230739.jpg', 'Men', '2020-09-01 06:07:39', '2020-08-31 17:37:39'),
(179, 'GRU0026', NULL, '', 'BLACK', '', 'GRU0026', NULL, 'clrsample_20200831232944.jpg', 'Men', '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(180, 'PR0027', NULL, '', 'BLACK', '', 'PR0027', NULL, NULL, 'Women', '2020-09-01 06:46:35', '2020-08-31 18:16:35'),
(181, 'GRU0027', NULL, '', 'ORANGE', '', 'GRU0027', NULL, 'clrsample_20200831235909.jpg', 'Men', '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(182, 'PR0028', NULL, '', 'PINK', '', 'PR0028', NULL, 'clrsample_20200901000955.jpg', 'Women', '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(183, 'GRU0028', NULL, '', 'PINK', '', 'GRU0028', NULL, 'clrsample_20200901001522.jpg', 'Men', '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(184, 'GRU0029', NULL, '', 'BLACK', '38', 'GRU0029', NULL, 'clrsample_20200901002416.jpg', 'Men', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(185, 'PR0029', NULL, '', 'BLACK', '', 'PR0029', NULL, 'clrsample_20200901002609.jpg', 'Women', '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(186, 'GRU0030', NULL, '', 'GREY', '40', 'GRU0030', NULL, 'clrsample_20200901003625.jpg', 'Men', '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(187, 'PR0030', NULL, '', 'BROWN', '', 'PR0030', NULL, 'clrsample_20200901004043.jpg', 'Women', '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(188, 'GRU0031', NULL, '', 'PURPLE', '', 'GRU0031', NULL, 'clrsample_20200901005006.jpg', 'Men', '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(189, 'PR0031', NULL, '', 'BLACK', '', 'PR0031', NULL, 'clrsample_20200901005534.jpg', 'Women', '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(190, 'GRU0032', NULL, '', 'BROWN', '38', 'GRU0032', NULL, 'clrsample_20200901005918.jpg', 'Men', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(191, 'PR0032', NULL, '', 'BLACK', '', 'PR0032', NULL, 'clrsample_20200901010237.jpg', 'Women', '2020-09-01 08:02:37', '2020-08-31 19:32:38'),
(192, 'GRU0033', NULL, '', 'BEIGE', '42', 'GRU0033', NULL, 'clrsample_20200901011332.jpg', 'Men', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(193, 'PR0033', NULL, '', 'BLACK', '', 'PR0033', NULL, 'clrsample_20200901011335.jpg', 'Women', '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(194, 'PR0034', NULL, '', 'BLACK', '', 'PR0034', NULL, 'clrsample_20200901012025.jpg', 'Women', '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(195, 'GRU0034', NULL, '', 'DKGREEN', '', 'GRU0034', NULL, 'clrsample_20200901012340.jpg', 'Men', '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(196, 'PR0035', NULL, '', 'SILVER', '', 'PR0035', NULL, 'clrsample_20200901013351.jpg', 'Women', '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(197, 'GRU0035', NULL, '', 'BLACK/SILVER', '', 'GRU0035', NULL, 'clrsample_20200901013415.jpg', 'Men', '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(198, 'PR0036', NULL, '', 'BLACK', 'EUR 34 / 1 UK WOMEN', 'PR0036', NULL, 'clrsample_20200901130912.jpg', 'Women', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(199, 'PR0037', NULL, '', 'GREENCOMB', 'EUR 37 / 4 UK WOMEN', 'PR0037', NULL, 'clrsample_20200901132422.jpg', 'Women', '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(200, 'PR0038', NULL, '', 'BLACK', 'EUR 35 / 2 UK WOMEN', 'PR0038', NULL, 'clrsample_20200901134854.jpg', 'Women', '2020-09-01 20:48:54', '2020-09-01 08:18:54'),
(201, 'PR0039', NULL, '', 'NUDE', 'EUR 35 / 2 UK WOMEN', 'PR0039', NULL, 'clrsample_20200901140557.jpg', 'Women', '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(202, 'PR0040', NULL, '', 'GOLD', 'EUR 40 / 7 UK WOMEN', 'PR0040', NULL, 'clrsample_20200901141549.jpg', 'Women', '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(203, 'GRU0036', NULL, '', 'BLACKWHT', '', 'GRU0036', NULL, 'clrsample_20200901143329.jpg', 'Men', '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(204, 'GRU0037', NULL, '', 'NAVY', '', 'GRU0036', NULL, 'clrsample_20200901144441.jpg', 'Men', '2020-09-01 21:44:41', '2020-09-01 09:14:41'),
(205, 'GRU0038', NULL, '', 'GREY', '15.5', 'GRU0038', NULL, 'clrsample_20200901150315.jpg', 'Men', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(206, 'GRU0039', NULL, '', 'GREY', '15.75 ', 'GRU0038', NULL, 'clrsample_20200901150315.jpg', 'Men', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(207, 'GRU0040', NULL, '', 'BLACK', '34', 'GRU0040', NULL, 'clrsample_20200901152046.jpg', 'Men', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(208, 'GRU0041', NULL, '', 'BLACK', '36', 'GRU0040', NULL, 'clrsample_20200901152046.jpg', 'Men', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(209, 'PR0041', NULL, '', 'GOLD', 'EUR 36 / 3 UK WOMEN', 'PR0041', NULL, 'clrsample_20200901154330.jpg', 'Women', '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(210, 'PR0042', NULL, '', 'BLACK/GOLD', '', 'PR0042', NULL, 'clrsample_20200901215945.jpg', 'Women', '2020-09-02 04:59:45', '2020-09-01 16:29:45'),
(211, 'PR0043', NULL, '', 'ROUGEOPIUM', '', 'PR0043', NULL, 'clrsample_20200901224250.jpg', 'Women', '2020-09-02 05:08:13', '2020-09-01 17:12:50'),
(212, 'PR0044', NULL, '', 'BROWN', '', 'PR0044', NULL, 'clrsample_20200901222045.jpg', 'Women', '2020-09-02 05:20:45', '2020-09-01 16:50:45'),
(213, 'GRU0042', NULL, '', 'BLACK', 'EUR 36 / 3 UK WOMEN', 'GRU0042', NULL, 'clrsample_20200901223024.jpg', 'Women', '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(214, 'PR0045', NULL, '', 'NUDE', 'EUR 40 / 7 UK WOMEN', 'PR0045', NULL, 'clrsample_20200901230500.jpg', 'Women', '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(215, 'GRU0043', NULL, '', 'BEIGE', 'EUR 36.5 / 3.5 UK WOMEN', 'GRU0043', NULL, 'clrsample_20200901230547.jpg', 'Women', '2020-09-02 06:05:47', '2020-09-01 17:35:47'),
(216, 'GRU0044', NULL, '', 'BEIGE', 'EUR 35 / 2 UK WOMEN', 'GRU0043', NULL, 'clrsample_20200901230547.jpg', 'Women', '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(217, 'PR0046', NULL, '', 'BLACK', 'EUR 37 / 4 UK WOMEN', 'PR0046', NULL, 'clrsample_20200901231212.jpg', 'Women', '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(218, 'GRU0045', NULL, '', 'BLACK', 'EUR 38/5 UK WOMEN', 'GRU0045', NULL, 'clrsample_20200901231823.jpg', 'Women', '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(219, 'GRU0046', NULL, '', 'BLACK', 'EUR 39 / 6 UK WOMEN', 'GRU0045', NULL, 'clrsample_20200901231823.jpg', 'Women', '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(220, 'PR0047', NULL, '', 'BLACK', '', 'PR0047', NULL, 'clrsample_20200901232902.jpg', 'Women', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(221, 'PR0048', NULL, '', 'WHITE', '', 'PR0048', NULL, 'clrsample_20200901233543.jpg', 'Women', '2020-09-02 06:35:43', '2020-09-01 18:05:43'),
(222, 'PR0049', NULL, '', 'LEOPARD', '', 'PR0049', NULL, 'clrsample_20200901234337.jpg', 'Women', '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(223, 'PR0050', NULL, '', 'PEBBLEGREY', '', 'PR0050', NULL, 'clrsample_20200901235219.jpg', 'Women', '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(224, 'GRU0048', NULL, '', 'BROWN/OTH', 'EUR 35 / 2 UK WOMEN', 'GRU0048', NULL, 'clrsample_20200903164515.jpg', 'Women', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(225, 'GRU0049', NULL, '', 'WHITE', 'EUR 36 / 3 UK WOMEN', 'GRU0049', NULL, 'clrsample_20200903165332.jpg', 'Women', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(226, 'GRU0050', NULL, '', 'BLACK', 'EUR 34.5 / 1.5 UK WOMEN', 'GRU0050', NULL, 'clrsample_20200903165903.jpg', 'Women', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(227, 'GRU0051', NULL, '', 'BLACK', 'EUR 36 / 3 UK WOMEN', 'GRU0050', NULL, 'clrsample_20200903165903.jpg', 'Women', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(228, 'GRU0052', NULL, '', 'BLACK', 'EUR 41 / 7 UK MEN', 'GRU0052', NULL, 'clrsample_20200903172157.jpg', 'Both', '2020-09-04 00:21:57', '2020-09-03 12:00:08'),
(229, 'GRU0053', NULL, '', 'BROWN', 'EUR 39 / 5 UK MEN', 'GRU0053', NULL, 'clrsample_20200903175104.jpg', 'Men', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(230, 'GRU0054', NULL, '', 'BROWN', 'EUR 41.5 / 7.5 UK MEN', 'GRU0053', NULL, 'clrsample_20200903175104.jpg', 'Men', '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(231, 'GRU0055', NULL, '', 'BLUE', 'EUR 40 / 6 UK MEN', 'GRU0055', NULL, 'clrsample_20200903180001.jpg', 'Men', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(232, 'GRU0056', NULL, '', 'BLUE', 'EUR 43 / 9 UK MEN', 'GRU0055', NULL, 'clrsample_20200903180001.jpg', 'Men', '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(233, 'GRU0057', NULL, '', 'BROWN', 'EUR 41 / 7 UK MEN', 'GRU0057', NULL, 'clrsample_20200903181238.jpg', 'Men', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(234, 'GRU0058', NULL, '', 'BROWN', 'EUR 43 / 9 UK MEN', 'GRU0057', NULL, 'clrsample_20200903181238.jpg', 'Men', '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(235, 'GRU0059', NULL, '', 'BLACK', 'EUR 42.5 / 8.5 UK MEN', 'GRU0059', NULL, 'clrsample_20200903182220.jpg', 'Men', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(236, 'GRU0060', NULL, '', 'BLACK', 'EUR 40 / 6 UK MEN', 'GRU0059', NULL, 'clrsample_20200903182220.jpg', 'Men', '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(237, 'PR0051', NULL, '', 'GOLD', 'EUR 36.5 / 3.5 UK WOMEN', 'PR0041', NULL, 'clrsample_20200901154330.jpg', 'Women', '2020-09-04 06:42:18', '2020-09-03 18:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `pc_id` int(255) NOT NULL,
  `product` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `p_img` text NOT NULL,
  `s_img` text DEFAULT NULL,
  `o_img` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_detail`
--

CREATE TABLE `product_detail` (
  `pd_id` int(255) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `qty` int(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_detail`
--

INSERT INTO `product_detail` (`pd_id`, `product`, `color`, `size`, `qty`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'bodzx', 'BROWN ', '10', 1, '', '', '2020-06-10 08:25:51', '2020-06-09 19:55:51'),
(2, 'gdtfb', 'BROWN', '', 1, '', '', '2020-06-10 08:33:29', '2020-06-09 20:03:30'),
(3, 'weqnd', 'WHITE', '', 1, '', '', '2020-06-10 08:44:07', '2020-06-09 20:14:07'),
(4, 'uwbqo', 'BLACK', 'EUR 42 / 8 UK MEN', 1, '', '', '2020-06-10 09:13:38', '2020-06-09 20:43:38'),
(5, 'uwbqo', 'BLACK', 'EUR 43 / 9 UK MEN', 1, '', '', '2020-06-10 09:13:38', '2020-06-09 20:43:38'),
(6, 'bjkfm', 'NAVY', 'EUR 41 / 7 UK MEN', 1, '', '', '2020-06-10 19:25:38', '2020-06-10 06:55:39'),
(7, 'bjkfm', 'NAVY', 'EUR 40 / 6 UK MEN', 1, '', '', '2020-06-10 19:25:38', '2020-06-10 06:55:39'),
(8, 'bjkfm', 'NAVY', 'EUR 39 / 5 UK MEN', 1, '', '', '2020-06-10 19:25:38', '2020-06-10 06:55:39'),
(9, 'zjavh', 'NAVY', 'EUR 41 / 7 UK MEN', 1, '', '', '2020-06-10 20:07:27', '2020-06-10 07:37:27'),
(10, 'zjavh', 'NAVY', 'EUR 40 / 6 UK MEN', 1, '', '', '2020-06-10 20:07:27', '2020-06-10 07:37:27'),
(11, 'zjavh', 'NAVY', 'EUR 39 / 5 UK MEN', 1, '', '', '2020-06-10 20:07:27', '2020-06-10 07:37:27'),
(12, 'vprfd', '', '300ML', 4, '', '', '2020-06-11 02:56:40', '2020-06-10 14:26:41'),
(13, 'wcdls', 'BLACK', '', 4, '', '', '2020-06-11 03:04:22', '2020-06-10 14:34:23'),
(14, 'kmvta', 'BLACK', '40', 3, '', '', '2020-06-11 03:14:39', '2020-06-10 14:44:40'),
(15, 'kmvta', 'WHITE', '38', 1, '', '', '2020-06-11 03:14:39', '2020-06-10 14:44:40'),
(16, 'yclab', 'WARM TAUPE', '', 4, '', '', '2020-06-11 03:37:05', '2020-06-10 15:07:05'),
(17, 'gyare', 'BLACK', 'EUR 35 / 2 UK WOMEN', 1, '', '', '2020-06-11 03:46:59', '2020-06-10 15:16:59'),
(18, 'gyare', 'BLACK', 'EUR 36 / 3 UK WOMEN', 1, '', '', '2020-06-11 03:46:59', '2020-06-10 15:16:59'),
(19, 'gyare', 'BLACK', 'EUR 38 / 5 UK WOMEN', 1, '', '', '2020-06-11 03:46:59', '2020-06-10 15:16:59'),
(20, 'svwxz', 'DARK BROWN', 'EUR 42 / 8 UK MEN ', 1, '', '', '2020-06-11 18:23:23', '2020-06-11 05:53:24'),
(21, 'svwxz', 'DARK BROWN', 'EUR 43 / 9 UK MEN', 1, '', '', '2020-06-11 18:23:23', '2020-06-11 05:53:24'),
(22, 'crtxy', 'WHITE PARCHMENT', '', 1, '', '', '2020-06-11 18:36:08', '2020-06-11 06:06:08'),
(23, 'cnypt', 'BLUE SUNRAY', '', 1, '', '', '2020-06-11 18:39:03', '2020-06-11 06:09:04'),
(24, 'cdxgn', 'GREEN COMB', 'EUR 37 / 4 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(25, 'cdxgn', 'GREEN COMB', 'EUR 37.5 / 4.5 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(26, 'cdxgn', 'GREEN COMB', 'EUR 36 / 3 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(27, 'cdxgn', 'GREEN COMB', 'EUR 38.5 / 5.5 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(28, 'cdxgn', 'GREEN COMB', 'EUR 39 / 6 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(29, 'cdxgn', 'GREEN COMB', 'EUR 40 / 7 UK WOMEN', 1, '', '', '2020-06-11 18:51:46', '2020-06-11 06:21:46'),
(30, 'rdabs', 'BLACK MATTE VELVET', '', 1, '', '', '2020-06-11 19:17:06', '2020-06-11 06:47:06'),
(31, 'hjmry', 'BLACK MATTE VELVET', '', 1, '', '', '2020-06-11 19:57:15', '2020-06-11 07:27:15'),
(32, 'juxvs', 'BLACK GLOSSY', '', 1, '', '', '2020-06-11 20:17:49', '2020-06-11 07:47:49'),
(33, 'bslno', 'MED BROWN', '8', 1, '', '', '2020-06-11 20:33:11', '2020-06-11 08:03:12'),
(34, 'bslno', 'MED BROWN', '10', 1, '', '', '2020-06-11 20:33:11', '2020-06-11 08:03:12'),
(35, 'togbz', '', '', 1, '', '', '2020-06-11 20:52:04', '2020-06-11 08:22:04'),
(36, 'gjcrx', '', '30ML', 1, '', '', '2020-06-11 20:59:05', '2020-06-11 08:29:06'),
(37, 'gjcrx', '', '75ML', 1, '', '', '2020-06-11 20:59:05', '2020-06-11 08:29:06'),
(38, 'kfexh', '', '75ML', 3, '', '', '2020-06-11 21:06:41', '2020-06-11 08:36:41'),
(39, 'grliw', '', '50ML', 1, '', '', '2020-06-11 21:11:04', '2020-06-11 08:41:04'),
(40, 'grliw', '', '100ML', 1, '', '', '2020-06-11 21:11:04', '2020-06-11 08:41:04'),
(41, 'zkqal', '', '50ML', 3, '', '', '2020-06-11 21:20:53', '2020-06-11 08:50:53'),
(42, 'quofj', 'BLEU/MULTICOLORE', 'XS', 1, '', '', '2020-06-19 19:34:46', '2020-06-19 07:04:46'),
(43, 'quofj', 'BLEU/MULTICOLORE', 'S', 1, '', '', '2020-06-19 19:34:46', '2020-06-19 07:04:46'),
(44, 'quofj', 'BLEU/MULTICOLORE', 'M', 1, '', '', '2020-06-19 19:34:46', '2020-06-19 07:04:46');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `pi_id` int(255) NOT NULL,
  `product` varchar(200) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `product`, `image`, `created_at`, `updated_at`) VALUES
(3, 'MOON-B-001', 'product_MOON-B-001_1_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(4, 'MOON-B-001', 'product_MOON-B-001_2_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(5, 'MOON-B-001', 'product_MOON-B-001_3_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(6, 'MOON-B-001', 'product_MOON-B-001_4_20200809210823.jpg', '2020-08-09 15:38:23', '2020-08-09 15:38:23'),
(7, 'MOON-B-002', 'product_MOON-B-002_1_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(8, 'MOON-B-002', 'product_MOON-B-002_2_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(9, 'MOON-B-002', 'product_MOON-B-002_3_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(10, 'MOON-B-002', 'product_MOON-B-002_4_20200809211501.jpg', '2020-08-09 15:45:01', '2020-08-09 15:45:01'),
(11, 'MOON-B-002', 'product_MOON-B-002_1_20200809211707.jpg', '2020-08-09 15:47:07', '2020-08-09 15:47:07'),
(12, 'MOON-T-001', 'product_MOON-T-001_1_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(13, 'MOON-T-001', 'product_MOON-T-001_2_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(14, 'MOON-T-001', 'product_MOON-T-001_3_20200809212850.jpg', '2020-08-09 15:58:50', '2020-08-09 15:58:50'),
(15, 'MOON-T-002', 'product_MOON-T-001_1_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(16, 'MOON-T-002', 'product_MOON-T-001_2_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(17, 'MOON-T-002', 'product_MOON-T-001_3_20200809212850.jpg', '2020-08-09 16:26:10', '2020-08-09 16:26:10'),
(18, 'MOON0058', 'product_MOON0058_1_20200810195003.jpg', '2020-08-11 02:50:03', '2020-08-10 14:20:03'),
(19, 'MOON-AN-001', 'product_213035_1_20200818175339.png', '2020-08-12 00:00:46', '2020-08-18 12:39:53'),
(20, 'MOON-AN-001', 'product_213035_2_20200818175340.png', '2020-08-12 00:00:46', '2020-08-18 12:40:24'),
(21, 'MOON-AN-001', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-12 00:00:46', '2020-08-11 11:30:46'),
(22, 'MOON0090', 'product_MOON0090_1_20200812222529.jpg', '2020-08-13 05:25:29', '2020-08-12 16:55:29'),
(23, 'MOON0090', 'product_MOON0090_2_20200812222529.jpg', '2020-08-13 05:25:29', '2020-08-12 16:55:29'),
(24, 'MOON0090', 'product_MOON0090_3_20200812222529.jpg', '2020-08-13 05:25:29', '2020-08-12 16:55:29'),
(25, 'MOON0090', 'product_MOON0090_4_20200812222529.jpg', '2020-08-13 05:25:29', '2020-08-12 16:55:29'),
(30, 'MOON0092', 'product_MOON0092_1_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(31, 'MOON0092', 'product_MOON0092_2_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(32, 'MOON0092', 'product_MOON0092_3_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(33, 'MOON0092', 'product_MOON0092_4_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(34, 'MOON0092', 'product_MOON0092_5_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(35, 'MOON0092', 'product_MOON0092_6_20200813135205.jpg', '2020-08-13 20:52:05', '2020-08-13 08:22:05'),
(78, 'MOON0014', 'product_MOON0014_1_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(79, 'MOON0014', 'product_MOON0014_2_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(80, 'MOON0014', 'product_MOON0014_3_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(81, 'MOON0014', 'product_MOON0014_4_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(82, 'MOON0014', 'product_MOON0014_5_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(83, 'MOON0014', 'product_MOON0014_6_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(84, 'MOON0014', 'product_MOON0014_7_20200813223448.jpg', '2020-08-14 05:34:48', '2020-08-13 17:04:48'),
(95, 'MOON0018', 'product_MOON0091_1_20200812225108.jpg', '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(96, 'MOON0018', 'product_MOON0091_2_20200812225108.jpg', '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(97, 'MOON0018', 'product_MOON0091_3_20200812225108.jpg', '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(98, 'MOON0018', 'product_MOON0091_4_20200812225108.jpg', '2020-08-14 17:32:31', '2020-08-14 05:02:31'),
(99, 'MOON0019', 'product_MOON0091_1_20200812225108.jpg', '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(100, 'MOON0019', 'product_MOON0091_2_20200812225108.jpg', '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(101, 'MOON0019', 'product_MOON0091_3_20200812225108.jpg', '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(102, 'MOON0019', 'product_MOON0091_4_20200812225108.jpg', '2020-08-14 17:34:59', '2020-08-14 05:04:59'),
(103, 'MOON0020', 'product_MOON0091_1_20200812225108.jpg', '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(104, 'MOON0020', 'product_MOON0091_2_20200812225108.jpg', '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(105, 'MOON0020', 'product_MOON0091_3_20200812225108.jpg', '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(106, 'MOON0020', 'product_MOON0091_4_20200812225108.jpg', '2020-08-14 17:38:34', '2020-08-14 05:08:34'),
(107, 'MOON0021', 'product_MOON0091_1_20200812225108.jpg', '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(108, 'MOON0021', 'product_MOON0091_2_20200812225108.jpg', '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(109, 'MOON0021', 'product_MOON0091_3_20200812225108.jpg', '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(110, 'MOON0021', 'product_MOON0091_4_20200812225108.jpg', '2020-08-14 17:47:28', '2020-08-14 05:17:28'),
(122, 'MOON0023', 'product_MOON0023_1_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(123, 'MOON0023', 'product_MOON0023_2_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(124, 'MOON0023', 'product_MOON0023_3_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(125, 'MOON0023', 'product_MOON0023_4_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(126, 'MOON0023', 'product_MOON0023_5_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(127, 'MOON0023', 'product_MOON0023_6_20200815230800.jpg', '2020-08-16 06:08:00', '2020-08-15 17:38:00'),
(128, 'MOON0024', 'product_MOON0024_1_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(129, 'MOON0024', 'product_MOON0024_2_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(130, 'MOON0024', 'product_MOON0024_3_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(131, 'MOON0024', 'product_MOON0024_4_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(132, 'MOON0024', 'product_MOON0024_5_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(133, 'MOON0024', 'product_MOON0024_6_20200815232350.jpg', '2020-08-16 06:23:50', '2020-08-15 17:53:50'),
(134, 'MOON0025', 'product_MOON0025_1_20200815233720.jpg', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(135, 'MOON0025', 'product_MOON0025_2_20200815233720.jpg', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(136, 'MOON0025', 'product_MOON0025_3_20200815233720.jpg', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(137, 'MOON0025', 'product_MOON0025_4_20200815233720.jpg', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(138, 'MOON0025', 'product_MOON0025_5_20200815233720.jpg', '2020-08-16 06:37:20', '2020-08-15 18:07:20'),
(139, 'MOON0026', 'product_MOON0026_1_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(140, 'MOON0026', 'product_MOON0026_2_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(141, 'MOON0026', 'product_MOON0026_3_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(142, 'MOON0026', 'product_MOON0026_4_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(143, 'MOON0026', 'product_MOON0026_5_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(144, 'MOON0026', 'product_MOON0026_6_20200815235108.jpg', '2020-08-16 06:51:07', '2020-08-15 18:21:08'),
(151, 'MOON0028', 'product_MOON0028_1_20200816001510.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:10'),
(152, 'MOON0028', 'product_MOON0028_2_20200816001510.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:11'),
(153, 'MOON0028', 'product_MOON0028_3_20200816001511.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:11'),
(154, 'MOON0028', 'product_MOON0028_4_20200816001511.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:11'),
(155, 'MOON0028', 'product_MOON0028_5_20200816001511.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:11'),
(156, 'MOON0028', 'product_MOON0028_6_20200816001511.jpg', '2020-08-16 07:15:10', '2020-08-15 18:45:11'),
(157, 'MOON0029', 'product_MOON0029_1_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(158, 'MOON0029', 'product_MOON0029_2_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(159, 'MOON0029', 'product_MOON0029_3_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(160, 'MOON0029', 'product_MOON0029_4_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(161, 'MOON0029', 'product_MOON0029_5_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(162, 'MOON0029', '', '2020-08-16 07:22:39', '2020-08-15 18:52:39'),
(163, 'MOON0029', 'product_MOON0029_7_20200816002239.jpg', '2020-08-16 07:22:39', '2020-08-15 18:52:40'),
(164, 'MOON0030', 'product_MOON0030_1_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(165, 'MOON0030', 'product_MOON0030_2_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(166, 'MOON0030', 'product_MOON0030_3_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(167, 'MOON0030', 'product_MOON0030_4_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(168, 'MOON0030', 'product_MOON0030_5_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(169, 'MOON0030', 'product_MOON0030_6_20200816002813.jpg', '2020-08-16 07:28:13', '2020-08-15 18:58:13'),
(170, 'MOON0032', 'product_MOON0032_1_20200816134111.jpg', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(171, 'MOON0032', 'product_MOON0032_2_20200816134111.jpg', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(172, 'MOON0032', 'product_MOON0032_3_20200816134111.jpg', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(173, 'MOON0032', 'product_MOON0032_4_20200816134111.jpg', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(174, 'MOON0032', 'product_MOON0032_5_20200816134111.jpg', '2020-08-16 20:41:11', '2020-08-16 08:11:11'),
(175, 'MOON0033', 'product_MOON0091_1_20200812225108.jpg', '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(176, 'MOON0033', 'product_MOON0091_2_20200812225108.jpg', '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(177, 'MOON0033', 'product_MOON0091_3_20200812225108.jpg', '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(178, 'MOON0033', 'product_MOON0091_4_20200812225108.jpg', '2020-08-16 20:54:36', '2020-08-16 08:24:36'),
(179, 'MOON-AN-001', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:00:34', '2020-08-16 08:30:34'),
(180, 'MOON-AN-001', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:00:34', '2020-08-16 08:30:34'),
(181, 'MOON0035', 'product_213035_1_20200818175339.png', '2020-08-16 21:16:12', '2020-08-18 12:39:53'),
(182, 'MOON0035', 'product_213035_2_20200818175340.png', '2020-08-16 21:16:12', '2020-08-18 12:40:24'),
(183, 'MOON0035', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-16 21:16:12', '2020-08-16 08:46:12'),
(184, 'MOON0035', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:16:12', '2020-08-16 08:46:12'),
(185, 'MOON0035', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:16:12', '2020-08-16 08:46:12'),
(186, 'MOON0036', 'product_213035_1_20200818175339.png', '2020-08-16 21:17:55', '2020-08-18 12:39:53'),
(187, 'MOON0036', 'product_213035_2_20200818175340.png', '2020-08-16 21:17:55', '2020-08-18 12:40:24'),
(188, 'MOON0036', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-16 21:17:55', '2020-08-16 08:47:55'),
(189, 'MOON0036', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:17:55', '2020-08-16 08:47:55'),
(190, 'MOON0036', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:17:55', '2020-08-16 08:47:55'),
(191, 'MOON0037', 'product_213035_1_20200818175339.png', '2020-08-16 21:20:06', '2020-08-18 12:39:53'),
(192, 'MOON0037', 'product_213035_2_20200818175340.png', '2020-08-16 21:20:06', '2020-08-18 12:40:24'),
(193, 'MOON0037', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-16 21:20:06', '2020-08-16 08:50:06'),
(194, 'MOON0037', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:20:06', '2020-08-16 08:50:06'),
(195, 'MOON0037', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:20:06', '2020-08-16 08:50:06'),
(196, 'MOON0038', 'product_213035_1_20200818175339.png', '2020-08-16 21:23:00', '2020-08-18 12:39:53'),
(197, 'MOON0038', 'product_213035_2_20200818175340.png', '2020-08-16 21:23:00', '2020-08-18 12:40:24'),
(198, 'MOON0038', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-16 21:23:00', '2020-08-16 08:53:00'),
(199, 'MOON0038', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:23:00', '2020-08-16 08:53:00'),
(200, 'MOON0038', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:23:00', '2020-08-16 08:53:00'),
(201, 'MOON0039', 'product_213035_1_20200818175339.png', '2020-08-16 21:24:30', '2020-08-18 12:39:53'),
(202, 'MOON0039', 'product_213035_2_20200818175340.png', '2020-08-16 21:24:30', '2020-08-18 12:40:24'),
(203, 'MOON0039', 'product_MOON-AN-001_3_20200811170046.jpg', '2020-08-16 21:24:30', '2020-08-16 08:54:30'),
(204, 'MOON0039', 'product_MOON-AN-001_1_20200816140034.jpg', '2020-08-16 21:24:30', '2020-08-16 08:54:30'),
(205, 'MOON0039', 'product_MOON-AN-001_2_20200816140034.jpg', '2020-08-16 21:24:30', '2020-08-16 08:54:30'),
(211, '213035', 'product_213035_1_20200818175339.png', '2020-08-19 00:53:39', '2020-08-18 12:23:40'),
(212, '213035', 'product_213035_2_20200818175340.png', '2020-08-19 00:53:39', '2020-08-18 12:23:40'),
(213, '213035', 'product_213035_3_20200818175340.jpg', '2020-08-19 00:53:39', '2020-08-18 12:23:40'),
(214, '213035', 'product_213035_4_20200818175340.jpg', '2020-08-19 00:53:39', '2020-08-18 12:23:40'),
(215, '213035', 'product_213035_1_20200818175924.png', '2020-08-19 00:59:24', '2020-08-18 12:29:24'),
(216, '213035', 'product_213035_2_20200818175924.png', '2020-08-19 00:59:24', '2020-08-18 12:29:24'),
(217, 'MOON0027', 'product_MOON0027_1_20200818181052.png', '2020-08-19 01:10:52', '2020-08-18 12:40:52'),
(218, 'MOON0027', 'product_MOON0027_2_20200818181052.png', '2020-08-19 01:10:52', '2020-08-18 12:40:52'),
(219, 'MOON0022', 'product_MOON0022_1_20200818181439.png', '2020-08-19 01:14:39', '2020-08-18 12:44:39'),
(221, 'MOON0022', 'product_MOON0022_1_20200818181631.png', '2020-08-19 01:16:31', '2020-08-18 12:46:31'),
(228, 'MWHD2HN/A', 'product_MWHD2HN_A_1_20200825145339.jpg', '2020-08-25 21:53:39', '2020-08-25 09:23:39'),
(229, 'MWHD2HN/A', 'product_MWHD2HN_A_1_20200825145411.jpg', '2020-08-25 21:54:11', '2020-08-25 09:24:11'),
(230, 'MWHD2HN/A', 'product_MWHD2HN_A_2_20200825145411.jpg', '2020-08-25 21:54:11', '2020-08-25 09:24:11'),
(231, 'MWHD2HN/A', 'product_MWHD2HN_A_3_20200825145411.jpg', '2020-08-25 21:54:11', '2020-08-25 09:24:11'),
(232, 'MWHD2HN/A', 'product_MWHD2HN_A_4_20200825145411.jpg', '2020-08-25 21:54:11', '2020-08-25 09:24:11'),
(233, 'MVH22HN/A', 'product_MVH22HN_A_1_20200826182806.jpg', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(234, 'MVH22HN/A', 'product_MVH22HN_A_2_20200826182806.jpg', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(235, 'MVH22HN/A', 'product_MVH22HN_A_3_20200826182806.jpg', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(236, 'MVH22HN/A', 'product_MVH22HN_A_4_20200826182806.jpg', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(237, 'MVH22HN/A', 'product_MVH22HN_A_5_20200826182806.jpg', '2020-08-27 01:28:06', '2020-08-26 12:58:06'),
(238, 'Mac0001', '', '2020-08-27 18:06:04', '2020-08-27 05:36:04'),
(239, 'Mac0001', '', '2020-08-27 18:06:04', '2020-08-27 05:36:04'),
(240, 'Mac0001', '', '2020-08-27 18:06:04', '2020-08-27 05:36:04'),
(241, 'Mac0001', '', '2020-08-27 18:06:04', '2020-08-27 05:36:04'),
(242, '1105032', 'product_1105032_1_20200827161654.jpeg', '2020-08-27 23:16:54', '2020-08-27 10:46:54'),
(243, '1105036', 'product_1105036_1_20200827163057.jpeg', '2020-08-27 23:30:57', '2020-08-27 11:00:57'),
(244, '1105039', 'product_1105039_1_20200827170334.jpeg', '2020-08-28 00:03:34', '2020-08-27 11:33:34'),
(245, '1105040', 'product_1105040_1_20200827172217.jpeg', '2020-08-28 00:22:17', '2020-08-27 11:52:17'),
(246, '1105041', 'product_1105041_1_20200827174237.jpeg', '2020-08-28 00:42:37', '2020-08-27 12:12:37'),
(248, '1320032', 'product_1320032_1_20200827185702.png', '2020-08-28 01:57:02', '2020-08-27 13:27:02'),
(249, 'SK001', 'product_SK001_1_20200830184849.jpg', '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(250, 'SK001', 'product_SK001_2_20200830184849.jpg', '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(251, 'SK001', 'product_SK001_3_20200830184849.jpg', '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(252, 'SK001', 'product_SK001_4_20200830184849.jpg', '2020-08-31 01:48:49', '2020-08-30 13:18:49'),
(257, 'GRU001', 'product_GRU001_1_20200830191416.jpg', '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(258, 'GRU001', 'product_GRU001_2_20200830191416.jpg', '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(259, 'GRU001', 'product_GRU001_3_20200830191416.jpg', '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(260, 'GRU001', 'product_GRU001_4_20200830191416.jpg', '2020-08-31 02:14:16', '2020-08-30 13:44:16'),
(261, 'PR001', 'product_PR001_1_20200830192019.jpg', '2020-08-31 02:20:19', '2020-08-30 13:50:19'),
(262, 'PR001', 'product_PR001_2_20200830192019.png', '2020-08-31 02:20:19', '2020-08-30 13:50:19'),
(263, 'PR001', 'product_PR001_3_20200830192019.jpg', '2020-08-31 02:20:19', '2020-08-30 13:50:19'),
(264, 'PR001', 'product_PR001_4_20200830192019.jpg', '2020-08-31 02:20:19', '2020-08-30 13:50:19'),
(265, 'GRU002', 'product_GRU002_1_20200830194757.jpg', '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(266, 'GRU002', 'product_GRU002_2_20200830194757.jpg', '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(267, 'GRU002', 'product_GRU002_3_20200830194757.jpg', '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(268, 'GRU002', 'product_GRU002_4_20200830194757.jpg', '2020-08-31 02:47:57', '2020-08-30 14:17:57'),
(273, 'PR002', 'product_PR002_1_20200830200440.png', '2020-08-31 03:04:40', '2020-08-30 14:34:40'),
(274, 'PR002', 'product_PR002_2_20200830200440.png', '2020-08-31 03:04:40', '2020-08-30 14:34:40'),
(275, 'PR002', 'product_PR002_3_20200830200440.png', '2020-08-31 03:04:40', '2020-08-30 14:34:41'),
(276, 'PR002', 'product_PR002_4_20200830200441.png', '2020-08-31 03:04:40', '2020-08-30 14:34:41'),
(277, 'DIM002', 'product_DIM002_1_20200830211203.jpg', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(278, 'DIM002', 'product_DIM002_2_20200830211203.jpg', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(279, 'DIM002', 'product_DIM002_3_20200830211203.jpg', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(280, 'DIM002', 'product_DIM002_4_20200830211203.png', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(281, 'DIM002', 'product_DIM002_5_20200830211203.jpg', '2020-08-31 04:12:03', '2020-08-30 15:42:03'),
(282, 'DIM003', 'product_DIM003_1_20200830212426.jpg', '2020-08-31 04:24:26', '2020-08-30 15:54:26'),
(283, 'DIM003', 'product_DIM003_2_20200830212426.jpg', '2020-08-31 04:24:26', '2020-08-30 15:54:27'),
(284, 'DIM003', 'product_DIM003_3_20200830212427.jpg', '2020-08-31 04:24:26', '2020-08-30 15:54:27'),
(285, 'DIM003', 'product_DIM003_4_20200830212427.jpg', '2020-08-31 04:24:26', '2020-08-30 15:54:27'),
(286, 'DIM003', 'product_DIM003_5_20200830212427.jpg', '2020-08-31 04:24:26', '2020-08-30 15:54:27'),
(287, 'GRU003', 'product_GRU003_1_20200830212442.jpg', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(288, 'GRU003', 'product_GRU003_2_20200830212442.jpg', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(289, 'GRU003', 'product_GRU003_3_20200830212442.jpg', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(290, 'GRU003', 'product_GRU003_4_20200830212442.jpg', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(291, 'GRU003', 'product_GRU003_5_20200830212442.jpg', '2020-08-31 04:24:42', '2020-08-30 15:54:42'),
(292, 'GRU004', 'product_GRU003_1_20200830212442.jpg', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(293, 'GRU004', 'product_GRU003_2_20200830212442.jpg', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(294, 'GRU004', 'product_GRU003_3_20200830212442.jpg', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(295, 'GRU004', 'product_GRU003_4_20200830212442.jpg', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(296, 'GRU004', 'product_GRU003_5_20200830212442.jpg', '2020-08-31 04:27:34', '2020-08-30 15:57:34'),
(297, 'DIM004', 'product_DIM003_1_20200830212426.jpg', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(298, 'DIM004', 'product_DIM003_2_20200830212426.jpg', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(299, 'DIM004', 'product_DIM003_3_20200830212427.jpg', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(300, 'DIM004', 'product_DIM003_4_20200830212427.jpg', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(301, 'DIM004', 'product_DIM003_5_20200830212427.jpg', '2020-08-31 04:30:48', '2020-08-30 16:00:48'),
(302, 'DIM005', 'product_DIM003_1_20200830212426.jpg', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(303, 'DIM005', 'product_DIM003_2_20200830212426.jpg', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(304, 'DIM005', 'product_DIM003_3_20200830212427.jpg', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(305, 'DIM005', 'product_DIM003_4_20200830212427.jpg', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(306, 'DIM005', 'product_DIM003_5_20200830212427.jpg', '2020-08-31 04:32:28', '2020-08-30 16:02:28'),
(312, 'DIM007', 'product_DIM006_1_20200830214121.jpg', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(313, 'DIM007', 'product_DIM006_2_20200830214121.jpg', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(314, 'DIM007', 'product_DIM006_3_20200830214121.jpg', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(315, 'DIM007', 'product_DIM006_4_20200830214122.jpg', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(316, 'DIM007', 'product_DIM006_5_20200830214122.jpg', '2020-08-31 04:43:22', '2020-08-30 16:13:22'),
(322, 'PR004', 'product_PR003_1_20200830221221.jpg', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(323, 'PR004', 'product_PR003_2_20200830221221.jpg', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(324, 'PR004', 'product_PR003_3_20200830221221.jpg', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(325, 'PR004', 'product_PR003_4_20200830221221.jpg', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(326, 'PR004', 'product_PR003_5_20200830221221.jpg', '2020-08-31 05:25:37', '2020-08-30 16:55:37'),
(327, 'GRU005', 'product_GRU005_1_20200830224927.jpg', '2020-08-31 05:49:27', '2020-08-30 17:19:27'),
(335, 'GRUOO6', 'product_GRUOO6_1_20200830230056.jpg', '2020-08-31 06:00:56', '2020-08-30 17:30:56'),
(342, 'GRU007', 'product_GRU007_1_20200830231338.jpg', '2020-08-31 06:13:38', '2020-08-30 17:43:38'),
(343, 'GRU008', 'product_GRU008_1_20200830231851.jpg', '2020-08-31 06:18:51', '2020-08-30 17:48:51'),
(344, 'GRU009', 'product_GRU009_1_20200830232140.jpg', '2020-08-31 06:21:40', '2020-08-30 17:51:40'),
(345, 'GRU0010', 'product_GRU009_1_20200830232140.jpg', '2020-08-31 06:23:18', '2020-08-30 17:53:18'),
(351, 'PR008', 'product_PR007_1_20200830233138.jpg', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(352, 'PR008', 'product_PR007_2_20200830233138.jpg', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(353, 'PR008', 'product_PR007_3_20200830233138.jpg', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(354, 'PR008', 'product_PR007_4_20200830233138.jpg', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(355, 'PR008', 'product_PR007_5_20200830233138.jpg', '2020-08-31 06:33:03', '2020-08-30 18:03:03'),
(356, 'PR009', 'product_PR007_1_20200830233138.jpg', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(357, 'PR009', 'product_PR007_2_20200830233138.jpg', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(358, 'PR009', 'product_PR007_3_20200830233138.jpg', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(359, 'PR009', 'product_PR007_4_20200830233138.jpg', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(360, 'PR009', 'product_PR007_5_20200830233138.jpg', '2020-08-31 06:34:03', '2020-08-30 18:04:03'),
(361, 'GRU0011', 'product_GRU0011_1_20200830233503.jpg', '2020-08-31 06:35:03', '2020-08-30 18:05:03'),
(362, 'PR0010', 'product_PR007_1_20200830233138.jpg', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(363, 'PR0010', 'product_PR007_2_20200830233138.jpg', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(364, 'PR0010', 'product_PR007_3_20200830233138.jpg', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(365, 'PR0010', 'product_PR007_4_20200830233138.jpg', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(366, 'PR0010', 'product_PR007_5_20200830233138.jpg', '2020-08-31 06:35:54', '2020-08-30 18:05:54'),
(367, 'PR0011', 'product_PR007_1_20200830233138.jpg', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(368, 'PR0011', 'product_PR007_2_20200830233138.jpg', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(369, 'PR0011', 'product_PR007_3_20200830233138.jpg', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(370, 'PR0011', 'product_PR007_4_20200830233138.jpg', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(371, 'PR0011', 'product_PR007_5_20200830233138.jpg', '2020-08-31 06:36:53', '2020-08-30 18:06:53'),
(375, 'GRU0012', 'product_GRU0012_1_20200830235437.jpg', '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(376, 'GRU0012', 'product_GRU0012_2_20200830235437.jpg', '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(377, 'GRU0012', 'product_GRU0012_3_20200830235437.jpg', '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(378, 'GRU0012', 'product_GRU0012_4_20200830235437.jpg', '2020-08-31 06:54:37', '2020-08-30 18:24:37'),
(379, 'PR0013', 'product_PR0013_1_20200831000247.jpg', '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(380, 'PR0013', 'product_PR0013_2_20200831000247.jpg', '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(381, 'PR0013', 'product_PR0013_3_20200831000247.jpg', '2020-08-31 07:02:47', '2020-08-30 18:32:47'),
(382, 'PR0013', 'product_PR0013_4_20200831000247.jpg', '2020-08-31 07:02:47', '2020-08-30 18:32:48'),
(383, 'PR0013', 'product_PR0013_5_20200831000248.jpg', '2020-08-31 07:02:47', '2020-08-30 18:32:48'),
(384, 'GRU0013', 'product_GRU0013_1_20200831000811.jpg', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(385, 'GRU0013', 'product_GRU0013_2_20200831000811.jpg', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(386, 'GRU0013', 'product_GRU0013_3_20200831000811.jpg', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(387, 'GRU0013', 'product_GRU0013_4_20200831000811.jpg', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(388, 'GRU0013', 'product_GRU0013_5_20200831000811.jpg', '2020-08-31 07:08:11', '2020-08-30 18:38:11'),
(392, 'GRU0015', 'product_GRU0015_1_20200831002319.jpg', '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(393, 'GRU0015', 'product_GRU0015_2_20200831002319.jpg', '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(394, 'GRU0015', 'product_GRU0015_3_20200831002319.jpg', '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(395, 'GRU0015', 'product_GRU0015_4_20200831002319.jpg', '2020-08-31 07:23:19', '2020-08-30 18:53:19'),
(396, 'GRU0016', 'product_GRU0016_1_20200831002937.jpg', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(397, 'GRU0016', 'product_GRU0016_2_20200831002937.jpg', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(398, 'GRU0016', 'product_GRU0016_3_20200831002937.jpg', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(399, 'GRU0016', 'product_GRU0016_4_20200831002937.jpg', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(400, 'GRU0016', 'product_GRU0016_5_20200831002937.jpg', '2020-08-31 07:29:37', '2020-08-30 18:59:37'),
(409, 'GRU0017', 'product_GRU0016_1_20200831002937.jpg', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(410, 'GRU0017', 'product_GRU0016_2_20200831002937.jpg', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(411, 'GRU0017', 'product_GRU0016_3_20200831002937.jpg', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(412, 'GRU0017', 'product_GRU0016_4_20200831002937.jpg', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(413, 'GRU0017', 'product_GRU0016_5_20200831002937.jpg', '2020-08-31 07:39:30', '2020-08-30 19:09:30'),
(414, 'GRU0018', 'product_GRU0018_1_20200831005419.jpg', '2020-08-31 07:54:19', '2020-08-30 19:24:19'),
(415, 'GRU0018', 'product_GRU0018_2_20200831005419.jpg', '2020-08-31 07:54:19', '2020-08-30 19:24:19'),
(416, 'GRU0018', 'product_GRU0018_3_20200831005419.jpg', '2020-08-31 07:54:19', '2020-08-30 19:24:20'),
(421, 'GRU0019', 'product_GRU0018_1_20200831005419.jpg', '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(422, 'GRU0019', 'product_GRU0018_2_20200831005419.jpg', '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(423, 'GRU0019', 'product_GRU0018_3_20200831005419.jpg', '2020-08-31 07:58:13', '2020-08-30 19:28:13'),
(424, 'PR0019', 'product_PR0019_1_20200831010848.jpg', '2020-08-31 08:08:47', '2020-08-30 19:38:48'),
(425, 'PR0019', 'product_PR0019_2_20200831010848.jpg', '2020-08-31 08:08:47', '2020-08-30 19:38:48'),
(426, 'PR0019', 'product_PR0019_3_20200831010848.jpg', '2020-08-31 08:08:47', '2020-08-30 19:38:48'),
(427, 'PR0019', 'product_PR0019_4_20200831010848.jpg', '2020-08-31 08:08:47', '2020-08-30 19:38:48'),
(428, 'GRU0020', 'product_GRU0020_1_20200831011100.jpg', '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(429, 'GRU0020', 'product_GRU0020_2_20200831011100.jpg', '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(430, 'GRU0020', 'product_GRU0020_3_20200831011100.jpg', '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(431, 'GRU0020', 'product_GRU0020_4_20200831011100.jpg', '2020-08-31 08:11:00', '2020-08-30 19:41:00'),
(432, 'GRU0021', 'product_GRU0020_1_20200831011100.jpg', '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(433, 'GRU0021', 'product_GRU0020_2_20200831011100.jpg', '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(434, 'GRU0021', 'product_GRU0020_3_20200831011100.jpg', '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(435, 'GRU0021', 'product_GRU0020_4_20200831011100.jpg', '2020-08-31 08:14:43', '2020-08-30 19:44:43'),
(436, 'PR0020', 'product_PR0019_1_20200831010848.jpg', '2020-08-31 08:16:27', '2020-08-30 19:46:27'),
(437, 'PR0020', 'product_PR0019_2_20200831010848.jpg', '2020-08-31 08:16:27', '2020-08-30 19:46:27'),
(438, 'PR0020', 'product_PR0019_3_20200831010848.jpg', '2020-08-31 08:16:27', '2020-08-30 19:46:27'),
(439, 'PR0020', 'product_PR0019_4_20200831010848.jpg', '2020-08-31 08:16:27', '2020-08-30 19:46:27'),
(440, 'GRU0022', 'product_GRU0022_1_20200831013930.jpg', '2020-08-31 08:39:30', '2020-08-30 20:09:30'),
(441, 'GRU0022', 'product_GRU0022_2_20200831013930.jpg', '2020-08-31 08:39:30', '2020-08-30 20:09:30'),
(442, 'PR0021', 'product_PR0021_1_20200831014741.jpg', '2020-08-31 08:47:41', '2020-08-30 20:17:41'),
(443, 'PR0022', 'product_PR0021_1_20200831014741.jpg', '2020-08-31 08:49:36', '2020-08-30 20:19:36'),
(444, 'PR0023', 'product_PR0023_1_20200831020504.jpg', '2020-08-31 09:05:04', '2020-08-30 20:35:04'),
(445, 'GRU0023', 'product_GRU0023_1_20200831020525.jpg', '2020-08-31 09:05:25', '2020-08-30 20:35:25'),
(446, 'GRU0023', 'product_GRU0023_2_20200831020525.jpg', '2020-08-31 09:05:25', '2020-08-30 20:35:25'),
(447, 'GRU0024', 'product_GRU0024_1_20200831022255.jpg', '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(448, 'GRU0024', 'product_GRU0024_2_20200831022255.jpg', '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(449, 'GRU0024', 'product_GRU0024_3_20200831022255.jpg', '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(450, 'GRU0024', 'product_GRU0024_4_20200831022255.jpg', '2020-08-31 09:22:55', '2020-08-30 20:52:55'),
(453, 'PR0025', 'product_PR0025_1_20200831023924.jpg', '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(454, 'PR0025', 'product_PR0025_2_20200831023924.jpg', '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(455, 'PR0025', 'product_PR0025_3_20200831023924.jpg', '2020-08-31 09:39:24', '2020-08-30 21:09:24'),
(456, 'DIM008', 'product_DIM008_1_20200831112026.jpg', '2020-08-31 18:20:25', '2020-08-31 05:50:26'),
(457, 'DIM008', 'product_DIM008_2_20200831112026.jpg', '2020-08-31 18:20:25', '2020-08-31 05:50:26'),
(458, 'DIM008', 'product_DIM008_3_20200831112026.jpg', '2020-08-31 18:20:25', '2020-08-31 05:50:26'),
(459, 'DIM008', 'product_DIM008_4_20200831112026.jpg', '2020-08-31 18:20:25', '2020-08-31 05:50:26'),
(460, 'DIM009', 'product_DIM009_1_20200831120244.jpg', '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(461, 'DIM009', 'product_DIM009_2_20200831120244.jpg', '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(462, 'DIM009', 'product_DIM009_3_20200831120244.jpg', '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(463, 'DIM009', 'product_DIM009_4_20200831120244.jpg', '2020-08-31 19:02:44', '2020-08-31 06:32:44'),
(464, 'DIM10', 'product_DIM10_1_20200831165707.jpg', '2020-08-31 23:57:07', '2020-08-31 11:27:07'),
(465, 'DIM10', 'product_DIM10_2_20200831165707.jpg', '2020-08-31 23:57:07', '2020-08-31 11:27:07'),
(466, 'DIM10', 'product_DIM10_3_20200831165707.jpg', '2020-08-31 23:57:07', '2020-08-31 11:27:07'),
(467, 'DIM11', 'product_DIM11_1_20200831171619.jpg', '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(468, 'DIM11', 'product_DIM11_2_20200831171619.jpg', '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(469, 'DIM11', 'product_DIM11_3_20200831171619.jpg', '2020-09-01 00:16:19', '2020-08-31 11:46:19'),
(470, 'DIM12', 'product_DIM12_1_20200831172251.jpg', '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(471, 'DIM12', 'product_DIM12_2_20200831172251.jpg', '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(472, 'DIM12', 'product_DIM12_3_20200831172251.jpg', '2020-09-01 00:22:51', '2020-08-31 11:52:51'),
(473, 'DIM0013', 'product_DIM0013_1_20200831180300.jpg', '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(474, 'DIM0013', 'product_DIM0013_2_20200831180300.jpg', '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(475, 'DIM0013', 'product_DIM0013_3_20200831180300.jpg', '2020-09-01 01:03:00', '2020-08-31 12:33:00'),
(476, 'DIM14', 'product_DIM14_1_20200831181507.jpg', '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(477, 'DIM14', 'product_DIM14_2_20200831181507.jpg', '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(478, 'DIM14', 'product_DIM14_3_20200831181507.jpg', '2020-09-01 01:15:07', '2020-08-31 12:45:07'),
(479, 'DIM15', 'product_DIM15_1_20200831182128.jpg', '2020-09-01 01:21:28', '2020-08-31 12:51:28'),
(480, 'DIM15', 'product_DIM15_2_20200831182128.jpg', '2020-09-01 01:21:28', '2020-08-31 12:51:28'),
(481, 'DMI17', 'product_DMI17_1_20200831182818.jpg', '2020-09-01 01:28:18', '2020-08-31 12:58:18'),
(482, 'DMI17', 'product_DMI17_2_20200831182818.jpg', '2020-09-01 01:28:18', '2020-08-31 12:58:18'),
(483, 'PR0026', 'product_PR0026_1_20200831230609.jpg', '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(484, 'PR0026', 'product_PR0026_2_20200831230609.jpg', '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(485, 'PR0026', 'product_PR0026_3_20200831230609.jpg', '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(486, 'PR0026', 'product_PR0026_4_20200831230609.jpg', '2020-09-01 06:06:09', '2020-08-31 17:36:09'),
(487, 'GRU0025', 'product_GRU0025_1_20200831230739.jpg', '2020-09-01 06:07:39', '2020-08-31 17:37:39'),
(488, 'GRU0025', 'product_GRU0025_2_20200831230739.jpg', '2020-09-01 06:07:39', '2020-08-31 17:37:40'),
(489, 'GRU0025', 'product_GRU0025_3_20200831230740.jpg', '2020-09-01 06:07:39', '2020-08-31 17:37:40'),
(490, 'GRU0025', 'product_GRU0025_4_20200831230740.jpg', '2020-09-01 06:07:39', '2020-08-31 17:37:40'),
(491, 'GRU0026', 'product_GRU0026_1_20200831232944.jpg', '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(492, 'GRU0026', 'product_GRU0026_2_20200831232944.jpg', '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(493, 'GRU0026', '', '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(494, 'GRU0026', 'product_GRU0026_4_20200831232944.jpg', '2020-09-01 06:29:44', '2020-08-31 17:59:44'),
(499, 'GRU0027', 'product_GRU0027_1_20200831235909.jpg', '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(500, 'GRU0027', 'product_GRU0027_2_20200831235909.jpg', '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(501, 'GRU0027', 'product_GRU0027_3_20200831235909.jpg', '2020-09-01 06:59:09', '2020-08-31 18:29:09'),
(502, 'PR0028', 'product_PR0028_1_20200901000955.jpg', '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(503, 'PR0028', 'product_PR0028_2_20200901000955.jpg', '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(504, 'PR0028', 'product_PR0028_3_20200901000955.jpg', '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(505, 'PR0028', '', '2020-09-01 07:09:55', '2020-08-31 18:39:55'),
(506, 'GRU0028', 'product_GRU0028_1_20200901001522.jpg', '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(507, 'GRU0028', 'product_GRU0028_2_20200901001522.jpg', '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(508, 'GRU0028', 'product_GRU0028_3_20200901001522.jpg', '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(509, 'GRU0028', 'product_GRU0028_4_20200901001522.jpg', '2020-09-01 07:15:22', '2020-08-31 18:45:22'),
(510, 'GRU0029', 'product_GRU0029_1_20200901002416.jpg', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(511, 'GRU0029', 'product_GRU0029_2_20200901002416.jpg', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(512, 'GRU0029', 'product_GRU0029_3_20200901002416.jpg', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(513, 'GRU0029', 'product_GRU0029_4_20200901002416.jpg', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(514, 'GRU0029', 'product_GRU0029_5_20200901002416.jpg', '2020-09-01 07:24:16', '2020-08-31 18:54:16'),
(515, 'PR0029', 'product_PR0029_1_20200901002609.jpg', '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(516, 'PR0029', 'product_PR0029_2_20200901002609.jpg', '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(517, 'PR0029', 'product_PR0029_3_20200901002609.jpg', '2020-09-01 07:26:09', '2020-08-31 18:56:09'),
(518, 'GRU0030', 'product_GRU0030_1_20200901003625.jpg', '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(519, 'GRU0030', 'product_GRU0030_2_20200901003625.jpg', '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(520, 'GRU0030', 'product_GRU0030_3_20200901003625.jpg', '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(521, 'GRU0030', 'product_GRU0030_4_20200901003625.jpg', '2020-09-01 07:36:25', '2020-08-31 19:06:25'),
(522, 'PR0030', 'product_PR0030_1_20200901004043.jpg', '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(523, 'PR0030', 'product_PR0030_2_20200901004043.jpg', '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(524, 'PR0030', 'product_PR0030_3_20200901004043.jpg', '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(525, 'PR0030', 'product_PR0030_4_20200901004043.jpg', '2020-09-01 07:40:43', '2020-08-31 19:10:43'),
(526, 'GRU0031', 'product_GRU0031_1_20200901005006.jpg', '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(527, 'GRU0031', 'product_GRU0031_2_20200901005006.jpg', '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(528, 'GRU0031', 'product_GRU0031_3_20200901005006.jpg', '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(529, 'GRU0031', 'product_GRU0031_4_20200901005006.jpg', '2020-09-01 07:50:06', '2020-08-31 19:20:06'),
(530, 'PR0031', 'product_PR0031_1_20200901005534.jpg', '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(531, 'PR0031', 'product_PR0031_2_20200901005534.jpg', '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(532, 'PR0031', 'product_PR0031_3_20200901005534.jpg', '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(533, 'PR0031', 'product_PR0031_4_20200901005534.jpg', '2020-09-01 07:55:34', '2020-08-31 19:25:34'),
(534, 'GRU0032', 'product_GRU0032_1_20200901005918.jpg', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(535, 'GRU0032', 'product_GRU0032_2_20200901005918.jpg', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(536, 'GRU0032', 'product_GRU0032_3_20200901005918.jpg', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(537, 'GRU0032', 'product_GRU0032_4_20200901005918.jpg', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(538, 'GRU0032', 'product_GRU0032_5_20200901005918.jpg', '2020-09-01 07:59:18', '2020-08-31 19:29:18'),
(543, 'GRU0033', 'product_GRU0033_1_20200901011332.jpg', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(544, 'GRU0033', 'product_GRU0033_2_20200901011332.jpg', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(545, 'GRU0033', 'product_GRU0033_3_20200901011332.jpg', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(546, 'GRU0033', 'product_GRU0033_4_20200901011332.jpg', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(547, 'GRU0033', 'product_GRU0033_5_20200901011332.jpg', '2020-09-01 08:13:32', '2020-08-31 19:43:32'),
(548, 'PR0033', 'product_PR0033_1_20200901011335.jpg', '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(549, 'PR0033', 'product_PR0033_2_20200901011335.jpg', '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(550, 'PR0033', 'product_PR0033_3_20200901011335.jpg', '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(551, 'PR0033', 'product_PR0033_4_20200901011335.jpg', '2020-09-01 08:13:35', '2020-08-31 19:43:35'),
(552, 'PR0034', 'product_PR0034_1_20200901012025.jpg', '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(553, 'PR0034', 'product_PR0034_2_20200901012025.jpg', '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(554, 'PR0034', 'product_PR0034_3_20200901012025.jpg', '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(555, 'PR0034', 'product_PR0034_4_20200901012025.jpg', '2020-09-01 08:20:25', '2020-08-31 19:50:25'),
(556, 'GRU0034', 'product_GRU0034_1_20200901012340.jpg', '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(557, 'GRU0034', 'product_GRU0034_2_20200901012340.jpg', '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(558, 'GRU0034', 'product_GRU0034_3_20200901012340.jpg', '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(559, 'GRU0034', 'product_GRU0034_4_20200901012340.jpg', '2020-09-01 08:23:40', '2020-08-31 19:53:40'),
(560, 'PR0035', 'product_PR0035_1_20200901013351.jpg', '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(561, 'PR0035', 'product_PR0035_2_20200901013351.jpg', '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(562, 'PR0035', 'product_PR0035_3_20200901013351.jpg', '2020-09-01 08:33:51', '2020-08-31 20:03:51'),
(563, 'GRU0035', 'product_GRU0035_1_20200901013415.jpg', '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(564, 'GRU0035', 'product_GRU0035_2_20200901013415.jpg', '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(565, 'GRU0035', 'product_GRU0035_3_20200901013415.jpg', '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(566, 'GRU0035', 'product_GRU0035_4_20200901013415.jpg', '2020-09-01 08:34:15', '2020-08-31 20:04:15'),
(567, 'PR0036', 'product_PR0036_1_20200901130912.jpg', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(568, 'PR0036', 'product_PR0036_2_20200901130912.jpg', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(569, 'PR0036', 'product_PR0036_3_20200901130912.jpg', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(570, 'PR0036', 'product_PR0036_4_20200901130912.jpg', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(571, 'PR0036', 'product_PR0036_5_20200901130912.jpg', '2020-09-01 20:09:12', '2020-09-01 07:39:12'),
(572, 'PR0037', 'product_PR0037_1_20200901132422.jpg', '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(573, 'PR0037', 'product_PR0037_2_20200901132422.jpg', '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(574, 'PR0037', 'product_PR0037_3_20200901132422.jpg', '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(575, 'PR0037', 'product_PR0037_4_20200901132422.jpg', '2020-09-01 20:24:22', '2020-09-01 07:54:22'),
(576, 'PR0038', 'product_PR0038_1_20200901134854.jpg', '2020-09-01 20:48:54', '2020-09-01 08:18:55'),
(577, 'PR0038', 'product_PR0038_2_20200901134855.jpg', '2020-09-01 20:48:54', '2020-09-01 08:18:55'),
(578, 'PR0038', 'product_PR0038_3_20200901134855.jpg', '2020-09-01 20:48:54', '2020-09-01 08:18:55'),
(579, 'PR0038', 'product_PR0038_4_20200901134855.jpg', '2020-09-01 20:48:54', '2020-09-01 08:18:55'),
(580, 'PR0039', 'product_PR0039_1_20200901140557.jpg', '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(581, 'PR0039', 'product_PR0039_2_20200901140557.jpg', '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(582, 'PR0039', 'product_PR0039_3_20200901140557.jpg', '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(583, 'PR0039', 'product_PR0039_4_20200901140557.jpg', '2020-09-01 21:05:57', '2020-09-01 08:35:57'),
(584, 'PR0040', 'product_PR0040_1_20200901141549.jpg', '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(585, 'PR0040', 'product_PR0040_2_20200901141549.jpg', '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(586, 'PR0040', 'product_PR0040_3_20200901141549.jpg', '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(587, 'PR0040', 'product_PR0040_4_20200901141549.jpg', '2020-09-01 21:15:49', '2020-09-01 08:45:49'),
(588, 'GRU0036', 'product_GRU0036_1_20200901143329.jpg', '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(589, 'GRU0036', 'product_GRU0036_2_20200901143329.jpg', '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(590, 'GRU0036', 'product_GRU0036_3_20200901143329.jpg', '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(591, 'GRU0036', 'product_GRU0036_4_20200901143329.jpg', '2020-09-01 21:33:29', '2020-09-01 09:03:29'),
(592, 'GRU0037', 'product_GRU0036_1_20200901144441.jpg', '2020-09-01 21:44:41', '2020-09-01 09:14:41'),
(593, 'GRU0037', 'product_GRU0036_2_20200901144441.jpg', '2020-09-01 21:44:41', '2020-09-01 09:14:41'),
(594, 'GRU0037', 'product_GRU0036_3_20200901144441.jpg', '2020-09-01 21:44:41', '2020-09-01 09:14:41'),
(595, 'GRU0037', 'product_GRU0036_4_20200901144441.jpg', '2020-09-01 21:44:41', '2020-09-01 09:14:41'),
(596, 'GRU0038', 'product_GRU0038_1_20200901150315.jpg', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(597, 'GRU0038', 'product_GRU0038_2_20200901150315.jpg', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(598, 'GRU0038', 'product_GRU0038_3_20200901150315.jpg', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(599, 'GRU0038', 'product_GRU0038_4_20200901150315.jpg', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(600, 'GRU0038', 'product_GRU0038_5_20200901150315.jpg', '2020-09-01 22:03:15', '2020-09-01 09:33:15'),
(601, 'GRU0039', 'product_GRU0038_1_20200901150315.jpg', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(602, 'GRU0039', 'product_GRU0038_2_20200901150315.jpg', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(603, 'GRU0039', 'product_GRU0038_3_20200901150315.jpg', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(604, 'GRU0039', 'product_GRU0038_4_20200901150315.jpg', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(605, 'GRU0039', 'product_GRU0038_5_20200901150315.jpg', '2020-09-01 22:08:42', '2020-09-01 09:38:42'),
(606, 'GRU0040', 'product_GRU0040_1_20200901152046.jpg', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(607, 'GRU0040', 'product_GRU0040_2_20200901152046.jpg', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(608, 'GRU0040', 'product_GRU0040_3_20200901152046.jpg', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(609, 'GRU0040', 'product_GRU0040_4_20200901152046.jpg', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(610, 'GRU0040', 'product_GRU0040_5_20200901152046.jpg', '2020-09-01 22:20:46', '2020-09-01 09:50:46'),
(611, 'GRU0041', 'product_GRU0040_1_20200901152046.jpg', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(612, 'GRU0041', 'product_GRU0040_2_20200901152046.jpg', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(613, 'GRU0041', 'product_GRU0040_3_20200901152046.jpg', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(614, 'GRU0041', 'product_GRU0040_4_20200901152046.jpg', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(615, 'GRU0041', 'product_GRU0040_5_20200901152046.jpg', '2020-09-01 22:29:17', '2020-09-01 09:59:17'),
(616, 'PR0041', 'product_PR0041_1_20200901154330.jpg', '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(617, 'PR0041', 'product_PR0041_2_20200901154330.jpg', '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(618, 'PR0041', 'product_PR0041_3_20200901154330.jpg', '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(619, 'PR0041', 'product_PR0041_4_20200901154330.jpg', '2020-09-01 22:43:30', '2020-09-01 10:13:30'),
(620, 'PR0042', 'product_PR0042_1_20200901215945.jpg', '2020-09-02 04:59:45', '2020-09-01 16:29:45'),
(622, 'PR0042', 'product_PR0042_3_20200901215945.jpg', '2020-09-02 04:59:45', '2020-09-01 16:29:45'),
(629, 'PR0044', 'product_PR0044_2_20200901222045.jpg', '2020-09-02 05:20:45', '2020-09-01 16:50:45'),
(630, 'PR0044', 'product_PR0044_3_20200901222045.jpg', '2020-09-02 05:20:45', '2020-09-01 16:50:45'),
(631, 'PR0044', 'product_PR0044_4_20200901222045.jpg', '2020-09-02 05:20:45', '2020-09-01 16:50:45'),
(632, 'PR0044', 'product_PR0044_1_20200901222213.jpg', '2020-09-02 05:22:13', '2020-09-01 16:52:14'),
(633, 'GRU0042', 'product_GRU0042_1_20200901223024.jpg', '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(634, 'GRU0042', 'product_GRU0042_2_20200901223024.jpg', '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(635, 'GRU0042', 'product_GRU0042_3_20200901223024.jpg', '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(636, 'GRU0042', 'product_GRU0042_4_20200901223024.jpg', '2020-09-02 05:30:24', '2020-09-01 17:00:24'),
(637, 'PR0043', 'product_PR0043_1_20200901224250.jpg', '2020-09-02 05:42:50', '2020-09-01 17:12:50'),
(638, 'PR0043', 'product_PR0043_2_20200901224250.jpg', '2020-09-02 05:42:50', '2020-09-01 17:12:50'),
(639, 'PR0043', 'product_PR0043_3_20200901224250.jpg', '2020-09-02 05:42:50', '2020-09-01 17:12:50'),
(640, 'PR0043', 'product_PR0043_4_20200901224250.jpg', '2020-09-02 05:42:50', '2020-09-01 17:12:50'),
(641, 'PR0045', 'product_PR0045_1_20200901230500.jpg', '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(642, 'PR0045', 'product_PR0045_2_20200901230500.jpg', '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(643, 'PR0045', 'product_PR0045_3_20200901230500.jpg', '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(644, 'PR0045', 'product_PR0045_4_20200901230500.jpg', '2020-09-02 06:05:00', '2020-09-01 17:35:00'),
(645, 'GRU0043', 'product_GRU0043_1_20200901230547.jpg', '2020-09-02 06:05:47', '2020-09-01 17:35:48'),
(646, 'GRU0043', 'product_GRU0043_2_20200901230548.jpg', '2020-09-02 06:05:47', '2020-09-01 17:35:48'),
(647, 'GRU0043', 'product_GRU0043_3_20200901230548.jpg', '2020-09-02 06:05:47', '2020-09-01 17:35:48'),
(648, 'GRU0043', 'product_GRU0043_4_20200901230548.jpg', '2020-09-02 06:05:47', '2020-09-01 17:35:48'),
(649, 'GRU0044', 'product_GRU0043_1_20200901230547.jpg', '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(650, 'GRU0044', 'product_GRU0043_2_20200901230548.jpg', '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(651, 'GRU0044', 'product_GRU0043_3_20200901230548.jpg', '2020-09-02 06:10:25', '2020-09-01 17:40:25'),
(652, 'GRU0044', 'product_GRU0043_4_20200901230548.jpg', '2020-09-02 06:10:25', '2020-09-01 17:40:25');
INSERT INTO `product_images` (`pi_id`, `product`, `image`, `created_at`, `updated_at`) VALUES
(653, 'PR0046', 'product_PR0046_1_20200901231212.jpg', '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(654, 'PR0046', 'product_PR0046_2_20200901231212.jpg', '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(655, 'PR0046', 'product_PR0046_3_20200901231212.jpg', '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(656, 'PR0046', '', '2020-09-02 06:12:12', '2020-09-01 17:42:12'),
(657, 'GRU0045', 'product_GRU0045_1_20200901231823.jpg', '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(658, 'GRU0045', 'product_GRU0045_2_20200901231823.jpg', '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(659, 'GRU0045', 'product_GRU0045_3_20200901231823.jpg', '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(660, 'GRU0045', 'product_GRU0045_4_20200901231823.jpg', '2020-09-02 06:18:23', '2020-09-01 17:48:23'),
(661, 'GRU0046', 'product_GRU0045_1_20200901231823.jpg', '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(662, 'GRU0046', 'product_GRU0045_2_20200901231823.jpg', '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(663, 'GRU0046', 'product_GRU0045_3_20200901231823.jpg', '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(664, 'GRU0046', 'product_GRU0045_4_20200901231823.jpg', '2020-09-02 06:20:36', '2020-09-01 17:50:36'),
(665, 'PR0047', 'product_PR0047_1_20200901232902.jpg', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(666, 'PR0047', 'product_PR0047_2_20200901232902.jpg', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(667, 'PR0047', 'product_PR0047_3_20200901232902.jpg', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(668, 'PR0047', 'product_PR0047_4_20200901232902.jpg', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(669, 'PR0047', 'product_PR0047_5_20200901232902.jpg', '2020-09-02 06:29:02', '2020-09-01 17:59:02'),
(670, 'PR0048', 'product_PR0048_1_20200901233543.jpg', '2020-09-02 06:35:43', '2020-09-01 18:05:43'),
(671, 'PR0048', 'product_PR0048_2_20200901233543.jpg', '2020-09-02 06:35:43', '2020-09-01 18:05:43'),
(672, 'PR0048', 'product_PR0048_3_20200901233543.jpg', '2020-09-02 06:35:43', '2020-09-01 18:05:44'),
(673, 'PR0048', 'product_PR0048_4_20200901233544.jpg', '2020-09-02 06:35:43', '2020-09-01 18:05:44'),
(674, 'PR0049', 'product_PR0049_1_20200901234337.jpg', '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(675, 'PR0049', 'product_PR0049_2_20200901234337.jpg', '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(676, 'PR0049', 'product_PR0049_3_20200901234337.jpg', '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(677, 'PR0049', 'product_PR0049_4_20200901234337.jpg', '2020-09-02 06:43:37', '2020-09-01 18:13:37'),
(678, 'PR0050', 'product_PR0050_1_20200901235219.jpg', '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(679, 'PR0050', 'product_PR0050_2_20200901235219.jpg', '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(680, 'PR0050', 'product_PR0050_3_20200901235219.jpg', '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(681, 'PR0050', 'product_PR0050_4_20200901235219.jpg', '2020-09-02 06:52:19', '2020-09-01 18:22:19'),
(682, 'PR0032', 'product_PR0032_3_20200901235838.jpg', '2020-09-02 06:58:38', '2020-09-01 18:28:38'),
(683, 'PR0032', 'product_PR0032_4_20200901235838.jpg', '2020-09-02 06:58:38', '2020-09-01 18:28:38'),
(684, 'PR0027', 'product_PR0027_1_20200902000846.jpg', '2020-09-02 07:08:46', '2020-09-01 18:38:46'),
(685, 'PR0027', 'product_PR0027_2_20200902000846.jpg', '2020-09-02 07:08:46', '2020-09-01 18:38:46'),
(686, 'PR0027', 'product_PR0027_3_20200902000846.jpg', '2020-09-02 07:08:46', '2020-09-01 18:38:46'),
(687, 'PR0027', 'product_PR0027_4_20200902000846.jpg', '2020-09-02 07:08:46', '2020-09-01 18:38:46'),
(688, 'GRU0048', 'product_GRU0048_1_20200903164515.jpg', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(689, 'GRU0048', 'product_GRU0048_2_20200903164515.jpg', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(690, 'GRU0048', 'product_GRU0048_3_20200903164515.jpg', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(691, 'GRU0048', 'product_GRU0048_4_20200903164515.jpg', '2020-09-03 23:45:15', '2020-09-03 11:15:15'),
(692, 'GRU0049', 'product_GRU0049_1_20200903165332.jpg', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(693, 'GRU0049', 'product_GRU0049_2_20200903165332.jpg', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(694, 'GRU0049', 'product_GRU0049_3_20200903165332.jpg', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(695, 'GRU0049', 'product_GRU0049_4_20200903165332.jpg', '2020-09-03 23:53:32', '2020-09-03 11:23:32'),
(696, 'GRU0050', 'product_GRU0050_1_20200903165903.jpg', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(697, 'GRU0050', 'product_GRU0050_2_20200903165903.jpg', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(698, 'GRU0050', 'product_GRU0050_3_20200903165903.jpg', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(699, 'GRU0050', 'product_GRU0050_4_20200903165903.jpg', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(700, 'GRU0050', 'product_GRU0050_5_20200903165903.jpg', '2020-09-03 23:59:03', '2020-09-03 11:29:03'),
(701, 'GRU0051', 'product_GRU0050_1_20200903165903.jpg', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(702, 'GRU0051', 'product_GRU0050_2_20200903165903.jpg', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(703, 'GRU0051', 'product_GRU0050_3_20200903165903.jpg', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(704, 'GRU0051', 'product_GRU0050_4_20200903165903.jpg', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(705, 'GRU0051', 'product_GRU0050_5_20200903165903.jpg', '2020-09-04 00:12:53', '2020-09-03 11:42:53'),
(706, 'GRU0052', 'product_GRU0052_1_20200903172157.jpg', '2020-09-04 00:21:57', '2020-09-03 11:51:57'),
(707, 'GRU0052', 'product_GRU0052_2_20200903172157.jpg', '2020-09-04 00:21:57', '2020-09-03 11:51:57'),
(708, 'GRU0052', 'product_GRU0052_3_20200903172157.jpg', '2020-09-04 00:21:57', '2020-09-03 11:51:57'),
(709, 'GRU0052', 'product_GRU0052_4_20200903172157.jpg', '2020-09-04 00:21:57', '2020-09-03 11:51:57'),
(710, 'GRU0053', 'product_GRU0053_1_20200903175104.jpg', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(711, 'GRU0053', 'product_GRU0053_2_20200903175104.jpg', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(712, 'GRU0053', 'product_GRU0053_3_20200903175104.jpg', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(713, 'GRU0053', 'product_GRU0053_4_20200903175104.jpg', '2020-09-04 00:51:04', '2020-09-03 12:21:04'),
(714, 'GRU0054', 'product_GRU0053_1_20200903175104.jpg', '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(715, 'GRU0054', 'product_GRU0053_2_20200903175104.jpg', '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(716, 'GRU0054', 'product_GRU0053_3_20200903175104.jpg', '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(717, 'GRU0054', 'product_GRU0053_4_20200903175104.jpg', '2020-09-04 00:54:06', '2020-09-03 12:24:06'),
(718, 'GRU0055', 'product_GRU0055_1_20200903180001.jpg', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(719, 'GRU0055', 'product_GRU0055_2_20200903180001.jpg', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(720, 'GRU0055', 'product_GRU0055_3_20200903180001.jpg', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(721, 'GRU0055', 'product_GRU0055_4_20200903180001.jpg', '2020-09-04 01:00:01', '2020-09-03 12:30:01'),
(722, 'GRU0056', 'product_GRU0055_1_20200903180001.jpg', '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(723, 'GRU0056', 'product_GRU0055_2_20200903180001.jpg', '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(724, 'GRU0056', 'product_GRU0055_3_20200903180001.jpg', '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(725, 'GRU0056', 'product_GRU0055_4_20200903180001.jpg', '2020-09-04 01:04:20', '2020-09-03 12:34:20'),
(726, 'GRU0057', 'product_GRU0057_1_20200903181238.jpg', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(727, 'GRU0057', 'product_GRU0057_2_20200903181238.jpg', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(728, 'GRU0057', 'product_GRU0057_3_20200903181238.jpg', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(729, 'GRU0057', 'product_GRU0057_4_20200903181238.jpg', '2020-09-04 01:12:38', '2020-09-03 12:42:38'),
(730, 'GRU0058', 'product_GRU0057_1_20200903181238.jpg', '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(731, 'GRU0058', 'product_GRU0057_2_20200903181238.jpg', '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(732, 'GRU0058', 'product_GRU0057_3_20200903181238.jpg', '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(733, 'GRU0058', 'product_GRU0057_4_20200903181238.jpg', '2020-09-04 01:16:10', '2020-09-03 12:46:10'),
(734, 'GRU0059', 'product_GRU0059_1_20200903182220.jpg', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(735, 'GRU0059', 'product_GRU0059_2_20200903182220.jpg', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(736, 'GRU0059', 'product_GRU0059_3_20200903182220.jpg', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(737, 'GRU0059', 'product_GRU0059_4_20200903182220.jpg', '2020-09-04 01:22:20', '2020-09-03 12:52:20'),
(738, 'GRU0060', 'product_GRU0059_1_20200903182220.jpg', '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(739, 'GRU0060', 'product_GRU0059_2_20200903182220.jpg', '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(740, 'GRU0060', 'product_GRU0059_3_20200903182220.jpg', '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(741, 'GRU0060', 'product_GRU0059_4_20200903182220.jpg', '2020-09-04 01:25:24', '2020-09-03 12:55:24'),
(742, 'MOON0099', 'product_MOON0099_1_20200903234120.jpg', '2020-09-04 06:41:20', '2020-09-03 18:11:20'),
(743, 'MOON0099', 'product_MOON0099_2_20200903234120.jpg', '2020-09-04 06:41:20', '2020-09-03 18:11:20'),
(744, 'PR0051', 'product_PR0041_1_20200901154330.jpg', '2020-09-04 06:42:18', '2020-09-03 18:12:18'),
(745, 'PR0051', 'product_PR0041_2_20200901154330.jpg', '2020-09-04 06:42:18', '2020-09-03 18:12:18'),
(746, 'PR0051', 'product_PR0041_3_20200901154330.jpg', '2020-09-04 06:42:18', '2020-09-03 18:12:18'),
(747, 'PR0051', 'product_PR0041_4_20200901154330.jpg', '2020-09-04 06:42:18', '2020-09-03 18:12:18'),
(748, 'MOON0012', 'product_MOON0012_1_20200903234419.jpg', '2020-09-04 06:44:19', '2020-09-03 18:14:19'),
(749, 'MOON0012', 'product_MOON0012_2_20200903234419.jpg', '2020-09-04 06:44:19', '2020-09-03 18:14:19'),
(750, 'MOON0011', 'product_MOON0011_1_20200903234732.jpg', '2020-09-04 06:47:32', '2020-09-03 18:17:32'),
(752, 'MOON0011', 'product_MOON0011_3_20200903234732.jpg', '2020-09-04 06:47:32', '2020-09-03 18:17:32'),
(754, 'DIM006', 'product_DIM006_2_20200904001630.jpg', '2020-09-04 07:16:30', '2020-09-03 18:46:30'),
(755, 'DIM006', 'product_DIM006_3_20200904001630.jpg', '2020-09-04 07:16:30', '2020-09-03 18:46:30'),
(756, 'DIM006', 'product_DIM006_4_20200904001630.jpg', '2020-09-04 07:16:30', '2020-09-03 18:46:30'),
(757, 'DIM006', 'product_DIM006_5_20200904001630.jpg', '2020-09-04 07:16:30', '2020-09-03 18:46:30'),
(758, 'MOON0091', 'product_MOON0091_1_20200904002041.jpg', '2020-09-04 07:20:41', '2020-09-03 18:50:41'),
(759, 'MOON0091', 'product_MOON0091_2_20200904002041.jpg', '2020-09-04 07:20:41', '2020-09-03 18:50:41'),
(760, 'MOON0091', 'product_MOON0091_3_20200904002041.jpg', '2020-09-04 07:20:41', '2020-09-03 18:50:41'),
(761, 'MOON0091', 'product_MOON0091_4_20200904002041.jpg', '2020-09-04 07:20:41', '2020-09-03 18:50:41'),
(762, 'PR0012', 'product_PR0012_1_20200904002452.jpg', '2020-09-04 07:24:52', '2020-09-03 18:54:52'),
(763, 'PR0012', 'product_PR0012_2_20200904002452.jpg', '2020-09-04 07:24:52', '2020-09-03 18:54:52'),
(764, 'PR0012', 'product_PR0012_3_20200904002452.jpg', '2020-09-04 07:24:52', '2020-09-03 18:54:53'),
(765, 'PR006', 'product_PR006_1_20200904004056.jpg', '2020-09-04 07:40:56', '2020-09-03 19:10:56'),
(766, 'PR006', 'product_PR006_2_20200904004056.jpg', '2020-09-04 07:40:56', '2020-09-03 19:10:56'),
(767, 'PR006', 'product_PR006_3_20200904004056.jpg', '2020-09-04 07:40:56', '2020-09-03 19:10:56'),
(768, 'PR006', 'product_PR006_4_20200904004056.jpg', '2020-09-04 07:40:56', '2020-09-03 19:10:56'),
(769, 'PR006', 'product_PR006_5_20200904004056.jpg', '2020-09-04 07:40:56', '2020-09-03 19:10:56'),
(770, 'PR0015', 'product_PR0015_1_20200904004500.jpg', '2020-09-04 07:45:00', '2020-09-03 19:15:00'),
(772, 'PR0015', 'product_PR0015_3_20200904004500.jpg', '2020-09-04 07:45:00', '2020-09-03 19:15:00'),
(773, 'MOON0017', 'product_MOON0017_1_20200904103228.jpg', '2020-09-04 17:32:28', '2020-09-04 05:02:28'),
(774, 'MOON0017', 'product_MOON0017_2_20200904103228.jpg', '2020-09-04 17:32:28', '2020-09-04 05:02:28'),
(775, 'MOON0017', 'product_MOON0017_3_20200904103228.jpg', '2020-09-04 17:32:28', '2020-09-04 05:02:28'),
(776, 'MOON0017', 'product_MOON0017_4_20200904103228.jpg', '2020-09-04 17:32:28', '2020-09-04 05:02:28'),
(777, 'PR0016', 'product_PR0016_1_20200904104258.jpg', '2020-09-04 17:42:58', '2020-09-04 05:12:58'),
(778, 'PR0016', 'product_PR0016_2_20200904104258.jpg', '2020-09-04 17:42:58', '2020-09-04 05:12:58'),
(779, 'PR0016', 'product_PR0016_3_20200904104258.jpg', '2020-09-04 17:42:58', '2020-09-04 05:12:58'),
(780, 'PR0016', 'product_PR0016_4_20200904104258.jpg', '2020-09-04 17:42:58', '2020-09-04 05:12:58'),
(781, 'PR0024', 'product_PR0024_1_20200904104738.jpg', '2020-09-04 17:47:38', '2020-09-04 05:17:38'),
(782, 'PR0024', 'product_PR0024_2_20200904104738.jpg', '2020-09-04 17:47:38', '2020-09-04 05:17:38'),
(783, 'MOON0096', 'product_MOON0096_1_20200904105222.jpg', '2020-09-04 17:52:22', '2020-09-04 05:22:22'),
(784, 'MOON0096', 'product_MOON0096_2_20200904105222.jpg', '2020-09-04 17:52:22', '2020-09-04 05:22:22'),
(785, 'MOON0096', 'product_MOON0096_3_20200904105222.jpg', '2020-09-04 17:52:22', '2020-09-04 05:22:22'),
(786, 'MOON0096', 'product_MOON0096_4_20200904105222.jpg', '2020-09-04 17:52:22', '2020-09-04 05:22:22'),
(787, 'MOON0096', 'product_MOON0096_5_20200904105222.jpg', '2020-09-04 17:52:22', '2020-09-04 05:22:22'),
(788, 'MOON0013', 'product_MOON0013_1_20200904105714.jpg', '2020-09-04 17:57:14', '2020-09-04 05:27:14'),
(789, 'MOON0013', 'product_MOON0013_2_20200904105714.jpg', '2020-09-04 17:57:14', '2020-09-04 05:27:14'),
(790, 'MOON0013', 'product_MOON0013_3_20200904105714.jpg', '2020-09-04 17:57:14', '2020-09-04 05:27:14'),
(791, 'PR005', 'product_PR005_1_20200904110312.jpg', '2020-09-04 18:03:12', '2020-09-04 05:33:12'),
(792, 'PR005', 'product_PR005_2_20200904110312.jpg', '2020-09-04 18:03:12', '2020-09-04 05:33:12'),
(793, 'PR005', 'product_PR005_3_20200904110312.jpg', '2020-09-04 18:03:12', '2020-09-04 05:33:12'),
(794, 'PR005', 'product_PR005_4_20200904110312.jpg', '2020-09-04 18:03:12', '2020-09-04 05:33:12'),
(795, 'PR005', 'product_PR005_5_20200904110312.jpg', '2020-09-04 18:03:12', '2020-09-04 05:33:12'),
(796, 'PR0017', 'product_PR0017_1_20200904113659.jpg', '2020-09-04 18:36:59', '2020-09-04 06:06:59'),
(797, 'PR0017', 'product_PR0017_2_20200904113659.jpg', '2020-09-04 18:36:59', '2020-09-04 06:06:59'),
(798, 'PR0017', 'product_PR0017_3_20200904113659.jpg', '2020-09-04 18:36:59', '2020-09-04 06:06:59'),
(799, 'PR003', 'product_PR003_1_20200904114434.jpg', '2020-09-04 18:44:34', '2020-09-04 06:14:35'),
(800, 'PR003', 'product_PR003_2_20200904114435.jpg', '2020-09-04 18:44:34', '2020-09-04 06:14:35'),
(801, 'PR003', 'product_PR003_3_20200904114435.jpg', '2020-09-04 18:44:34', '2020-09-04 06:14:35'),
(802, 'PR003', 'product_PR003_4_20200904114435.jpg', '2020-09-04 18:44:34', '2020-09-04 06:14:35'),
(803, 'MOON0097', 'product_MOON0097_1_20200904115128.jpg', '2020-09-04 18:51:28', '2020-09-04 06:21:28'),
(804, 'MOON0097', 'product_MOON0097_2_20200904115128.jpg', '2020-09-04 18:51:28', '2020-09-04 06:21:28'),
(805, 'MOON0097', 'product_MOON0097_3_20200904115128.jpg', '2020-09-04 18:51:28', '2020-09-04 06:21:28'),
(806, 'MOON0097', 'product_MOON0097_4_20200904115128.jpg', '2020-09-04 18:51:28', '2020-09-04 06:21:29'),
(807, 'MOON0097', 'product_MOON0097_5_20200904115129.jpg', '2020-09-04 18:51:28', '2020-09-04 06:21:29'),
(808, 'MOON0016', 'product_MOON0016_1_20200904115714.jpg', '2020-09-04 18:57:14', '2020-09-04 06:27:15'),
(809, 'MOON0016', 'product_MOON0016_2_20200904115715.jpg', '2020-09-04 18:57:14', '2020-09-04 06:27:15'),
(810, 'MOON0016', 'product_MOON0016_3_20200904115715.jpg', '2020-09-04 18:57:14', '2020-09-04 06:27:15'),
(811, 'MOON0016', 'product_MOON0016_4_20200904115715.jpg', '2020-09-04 18:57:14', '2020-09-04 06:27:15'),
(812, 'MOON0016', 'product_MOON0016_5_20200904115715.jpg', '2020-09-04 18:57:14', '2020-09-04 06:27:15'),
(813, 'MOON0093', 'product_MOON0093_1_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(814, 'MOON0093', 'product_MOON0093_2_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(815, 'MOON0093', 'product_MOON0093_3_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(816, 'MOON0093', 'product_MOON0093_4_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(817, 'MOON0093', 'product_MOON0093_5_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(818, 'MOON0093', 'product_MOON0093_6_20200904120314.jpg', '2020-09-04 19:03:14', '2020-09-04 06:33:14'),
(819, 'PR0018', 'product_PR0018_1_20200904121112.jpg', '2020-09-04 19:11:12', '2020-09-04 06:41:12'),
(820, 'PR0018', 'product_PR0018_2_20200904121112.jpg', '2020-09-04 19:11:12', '2020-09-04 06:41:12'),
(821, 'PR0018', 'product_PR0018_3_20200904121112.jpg', '2020-09-04 19:11:12', '2020-09-04 06:41:12'),
(822, 'PR0018', 'product_PR0018_4_20200904121112.jpg', '2020-09-04 19:11:12', '2020-09-04 06:41:12'),
(823, 'MOON0094', 'product_MOON0094_1_20200904121855.jpg', '2020-09-04 19:18:55', '2020-09-04 06:48:55'),
(824, 'MOON0094', 'product_MOON0094_2_20200904121855.jpg', '2020-09-04 19:18:55', '2020-09-04 06:48:55'),
(825, 'MOON0094', 'product_MOON0094_3_20200904121855.jpg', '2020-09-04 19:18:55', '2020-09-04 06:48:55'),
(826, 'MOON0094', 'product_MOON0094_1_20200904122102.jpg', '2020-09-04 19:21:02', '2020-09-04 06:51:02'),
(827, 'MOON0094', 'product_MOON0094_2_20200904122102.jpg', '2020-09-04 19:21:02', '2020-09-04 06:51:02'),
(828, 'MOON0094', 'product_MOON0094_3_20200904122102.jpg', '2020-09-04 19:21:02', '2020-09-04 06:51:02'),
(829, 'MOON0094', 'product_MOON0094_4_20200904122102.jpg', '2020-09-04 19:21:02', '2020-09-04 06:51:02'),
(830, 'MOON0094', 'product_MOON0094_5_20200904122102.jpg', '2020-09-04 19:21:02', '2020-09-04 06:51:02'),
(842, 'PR007', 'product_PR007_1_20200904123615.jpg', '2020-09-04 19:36:15', '2020-09-04 07:06:15'),
(843, 'PR007', 'product_PR007_2_20200904123615.jpg', '2020-09-04 19:36:15', '2020-09-04 07:06:15'),
(844, 'PR007', 'product_PR007_3_20200904123615.jpg', '2020-09-04 19:36:15', '2020-09-04 07:06:15'),
(845, 'PR007', 'product_PR007_4_20200904123615.jpg', '2020-09-04 19:36:15', '2020-09-04 07:06:15'),
(846, 'PR007', 'product_PR007_5_20200904123615.jpg', '2020-09-04 19:36:15', '2020-09-04 07:06:15'),
(847, 'MOON0095', 'product_MOON0095_1_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57'),
(848, 'MOON0095', 'product_MOON0095_2_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57'),
(849, 'MOON0095', 'product_MOON0095_3_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57'),
(850, 'MOON0095', 'product_MOON0095_4_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57'),
(851, 'MOON0095', 'product_MOON0095_5_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57'),
(852, 'MOON0095', 'product_MOON0095_6_20200904123957.jpg', '2020-09-04 19:39:57', '2020-09-04 07:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `ps_id` int(255) DEFAULT NULL,
  `pc_id` int(255) NOT NULL,
  `product` varchar(100) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `ps_id` int(255) NOT NULL,
  `product` varchar(100) NOT NULL,
  `challan` varchar(100) NOT NULL,
  `date` date DEFAULT NULL,
  `qty` int(100) NOT NULL,
  `start_date` varchar(50) DEFAULT NULL,
  `end_date` varchar(50) DEFAULT NULL,
  `place_order_date` varchar(50) DEFAULT NULL,
  `purchase_price` float NOT NULL,
  `status` int(10) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`ps_id`, `product`, `challan`, `date`, `qty`, `start_date`, `end_date`, `place_order_date`, `purchase_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'MOON-T-002', '1201', '2020-08-10', 10, NULL, NULL, NULL, 0, 1, '2020-08-10 11:04:01', '2020-08-10 11:04:01'),
(2, 'MOON-T-002', '1202', '2020-08-10', 5, NULL, NULL, NULL, 0, 1, '2020-08-10 11:15:14', '2020-08-10 11:15:14'),
(3, 'MOON-T-002', '1202', '2020-08-10', 5, NULL, NULL, NULL, 0, 1, '2020-08-10 11:15:31', '2020-08-10 11:15:31'),
(4, 'MOON-T-002', '1205', '2020-08-10', 5, NULL, NULL, NULL, 0, 1, '2020-08-10 11:17:47', '2020-08-10 11:17:47'),
(5, 'MOON-T-002', '1206', '2020-10-10', 2, NULL, NULL, NULL, 0, 1, '2020-08-10 11:21:31', '2020-08-10 11:21:31'),
(6, 'MOON0016', 'Test PO', '2020-08-13', 1, NULL, NULL, NULL, 0, 1, '2020-08-13 23:18:11', '2020-08-13 10:48:11'),
(7, 'MOON0016', '001', '2020-08-13', 2, NULL, NULL, NULL, 0, 1, '2020-08-14 11:47:41', '2020-08-13 23:17:41'),
(8, '213035', '001', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-17 12:46:25', '2020-08-17 00:16:25'),
(9, 'MOON0039', '0001', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:17:01', '2020-08-18 01:47:01'),
(10, 'MOON0038', '0002', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:17:18', '2020-08-18 01:47:18'),
(11, 'MOON0037', '0003', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:17:38', '2020-08-18 01:47:38'),
(12, 'MOON0036', '0004', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:18:03', '2020-08-18 01:48:03'),
(13, 'MOON0035', '0005', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:18:26', '2020-08-18 01:48:26'),
(14, 'MOON0033', '0006', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:18:49', '2020-08-18 01:48:49'),
(15, 'MOON0032', '0007', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:19:12', '2020-08-18 01:49:12'),
(16, 'MOON0030', '0007', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:19:34', '2020-08-18 01:49:34'),
(17, 'MOON0029', '0007', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:20:07', '2020-08-18 01:50:07'),
(18, 'MOON0028', '0008', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:21:54', '2020-08-18 01:51:54'),
(19, 'MOON0027', '0009', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:22:25', '2020-08-18 01:52:25'),
(20, 'MOON0027', '0009', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:22:34', '2020-08-18 01:52:34'),
(21, 'MOON0026', '00010', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:23:10', '2020-08-18 01:53:10'),
(22, 'MOON0025', '00011', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:23:51', '2020-08-18 01:53:51'),
(23, 'MOON0024', '00012', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:24:30', '2020-08-18 01:54:30'),
(24, 'MOON0023', '00013', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:25:01', '2020-08-18 01:55:01'),
(25, 'MOON0022', '00014', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:25:53', '2020-08-18 01:55:53'),
(26, 'MOON0021', '00015', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:26:20', '2020-08-18 01:56:20'),
(27, 'MOON0020', '00015', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:26:48', '2020-08-18 01:56:48'),
(28, 'MOON0019', '00018', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:27:15', '2020-08-18 01:57:15'),
(29, 'MOON0018', '00019', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:27:46', '2020-08-18 01:57:46'),
(30, 'MOON0017', '00020', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:28:17', '2020-08-18 01:58:17'),
(31, 'MOON0014', '00021', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:28:45', '2020-08-18 01:58:45'),
(32, 'MOON0013', '00022', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:29:12', '2020-08-18 01:59:12'),
(33, 'MOON0012', '00023', '2020-07-01', 3, NULL, NULL, NULL, 0, 1, '2020-08-18 14:29:42', '2020-08-18 01:59:42'),
(34, 'MOON0011', '00024', '2020-07-01', 3, NULL, NULL, NULL, 0, 1, '2020-08-18 14:30:17', '2020-08-18 02:00:17'),
(35, 'MOON0099', '00025', '2020-07-01', 3, NULL, NULL, NULL, 0, 1, '2020-08-18 14:30:43', '2020-08-18 02:00:43'),
(36, 'MOON0097', '00026', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:31:14', '2020-08-18 02:01:14'),
(37, 'MOON0096', '00028', '2020-07-01', 1, NULL, NULL, NULL, 0, 1, '2020-08-18 14:31:37', '2020-08-18 02:01:37'),
(38, 'MOON-AN-001', '00030', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:32:20', '2020-08-18 02:02:20'),
(39, 'MOON0092', '00030', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:33:03', '2020-08-18 02:03:03'),
(40, 'MOON0093', '00031', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:33:23', '2020-08-18 02:03:23'),
(41, 'MOON0094', '00031', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:33:58', '2020-08-18 02:03:58'),
(42, 'MOON0095', '00031', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:34:32', '2020-08-18 02:04:32'),
(43, 'MOON0091', '00032', '2020-07-01', 2, NULL, NULL, NULL, 0, 1, '2020-08-18 14:34:54', '2020-08-18 02:04:54'),
(44, '1105032', '2311121', '2020-08-27', 1, '', '', '', 12000, 1, '2020-08-27 16:41:47', '2020-08-27 04:11:47'),
(45, 'SK001', 'SK001', '2020-08-30', 2, '', '', '', 45000, 1, '2020-08-30 18:52:02', '2020-08-30 06:22:02'),
(46, 'PR0024', 'INV001', '2020-08-30', 1, '', '', '', 58236.9, 1, '2020-08-31 02:28:43', '2020-08-30 13:58:43'),
(47, 'GRU0024', 'INV001', '2020-08-30', 2, '', '', '', 24815.1, 1, '2020-08-31 02:29:38', '2020-08-30 13:59:38'),
(48, 'GRU0023', 'INV001', '2020-08-30', 1, '', '', '', 84371.2, 1, '2020-08-31 02:32:05', '2020-08-30 14:02:05'),
(49, 'PR0023', 'INV001', '2020-08-30', -15, '', '', '', 104374, 1, '2020-08-31 02:32:40', '2020-08-30 14:02:40'),
(50, 'PR0022', 'INV001', '2020-08-30', 1, '', '', '', 63460, 1, '2020-08-31 02:34:39', '2020-08-30 14:04:39'),
(51, 'PR0021', 'INV001', '2020-08-30', 1, '', '', '', 63460, 1, '2020-08-31 02:35:27', '2020-08-30 14:05:27'),
(52, 'GRU0022', 'INV001', '2020-08-30', 1, '', '', '', 208882, 1, '2020-08-31 02:36:03', '2020-08-30 14:06:03'),
(53, 'PR0020', 'INVOICE001', '2020-08-30', 1, '', '', '', 592608, 1, '2020-08-31 02:40:56', '2020-08-30 14:10:56'),
(54, 'GRU0021', 'INVOICE001', '2020-08-30', 1, '', '', '', 54419, 1, '2020-08-31 02:41:41', '2020-08-30 14:11:41'),
(55, 'GRU0020', 'INVOICE001', '2020-08-30', 1, '', '', '', 54419, 1, '2020-08-31 02:42:42', '2020-08-30 14:12:42'),
(56, 'PR0025', 'INVOICE001', '2020-08-30', 1, '', '', '', 17410.1, 1, '2020-08-31 02:43:04', '2020-08-30 14:13:04'),
(57, 'PR0019', 'INVOICE001', '2020-08-30', 2, '', '', '', 592608, 1, '2020-08-31 02:43:39', '2020-08-30 14:13:39'),
(58, 'GRU0019', 'INVOICE001', '2020-08-30', 2, '', '', '', 24815.1, 1, '2020-08-31 02:44:16', '2020-08-30 14:14:16'),
(59, 'PR0018', 'INVOICE001', '2020-08-30', 2, '', '', '', 205094, 1, '2020-08-31 02:46:12', '2020-08-30 14:16:12'),
(60, 'GRU0018', 'INVOICE001', '2020-08-30', 2, '', '', '', 24815.1, 1, '2020-08-31 02:46:34', '2020-08-30 14:16:34'),
(61, 'GRU0017', 'INVOICE001', '2020-08-24', 3, '', '', '', 152809, 1, '2020-08-31 02:47:30', '2020-08-30 14:17:30'),
(62, 'GRU0015', 'INVOICE001', '2020-08-05', 2, '', '', '', 45711.9, 1, '2020-08-31 02:47:51', '2020-08-30 14:17:51'),
(63, 'GRU0016', 'INVOICE001', '2020-08-13', 2, '', '', '', 152809, 1, '2020-08-31 02:48:19', '2020-08-30 14:18:19'),
(64, 'PR0016', 'INVOICE001', '2020-08-13', 1, '', '', '', 152809, 1, '2020-08-31 02:48:46', '2020-08-30 14:18:46'),
(65, 'PR0017', 'INVOICE001', '2020-08-30', 2, '', '', '', 54638.4, 1, '2020-08-31 02:49:11', '2020-08-30 14:19:11'),
(66, 'PR0015', 'INVOICE001', '2020-08-17', 2, '', '', '', 36082, 1, '2020-08-31 02:49:38', '2020-08-30 14:19:38'),
(67, 'GRU0013', 'INVOICE001', '2020-08-30', 1, '', '', '', 36082, 1, '2020-08-31 02:50:02', '2020-08-30 14:20:02'),
(68, 'PR0013', 'INVOICE001', '2020-08-20', 3, '', '', '', 365974, 1, '2020-08-31 02:50:47', '2020-08-30 14:20:47'),
(69, 'GRU0012', 'INVOICE001', '2020-08-29', 2, '', '', '', 164998, 1, '2020-08-31 02:51:21', '2020-08-30 14:21:21'),
(70, 'PR008', 'INVOICE001', '2020-08-19', 1, '', '', '', 164998, 1, '2020-08-31 02:51:47', '2020-08-30 14:21:47'),
(71, 'PR0012', 'INVOICE001', '2020-08-26', 4, '', '', '', 164998, 1, '2020-08-31 02:52:05', '2020-08-30 14:22:05'),
(72, 'PR0010', 'INVOICE001', '2020-08-11', 4, '', '', '', 214430, 1, '2020-08-31 02:52:30', '2020-08-30 14:22:30'),
(73, 'PR0011', 'INVOICE001', '2020-08-25', 2, '', '', '', 214430, 1, '2020-08-31 02:52:53', '2020-08-30 14:22:53'),
(74, 'GRU0011', 'INVOICE001', '2020-08-18', 2, '', '', '', 13495.9, 1, '2020-08-31 02:53:25', '2020-08-30 14:23:25'),
(75, 'PR009', 'INVOICE001', '2020-08-11', 5, '', '', '', 214430, 1, '2020-08-31 02:53:45', '2020-08-30 14:23:45'),
(76, 'PR007', 'INVOICE001', '2020-08-18', 2, '', '', '', 214430, 1, '2020-08-31 02:54:15', '2020-08-30 14:24:15'),
(77, 'GRU0010', 'INVOICE001', '2020-08-19', 2, '', '', '', 214430, 1, '2020-08-31 02:54:40', '2020-08-30 14:24:40'),
(78, 'GRU009', 'INVOICE001', '2020-08-25', 1, '', '', '', 10883.8, 1, '2020-08-31 02:55:10', '2020-08-30 14:25:10'),
(79, 'GRU008', 'INVOICE001', '2020-08-18', 1, '', '', '', 10883.8, 1, '2020-08-31 02:55:34', '2020-08-30 14:25:34'),
(80, 'GRU007', 'INVOICE001', '2020-08-21', 1, '', '', '', 11754.5, 1, '2020-08-31 02:55:55', '2020-08-30 14:25:55'),
(81, 'PR005', 'INVOICE001', '2020-08-06', 2, '', '', '', 63916, 1, '2020-08-31 02:56:26', '2020-08-30 14:26:26'),
(82, 'GRUOO6', 'INVOICE001', '2020-08-11', 2, '', '', '', 42229.1, 1, '2020-08-31 02:56:49', '2020-08-30 14:26:49'),
(83, 'PR006', 'INVOICE001', '2020-08-05', 1, '', '', '', 42267.4, 1, '2020-08-31 02:57:10', '2020-08-30 14:27:10'),
(84, 'GRU005', 'INVOICE001', '2020-08-14', 1, '', '', '', 14366.6, 1, '2020-08-31 02:57:29', '2020-08-30 14:27:29'),
(85, 'PR004', 'INVOICE001', '2020-08-12', 1, '', '', '', 72094.3, 1, '2020-08-31 02:57:51', '2020-08-30 14:27:51'),
(86, 'PR003', 'INVOICE001', '2020-08-08', 2, '', '', '', 72094.3, 1, '2020-08-31 02:58:32', '2020-08-30 14:28:32'),
(87, 'DIM007', 'INVOICE001', '2020-08-12', 2, '', '', '', 72094.3, 1, '2020-08-31 02:58:52', '2020-08-30 14:28:52'),
(88, 'DIM006', 'INVOICE001', '2020-08-14', 1, '', '', '', 60823, 1, '2020-08-31 02:59:26', '2020-08-30 14:29:26'),
(89, 'DIM005', 'INVOICE001', '2020-08-03', 1, '', '', '', 582466, 1, '2020-08-31 03:00:36', '2020-08-30 14:30:36'),
(90, 'DIM004', 'INVOICE001', '2020-08-04', 1, '', '', '', 582466, 1, '2020-08-31 03:00:56', '2020-08-30 14:30:56'),
(91, 'PR002', 'INVOICE001', '2020-07-30', 2, '', '', '', 35698.9, 1, '2020-08-31 03:01:17', '2020-08-30 14:31:17'),
(92, 'GRU004', 'INVOICE001', '2020-08-08', 4, '', '', '', 82716.9, 1, '2020-08-31 03:01:36', '2020-08-30 14:31:36'),
(93, 'GRU003', 'INVOICE001', '2020-07-31', 3, '', '', '', 82716.9, 1, '2020-08-31 03:01:58', '2020-08-30 14:31:58'),
(94, 'DIM003', 'INVOICE001', '2020-08-13', 1, '', '', '', 582466, 1, '2020-08-31 03:02:23', '2020-08-30 14:32:23'),
(95, 'DIM002', 'INVOICE001', '2020-08-12', 1, '', '', '', 74880.5, 1, '2020-08-31 03:02:47', '2020-08-30 14:32:47'),
(96, 'Mac0001', 'INVOICE001', '2020-08-06', 2, '', '', '', 199900, 1, '2020-08-31 03:03:24', '2020-08-30 14:33:24'),
(97, 'MVH22HN/A', 'INVOICE001', '2020-08-06', 1, '', '', '', 122990, 1, '2020-08-31 03:03:51', '2020-08-30 14:33:51'),
(98, 'MWHD2HN/A', 'INVOICE001', '2020-08-05', 1, '', '', '', 117100, 1, '2020-08-31 03:04:09', '2020-08-30 14:34:09'),
(99, 'DIM008', 'INVOICE001', '2020-07-09', 2, '', '', '', 214430, 1, '2020-08-31 12:05:04', '2020-08-30 23:35:04'),
(100, 'DIM009', 'INVOICE001', '2020-08-13', 1, '', '', '', 163511, 1, '2020-08-31 12:05:24', '2020-08-30 23:35:24'),
(101, 'DIM10', 'INVOICE001', '2020-09-16', 2, '', '', '', 2148, 1, '2020-09-03 12:43:50', '2020-09-03 00:13:50'),
(102, 'PR0050', 'INVOICE001', '2020-09-18', 3, '', '', '', 2148, 1, '2020-09-03 12:45:22', '2020-09-03 00:15:22'),
(103, 'PR0049', 'INVOICE001', '2020-09-17', 2, '', '', '', 76362, 1, '2020-09-03 12:47:02', '2020-09-03 00:17:02'),
(104, 'PR0048', 'INVOICE001', '2020-09-22', 2, '', '', '', 73308, 1, '2020-09-03 15:26:08', '2020-09-03 02:56:08'),
(105, 'PR0047', 'INVOICE001', '2020-09-17', 1, '', '', '', 65453, 1, '2020-09-03 15:26:37', '2020-09-03 02:56:37'),
(106, 'GRU0046', 'INVOICE001', '2020-09-17', 3, '', '', '', 44486, 1, '2020-09-03 15:28:15', '2020-09-03 02:58:15'),
(107, 'GRU0045', 'INVOICE001', '2020-09-18', 1, '', '', '', 44486, 1, '2020-09-03 15:28:53', '2020-09-03 02:58:53'),
(108, 'PR0046', 'INV001', '2020-09-02', 2, '', '', '', 32726, 1, '2020-09-03 15:32:28', '2020-09-03 03:02:28'),
(109, 'GRU0044', 'INV001', '2020-09-17', 3, '', '', '', 34716, 1, '2020-09-03 15:34:05', '2020-09-03 03:04:05'),
(110, 'GRU0043', 'INV001', '2020-09-09', 4, '', '', '', 34716, 1, '2020-09-03 15:35:15', '2020-09-03 03:05:15'),
(111, 'PR0045', 'INV001', '2020-09-25', 4, '', '', '', 32726, 1, '2020-09-03 15:36:39', '2020-09-03 03:06:39'),
(112, 'GRU0042', 'INV001', '2020-09-23', 3, '', '', '', 43177, 1, '2020-09-03 15:37:53', '2020-09-03 03:07:53'),
(113, 'GRU0060', 'INV001', '2020-09-15', 3, '', '', '', 52990, 1, '2020-09-03 22:17:51', '2020-09-03 09:47:51'),
(114, 'GRU0059', 'INV001', '2020-09-12', 2, '', '', '', 52990, 1, '2020-09-03 22:20:47', '2020-09-03 09:50:47'),
(115, 'GRU0058', 'INV001', '2020-09-13', 4, '', '', '', 52990, 1, '2020-09-03 22:31:53', '2020-09-03 10:01:53'),
(116, 'GRU0057', 'INV001', '2020-09-14', 2, '', '', '', 52990, 1, '2020-09-03 22:38:20', '2020-09-03 10:08:20'),
(117, 'GRU0056', 'INV001', '2020-09-17', 3, '', '', '', 52990, 1, '2020-09-03 22:40:12', '2020-09-03 10:10:12'),
(118, 'GRU0056', 'INV001', '2020-09-17', 3, '', '', '', 52990, 1, '2020-09-03 22:40:27', '2020-09-03 10:10:27'),
(119, 'GRU0055', 'INV001', '2020-09-14', 2, '', '', '', 52990, 1, '2020-09-03 22:44:13', '2020-09-03 10:14:13'),
(120, 'GRU0054', 'INV001', '2020-09-16', 3, '', '', '', 46909, 1, '2020-09-03 22:45:07', '2020-09-03 10:15:07'),
(121, 'GRU0053', 'INV001', '2020-09-15', 4, '', '', '', 46909, 1, '2020-09-03 22:47:14', '2020-09-03 10:17:14'),
(122, 'GRU0052', 'INV001', '2020-09-18', 3, '', '', '', 22586, 1, '2020-09-03 22:48:31', '2020-09-03 10:18:31'),
(123, 'GRU0051', 'INV001', '2020-09-15', 2, '', '', '', 69930, 1, '2020-09-03 22:50:19', '2020-09-03 10:20:19'),
(124, 'GRU0050', 'INV001', '2020-09-14', 4, '', '', '', 69930, 1, '2020-09-03 22:51:24', '2020-09-03 10:21:24'),
(125, 'GRU0049', 'INV001', '2020-09-13', 2, '', '', '', 58202, 1, '2020-09-03 22:52:02', '2020-09-03 10:22:02'),
(126, 'GRU0048', 'INV001', '2020-09-16', 4, '', '', '', 42131, 1, '2020-09-03 22:56:11', '2020-09-03 10:26:11'),
(127, 'PR0044', 'INV001', '2020-09-18', 3, '', '', '', 126980, 1, '2020-09-03 22:57:24', '2020-09-03 10:27:24'),
(128, 'PR0043', 'INV001', '2020-09-17', 3, '', '', '', 91635, 1, '2020-09-03 22:57:57', '2020-09-03 10:27:57'),
(129, 'PR0042', 'INV001', '2020-09-20', 3, '', '', '', 57599, 1, '2020-09-03 22:58:31', '2020-09-03 10:28:31'),
(130, 'PR0041', 'INV001', '2020-09-16', 3, '', '', '', 41891, 1, '2020-09-03 23:02:03', '2020-09-03 10:32:03'),
(131, 'GRU0041', 'INV001', '2020-09-17', 4, '', '', '', 73048, 1, '2020-09-03 23:02:46', '2020-09-03 10:32:46'),
(132, 'GRU0040', 'INV001', '2020-09-16', 4, '', '', '', 73048, 1, '2020-09-03 23:03:27', '2020-09-03 10:33:27'),
(133, 'GRU0039', 'INV001', '2020-09-12', 2, '', '', '', 43287, 1, '2020-09-03 23:06:50', '2020-09-03 10:36:50'),
(134, 'GRU0038', 'INV001', '2020-09-14', 3, '', '', '', 43287, 1, '2020-09-03 23:07:34', '2020-09-03 10:37:34'),
(135, 'GRU0037', 'INV001', '2020-09-18', 3, '', '', '', 15709, 1, '2020-09-03 23:08:43', '2020-09-03 10:38:43'),
(136, 'GRU0036', 'INV001', '2020-09-18', 4, '', '', '', 15709, 1, '2020-09-03 23:09:27', '2020-09-03 10:39:27'),
(137, 'PR0040', 'INV001', '2020-09-15', 4, '', '', '', 61091, 1, '2020-09-03 23:10:13', '2020-09-03 10:40:13'),
(138, 'PR0039', 'INV001', '2020-09-17', 2, '', '', '', 53673, 1, '2020-09-03 23:11:11', '2020-09-03 10:41:11'),
(139, 'PR0038', 'INV001', '2020-09-16', 3, '', '', '', 73310, 1, '2020-09-03 23:11:46', '2020-09-03 10:41:46'),
(140, 'PR0037', 'INV001', '2020-09-19', 4, '', '', '', 73254, 1, '2020-09-03 23:14:55', '2020-09-03 10:44:55'),
(141, 'PR0036', 'INV001', '2020-09-14', 2, '', '', '', 53632, 1, '2020-09-03 23:16:07', '2020-09-03 10:46:07'),
(142, 'GRU0035', 'INV001', '2020-09-16', 3, '', '', '', 146126, 1, '2020-09-03 23:16:40', '2020-09-03 10:46:40'),
(143, 'PR0035', 'INV001', '2020-09-18', 3, '', '', '', 28000, 1, '2020-09-03 23:17:18', '2020-09-03 10:47:18'),
(144, 'GRU0034', 'INV001', '2020-09-18', 3, '', '', '', 39375, 1, '2020-09-03 23:17:58', '2020-09-03 10:47:58'),
(145, 'PR0034', 'INV001', '2020-09-17', 3, '', '', '', 21875, 1, '2020-09-03 23:19:04', '2020-09-03 10:49:04'),
(146, 'PR0033', 'INV001', '2020-09-19', 4, '', '', '', 26250, 1, '2020-09-03 23:19:40', '2020-09-03 10:49:40'),
(147, 'GRU0033', 'INV001', '2020-09-19', 2, '', '', '', 16887, 1, '2020-09-03 23:20:24', '2020-09-03 10:50:24'),
(148, 'PR0032', 'INV001', '2020-09-18', 4, '', '', '', 21875, 1, '2020-09-03 23:21:06', '2020-09-03 10:51:06'),
(149, 'GRU0032', 'INV001', '2020-09-20', 4, '', '', '', 157064, 1, '2020-09-03 23:22:00', '2020-09-03 10:52:00'),
(150, 'PR0031', 'INV001', '2020-09-19', 3, '', '', '', 21437, 1, '2020-09-03 23:23:50', '2020-09-03 10:53:50'),
(151, 'GRU0031', 'INV001', '2020-09-19', 2, '', '', '', 15748, 1, '2020-09-03 23:25:49', '2020-09-03 10:55:49'),
(152, 'PR0030', 'INV001', '2020-09-20', 3, '', '', '', 27125, 1, '2020-09-03 23:27:38', '2020-09-03 10:57:38'),
(153, 'PR0051', 'INVOICE001', '2020-09-17', 2, '', '', '', 152809, 1, '2020-09-04 10:09:55', '2020-09-03 21:39:55'),
(154, 'GRU002', 'INVOICE001', '2020-09-18', 2, '', '', '', 54419, 1, '2020-09-04 10:11:16', '2020-09-03 21:41:16'),
(155, 'GRU001', 'INVOICE001', '2020-09-19', 2, '', '', '', 72094, 1, '2020-09-04 10:12:01', '2020-09-03 21:42:01'),
(156, '1320032', 'INVOICE001', '2020-09-15', 3, '', '', '', 72094, 1, '2020-09-04 10:12:35', '2020-09-03 21:42:35'),
(157, '1105041', 'INVOICE001', '2020-09-16', 2, '', '', '', 152809, 1, '2020-09-04 10:13:52', '2020-09-03 21:43:52'),
(158, '1105040', 'INVOICE001', '2020-09-16', 1, '', '', '', 54419, 1, '2020-09-04 10:14:25', '2020-09-03 21:44:25'),
(159, '1105039', 'INVOICE001', '2020-09-17', 2, '', '', '', 582466, 1, '2020-09-04 10:15:09', '2020-09-03 21:45:09'),
(160, '1105036', 'INVOICE001', '2020-09-15', 2, '', '', '', 214430, 1, '2020-09-04 10:15:50', '2020-09-03 21:45:50'),
(161, 'DMI17', 'INVOICE001', '2020-09-16', 2, '', '', '', 582466, 1, '2020-09-04 10:20:09', '2020-09-03 21:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `shop_by_cat`
--

CREATE TABLE `shop_by_cat` (
  `sb_id` int(255) NOT NULL,
  `title` text NOT NULL,
  `link` text DEFAULT NULL,
  `img` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `type` int(2) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shop_by_cat`
--

INSERT INTO `shop_by_cat` (`sb_id`, `title`, `link`, `img`, `status`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Trending: Shoes', 'https://lsmart.in/cat?cat=nbawm-_st!_', 'slider_20200817103538.png', 1, 1, '2020-08-17 05:05:38', '2020-08-17 05:05:38'),
(2, 'Trending: Men\'s Collection', 'https://lsmart.in/cat?cat=saqem-_st!_Men', 'slider_20200610000904.jpg', 1, 1, '2020-08-31 07:06:57', '2020-08-31 07:06:57'),
(3, 'Trending: Shoes', 'https://lsmart.in/cat?cat=nbawm-_st!_', 'slider_20200610000957.jpg', 1, 1, '2020-08-31 07:05:35', '2020-08-31 07:05:35'),
(4, 'Trending: Fragrances', 'https://lsmart.in/cat?cat=bedsz-gfqra_st!_Men', 'slider_20200610001054.jpg', 1, 1, '2020-08-31 07:04:47', '2020-08-31 07:04:47'),
(5, 'Shop Men\'s', 'https://lsmart.in/cat?cat=-_st!_Men', 'slider_20200610092745.jpeg', 1, 0, '2020-08-31 07:09:20', '2020-08-31 07:09:20'),
(6, 'Shop Women\'s ', 'https://lsmart.in/cat?cat=-_st!_Women', 'slider_20200610092822.jpg', 1, 0, '2020-08-31 07:08:31', '2020-08-31 07:08:31'),
(7, 'Shop Beauty', 'https://lsmart.in/cat?cat=bedsz-_st!_', 'slider_20200610092909.jpeg', 1, 0, '2020-08-17 05:14:39', '2020-08-17 05:14:39'),
(8, 'Shop Home &Tech', 'https://lsmart.in/cat?cat=njchx-_st!_', 'slider_20200610093050.jpg', 1, 0, '2020-08-17 05:14:57', '2020-08-17 05:14:57'),
(9, 'Shop Fragrance', 'https://lsmart.in/cat?cat=bedsz-_st!_', 'slider_20200610093428.jpg', 1, 0, '2020-08-17 05:18:16', '2020-08-17 05:18:16'),
(10, 'Shop Kid\'s', 'https://lsmart.in/cat?cat=xkmiv-_st!_', 'slider_20200610093625.jpg', 1, 0, '2020-08-17 05:18:36', '2020-08-17 05:18:36'),
(11, 'Women\'s Shoulder bags', 'https://lsmart.in/cat?cat=fancy-esowr_st!_', 'slider_20200611153743.png', 1, 2, '2020-08-31 08:44:22', '2020-08-31 08:44:22'),
(12, 'Women\'s Sandals', '', 'slider_20200611154413.png', 0, 1, '2020-08-17 05:07:48', '2020-08-17 05:07:48'),
(13, 'Men\'s Loafers', 'https://lsmart.in/cat?cat=nbawm-tyfcl_st!_Men', 'slider_20200611154732.png', 1, 2, '2020-08-31 07:17:02', '2020-08-31 07:17:02'),
(14, 'Women\'s Dresses', 'https://lsmart.in/cat?cat=saqem-vktjn_st!_Women', 'slider_20200831124551.jpg', 1, 2, '2020-08-31 07:15:51', '2020-08-31 07:15:51'),
(15, 'Women\'s Fragrance', 'https://lsmart.in/cat?cat=bedsz-gfqra_st!_Women', 'slider_20200611160551.png', 1, 2, '2020-08-31 07:13:45', '2020-08-31 07:13:45'),
(16, 'Women\'s Shirt', '', 'slider_20200611162606.png', 0, 2, '2020-08-17 05:08:35', '2020-08-17 05:08:35'),
(17, 'Women\'s Makeup', '', 'slider_20200611163007.png', 1, 2, '2020-08-17 05:08:46', '2020-08-17 05:08:46');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `store_id` int(255) NOT NULL,
  `store_key` varchar(10) NOT NULL,
  `store_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `gst` varchar(100) DEFAULT NULL,
  `password` text NOT NULL,
  `address` text NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`store_id`, `store_key`, `store_name`, `email`, `mobile`, `gst`, `password`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vrbsh', 'Bequest Group', 'gaganmeet@bequestgroup.com', '9899123123', '', 'e1b4755403710e0deb7aa5d45e43996d', '30/1 East Patel Nagar, Delhi ', 1, '2020-06-10 06:47:35', '2020-06-09 18:17:35');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `subcat_id` int(255) NOT NULL,
  `subcat_key` varchar(10) NOT NULL,
  `cat_key` varchar(10) NOT NULL,
  `subcategory` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`subcat_id`, `subcat_key`, `cat_key`, `subcategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rihfu', 'fancy', 'Backpacks', 1, '2020-06-10 06:30:38', '2020-06-09 18:00:38'),
(2, 'kxryd', 'fancy', 'Belt Bags', 1, '2020-06-10 06:30:51', '2020-06-09 18:00:51'),
(3, 'ihqtv', 'fancy', 'Bucket Bags', 1, '2020-06-10 06:31:11', '2020-06-09 18:01:11'),
(4, 'treia', 'fancy', 'Clutch Bags', 1, '2020-06-10 06:31:29', '2020-06-09 18:01:29'),
(5, 'mdokh', 'fancy', 'Cross Body Bags', 1, '2020-06-10 06:31:41', '2020-06-09 18:01:41'),
(6, 'exhwn', 'fancy', 'Evening Bags', 1, '2020-06-10 06:31:56', '2020-06-09 18:01:56'),
(7, 'puqjh', 'fancy', 'Luggage and Travel', 1, '2020-06-10 06:32:07', '2020-06-09 18:02:07'),
(8, 'esowr', 'fancy', 'Shoulder Bags', 1, '2020-06-10 06:32:18', '2020-06-09 18:02:18'),
(9, 'xrlas', 'fancy', 'Tote Bags', 1, '2020-06-10 06:32:29', '2020-06-09 18:02:29'),
(10, 'draim', 'nbawm', 'Ankle Boots', 1, '2020-06-10 06:42:23', '2020-06-09 18:12:23'),
(11, 'brnpl', 'nbawm', 'Boots', 1, '2020-06-10 06:42:35', '2020-06-09 18:12:35'),
(12, 'dgzfq', 'nbawm', 'Espadrilles', 1, '2020-06-10 06:42:44', '2020-06-09 18:12:44'),
(13, 'tfkum', 'nbawm', 'Evening Shoes', 1, '2020-06-10 06:43:13', '2020-06-09 18:13:13'),
(14, 'mwbki', 'nbawm', 'Flat Shoes', 1, '2020-06-10 06:43:25', '2020-06-09 18:13:25'),
(15, 'erblu', 'nbawm', 'Mules', 1, '2020-06-10 06:43:42', '2020-06-09 18:13:42'),
(16, 'hoplq', 'nbawm', 'Pumps', 1, '2020-06-10 06:43:55', '2020-06-09 18:13:55'),
(17, 'gmyjl', 'nbawm', 'Sandals', 1, '2020-06-10 06:44:08', '2020-06-09 18:14:08'),
(18, 'rewil', 'nbawm', 'Sneakers', 1, '2020-06-10 06:44:15', '2020-06-09 18:14:15'),
(19, 'qldej', 'nbawm', 'Boat Shoes ', 1, '2020-06-10 06:44:27', '2020-06-09 18:14:27'),
(20, 'qrckp', 'nbawm', 'Brogues ', 1, '2020-06-10 06:44:37', '2020-06-09 18:14:37'),
(21, 'mgqaj', 'nbawm', 'Chelsea Boots ', 1, '2020-06-10 06:44:50', '2020-06-09 18:14:50'),
(22, 'wlpdo', 'nbawm', 'Derby Shoes', 1, '2020-06-10 06:45:00', '2020-06-09 18:15:00'),
(23, 'ikvzp', 'nbawm', 'Driving Shoes ', 1, '2020-06-10 06:45:08', '2020-06-09 18:15:08'),
(24, 'elvku', 'nbawm', 'House Shoes ', 1, '2020-06-10 06:45:30', '2020-06-09 18:15:30'),
(25, 'ybmkr', 'nbawm', 'Lace-Up Boots ', 1, '2020-06-10 06:45:39', '2020-06-09 18:15:39'),
(26, 'tyfcl', 'nbawm', 'Loafers ', 1, '2020-06-10 06:46:44', '2020-06-09 18:16:44'),
(27, 'cseyg', 'nbawm', 'Monk Strap Shoes', 1, '2020-06-10 06:46:55', '2020-06-09 18:16:55'),
(28, 'hzvim', 'nbawm', 'Oxford Shoes ', 1, '2020-06-10 06:47:02', '2020-06-09 18:17:02'),
(29, 'ytzix', 'nbawm', 'Slides ', 1, '2020-06-10 06:47:10', '2020-06-09 18:17:10'),
(30, 'krhib', 'nbawm', 'Slippers ', 1, '2020-06-10 06:47:19', '2020-06-09 18:17:19'),
(31, 'alptd', 'nbawm', 'Summer Shoes ', 1, '2020-06-10 06:47:37', '2020-06-09 18:17:37'),
(32, 'ncotb', 'nbawm', 'Hiking Boots', 1, '2020-06-10 06:47:46', '2020-06-09 18:17:46'),
(33, 'ckmvp', 'njchx', 'Laptop', 1, '2020-06-10 07:03:05', '2020-06-09 18:33:05'),
(34, 'syodq', 'njchx', 'iPhone', 1, '2020-06-10 07:03:22', '2020-06-09 18:33:22'),
(35, 'cjwrx', 'njchx', 'iPad', 1, '2020-06-10 07:03:35', '2020-06-09 18:33:35'),
(36, 'pmwzx', 'njchx', 'Accessories', 1, '2020-06-10 07:03:49', '2020-06-09 18:33:49'),
(37, 'kziam', 'bedsz', 'Rompers & Sleepsuits', 0, '2020-06-09 19:01:47', '2020-06-09 19:01:47'),
(38, 'lqoew', 'bedsz', 'Dresses', 0, '2020-06-09 19:02:28', '2020-06-09 19:02:28'),
(39, 'pdlfn', 'bedsz', 'Tops', 0, '2020-06-09 19:02:14', '2020-06-09 19:02:14'),
(40, 'jdeix', 'bedsz', 'Coats', 0, '2020-06-09 19:02:02', '2020-06-09 19:02:02'),
(41, 'wuvbm', 'bedsz', 'Accessories', 1, '2020-06-10 07:08:06', '2020-06-09 18:38:06'),
(42, 'bmpzk', 'bedsz', 'Shoes', 0, '2020-06-09 19:01:12', '2020-06-09 19:01:12'),
(43, 'qlvxy', 'xkmiv', 'Rompers & Sleepsuits', 1, '2020-06-10 07:17:12', '2020-06-09 18:47:12'),
(44, 'ikgof', 'xkmiv', 'Dresses', 1, '2020-06-10 07:17:21', '2020-06-09 18:47:21'),
(45, 'fmtvc', 'xkmiv', 'Tops', 1, '2020-06-10 07:17:38', '2020-06-09 18:47:38'),
(46, 'gljqc', 'xkmiv', 'Coats', 1, '2020-06-10 07:17:48', '2020-06-09 18:47:48'),
(47, 'ajinq', 'xkmiv', 'Shoes', 1, '2020-06-10 07:18:13', '2020-06-09 18:48:13'),
(48, 'cohkq', 'xkmiv', 'T-Shirts', 1, '2020-06-10 07:18:31', '2020-06-09 18:48:31'),
(49, 'hceub', 'xkmiv', 'Trousers', 1, '2020-06-10 07:18:44', '2020-06-09 18:48:44'),
(50, 'xfqbo', 'xkmiv', 'Knitwear', 1, '2020-06-10 07:18:54', '2020-06-09 18:48:54'),
(51, 'gmpuo', 'xkmiv', 'Accessories', 1, '2020-06-10 07:19:04', '2020-06-09 18:49:04'),
(52, 'qpfel', 'bedsz', 'Bath and Body', 1, '2020-06-10 07:24:13', '2020-06-09 18:54:13'),
(53, 'gfqra', 'bedsz', 'Fragrance', 1, '2020-06-10 07:24:22', '2020-06-09 18:54:22'),
(54, 'dkoqc', 'bedsz', 'Haircare Sets ', 1, '2020-06-10 07:24:35', '2020-06-09 18:54:35'),
(55, 'voefn', 'bedsz', 'Shave ', 1, '2020-06-10 07:24:47', '2020-06-09 18:54:47'),
(56, 'wmzkj', 'bedsz', 'Skincare ', 1, '2020-06-10 07:24:57', '2020-06-09 18:54:57'),
(57, 'uzbqa', 'bedsz', 'Beauty Sets', 1, '2020-06-10 07:25:10', '2020-06-09 18:55:10'),
(58, 'xzpdg', 'bedsz', 'Candles', 1, '2020-06-10 07:25:19', '2020-06-09 18:55:19'),
(59, 'phbvo', 'bedsz', 'Cosmetic Cases', 1, '2020-06-10 07:25:30', '2020-06-09 18:55:30'),
(60, 'gwtef', 'bedsz', 'Handcare', 1, '2020-06-10 07:25:47', '2020-06-09 18:55:47'),
(61, 'srgfy', 'bedsz', 'Makeup', 1, '2020-06-10 07:25:55', '2020-06-09 18:55:55'),
(62, 'pkgix', 'bedsz', 'Tools and Devices', 1, '2020-06-10 07:26:07', '2020-06-09 18:56:07'),
(63, 'xxyxx', 'tzylx', 'Fine Watches', 1, '2020-08-18 12:06:48', '2020-08-18 12:06:48'),
(64, 'ctklh', 'zisqv', 'Earrings', 1, '2020-06-10 07:45:52', '2020-06-09 19:15:52'),
(65, 'wvbly', 'zisqv', 'Necklaces', 1, '2020-06-10 07:46:05', '2020-06-09 19:16:05'),
(66, 'lpvuo', 'zisqv', 'Bracelets', 1, '2020-06-10 07:46:16', '2020-06-09 19:16:16'),
(67, 'whznl', 'zisqv', 'Rings', 1, '2020-06-10 07:46:25', '2020-06-09 19:16:25'),
(68, 'ahyxc', 'saqem', 'Blazers', 1, '2020-06-10 07:51:35', '2020-06-09 19:21:35'),
(69, 'gjvqf', 'saqem', 'Coats', 1, '2020-06-10 07:51:48', '2020-06-09 19:21:48'),
(70, 'vktjn', 'saqem', 'Dresses', 1, '2020-06-10 07:51:58', '2020-06-09 19:21:58'),
(71, 'pneht', 'saqem', 'Gowns', 1, '2020-06-10 07:52:07', '2020-06-09 19:22:07'),
(72, 'wiflq', 'saqem', 'Jackets', 1, '2020-06-10 07:52:20', '2020-06-09 19:22:20'),
(73, 'iolzh', 'saqem', 'Jeans', 1, '2020-06-10 07:52:29', '2020-06-09 19:22:29'),
(74, 'lcxrf', 'saqem', 'Knitwear', 1, '2020-06-10 07:52:40', '2020-06-09 19:22:40'),
(75, 'etcmz', 'saqem', 'Lingerie', 1, '2020-06-10 07:52:49', '2020-06-09 19:22:49'),
(76, 'lpten', 'saqem', 'Nightwear', 1, '2020-06-10 07:52:58', '2020-06-09 19:22:58'),
(77, 'wifle', 'saqem', 'Skirts', 1, '2020-06-10 07:53:10', '2020-06-09 19:23:10'),
(78, 'cmzha', 'saqem', 'Sportswear', 1, '2020-06-10 07:53:18', '2020-06-09 19:23:18'),
(79, 'txmvd', 'saqem', 'Swimwear', 1, '2020-06-10 07:53:27', '2020-06-09 19:23:27'),
(80, 'ltpsw', 'saqem', 'Tops', 1, '2020-06-10 07:53:36', '2020-06-09 19:23:36'),
(81, 'mdrvj', 'saqem', 'Trousers', 1, '2020-06-10 07:53:45', '2020-06-09 19:23:45'),
(82, 'goyec', 'saqem', 'Polo Shirts', 1, '2020-06-10 07:53:59', '2020-06-09 19:23:59'),
(83, 'xplro', 'saqem', 'Shirts', 1, '2020-06-10 07:54:08', '2020-06-09 19:24:08'),
(84, 'nkbhl', 'saqem', 'Suits', 1, '2020-06-10 07:54:17', '2020-06-09 19:24:17'),
(85, 'ausei', 'saqem', 'Sweatshirts', 1, '2020-06-10 07:54:28', '2020-06-09 19:24:28'),
(86, 'ygtuj', 'saqem', 'T-Shirts', 1, '2020-06-10 07:54:39', '2020-06-09 19:24:39'),
(87, 'fgmdy', 'saqem', 'Underwear', 1, '2020-06-10 07:54:48', '2020-06-09 19:24:48'),
(88, 'ljcnx', 'lcgpa', 'Belts', 1, '2020-06-10 07:55:51', '2020-06-09 19:25:51'),
(89, 'vqosu', 'lcgpa', 'Hats', 1, '2020-06-10 07:55:59', '2020-06-09 19:25:59'),
(90, 'unqml', 'lcgpa', 'Luggage', 0, '2020-06-09 19:26:40', '2020-06-09 19:26:40'),
(91, 'bnxkh', 'lcgpa', 'Sunglasses', 1, '2020-06-10 07:56:23', '2020-06-09 19:26:23'),
(92, 'fmula', 'lcgpa', 'Wallets & Purses', 1, '2020-06-10 07:56:48', '2020-06-09 19:26:48'),
(93, 'pcyzr', 'lcgpa', 'Ties', 1, '2020-06-10 07:56:59', '2020-06-09 19:26:59'),
(94, 'dmfcj', 'lcgpa', 'Cufflinks', 1, '2020-06-10 07:57:12', '2020-06-09 19:27:12'),
(95, 'dmsie', 'fancy', 'Messenger Bag', 1, '2020-08-14 00:15:31', '2020-08-13 11:45:31'),
(96, 'pqmif', 'fancy', 'Briefcases', 1, '2020-08-14 04:10:08', '2020-08-13 15:40:08'),
(97, 'vuotm', 'fancy', 'Suitcases', 1, '2020-08-14 05:27:42', '2020-08-13 16:57:42'),
(98, 'kasfb', 'lcgpa', 'Watch Box', 1, '2020-08-16 20:25:44', '2020-08-16 07:55:44'),
(99, 'tmdeg', 'njchx', 'Mac', 1, '2020-08-27 06:24:36', '2020-08-26 17:54:36'),
(100, 'aembv', 'njchx', 'Computing', 1, '2020-08-27 17:33:07', '2020-08-27 05:03:07'),
(101, 'izvhf', 'nbawm', 'Trainers', 1, '2020-09-04 00:38:17', '2020-09-03 12:08:17');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `submenu_id` int(255) NOT NULL,
  `menu` int(255) NOT NULL,
  `submenu` varchar(100) NOT NULL,
  `subcategory` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`submenu_id`, `menu`, `submenu`, `subcategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Backpacks', 'rihfu', 1, '2020-06-10 06:50:09', '2020-06-09 18:20:09'),
(2, 1, 'Belt Bags', 'kxryd', 1, '2020-06-10 06:50:32', '2020-06-09 18:20:32'),
(3, 1, 'Bucket Bags', 'ihqtv', 1, '2020-06-10 06:50:43', '2020-06-09 18:20:43'),
(4, 1, 'Clutch Bags', 'treia', 1, '2020-06-10 06:50:56', '2020-06-09 18:20:56'),
(5, 1, 'Cross Body Bags', 'mdokh', 1, '2020-06-10 06:51:17', '2020-06-09 18:21:17'),
(6, 1, 'Evening Bags', 'exhwn', 1, '2020-06-10 06:51:36', '2020-06-09 18:21:36'),
(7, 1, 'Luggage and Travel', 'puqjh', 1, '2020-06-10 06:51:50', '2020-06-09 18:21:50'),
(8, 1, 'Shoulder Bags', 'esowr', 1, '2020-06-10 06:52:15', '2020-06-09 18:22:15'),
(9, 1, 'Tote Bags', 'xrlas', 1, '2020-06-10 06:52:39', '2020-06-09 18:22:39'),
(10, 2, 'Ankle Boots', 'draim', 1, '2020-06-10 06:53:07', '2020-06-09 18:23:07'),
(11, 2, 'Boots', 'brnpl', 1, '2020-06-10 06:53:21', '2020-06-09 18:23:21'),
(12, 2, 'Espadrilles', 'dgzfq', 1, '2020-06-10 06:53:35', '2020-06-09 18:23:35'),
(13, 2, 'Evening Shoes', 'tfkum', 1, '2020-06-10 06:53:49', '2020-06-09 18:23:49'),
(14, 2, 'Flat Shoes', 'mwbki', 1, '2020-06-10 06:54:07', '2020-06-09 18:24:07'),
(15, 2, 'Mules', 'erblu', 1, '2020-06-10 06:54:24', '2020-06-09 18:24:24'),
(16, 2, 'Pumps', 'hoplq', 1, '2020-06-10 06:54:45', '2020-06-09 18:24:45'),
(17, 2, 'Sandals', 'gmyjl', 1, '2020-06-10 06:55:02', '2020-06-09 18:25:02'),
(18, 2, 'Sneakers', 'rewil', 1, '2020-06-10 06:55:19', '2020-06-09 18:25:19'),
(19, 2, 'Boat Shoes', 'qldej', 1, '2020-06-10 06:55:46', '2020-06-09 18:25:46'),
(20, 2, 'Brogues', 'qrckp', 1, '2020-06-10 06:56:03', '2020-06-09 18:26:03'),
(21, 2, 'Chelsea Boots', 'mgqaj', 1, '2020-06-10 06:56:25', '2020-06-09 18:26:25'),
(22, 2, 'Derby Shoes', 'wlpdo', 1, '2020-06-10 06:56:50', '2020-06-09 18:26:50'),
(23, 2, 'Driving Shoes', 'ikvzp', 1, '2020-06-10 06:57:20', '2020-06-09 18:27:20'),
(24, 2, 'House Shoes', 'elvku', 1, '2020-06-10 06:57:36', '2020-06-09 18:27:36'),
(25, 2, 'Lace-Up Boots', 'ybmkr', 1, '2020-06-10 06:57:54', '2020-06-09 18:27:54'),
(26, 2, 'Loafers', 'tyfcl', 1, '2020-06-10 06:58:14', '2020-06-09 18:28:14'),
(27, 2, 'Monk Strap Shoes', 'cseyg', 1, '2020-06-10 06:58:33', '2020-06-09 18:28:33'),
(28, 2, 'Oxford Shoes', 'hzvim', 1, '2020-06-10 06:58:50', '2020-06-09 18:28:50'),
(29, 2, 'Slides ', 'ytzix', 1, '2020-06-10 06:59:11', '2020-06-09 18:29:11'),
(30, 2, 'Slippers', 'krhib', 1, '2020-06-10 06:59:43', '2020-06-09 18:29:43'),
(31, 2, 'Summer Shoes ', 'alptd', 1, '2020-06-10 07:00:27', '2020-06-09 18:30:27'),
(32, 2, 'Hiking Boots', 'ncotb', 1, '2020-06-10 07:00:39', '2020-06-09 18:30:39'),
(33, 3, 'Laptop', 'ckmvp', 1, '2020-06-10 07:05:26', '2020-06-09 18:35:26'),
(34, 3, 'iPhone', 'syodq', 1, '2020-06-10 07:05:38', '2020-06-09 18:35:38'),
(35, 3, 'iPad', 'cjwrx', 1, '2020-06-10 07:05:49', '2020-06-09 18:35:49'),
(36, 3, 'Accessories', 'pmwzx', 1, '2020-06-10 07:06:03', '2020-06-09 18:36:03'),
(37, 4, 'Rompers & Sleepsuits', 'qlvxy', 1, '2020-06-10 07:19:48', '2020-06-09 18:49:48'),
(38, 4, 'Dresses', 'ikgof', 1, '2020-06-10 07:20:01', '2020-06-09 18:50:01'),
(39, 4, 'Tops', 'fmtvc', 1, '2020-06-10 07:20:14', '2020-06-09 18:50:14'),
(40, 4, 'Coats', 'gljqc', 1, '2020-06-10 07:20:24', '2020-06-09 18:50:24'),
(41, 4, 'Shoes', 'ajinq', 1, '2020-06-10 07:20:46', '2020-06-09 18:50:46'),
(42, 4, 'T-Shirts', 'cohkq', 1, '2020-06-10 07:21:03', '2020-06-09 18:51:03'),
(43, 4, 'Trousers', 'hceub', 1, '2020-06-10 07:21:26', '2020-06-09 18:51:26'),
(44, 4, 'Knitwear', 'xfqbo', 1, '2020-06-10 07:21:38', '2020-06-09 18:51:38'),
(45, 5, 'Bath and Body', 'qpfel', 1, '2020-06-10 07:33:00', '2020-06-09 19:03:00'),
(46, 5, 'Fragrance ', 'gfqra', 1, '2020-06-10 07:33:11', '2020-06-09 19:03:11'),
(47, 5, 'Haircare Sets ', 'dkoqc', 1, '2020-06-10 07:33:28', '2020-06-09 19:03:28'),
(48, 5, 'Shave', 'voefn', 1, '2020-06-10 07:33:56', '2020-06-09 19:03:56'),
(49, 5, 'Skincare', 'wmzkj', 1, '2020-06-10 07:34:09', '2020-06-09 19:04:09'),
(50, 5, 'Beauty Sets', 'uzbqa', 1, '2020-06-10 07:34:24', '2020-06-09 19:04:24'),
(51, 5, 'Candles', 'xzpdg', 1, '2020-06-10 07:34:35', '2020-06-09 19:04:35'),
(52, 5, 'Cosmetic Cases', 'phbvo', 1, '2020-06-10 07:34:47', '2020-06-09 19:04:47'),
(53, 5, 'Handcare', 'gwtef', 1, '2020-06-10 07:35:01', '2020-06-09 19:05:01'),
(54, 5, 'Makeup', 'srgfy', 1, '2020-06-10 07:35:18', '2020-06-09 19:05:18'),
(55, 5, 'Tools and Devices', 'pkgix', 1, '2020-06-10 07:35:35', '2020-06-09 19:05:35'),
(56, 6, 'Earrings', 'ctklh', 1, '2020-06-10 07:47:44', '2020-06-09 19:17:44'),
(57, 6, 'Necklaces', 'wvbly', 1, '2020-06-10 07:47:58', '2020-06-09 19:17:58'),
(58, 6, 'Bracelets', 'lpvuo', 1, '2020-06-10 07:48:38', '2020-06-09 19:18:38'),
(59, 6, 'Rings', 'whznl', 1, '2020-06-10 07:48:57', '2020-06-09 19:18:57'),
(60, 6, 'Fine Watches', 'dklgu', 1, '2020-06-10 07:49:12', '2020-06-09 19:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_cart`
--

CREATE TABLE `user_cart` (
  `uc_id` int(255) NOT NULL,
  `uc_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_cart`
--

INSERT INTO `user_cart` (`uc_id`, `uc_key`, `user`, `product`, `color`, `size`, `qty`, `created_at`, `updated_at`) VALUES
(13, 'lwkhs', 'admin', 'MOON0099', 'TOBACCO BIS', 'TU', 1, '2020-08-19 01:02:23', '2020-08-18 12:32:23'),
(14, 'evhoa', 'admin', 'MOON0014', 'TDM SCURO', 'TU', 1, '2020-08-19 01:02:23', '2020-08-18 12:32:23'),
(20, 'xbopn', 'oumkh', 'MOON0011', 'TOBACCO BIS', 'TU', 0, '2020-08-28 21:45:49', '2020-08-28 09:15:49'),
(33, 'tnvra', 'ywifr', 'PR0025', 'BLACKSILVER', '', 1, '2020-09-03 22:43:55', '2020-09-03 10:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `w_id` int(255) NOT NULL,
  `w_key` varchar(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `product` varchar(10) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`w_id`, `w_key`, `user`, `product`, `color`, `size`, `qty`, `created_at`, `updated_at`) VALUES
(9, 'pwjgv', 'xhqgm', 'MOON0091', 'NAVY', '7', '1', '2020-09-03 17:51:30', '2020-09-03 05:21:30'),
(13, 'tzhpi', 'ywifr', 'GRU0024', 'BROWN', '', '1', '2020-09-03 22:45:24', '2020-09-03 10:15:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`ab_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`au_id`);

--
-- Indexes for table `apple`
--
ALTER TABLE `apple`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  ADD PRIMARY KEY (`at_id`);

--
-- Indexes for table `berluti`
--
ALTER TABLE `berluti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `brand_profile`
--
ALTER TABLE `brand_profile`
  ADD PRIMARY KEY (`bf_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`deg_id`);

--
-- Indexes for table `fit`
--
ALTER TABLE `fit`
  ADD PRIMARY KEY (`fit_id`);

--
-- Indexes for table `home_section`
--
ALTER TABLE `home_section`
  ADD PRIMARY KEY (`hs_id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`inv_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `orders_status`
--
ALTER TABLE `orders_status`
  ADD PRIMARY KEY (`os_id`);

--
-- Indexes for table `order_book`
--
ALTER TABLE `order_book`
  ADD PRIMARY KEY (`ob_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `sku` (`sku`),
  ADD KEY `product_name` (`product_name`(768)),
  ADD KEY `brand` (`brand`),
  ADD KEY `category` (`category`),
  ADD KEY `subcategory` (`subcategory`),
  ADD KEY `mrp` (`mrp`),
  ADD KEY `price` (`price`);

--
-- Indexes for table `products_db`
--
ALTER TABLE `products_db`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `pname` (`product_name`(768)),
  ADD KEY `category` (`category`),
  ADD KEY `subcategory` (`subcategory`),
  ADD KEY `price` (`price`),
  ADD KEY `stock` (`stock`),
  ADD KEY `offer` (`offer`),
  ADD KEY `status` (`status`),
  ADD KEY `store` (`store`),
  ADD KEY `brand` (`brand`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`pa_id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`pd_id`),
  ADD KEY `product` (`product`),
  ADD KEY `color` (`color`),
  ADD KEY `size` (`size`),
  ADD KEY `name` (`name`),
  ADD KEY `value` (`value`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `shop_by_cat`
--
ALTER TABLE `shop_by_cat`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`),
  ADD KEY `store_mobile` (`mobile`),
  ADD KEY `store` (`store_key`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`submenu_id`);

--
-- Indexes for table `user_cart`
--
ALTER TABLE `user_cart`
  ADD PRIMARY KEY (`uc_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`w_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address_book`
--
ALTER TABLE `address_book`
  MODIFY `ab_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `au_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `apple`
--
ALTER TABLE `apple`
  MODIFY `aid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  MODIFY `at_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `berluti`
--
ALTER TABLE `berluti`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  MODIFY `ba_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `brand_profile`
--
ALTER TABLE `brand_profile`
  MODIFY `bf_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `deg_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fit`
--
ALTER TABLE `fit`
  MODIFY `fit_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_section`
--
ALTER TABLE `home_section`
  MODIFY `hs_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `slider_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `inv_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders_status`
--
ALTER TABLE `orders_status`
  MODIFY `os_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_book`
--
ALTER TABLE `order_book`
  MODIFY `ob_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `p_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `products_db`
--
ALTER TABLE `products_db`
  MODIFY `p_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `pa_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `pc_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `pd_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `pi_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=853;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `ps_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `shop_by_cat`
--
ALTER TABLE `shop_by_cat`
  MODIFY `sb_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `subcat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `submenu_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `user_cart`
--
ALTER TABLE `user_cart`
  MODIFY `uc_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `w_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
