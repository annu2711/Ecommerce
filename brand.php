<?php 
include_once "helpers/index.php";
if(isset($_GET['brand'])){
	extract($_GET);
	if(!empty($brand)){
	$urlarray = explode("_", $brand);
	$brand = "";
	$prf = "";
	$genderr =  "";
	$category = "";
	$product = "";
	$bd = "";
	
	if(!empty($urlarray[0])){
		$bd = getSinglevalue('brands', 'brand_name', $urlarray[0], 1);
		$brand = "AND t1.brand='$bd'";
	}
	if(!empty($urlarray[1])){
	    if($urlarray[1] === 'Men' || $urlarray[1] === 'Women'){
			$prf = "AND t2.gender IN ('".$urlarray[1]."','UNISEX')";
			$genderr = "AND t1.gender IN ('".$urlarray[1]."','UNISEX')";
		}else{
			$prf = "AND t2.gender='".$urlarray[1]."'";
			$genderr = "AND t1.gender='$urlarray[1]'";
		}
// 		$prf = "AND t2.gender='$urlarray[1]'";
		$genderr = "AND t1.gender='$urlarray[1]'";
	}
	if(!empty($urlarray[2])){
		$category = "AND t1.category='$urlarray[2]'";
	}
	$countq = mysqli_query($conn, "SELECT t1.p_id FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $brand $category $prf GROUP BY t2.color,t2.groupid, IF(t2.color='', t2.p_id, 0)");
    if($countq){
        $prdrows = mysqli_num_rows($countq);
        if($prdrows > 12){
            // echo "rows =>".$rows;
            $totalpages = ceil($prdrows / 12);
            echo'<input type="hidden" id="totalpages" value="'.$totalpages.'"><input type="hidden" id="currentpage" value="1">';
        }else{
            // echo "rows2 =>".$rows;
            echo'<input type="hidden" id="totalpages" value="1"><input type="hidden" id="currentpage" value="1">';
        }
	}else{
		echo mysqli_error($conn);
	}
	
	// data fetching
	$query = mysqli_query($conn, "SELECT t1.* FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $brand $category $prf GROUP BY t2.color,t2.groupid, IF(t2.color='', t2.p_id, 0) ORDER BY price ASC LIMIT 12");
// 	GROUP BY t2.color, IF(t2.color='', t2.p_id, 0)
	if($query){
        if(mysqli_num_rows($query) > 0){
			while($products = mysqli_fetch_assoc($query)){
				$sku = $products['sku'];
				$imgsql = mysqli_query($conn, "SELECT image FROM product_images WHERE product='$sku' ORDER BY pi_id ASC LIMIT 2");
				if($imgsql){
					if(mysqli_num_rows($imgsql) > 0){
						$imgarr = [];
						while($imgrow = mysqli_fetch_assoc($imgsql)){
							$imgarr[] = $imgrow['image'];
						}
					}
				}
				$f_image = "no-product.png";
				if(file_exists('https://bquestindia.com/products/'.$imgarr[0])){
					$f_image = $imgarr[0];
				}
			$product = $product.'
				<div class="col-xl-4 col-6">
				<div class="categoryOuter">
				<div class="categoryOuterImg">
					<a href="single-product?p='.$products['sku'].'">
						<img src="'.PRODUCT_IMAGE_URL.$f_image.'" class="imgWithOutHover" class="img-fluid" alt="">';
						if(empty($imgarr[1])){
							$product = $product.'<img src="'.PRODUCT_IMAGE_URL.$f_image.'" class="imgWithHover" class="img-fluid" alt="">';
						}else{
							$product = $product.'<img src="'.PRODUCT_IMAGE_URL.$imgarr[1].'" class="imgWithHover" class="img-fluid" alt="">';
						}
			$product = $product.'</a>
					</div>
					<div class="categoryButton">
					<div class="categoryContent">
						<h6>'.getSinglevalue('brands', 'brand_key', $products['brand'], 3).'</h6>
						<p>'.$products['product_name'].'</p>
						<p><i class="fa fa-inr"></i> '.str_replace('INR', '',money_format('%i', $products['price'])).'</p>
					</div>
					</div>
				</div>
			</div>
				';
			}
		}else{
            $error = '<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>';
        }
	}else{
		echo mysqli_error($conn);
	}
	}
}else{
    header('location: index');
}

?>


<input type="hidden" id="filter" value="0">
<section class="category py-2 py-sm-3 py-md-5">
	<div class="container">
		<div class="row">
			<?php 
				$query = mysqli_query($conn, "SELECT * FROM brand_profile WHERE brand_key='$bd' AND status=1 ORDER BY bf_id DESC LIMIT 1");
				if($query){
					if(mysqli_num_rows($query) > 0){
						$bf_data = mysqli_fetch_array($query); ?>
			
			<div class="col-12 categoryHeaderDiv" style="background: url(<?php echo WEBSITE_IMAGE_URL.$bf_data[4] ?>);">
				<div class="row">
					<div class="col-lg-6">
						<div class="categoryName">
							<h5><?php echo $urlarray[0] ?></h5>
							<div class="bp-content paraText" id="paraText">
							<?php echo $bf_data[3] ?>
							<a href="javascript:;" class="readMore"><span  onclick="readLessMore()">... more</span></a>
							</div>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					 $(document).ready(function(){
		              var textHeigth = document.getElementById('paraText').offsetHeight;
		              console.log('hei', textHeigth)
		             if(textHeigth > 58){
		              $('.paraText').addClass('active');
		              $('.paraText a').addClass('active');
		             } else {
		              $('.paraText').removeClass('active');
		              $('.paraText a').removeClass('active');
		             }
		            });


				</script>
			</div>
			<?php		}
				}
			?>
            <?php if($bd!=""){ 
                if($prdrows > 0){
            ?>
			<div class="col-lg-3 d-none d-lg-block">
				<div class="filterMainDiv">
					<p><span class="pd-count"><?php echo $prdrows; ?></span> Results</p>
					<div class="accordion">
				<!-- category starts from here -->
				
				<input type="checkbox" name="brand[]" value="<?php echo $bd ?>" checked style="visibility: hidden;">	
				<?php    
				$query = mysqli_query($conn, "SELECT DISTINCT(a.category) as cat FROM `product` as a WHERE publish=1 AND brand='$bd'");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Category</h4>
							<p class="mb-0"><?php echo (!empty($urlarray[2])) ? getSinglevalue('category', 'cat_key', $urlarray[2], 2) : 'All'; ?></p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
									<div class="scrolly">
									<div class="checkboxGroup my-lg-2">
										<p><a href="brand?brand=<?php echo $urlarray[0] ?>_<?php echo $urlarray[1] ?>_" <?php echo empty($urlarray[2]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
									</div>
									<?php  
									while($rows = mysqli_fetch_assoc($query)){ 
										$cat = $rows['cat'];
										$subsql = mysqli_query($conn, "SELECT count(p_id) FROM product where publish=1 AND category='$cat' AND brand='$bd' GROUP BY groupid");
										$rc = mysqli_num_rows($subsql);
										?>
										<div class="checkboxGroup my-lg-2">
											<p><a href="brand?brand=<?php echo $urlarray[0] ?>_<?php echo $urlarray[1] ?>_<?php echo $rows['cat'] ?>" <?php echo ($urlarray[2] == $rows['cat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
                                    <?php } ?>
									<?php }
                                    }
                            ?>	
									</div>
							</div>
						</div>
						<!-- Category Ends here  -->

						<!-- color starts from here -->
						<?php 
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.color!='' $genderr");
                            }else{
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.brand='$bd' AND t1.color!='' $genderr");
                            }

                            if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Color</h4>
							<p class="mb-0" id="color-length">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="color[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')" value="<?php echo $rows['color'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['color'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>
									</div>
								</form>
							</div>
						</div>
									<?php } } ?>
						<!-- color ends here -->

					   <!-- Size Starts From Here -->
						<?php 
                            $pass = 0;
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size, (SELECT unit FROM product_attributes WHERE pa_id=t1.pa_id) as unit FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.size!='' $genderr");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size, (SELECT unit FROM product_attributes WHERE pa_id=t1.pa_id) as unit FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.brand='$bd' AND t1.size!='' $genderr");
                            }
                            	if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Size</h4>
							<p class="mb-0" id="size-length">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="size[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')" value="<?php echo $rows['size'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['size']." ".$rows['unit'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } } ?>
					<!-- size end here -->

					<!-- price range starts from here -->
					<?php
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd' AND category='$urlarray[2]'");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd'");
                            }
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
					<div class="accordion-toggle" style="visibility: hidden">
							<h4 class="mb-0">Rate</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
	<div id="rangeBox">    
         <div id="sliderBox" class="rangeslider">
             <input type="range" id="slider0to50" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
             <input type="range" id="slider51to100" min="0" max="<?php echo $priceRows['maxprice'] ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
         </div>
         <div id="inputRange" class="priceRangeslider">
             <input type="number" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" value="<?php echo $priceRows['minprice'] ?>" id="min" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
             <input type="number" min="0" max="<?php echo $priceRows['maxprice'] ?>" value="<?php echo $priceRows['maxprice'] ?>" id="max" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
         </div>
	</div>

									</div>
									<?php } }else{
										echo mysqli_error($conn);
									} ?>
					<!-- Price range ends here -->

					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="row justify-content-end">
					<div class="col-lg-12 col-6 mt-0 mt-md-2 mt-lg-4 mb-2 d-block d-lg-none">
						<div class="mobileFilterDiv">
							<a href="" data-toggle="modal" data-target="#exampleModalCenter">
								<p><img src="img/filter.svg" alt=""> Filter <span><?php echo $prdrows; ?></span> result</p>
							</a>
						</div>
					</div>
					<div class="col-lg-12 col-6 text-right mt-0 mt-md-2 mt-lg-4 mb-2">
						<div class="categoryDropdown">
						<form>
							  <select class="searchInput" id="sorting" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
							    <option selected value="0">Price Low to High</option>
							    <option value="1">Price High to Low</option>
							  </select>
							</form>
						</div>
					</div>
					<div class="col-12">
						<div class="row" id="grid">
						<?php 
                        if(!empty($product)){ 
                            echo $product;
                        }else{
                            echo $error;
                        }
                        ?>

							
						</div>
					</div>
				</div>
				<div class="loaderCenter text-center"  id="loaderOuterProduct" style="display: none">
				<div class="loaderOuter2 text-center">
					<div class="categoryLoader"></div>
					<p><strong>Loading.....</strong></p>
				</div>
				</div>
			</div>
			<?php }else{ ?>
			<div class="col-md-12">
				<div class="col-md-12 text-center noProduct"><p><strong>No products available.</strong></p></div>
				</div>
			<?php } }else{ ?>
			    <div class="col-md-12">
				<div class="col-md-12 text-center noProduct"><p><strong>No products available.</strong></p></div>
				</div>
			<?php } ?>
		</div>
		
	</div>
</section>


<!-- filter modal  -->
<!-- Modal -->
<div class="modal fade filterMobileModal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-3">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLongTitle">Filter</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
<?php if(!empty($product)){ ?>
				<div class="filterMainDiv mt-0">
					<p class="mb-0"><span class="pd-count"><?php echo $prdrows; ?></span> Results</p>
					<div class="accordion">
				
					<!-- category starts from here -->	
				<?php    
				$query = mysqli_query($conn, "SELECT DISTINCT(a.category) as cat FROM `product` as a WHERE publish=1 AND brand='$bd'");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Category</h4>
							<p class="mb-0"><?php echo (!empty($urlarray[2])) ? getSinglevalue('category', 'cat_key', $urlarray[2], 2) : 'All'; ?></p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
									<div class="scrolly">
									<div class="checkboxGroup my-lg-2">
										<p><a href="brand?brand=<?php echo $urlarray[0] ?>_<?php echo $urlarray[1] ?>_" <?php echo empty($urlarray[2]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
									</div>
									<?php  
									while($rows = mysqli_fetch_assoc($query)){ 
										$cat = $rows['cat'];
										$subsql = mysqli_query($conn, "SELECT count(p_id) FROM product where publish=1 AND category='$cat' AND brand='$bd' GROUP BY groupid");
										$rc = mysqli_num_rows($subsql);
										?>
										<div class="checkboxGroup my-lg-2">
											<p><a href="brand?brand=<?php echo $urlarray[0] ?>_<?php echo $urlarray[1] ?>_<?php echo $rows['cat'] ?>" <?php echo ($urlarray[2] == $rows['cat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
                                    <?php } ?>
									<?php }
                                    }
                            ?>	
									</div>
							</div>
						</div>
						<!-- Category Ends here  -->

						<!-- color starts from here -->
						<?php 
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.color!='' $genderr");
                            }else{
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.brand='$bd' AND t1.color!='' $genderr");
                            }

                            if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Color</h4>
							<p class="mb-0" id="color-length">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="color[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')" value="<?php echo $rows['color'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['color'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>
									</div>
								</form>
							</div>
						</div>
									<?php } } ?>
						<!-- color ends here -->

					   <!-- Size Starts From Here -->
						<?php 
                            $pass = 0;
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.size!='' $genderr");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.brand='$bd' AND t1.size!='' $genderr");
                            }
                            	if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Size</h4>
							<p class="mb-0" id="size-length">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="size[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')" value="<?php echo $rows['size'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['size'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } } ?>
					<!-- size end here -->

					<!-- price range starts from here -->
					<?php
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd' AND category='$urlarray[2]'");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd'");
                            }
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
					<div class="accordion-toggle" style="visibility: hidden">
							<h4 class="mb-0">Rate</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
	<div id="rangeBox">    
         <div id="sliderBox" class="rangeslider">
             <input type="range" id="slider0to50" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
             <input type="range" id="slider51to100" min="0" max="<?php echo $priceRows['maxprice'] ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
         </div>
         <div id="inputRange" class="priceRangeslider">
             <input type="number" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" value="<?php echo $priceRows['minprice'] ?>" id="min" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
             <input type="number" min="0" max="<?php echo $priceRows['maxprice'] ?>" value="<?php echo $priceRows['maxprice'] ?>" id="max" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '', '<?php echo $urlarray[1] ?>')">
         </div>
	</div>

									</div>
									<?php } }else{
										echo mysqli_error($conn);
									} ?>
					<!-- Price range ends here -->
					


					<div class="applyBtn">
								<div class="row">
									<div class="col-6">
										<div class="clearFilterBtn">
											<a href="javascript;:" onclick="refreshPage()" data-dismiss="modal" aria-label="Close">Clear filters</a>
										</div>
									</div>
									<div class="col-6">
										<div class="applyBtnInn">
										<a href="javascript;:" data-dismiss="modal" aria-label="Close">Apply</a>
										</div>
									</div>
								</div>
					</div>
					</div>
				</div>
								
					<?php }?>
	
      </div>  
    </div>
  </div>
</div>
<!-- filter modal ends -->
<script type="text/javascript">
	  var lines = $(".paraText p").val().split("\n"); 
</script>
<?php include_once "helpers/footer.php"; ?>