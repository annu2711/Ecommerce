<?php 
include_once"helpers/index.php";
if(isset($_GET['brand'])){
	extract($_GET);
	$urlarray = explode("_", $brand);
	$brand = "";
	$prf = "";
	$category = "";
	$product = "";
	$bd = "";
	if(!empty($urlarray[0])){
		$bd = getSinglevalue('brands', 'brand_name', $urlarray[0], 1);
		$brand = "AND t1.brand='$bd'";
	}
	if(!empty($urlarray[1])){
		$prf = "AND t2.gender='$urlarray[1]'";
	}
	if(!empty($urlarray[2])){
		$category = "AND t1.category='$urlarray[2]'";
	}
	echo '<input type="hidden" name="brand[]" value="'.$bd.'">';
	// count products
	$countq = mysqli_query($conn, "SELECT t1.p_id FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $brand $category $prf GROUP BY t1.groupid");
    if($countq){
        $prdrows = mysqli_num_rows($countq);
        if($prdrows > 12){
            // echo "rows =>".$rows;
            $totalpages = ceil($prdrows / 12);
            echo'<input type="hidden" id="totalpages" value="'.$totalpages.'"><input type="hidden" id="currentpage" value="1">';
        }else{
            // echo "rows2 =>".$rows;
            echo'<input type="hidden" id="totalpages" value="1"><input type="hidden" id="currentpage" value="1">';
        }
	}else{
		echo mysqli_error($conn);
	}
	// data fetching
	$query = mysqli_query($conn, "SELECT t1.* FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $brand $category $prf GROUP BY groupid ORDER BY price ASC LIMIT 12");
	if($query){
        if(mysqli_num_rows($query) > 0){
			while($products = mysqli_fetch_assoc($query)){
				$sku = $products['sku'];
				$imgsql = mysqli_query($conn, "SELECT image FROM product_images WHERE product='$sku' ORDER BY pi_id ASC LIMIT 2");
				if($imgsql){
					if(mysqli_num_rows($imgsql) > 0){
						$imgarr = [];
						while($imgrow = mysqli_fetch_assoc($imgsql)){
							$imgarr[] = $imgrow['image'];
						}
					}
				}

			$product = $product.'
				<div class="col-xl-4 col-sm-6 col-12">
				<div class="categoryOuter">
					<a href="single-product?p='.$products['sku'].'">
						<img src="'PRODUCT_IMAGE_URL.$imgarr[0].'" class="imgWithOutHover" class="img-fluid" alt="">
						<img src="'PRODUCT_IMAGE_URL.$imgarr[1].'" class="imgWithHover" class="img-fluid" alt="">
					</a>
					<div class="categoryButton">
					<div class="categoryContent">
						<h6>'.getSinglevalue('brands', 'brand_key', $products['brand'], 3).'</h6>
						<p>'.$products['product_name'].'</p>
						<p><i class="fa fa-inr"></i> '.$products['price'].'.<span>00</span></p>
					</div>
					</div>
				</div>
			</div>
				';
			}
		}else{
            $error = '<p>Products are not available</p>';
        }
	}else{
		echo mysqli_error($conn);
	}
}else{
    header('location: index');
}

?>
<input type="hidden" id="filter" value="0">
<section class="category py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 categoryHeaderDiv" style="background: url(admin/website_images/<?php echo getSinglevalue('brand_profile', 'brand_key', $bd, 4) ?>);">
				<div class="row">
					<div class="col-lg-6">
						<div class="categoryName">
							<h5><?php echo $urlarray[0] ?></h5>
							<div class="bp-content"><?php echo getSinglevalue('brand_profile', 'brand_key', $bd, 3) ?></div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="filterMainDiv">
					<p><span class="pd-count"><?php echo $prdrows; ?></span> Results</p>
					<div class="accordion">
				<!-- category starts from here -->
				<?php if($bd!=""){ ?>
				<input type="checkbox" name="brand[]" value="<?php echo $bd ?>" checked style="visibility: hidden;">	
                <?php $query = mysqli_query($conn, "SELECT DISTINCT(a.category) as cat, (SELECT COUNT(*) FROM product WHERE category=a.category) as rc FROM `product` as a WHERE publish=1 AND brand='$bd'");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Category</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<!-- <form> -->
									<div class="scrolly">
									<?php  while($rows = mysqli_fetch_assoc($query)){ 
									$cat = $rows['cat'];
										$subsql = mysqli_query($conn, "SELECT count(p_id) FROM product where publish=1 AND category='$cat' AND brand='$bd' GROUP BY groupid");
										$rc = mysqli_num_rows($subsql);
										?>

										<div class="checkboxGroup my-lg-2">
											<!-- <input type="checkbox" aria-label="Checkbox for following text input" name="category[]" value="<?php // echo $rows['cat'] ?>"> -->
											<p><a href="brand?brand=<?php echo $urlarray[0] ?>_<?php echo $urlarray[1] ?>_<?php echo $rows['cat'] ?>"><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?> <span>(<?php echo $rc ?>)</span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
                                    <?php } ?>
									<?php }
                                    }
                                }
                            ?>	
									</div>
								<!-- </form> -->
							</div>
						</div>
						<!-- Category Ends here  -->

						<!-- color starts from here -->
						<?php 
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.color!=''");
                            }else{
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.color) as color FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.brand='$bd' AND t1.color!=''");
                            }

                            if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Color</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="color[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '')" value="<?php echo $rows['color'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['color'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>
									</div>
								</form>
							</div>
						</div>
									<?php } } ?>
						<!-- color ends here -->

					   <!-- Size Starts From Here -->
						<?php 
                            $pass = 0;
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.sku WHERE t2.publish=1 AND t2.category='$urlarray[2]' AND t2.brand='$bd' AND t1.size!=''");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t1.size) as size FROM product_attributes as t1 JOIN product as t2 on t1.p_id=t2.p_key WHERE t2.publish=1 AND t2.brand='$bd' AND t1.size!=''");
                            }
                            	if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Size</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="size[]" onchange="applyFilter('<?php echo $urlarray[2] ?>', '')" value="<?php echo $rows['size'] ?>">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['size'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } } ?>
					<!-- size end here -->

					<!-- price range starts from here -->
					<?php
                            if($urlarray[2]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd' AND category='$urlarray[2]'");
                            }elseif($urlarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND brand='$bd'");
                            }
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
					<div class="accordion-toggle">
							<h4 class="mb-0">Rate</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
	<div id="rangeBox">    
         <div id="sliderBox" class="rangeslider">
             <input type="range" id="slider0to50" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '')">
             <input type="range" id="slider51to100" min="0" max="<?php echo $priceRows['maxprice'] ?>" onchange="applyFilter('<?php echo $urlarray[2] ?>', '', 'slider0to50', 'slider51to100')">
         </div>
         <div id="inputRange" class="priceRangeslider">
             <input type="number" min="0" max="<?php echo $priceRows['minprice'] / 2 ?>" value="<?php echo $priceRows['minprice'] ?>" id="min" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '')">
             <input type="number" min="0" max="<?php echo $priceRows['maxprice'] ?>" value="<?php echo $priceRows['maxprice'] ?>" id="max" onkeyup="applyFilter('<?php echo $urlarray[2] ?>', '')" onchange="applyFilter('<?php echo $urlarray[2] ?>', '')">
         </div>
	</div>
	<!-- new range silder starts -->
	 <!--<div data-role="main" class="ui-content">-->
  <!--  <form method="post" action="/action_page_post.php">-->
  <!--    <div data-role="rangeslider">-->
  <!--      <label for="price-min">Price:</label>-->
  <!--      <input type="range" name="price-min" id="price-min" value="200" min="0" max="1000">-->
  <!--      <label for="price-max">Price:</label>-->
  <!--      <input type="range" name="price-max" id="price-max" value="800" min="0" max="1000">-->
  <!--    </div>-->
  <!--      <input type="submit" data-inline="true" value="Submit">-->
  <!--      <p>The range slider can be useful for allowing users to select a specific price range when browsing products.</p>-->
  <!--    </form>-->
  <!--</div>-->
  <!-- new range slider ends -->

									</div>
									<?php } }else{
										echo mysqli_error($conn);
									} ?>
					<!-- Price range ends here -->

					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="row justify-content-end">
					<div class="col-12 text-right my-4">
						<div class="categoryDropdown">
							<form>
							 <select class="searchInput" id="sorting" onchange="applyFilter('<?php echo $urlarray[2] ?>', '')">
							    <option selected value="0">Price Low to High</option>
							    <option value="1">Price High to Low</option>
							  </select>
							</form>
						</div>
					</div>
					<div class="col-12">
						<div class="row" id="grid">
						<?php 
                        if(!empty($product)){ 
                            echo $product;
                        }else{
                            echo $error;
                        }
                        ?>

							
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>
<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script> -->

<?php include_once"helpers/footer.php"; ?>