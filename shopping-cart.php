    <?php 
    include_once "helpers/index.php";
    ?>
    <section class="shopping-cart mt-1 mt-md-3 mt-lg-5 mb-5">
        <div class="main-content-area">
            <div class="container">
                <div class="shipping-area">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row no-gutters align-items-center shopping-bag shopping-bag-sec">
                                <div class="col-md-8 col-10">
                                    <h2>Shopping bag</h2>
                                </div>
                                <div class="col-md-4 col-2 text-right">
                                    <p class="mb-0"><span>
                                        <?php echo $totalcartquantity; ?>
                                    </span> Item</p>
                                </div>
                            </div>
                            <div class="shipping-content">
                            <?php if(!empty($cart_data)){
                                for($i=0; $i <= count($cart_data) - 1; $i++){
                                ?>
                                <div class="row no-gutters pt-4 border-bottom">
                                    <div class="col-md-2 col-2">
                                        <div class="cart-image">
                                            <a href="single-product?p=<?php echo $cart_data[$i]['product'] ?>">
                                                <img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $cart_data[$i]['product'], 2))  ?>" alt="Cart">
                                            </a>
                                        </div>
                                    </div>

                                <div class="col-md-6 px-4 col-8 mb-4 my-sm-0">
                                    <div id="shipping" class="floatLeft">
                                        <a href="single-product?p=<?php echo $cart_data[$i]['product'] ?>"><h4><strong><?php echo getSinglevalue('brands', 'brand_key', getSinglevalue('product', 'sku', $cart_data[$i]['product'], 3), 3) ?></strong></h4></a>
                                        <p><?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 4) ?></p>
                                        <h5><strong><i class="fa fa-inr"></i> <span><?php echo getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10) ?>.00</span></strong></h5>
                                        <form action="#">
                                            <p class="mb">Quantity</p>
                                            <div class="form-field-area">
                                                <p>
                                                    <select onchange="changeCartQty('<?php echo $cart_data[$i]['cartid'] ?>', 'cartqty_<?php echo $i ?>', '<?php echo $cart_data[$i]['product'] ?>', '<?php echo $cart_data[$i]['qty'] ?>')" id="cartqty_<?php echo $i ?>">
                                                        <?php for($j=1; $j <= 10; $j++){ ?>
                                                        <option value="<?php echo $j ?>" <?php echo ($cart_data[$i]['qty'] == $j) ? 'selected="selected"' : ''; ?>><?php echo $j ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </p>
                                                <div class="cart_error cartqty_<?php echo $i ?>"></div>
                                            </div>
                                            <br>
                                        <div class="row mt-3">
                                        <?php $color = getSinglevalue('product_attributes', 'p_id', $cart_data[$i]['product'], 4); ?>
                                        <?php if(!empty($color)){ ?>
                                                <div class="col-md-7">
                                                    <p>Color: <span><?php echo $color ?></span></p>
                                                </div>
                                        <?php } ?>
                                        <?php if(!empty($cart_data[$i]['size'])){ ?>
                                                <div class="col-md-5">
                                                    <p class="m-0 mb-md-2">Size: <span><?php echo getSinglevalue('product_attributes', 'p_id', $cart_data[$i]['product'], 5) ?></span></p>
                                                </div>
                                        <?php } ?>
                                            </div>
                                        </form><br>
                                        <div class="favourite_cont d-block d-md-none">
                                        <a href="#"><img src="img/like.png" alt="Cart"> <span> Move to wish list</span></a>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-2 mb-4 my-sm-0">
                                <form class="sh-left-tab-cont" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                                <!-- <div > -->
                                    <div class="delete_cont">
                                        <input type="hidden" name="cartid" value="<?php echo $cart_data[$i]['cartid'] ?>">
                                        <button type="submit" name="remove_cart"><img src="img/delete.svg" alt="Cart"></button>
                                    </div>                                        
                                    <div class="favourite_cont ml-auto d-none d-md-block">
                                        <button type="submit" name="move_cart"><img src="img/like.png" alt="Cart"> <span> Move to wish list</span></button>

                                

                            </div>
                            
                                <!-- </div> -->
                                </form>
                                </div>
                            </div>
                            <?php  }
                            }else{
                                echo '<p class="mt-2">Your shopping bag is empty. Start shopping now or adding items to your Wish List to save them for later.</p>';
                            }?>
                        </div>

                            
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="order-summary">
                                <div class="row shopping-bag">
                                    <div class="col-md-12">
                                        <h2>Order summary</h2>
                                    </div>
                                </div>
                                <div class="order-cont">
                                    <?php if($totalcartquantity > 0){ ?>
                                    <p><strong>Free standard delivery will be applied to your order if you use code FREEDEL</strong></p>
                                    <p>My bag (<?php echo $totalcartquantity ?> item) <span class="floatRight"><i class="fa fa-inr"></i><?php echo $totalcartamount ?>.00</span></p>
                                    <a href="#" style="background: transparent; text-decoration: underline !important;">Add your promotion code</a>
                                    <p><strong>Total (excluding delivery)<span class="floatRight"><i class="fa fa-inr"></i><?php echo $totalcartamount ?>.00</span></strong></p>
                                    <button class="btn btn-lg btn-block p-0">
                                        <a href="shopping-information" class="mb-0 py-2 py-md-3 d-block">Checkout now</a>
                                    </button>
                                    <?php }
                                    // else{ ?>
                                        <!-- <button class="btn btn-lg btn-block">Checkout now</button> -->
                                    <button class="btn btn-lg btn-block p-0 button2">
                                    <a href="index" class="mb-0 py-2 py-md-3 d-block">Continue Shopping</a>
                                    </button>
                                    <?php // } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- shipping area-end -->
            </div>
        </div>
</section>
<div id="carti"></div>
    </section>

    
    <!-- <div id="carti"></div> -->
<?php 
    include_once "helpers/footer.php"; 

    flash_session_web();
    if(isset($_POST['remove_cart'])){
        extract($_POST);
        // print_r($cart_data);
        if(!empty($cartid)){
            if(empty($id) && empty($usertoken)){
            $new_cart = [];
            for($i=0; $i <= count($cart_data) - 1; $i++){
                $cartkey = $cart_data[$i]['cartid'];
                if($cartid != $cartkey){
                    $new_cart[] = $cart_data[$i];
                }
            }
            $item_data = "";
            if(!empty($new_cart)){
                $item_data = json_encode($new_cart);
                $cartadd = setcookie('shopping_cart', $item_data, time() + (172800 * 30)); // 86400 = 1 day
            }else{
                setcookie('shopping_cart', $item_data, time() - 3600);
            }
                $_SESSION['result'] = [true, 'Product Removed Successfully', 'success'];
                header('location: shopping-cart');
            }else{
                $sql = mysqli_query($conn, "DELETE FROM user_cart WHERE uc_key='$cartid'");
                if($sql){
                    $_SESSION['result'] = [true, 'Product Removed Successfully', 'success'];
                    header('location: shopping-cart');
                }
            }
        }
    }


    if(isset($_POST['move_cart'])){
        extract($_POST);
        if(!empty($cartid)){
            if(empty($id) && empty($usertoken)){
                $new_cart_array = [];
                for ($i=0; $i < count($cart_data); $i++) { 
                    if($cartid == $cart_data[$i]['cartid']){
                        $wish_cart = array(
                            'w_key' => $cart_data[$i]['cartid'],
                            'product' => $cart_data[$i]['product'],
                            'color' => $cart_data[$i]['color'],
                            'size' => $cart_data[$i]['size'],
                            'qty' => $cart_data[$i]['qty']
                        );
                    }else{
                        $new_cart_array[] = $cart_data[$i];
                    }
                }
                if(!empty($new_cart_array)){
                    $cart_item_data = json_encode($new_cart_array);
                    setcookie('shopping_cart', $cart_item_data, time() + (172800 * 30)); // 86400 = 1 day
                }else{
                    $item_data = "";
                    setcookie('shopping_cart', $item_data, time() - 3600);
                }

                $user_wish[] = $wish_cart;
                $item_data = json_encode($user_wish);
                $cartadd = setcookie('wishlist', $item_data, time() + (172800 * 30));
                $_SESSION['result'] = [true, 'Product Moved To Wishlist Successfully', 'success'];
                header('location: shopping-cart');
            }else{
                $sql = mysqli_query($conn, "SELECT uc_key, product, color, size, qty FROM user_cart WHERE uc_key='$cartid' AND user='$id'");
                if($sql){
                    if(mysqli_num_rows($sql) > 0){
                        $row = mysqli_fetch_assoc($sql);
                        $key = $row['uc_key'];
                        $product = $row['product'];
                        $color = $row['color'];
                        $size = $row['size'];
                        $qty = $row['qty'];
                        // save in cart
                        $query = mysqli_query($conn, "INSERT INTO wishlist (w_key, user, product, color, size, qty, created_at) VALUES ('$key', '$id', '$product', '$color', '$size', '$qty', '$created_at')");
                        if($query){
                            $delete = mysqli_query($conn, "DELETE FROM user_cart WHERE uc_key='$key'");
                            if($delete){
                                $_SESSION['result'] = [true, 'Product Moved To Wishlist Successfully', 'success'];
                                header('location: shopping-cart');
                            }
                        }
                    }
                }else{
                    echo mysqli_error($conn);
                }
            }
        }
    }




    ?>