<?php include_once"helpers/index.php";

if(isset($_COOKIE['recently_viewed'])){
				$view_data = stripcslashes($_COOKIE['recently_viewed']);
				$rcview_data = json_decode($view_data, true);
			}else{
				$rcview_data = [];
			}

 ?>
<style>
#ac-four {
    background: #fff;
    font-weight: bold;
}
</style>
<section class="mb-5">
	<div class="container-fluid px-0">
		<div class="accountMainDiv justify-content-center">
					<div class="heading w-100 text-center mt-4 mb-0">
						<h2>Wish List</h2>
					</div>
					<div class="row justify-content-center m-0 w-100">
						<div class="col-lg-10">
								<div class="addressBookContent text-center">
									<p><strong><?php echo $totalwishquantity ?> item</strong></p>
									<p>Simply click the <img src="img/heartPlus.png" width="20px"> icon that sits next to our products to save the things you love for later.</p>
									<div class="row justify-content-center">
										<div class="col-md-3 col-sm-5 col-8">
										<?php if($totalwishquantity == 0){ ?>
										<a class="continueShopping" href="index">Continue Shopping</a>
										<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>

		</div>
	</div>
	<div class="container my-5">
		<div class="row">
			<?php 
			// print_r($user_wish);
				for ($i=0; $i < count($user_wish) ; $i++) { 
			?>
			<div class="col-lg-4 col-md-6 mb-4">
			<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
				<div class="wishlistmain">
					<input type="hidden" name="wishlistid" value="<?php echo $user_wish[$i]['w_key'] ?>">
					<button type="submit" name="remove_wishlist"><span class="wishlistclose">&times;</span></button>
					<div class="wishlistImg">
						<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $user_wish[$i]['product'], 2)); ?>">
					</div>
					<div class="wishlistCont">
						<h6><?php echo getSinglevalue('brands', 'brand_key', getSinglevalue('product', 'sku', $user_wish[$i]['product'], 3), 3) ?></h6>
						<P class="mb-0"><?php echo getSinglevalue('product', 'sku', $user_wish[$i]['product'], 4) ?></P>
						<?php if(!empty($user_wish[$i]['color'])){ ?>
						<p class="mb-0">Color: <?php echo $user_wish[$i]['color'] ?></p>
						<?php }
							if(!empty($user_wish[$i]['size'])){
						?>
						<p class="mb-0">Size: <?php echo $user_wish[$i]['size'] ?></p>
						<?php }
							if(!empty($user_wish[$i]['qty'])){
						?>
						<p class="mb-0">Quantity: <?php echo $user_wish[$i]['qty'] ?></p>
						<?php } ?>
						<p class="mb-0"><i class="fa fa-inr"></i> <?php echo getSinglevalue('product', 'sku', $user_wish[$i]['product'], 10) ?>.00</p>
					</div>
					<?php if(empty($user_wish[$i]['qty']) && empty($user_wish[$i]['size']) && empty($user_wish[$i]['color'])){ ?>
					<button type="button" class="viewDetail">
					<a href="single-product?p=<?php echo $user_wish[$i]['product'] ?>">View Details</a>
					</button>
					<?php }else{ ?>
					<button type="submit" class="viewDetailWishlist" name="move_wishlist">
						Move To Bag
					</button>
					<?php } ?>
					
				</div>
				</form>
			</div>
				<?php } ?>
		</div>
		


<?php 
$rc_cnt = count($rcview_data);

if($rc_cnt > 0){
	// echo $rc_cnt;
?>
<hr>
		<div class="row">
			<div class="heading w-100 text-center mt-4">
				<h2>Recent View</h2>
			</div>
			<?php
								foreach($rcview_data as $rcv => $value){
									// echo $value;
									$sql = mysqli_query($conn, "SELECT * FROM product WHERE sku='$value'");
									if($sql){
										if(mysqli_num_rows($sql) > 0){
											$rcviewarray = mysqli_fetch_array($sql);
											// print_r($rcviewarray);
											?>
			<div class="col-lg-4 col-md-6 mb-4">
				<div class="wishlistmain">
					<!-- <span class="wishlistclose">&times;</span> -->
					<form class="wishlistclose" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
									<input type="hidden" name="rc_product" value="<?php echo $value ?>">
									<input type="submit" name="remove_rcview" class="close cursor remove_sec" value="x">
								</form>
					<div class="wishlistImg">
						<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $value, 2)) ?>">
					</div>
					<div class="wishlistCont">
						<h6><?php echo getSinglevalue('brands', 'brand_key', $rcviewarray[3], 3) ?></h6>
						<P class="mb-2"><?php echo $rcviewarray[4] ?></P>
						<p class="mb-0"><i class="fa fa-inr"></i> <?php echo $rcviewarray[10] ?>.00</p>
					</div>
					<button class="viewDetail"><a href="single-product?p=<?php echo $rcviewarray[5] ?>">View Details</a></button>
				</div>

			</div>
			<?php		}
									}
								}
							?>
		</div>

<?php } ?>


	</div>
</section>

<?php include_once "helpers/footer.php";

flash_session_web();

if(isset($_POST['remove_wishlist'])){
    extract($_POST);
    if(!empty($wishlistid)){
        if(empty($id) && empty($usertoken)){
        $new_wish = [];
        for($i=0; $i <= count($user_wish) - 1; $i++){
            $wishkey = $user_wish[$i]['w_key'];
            if($wishlistid != $wishkey){
                $new_wish[] = $user_wish[$i];
            }
        }
        // print_r($new_cart);
        if(!empty($new_wish)){
            $item_data = json_encode($new_wish);
            $cartadd = setcookie('wishlist', $item_data, time() + (172800 * 30)); // 86400 = 1 day
        }else{
			$item_data = "";
            setcookie('wishlist', $item_data, time() - 3600);
		}
		$_SESSION['result'] = [true, 'Product Removed Successfully', 'success'];
        header('location: wishlist');
        }else{
            $sql = mysqli_query($conn, "DELETE FROM wishlist WHERE w_key='$wishlistid'");
            if($sql){
                $_SESSION['result'] = [true, 'Product Removed Successfully', 'success'];
        		header('location: wishlist');
            }
        }
    }
}

if(isset($_POST['move_wishlist'])){
	extract($_POST);
	if(!empty($wishlistid)){
        if(empty($id) && empty($usertoken)){
			$new_wish_array = [];
			for ($i=0; $i < count($user_wish); $i++) { 
				if($wishlistid == $user_wish[$i]['w_key']){
					$wish_cart = array(
						'cartid' => $user_wish[$i]['w_key'],
						'product' => $user_wish[$i]['product'],
						'color' => $user_wish[$i]['color'],
						'size' => $user_wish[$i]['size'],
						'quantity' => $user_wish[$i]['qty']
					);
				}else{
					$new_wish_array[] = $user_wish[$i];
				}
			}
			$wish_item_data = json_encode($new_wish_array);
			$wishadd = setcookie('wishlist', $wish_item_data, time() + (172800 * 30)); // 86400 = 1 day
			$cart_data[] = $wish_cart;
			$item_data = json_encode($cart_data);
			$cartadd = setcookie('shopping_cart', $item_data, time() + (172800 * 30));
			$_SESSION['result'] = [true, 'Product Moved To Cart Successfully', 'success'];
            header('location: wishlist');
		}else{
			$sql = mysqli_query($conn, "SELECT w_key, product, color, size, qty FROM wishlist WHERE w_key='$wishlistid' AND user='$id'");
			if($sql){
				if(mysqli_num_rows($sql) > 0){
					$row = mysqli_fetch_assoc($sql);
					$key = $row['w_key'];
					$product = $row['product'];
					$color = $row['color'];
					$size = $row['size'];
					$qty = $row['qty'];
					// save in cart
					$query = mysqli_query($conn, "INSERT INTO user_cart (uc_key, user, product, color, size, qty, created_at) VALUES ('$key', '$id', '$product', '$color', '$size', '$qty', '$created_at')");
					if($query){
						$delete = mysqli_query($conn, "DELETE FROM wishlist WHERE w_key='$key'");
						if($delete){
							$_SESSION['result'] = [true, 'Product Moved To Cart Successfully', 'success'];
            				header('location: wishlist');
						}
					}
				}
			}
		}
	}
}


// remove wishlist
if(isset($_POST['remove_rcview'])){
	extract($_POST);
	$removed_array = array_diff($rcview_data, array($rc_product));
	// echo $removed_array;
	if(!empty($removed_array)){
		$rcitems = json_encode($removed_array);
		setcookie('recently_viewed', $rcitems, time() + (172800 * 30));
	}else{
		$data = "";
		setcookie('recently_viewed', $data, time() - 3600);
	}
	$_SESSION['result'] = [true, 'Product Removed Successfully', 'success'];
    header('location: wishlist');
}

?>