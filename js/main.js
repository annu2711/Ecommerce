//    url

var url = 'http://localhost/lsmart/ecommerce/';

var loader = '<div class="loaderOuter"><div class="categoryLoader"></div></div>';
   
$(document).ready(function(){

	/*carousel*/
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText:['<img src="img/arrow/leftArrow.svg">','<img src="img/arrow/rightArrow.svg">'],
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        991:{
            items:3
        },
        1199:{
            items:4
        }

    }
})

    $('.menuBar').click(function(){
    $('.mobileMenu').addClass('active');
    $('body').addClass('active');
    });
		
		$('.sidebarContent>ul>li').click(function(){
			$(this).children('div.subMenu').not('a').toggleClass('active')
		})
		$('.subMenuInn>ul>li').click(function(){
			$('.mobileMenu').removeClass('active');
            $('.subMenu').removeClass('active');
            $('body').removeClass('active');
		})


		$('body,html').click(function (e) {
			// console.log('e.target', (e.target).length);
		    var container = $(".mobileMenu");
            var innercontainer = $(".subMenu");
            var body = $("body");
		    if (!container.is(e.target) && container.has(e.target).length === 0) {
		        container.removeClass('active');
                innercontainer.removeClass('active');
                body.removeClass('active');
		    }
		});

});
// $('.menuClick').click(function(){
// 		$('.mobileMenu').addClass('active');
// 		});
// winow width
if($(window).width() <= 767){
    $("header .custom-menu-primary").slideUp().removeClass('menu_open');  
 }


// header fix
$(window).scroll(function(){
    var headerFix = $(this).scrollTop()
    if($(window).width() > 991){
        if(headerFix > 0){
            $('.mobileMenu').addClass('active')
        }else{
            $('.mobileMenu').removeClass('active');
        }
    } else{
        if(headerFix > 0){
            $('.mobileMenu').addClass('fixHeader')
        }else{
            $('.mobileMenu').removeClass('fixHeader');
        }
    }
});

// brand Directory

// $(window).scroll(function(){
//     var brandDirectory = $(this).scrollTop()
//     if(brandDirectory > 0){
//         $('.brandDirectory').addClass('active')
//     }else{
//         $('.brandDirectory').removeClass('active')
//     }
// });

// AD Js

function recentUrl(){
    var url = window.location.href; 
    localStorage.setItem("recentUrl", url);
}

// Selectbox dropdown

    var stateOption = '<option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Lakshadweep">Lakshadweep</option><option value="Puducherry">Puducherry</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option>';
       $('.state').html(stateOption);
   
   
      $( '#ship-box' ).on('click', function() {
           $( '#ship-box-info' ).slideToggle(1000);
           // if()
           var checkStatus = $('#ship-box').is(':checked');
           if(checkStatus){
               $('#ship_add').val("1");
               $('#ship-box-info').html('<div class="col-md-12"><div class="checkout-form-list"><label>Name <span class="required">*</span></label> <input type="text" name="ship_name" placeholder=""/></div></div><div class="col-md-12"><div class="checkout-form-list"><label>Phone <span class="required">*</span></label> <input type="text" name="ship_mobile" placeholder="" required/></div></div><div class="col-md-12"><div class="checkout-form-list"><label>Email Address <span class="required">*</span></label> <input type="email" name="ship_email" placeholder="" required/></div></div><div class="col-md-12"><div class="checkout-form-list"><label>Address <span class="required">*</span></label><textarea name="ship_address" required></textarea></div></div><div class="col-md-12"><div class="checkout-form-list"><label>Town / City <span class="required">*</span></label><input type="text" name="ship_city" placeholder="Town / City"/></div></div><div class="col-md-6"><div class="country-select"><label>State<span class="required">*</span></label> <select name="ship_state" class="state" required>'+ stateOption +'</select></div></div><div class="col-md-6"><div class="checkout-form-list"><label>Zip <span class="required">*</span></label> <input type="text" name="ship_pincode" placeholder="Postcode / Zip"/></div></div>');
           }else{
               $('#ship_add').val("0");
               $('#ship-box-info').html("");
           }
        });
   
   

   // product Quick B=View
   
   function productQuickView(x){
       if(x != ''){
           $.ajax({
               type: 'post',
               url: 'admin/helpers/event.php',
               data: {product: x},
               dataType: 'json',
               success: function(data){
                   console.log("cart-data=>", data);
                   // alert(data[0]);
                   var quote = "'";
                   var colorOpt = "";
                   var colorv = "";
                   var sizev = "";
                   if(data.colors != ""){
                       // var color = data[18].split(",");
                       var colors = "";
                       for(i=0; i <= data.colors.length - 1; i++){
                           if(data.colors[i] != "" || data.colors[i] != undefined){
                               colors = colors + '<option value="' + data.colors[i] + '">' + data.colors[i] + '</option>';
                           }
                       }
                       colorv = 1;
                       colorOpt = '<select id="itemcolor" name="color" onclick="changeCartColor(' + quote + data.product[0] + quote +')">' + colors + '</select>';
                   }else{
                       colorOpt ="";
                       colorv = 0;
                   }
   
                   var sizeOpt = "";
                   if(data.size != ""){
                       // var size = data[19].split(",");
                       var sizes = "";
                       for(i=0; i <= data.size.length - 1; i++){
                           if(data.size[i] != ""){
                               sizes = sizes + '<option value="' + data.size[i] + '">' + data.size[i] + '</option>';
                           }
                       }
                       sizev = 1;
                       sizeOpt = '<select id="itemsize" name="size" onclick="changeCartSize()">' + sizes + '</select>';
                   }else{
                       sizeOpt = "";
                       sizev = 0;
   
                   }
                   
                   var url = 'https://lsmart.in/';
                   $('#modal-product').html("");

                   var content = '<input type="hidden" name="product" value="'+ data.product[0] +'"><div class="row"> <div class="col-md-6"> <div class="row"><div class="col-md-12"><div class="popBigImg"><img src="products/' + data.product[3] +'" class="img-fluid newImg" alt=""></div></div></div><div class="row justify-content-center mt-3"><div class="popup col-md-9"><div class="popup owl-carousel owl-theme"> <div class="item"> <div class="popupCont"> <a href="javascript:void(0)" data-img="img/img1.jpg"> <img src="img/img1.jpg" alt="" class="carousel-item"> </a> </div></div><div class="item"> <div class="popupCont"> <a href="javascript:void(0)" data-img="img/suit.jpg"> <img src="img/suit.jpg" alt="" class="carousel-item"> </a> </div></div><div class="item"> <div class="popupCont"> <a href="javascript:void(0)" data-img="img/purse.jpg"> <img src="img/purse.jpg" alt="" class="carousel-item"> </a> </div></div><div class="item"> <div class="popupCont"> <a href="javascript:void(0)" data-img="img/img15.jpg"> <img src="img/img15.jpg" alt="" class="carousel-item"> </a> </div></div></div></div></div></div><div class="col-md-6"> <div class="popupContent p-4 pr-5"> <div class="categoryContent mt-5"><h4>' + data.product[4] + '</h4><p>' + data.product[1] + '</p><p><i class="fa fa-inr"></i> ' + data.product[2] + '.<span>00</span></p></div><div class="mt-5"><div class="inputSearch mb-5">' + colorOpt +'</div><div class="inputSearch mb-5">' + sizeOpt + '</div><div class="input-group numberBtn mb-5 w-50"> <span class="input-group-btn"> <button type="button" class="quantity-left-minus btn btn-number" onclick="minusQuantity()" data-type="minus"> <span class="fa fa-minus"></span> </button> </span> <input type="text" id="cartqty" readonly name="quantity" class="form-control editCont input-number" value="1" min="1" max="100"> <span class="input-group-btn"> <button type="button" class="quantity-right-plus btn btn-number" data-type="plus" onclick="addQuantity(' + quote + data.product[0] + quote +', '+ colorv +', ' + sizev + ')"> <span class="fa fa-plus"></span> </button> </span><div class="cart_error"></div></div></div><div class="row"><div class="col-md-9"><div class="addToBag w-100"><p class="mb-0"><input type="submit" name="addcart" class="button d-block" value="Add to bag" style="width: 100%;border: none;"></p></div></div><div class="col-md-3 "><button type="submit" name="wishlist" class="popupLikeBtn ml-auto"><img src="img/like.png"></button></div></div><div class="productDetailPage mt-5"><a href="single-product?p=' + data.product[0] +'">VIEW FULL PRODUCT DETAILS <span><img src="img/arrow/rightArrow.svg" width="15px"></span></a></div></div></div></div>';
                   $('#modal-product').show();
                   $('#modal-product').html(content);
               }
           })
       }
   }
   // recent url
   recentUrl();
   // handleCart();
   
   // add products on scroll
   
  var filterpagination = true;
  
  $(document).ready(function(){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 300){
            var params = new window.URLSearchParams(window.location.search);
            var cat = params.get('cat');
            var brand = params.get('brand');
            var hasFilter = $('#filter').val();
            var page = parseInt($('#currentpage').val());
            var totalpages = $('#totalpages').val();
            if(params.has('cat')){
            if(hasFilter > 0){
                if(page < totalpages){
                var catarray = cat.split("_");
                var catSubcat = catarray[0].split("-");
                if(filterpagination){
                    console.log("filtervar", filterpagination);
                applyFilter(catSubcat[0], catSubcat[1], catarray[2], page);
                }
                }
            }else{
                if(page < totalpages){
                getMoredata(cat, page);
                }
            }
            }

            if(params.has('brand')){
                if(hasFilter > 0){
                    if(page < totalpages){
                        var brandarray = brand.split("_");
                        // var catSubcat = catarray[0].split("-");
                        var subcategory = "";
                        // alert(brandarray[1]);
                        if(filterpagination){
                        applyFilter(brandarray[2], subcategory, brandarray[1], page);
                        }
                    }
                }else{
                    if(page < totalpages){
                        getMoreBranddata(brand, page);
                    }
                }
            }

        }
    });
});
   
   // Apply filter

//    $("#max").on('change', function(){
//        alert("Hii");
//    })


// function getAjax(data){
//     console.log("ajax");
//     var resc = "";    
//      $.ajax({
//            type: 'post',
//            url: 'admin/helpers/event.php',
//            async: false,
//            data,
//            dataType: 'json',
//             success: function(res){
//               console.log("hii", res);
//               resc = res;
//           }
//        });
//        return resc;
// }

//  async function applyFilter(category, subcategory="", gender="",  page=""){
//     //   setTimeout(function(){ 
//     filterpagination = false;
//     if(page != ''){
//         // alert(page);
//         $('#currentpage').val(page + 1);
//         // alert($('#currentpage').val());
//     }else{
//         $('#currentpage').val('1');
//     }
//     $('#loaderOuter').show();
//     //   alert('Hii');
//        var brand = [];
//        $("input[name='brand[]']:checked").each(function (e) {
//            brand[e] = $(this).val();
//        });
//        var color = [];
//        $("input[name='color[]']:checked").each(function (e) {
//                color[e] = $(this).val();
//        });
//        var size = [];
//        $("input[name='size[]']:checked").each(function (e) {
//            size[e] = $(this).val();
//        });
//        var storage = [];
//        $("input[name='storage[]']:checked").each(function (e) {
//            storage[e] = $(this).val();
//        });
//        var priceFrom = $('#min').val();
//        var priceTo = $('#max').val();
//        var sort = $('#sorting').val();
//        console.log("brand =>", brand);
//        console.log("color =>", color);
//        console.log("size =>", size);
//        console.log("min =>", priceFrom);
//        console.log("max =>", priceTo);
//        console.log("cat =>", category);
//        console.log("subcat =>", subcategory);
//        console.log("gender =>", gender);
//        console.log("storage =>", storage);
//        console.log("page =>", page);
//         var data = {filter: 1, brand:brand, color:color, size:size, min:priceFrom, max:priceTo, cat:category, subcat:subcategory, page:page, gender:gender, storage:storage, sort:sort};
//         console.log("before", data);
//         const res = await getAjax(data);
//         console.log("after", res);
//          if(brand != ""){
//                    $('#brand-length').text(" ");
//                    $('#brand-length').text(brand.length + ' Selected');
//                }else{
//                 $('#brand-length').text(" ");
//                 $('#brand-length').text('All');
//                }
//                if(color != ""){
//                 $('#color-length').text(" ");
//                 $('#color-length').text(color.length + ' Selected');
//                 }else{
//                     $('#color-length').text(" ");
//                     $('#color-length').text('All');
//                 }
//                 if(size != ""){
//                     $('#size-length').text(" ");
//                     $('#size-length').text(size.length + ' Selected');
//                     }else{
//                         $('#size-length').text(" ");
//                         $('#size-length').text('All');
//                     }
//                $('#loaderOuter').hide();
//                $('#filter').val("1");
//         if(res.productdata.length > 0){
//             filterpagination = true;
//             $('#loaderOuter').hide();
//             //   $('#filter').val("1");
//                 if(res.productdata.length > 0){
//                var totalpages = Math.ceil(res.productcount / 12);
//                $('.pd-count').text(" ");
//                $('.pd-count').text(res.productcount);
//                $('#totalpages').val(totalpages);
//                var	gridContent = "";
//                for ( i=0; i<res.productdata.length; i++ )
//                    {
//                    var quote = "'";
//                     let img2 = ((res.productdata[i][6] != '' | res.productdata[i][6] != 'null' || res.productdata[i][6] != 'undefined') ? res.productdata[i][5] : res.productdata[i][6]) ;
//                     console.log('img2=>', img2);
//                    gridContent = gridContent + '<div class="col-xl-4 col-sm-6 col-12"><div class="categoryOuter"><div class="categoryOuterImg"><a href="single-product?p=' + res.productdata[i][1] +'"><img src="products/' + res.productdata[i][5] +'" class="imgWithOutHover" class="img-fluid" alt=""><img src="products/' + img2 +'" class="imgWithHover" class="img-fluid" alt=""></a></div><div class="categoryButton"><div class="categoryContent"><h6>' + res.productdata[i][7] + '</h6><p>' + res.productdata[i][2] +'</p><p><i class="fa fa-inr"></i> ' + Number(res.productdata[i][4]).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,') + '</p></div></div></div></div>';
//                    }
//                    if(page != ""){
//                     $('#grid').append(gridContent);
//                    }else{
//                    $('#grid').html("");
//                    $('#grid').html(gridContent);
//                 }

//                }else{
//                    var content = '<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>';
//                    $('#grid').html("");
//                    $('#grid').html(content);
//                }
//         }else{
//                    var content = '<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>';
//                    $('#grid').html("");
//                    $('#grid').html(content);
//                }
//         console.log("filterpage", filterpagination);
//   }

function isUrlExists(nurl){
    console.log("nurl=>", nurl);
    $.ajax(  
        {  
            url: nurl,  
            type:'HEAD',
            success: function(data)  
            {  
                return true;  
            },  
            error: function(data)  
            {  
                return false; 
            }  
        });
}

//   setTimeout(function(){ 
function applyFilter(category, subcategory="", gender="",  page=""){
    if(page != ''){
        // alert(page);
        $('#currentpage').val(page + 1);
        // alert($('#currentpage').val());
    }else{
        $('#currentpage').val('1');
    }
    // $('#loaderOuter').show();
    //   alert('Hii');
       var brand = [];
       $("input[name='brand[]']:checked").each(function (e) {
           brand[e] = $(this).val();
       });
       var color = [];
       $("input[name='color[]']:checked").each(function (e) {
               color[e] = $(this).val();
       });
       var size = [];
       $("input[name='size[]']:checked").each(function (e) {
           size[e] = $(this).val();
       });
       var storage = [];
       $("input[name='storage[]']:checked").each(function (e) {
           storage[e] = $(this).val();
       });

       var priceFrom = $('#min').val();
       var priceTo = $('#max').val();
       var sort = $('#sorting').val();
    //    console.log("brand =>", brand);
    //    console.log("color =>", color);
    //    console.log("size =>", size);
    //    console.log("min =>", priceFrom);
    //    console.log("max =>", priceTo);
    //    console.log("cat =>", category);
    //    console.log("subcat =>", subcategory);
    //    console.log("gender =>", gender);
    //    console.log("storage =>", storage);
    //    console.log("page =>", page);
       $.ajax({
           type: 'post',
           url: 'lsmartbk/helpers/event.php',
           data: {filter: 1, brand:brand, color:color, size:size, min:priceFrom, max:priceTo, cat:category, subcat:subcategory, page:page, gender:gender, storage:storage, sort:sort},
           dataType: 'json',
           success: function(data){
               console.log(data);
            //    $('#loaderOuter').hide();
            // //   resolve(data);
            //    if(brand != ""){
            //        $('#brand-length').text(" ");
            //        $('#brand-length').text(brand.length + ' Selected');
            //    }else{
            //     $('#brand-length').text(" ");
            //     $('#brand-length').text('All');
            //    }
            //    if(color != ""){
            //     $('#color-length').text(" ");
            //     $('#color-length').text(color.length + ' Selected');
            //     }else{
            //         $('#color-length').text(" ");
            //         $('#color-length').text('All');
            //     }
            //     if(size != ""){
            //         $('#size-length').text(" ");
            //         $('#size-length').text(size.length + ' Selected');
            //         }else{
            //             $('#size-length').text(" ");
            //             $('#size-length').text('All');
            //         }
               
            //    $('#filter').val("1");
            //    console.log("filter data", data);
            //    var arrayCount = data.productdata.length;
            //    if(data.productdata.length > 0){
            //    var totalpages = Math.ceil(data.productcount / 12);
            //    $('.pd-count').text(" ");
            //    $('.pd-count').text(data.productcount);
            //    $('#totalpages').val(totalpages);
            //     // if(page != ''){
            //     //     $('#currentpage').val(page);
            //     // }else{
            //     //     $('#currentpage').val('1');
            //     // }
            //    var	gridContent = "";
            //    for ( i=0; i<data.productdata.length; i++ )
            //        {
            //        var quote = "'";
            //         let img2 = ((data.productdata[i][6] != '' | data.productdata[i][6] != 'null' || data.productdata[i][6] != 'undefined') ? data.productdata[i][5] : data.productdata[i][6]) ;
            //         console.log('img2=>', img2);
            //        gridContent = gridContent + '<div class="col-xl-4 col-6"><div class="categoryOuter"><div class="categoryOuterImg"><a href="single-product?p=' + data.productdata[i][1] +'"><img src="products/' + data.productdata[i][5] +'" class="imgWithOutHover" class="img-fluid" alt=""><img src="products/' + img2 +'" class="imgWithHover" class="img-fluid" alt=""></a></div><div class="categoryButton"><div class="categoryContent"><h6>' + data.productdata[i][7] + '</h6><p>' + data.productdata[i][2] +'</p><p><i class="fa fa-inr"></i> ' + Number(data.productdata[i][4]).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,') + '</p></div></div></div></div>';
            //        }
            //        if(page != ""){
            //         $('#grid').append(gridContent);
            //        }else{
            //        $('#grid').html("");
            //        $('#grid').html(gridContent);
            //     }

            //    }else{
            //        var content = '<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>';
            //        $('#grid').html("");
            //        $('#grid').html(content);
            //    }
           }
       });
   }



   // scroll data
    function getMoredata(con, page){
       if(con != "" && page != ""){
        $('#loaderOuterProduct').show();
        $('#currentpage').val(page + 1);
        console.log('pagination=>', page);
           $.ajax({
               type: 'post',
               url: 'admin/helpers/event.php',
               data: {con: con, page: page},
               dataType: 'json',
               success: function(data){
                $('#loaderOuterProduct').hide();
                   console.log("page Scroll =>", data);
                   var arrayCount = data.length;
               if(data.length > 0){
               var	gridContent = "";
               var listContent = "";
               for ( i=0; i<data.length; i++ )
                   {
                    
                    let image = $('<img src="https://bquestindia.com/products/' + data[i][5] + '" />');
                    let fImg = 'no-product.png';
                    if (image.attr('width') > 0){
                        fImg = data[i][5];
                    }


                   var quote = "'";
                   let img2 = ((data[i][6] == '' | data[i][6] == null || data[i][6] == 'undefined') ? fImg : data[i][6]) ;
                   gridContent = gridContent + '<div class="col-xl-4 col-sm-6 col-12"><div class="categoryOuter"><a href="single-product?p=' + data[i][1] +'"><img src="products/' + fImg +'" class="imgWithOutHover" class="img-fluid" alt=""><img src="products/' + img2 +'" class="imgWithHover" class="img-fluid" alt=""></a><div class="categoryButton"><div class="categoryContent"><h6>' + data[i][7] + '</h6><p>' + data[i][2] +'</p><p><i class="fa fa-inr"></i> ' + Number(data[i][4]).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,') +'</p></div></div></div></div>';
				}
                   }
                   $('#grid').append(gridContent);
                //   $('#currentpage').val(page);
                //   var records = $('#grid .col-xl-4').length;
                //   $('#onpage').text("");
                //   $('#onpage').text(records);
               }
            //    }
           })
       }
   }

   function getMoreBranddata(brandcon, page){
    //   alert('pagination calling');
    if(brandcon != "" && page != ""){
        $('#loaderOuterProduct').show();
        $('#currentpage').val(page + 1);
        var sort = $('#sorting').val();
        $.ajax({
            type: 'post',
            url: 'admin/helpers/event.php',
            data: {brandcon: brandcon, page: page, sort: sort},
            dataType: 'json',
            success: function(data){
                $('#loaderOuterProduct').hide();
                console.log("page Scroll =>", data);
                var arrayCount = data.data.length;
            if(data.data.length > 0){
            var	gridContent = "";
            var listContent = "";
            for ( i=0; i<data.data.length; i++ )
                {
                var quote = "'";
                let img2 = ((data.data[i][6] != '' | data.data[i][6] != 'null' || data.data[i][6] != 'undefined') ? data.data[i][5] : data.data[i][6]) ;

                gridContent = gridContent + '<div class="col-xl-4 col-sm-6 col-12"><div class="categoryOuter"><div class="categoryOuterImg"><a href="single-product?p=' + data.data[i][1] +'"><img src="products/' + data.data[i][5] +'" class="imgWithOutHover" class="img-fluid" alt=""><img src="products/' + img2 +'" class="imgWithHover" class="img-fluid" alt=""></a></div><div class="categoryButton"><div class="categoryContent"><h6>' + data.data[i][7] + '</h6><p>' + data.data[i][2] +'</p><p><i class="fa fa-inr"></i> ' + Number(data.data[i][4]).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,') +'</p></div></div></div></div>';
             }
                }
                $('#grid').append(gridContent);
                // $('#currentpage').val(page);
                // var records = $('#grid .col-xl-4').length;
                // $('#onpage').text("");
                // $('#onpage').text(records);
            }
         //    }
        })
    }
}
   
   
   // // cart
   // if(userLogin()){
   // var l = $('#uppercart li').length;
   // $('.total-cart-item').text("");
   // $('.total-cart-item').text(l);
   // }
   // console.log('l=>', l);
   // addToCart('abc');
   
   
   // cartqty
   
   function addQuantity(item, itemcolor, itemsize){
       $('.cart_error').html("");
       var lval = parseInt($('#cartqty').val());
       var color = "";
       var size = "";
       if(itemcolor > 0){
           color = $('#itemcolor').val();
       }
       if(itemsize > 0){
           size = $('#itemsize').val();
       }
       if(item != ''){
           $.ajax({
               type: 'post',
               url: 'admin/helpers/event.php',
               data: {cartquantity: item, itemcolor: color, itemsize: size},
               success: function(data){
                   var itemval = parseInt(data);
                   if(lval < data){
                       $('#cartqty').val(lval + 1);
                   }else{
                       $('.cart_error').html("");
                       $('.cart_error').html('<p class="text-danger">Only '+ data +' Items Available.</p>');
                   }
                   // console.log("check qty =>", data);
               }
           })
       }
       
       
   }
   
   function minusQuantity(){
       $('.cart_error').html("");
       var lval = parseInt($('#cartqty').val());
       if(lval > 1){
       $('#cartqty').val(lval - 1);
       }
   }
   

   function changeCartColor(item){
    //    alert("hii");
       $('.cart_error').html("");
       var colorval = $('#itemcolor').val();
       if(item != ""){
           $.ajax({
               type: 'post',
               url: 'admin/helpers/event.php',
               data: {productitem: item, colorval: colorval},
               dataType: 'json',
               success: function(data){
                   if(data.length > 0){
                    //    console.log("data for =>", data);
                       var sizes = "";
                       for(i=0; i <= data.length - 1; i++){
                           if(data[i] != ""){
                               sizes = sizes + '<option value="' + data[i] + '">' + data[i] + '</option>';
                           }
                       }
                       $('#itemsize').html("");
                       $('#itemsize').html(sizes);
                   }
                   $('#cartqty').val(1);
               }
           })
       }
   }
   
   function changeCartSize(){
       $('.cart_error').html("");
       $('#cartqty').val(1);
   }
   

   
$(document).ready(function(){
    $('.toggleDiv').hide();
    $('.formSect').hide();
    $('.onClickToggle').click(function(){
        $('.toggleDiv').slideToggle();
    })
    $('.openSignUp').click(function(){
        $('.formSect').slideDown();
        $('.openSignUp').hide();
    })
})



function password_check() {
    pass = document.getElementById("password").value;
    regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (regex.exec(pass) == null) {
    $('.error-password').show();
    }
    else {
    $('.error-password').hide();
    }
  }


function checkPassword(){
    var passs = document.getElementById("password").value;
    regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (regex.exec(passs) == null) {
        $('.error-password').show();
}else{
    var pass = $('#password').val();
    var cpass = $('#confirm-password').val();
    if(pass == cpass){
        console.log('confirm');
        $('.error-match-password').hide();
        $('#signup').removeAttr('disabled');
        $('#signup').css('cursor', 'pointer');
    }else{
        console.log('error');
        $('.error-match-password').show();
        $('#signup').attr('disabled');
        $('#signup').css('cursor', 'not-allowed');
    }
}
}


function changeCartQty(x, y, product, alreadyqty){
    var qty = $('#' + y).val();
    if(product != ""){
           $.ajax({
               type: 'post',
               url: 'lsmartbk/helpers/event.php',
               data: {cartquantity: product, alreadyqty: alreadyqty},
               success: function(data){
                   console.log("rcqty =>", data);
                   var itemval = parseInt(data);
                   console.log("cart quantity =>", qty + "=" + itemval);
                   if(qty <= itemval){
                    window.location.href = url + "cartaction?cartid=" + x + "&&qty=" + qty;
                       console.log("uantity=>", itemval);
                   }else{
                       if($.cookie("shopping_cart")){
                           console.log("thisone");
                            $('#' + y).val(getCookiesVal(x));
                        }else{
                            console.log("this one");
                            cartquantity(x, y);
                        }
                       $('.' + y).html("");
                       $('.' + y).html('<p class="text-danger">Only '+ data +' Items Available.</p>');
                   }
                   // console.log("check qty =>", data);
               }
           })
    }
}

function getCookiesVal(cartid){
    var cartitem = JSON.parse($.cookie("shopping_cart"));
    for(i=0; i <= cartitem.length - 1; i++){
        if(cartid == cartitem[i]['cartid']){
            return cartitem[i]['qty'];
        }
    } 
}

function cartquantity(id, y){
    if(id != ""){
        $.ajax({
            type: 'post',
            url: 'admin/helpers/event.php',
            data: {getcartquantity:id},
            success: function(data){
                console.log("res =>", data);
                // return data;
                $('#' + y).val(data);
            }
        })
    }
}

// getCookiesVal();

function bussinessPurpose() {
    var checkBx = document.getElementById('myCheck');
    var bussinessInput = document.getElementById('bussinessInputDiv');
    if(checkBx.checked == true){
        bussinessInput.style.display = "block";
    }else{
        bussinessInput.style.display = "none";
    }
  }


  // checkout
  $(document).ready(function(){
    $('.slideUpDown, .clickRadio, .hidetag, .confirmOrder, .paymentNow').hide();
    $('.viewToggle').click(function(){
      $('.slideUpDown').slideToggle();
    });
    $('.viewtag').click(function(){
      $(this).hide();
      $('.hidetag').show();
    });
    $('.hidetag').click(function(){
      $(this).hide();
      $('.viewtag').show();
    });

    $('.deliveryMethod input[type="radio"]').click(function(){
      var inputValue = $(this).attr('value');
      var textInputValue = $('.' + inputValue);
      $(".deliverMethodPopup").not(textInputValue).hide();
      $(textInputValue).show();
    });

   
    
  });

    function orderBlock(x, y){
        $('.' + x).show();
        $('.' + y).hide();
    }

// open address block
var titlearray=["Mr", "Mrs", "Miss", "Ms", "Dr"];
        var option = '';
        for(i = 0; i <= titlearray.length - 1; i++){
            option = option + '<option value="' + titlearray[i] + '">' + titlearray[i] + '</option>';
        }
        let quote = "'";
        let billingContent = '<div class="billingFormBorder"><div class="checkoutProcess"><p>Billing Address</p></div><div class="billing-address-form"><div class="titleDiv"> <p>Title</p><select name="title" required>' + option + '</select> </div><div class="addressInputDiv"> <input type="text" placeholder="First name*" name="name" required> </div><div class="addressInputDiv"> <input type="text" placeholder="Last name*" name="last_name" required> </div><div class="addressInputDiv"> <div class="row"> <div class="col-3"> <p>+91</p></div><div class="col-9"> <input type="text" placeholder="Mobile number*" name="mobile" onkeyup="mobileValidations(' + quote +'shipmobile2' + quote +', ' + quote +'shipmobileerror2' + quote +')" class="mb-0" id="shipmobile2" required><p class="text-danger" style="border: none; display:none" id="shipmobileerror2">Please Enter 10 Digit Valid Mobile Number</p> </div></div></div><div class="addressInputDiv"> <input type="text" placeholder="Address line 1*" name="add_line_1" required> </div><div class="addressInputDiv"> <input type="text" placeholder="Address line 2" name="add_line_2"> </div><div class="addressInputDiv"> <input type="text" placeholder="Town/city*" name="city" required> </div><div class="addressInputDiv"> <select class="state" name="state" required><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Lakshadweep">Lakshadweep</option><option value="Puducherry">Puducherry</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option></select> </div><div class="addressInputDiv"> <input type="text" placeholder="Postcode*" name="pincode" required> </div></div></div>';
function openAddressBlock(x, y){
    if($("#" + x).prop('checked') == true){
        $('.' + y).html(" ");
        let quote = "'";
        $('.' + y).html('<div class="addressFormBorder"><div class="titleDiv"><p>Title</p> <select name="title" required>' + option + '</select></div><div class="addressInputDiv"> <input type="text" placeholder="First name*" name="name" required></div><div class="addressInputDiv"> <input type="text" placeholder="Last name*" name="last_name" required></div><div class="addressInputDiv"><div class="row"><div class="col-3"><p>+91</p></div><div class="col-9"> <input type="text" placeholder="Mobile number*" name="mobile"  onkeyup="mobileValidations(' + quote +'shipmobile' + quote +', ' + quote +'shipmobileerror' + quote +')" class="mb-0" id="shipmobile" required><p class="text-danger" style="border: none; display:none" id="shipmobileerror">Please Enter 10 Digit Valid Mobile Number</p></div></div></div><div class="addressInputDiv"> <input type="text" placeholder="Address line 1*" name="add_line_1" required></div><div class="addressInputDiv"> <input type="text" placeholder="Address line 2" name="add_line_2"></div><div class="addressInputDiv"> <input type="text" placeholder="Town/city*" name="city" required></div><div class="addressInputDiv"><select class="state" name="state" required><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Lakshadweep">Lakshadweep</option><option value="Puducherry">Puducherry</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option> </select></div><div class="addressInputDiv"> <input type="text" placeholder="Postcode*" name="pincode" required></div> <div class="px-3"><button type="submit" name="save_address" class="button">Save address</button></div></div>')
    }else{
        $('.' + y).html(" ");
    }
}

function openBillingBlock(x, y){
    if($("#" + x).prop('checked') == true){
        $('.' + y).html(" ");
    }else{
        
$('.' + y).html(" ");
$('.' + y).html('<input type="hidden" name="bill_add" value="diff">' + billingContent);
}
}



// checkout sachin's
$('.checkoutBorder').click(function(){
    $(this).css('border-color','#ffe256');
    $(this).parent().parent().siblings().find(".checkoutBorder").css('border-color','#f2f2f2');
})



// new cart
$(document).ready(function() {
    $('.accordion_content-open').show();
    $('.accordion').find('.accordion-toggle').click(function() {
        console.log('toggle', $(this).next());
        $(this).next().slideToggle('600');
        $(".accordion-content").not($(this).next()).slideUp('600');
    });
    $('.accordion-toggle').on('click', function() {
        $(this).toggleClass('active').siblings().removeClass('active');
    });
});


$(document).ready(function(){
    // Read value on page load
    $("#result b").html($("#customRange").val());

    // Read value on change
    $("#customRange").change(function(){
        $("#result b").html($(this).val());
    });
});  

$('.subMenuInn ul li').mouseover(function(){
    var srcimg = $(this).attr('data-img');
    var storeType = $(this).attr('data-type');
    $('.subMenuimg img').attr("src", srcimg);
    $(".submenuAddress h2").text(storeType)
    
})



var sliderLeft=document.getElementById("slider0to50");
 var sliderRight=document.getElementById("slider51to100");
 var inputMin=document.getElementById("min");
 var inputMax=document.getElementById("max");

///value updation from input to slider
//function input update to slider
function sliderLeftInput(){//input udate slider left
    sliderLeft.value=inputMin.value;
    // alert("Hello");
}
function sliderRightInput(){//input update slider right
    sliderRight.value=(inputMax.value);//chnage in input max updated in slider right
   
}

//calling function on change of inputs to update in slider
// inputMin.addEventListener("change",sliderLeftInput);
// inputMax.addEventListener("change",sliderRightInput);


///value updation from slider to input
//functions to update from slider to inputs 
function inputMinSliderLeft(){//slider update inputs
    inputMin.value=sliderLeft.value;
    var params = new window.URLSearchParams(window.location.search);
    var cat = params.get('cat');
    var catarray = cat.split("_");
    var catSubcat = catarray[0].split("-");
    applyFilter(catSubcat[0], catSubcat[1]);
}
function inputMaxSliderRight(){//slider update inputs
    inputMax.value=sliderRight.value;
    var params = new window.URLSearchParams(window.location.search);
    var cat = params.get('cat');
    var catarray = cat.split("_");
    var catSubcat = catarray[0].split("-");
    applyFilter(catSubcat[0], catSubcat[1]);
    // alert($('#max').val());
}
// sliderLeft.addEventListener("change",inputMinSliderLeft);
// sliderRight.addEventListener("change",inputMaxSliderRight);


$('.closeTab .closeBtn').click(function(){
    $('.closeTab').remove();
})

// setTimeout(() => {
//     $('.loaderOuter').remove();
// }, 3000)

function getAddress(x){
    if(x != ""){
      $.ajax({
        type: 'post',
        url: 'admin/helpers/event.php',
        data: {address: x},
        dataType: 'json',
        success: function(data){
          // alert(data);
          console.log("address=>", data);
          if(data != ""){
            $('#add-title').val(data[3]);
            $('#first-name').val(data[4]);
            $('#last-name').val(data[5]);
            $('#mobile').val(data[6]);
            $('#line-a').val(data[7]);
            $('#add-city').val(data[8]);
            $('#add-state').val(data[9]);
            $('#add-pincode').val(data[10]);
            $('#save-address').hide();
            $('#update-address').show();
          }else{
            $('#address-form-one').reset();
          }
        }
      })
    }
  }

  function formReset(){
    $('#address-form-one')[0].reset();
    $('#save-address').show();
    $('#update-address').hide();
  }

  function changeSize(sku){
   let newsku = $('#' + sku).find('option:selected').attr('id');
    location.href = url + 'single-product?p=' + newsku;
    // alert(url + 'single-product?p=' + newsku);
  }
  
  (function () {  
        var  
         form = $('.form'),  
         cache_width = form.width(),  
         a4 = [595.28, 841.89];
  
        $('#create_pdf').on('click', function () {
            // alert('Hii');
            $('body').scrollTop(0);  
            createPDF();  
        });  
 
        function createPDF() {  
            getCanvas().then(function (canvas) {  
                var  
                 img = canvas.toDataURL("image/png"),  
                 doc = new jsPDF({  
                     unit: 'px',  
                     format: 'a4'  
                 });  
                doc.addImage(img, 'JPEG', 20, 20);  
                doc.save('Greyon-invoice.pdf');  
                form.width(cache_width);  
            });  
        }  
  

        function getCanvas() {  
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');  
            return html2canvas(form, {  
                imageTimeout: 2000,  
                removeContainer: true  
            });  
        }  
  
    }());  

    (function ($) {  
        $.fn.html2canvas = function (options) {  
            var date = new Date(),  
            $message = null,  
            timeoutTimer = false,  
            timer = date.getTime();  
            html2canvas.logging = options && options.logging;  
            html2canvas.Preload(this[0], $.extend({  
                complete: function (images) {  
                    var queue = html2canvas.Parse(this[0], images, options),  
                    $canvas = $(html2canvas.Renderer(queue, options)),  
                    finishTime = new Date();  
  
                    $canvas.css({ position: 'absolute', left: 0, top: 0 }).appendTo(document.body);  
                    $canvas.siblings().toggle();  
  
                    $(window).click(function () {  
                        if (!$canvas.is(':visible')) {  
                            $canvas.toggle().siblings().toggle();  
                            throwMessage("Canvas Render visible");  
                        } else {  
                            $canvas.siblings().toggle();  
                            $canvas.toggle();  
                            throwMessage("Canvas Render hidden");  
                        }  
                    });  
                    throwMessage('Screenshot created in ' + ((finishTime.getTime() - timer) / 1000) + " seconds<br />", 4000);  
                }  
            }, options));  
  
            function throwMessage(msg, duration) {  
                window.clearTimeout(timeoutTimer);  
                timeoutTimer = window.setTimeout(function () {  
                    $message.fadeOut(function () {  
                        $message.remove();  
                    });  
                }, duration || 2000);  
                if ($message)  
                    $message.remove();  
                $message = $('<div ></div>').html(msg).css({  
                    margin: 0,  
                    padding: 10,  
                    background: "#000",  
                    opacity: 0.7,  
                    position: "fixed",  
                    top: 10,  
                    right: 10,  
                    fontFamily: 'Tahoma',  
                    color: '#fff',  
                    fontSize: 12,  
                    borderRadius: 12,  
                    width: 'auto',  
                    height: 'auto',  
                    textAlign: 'center',  
                    textDecoration: 'none'  
                }).hide().fadeIn().appendTo('body');  
            }  
        };  
    })(jQuery);  

    function mobileValidations(id, mobileerror) {
        let mobile = $('#' + id).val();
        if(/^\d{10}$/.test(mobile)){
            $('#' + mobileerror).hide();
            $('#' + id).removeClass('errorborder');
            $('.paymantOption input[type="submit"]').prop('checked', false);
        }else{
            $('#' + id).addClass('errorborder');
            $('#' + mobileerror).show();
            $('.paymantOption input[type=submit]').prop('checked', true);
        }
    }

    function refreshPage(){
        location.reload();  
    }

    
	function readLessMore(){
			$('.paraText.active').css({"height": "80px", "overflow-y":"scroll"});
			$('.readMore span:nth-child(2)').show();
			$('.readMore span:nth-child(1)').hide();
	}

  function openDiv(data){
    $('.loginDiv').hide();
    $('#' + data + ', .signupDiv').show();
  }

    function mobileValidations(id, mobileerror) {
        let mobile = $('#' + id).val();
        if(/^\d{10}$/.test(mobile)){
            $('#' + mobileerror).hide();
            $('#' + id).removeClass('errorborder');
            $('.paymantOption2 input[type="submit"], .paymantOption2 button').prop('disabled', false);
        }else{
            $('#' + id).addClass('errorborder');
            $('#' + mobileerror).show();
            $('.paymantOption2 input[type=submit], .paymantOption2 button').prop('disabled', true);
        }
    }



function saveAddress(addtype){
    // console.log('Hit1')
    let name = $('#first-name').val();
    let lname = $('#last-name').val();
    let mobile = $('#mobile').val();
    let address = $('#line-a').val();
    let city = $('#add-city').val();
    let state = $('#add-state').val();
    let pincode = $('#add-pincode').val();
    if(name != "" && mobile != "" && city != "" && state != "" && pincode != ""){
    // $('#save-address, #update-address').prop('disabled', true);
    $('.loaderOuter').show();
      $.ajax({
        type: 'post',
        url: 'lsmartbk/helpers/event.php',
        data: {name:name, lname:lname, mobile:mobile, addresss:address, city:city, state:state, pincode:pincode, addtype:addtype},
        success: function(data){
          data = data.replace(/(\r\n|\n|\r)/gm, "");
          if(data == 400){
            $('.add-error').text("");
            $('.add-error').text("Please Fill all Fields");
            $('.loading-page').hide();
            // $('#save-address').prop('disabled', false);
          }else if(data == 401){
            $('.add-error').text("");
            $('.add-error').text("Invalid Pincode");
            $('.loading-page').hide();
            // $('#save-address').prop('disabled', false);
          }else if(data == 403){
            $('.add-error').text("");
            $('.add-error').text("State Not Matched with pincode");
            $('.loading-page').hide();
            // $('#save-address').prop('disabled', false);
          }else{
            //   console.log('city=>', data);
            $('#add-city').val(data);
            $('.add-error').text("");
            $('.loading-page').hide();
            // $('#save-address').prop('disabled', false);
            document.usersave.submit();
          }
          
        }
      });
    }else{
      errorMsg("Please Fill All Fields.")
    }
  }

  
  $('#dba').click(function(){
    $('.diffBillAdd').css('display','block');
    $('.diffBillAdd .form-control').prop('required', true);
  });

  $('#ssa').click(function(){
    $('.diffBillAdd').css('display','none');
    $('.diffBillAdd .form-control').prop('required', false);
  });