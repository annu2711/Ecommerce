<?php include_once"helpers/index.php";
if(!empty($id) && !empty($usertoken)){
	header('location: index');
}
?>

<section class="loginSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-12 my-5">
				<div class="row no-gutters">
					<div class="col-md-12 px-5">
						<div class="loginDiv forgotPassword">
							<p>Forgot your password?</p>
							<p>Please enter your email address below. We will send you a new password, which you can use to sign in. You will then be prompted to reset your password for security.</p>
							<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
								<label class="mt-4">EMAIL ADDRESS*</label>
								<input type="email" placeholder="Email address" name="email" required>
								<p class="mx-auto"><input type="submit" name="forget_password" class="button" value="Submit"></p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include_once"helpers/footer.php";

if(isset($_POST['forget_password'])){
	extract($_POST);
	if(!empty($email)){
		$query = mysqli_query($conn, "SELECT * FROM clients WHERE email='$email'");
		if($query){
			if(mysqli_num_rows($query) > 0){
				$user_data = mysqli_fetch_array($query);
				$user_key = $user_data[1];
				$password = rand_char(10);
				$pass = md5($password);
				$sql = mysqli_query($conn, "UPDATE clients SET password='$pass' WHERE client_key='$user_key'");
				if($sql){
					$content = 'Hi '.$user_data[2].' '.$user_data[3].',<br>You requested a password reset for your Lsmart.in account.<br><br>
					Your new password is :<strong>'.$password.'</strong>
					If you need any assistance, please contact us at support@lsmart.in.';
					$mail = sendEmail('orders@lsmart.in', $email, 'Password Reset', $content);
					if($mail){
						echo '<script>$.notify("We have Sent You a Mail", "success");</script>';
						// header()
					}
				}
			}else{
				echo '<script>$.notify("Invalid Email ID", "danger");</script>';
			}
		}
	}
}

?>