<?php
ob_start();
include_once "admin/helpers/config.php";
define("LAST_URL", $_SERVER['HTTP_REFERER']);
if(isset($_GET['cartid'])){
    extract($_GET);
    if(!empty($cartid) && !empty($qty)){
        if(isset($_COOKIE['shopping_cart'])){
            $cookie_data = stripcslashes($_COOKIE['shopping_cart']);
            $cart_data = json_decode($cookie_data, true);
            $new_cart = [];
            for($i=0; $i <= count($cart_data) - 1; $i++){
                $cartkey = $cart_data[$i]['cartid'];
                $cartproduct = $cart_data[$i]['product'];
                $cartcolor = $cart_data[$i]['color'];
                $cartsize = $cart_data[$i]['size'];
                $cartqty = $cart_data[$i]['quantity'];
                if($cartid == $cartkey){
                    $item_array = array(
                        'cartid' => $cartkey,
                        'product' => $cartproduct,
                        'color' => $cartcolor,
                        'size' => $cartsize,
                        'qty' => $qty
                    );
                    $new_cart[] = $item_array;
                }else{
                    $new_cart[] = $cart_data[$i];
                }
    }
    
    // end forloop
    $data = "";
    $removecart = setcookie('shopping_cart', $data, time() - 3600, '/');
    // echo "Hii".$removecart;
    $item_data = json_encode($new_cart);
    echo json_encode($item_data);
    $cartadd = setcookie('shopping_cart', $item_data, time() + (172800 * 30)); // 86400 = 1 day
    if($cartadd){
        // echo 1;
        header('location: '.LAST_URL);
    }else{
        header('location: '.LAST_URL);
    }
}else{
    try{
        $stmt = $conn->prepare("UPDATE user_cart SET qty=? WHERE uc_key=?");
        $stmt->bind_param('ss', $qty, $cartid);
        if ($stmt->execute()){
            // echo "hello";
            header('location: '.LAST_URL);
        }else{
            throw new exception($conn->error);
        }
        $stmt->close();
    }catch(Exception $e){
        // echo "Error: " . $e->getMessage();
        header('location: '.LAST_URL);
    }
}
    }else{
        header('location: '.LAST_URL);
    }
}else{
    header('location: '.LAST_URL);
}

