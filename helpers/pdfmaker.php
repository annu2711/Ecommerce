<?php
ob_start();
require_once '../dompdf/autoload.inc.php';
require_once '../lsmartbk/helpers/config.php';
require_once '../lsmartbk/helpers/function.php';

use Dompdf\Dompdf;

function downloadInvoice($html, $invoiceno){
$dompdf = new Dompdf(); 
$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
ob_end_clean();
$dompdf->stream($invoiceno.".pdf",array("Attachment" => true));
}
$orderarr = [];
if(isset($_GET['invoice'])){
    $orderid = $_GET['invoice'];
    $gst = 18;
    $sql = mysqli_query($conn, "SELECT * FROM orders WHERE order_key='$orderid' ORDER BY order_id DESC LIMIT 1");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            $orderarr = mysqli_fetch_array($sql);
            print_r($orderarr);
        }
    }else{
        echo mysqli_error($conn);
    }
    
    // company info
 $companysql = mysqli_query($conn, "SELECT * FROM company_info ORDER BY id ASC LIMIT 1");
 if($companysql){
     if(mysqli_num_rows($companysql) > 0){
         $companyarr = mysqli_fetch_array($companysql);
     }
 }

// shipping address
$shippingsql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$orderarr[6]' ORDER BY ab_id DESC LIMIT 1");
if($shippingsql){
    if(mysqli_num_rows($shippingsql) > 0){
        $shippingarr = mysqli_fetch_array($shippingsql);
    }
}

// billing address
$billingsql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$orderarr[5]' ORDER BY ab_id DESC LIMIT 1");
if($billingsql){
    if(mysqli_num_rows($billingsql) > 0){
        $billingarr = mysqli_fetch_array($billingsql);
    }
}
    $orderdate = explode(" ", $orderarr[10]);
    
    $invoiceno =  getSinglevalue('invoice', 'order_id', $orderid, 2);
    $headerinvoice = '
    <table style="width: 100%;">
        <tr>
            <td colspan="2" width="100%"><img src="../img/logo.png" width="100px" style="margin-bottom: 25px;"></td>
        </tr>
        <tr>
            <td width="50%" align="left">
            <h4 style="margin: 0px; font-size: 18px;"><strong>Sold By</strong></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>'.$companyarr[1].'</strong></h4>
            <p style="margin: 5px 0px">'.$companyarr[2].'</p>
            <p style="margin: 5px 0px">'.$companyarr[4].'-'.$companyarr[5].', INDIA</p>
            <div style="margin-bottom: 30px;"></div>
            </td>
            <td width="50%" align="right">
            <h4 style="margin: 0px; font-size: 18px;"><strong>Billing Address</strong></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>'.$shippingarr[3]." ".$shippingarr[4]." ".$shippingarr[5].'</strong></h4>
            <p style="margin: 5px 0px 0px 0px">Mo: '.$shippingarr[6].'</p>
            <p style="margin: 5px 0px">'.$shippingarr[7].", ".$shippingarr[8].'</p>
            <p style="margin: 5px 0px">'.$shippingarr[9].'-'.$shippingarr[10].', INDIA</p>
            <div style="margin-bottom: 30px;"></div>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left">
            <h4 style="margin: 0px; font-size: 18px;"><strong>Pan No: </strong><span style="font-weight: normal; font-size: 16px;">'.$companyarr[7].'</span></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>GST Registration No: </strong><span style="font-weight: normal; font-size: 16px;">'.$companyarr[6].'</span></h4>
            <div style="margin-bottom: 30px;"></div>
            </td>
            <td width="50%" align="right">   
            <h4 style="margin: 0px; font-size: 18px;"><strong>Shipping Address</strong></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>'.$billingarr[3]." ".$billingarr[4]." ".$billingarr[5].'</strong></h4>
            <p style="margin: 5px 0px">Mo: '.$billingarr[6].'</p>
            <p style="margin: 5px 0px">'.$billingarr[7].", ".$billingarr[8].'</p>
            <p style="margin: 5px 0px">'.$billingarr[9].'-'.$billingarr[10].', INDIA</p>
            <div style="margin-bottom: 30px;"></div>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left">
            <h4 style="margin: 0px; font-size: 18px;"><strong>Order No: </strong><span style="font-weight: normal; font-size: 16px;">'.$orderarr[0].'</span></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>Order Date: </strong><span style="font-weight: normal; font-size: 16px;">'. $orderdate[0] .'</span></h4>
            </td>
            <td width="50%" align="right">   
            <h4 style="margin: 0px; font-size: 18px;"><strong>Invoice Number: </strong><span style="font-weight: normal; font-size: 16px;">'.$invoiceno.'</span></h4>
            <h4 style="margin: 0px; font-size: 18px;"><strong>Invoice Date: </strong><span style="font-weight: normal; font-size: 16px;">'.$orderdate[0].'</span></h4>
            </td>
        </tr>
    </table>
    <div class="col-md-12">
    <table cellspacing="0" cellpadding="0" border="1" style="border: 1px solid #555; margin-top: 30px; width: 100%;">
                <thead style="background: #dedede;">
                <tr>
                    <td width="5%" style="padding: 3px">S. No.</td>
                    <td width="30%" style="padding: 3px">Description</td>
                    <td width="10%" style="padding: 3px">Unit Price</td>
                    <td width="5%" style="padding: 3px">Qty</td>
                    <td width="10%" style="padding: 3px">Net Amount</td>
                    <td width="10%" style="padding: 3px">Tax Rate</td>
                    <td width="10%" style="padding: 3px">Tax Type</td>
                    <td width="10%" style="padding: 3px">Tax Amount</td>
                    <td width="10%" style="padding: 3px">Total Amount</td>
                </tr>
                </thead>
                <tbody>';

                    $orderdetailsql = mysqli_query($conn, "SELECT * FROM order_book WHERE order_key='$orderarr[1]'");
                    if($orderdetailsql){
                        if(mysqli_num_rows($orderdetailsql) > 0){
                            $num = 1;
                            $totaltax = 0;
                            $totalamount = 0;
                            $coreprice = "";
                            $tax = "";
                            $middleinvoice = "";
                            while($rows = mysqli_fetch_assoc($orderdetailsql)){ 
                                $coreprice = round($rows['price'] / ((100 + $gst) / 100), 2);
                                $tax = round($rows['price'] - $coreprice, 2);
                                $currenttotal = $coreprice + $tax;
                                $totaltax = $totaltax + $tax;
                                $totalamount = $totalamount + $coreprice + $tax;

                    $middleinvoice = $middleinvoice.'<tr>
                        <td style="padding: 3px">'. $num++ .'</td>
                        <td style="padding: 3px">'.getSinglevalue('product', 'sku', $rows['product'], 4).'</td>
                        <td style="padding: 3px">'.$coreprice.'</td>
                        <td style="padding: 3px">'.$rows['qty'] .'</td>
                        <td style="padding: 3px">'. $coreprice * $rows['qty'] .'</td>
                        <td style="padding: 3px">'.$gst.' %</td>
                        <td style="padding: 3px">';
                            if($billingarr[9] == 'Delhi' || $billingarr[9] == 'New Delhi' || $billingarr[9] == 'DELHI' || $billingarr[9] == 'NEW DELHI'){
                                $middleinvoice = $middleinvoice.'IGST <br> CGST';
                            }else{
                                $middleinvoice = $middleinvoice.'IGST';
                            }
                        $middleinvoice = $middleinvoice.'</td>
                        <td style="padding: 3px">'. $tax* $rows['qty'] .'</td>
                        <td style="padding: 3px">'. $currenttotal* $rows['qty'] .'</td>
                    </tr>';
                        }
                        }
                    }
                
                    
                    $footerinvoice = '<tr>
                        <td colspan="7" style="padding: 3px">Total</td>
                        <td style="padding: 3px; background: #dedede;">'.round($totaltax, 2).'</td>
                        <td style="padding: 3px; background: #dedede;">'.round($totalamount, 2).'</td>
                    </tr>
                    <tr>
                    <td colspan="9" align="left">
                        <h3 style="margin: 5px; font-size: 16px; font-weight: bold">Amount in Words:</h3>
                        <h3 style="margin: 5px; font-size: 16px; margin-top: 0px; text-transform: capitalize">'.getIndianCurrency($totalamount).'</h3>
                    </td>
                    </tr>
                </tbody>
    </table>
    </div>
    <div class="col-md-12">
        <div class="sign-box" align="right">
        <h3 style="margin: 5px; font-size: 18px">For Bquest India</h3>
                        <h3 style="margin: 5px; font-size: 18px">Authorized Signatory</h3>
        </div>
        <p style="font-weight: normal; float: left; display: inline-block;">Whether tax is payable under reverse charge - No</p>
    </div>';


$invoice =  $headerinvoice.$middleinvoice.$footerinvoice;
downloadInvoice($invoice, $invoiceno);   
// echo $invoice;
}

?>