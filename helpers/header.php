<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="x-ua-compatible" content="ie=edge"/>
  <title>Lsmart || Designer , Fashion , Accessories and more - Shop online at Lsmart.</title>
<meta name="description" content="">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
</head>
<body id="main-body">
<div class="loaderOuter" id="loaderOuter" style="display: none"><div class="categoryLoader"></div></div>
<header class="mobileMenu">
	<div class="mobileMeniActive">
	<div class="upperHeader" style="display: none;">
		<div class="container-fluid px-0">
			<div class="row no-gutters">
				<div class="col-lg-6 col-xl-8 offset-xl-2 offset-lg-3 d-flex align-items-center justify-content-center py-2 py-lg-0">
					<div class="upperHeaderContent">
						<p>Free india standard deliveries</p>
					</div>
				</div>
				<div class="col-lg-3 col-xl-2 d-lg-block d-none">
					<div class="upperHeaderSideContent text-right">
						<p><a href="javascript:void(0)"><b>Stores</b></a></p>
						<p><a href="javascript:void(0)"><b>Services</b></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="middleHeader py-2">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-lg-3 col-4 d-flex align-items-center">
					<div class="translation-country">
						<div class="menuBar d-lg-none d-block">
							<img src="img/arrow/bars.svg" class="menuClick">
						</div>
				       <figure class="mb-0 d-lg-block d-none">  
				        <figcaption> 
						<p class="mt-0 mb-0"><a href="#"><img src="img/country.png" class="mr-1"> INR <strong style="display: none">| English</strong></a></p>  
				        </figcaption> 
				       </figure> 
				    </div>
				</div>
				<div class="col-lg-6 col-4 d-flex align-items-center justify-content-center">
					<a href="index.php"> 
						<img src="img/logo.png" class="logoLsmart" alt="" title="">
 					</a>
				</div>
				<div class="col-lg-3 col-4 d-flex align-items-center">
					<div class="upperMiddleSideContent w-100">
						<ul>
							<li class="d-lg-block d-none mt-1" style="position: relative;"><a href="javascript:void(0)">
								<?php if(!empty($id) && !empty($usertoken)){ ?>
								<span style="left: -16px; top: 17px; width: 100px; padding:5px 0; overflow: hidden;"><strong><?php echo $name ?></strong></span>
								<?php } ?>
								<img src="img/user.png" alt=""></a>
								<div class="menuPopup">
									<ul>
										<!-- <li><a href="#">My account</a></li> -->
										<?php if(empty($id) && empty($usertoken)){ ?>
										<li><a href="login">Sign in or Register</a></li>
										<?php }else{ ?>
										<li class="active"><a href="myaccount">My Account</a></li>
										<li><a href="address-book">Addresses</a></li>
										<li><a href="order-return">Orders & Returns</a></li>
										<!-- <li><a href="my-preferences">My Preferences</a></li> -->
										<li><a href="logout">Logout</a></li>
										<?php } ?>
									</ul>
								</div>
							</li>


							<li class="d-lg-block d-none mt-1 wishlistCount"><a href="wishlist"><img src="img/like.png" alt=""><p><?php echo $totalwishquantity ?></p></a></li>
							<li class="mt-1"><a class="position-relative" href="shopping-cart"><img src="img/cart.png" alt=""><span><?php echo $totalcartquantity ?></span></a></li>
							<li class="d-md-block d-none"><a href="javascript:void(0)">
								<form action="search.php" method="get">
									<div class="input-group inputSearch">
										<input type="text" class="searchInput" <?php echo (!empty($searchval)) ? 'value="'.$searchval.'"' : 'placeholder="Search Bquest"' ; ?> name="searchinput">
										<button input="submit" class="input-group-append">
											<img src="img/search.svg" alt="">
										</button>
									</div>
								</form>
							</a></li>



							
							<li class="col-12 d-block d-md-none"><a href="javascript:void(0)">
								<form action="search.php" method="get">
									<div class="input-group inputSearch">
										<input type="text" class="searchInput" <?php echo (!empty($searchval)) ? 'value="'.$searchval.'"' : 'placeholder="Search Bquest"' ; ?> name="searchinput">
										<button input="submit" class="input-group-append">
											<img src="img/search.svg" alt="">
										</button>
									</div>
								</form>
							</a></li>

							
						</ul>
					</div>
				</div>

				
                <!--  			
	                <div class="">
						<form>
									<div class="input-group inputSearch2">
										<input type="text" class="searchInput" placeholder="search Lsmart" name="">
										<div class="input-group-append">
											<img src="img/search.svg" alt="">
										</div>
									</div>
								</form>
					</div> -->


			</div>
		</div>
	</div>
	<div class="lowerHeader">
		<div class="lowerHeaderInnerD">
			 <div class="container">
			 	<div class="row">
					 
			 		<div class="sidebarContent w-100">
					 <div class="backgroundOpacity"></div>
			 			<p class="d-block d-lg-none">Shop</p>
			 		<ul class="d-block d-lg-flex w-100 mb-0">

					 
						 <li class="subMenuBtn">
						 <a href="designers" class="d-none d-lg-block">Designers</a>
						 <a href="javascript:;" class="d-block d-lg-none">Designers <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row multipleMenuDiv">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Designers</p>
					 					</div>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Women</strong></p></li>
												 <!-- <li><a href="">All Women Brands</a></li> -->
												 <li><a href="brand?brand=Saint Laurant_Women_">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Alexander McQueen_Women_">Alexander McQueen</a></li>
												 <li><a href="brand?brand=Chloe_Women_">Chloe</a></li> -->
												 <li><a href="brand?brand=Tods_Women_">Tod's</a></li>
												 <li><a href="brand?brand=Brunello Cucinelli_Women_">Brunello Cucinelli</a></li>
												 <!-- <li><a href="brand?brand=Tom Ford_Women_">Tom Ford</a></li> -->
												 <!-- <li><a href="brand?brand=Roberto Cavalli_Women_">Roberto Cavalli</a></li>
												 <li><a href="brand?brand=Missoni_Women_">Missoni</a></li> -->
					 						</ul>
					 					</div>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Men</strong></p></li>
												 <!-- <li><a href="">All Men Brands</a></li> -->
												 <li><a href="brand?brand=Berluti_Men_">Berluti</a></li>
												 <li><a href="brand?brand=Brunello Cucinelli_Men_">Brunello Cucinelli</a></li>
												 <li><a href="brand?brand=Tods_Men_">Tod's</a></li>
												 <li><a href="brand?brand=Saint Laurant_Men_">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Tom Ford_Men_">Tom Ford</a></li> -->
												 <!-- <li><a href="brand?brand=Brioni_Men_">Brioni</a></li>
												 <li><a href="brand?brand=Alexander McQueen_Men_">Alexander McQueen</a></li>
												 <li><a href="brand?brand=Corneliani_Men_">Corneliani</a></li>
												 <li><a href="brand?brand=Gucci_Men_">Gucci</a></li>
												 <li><a href="brand?brand=Off-White_Men_">Off-White</a></li>
												 <li><a href="brand?brand=Balmain_Men_">Balmain</a></li> -->
					 						</ul>
					 					</div>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Accessories</strong></p></li>
												 <!-- <li><a href="brand?brand=Bottega Veneta__lcgpa">Bottega Veneta</a></li>
												 <li><a href="brand?brand=Dolce & Gabbana__lcgpa">Dolce & Gabbana</a></li>
												 <li><a href="brand?brand=Loewe__lcgpa">Loewe</a></li> -->
												 <li><a href="brand?brand=Saint Laurant__lcgpa">Saint Laurent</a></li>
												 <li><a href="brand?brand=Berluti__lcgpa">Berluti</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Shoes</strong></p></li>
												 <li><a href="brand?brand=Tods__nbawm">Tod's</a></li>
												 <li><a href="brand?brand=Saint Laurant__nbawm">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Roberto Cavalli__nbawm">Roberto Cavalli</a></li> -->
												 <li><a href="brand?brand=Berluti__nbawm">Berluti</a></li>
												 <!-- <li><a href="brand?brand=Tom Ford__nbawm">Tom Ford</a></li> -->
												 <!-- <li><a href="brand?brand=Alexander McQueen__nbawm">Alexander McQueen</a></li>
												 <li><a href="brand?brand=Balenciaga__nbawm">Balenciaga</a></li>
												 <li><a href="brand?brand=Gucci__nbawm">Gucci</a></li> -->
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>BEAUTY</strong></p></li>
												 <!-- <li><a href="">All Beauty Brands </a></li> -->
												<li><a href="brand?brand=Molton Brown__bedsz">Molton Brown </a></li>
												 <!--<li><a href="brand?brand=Charlotte Tilbury__bedsz">Charlotte Tilbury </a></li>
												 <li><a href="brand?brand=Clé De Peau Beauté__bedsz">Clé De Peau Beauté </a></li> -->
												 <li><a href="brand?brand=Creed__bedsz">Creed</a></li>
												 <!-- <li><a href="brand?brand=Hermès__bedsz">Hermès</a></li>
												 <li><a href="brand?brand=La Mer__bedsz">La Mer </a></li> -->
												 <!-- <li><a href="brand?brand=TOM FORD__bedsz">TOM FORD </a></li> -->
					 						</ul>
					 					</div>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>
					 	<li class="subMenuBtn">
						 <a href="cat?cat=-_st!_Women" class="d-none d-lg-block">Women</a>
						 <a href="javascript:;" class="d-block d-lg-none">Women <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Women</p>
					 					</div>
				
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>CLOTHING</strong></p></li>
												 <li><a href="cat?cat=saqem-_st!_Women">All Clothing</a></li>								
												 <li><a href="cat?cat=saqem-ahyxc_st!_Women">Blazers</a></li>												 
												 <li><a href="cat?cat=saqem-gjvqf_st!_Women">Coats</a></li>												 
												 <li><a href="cat?cat=saqem-vktjn_st!_Women">Dresses</a></li>												 
												 <li><a href="cat?cat=saqem-wiflq_st!_Women">Jackets</a></li>											
												 <li><a href="cat?cat=saqem-iolzh_st!_Women">Jeans</a></li>												 
												 <li><a href="cat?cat=saqem-lpten_st!_Women">Nightwear</a></li>												 
												 <li><a href="cat?cat=saqem-wifle_st!_Women">Skirts</a></li>												 
												 <li><a href="cat?cat=saqem-cmzha_st!_Women">Sportswear</a></li>												 
												 <li><a href="cat?cat=saqem-ltpsw_st!_Women">Tops</a></li>			 
												 <li><a href="cat?cat=saqem-mdrvj_st!_Women">Trousers</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>ACCESSORIES</strong></p></li>
												 <li><a href="cat?cat=lcgpa-_st!_Women">All Accessories</a></li>
												 <li><a href="cat?cat=fancy-_st!_Women">Bags</a></li>
												 <li><a href="cat?cat=lcgpa-ljcnx_st!_Women">Belts</a></li>
												 <li><a href="cat?cat=lcgpa-vqosu_st!_Women">Hats</a></li>
												 <li><a href="cat?cat=lcgpa-unqml_st!_Women">Luggage</a></li>
												 <li><a href="cat?cat=lcgpa-bnxkh_st!_Women">Sunglasses</a></li>
												 <li><a href="cat?cat=lcgpa-fmula_st!_Women">Wallets & Purses</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>SHOES</strong></p></li>
												 <li><a href="cat?cat=nbawm-_st!_Women">All Shoes</a></li>
												 <li><a href="cat?cat=nbawm-rewil_st!_Women">Sneakers</a></li>
												 <li><a href="cat?cat=nbawm-gmyjl_st!_Women">Sandals</a></li>
												 <li><a href="cat?cat=nbawm-_st!_Women">Heels</a></li>
												 <li><a href="cat?cat=nbawm-mwbki_st!_Women">Flats</a></li>
												 <li><a href="cat?cat=nbawm-brnpl_st!_Women">Boots</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>FEATURED BRANDS</strong></p></li>
												 <!-- <li><a href="">All Womens Brands</a></li> -->
												 <!-- <li><a href="brand?brand=Alexander McQueen_Women_">Alexander McQueen</a></li>
												 <li><a href="brand?brand=Balenciaga_Women_">Balenciaga</a></li>
												 <li><a href="brand?brand=Balmain_Women_">Balmain</a></li>
												 <li><a href="brand?brand=Burberry_Women_">Burberry</a></li>
												 <li><a href="brand?brand=Chloe_Women_">Chloe</a></li> -->
												 <li><a href="brand?brand=Tods_Women_">Tod's</a></li>
												 <!-- <li><a href="brand?brand=Dolce & Gabbana_Women_">Dolce & Gabbana</a></li>
												 <li><a href="brand?brand=Givenchy_Women_">Givenchy</a></li>
												 <li><a href="brand?brand=Gucci_Women_">Gucci</a></li>
												 <li><a href="brand?brand=Jimmy Choo_Women_">Jimmy Choo</a></li>
												 <li><a href="brand?brand=Max Mara_Women_">Max Mara</a></li>
												 <li><a href="brand?brand=Moncler_Women_">Moncler</a></li>
												 <li><a href="brand?brand=Roger Vivier_Women_">Roger Vivier</a></li> -->
												 <li><a href="brand?brand=Saint Laurant_Women_">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Valentino_Women_">Valentino</a></li> -->

					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>

						 <li class="subMenuBtn">
						 <a href="cat?cat=-_st!_Men" class="d-none d-lg-block">Men</a>
						 <a href="javascript:;" class="d-block d-lg-none">Men <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Men</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>CLOTHING</strong></p></li>
												 <li><a href="cat?cat=saqem-_st!_Men">All Clothing</a></li>								
												 <li><a href="cat?cat=saqem-ahyxc_st!_Men">Blazers</a></li>												 
												 <li><a href="cat?cat=saqem-gjvqf_st!_Men">Coats</a></li>												 
												 <li><a href="cat?cat=saqem-wiflq_st!_Men">Jackets</a></li>											
												 <li><a href="cat?cat=saqem-iolzh_st!_Men">Jeans</a></li>
												 <li><a href="cat?cat=saqem-goyec_st!_Men">Polo Shirts</a></li>
												 <li><a href="cat?cat=saqem-xplro_st!_Men">Shirts</a></li>												 
												 <li><a href="cat?cat=saqem-cmzha_st!_Men">Sportswear</a></li>	
												 <li><a href="cat?cat=saqem-nkbhl_st!_Men">Suits</a></li>
												 <li><a href="cat?cat=saqem-ausei_st!_Men">Sweatshirts</a></li>
												 <li><a href="cat?cat=saqem-mdrvj_st!_Men">Trousers</a></li>
												 <li><a href="cat?cat=saqem-ygtuj_st!_Men">T-Shirts</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>ACCESSORIES</strong></p></li>
												 <li><a href="cat?cat=lcgpa-_st!_Men">All Accessories</a></li>
												 <li><a href="cat?cat=fancy-_st!_Men">Bags</a></li>
												 <li><a href="cat?cat=lcgpa-ljcnx_st!_Men">Belts</a></li>
												 <li><a href="cat?cat=lcgpa-dmfcj_st!_Men">Cufflinks</a></li>
												 <li><a href="cat?cat=lcgpa-bnxkh_st!_Men">Sunglasses</a></li>
												 <li><a href="cat?cat=lcgpa-pcyzr_st!_Men">Ties</a></li>
												 <li><a href="cat?cat=lcgpa-fmula_st!_Men">Wallets</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>SHOES</strong></p></li>
												 <li><a href="cat?cat=nbawm-_st!_Men">All Shoes</a></li>
												 <li><a href="cat?cat=nbawm-rewil_st!_Men">Sneakers</a></li>
												 <li><a href="cat?cat=nbawm-gmyjl_st!_Men">Sandals</a></li>
												 <li><a href="cat?cat=nbawm-tyfcl_st!_Men">Loafers</a></li>
												 <!-- <li><a href="cat?cat=nbawm-_st!_Men">Smart Shoes</a></li> -->
												 <li><a href="cat?cat=nbawm-brnpl_st!_Men">Boots</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>FEATURED BRANDS</strong></p></li>
												 <!-- <li><a href="">All Mens Brands</a></li> -->
												 <!-- <li><a href="brand?brand=Alexander McQueen_Men_">Alexander McQueen</a></li>
												 <li><a href="brand?brand=Balmain_Men_">Balmain</a></li>
												 <li><a href="brand?brand=Brioni_Men_">Brioni</a></li> -->
												 <li><a href="brand?brand=Brunello Cucinelli_Men_">Brunello Cucinelli</a></li>
												 <!-- <li><a href="brand?brand=Burberry_Men_">Burberry</a></li>
												 <li><a href="brand?brand=Dolce & Gabbana_Men_">Dolce & Gabbana</a></li>
												 <li><a href="brand?brand=Givenchy_Men_">Givenchy</a></li>
												 <li><a href="brand?brand=Gucci_Men_">Gucci</a></li> -->
												 <li><a href="brand?brand=Berluti_Men_">Berluti</a></li>
												 <!-- <li><a href="brand?brand=Off-White_Men_">Off-White</a></li> -->
												 <li><a href="brand?brand=Saint Laurant_Men_">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Tom Ford_Men_">Tom Ford</a></li> -->
					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 <li class="subMenuBtn">
						 <a href="cat?cat=fancy-_st!_" class="d-none d-lg-block">Bags</a>
						 <a href="javascript:;" class="d-block d-lg-none">Bags <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Bags</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>All Bags</strong></p></li>
												 <li><a href="cat?cat=fancy-rihfu_st!_">Backpacks</a></li>
												 <li><a href="cat?cat=fancy-kxryd_st!_">Belt Bags</a></li>
												 <li><a href="cat?cat=fancy-ihqtv_st!_">Bucket Bags</a></li>
												 <li><a href="cat?cat=fancy-treia_st!_">Clutch Bags</a></li>
												 <li><a href="cat?cat=fancy-mdokh_st!_">Cross Body Bags</a></li>
												 <li><a href="cat?cat=fancy-exhwn_st!_">Evening Bags</a></li>
												 <li><a href="cat?cat=fancy-puqjh_st!_">Luggage and Travel</a></li>
												 <li><a href="cat?cat=fancy-esowr_st!_">Shoulder Bags</a></li>
												 <li><a href="cat?cat=fancy-xrlas_st!_">Tote Bags</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>DESIGNERS</strong></p></li>
												 <!-- <li><a href="brand?brand=Balenciaga__fancy">Balenciaga</a></li>
												 <li><a href="brand?brand=Bottega Veneta__fancy">Bottega Veneta</a></li> -->
												 <li><a href="brand?brand=Saint Laurant__fancy">Saint Laurent</a></li>
												 <!-- <li><a href="brand?brand=Chloe__fancy">Chloé</a></li>
												 <li><a href="brand?brand=Givenchy__fancy">Givenchy</a></li>
												 <li><a href="brand?brand=Gucci__fancy">Gucci</a></li>
												 <li><a href="brand?brand=Loewe__fancy">Loewe</a></li> -->
												 <!-- <li><a href="brand?brand=Tom Ford__fancy">Tom Ford</a></li> -->
					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 <li class="subMenuBtn">
						 <a href="cat?cat=nbawm-_st!_" class="d-none d-lg-block">Shoes</a>
						 <a href="javascript:;" class="d-block d-lg-none">Shoes <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Shoes</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>WOMEN SHOES</strong></p></li>
												 <li><a href="cat?cat=nbawm-_st!_Women">All Shoes</a></li>
												 <li><a href="cat?cat=nbawm-draim_st!_Women">Ankle Boots</a></li>
												 <li><a href="cat?cat=nbawm-brnpl_st!_Women">Boots</a></li>
												 <li><a href="cat?cat=nbawm-dgzfq_st!_Women">Espadrilles</a></li>
												 <li><a href="cat?cat=nbawm-tfkum_st!_Women">Evening Shoes</a></li>
												 <li><a href="cat?cat=nbawm-mwbki_st!_Women">Flat Shoes</a></li>
												 <li><a href="cat?cat=nbawm-erblu_st!_Women">Mules</a></li>
												 <li><a href="cat?cat=nbawm-hoplq_st!_Women">Pumps</a></li>
												 <li><a href="cat?cat=nbawm-gmyjl_st!_Women">Sandals</a></li>
												 <li><a href="cat?cat=nbawm-rewil_st!_Women">Sneakers</a></li>
					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>MEN SHOES</strong></p></li>
												 <li><a href="cat?cat=nbawm-_st!_Men">ALL SHOES </a></li>
												 <li><a href="cat?cat=nbawm-brnpl_st!_Men">Boots</a></li>
												 <li><a href="cat?cat=nbawm-qrckp_st!_Men">Brogues </a></li>
												 <li><a href="cat?cat=nbawm-mgqaj_st!_Men">Chelsea Boots </a></li>
												 <li><a href="cat?cat=nbawm-wlpdo_st!_Men">Derby Shoes </a></li>
												 <li><a href="cat?cat=nbawm-wlpdo_st!_Men">Driving Shoes </a></li>
												 <li><a href="cat?cat=nbawm-ikvzp_st!_Men">Espadrilles</a></li>
												 <li><a href="cat?cat=nbawm-tyfcl_st!_Men">Loafers</a></li>
												 <li><a href="cat?cat=nbawm-cseyg_st!_Men">Monk Strap Shoes </a></li>
												 <li><a href="cat?cat=nbawm-hzvim_st!_Men">Oxford Shoes</a></li> 
												 <li><a href="cat?cat=nbawm-krhib_st!_Men">Slippers </a></li>
												 <li><a href="cat?cat=nbawm-gmyjl_st!_Men">Sandals</a></li>
												 <li><a href="cat?cat=nbawm-rewil_st!_Men">Sneakers</a></li>
					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 <li class="subMenuBtn" style="display: none"><a href="javascript:void(0)">Home & Tech <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Home & Tech</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Apple</strong></p></li>
												 <li><a href="cat?cat=njchx-ckmvp_st!_">Laptop</a></li>
												 <li><a href="cat?cat=njchx-syodq_st!_">iPhone</a></li>
												 <li><a href="cat?cat=njchx-cjwrx_st!_">iPad</a></li>
												 <li><a href="cat?cat=njchx-pmwzx_st!_">Accessories</a></li>
					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 
						 <li class="subMenuBtn" style="display: none"><a href="javascript:void(0)">Kids <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Kids</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>BABY GIRLS (0–3 YEARS)</strong></p></li>
												 <li><a href="cat?cat=xkmiv-_st!_">All Baby Girls</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Rompers & Sleepsuits</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Dresses</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Tops</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Coats</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Accessories</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Shoes</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>BABY BOYS (0–3 YEARS)</strong></p></li>
												 <li><a href="cat?cat=xkmiv-_st!_">All Baby Boys</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Rompers & Sleepsuits</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">T-Shirts</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Coats</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Accessories</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Shoes</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>GIRLS (3–16 YEARS)</strong></p></li>
												 <li><a href="cat?cat=xkmiv-_st!_">All Girls</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Coats</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Dresses</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Tops</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Trousers</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Accessories</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Shoes</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>BOYS (3–16 YEARS)</strong></p></li>
												 <li><a href="cat?cat=xkmiv-_st!_">All Boys</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Coats</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">T-shirts</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Knitwear</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Trousers</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Accessories</a></li>
												 <li><a href="cat?cat=xkmiv-_st!_">Shoes</a></li>

					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 
						 <li class="subMenuBtn">
						 <a href="javascript:void(0)">Beauty <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Beauty</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>MEN GROOMING</strong></p></li>
												 <li><a href="cat?cat=bedsz-qpfel_st!_Men">Bath and Body </a></li>
												 <li><a href="cat?cat=bedsz-gfqra_st!_Men">Fragrance </a></li>
												 <li><a href="cat?cat=bedsz-dkoqc_st!_Men">Haircare Sets </a></li>
												 <li><a href="cat?cat=bedsz-voefn_st!_Men">Shave</a></li> 
												 <li><a href="cat?cat=bedsz-wmzkj_st!_Men">Skincare </a></li>
												 <li><a href="cat?cat=bedsz-wuvbm_st!_Men">Accessories </a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Women Beauty</strong></p></li>
												 <li><a href="cat?cat=bedsz-qpfel_st!_Women">Bath and Body</a></li>
												 <li><a href="cat?cat=bedsz-uzbqa_st!_Women">Beauty Sets</a></li>
												 <li><a href="cat?cat=bedsz-xzpdg_st!_Women">Candles</a></li>
												 <li><a href="cat?cat=bedsz-phbvo_st!_Women">Cosmetic Cases</a></li>
												 <li><a href="cat?cat=bedsz-gfqra_st!_Women">Fragrance</a></li>
												 <li><a href="cat?cat=bedsz-dkoqc_st!_Women">Haircare</a></li>
												 <li><a href="cat?cat=bedsz-gwtef_st!_Women">Handcare</a></li>
												 <li><a href="cat?cat=bedsz-srgfy_st!_Women">Makeup</a></li>
												 <li><a href="cat?cat=bedsz-wmzkj_st!_Women">Skincare</a></li>
												 <li><a href="cat?cat=bedsz-pkgix_st!_Women">Tools and Devices</a></li>
					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						 </li>
						 
						 <li class="subMenuBtn" style="display: none"><a href="javascript:void(0)">Jewellery & Watches <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Jewellery & Watches</p>
					 					</div>
										
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>MEN</strong></p></li>
												 <li><a href="cat?cat=zisqv-_st!_Men">Jewellery</a></li>
												 <li><a href="cat?cat=tzylx-_st!_Men">Watches</a></li>

					 						</ul>
										 </div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>WOMEN</strong></p></li>
												 <li><a href="cat?cat=zisqv-ctklh_st!_Women">Earrings</a></li>
												 <li><a href="cat?cat=zisqv-wvbly_st!_Women">Necklaces</a></li>
												 <li><a href="cat?cat=zisqv-lpvuo_st!_Women">Bracelets</a></li>
												 <li><a href="cat?cat=zisqv-whznl_st!_Women">Rings</a></li>
												 <li><a href="cat?cat=tzylx-xxyxx_st!_Women">Fine Watches</a></li>

					 						</ul>
					 					</div>
									
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>

						 
					 </ul>
					 	<p class="d-block d-lg-none">Information & Help</p>
    				<ul class="d-block d-lg-none">
    					<li class="subMenuBtn"><a href="javascript:void(0)">Our Stores<span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
						<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Our Stores</p>
					 					</div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><a href="javascript:;">Delhi</a></li>
												 <li><a href="javascript:;">Mumbai</a></li>
												 <li><a href="javascript:;">Ludhiana</a></li>
												 <li><a href="javascript:;">Hyderabad</a></li>
												 <li><a href="javascript:;">Kolkata</a></li>
					 						</ul>
										 </div>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						</li>
						<li class="subMenuBtn"><a href="javascript:void(0)">Customer Services<span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
						<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Customer Services</p>
					 					</div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><a href="javascript:;">FAQs</a></li>
												 <li><a href="javascript:;">Bquest Rewards</a></li>
												 <li><a href="javascript:;">Product Recalls</a></li>
												 <li><a href="javascript:;">Contact us</a></li>
					 						</ul>
										 </div>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
							 </li>
						<li class="subMenuBtn"><a href="javascript:void(0)">Shopping Online<span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
						<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Shopping Online</p>
					 					</div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><a href="javascript:;">Click & Collect</a></li>
												 <li><a href="javascript:;">Track Your Order</a></li>
												 <li><a href="javascript:;">Return Your Order</a></li>
												 <li><a href="javascript:;">Delivery</a></li>
												 <li><a href="javascript:;">Return</a></li>
												 <li><a href="javascript:;">Brand directory</a></li>
												 <li><a href="javascript:;">Return</a></li>
					 						</ul>
										 </div>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
							 </li>
						<li class="subMenuBtn"><a href="javascript:void(0)">About Us<span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
						<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>About Us</p>
					 					</div>
										 <div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><a href="javascript:;">Bquest India</a></li>
												 <li><a href="javascript:;">Group</a></li>
												 <li><a href="javascript:;">Careers</a></li>
												 <li><a href="javascript:;">Terms & Conditions</a></li>
												 <li><a href="javascript:;">Policy & Cookie Policies</a></li>
					 						</ul>
										 </div>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
							 </li>
    				</ul>
    				<p class="d-block d-lg-none">My Account</p>
    				<ul class="d-block d-lg-none">
    					<li class="subMenuBtn"> <a href="javascript:void(0)"><img src="img/user.png" alt=""> &nbsp;Sign in</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)"><img src="img/like.png" alt=""> &nbsp;Wish List</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)"><img src="img/country.svg"> &nbsp;GB / GBP £ <strong>| English</strong></a></li>
    				</ul>
    				<a class="d-block d-lg-none mb-3" href="index.html"> 
						<img src="img/logo.png" class="logoLsmart" alt="">
 					</a>
					 </div>
			 	</div>
			 </div>
			 </div>	
	</div>
	</div>
<!-- Header Ends -->

	</div>
</header>