<div class="information5 p-2 p-lg-5 py-md-5">
					<div class="addCartDiv borderBottomGray pb-4">
						<?php 
						if(!empty($cart_data)){
						for($i = 0; $i < count($cart_data); $i++){ ?>
						<div class="row align-items-center mb-4 no-gutters">
							<div class="col-2">
								<div class="addCartImg">
									<img src="<?php echo PRODUCT_IMAGE_URL.checkImage(getSinglevalue('product_images', 'product', $cart_data[$i]['product'], 2)); ?>" alt="" class="w-100">
								</div>
							</div>
							<div class="col-7">
								<div class="addCartCont pl-4">
									<h6><strong><?php echo strtoupper(getSinglevalue('product', 'sku', $cart_data[$i]['product'], 4)) ?></strong></h6>
									<p><?php 
									$size = getSinglevalue('product_attributes', 'p_id', $cart_data[$i]['product'], 5);
									if(!empty($size)){
									echo strtoupper($size.getSinglevalue('product_attributes', 'p_id', $cart_data[$i]['product'], 10));
									}
									?></p>
								</div>
							</div>
							<div class="col-3">
								<div class="addCartPrice text-right">
									<p><img src="img/rupee.svg" alt=""> <?php 
								$finalp = getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10) * $cart_data[$i]['quantity'];
								echo str_replace('INR', '',money_format('%i', $finalp)) ?></p>
								</div>
							</div>
						</div>
						<?php } } ?>

					</div>
					<div class="borderBottomGray pb-2 pt-3">
						<div class="row justify-content-space-between mb-2">
							<div class="col-md-6">
								<div class="subTotal">
									<p class="mb-0">Subtotal</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="subTotal2 img12 float-right">
									<p class="mb-0"><img src="img/rupee.svg" alt=""> <?php echo str_replace('INR', '',money_format('%i', $totalcartamount)) ?></p>
								</div>
							</div>
						</div>
						<div class="row justify-content-space-between mb-2">
							<div class="col-md-6">
								<div class="subTotal">
									<p class="mb-0">Shipping</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="subTotal2 img12 float-right">
									<p class="mb-0">free</p>
								</div>
							</div>
						</div>
					</div>
					<div class="pt-3">
						<div class="row justify-content-space-between mb-2">
							<div class="col-md-6">
								<div class="subTotal">
									<p class="mb-0">Total</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="subTotal2 img12 float-right">
									<p class="mb-0"><img src="img/rupee.svg" alt=""> <?php echo str_replace('INR', '',money_format('%i', $totalcartamount)) ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>