<div class="addressFormBorder">
<div class="titleDiv">
										        		<p>Title</p>
										        		<select name="title" required>
														<?php 
															$titlearray = ["Mr", "Mrs", "Miss", "Ms", "Dr"];
															foreach($titlearray as $k => $value){ ?>
															<option value="<?php echo $value ?>"><?php echo $value ?></option>	
														<?php }
														?>
														</select>
										        	</div>
													<div class="addressInputDiv">
										        		<input type="text" placeholder="First name*" name="name" required>	
										        	</div>
										        	<div class="addressInputDiv">
										        		<input type="text" placeholder="Last name*" name="last_name" required>	
										        	</div>

										        	<div class="addressInputDiv">
										        		<div class="row">
										        			<div class="col-3">
										        				<p>+91</p>
										        			</div>
										        			<div class="col-9">
										        				<input type="text" placeholder="Mobile number*" name="mobile" required>
										        			</div>
										        		</div>	
										        	</div>
										        	<div class="addressInputDiv">
										        		<input type="text" placeholder="Address line 1*" name="add_line_1" required>	
										        	</div>
										        	<div class="addressInputDiv">
										        		<input type="text" placeholder="Address line 2" name="add_line_2">	
										        	</div>
										        	<div class="addressInputDiv">
										        		<input type="text" placeholder="Town/city*" name="city" required>	
										        	</div>
										        	<div class="addressInputDiv">

										        		<select class="state" name="state" required>
														<option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Lakshadweep">Lakshadweep</option><option value="Puducherry">Puducherry</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option>
														</select>	
										        	</div>
										        	<div class="addressInputDiv">
										        		<input type="text" placeholder="Postcode*" name="pincode" required>	
										        	</div>
</div>