<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="x-ua-compatible" content="ie=edge"/>
  <title>Lsmart || Designer , Fashion , Accessories and more - Shop online at Lsmart.</title>
<meta name="description" content="">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body id="main-body">
<div class="loaderOuter" id="loaderOuter" style="display: none"><div class="categoryLoader"></div></div>
<header class="mobileMenu"	>
	<div class="upperHeader">
		<div class="container-fluid px-0">
			<div class="row no-gutters">
				<div class="col-lg-6 col-xl-8 offset-xl-2 offset-lg-3 d-flex align-items-center justify-content-center py-2 py-lg-0">
					<div class="upperHeaderContent">
						<p>Free india standard deliveries</p>
					</div>
				</div>
				<div class="col-lg-3 col-xl-2 d-lg-block d-none">
					<div class="upperHeaderSideContent text-right">
						<p><a href="javascript:void(0)"><b>Stores</b></a></p>
						<p><a href="javascript:void(0)"><b>Services</b></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="middleHeader py-2">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-lg-3 col-4 d-flex align-items-center">
					<div class="translation-country">
						<div class="menuBar d-lg-none d-block">
							<img src="img/arrow/bars.svg" class="menuClick">
						</div>
				       <figure class="mb-0 d-lg-block d-none">  
				        <figcaption> 
						<p class="mt-0 mb-0"><a href="#"><img src="img/country.png" class="mr-1"> INR <strong>| English</strong></a></p>  
				        </figcaption> 
				       </figure> 
				    </div>
				</div>
				<div class="col-lg-6 col-4 d-flex align-items-center justify-content-center">
					<a href="index.php"> 
						<img src="img/logo.png" class="logoLsmart" alt="" title="">
 					</a>
				</div>
				<div class="col-lg-3 col-4 d-flex align-items-center">
					<div class="upperMiddleSideContent w-100">
						<ul>
							<li class="d-lg-block d-none mt-1" style="position: relative;"><a href="javascript:void(0)">
								<?php if(!empty($id) && !empty($usertoken)){ ?>
								<span style="left: -16px; top: 17px; width: 100px; padding:5px 0; overflow: hidden;"><strong><?php echo $name ?></strong></span>
								<?php } ?>
								<img src="img/user.png" alt=""></a>
								<div class="menuPopup">
									<ul>
								
										<?php if(empty($id) && empty($usertoken)){ ?>
										<li><a href="login">Sign in or Register</a></li>
										<?php }else{ ?>
										<li class="active"><a href="myaccount">My Account</a></li>
										<li><a href="address-book">Addresses</a></li>
										<li><a href="order-return">Orders & Returns</a></li>
								
										<li><a href="logout">Logout</a></li>
										<?php } ?>
									</ul>
								</div>
							</li>
							<li class="d-lg-block d-none mt-1 wishlistCount"><a href="wishlist"><img src="img/like.png" alt=""><p><?php echo $totalwishquantity ?></p></a></li>
							<li class="mt-1"><a class="position-relative" href="shopping-cart"><img src="img/cart.png" alt=""><span><?php echo $totalcartquantity ?></span></a></li>
							<li class="d-md-block d-none"><a href="javascript:void(0)">
								<form>
									<div class="input-group inputSearch">
										<input type="text" class="searchInput" placeholder="search Lsmart" name="">
										<div class="input-group-append">
											<img src="img/search.svg" alt="">
										</div>
									</div>
								</form>
							</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 d-block d-md-none">
						<form>
									<div class="input-group inputSearch2">
										<input type="text" class="searchInput" placeholder="search Lsmart" name="">
										<div class="input-group-append">
											<img src="img/search.svg" alt="">
										</div>
									</div>
								</form>
					</div>
			</div>
		</div>
	</div>
	<div class="lowerHeader">
			 <div class="container">
			 	<div class="row">
			 		<div class="sidebarContent w-100">
			 			<p class="d-block d-lg-none">Shop</p>
			 		<ul class="d-block d-lg-flex w-100 mb-0">

					 	<li class="subMenuBtn onHoverImgScale"><a href="javascript:void(0)">Store <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni storeDiv">
					 				<div class="container">
					 				<div class="row">
									 <div class="col-lg-5 col-sm-4 col-12">
									 	<div class="subMenuInn">
					 						<ul class="pt-5">
												<li data-type="DELHI, HARYANA, PUNJAB " data-img="img/iworld.png">
												   <a href="javascript:void(0)">IWORLD</a>
												</li>
												<li data-type="" data-img="img/bang-olufsen.png">
												   <a href="javascript:void(0)">BANG & OLUFSEN</a>
												</li>
												<li data-type="DELHI : DLF Emporio Mall, Vasant Kunj" data-img="img/Molton.png">
												   <a href="javascript:void(0)">MOLTON BROWN</a>
												</li>
												<li data-type="DELHI : The Chanakya Mall, Chanakyapuri " data-img="img/creed.png">
												   <a href="javascript:void(0)">CREED PERFUMES</a>
												</li>
												<li data-type="DELHI : The Chanakya Mall, Chanakyapuri " data-img="img/YSL.png">
												   <a href="javascript:void(0)">SANT LAURENT</a>
												</li>
												<li data-type="DELHI : The Chanakya Mall, Chanakyapuri " data-img="img/Brunello.png">
												   <a href="javascript:void(0)">BRUNELLO CUCINELLI</a>
												</li>
												<li data-type="DELHI : DLF Emporio Mall, Vasant Kunj " data-img="img/Berluti.png">
												   <a href="javascript:void(0)">BERLUTI</a>
												</li>
												<li data-type="HYDERABAD, KOLKATA " data-img="img/iworld.png">
												   <a href="javascript:void(0)">TODS</a>
												</li>
												<li data-type="DELHI : Ground floor, DLF EMPORIO " data-img="img/Roberto-Cavalli.png">
												   <a href="javascript:void(0)">ROBERTO CAVALLI</a>
												</li>
												<li data-type="DELHI: 1ST FLOOR , DLF EMPORIO " data-img="img/missoni.png">
												   <a href="javascript:void(0)">MISSONI</a>
												</li>
												<li data-type="DELHI : Ground floor, DLF EMPORIO " data-img="img/Tom-Ford.png">
												   <a href="javascript:void(0)">TOM FORD</a>
												</li>
												<li data-type="DELHI: 1ST FLOOR , DLF EMPORIO " data-img="img/Corniliani.png">
												   <a href="javascript:void(0)">CORNELIANI</a>
												</li>
					 						</ul>
					 					</div>
									 </div>
									 <div class="col-lg-7 col-sm-8 col-12">
									 	<div class="subMenuimg mt-5">
					 						<div class="w-100 subMenuImgOverflow">
					 							<img class="img-fluid" src="img/iworld.png" alt="">
					 						</div>
											 <div class ="submenuAddress">
											 	<h2>DELHI, HARYANA, PUNJAB</h2>
												 <div class="submenuLine1"></div>
												 <div class="submenuLine2"></div>
												 <div class="submenuLine3"></div>
												 <div class="submenuLine4"></div>
											 </div>
					 					</div>
									 </div>
					 					
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>
						 <li class="subMenuBtn"><a href="javascript:void(0)">Designers <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row multipleMenuDiv">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Designers</p>
					 					</div>
					 					<!-- brands for women -->
												 <?php
													//  $menbq = mysqli_query($conn, "SELECT DISTINCT(t1.brand) as brand FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t2.gender='Women'");
							
													//  if($menbq){
													// 	 if(mysqli_num_rows($menbq) > 0){ 
															 ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Women</strong></p></li>
												 <li><a href="">All Women Brands</a></li>
												 <li><a href="">Saint Laurent</a></li>
												 <li><a href="">Alexander McQueen</a></li>
												 <li><a href="">Chloe</a></li>
												 <li><a href="">Tod's</a></li>
												 <li><a href="">Brunello Cucinelli</a></li>
												 <li><a href="">Tom Ford</a></li>
												 <li><a href="">Roberto Cavalli</a></li>
												 <li><a href="">Missoni</a></li>
												<?php // while($rows = mysqli_fetch_assoc($menbq)){ ?>
												<!-- <li><a href="brand?brand=<?php // echo getSinglevalue('brands', 'brand_key', $rows['brand'], 3); ?>_Women_"><?php // echo getSinglevalue('brands', 'brand_key', $rows['brand'], 3); ?></a></li> -->
												<?php // } ?>
					 						</ul>
					 					</div>
												<?php	 
												// }
												// 	 }else{
												// 		 echo mysqli_error($conn);
												// 	 }
												 ?>
												 <!-- Brands for men -->

												 <?php
													//  $menbq = mysqli_query($conn, "SELECT DISTINCT(t1.brand) as brand FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t2.gender='Men'");
													//  if($menbq){
													// 	 if(mysqli_num_rows($menbq) > 0){ 
															 ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Men</strong></p></li>
												 <li><a href="">All Men Brands</a></li>
												 <li><a href="">Berluti</a></li>
												 <li><a href="">Brunello Cucinelli</a></li>
												 <li><a href="">Tod's</a></li>
												 <li><a href="">Tom Ford</a></li>
												 <li><a href="">Brioni</a></li>
												 <li><a href="">Alexander McQueen</a></li>
												 <li><a href="">Corneliani</a></li>
												 <li><a href="">Gucci</a></li>
												 <li><a href="">Off-White</a></li>
												 <li><a href="">Balmain</a></li>
												<?php // while($rows = mysqli_fetch_assoc($menbq)){ ?>
												<!-- <li><a href="brand?brand=<?php // echo getSinglevalue('brands', 'brand_key', $rows['brand'], 3); ?>_Men_"><?php // echo getSinglevalue('brands', 'brand_key', $rows['brand'], 3); ?></a></li> -->
												<?php // } ?>
					 						</ul>
					 					</div>
												<?php	
												//  }
												// 	 }
												 ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Accessories</strong></p></li>
												 <li><a href="">Bottega Veneta</a></li>
												 <li><a href="">Dolce & Gabbana</a></li>
												 <li><a href="">Loewe</a></li>
												 <li><a href="">Saint Laurent</a></li>
					 						</ul>
					 					</div>
									
										<?php
											// $pdcategory = mysqli_query($conn, "SELECT DISTINCT(category) as category FROM product WHERE publish=1 AND category!=''");
											// if($pdcategory){
											// 	if(mysqli_num_rows($pdcategory) > 0){
											// 		while($rows = mysqli_fetch_assoc($pdcategory)){
											// 			$bdcat = $rows['category']; 
														
														?>
													<!-- <div class="subMenuInn">
					 									<ul class="mb-4">
											 				<li><p><strong><?php // echo getSinglevalue('category', 'cat_key', $bdcat, 2) ?></strong></p></li>
															 <?php 
																//  $sql = mysqli_query($conn, "SELECT DISTINCT(brand) as brand FROM product WHERE publish=1 AND category='$bdcat'");
																//  if($sql){
																// 	 if(mysqli_num_rows($sql) > 0){
																// 		 while($rw = mysqli_fetch_assoc($sql)){ 
																			 ?>
																			<li><a href="brand?brand=<?php // echo getSinglevalue('brands', 'brand_key', $rw['brand'], 3); ?>__<?php // echo $bdcat ?>"><?php // echo getSinglevalue('brands', 'brand_key', $rw['brand'], 3); ?></a></li>
															<?php 		 
															// }
															// 		 }
															// 	 }
															 ?>
														</ul>
													</div> -->
										<?php		
										// }
										// 		}
										// 	}
										?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												 <li><p><strong>Accessories</strong></p></li>
												 <li><a href="">Bottega Veneta</a></li>
												 <li><a href="">Dolce & Gabbana</a></li>
												 <li><a href="">Loewe</a></li>
												 <li><a href="">Saint Laurent</a></li>
					 						</ul>
					 					</div>

					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>
					 	<li class="subMenuBtn"><a href="javascript:void(0)">Women <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Women</p>
					 					</div>
										 <?php 
											 $sql = mysqli_query($conn, "SELECT DISTINCT(t1.category) as category FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t2.gender='Women'");
											 if($sql){
												 if(mysqli_num_rows($sql) > 0){
													 while($rows = mysqli_fetch_assoc($sql)){
														echo $wcat = $rows['category'];
														 ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												<li><a href="cat?cat=<?php echo $wcat ?>-_st!_Women"><strong><?php echo getSinglevalue('category', 'cat_key', $wcat, 2); ?></strong></a></li>
												<?php 
													$nextsql = mysqli_query($conn, "SELECT DISTINCT(t1.subcategory) as subcategory FROM product as t1 join product_attributes as t2 on t1.sku=t2.P_id WHERE t1.publish=1 AND t2.gender='Women' AND t1.category='$wcat'");
													if($nextsql){
														if(mysqli_num_rows($nextsql) > 0){
															while($subrows = mysqli_fetch_assoc($nextsql)){ ?>
												<li><a href="cat?cat=<?php echo $wcat ?>-<?php echo $subrows['subcategory'] ?>_st!_Women"><?php echo getSinglevalue('subcategory', 'subcat_key', $subrows['subcategory'], 3); ?></a></li>
												<?php		}
														}
													}else{
														echo mysqli_error($conn);
													}
												?>
					 						</ul>
					 					</div>
										<?php		 }
												 }
											 }else{
												 echo mysqli_error($conn);
											 }
										 ?>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>

						 <li class="subMenuBtn"><a href="javascript:void(0)">Men <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					 		<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>Men</p>
					 					</div>
										 <?php 
											 $sql = mysqli_query($conn, "SELECT DISTINCT(t1.category) as category FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t2.gender='Men'");
											 if($sql){
												 if(mysqli_num_rows($sql) > 0){
													 while($rows = mysqli_fetch_assoc($sql)){
														 $wcat = $rows['category'];
														 ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												<li><a href="cat?cat=<?php echo $wcat ?>-_st!_Men"><strong><?php echo getSinglevalue('category', 'cat_key', $wcat, 2); ?></strong></a></li>
												<?php 
													$nextsql = mysqli_query($conn, "SELECT DISTINCT(t1.subcategory) as subcategory FROM product as t1 join product_attributes as t2 on t1.sku=t2.P_id WHERE t1.publish=1 AND t2.gender='Men' AND t1.category='$wcat'");
													if($nextsql){
														if(mysqli_num_rows($nextsql) > 0){
															while($subrows = mysqli_fetch_assoc($nextsql)){ ?>
												<li><a href="cat?cat=<?php echo $wcat ?>-<?php echo $subrows['subcategory'] ?>_st!_Men"><?php echo getSinglevalue('subcategory', 'subcat_key', $subrows['subcategory'], 3); ?></a></li>
												<?php		}
														}
													}
												?>
					 						</ul>
					 					</div>
										<?php		 }
												 }
											 }
										 ?>
					 				</div>
					 			</div>
					 			</div>
					 		</div>
					 	</li>

						 <?php 
                            $query = mysqli_query($conn, "SELECT * FROM menu WHERE status=1 ORDER BY menu_id ASC LIMIT 8");
                            if($query){
                                if(mysqli_num_rows($query) > 0){
                                    while($menuarray = mysqli_fetch_array($query)){ ?>
						<li class="subMenuBtn"><a href="cat?cat=<?php echo $menuarray[2] ?>-_st!_"><?php echo $menuarray[1] ?> <span class="d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
							<div class="subMenu">
					 			<div class="submenuAni">
					 				<div class="container">
					 				<div class="row">
					 					<div class="backToMenu d-block d-lg-none">
					 						<p><span><i class="fa fa-angle-left" aria-hidden="true"></i></span><?php echo $menuarray[1] ?></p>
					 					</div>
										 <!-- Reserved for status tags starts -->
										 <?php 
                                        $sql = mysqli_query($conn, "SELECT DISTINCT(status) FROM product WHERE publish=1 AND category='$menuarray[2]' AND status!=''");
                                        if($sql){
                                            if(mysqli_num_rows($sql) > 0){ ?>
										<div class="subMenuInn">
					 						<ul class="mb-4">
												<?php while($statuss = mysqli_fetch_assoc($sql)){ ?>
					 							<li><a href="cat?cat=<?php echo $menuarray[2] ?>-_st!<?php echo $statuss['status'] ?>_"><strong><?php echo getSinglevalue('color', 'color_id', $statuss['status'], 1); ?></strong></a></li>
												 <?php } ?>
											</ul>
					 					</div>

                                        <?php } }?>
										 <!-- Reserved for status tags ends -->
										 <!-- Reserved For All Submenus starts -->
										 <?php
											$sql = mysqli_query($conn, "SELECT DISTINCT(subcategory) FROM product WHERE publish=1 AND category='$menuarray[2]'");
											$submenucount = mysqli_num_rows($sql);
                                            if($submenucount > 0){
											$num = 0;
											$submenu = [];
											?>
										<?php
											while($subarray = mysqli_fetch_array($sql)){
												$submenu[] = $subarray;
											}
											// print_r($submenu);
											$submenu_count = count($submenu);
											$ar_count = $submenu_count / 10;
											$first_loop_time = ceil($ar_count);
											for ($i=0; $i <= $first_loop_time - 1 ; $i++) {
												if($i > 0){
													$second_loop_index = $i * 10;
												}else{
													$second_loop_index = $i ;
												}
												$second_array_length = $second_loop_index + 9;
												$second_loop_time = ($submenu_count > $second_array_length) ? $second_array_length : $submenu_count - 1 ;
												?>
												<div class="subMenuInn">
												<ul>
												<?php
													for ($j=$second_loop_index; $j <= $second_loop_time ; $j++) { ?>
														<li><a href="cat?cat=<?php echo $menuarray[2] ?>-<?php echo $submenu[$j][0] ?>_st!_"><?php echo getSinglevalue('subcategory', 'subcat_key', $submenu[$j][0], 3); ?> </a></li>
												<?php } ?>
												</ul>
												</div>
										<?php	}
										?>
											
										<?php  }  ?>
										 <!-- Reserved For All Submenus Ends -->
										 <!-- Reserved For All Brands Starts -->
										 <?php 
										$sql = mysqli_query($conn, "SELECT DISTINCT(brand) as brand FROM product WHERE visible='1' AND category='$menuarray[2]' AND brand!=''");
										if($sql){
											if(mysqli_num_rows($sql) > 0){ ?>
					 					<div class="subMenuInn">
					 						<ul>
											 	<li><a href="#"><strong>Shop By Brand</strong></a></li>
												 <?php while($brands = mysqli_fetch_assoc($sql)){ ?>
												<li><a href="brand?brand=<?php echo getSinglevalue('brands', 'brand_key', $brands['brand'], 3)."__".$menuarray[2] ?>"><?php echo getSinglevalue('brands', 'brand_key', $brands['brand'], 3) ?></a></li>
												<?php } ?>
					 						</ul>
					 					</div>
										 <?php } } ?>
										 <!-- Reserved For All Brands Ends -->
					 				</div>
					 			</div>
					 			</div>
					 		</div>
						</li>
						<?php } } } ?>
					 </ul>
					 	<p class="d-block d-lg-none">Information & Help</p>
    				<ul class="d-block d-lg-none">
    					<li class="subMenuBtn"><a href="javascript:void(0)">Our Stores & Events</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)">The Cinema at Lsmart</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)">Appointments & Services</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)">24/7 Customers Services</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)">Delivery, Returns & Refunds</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)">About Us</a></li>
    				</ul>
    				<p class="d-block d-lg-none">My Account</p>
    				<ul class="d-block d-lg-none">
    					<li class="subMenuBtn"> <a href="javascript:void(0)"><img src="img/user.png" alt=""></a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)"><img src="img/like.png" alt=""> Wish List</a></li>
						<li class="subMenuBtn"><a href="javascript:void(0)"><img src="img/country.svg"> GB / GBP £ <strong>| English</strong></a></li>
    				</ul>
    				<a class="d-block d-lg-none mb-3" href="index.html"> 
						<img src="img/logo.png" class="logoLsmart" alt="">
 					</a>
					 </div>
			 	</div>
			 </div>
	</div>
<!-- Header Ends -->
<!-- <div class="richText-content text-center py-1 mb-1">       
		<h2><strong>We are open for business</strong></h2>
	</div> -->

</header>