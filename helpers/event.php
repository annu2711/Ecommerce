<?php
ob_start();
session_start();
include_once "../lsmartbk/helpers/config.php";
include_once "../lsmartbk/helpers/function.php";
include_once "shiprocket.php";

if(isset($_POST['title'])){
    extract($_POST);
    if(!empty($title) && !empty($name) && !empty($mobile) && !empty($addresss) && !empty($city) && !empty($state) && !empty($pincode))
    {
        $status = verifyAddress($pincode);
        // print_r($status);
        if(array_key_exists("message", $status)){
            echo 401;
        }
        if(array_key_exists("success", $status)){
            $postcode_state = $status["postcode_details"]['state'];
            $postcode_city = $status["postcode_details"]['city'];
            $pass = true;
            if($pass == true && $postcode_state !== $state){
                $pass = false;
            }
            if($pass){
                $city = $postcode_city;
                $id = $_SESSION['user'];
                if(empty($addtype)){
                $key = rand_char(5);
                try{
                    $stmt = $conn->prepare("INSERT INTO `address_book`(`ab_key`, `user`, `title`, `name`, `last_name`, `mobile`, `address`, `town`, `state`, `pincode`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param('sssssssssss', $key, $id, $title, $name, $lname, $mobile, $addresss, $city, $state, $pincode, $created_at);
                    if($stmt->execute()){
                        $_SESSION['result'] = [true, 'Address Added Successfully', 'success'];
                        echo 200;
                    }else{
                        echo "Error: " . $e->getMessage();
                        throw new exception($conn->error);
                    }
                  }catch(Exception $e){
                    echo 403;
                  }
                }else{
                    if(!empty($ab_key)){
                    try{
                        $stmt = $conn->prepare("UPDATE address_book SET title=?, name=?, last_name=?, mobile=?, address=?, town=?, state=?, pincode=? WHERE ab_id=? AND user=?");
                        $stmt->bind_param('ssssssssss', $title, $name, $lname, $mobile, $addresss, $city, $state, $pincode, $ab_key, $id);
                        if($stmt->execute()){
                            $_SESSION['result'] = [true, 'Address Updated Successfully', 'success'];
                            echo 200;
                        }else{
                          throw new exception($conn->error);
                        }
                      }catch(Exception $e){
                          echo "Error: " . $e->getMessage();
                          echo 403;
                      }
                    }else{
                        echo 400;
                    }
                }

            }else{
                echo 403;
            }
        }
    }else{
        echo "empty Fields";
    }
}

if(isset($_POST['address'])){
    extract($_POST);
    $addressdata = "";
    if(!empty($address)){
        $sql = mysqli_query($conn, "SELECT * FROM address_book WHERE ab_key='$address' ORDER BY ab_id DESC LIMIT 1");
        if($sql){
            if(mysqli_num_rows($sql) > 0){
                $row = mysqli_fetch_array($sql);
                $addressdata = $row;
            }
        }
    }
    echo json_encode($addressdata);
}