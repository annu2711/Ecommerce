<?php 
    include_once "helpers/index.php";
    $edit = "";
    $color = "";
    $size = "";
    $sample = "";
    $gender = "";
    if(isset($_GET['product'])){
        extract($_GET);
        $query = mysqli_query($conn, "SELECT * FROM product WHERE p_id='$product'");
        if($query){
            if(mysqli_num_rows($query) > 0){
                $array = mysqli_fetch_array($query);
                $edit = 1;
            }else{
                header('location: product-list');
            }
        }
    }

?>
<style>
.imagePreview {
    width: 100%;
    height: 180px;
    background-position: center center;
  background:url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
  background-color:#fff;
    background-size: cover;
  background-repeat:no-repeat;
    display: inline-block;
  box-shadow:0px -3px 6px 2px rgba(0,0,0,0.2);
}
.btn-primary
{
  display:block;
  border-radius:0px;
  box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
  margin-top:-5px;
}
.imgUp
{
  margin-bottom:15px;
}
.del
{
  position:absolute;
  top:0px;
  right:15px;
  width:30px;
  height:30px;
  text-align:center;
  line-height:30px;
  background-color:rgba(255,255,255,0.6);
  cursor:pointer;
}
.imgAdd, .imgAddd
{
  width:30px;
  height:30px;
  border-radius:50%;
  background-color:#4bd7ef;
  color:#fff;
  box-shadow:0px 0px 2px 1px rgba(0,0,0,0.2);
  text-align:center;
  line-height:30px;
  margin-top:0px;
  cursor:pointer;
  font-size:15px;
}
    </style>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2> <?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Masters</a></li>
                    <li class="breadcrumb-item active"><?php echo (!empty($edit)) ? 'Update' : 'Add' ; ?> Product</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Validation -->
        <!-- Advanced Validation -->
        <form id="form_advanced_validation" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">
        <?php if(empty($edit)){ ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <h5 class="header">Add Attribute</h5>
                    <div class="body">
                        <div class="row">
                        <?php // echo $rows['name'] ?>
                            <?php 
                                $sql = mysqli_query($conn, "SELECT * FROM attribute_tbl");
                                if($sql){
                                    if(mysqli_num_rows($sql) > 0){
                                        while($rows = mysqli_fetch_assoc($sql)){ ?>
                                        <div class="col-md-3 mt-2">
                                            
                                                
                                            
                                            <lavel onclick="addAttribute('<?php echo $rows['name'] ?>')">
                                            
                                            <input type="checkbox" name="attribute[]" value="<?php echo $rows['name'] ?>" id="<?php echo $rows['name'] ?>"> 
                                            <?php if($rows['name'] == 'GENDER'){
                                                echo 'Suitability';
                                                ?>
                                                <?php }else{ ?>
                                            <?php echo $rows['name'] ?>
                                            <?php } ?>
                                            </label>
                                        </div>
                            <?php          }
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                        <?php 
                                if(!empty($edit)){ ?>
                                        <input type="hidden" class="form-control" name="type" value="1">
                                        <input type="hidden" class="form-control" name="groupid" value="<?php echo $array[20] ?>">
                            <?php    }
                            ?>
                            <div class="form-group col-md-6">
                                <label>Store<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="store" id="store" required>
                                    <option value="">-- Select Store --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[2] : '';
                                    display_option_selected('store', 1, 2, $value) ?>
                                </select>
                            </div>
                            
                            <div class="form-group col-md-6">
                                <div id="brand">
                                <label>Brand<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="brand" style="padding: 0px !important;" required>
                                    <option value="">-- Select Brand --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[3] : '';
                                    display_option_selected('brands', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Product Title<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="title" <?php echo (!empty($edit)) ? 'value="'.$array[4].'"' : 'placeholder="Enter Product Title"'; ?> required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>SKU Code<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="code" <?php echo (!empty($edit)) ? 'value="'.$array[5].'"' : 'placeholder="Enter Code"'; ?> required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Collection Tag<?php echo $value = (!empty($edit)) ? $array[22] : ''; ?><sup class="text-danger">*</sup></label>
                                <select class="form-control" name="collection_tag" required>
                                    <option value="">-- Select Collection Tag --</option>
                                    
                                    <option value="PE" <?php echo ($value == 'PE') ? 'selected="selected"' : ''; ?>>PE</option>
                                    <option value="SE" <?php echo ($value == 'SE') ? 'selected="selected"' : ''; ?>>SE</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Order Start Date</label>
                                <input type="date" class="form-control" name="order_start_date" <?php echo (!empty($edit)) ? 'value="'.$array[23].'"' : ''; ?>>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Line of Business</label>
                                <input type="text" class="form-control" name="line_of_business" value="<?php echo (!empty($edit)) ? $array[24] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label>UPC or VPN Code<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="vpn" value="<?php echo (!empty($edit)) ? $array[25] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Style Name</label>
                                <input type="text" class="form-control" name="style_code" value="<?php echo (!empty($edit)) ? $array[26] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Serial No<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="serial_no" value="<?php echo (!empty($edit)) ? $array[27] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Color Code</label>
                                <input type="text" class="form-control" name="color_code" value="<?php echo (!empty($edit)) ? $array[28] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Material</label>
                                <input type="text" class="form-control" name="material" value="<?php echo (!empty($edit)) ? $array[29] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Fit</label>
                                <input type="text" class="form-control" name="fit" value="<?php echo (!empty($edit)) ? $array[30] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Batch No<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="batch_no" value="<?php echo (!empty($edit)) ? $array[31] : ''; ?>" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Minimum Sale Qty</label>
                                <input type="text" class="form-control" name="moq" value="<?php echo (!empty($edit)) ? $array[32] : ''; ?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Category<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="category" id="category" required>
                                    <option value="">-- Select Category --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[6] : '';
                                    display_option_selected('category', 1, 2, $value) ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <div id="subcategory">
                                <label>Subcategory<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="subcategory" style="padding: 0px !important;" required>
                                    <option value="">-- Select Subcategory --</option>
                                    <?php 
                                    $value = (!empty($edit)) ? $array[7] : '';
                                    display_option_selected('subcategory', 1, 3, $value) ?>
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Additional Category</label>
                                <input type="text" class="form-control" name="add_cat" <?php echo (!empty($edit)) ? 'value="'.$array[8].'"' : 'placeholder="Enter Additional Category"'; ?>>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Editor's Note<sup class="text-danger">*</sup></label>
                                <textarea class="form-control editor" name="specification" required>
                                <?php echo (!empty($edit)) ? $array[11] : ''; ?>
                                </textarea>
                                <hr>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Size & Fit<sup classs="text-danger">*</sup></label>
                                <textarea class="form-control editor" name="description" >
                                <?php echo (!empty($edit)) ? $array[12] : ''; ?>
                                </textarea>
                                <hr>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Technical Specification</label>
                                <textarea class="form-control editor" name="features">
                                <?php echo (!empty($edit)) ? $array[13] : ''; ?>
                                </textarea>
                                <hr>
                            </div>

                            <div class="form-group col-md-3">
                                <label>MRP</label>
                                <input type="text" class="form-control" name="mrp" <?php echo (!empty($edit)) ? 'value="'.$array[9].'"' : 'placeholder="Enter MRP"'; ?>>
                            </div>

                            <div class="form-group col-md-3">
                                <label>Price<sup class="text-danger">*</sup></label>
                                <input type="number" class="form-control" name="price" <?php echo (!empty($edit)) ? 'value="'.$array[10].'"' : 'placeholder="Enter Price"'; ?> required>
                            </div>

                            <div class="col-md-3">
                                    <label>Reviews<sup class="text-danger">*</sup></label>
                                    <select class="form-control" name="review" required>
                                        <option value="">-- Select --</option>
                                        <option value="0" <?php 
                                        if(!empty($edit)){
                                            if($array[14] == 0){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>Enable</option>
                                        <option value="1"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[14] == 1){
                                                echo 'selected="selected"';
                                            }
                                        } ?>
                                        >Disable</option>
                                    </select>
                            </div>

                            <div class="col-md-3">
                                    <label>Publish<sup class="text-danger">*</sup></label>
                                    <select class="form-control" name="publish" required>
                                        <option value="">-- Select --</option>
                                        <option value="0" <?php 
                                        if(!empty($edit)){
                                            if($array[15] == 0){
                                                echo 'selected="selected"';
                                            }
                                        } ?>>No</option>
                                        <option value="1"
                                        <?php 
                                        if(!empty($edit)){
                                            if($array[15] == 1){
                                                echo 'selected="selected"';
                                            }
                                        } ?>
                                        >Yes</option>
                                    </select>
                            </div>
                            <div class="col-md-3">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="">-- Select Status Tag --</option>
                                    <?php $statusvalue = (!empty($edit)) ? $array[16] : ''; ?>
                                    <?php display_option_selected('color', 0, 1, $statusvalue); ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label>Stock</label>
                                <input type="text" class="form-control" name="stock" value="<?php echo (!empty($edit)) ? $array[33] : ''; ?>">
                            </div>

                            <div class="col-md-3">
                                <label>Low Stock Quantity</label>
                                <input type="text" class="form-control" name="low_stock" value="<?php echo (!empty($edit)) ? $array[34] : ''; ?>">
                            </div>

                            <div class="col-md-12">
                                <hr>
                                <h3 class="mb-0"><strong>Attributes</strong></h3>
                                <?php 
                                    if(!empty($edit)){ ?>
                                        <div class="row mt-2">
                                            <?php 
                                                $sql = mysqli_query($conn, "SELECT * FROM product_attributes WHERE p_id='$array[5]'");
                                                if($sql){
                                                    if(mysqli_num_rows($sql) > 0){
                                                    $attrrows = mysqli_fetch_array($sql);
                                                             ?>
                                                           

                                                <?php if(!empty($attrrows[4])){ ?>
                                                    <div class="col-md-4">
                                                        <label><?php echo 'Color';  ?></label>
                                                        <input type="text" name="color" class="form-control" value="<?php echo $attrrows[4] ?>">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Color Sample</label>
                                                        <input type="file" class="form-control" name="clrsample" accept=".png, .jpeg, .jpg" <?php echo (empty($attrrows[8])) ? 'required' : ''; ?>>
                                                    </div>
                                                    <?php if(!empty($attrrows[8])){ ?>
                                                    <div class="col-md-4">
                                                        <input type="hidden" name="prev_sample" value="<?php echo $attrrows[8] ?>">
                                                        <a href="products/<?php echo $attrrows[8] ?>" target="_blank"><img src="products/<?php echo $attrrows[8] ?>" width="70px" height="70px"></a>
                                                    </div>
                                                <?php } 
                                            } ?>
                                                <?php if(!empty($attrrows[5])){ ?>
                                                    <div class="col-md-5">
                                                    <label><?php echo 'Size';  ?></label>
                                                    <input type="text" name="size" class="form-control" value="<?php echo $attrrows[5] ?>">
                                                    </div>
                                                <?php } ?>
                                                <?php if(!empty($attrrows[9])){ ?>
                                                    <div class="col-md-5">
                                                    <label><?php echo 'Gender';  ?></label>
                                                    <input type="text" name="gender" class="form-control" value="<?php echo $attrrows[9] ?>">
                                                    </div>
                                                <?php } ?>
                                                
                                                            <?php        //}
                                                                    }
                                                                }else{
                                                                    echo mysqli_error($conn);
                                                                }
                                                            ?>
                                                        </div>
                                <?php    }else{
                                ?>
                                <p class="text-danger no-attr">No Attributes Available. Please Add Some Attributes.</p>
                                <?php } ?>
                                <div class="row attr">
                                    
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3 class="mb-0"><strong>Add Images<sup class="text-danger">*</sup></strong></h3>
                                <p class="text-danger">First & second images will be use as thumbnail.</p>
                                <div class="row">
                                <div class="col-sm-2 imgUp">
                                    <div class="imagePreview"></div>
                                        <label class="btn btn-primary">Upload<input type="file" class="uploadFile img" name="file[]" style="width: 0px;height: 0px;overflow: hidden;" <?php echo (!empty($edit)) ? '' : 'required' ; ?>></label>
                                    </div><!-- col-2 -->
                                <span class="imgAdd">+</span>
                                </div>
                                <?php if(!empty($edit)){ ?>
                                    <div class="row">
                                        <?php 
                                            $imgsql = mysqli_query($conn, "SELECT * FROM product_images WHERE product='$array[5]'");
                                            if($imgsql){
                                                if(mysqli_num_rows($imgsql) > 0){
                                                    $vimagearr = [];
                                                    while($imgss = mysqli_fetch_array($imgsql)){
                                                        $vimagearr[] = $imgss;
                                                    }
                                                    $sno = 1;
                                                    ?>
                                        <table class="table table-bordered table-striped table-hover ">
                                            <tr>
                                                <th>S.No</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                            </tr>
                                            <?php for($i = 0; $i <= count($vimagearr) - 1; $i++){ ?>
                                                <tr>
                                                    <td><?php echo $sno++ ?></td>
                                                    <td><a href="products/<?php echo $vimagearr[$i]['image'] ?>" target="_blank"><img src="products/<?php echo $vimagearr[$i]['image'] ?>" width="100px" height="100px"></a></td>
                                                    <td><a href="delete.php?image=<?php echo $vimagearr[$i]['pi_id'] ?>" class="btn btn-danger">Delete</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <?php      }
                                            }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-12" align="center">
                                <?php if(empty($edit)){ ?>
                                <input type="submit" name="add_product" value="Save & Continue" class="btn btn-success">
                                <?php }else{ ?>
                                    <input type="submit" name="update_product" value="Update Product" class="btn btn-success">
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Product</strong> Variations</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12 col-lg-12">
                                <div class="panel-group full-body" id="accordion_5" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingOne_5">
                                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion_5" href="#collapseOne_5" aria-expanded="true" aria-controls="collapseOne_5">Add Product Variations </a> </h4>
                                        </div>
                                        <div id="collapseOne_5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_5">
                                            <div class="panel-body" style="background-color: transparent; color: #555;"> 
                                               <?php if(!empty($edit)){ ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Product SKU<sup class="text-danger">*</sup></label>
                                                        <input type="text" class="form-control" name="variationsku" value="" required>
                                                    </div>

                            <div class="form-group col-md-4">
                                <label>Collection Tag<sup class="text-danger">*</sup></label>
                                <select class="form-control" name="varcollection_tag" required>
                                    <option value="">-- Select Collection Tag --</option>
                                    <option value="PE">PE</option>
                                    <option value="SE">SE</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Order Start Date</label>
                                <input type="date" class="form-control" name="varorder_start_date">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Line of Business</label>
                                <input type="text" class="form-control" name="varline_of_business" value="">
                            </div>

                            <div class="form-group col-md-4">
                                <label>UPC or VPN Code<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="varvpn" value="" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Style Name</label>
                                <input type="text" class="form-control" name="varstyle_code" value="">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Serial No<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="varserial_no" value="" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Color Code</label>
                                <input type="text" class="form-control" name="varcolor_code" value="">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Material</label>
                                <input type="text" class="form-control" name="varmaterial" value="">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Fit</label>
                                <input type="text" class="form-control" name="varfit" value="">
                            </div>

                            <div class="form-group col-md-4">
                                <label>Batch No<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" name="varbatch_no" value="" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Minimum Sale Qty</label>
                                <input type="text" class="form-control" name="varmoq" value="">
                            </div>

                            <div class="col-md-3">
                                <label>Stock</label>
                                <input type="text" class="form-control" name="varstock" value="">
                            </div>

                            <div class="col-md-3">
                                <label>Low Stock Quantity</label>
                                <input type="text" class="form-control" name="varlow_stock" value="">
                            </div>

                                                    <div class="col-md-12">
                                                        <div class="row mt-2">
                                                        <?php 
                                                $sql = mysqli_query($conn, "SELECT * FROM product_attributes WHERE p_id='$array[5]'");
                                                if($sql){
                                                    if(mysqli_num_rows($sql) > 0){
                                                    $attrrows = mysqli_fetch_array($sql);
                                                             ?>
                                                           

                                                <?php if(!empty($attrrows[4])){ ?>
                                                    <div class="col-md-7">
                                                        <label class="color">Color</label>
                                                        <input type="text" name="varcolor" class="form-control color" value="">
                                                        <label onclick="hideDiv('color', 'atr_1')"><input type="checkbox" class="" id="atr_1"> SKIP & USE VERTUAL COLOR</label>
                                                    </div>
                                                    <div class="col-md-4 color">
                                                        <label>Color Sample</label>
                                                        <input type="file" class="form-control" name="varclrsample" accept=".png, .jpeg, .jpg">
                                                    </div>
                                                <?php  
                                            } ?>
                                                <?php if(!empty($attrrows[5])){ ?>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-7">
                                                    <label class="size">Size</label>
                                                    <input type="text" name="varsize" class="form-control size" value="">
                                                    <label onclick="hideDiv('size', 'atr_2')"><input type="checkbox" class="" id="atr_2"> SKIP & USE VERTUAL COLOR</label>
                                                    </div>
                                                <?php } ?>
                                                <?php if(!empty($attrrows[9])){ ?>
                                                    <div class="col-md-7">
                                                    <label class="gender">Gender</label>
                                                    <input type="text" name="vargender" class="form-control gender" value="<?php echo $attrrows[9] ?>">
                                                    <label onclick="hideDiv('gender', 'atr_3')"><input type="checkbox" class="" id="atr_3"> SKIP & USE VERTUAL COLOR</label>
                                                    </div>
                                                <?php } } } ?>



                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 img-group-div">
                                                        <div class="row mt-4">
                                                            <div class="col-sm-2 imgUp">
                                                                <div class="imagePreview"></div>
                                                                <label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" name="variableimg[]" style="width: 0px;height: 0px;overflow: hidden;"></label>
                                                            </div><!-- col-2 -->
                                                            <span class="imgAddd" onclick="addImage('variableimg')">+</span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label><input type="checkbox" id="image-skip" onclick="skipImages('imageuse')"> SKIP AND USER VERTUAL IMAGE</label>
                                                        <input type="hidden" name="imageuse" id="imageuse" value="0">
                                                        <?php // print_r($vimagearr); ?>
                                                    </div>

                                                    <div class="col-md-12 text-center">
                                                        <input type="submit" name="addvariation" class="btn btn-success" value="Add Variation">
                                                    </div>
                                                </div>
                                               <?php }else{ ?>
                                                <p class="text-danger"><strong>Note: </strong>Please Save Product First</p>
                                               <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </form>

    </div>
</section>


<?php 
include_once"helpers/footer.php";

if(isset($_POST['add_product'])){
    extract($_POST);
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($code) && !empty($category) && !empty($subcategory) && $price!="" && !empty($specification) && !empty($collection_tag) && !empty($style_code) && !empty($serial_no) && !empty($batch_no)){
        $check_sku = check_duplicate('product', 'sku', $code);
        if($check_sku == 1){
            echo "<script>showNotification('alert-warning', 'Product Sku Already Exists', 'top', 'right', '', '')</script>";
        }else{
        if(empty($edit)){
           $type= 0;
           $groupid = $code; 
        }
        mysqli_autocommit($conn, FALSE);
        $key = rand_char(5);
        try {
            $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `type`, `groupid`, `status`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssssssssssssssssssssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $add_cat, $mrp, $price, $specification, $description, $features, $review, $publish, $type, $groupid, $status, $collection_tag, $order_start_date, $line_of_business, $vpn, $style_code, $serial_no, $color_code, $material, $fit, $batch_no, $moq, $stock, $low_stock, $created_at);
            if ($stmt->execute()) {
                if(!empty($color)){
                    $sample = fileUpload($_FILES['clrsample']['name'], $_FILES['clrsample']['tmp_name'], 'clrsample', 'products/');
                }
                try{
                    $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, color, size, gender, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                    $stmt2->bind_param('sssssss', $code, $groupid, $sample, $color, $size, $gender, $created_at);
                    if($stmt2->execute()){
                        if(!empty($_FILES['file']['name'])){
                            $num = 0;
                            foreach($_FILES['file']['tmp_name'] as $keey => $tmp_name){
                                $num = $num + 1;
                                $order_unique = 'product_'.$code.'_'.$num;
                                $filename = fileUpload($_FILES['file']['name'][$keey], $tmp_name, $order_unique, 'products/');
                                $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$filename', '$created_at')");
                            }
                            mysqli_commit($conn);
                            echo "<script>showNotification('alert-success', 'Product Added Successfully', 'top', 'right', '', '')</script>";

                        }else{
                            // image not found
                            echo "<script>showNotification('alert-success', 'Product Added Successfully', 'top', 'right', '', '')</script>";
                        }
                    }else{
                        mysqli_rollback($onn);
                        echo mysqli_error($conn);
                        throw new exception($conn->error);
                    }
                }catch(Exception $e){
                    // echo "Error: " . $e->getMessage();
                    echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
                }
            }else{
                // echo mysqli_error($conn);
                throw new exception($conn->error);
            }
        }catch(Exception $e){
            // echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-Danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
        }
    }
    }else{
        echo "<script>showNotification('alert-info', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }

}

if(isset($_POST['addvariation'])){
    extract($_POST);
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($variationsku) && !empty($category) && !empty($subcategory) && $price!="" && !empty($specification) && !empty($collection_tag) && !empty($style_code) && !empty($serial_no) && !empty($batch_no)){
    $check_sku = check_duplicate('product', 'sku', $variationsku);
    if($check_sku == 1){
        echo "<script>showNotification('alert-warning', 'Product Sku Already Exists', 'top', 'right', '', '')</script>";
    }else{
    mysqli_autocommit($conn, FALSE);
    $key = rand_char(5);
    try {
        $stmt = $conn->prepare("INSERT INTO `product`(`p_key`, `store`, `brand`, `product_name`, `sku`, `category`, `subcategory`, `add_cat`, `mrp`, `price`, `specification`, `description`, `features`, `review`, `publish`, `type`, `groupid`, `status`, `collection_tag`, `order_start_date`, `line_of_business`, `vpn`, `style_name`, `serial_no`, `color_code`, `material_code`, `fit`, `batch_no`, `min_sale_quantity`, `stock`, `low_stock`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssssssssssssssssssssssssss", $key, $store, $brand, $title, $variationsku, $category, $subcategory, $add_cat, $mrp, $price, $specification, $description, $features, $review, $publish, $type, $groupid, $status, $varcollection_tag, $varorder_start_date, $varline_of_business, $varvpn, $varstyle_code, $varserial_no, $varcolor_code, $varmaterial, $varfit, $varbatch_no, $varmoq, $varstock, $varlow_stock, $created_at);
        if ($stmt->execute()) {
            $sample = $prev_sample;
            // color attribute
            if(!empty($varcolor)){
                $color  = $varcolor;
                if(!empty($_FILES['varclrsample']['name'])){
                    $sample = fileUpload($_FILES['varclrsample']['name'], $_FILES['varclrsample']['tmp_name'], 'clrsample', 'products/');
                }
            }
            // size attribute
            if(!empty($varsize)){
                $size = $varsize;
            }

            // size attribute
            if(!empty($vargender)){
                $gender = $vargender;
            }
            
            try{
                $stmt2 = $conn->prepare("INSERT INTO product_attributes (p_id, groupid, sample, color, size, gender, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt2->bind_param('sssssss', $variationsku, $groupid, $sample, $color, $size, $gender, $created_at);
                if($stmt2->execute()){
                    if(empty($imageuse)){
                        $num = 0;
                        if(!empty($_FILES['variableimg']['name'])){
                            foreach($_FILES['variableimg']['tmp_name'] as $keey => $tmp_name){
                                $num = $num + 1;
                                $order_unique = 'product_'.$code.'_'.$num;
                                // $images = $images.$_FILES['files']['name'][$key];
                                $filename = fileUpload($_FILES['variableimg']['name'][$keey], $tmp_name, $order_unique, 'products/');
                                $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$variationsku', '$filename', '$created_at')");
                                // if($sql){
                                //     echo "IMAGE => Done";
                                // }else{
                                //     echo "Image Err => ".mysqli_error($conn);
                                // }
                                // $images = $images.$filename.",";
                            }
                        }
                    }else{
                        for($i = 0; $i <= count($vimagearr) - 1; $i++){
                            $nimagename = $vimagearr[$i]['image'];
                            $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$variationsku', '$nimagename', '$created_at')");
                                // if($sql){
                                //     echo "IMAGE => Done";
                                // }else{
                                //     echo "Image Err => ".mysqli_error($conn);
                                // }
                        }
                    }
                    mysqli_commit($conn);
                    echo "<script>showNotification('alert-success', 'Variation Added Successfully', 'top', 'right', '', '')</script>";
                }else{
                    mysqli_rollback($conn);
                    // mysqli_error($conn);
                    throw new exception($conn->error);
                }
                }catch(Exception $e){
                    // echo "Error: " . $e->getMessage();
                    echo "<script>showNotification('alert-error', 'Variation Not Added Successfully', 'top', 'right', '', '')</script>";
                }
        }else{
            throw new exception($conn->error);
        }
    }catch(Exception $e){
        // echo "Error: " . $e->getMessage();
        echo "<script>showNotification('alert-info', 'Variation Not Added Successfully', 'top', 'right', '', '')</script>";
    }
    }
}else{
    echo "<script>showNotification('alert-info', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
}
}


if(isset($_POST['update_product'])){
    extract($_POST);
    echo $store."=".$brand."=".$title."=".$code."=".$category."=".$subcategory."=".$price."=".$specification."=".$collection_tag."=".$style_code."=".$serial_no."=".$batch_no;
    if(!empty($store) && !empty($brand) && !empty($title) && !empty($code) && !empty($category) && !empty($subcategory) && $price!="" && !empty($specification) && !empty($collection_tag) && !empty($style_code) && !empty($serial_no) && !empty($batch_no)){
    mysqli_autocommit($conn, FALSE);
    try{
        $stmt = $conn->prepare("UPDATE product SET store=?, brand=?, product_name=?, sku=?, category=?, subcategory=?, add_cat=?, mrp=?, price=?, specification=?, description=?, features=?, review=?, publish=?, type=?, groupid=?, status=?,collection_tag=?, order_start_date=?, line_of_business=?, vpn=?, style_name=?, serial_no=?, color_code=?, material_code=?, fit=?, batch_no=?, min_sale_quantity=?, stock=?, low_stock=? WHERE p_id=?");
        $stmt->bind_param('sssssssssssssssssssssssssssssss', $store, $brand, $title, $code, $category, $subcategory, $add_cat, $mrp, $price, $specification, $description, $features, $review, $publish, $type, $groupid, $status, $varcollection_tag, $varorder_start_date, $line_of_business, $vpn, $style_code, $serial_no, $color_code, $material, $fit, $batch_no, $moq, $stock, $low_stock, $product);
        if($stmt->execute()){
            if(!empty($color)){
                if(!empty($_FILES['clrsample']['name'])){
                    $sample = fileUpload($_FILES['clrsample']['name'], $_FILES['clrsample']['tmp_name'], 'clrsample', 'products/');
                }else{
                    $sample = $prev_sample;
                }
            }
            try{
                $stmt2 = $conn->prepare("UPDATE product_attributes SET sample=?, color=?, size=?, gender=? WHERE p_id=?");
                $stmt2->bind_param('sssss', $sample, $color, $size, $gender, $array[5]);
                if($stmt2->execute()){
                    $num = 0;
                    if(!empty($_FILES['file']['name'])){
                        foreach($_FILES['file']['tmp_name'] as $keey => $tmp_name){
                            $num = $num + 1;
                            $order_unique = 'product_'.$code.'_'.$num;
                            $filename = fileUpload($_FILES['file']['name'][$keey], $tmp_name, $order_unique, 'products/');
                            if(!empty($filename)){
                            $sql = mysqli_query($conn, "INSERT INTO product_images (product, image, created_at) VALUES ('$code', '$filename', '$created_at')");
                            }
                        }
                        mysqli_commit($conn);
                        echo "<script>showNotification('alert-success', 'Product Updated Successfully', 'top', 'right', '', '')</script>";

                    }else{
                        // image not found
                        mysqli_commit($conn);
                        echo "<script>showNotification('alert-success', 'Product Updated Successfully', 'top', 'right', '', '')</script>";
                    }
                }else{
                    mysqli_rollback($onn);
                    // echo mysqli_error($conn);
                    throw new exception($conn->error);
                }
            }catch(Exception $e){
                // echo "Error: " . $e->getMessage();
                echo "<script>showNotification('alert-danger', 'Product Not Updated Successfully', 'top', 'right', '', '')</script>";
            }
        }else{
            throw new exception($conn->error);
        }
    }catch(Exception $e){
        // echo "Error: " . $e->getMessage();
        echo "<script>showNotification('alert-danger', 'Product Not Updated Successfully', 'top', 'right', '', '')</script>";
    }
}else{
    echo "<script>showNotification('alert-info', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
}
}
?>