<div class="table-responsive">
<table class="table table-hover attr-table">
                                        <thead>
                                        <tr>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th>Name<br>(Custom Attribute)</th>
                                            <th>Value<br>(Custom Attribute)</th>
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                <div class="form-group">
                                                <input type="text" name="color[]" class="form-control" placeholder="Enter Color">
                                                </div>
                                                </td>
                                                <td>
                                                <div class="form-group">
                                                <input type="text" name="size[]" class="form-control" placeholder="Enter Size">
                                                </div>
                                                </td>
                                                <td>
                                                <div class="form-group">
                                                <input type="text" name="atr[]" class="form-control" placeholder="Custom Name">
                                                </div>
                                                </td>
                                                <td>
                                                <div class="form-group">
                                                <input type="text" name="atr_val[]" class="form-control" placeholder="Custom Value">
                                                </div>
                                                </td>
                                                <td>
                                                <div class="form-group">
                                                <input type="text" onkeyup="setQty(0)" name="qty[]" class="form-control qtr_0" value="1">
                                                </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><span class="text-primary float-right" onclick="addAttr()">+Add More Row</span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    </div>z