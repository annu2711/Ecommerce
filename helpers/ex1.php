// save store
if(isset($_POST['add_product'])){
    extract($_POST);
    if(!empty($store) && !empty($title) && !empty($code) && !empty($category) && !empty($description) && !empty($sale_price) && $review!=""){
        $primary_img = $_FILES['primary_img']['name'];
        $secondry_img = $_FILES['second_img']['name'];
        $other = $_FILES['other_img']['name'];
        $image1 = "";
        $image2 = "";
        $images = "";
        $num = 0;
        $attributes = "";
        // $shipping_info = $weight.','.$width.','.$height;
        if(!empty($primary_img)){
           $image1 = fileUpload($primary_img, $_FILES['primary_img']['tmp_name'], 'primary', 'products/', 1);
        }
        if(!empty($secondry_img)){
            $image2 = fileUpload($secondry_img, $_FILES['second_img']['tmp_name'], 'second', 'products/', 1);
         }
         if(!empty($other)){
         foreach($_FILES['other_img']['tmp_name'] as $key => $tmp_name){
            $num = $num + 1;
            $order_unique = 'other'.$num;
            // $images = $images.$_FILES['files']['name'][$key];
            $filename = fileUpload($_FILES['other_img']['name'][$key], $_FILES['other_img']['tmp_name'][$key], $order_unique, 'products/', 1);
            $images = $images.$filename.",";
        }
        }

        // custom attributes starts

            // if($atr!= "" && $atr_val!= ""){
            //     foreach($atr as $key => $attribute){
            //         $attributes = $attributes.','.$attribute.'!'.$atr_val[$key];
            //     }
            // }

        // custom attributes ends
        $a = 1;
        $key = rand_char(5);
        mysqli_autocommit($conn, FALSE);
        try {
        $stmt = $conn->prepare("INSERT INTO `products_db`(`p_key`, `store`, `brand`, `product_name`, `code`, `category`, `subcategory`, `add_cat`, `regular_price`, `price`, `offer`, `offer_start`, `offer_end`, `prf`, `specification`, `description`, `features`, `reviews`, `status`, `visible`, `p_img`, `s_img`, `o_img`, `weight`, `width`, `height`, `create_by`, `created_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssssssssssssssssssssss", $key, $store, $brand, $title, $code, $category, $subcategory, $add_cat, $regular_price, $sale_price, $offer, $offer_start_date, $offer_end_date, $prf, $specification, $description, $features, $review, $status, $a, $image1, $image2, $images, $weight, $width, $height, $id, $created_at);
        if ($stmt->execute()) {
        foreach($color as $m => $n){
            $cln = strtoupper($n);
            $se = strtoupper($size[$m]);
            $qy = $qty[$m];
            $ar = strtoupper($atr[$m]);
            $ar_val = strtoupper($atr_val[$m]);
            try {
                $stmt = $conn->prepare("INSERT INTO product_detail (product, color, size, qty, name, value, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("sssssss", $key, $cln, $se, $qy, $ar, $ar_val, $created_at);
                if($stmt->execute()){
                    mysqli_commit($conn);
                }else{
                    mysqli_rollback($conn);
					throw new exception($conn->error);
                }
            }
            catch(Exception $e){
                echo "Error: " . $e->getMessage();
                    
            }

        }
        $exsql = mysqli_query($conn, "SELECT * FROM products_db WHERE p_key='$key'");
        if($exsql){
            if(mysqli_num_rows($exsql) > 0){
                echo "<script>showNotification('alert-info', 'Product Added ', 'top', 'right', '', '')</script>";
            }else{
                echo "<script>showNotification('alert-danger', 'Product Not Added ', 'top', 'right', '', '')</script>";
            }
        }
        $stmt->close();
        }else{
            throw new exception($conn->error);
        }
        // $stmt->close();
    }
    catch(Exception $e){
        echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-danger', 'Product Not Added Successfully', 'top', 'right', '', '')</script>";
    }
    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}

// Update Product


if(isset($_POST['update_product'])){
    extract($_POST);
    if(!empty($store) && !empty($title) && !empty($code) && !empty($category) && !empty($subcategory) && !empty($specification) && !empty($description) && !empty($sale_price) && $review!=""){
        // chk Images
        $image1 = "";
        $image2 = "";
        $images = "";
        $num = 0;
        $attributes = "";
        $shipping_info = $weight.','.$width.','.$height;
        if(!empty($_FILES['primary_img']['name'])){
            $image1 = fileUpload($primary_img, $_FILES['primary_img']['tmp_name'], 'primary', 'products/', 1);
        }else{
            $image1 = $array[27];
        }

        if(!empty($_FILES['primary_img']['name'])){
            $image2 = fileUpload($secondry_img, $_FILES['second_img']['tmp_name'], 'second', 'products/', 1);
        }else{
            $image2 = $array[28];
        }

        if(!empty($_FILES['primary_img']['name'])){
            foreach($_FILES['other_img']['tmp_name'] as $key => $tmp_name){
                $num = $num + 1;
                $order_unique = 'other'.$num;
                // $images = $images.$_FILES['files']['name'][$key];
                $filename = fileUpload($_FILES['other_img']['name'][$key], $_FILES['other_img']['tmp_name'][$key], $order_unique, 'products/', 1);
                $images = $images.$filename.",";
            }
        }else{
            $images = $array[29];
        }

        // attributes
        if($atr!= "" && $atr_val!= ""){
            foreach($atr as $key => $attribute){
                $attributes = $attributes.','.$attribute.'!'.$atr_val[$key];
            }
        }

        // excute query

        try {
            $stmt = $conn->prepare("UPDATE `products_db` SET `store`=?,`brand`=?,`product_name`=?,`code`=?,`category`=?,`subcategory`=?,`add_cat`=?,`regular_price`=?,`price`=?,`stock`=?,`min_qty`=?,`max_qty`=?,`offer`=?,`offer_start`=?,`offer_end`=?,`prf`=?,`color`=?,`size`=?,`attribute`=?,`specification`=?,`description`=?,`features`=?,`reviews`=?,`status`=?,`visible`=?,`p_img`=?,`s_img`=?,`o_img`=?,`shipping_info`=? WHERE `p_key` = ? ");
            $stmt->bind_param("ssssssssssssssssssssssssssssss", $store, $brand, $title, $code, $category, $subcategory, $add_cat, $regular_price, $sale_price, $stock, $min_qty, $max_qty, $offer, $offer_start_date, $offer_end_date, $prf, $color, $size, $attributes, $specification, $description, $features, $review, $status, $visible, $image1, $image2, $images, $shipping_info, $product);
            if ($stmt->execute()) {
                echo "<script>showNotification('alert-info', 'Product Updated Successfully', 'top', 'right', '', '')</script>";
                $stmt->close();
                header('location: product-list');
            }else{
                throw new exception($conn->error);
            }
            // $stmt->close();
        }catch(Exception $e){
            echo "Error: " . $e->getMessage();
            echo "<script>showNotification('alert-danger', 'Product Not Updated Successfully', 'top', 'right', '', '')</script>";
        }



    }else{
        echo "<script>showNotification('alert-warning', 'Please Fill All Fields', 'top', 'right', '', '')</script>";
    }
}