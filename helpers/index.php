<?php 
ob_start();
session_start();
include_once "lsmartbk/helpers/config.php";
include_once "lsmartbk/helpers/function.php";

if(isset($_SESSION['user']) && isset($_SESSION['token'])){
    $id = $_SESSION['user'];
    $usertoken = $_SESSION['token'];
    // find user name
    $query = mysqli_query($conn, "SELECT * FROM clients WHERE client_key='$id' AND token='$usertoken'");
    if($query){
        $userdata = mysqli_fetch_array($query);
        $name = $userdata[2]." ".$userdata[3];
    }

    // addredd array starts

        $sql = mysqli_query($conn, "SELECT * FROM address_book WHERE user='$userdata[0]' ORDER BY ab_id DESC LIMIT 1");
		if($sql){
			if(mysqli_num_rows($sql) > 0){
				$addressarr = mysqli_fetch_array($sql);
			}
		}

// address array ends

    // upload cart data on cloud server
    if(isset($_COOKIE['shopping_cart'])){
        $cookie_data = stripcslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
        if(count($cart_data) > 0){
            $insertp = 0;
            for($i=0; $i <= count($cart_data) - 1; $i++){
                $cartid = $cart_data[$i]['cartid'];
                $cartproduct = $cart_data[$i]['product'];
                $cartcolor = $cart_data[$i]['color'];
                $cartsize = $cart_data[$i]['size'];
                $cartqty = $cart_data[$i]['quantity'];
                $query = mysqli_query($conn, "INSERT INTO user_cart (uc_key, user, product, color, size, qty, created_at) VALUES ('$cartid', '$id', '$cartproduct', '$cartcolor', '$cartsize', '$cartqty', '$created_at')");
                if($query){
                    $insertp = $insertp + 1;
                }
            }
            $data = "";
            setcookie('shopping_cart', $data, time() - 3600);
        }
    }

    // upload wshlist on server
    if(isset($_COOKIE['wishlist'])){
        $cookie_data = stripcslashes($_COOKIE['wishlist']);
        $wish_data = json_decode($cookie_data, true);
        if(count($wish_data) > 0){
            // echo  "Hii";
            $insertw = 0;
            $error = "";
            for ($i=0; $i < count($wish_data); $i++) { 
                $wishid = $wish_data[$i]['w_key'];
                $product = $wish_data[$i]['product'];
                $color = $wish_data[$i]['color'];
                $size = $wish_data[$i]['size'];
                $quantity = $wish_data[$i]['qty'];
                $query = mysqli_query($conn, "INSERT INTO wishlist (w_key, user, product, color, size, qty, created_at) VALUES ('$wishid', '$id', '$product', '$color', '$size', '$quantity', '$created_at')");
                if($query){
                    $insertw = $insertw + 1;
                }else{
                    $error = $error.mysqli_error($conn);
                }            
            }
            $data = "";
            setcookie('wishlist', $data, time() - 3600);
            // echo $insertw;
            // echo $error;
        }
    }


}else{
    $id = "";
    $usertoken = "";
}

//display cart
$totalcartquantity = 0;
$totalcartamount = 0;
$totalwishquantity = 0;
if(empty($id) && empty($usertoken)){
    // local cart starts
if(isset($_COOKIE['shopping_cart'])){
    $cookie_data = stripcslashes($_COOKIE['shopping_cart']);
    $cart_data = json_decode($cookie_data, true);
    for ($i=0; $i < count($cart_data); $i++) { 
        $totalcartquantity = $totalcartquantity + $cart_data[$i]['qty'];
        $totalcartamount = $totalcartamount + ($cart_data[$i]['qty'] * intval(getSinglevalue('product', 'sku', $cart_data[$i]['product'], 10)));
    }
}
// local cart ends

// local wishlist starts
$user_wish = [];
if(isset($_COOKIE['wishlist'])){
    $cookie_data = stripcslashes($_COOKIE['wishlist']);
    $user_wish = json_decode($cookie_data, true);
    $totalwishquantity = count($user_wish);
}
}else{
    $totalcartquantity = 0;
    $totalcartamount = 0;
    $cart_data = [];
    $sql = mysqli_query($conn, "SELECT * FROM user_cart WHERE user='$id'");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $totalcartquantity = $totalcartquantity + intval($rows[6]);
                $totalcartamount = $totalcartamount + (intval($rows[6]) * getSinglevalue('product', 'sku', $rows[3], 10));
                $cart_data[] = ['cartid' => $rows[1], 'product' => $rows[3], 'color' => $rows[4], 'size' => $rows[5], 'qty' => $rows[6]];
            }
        }
    }
    // cloud cart ends
    $user_wish = [];
    $totalwishquantity = 0;
    $sql = mysqli_query($conn, "SELECT * FROM wishlist WHERE user='$id'");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            while($rows = mysqli_fetch_array($sql)){
                $user_wish[] = $rows;
            }
        }
    }
    $totalwishquantity = count($user_wish);

}

// search starts 
$searchval = "";
if(isset($_GET['searchinput'])){
    extract($_GET);
    if(!empty($searchinput)){
        $searchval = $searchinput;
    }   
}

// search ends
include_once "helpers/header.php";

?>