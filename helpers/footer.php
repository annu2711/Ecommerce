<footer>
	<div class="mobile_menu d-blcck d-lg-none">
		<div class="container-fluid">
			<div class="row">
				<ul class="w-100 mb-0">
					<li><a href="javascript:void(0)"><img src="img/country.png"> INR | English <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li><a href="javascript:void(0)">Our Stores<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
					<li><a href="javascript:void(0)">Contact Us<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="newsletterPart pt-0 pt-lg-2 pb-2">
		<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">
				<div class="newletter mb-2 mb-lg-0">
					<p class="mb-0"><a class="newletter" href="" data-toggle="modal" data-target="#newslatterModal">Sign up to our newsletter <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
				</div>
			</div>
			<div class="col-lg-6 align-items-center d-flex justify-content-center justify-content-lg-end">
				<div class="socialIcon">
					<ul class="mb-0">
						<li class="d-none d-lg-inline-block">Contact with us:</li>
						<li>
							<a href="https://www.facebook.com/bquestindia/" target="_blank">
								<img src="img/facebook.svg" alt="">
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/bquestindia/?hl=en" target="_blank">
								<img src="img/instagram.svg" alt="">
							</a>
						</li>
						<!-- <li>
							<a href="javascript:void(0)" target="_blank">
								<img src="img/pinterest.svg" alt="">
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" target="_blank">
								<img src="img/twitter.svg" alt="">
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" target="_blank">
								<img src="img/youtube.svg" alt="">
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" target="_blank">
								<img src="img/weibo.svg" alt="">
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" target="_blank">
								<img src="img/chat.svg" alt="">
							</a>
						</li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="footerMenu px-3 py-2 d-none d-lg-block">
		<div class="boxFooterGrid">
			<div class="componentContent">
				<p>Our Stores</p>
				<ul>
					<li><a href="javascript:void(0)">Delhi</a></li>
					<li><a href="javascript:void(0)">Mumbai</a></li>
					<li><a href="javascript:void(0)">Ludhiana</a></li>
					<li><a href="javascript:void(0)">Hyderabad</a></li>
					<li><a href="javascript:void(0)">Kolkata</a></li>
				</ul>
			</div>
		</div>
		<div class="boxFooterGrid">
			<div class="componentContent">
				<p>Customer Services</p>
				<ul>
					<li><a href="javascript:void(0)">FAQs</a></li>
					<!-- <li><a href="javascript:void(0)">LSMART Rewards</a></li> -->
					<!-- <li><a href="javascript:void(0)">Product Recalls</a></li> -->
					<li><a href="javascript:void(0)">Contact us</a></li>
					<!--<li><a href="javascript:void(0)">Personal shopping</a></li>-->
					<!--<li><a href="javascript:void(0)">Gift cards</a></li>-->
					<!--<li><a href="javascript:void(0)">Gift packaging</a></li>-->
				</ul>
			</div>
		</div>
		<div class="boxFooterGrid">
			<div class="componentContent">
				<p>Shopping Online</p>
				<ul>
				
					<li><a href="javascript:void(0)">Track Your Order</a></li>
					<li><a href="return_to_store.php">Return Your Order</a></li>
					<li><a href="javascript:void(0)">Delivery</a></li>
					<li><a href="return_policy.php">Returns</a></li>
					<li><a href="designers.php">Brand directory</a></li>
					<li><a href="javascript:void(0)">Cookie Policy</a></li>
				</ul>
			</div>
		</div>
		<div class="boxFooterGrid">
			<div class="componentContent">
				<p>Language</p>
				<ul>
					<li><a href="javascript:void(0)"><img src="img/country.png"> INR | English</a></li>
				</ul>
			</div>
		</div>
		<div class="boxFooterGrid boxFooterGrid2">
			<div class="componentContent">
				<p>About Us</p>
				<ul>
					<li><a href="#">BQUEST India Group</a></li>
					<li><a href="#">Careers</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="footerBottom py-1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-6 logoPart">
					<div class="footBtmInn ml-md-0 mx-auto">
						<a href="index.html">
							<img src="img/logo.png" class="logoLsmart">
 						</a>
 						<div class="term-and-cond d-none d-lg-block">
 							<ul class="mb-0">
 								<li><a href="javascript:void(0)">Terms & Conditions</a></li>
 								<li><a href="privacy_policy.php">Privacy & Cookie Policies</a></li>
 							</ul>
 						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6">
					<div class="copyright text-md-right text-center py-sm-2">
						<p class="mb-0">Copyright © 2020 LSmart. All rights reserved</p>
						<p class="mb-0 d-none d-lg-block">Registered office: 27, Basant Lok, Vasant Vihar, New Delhi - 110057<br>Mob: 9599123123</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
        <!-- NEWS LATTER MODAL START-->
        <div class="newslatterModal">
            <div class="modal fade" id="newslatterModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header px-4 mt-3">
                        	<h5 class="ml-2 mb-0"><strong>Brighten up your inbox.</strong></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
                        </div>
                        <div class="modal-body">
                                <div class="newsletter-desc">
                                    <div class="newsletter-form p-3 pb-4">
                                        <form method="post">
                                            <p class="mb-5 mt-3">
                                                <input type="email" name="newsLetter" class="form-control" placeholder="Type your email here" required>
                                            </p>
                                            <p class="mb-4">
                                                <input type="submit" name="submitNewsLetter" class="btn" value="Subscribe">
                                            </p>
                                            <p>By signing up, you are accepting our terms and conditions and have read and understood our privacy and cookie policies.</p>
                                        </form>
                                    </div>
                            </div>
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        </div>
        <!-- NEWS LATTER MODAL END-->
        <!-- cookies policy start -->
        <!---cookies popup--->
        <div class="container cookies" style="display: none">
             <div class="row">
                <div class="col-lg-8 col-md-6">
                    <div class="cookies-cont">
                        <h1>COOKIES</h1>
                        <P>To offer you a better experience, this site uses profiling cookies, even from third parties. By continuing to browse our site you accept our cookie policy.<br>
                        <a href="privacy-policy">LEARN MORE</a></P>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="row">
                        <div class="cookies-btn">
                           
                                <button class="btn btn-light btn-block" onclick="saveCookieIt('cookies')">ACCEPT COOKIES</button>
                          
                        </div>
                    </div>
                </div>
             </div>
         </div>
		<!-- cookies policy Ends -->
		<!-- Modal -->
<div class="shopPopup modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
	  <div class="modal-product">
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="modal-product" style="display: none"> 
        <!-- .product-info -->
        </form>
        </div><!-- .modal-product -->
      </div>
      
    </div>
  </div>
</div>
<!-- Modal ends here -->

<!-- Modal start-->
<div class="addressModal modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header">
								        <h5 class="modal-title" id="exampleModalLabel">Add new address</h5>
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="formReset()">
								          <span aria-hidden="true">&times;</span>
								        </button>
								      </div>
								      <div class="modal-body">
								        	<div class="addressBookPopup">
												<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="address-form-one" class="paymantOption2">
												<input type="hidden" id="ab_key" name="ab_key" value="">
													<div class="row">
														<div class="col-md-2">
															<div class="titleDiv p-0">
										        				<select name="title" id="add-title" required>
																	<?php 
																		$titlearray = ["Mr", "Mrs", "Miss", "Ms", "Dr"];
																		foreach($titlearray as $k => $value){ ?>
																		<option value="<?php echo $value ?>"><?php echo $value ?></option>	
																	<?php } ?>
																</select>
										        			</div>
														</div>
														<div class="col-md-5">
															<div class="addressInputDiv p-0">
										        				<input type="text" placeholder="First name*" id="first-name" name="name" required>	
										        			</div>
														</div>
														<div class="col-md-5">
															<div class="addressInputDiv p-0">
										        				<input type="text" placeholder="Last name*" name="last_name" id="last-name">	
										        			</div>
														</div>
														<div class="col-md-12">
															<div class="addressInputDiv p-0">
										        				<div class="row">
										        					<div class="col-3">
										        						<p>+91</p>
										        					</div>
										        					<div class="col-9">
																		<input type="text" placeholder="Mobile number*" onkeyup="mobileValidations('mainmobile', 'mainmobileerror')" id="mainmobile" name="mobile" required>
																		<p class="text-danger" style="border: none; margin-bottom: 0px; padding: 0px; display:none" id="mainmobileerror">Please Enter 10 Digit Valid Mobile Number</p>
																	</div>
										        				</div>	
										        			</div>
														</div>
														<div class="col-md-12">
															<div class="addressInputDiv p-0">
										        				<input type="text" placeholder="Address line 1*" id="line-a" name="add_line_1" required>	
										        			</div>
														</div>
														<div class="col-md-12">
															<div class="addressInputDiv p-0">
																<input type="text" placeholder="Address line 2" id="line-b" name="add_line_2">		
										        			</div>
														</div>
														<div class="col-md-4">
															<div class="addressInputDiv p-0">
										        				<input type="text" placeholder="Town/city*" name="city" id="add-city" required>	
										        			</div>
														</div>
														<div class="col-md-4">
															<div class="addressInputDiv p-0">
										        				<select class="state" name="state" id="add-state" required>
																	<?php 
																		echo indianState();
																	?>
																</select>	
										        			</div>
														</div>
														<div class="col-md-4">
															<div class="addressInputDiv p-0">
										        				<input type="text" placeholder="Postcode*" id="add-pincode" name="pincode" required>	
										        			</div>
														</div>
													</div>
													<p class="text-danger add-error"></p>
													<button type="button" onclick="saveAddress('0')" class="button save-address">Save address</button>
													
													<button type="button" onclick="saveAddress('1')" class="button update-address" style="display: none">update address</button>
										        	
								        		</form>
								        	</div>
								      </div>
								    </div>
								  </div>
								</div>
								<!-- modal end -->

		<!-- all js here -->
		<!-- bootstrap js -->
 <script type="text/javascript" src="js/bootstrap.js"></script> 
 <script type="text/javascript" src="js/owl.carousel.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>

 <!-- zoom -->
<!-- <script src="Vendor/jquery/jquery-1.8.3.min.js"></script> -->
<script src="Vendor/jquery/jquery-ui.min.js"></script>
<script src="Vendor/fancybox/jquery.fancybox.js"></script>
<script src="Vendor/elevatezoom/jquery.elevatezoom.js"></script>
<script src="Vendor/panZoom/panZoom.js"></script>
<script src="Vendor/ui-carousel/ui-carousel.js"></script>
<script src="Vendor/js/zoom.js"></script>
<script type="text/javascript" src="js/notify.js"></script>
<script type="text/javascript" src="js/main.js"></script> 

	<script>
		var cookiee = localStorage.getItem("cookiee");
		if(cookiee != null){
		$('.cookies').hide();
		}else{
			$('.cookies').show();
		}
</script>
<script type="text/javascript">
	function saveCookieIt(x){
		// $('.' + x).hide();
		localStorage.setItem("cookiee", "yes");
		$('.cookies').hide();
	}
</script>
<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
    </body>
</html>

<?php flash_session_web();

	if(isset($_POST['submitNewsLetter'])){
		$newsLetter = mysqli_real_escape_string($conn,$_POST['newsLetter']);
		if(!empty($newsLetter)){
			$checkNewsLetter = check_duplicate('news_letter', 'email', $newsLetter);
			if($checkNewsLetter == 1){
				echo '<script>$.notify("You have already subscribed", "warning")</script>';
			}else{
				$newsSql = mysqli_query($conn,"INSERT INTO `news_letter`(`email`) VALUES('$newsLetter')");
				if($newsSql){
					echo '<script>$.notify("Subscribed", "success")</script>';
				}else{
					echo '<script>$.notify("Failed to subscribe", "error")</script>';
				}
			}
		}else{
			echo '<script>$.notify("Please fill out this field", "error")</script>';
		}
	}
?>