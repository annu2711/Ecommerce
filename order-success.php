<?php include_once "helpers/index.php"; 

if(isset($_GET['orderid'])){
    extract($_GET);
    $sql = mysqli_query($conn, "SELECT * FROM orders WHERE order_key='$orderid' ORDER BY order_id DESC LIMIT 1");
    if($sql){
        if(mysqli_num_rows($sql) > 0){
            $orderarr = mysqli_fetch_array($sql);
                $gst = 18;
                if($orderarr[8] == 'paynow'){
                    if(isset($_POST['status'])){
                    // payment gateway starts
                    $status=$_POST["status"];
                    $firstname=$_POST["firstname"];
                    $amount=$_POST["amount"];
                    $txnid=$_POST["txnid"];
                    $posted_hash=$_POST["hash"];
                    $key=$_POST["key"];
                    $productinfo=$_POST["productinfo"];
                    $email=$_POST["email"];
                    $salt="";
                    $api_response = $status."|".$firstname."|".$amount."|".$txnid."|".$posted_hash."|".$key."|".$productinfo."|".$email;
                    $sql = mysqli_query($conn, "INSERT INTO service_response(response, api, orderid) VALUES ('$api_response', 'payu', '$txnid')");
                    // Salt should be same Post Request 
                    If(isset($_POST["additionalCharges"])) {
                        $additionalCharges=$_POST["additionalCharges"];
                        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                    }else{
                        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                    }
		            $hash = hash("sha512", $retHashSeq);
                    if ($hash != $posted_hash) {
                        define("PAYMENT_STATUS", $status);
                        define("PAYMENT_ORDER_ID", $txnid);
                        define("PAYMENT_AMOUNT", $amount);
                    } 
                //  payment gateway ends
            }else{
                header('location: index.php');
            }
                }
        }else{
            header('location: index');
        }
    }
}else{
    header('location: index');
}

?>
<script>
    localStorage.removeItem("cart");
</script>
<style>
    .ch-btn{
background-color: #fde256;
    font-weight: bold;
    border-radius: 0;
    width: 219px;
    font-size: 15px;
    text-align: center;
    margin: 0px auto;
}
.order-msg{
  padding: 100px 0px;
  text-align: center;
}
.order-title{
  margin-top: 20px;
    font-size: 16px;
    text-transform: capitalize;
    line-height: 30px;
}
h3.cupon-pop{
	font-size: 18px;
    margin-bottom: 20px;
	color:#222;
	display:inline-block;
	text-align:center;
	padding:5px 20px;
	border:2px dashed #222;
	clear:both;
	font-weight:normal;
}
h3.cupon-pop span{
	color:#f6c92f;
}
    </style>
<!-- LOGIN-REGISTER-AREA-START -->
<div class="login-register-area">
            <div class="container">
				<div class="row justify-content-center">
                    <div class="col-md-12 col-sm-12">
                        <div class="order-msg">
                        <?php 
                            if($orderarr[8] == 'paynow'){
                                if(PAYMENT_STATUS == 'success'){
                                    $show_bill = true; 
                                    $rcemail = $useremail;
									$rcmobile = $usermobile;
                                    $smscontent = 'Hi '.$username.', We are pleased to confirm your order no '.$order_key.' .Thank you for shipping with Vanamour.in';
									$msg = mobilesms($rcmobile, $smscontent);
									$content = 'Hi,<br>
										We are pleased to confirm your order no '.$orderid.'.
										Thank you for shopping with Vanamour.in<br>
										Check your order <a href="https://vanamour.in/my-account">here</a>';
										$mail = sendEmail('orders@lsmart.in', $rcemail, 'Order Successfully', $content);
										// header('location: order-success?orderid='.$orderid);
                                    ?>
                                    <img src="img/success-tick.png" width="75px">
                                    <h4>We have received a payment of Rs. <?php echo PAYMENT_AMOUNT ?>. Your order will soon be shipped.</h4>
                        <?php   }else{ 
                                    $show_bill = false;
                                    ?>
                                    <img src="img/delete.png" width="75px" style="margin-top: 30px">
                                    <h3>Your order status is <?php echo PAYMENT_STATUS ?>.</h3>
                        <?php   }
                            }else{ ?>
                                <h3>Your order booked successfully.</h3>
                        <?php }
                        ?>
                        <h3 class="cupon-pop">Your Order Id: <span><?php echo $orderid ?></span></h3>
                        </div>
					</div>
				</div> 
			</div>
        </div>
        <!-- LOGIN-REGISTER-AREA-END -->  
<!-- <div style="max-width: 1000px; text-align: center; display:none;"> -->
        
<style>
.order-success-li li{
    display: inline-block;
}
.order-success-li a, .order-success-li button{
    line-height: 19px;
    background-color: #fde256;
    font-weight: bold;
    border-radius: 0;
    padding: 15px 25px;
    margin-top: 10px;
    display: inline-block;
    font-weight: 500;
}
</style>
<div align="center">
    <ul class="order-success-li">
        <li><a href="helpers/pdfmaker.php?invoice=<?php echo $orderid ?>" class="default-btn">Download Invoice</a></li>
        <li><a href="index.php" class="default-btn">Continue Shopping</a></li>
    </ul>
    </div>
                           <!-- </div> -->

<?php include_once "helpers/footer.php"; ?>