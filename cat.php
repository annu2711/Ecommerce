<?php include_once "helpers/index.php";


if(isset($_GET['cat'])){
    extract($_GET);
    $urlarray = explode("_", $cat);
    $category = "";
    $subcategory = "";
    $product = "";
    $product_list = "";
    $quote = "'";
    $prf="";
    $pstatus = "";
    $catarray = explode("-",$urlarray[0]);
    if($catarray[0]!= ""){
      	$category = "AND t1.category='".$catarray[0]."'";
    }
    if($catarray[1]!= ""){
		$subcategory = "AND t1.subcategory='".$catarray[1]."'";
    }
    if($urlarray[2]!= ""){
		if($urlarray[2] === 'Men' || $urlarray[2] === 'Women'){
			$prf = "AND t2.gender IN ('".$urlarray[2]."', 'UNISEX')";
		}else{
			$prf = "AND t2.gender='".$urlarray[2]."'";
		}
    }
    $status = explode("!",$urlarray[1]);
    if($status[1]!= ""){
		$pstatus = "AND t1.status='".$status[1]."'";
    }

    $countq = mysqli_query($conn, "SELECT t1.p_id FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $category $subcategory $pstatus $prf GROUP BY t1.groupid");
    if($countq){
        $prdrows = mysqli_num_rows($countq);
        if($prdrows > 12){
            // echo "rows =>".$rows;
            $totalpages = ceil($prdrows / 12);
            echo'<input type="hidden" id="totalpages" value="'.$totalpages.'"><input type="hidden" id="currentpage" value="1">';
        }else{
            // echo "rows2 =>".$rows;
            echo'<input type="hidden" id="totalpages" value="1"><input type="hidden" id="currentpage" value="1">';
        }
    }else{
		echo mysqli_error($conn);
	}

    $query = mysqli_query($conn, "SELECT t1.* FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $category $subcategory $prf $pstatus GROUP BY t1.groupid ORDER BY price ASC LIMIT 12");
    if($query){
        if(mysqli_num_rows($query) > 0){
            // $products_array = mysqli_fetch_array($query);
            while($products = mysqli_fetch_assoc($query)){
                $sku = $products['sku'];
				$imgsql = mysqli_query($conn, "SELECT image FROM product_images WHERE product='$sku' ORDER BY pi_id ASC LIMIT 2");
				if($imgsql){
					if(mysqli_num_rows($imgsql) > 0){
						$imgarr = [];
						while($imgrow = mysqli_fetch_assoc($imgsql)){
							$imgarr[] = $imgrow['image'];
						}
					}
				}

			$product = $product.'
				<div class="col-xl-4 col-6">
				<div class="categoryOuter">
				<div class="categoryOuterImg">
					<a href="single-product?p='.$products['sku'].'">
						<img src="'.PRODUCT_IMAGE_URL.checkImage($imgarr[0]).'" class="imgWithOutHover" class="img-fluid" alt="">';
						if(empty($imgarr[1])){
							$product = $product.'<img src="'.PRODUCT_IMAGE_URL.checkImage($imgarr[0]).'" class="imgWithHover" class="img-fluid" alt="">';
						}else{
							$product = $product.'<img src="'.PRODUCT_IMAGE_URL.checkImage($imgarr[1]).'" class="imgWithHover" class="img-fluid" alt="">';
						}
			$product = $product.'</a>
					</div>
					<div class="categoryButton">
					<div class="categoryContent">
						<h6>'.getSinglevalue('brands', 'brand_key', $products['brand'], 3).'</h6>
						<p>'.$products['product_name'].'</p>
						<p><i class="fa fa-inr"></i> '.str_replace('INR', '',money_format('%i', $products['price'])).'</p>
					</div>
					</div>
				</div>
			</div>
				';
            }
        }else{
            $error = '<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>';
        }
    }

}else{
    header('location: index');
}


?>
<input type="hidden" id="filter" value="0">
<section class="category pb-5 pt-0 pt-lg-5">
	<div class="container">
		<div class="row">
	
<?php if(!empty($product)){ ?>
			<div class="col-lg-3 d-none d-lg-block">
				<div class="filterMainDiv">
					<p><span class="pd-count"><?php echo $prdrows; ?></span> Results</p>
					<div class="accordion">
				<!-- ex one -->
				<?php
					// subcategory or category
                    if(!empty($prf)){
									$query = mysqli_query($conn, "SELECT DISTINCT(t1.category) as cat FROM `product` as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf");
									if($query){
										if(mysqli_num_rows($query) > 0){ ?>
											<div class="accordion-toggle">
												<h4 class="mb-0">Category</h4>
												<!-- <p class="mb-0">All</p> -->
												<p class="mb-0"><?php echo (!empty($catarray[0])) ? getSinglevalue('category', 'cat_key', $catarray[0], 2) : 'All'; ?></p>
											</div>
											<div class="accordion-content">
												<div class="refinedBrand">
													<!-- <form> -->
													<!-- <div class="scrolly"> -->
														
														<div class="scrolly">
														<div class="checkboxGroup my-lg-2">
															<p><a href="cat?cat=-_<?php echo $urlarray[1] ?>_<?php echo $urlarray[2] ?>" <?php echo empty($catarray[1]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
														</div>
														<?php  while($rows = mysqli_fetch_assoc($query)){ ?>
															<div class="checkboxGroup my-lg-1">
																<p><a href="cat?cat=<?php echo $rows['cat'] ?>-_st!_<?php echo $urlarray[2] ?>" <?php echo ($catarray[1] == $rows['cat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?> 
																<!-- <span>(<?php // echo $rows['rc'] ?>)</span> -->
																<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
															</div>
														<?php } ?>
														<?php }
														} ?>
															</div>
								<!-- </form> -->
							</div>
						</div>
							<?php	}
                            ?>
											
								

				<!-- ex one ends -->

						<!-- category starts from here -->
				<?php
                    if($catarray[0]!=""){
                    $query = mysqli_query($conn, "SELECT DISTINCT(t1.subcategory) as subcat FROM `product` as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0"><?php echo (!empty($catarray[0])) ? 'Subcategory' : 'Category'; ?></h4>
							<!-- <p class="mb-0">All</p> -->
							<p class="mb-0"><?php echo (!empty($catarray[1])) ? getSinglevalue('subcategory', 'subcat_key', $catarray[1], 3) : 'All'; ?></p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<!-- <form> -->
								<!-- <div class="scrolly"> -->
									
									<div class="scrolly">
									<div class="checkboxGroup my-lg-2">
										<p><a href="cat?cat=<?php echo $catarray[0] ?>-_<?php echo $urlarray[1] ?>_<?php echo $urlarray[2] ?>" <?php echo empty($catarray[1]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
									</div>
									<?php  while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-1">
											<p><a href="cat?cat=<?php echo $catarray[0] ?>-<?php echo $rows['subcat'] ?>_st!_<?php echo $urlarray[2] ?>" <?php echo ($catarray[1] == $rows['subcat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('subcategory', 'subcat_key', $rows['subcat'], 3); ?> 
											<!-- <span>(<?php // echo $rows['rc'] ?>)</span> -->
											<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
                                    <?php } ?>
									<?php }
									} ?>
										</div>
								<!-- </form> -->
							</div>
						</div>
							<?php	} ?>
											
								
						<!-- Category Ends here  -->

						<!-- brand starts here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b FROM product as a join product_attributes as t2 on a.sku=t1.p_id WHERE publish=1 AND subcategory='$catarray[1]' $pstatus $prf AND brand!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b FROM product as a join product_attributes as t2 on a.sku=t1.p_id WHERE publish=1 AND category='$catarray[0]' $pstatus $prf AND brand!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t1.brand) as b FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf AND t1.brand!=''");
							}
                                if($query){
                                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Brands</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php 
										while($rows = mysqli_fetch_assoc($query)){ 
									?>

									<div class="checkboxGroup my-lg-2">
										<input type="checkbox" aria-label="Checkbox for following text input" name="brand[]" value="<?php echo $rows['b'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
										<p class="mt-0">
											<a href="javascript:void(0)"><?=getSinglevalue('brands', 'brand_key', $rows['b'], 3); ?> 
												<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
											</a>
										</p>
									</div>

									<?php } ?>
									</div>
								</form>
							</div>
						</div>
						<?php }
                                }
                            
                            
                            ?>
						<!-- Brands Ends here -->

						<!-- color starts from here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.color!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.color!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.color!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Color</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="color[]" value="<?php echo $rows['color'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['color'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>
									</div>
								</form>
							</div>
						</div>
									<?php } } ?>
						<!-- color ends here -->

						<!-- Size Starts From Here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size, (SELECT unit FROM product_attributes WHERE pa_id=t2.pa_id) as unit FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.size!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size, (SELECT unit FROM product_attributes WHERE pa_id=t2.pa_id) as unit FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.size!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size, (SELECT unit FROM product_attributes WHERE pa_id=t2.pa_id) as unit FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.size!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Size</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="size[]" value="<?php echo $rows['size'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['size']." ".$rows['unit'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } }  ?>
     <!-- size ends here -->
     <!-- storage starts from here -->
     
     <?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.storage!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.storage!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.storage!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Storage</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="storage[]" value="<?php echo $rows['storage'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['storage'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } }  ?>
     
     <!-- storage ends from here -->

				<!-- price range starts from here -->
				<?php
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND subcategory='$catarray[1]'");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND category='$catarray[0]'");
                            }else{
								$query = mysqli_query($conn, "SELECT MIN(t1.price) as minprice, MAX(t1.price) as maxprice FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf");
							}
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
				<div class="accordion-toggle" style="visibility: hidden">
							<h4 class="mb-0">Rate</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
	<div id="rangeBox">    
         <div id="sliderBox" class="rangeslider">
             <input type="range" class="min" id="slider0to50" min="0" max="<?php echo $priceRows['maxprice'] / 2 ?>">
             <input type="range" class="max" id="slider51to100" min="<?php echo ($priceRows['maxprice'] / 2) + 1 ?>" max="<?php echo $priceRows['maxprice'] ?>">
         </div>
         <div id="inputRange" class="priceRangeslider">
             <input type="number" min="0" max="<?php echo $priceRows['maxprice'] / 2 ?>" value="<?php echo ceil($priceRows['minprice']) ?>" id="min" onkeyup="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
             <input type="number" min="<?php echo ($priceRows['maxprice'] / 2) + 1 ?>" max="<?php echo $priceRows['maxprice'] ?>" value="<?php echo ceil($priceRows['maxprice']) ?>" id="max" onkeyup="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
         </div>
	</div>

					
									</div>
									<?php    }
                                }else{
									echo mysqli_error($conn);
								}
                            ?>
					<!-- Price range ends here -->
					</div>
				</div>
			</div>
			<div class="col-lg-9 textTab">
				<div class="row justify-content-end">
					<div class="col-lg-12 col-6 mt-0 mt-md-2 mt-lg-4 mb-2 d-block d-lg-none">
						<div class="mobileFilterDiv">
							<a href="" data-toggle="modal" data-target="#exampleModalCenter">
								<p><img src="img/filter.svg" alt=""> <strong>Filter </strong><span><?php echo $prdrows; ?></span> result</p>
							</a>
						</div>
					</div>
					<div class="col-lg-12 col-6 text-right mt-0 mt-md-2 mt-lg-4 mb-2">
						<div class="categoryDropdown">
							<form>
							<select class="searchInput" id="sorting" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
							    <option selected value="0">Price Low to High</option>
							    <option value="1">Price High to Low</option>
							  </select>
							</form>
						</div>
					</div>
					<div class="col-12 my-4 d-none">
								<div class="closeTab">
									<p>Clothing</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing1</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing2</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing3</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing4</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing5</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing6</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
								<div class="closeTab">
									<p>Clothing7</p>
									<button type="button" class="closeBtn">
										<img src="img/close.svg" alt="">
									</button>
								</div>
					</div>
					<div class="col-12">
						<div class="row" id="grid">
						<?php 
                        if(!empty($product)){ 
                            echo $product;
                        }else{
                            echo $error;
                        }
                        ?>

							
						</div>
					</div>
				</div>
				<div class="loaderCenter text-center"  id="loaderOuterProduct" style="display: none">
				<div class="loaderOuter2 text-center">
					<div class="categoryLoader"></div>
					<p><strong>Loading.....</strong></p>
				</div>
				</div>
			</div>
					<?php }else{ ?>
						<div class="col-md-12 text-center noProduct"><p><strong>No products available in this category.</strong></p></div>
					<?php } ?>
		</div>
	</div>
</section>




















<!-- filter modal  -->
<!-- Modal -->
<div class="modal fade filterMobileModal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-3">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLongTitle">Filter</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
<?php if(!empty($product)){ ?>
				<div class="filterMainDiv mt-0">
					<p class="mb-0"><span class="pd-count"><?php echo $prdrows; ?></span> Results</p>
					<div class="accordion">
				<!-- ex one -->
				<?php
					// subcategory or category
                    if(!empty($prf)){
									$query = mysqli_query($conn, "SELECT DISTINCT(t1.category) as cat FROM `product` as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf");
									if($query){
										if(mysqli_num_rows($query) > 0){ ?>
											<div class="accordion-toggle">
												<h4 class="mb-0">Category</h4>
												<!-- <p class="mb-0">All</p> -->
												<p class="mb-0"><?php echo (!empty($catarray[0])) ? getSinglevalue('category', 'cat_key', $catarray[0], 2) : 'All'; ?></p>
											</div>
											<div class="accordion-content">
												<div class="refinedBrand">
													<!-- <form> -->
													<!-- <div class="scrolly"> -->
														
														<div class="scrolly">
														<div class="checkboxGroup my-lg-2">
															<p><a href="cat?cat=-_<?php echo $urlarray[1] ?>_<?php echo $urlarray[2] ?>" <?php echo empty($catarray[1]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
														</div>
														<?php  while($rows = mysqli_fetch_assoc($query)){ ?>
															<div class="checkboxGroup my-lg-1">
																<p><a href="cat?cat=<?php echo $rows['cat'] ?>-_st!_<?php echo $urlarray[2] ?>" <?php echo ($catarray[1] == $rows['cat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('category', 'cat_key', $rows['cat'], 2); ?> 
																<!-- <span>(<?php // echo $rows['rc'] ?>)</span> -->
																<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
															</div>
														<?php } ?>
														<?php }
														} ?>
															</div>
								<!-- </form> -->
							</div>
						</div>
							<?php	}
                            ?>
											
								

				<!-- ex one ends -->

						<!-- category starts from here -->
				<?php
                    if($catarray[0]!=""){
                    $query = mysqli_query($conn, "SELECT DISTINCT(t1.subcategory) as subcat FROM `product` as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf");
                    if($query){
                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0"><?php echo (!empty($catarray[0])) ? 'Subcategory' : 'Category'; ?></h4>
							<!-- <p class="mb-0">All</p> -->
							<p class="mb-0"><?php echo (!empty($catarray[1])) ? getSinglevalue('subcategory', 'subcat_key', $catarray[1], 3) : 'All'; ?></p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<!-- <form> -->
								<!-- <div class="scrolly"> -->
									
									<div class="scrolly">
									<div class="checkboxGroup my-lg-2">
										<p><a href="cat?cat=<?php echo $catarray[0] ?>-_<?php echo $urlarray[1] ?>_<?php echo $urlarray[2] ?>" <?php echo empty($catarray[1]) ? 'style="font-weight: bold"' : '' ; ?>>All</a></p>
									</div>
									<?php  while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-1">
											<p><a href="cat?cat=<?php echo $catarray[0] ?>-<?php echo $rows['subcat'] ?>_st!_<?php echo $urlarray[2] ?>" <?php echo ($catarray[1] == $rows['subcat']) ? 'style="font-weight: bold"' : ''; ?>><?php echo getSinglevalue('subcategory', 'subcat_key', $rows['subcat'], 3); ?> 
											<!-- <span>(<?php // echo $rows['rc'] ?>)</span> -->
											<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
                                    <?php } ?>
									<?php }
									} ?>
										</div>
								<!-- </form> -->
							</div>
						</div>
							<?php	} ?>
											
								
						<!-- Category Ends here  -->

						<!-- brand starts here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b FROM product as a join product_attributes as t2 on a.sku=t1.p_id WHERE publish=1 AND subcategory='$catarray[1]' $pstatus $prf AND brand!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(a.brand) as b FROM product as a join product_attributes as t2 on a.sku=t1.p_id WHERE publish=1 AND category='$catarray[0]' $pstatus $prf AND brand!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t1.brand) as b FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf AND t1.brand!=''");
							}
                                if($query){
                                    if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Brands</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>

<div class="checkboxGroup my-lg-2">
	<input type="checkbox" aria-label="Checkbox for following text input" name="brand[]" value="<?php echo $rows['b'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
	<p class="mt-0"><a href="javascript:void(0)"><?php echo getSinglevalue('brands', 'brand_key', $rows['b'], 3); ?> 
	<!-- <span>(<?php // echo $rows['brd'] ?>)</span> -->
	<span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
</div>

<?php } ?>
									</div>
								</form>
							</div>
						</div>
						<?php }
                                }
                            
                            
                            ?>
						<!-- Brands Ends here -->

						<!-- color starts from here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.color!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.color!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.color) as color FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.color!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Color</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">
									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="color[]" value="<?php echo $rows['color'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['color'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>
									</div>
								</form>
							</div>
						</div>
									<?php } } ?>
						<!-- color ends here -->

						<!-- Size Starts From Here -->
						<?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.size!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.size!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.size) as size FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.size!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Size</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="size[]" value="<?php echo $rows['size'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['size'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } }  ?>
     <!-- size ends here -->
     <!-- storage starts from here -->
     
     <?php 
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.subcategory='$catarray[1]' $pstatus $prf AND t2.storage!=''");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 AND t1.category='$catarray[0]' $pstatus $prf AND t2.storage!=''");
                            }else{
								$query = mysqli_query($conn, "SELECT DISTINCT(t2.storage) as storage FROM product_attributes as t2 JOIN product as t1 on t2.p_id=t1.sku WHERE t1.publish=1 $pstatus $prf AND t2.storage!=''");
							}
                                if($query){ 
									if(mysqli_num_rows($query) > 0){ ?>
						<div class="accordion-toggle">
							<h4 class="mb-0">Storage</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
							<div class="refinedBrand">
								<form>
									<div class="scrolly">

									<?php while($rows = mysqli_fetch_assoc($query)){ ?>
										<div class="checkboxGroup my-lg-2">
											<input type="checkbox" aria-label="Checkbox for following text input" name="storage[]" value="<?php echo $rows['storage'] ?>" onchange="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
											<p class="mt-0"><a href="javascript:void(0)"><?php echo $rows['storage'] ?></span><span class="float-right d-block d-lg-none"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></p>
										</div>
									<?php } ?>

									</div>
								</form>
							</div>
						</div>
					<?php } }  ?>
     
     <!-- storage ends from here -->

				<!-- price range starts from here -->
				<?php
                            if($catarray[1]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND subcategory='$catarray[1]'");
                            }elseif($catarray[0]!= ""){
                                $query = mysqli_query($conn, "SELECT MIN(price) as minprice, MAX(price) as maxprice FROM product WHERE publish=1 AND category='$catarray[0]'");
                            }else{
								$query = mysqli_query($conn, "SELECT MIN(t1.price) as minprice, MAX(t1.price) as maxprice FROM product as t1 join product_attributes as t2 on t1.sku=t2.p_id WHERE t1.publish=1 $pstatus $prf");
							}
                                if($query){
                                    if(mysqli_num_rows($query) > 0){
                                        $priceRows = mysqli_fetch_assoc($query); ?>
				<div class="accordion-toggle" style="visibility: hidden">
							<h4 class="mb-0">Rate</h4>
							<p class="mb-0">All</p>
						</div>
						<div class="accordion-content">
						<div id="rangeBox">    
							<div id="sliderBox" class="rangeslider">
								<input type="range" class="min" id="slider0to50" min="0" max="<?php echo $priceRows['maxprice'] / 2 ?>">
								<input type="range" class="max" id="slider51to100" min="<?php echo ($priceRows['maxprice'] / 2) + 1 ?>" max="<?php echo $priceRows['maxprice'] ?>">
							</div>
							<div id="inputRange" class="priceRangeslider">
								<input type="number" min="0" max="<?php echo $priceRows['maxprice'] / 2 ?>" value="<?php echo ceil($priceRows['minprice']) ?>" id="min" onkeyup="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
								<input type="number" min="<?php echo ($priceRows['maxprice'] / 2) + 1 ?>" max="<?php echo $priceRows['maxprice'] ?>" value="<?php echo ceil($priceRows['maxprice']) ?>" id="max" onkeyup="applyFilter('<?php echo $catarray[0] ?>', '<?php echo $catarray[1] ?>', '<?php echo $urlarray[2] ?>')">
							</div>
						</div>

					<!-- <div class="rangeslider">
                                <input class="min" name="range_1" type="range" min="1" max="100" value="10" />
                                <input class="max" name="range_1" type="range" min="1" max="100" value="90" />
                                <span class="range_min light left">10.000 €</span>
                                <span class="range_max light right">90.000 €</span>
                            </div> -->
									</div>
									<?php    }
                                }else{
									echo mysqli_error($conn);
								}
                            ?>
					<!-- Price range ends here -->


					<div class="applyBtn">
								<div class="row">
									<div class="col-6">
										<div class="clearFilterBtn">
											<a href="javascript;:" onclick="refreshPage()" data-dismiss="modal" aria-label="Close">Clear filters</a>
										</div>
									</div>
									<div class="col-6">
										<div class="applyBtnInn">
										<a href="javascript;:" data-dismiss="modal" aria-label="Close">Apply</a>
										</div>
									</div>
								</div>
					</div>
					</div>
				</div>
								
					<?php }?>
	
      </div>  
    </div>
  </div>
</div>

<?php include_once "helpers/footer.php"; ?>
