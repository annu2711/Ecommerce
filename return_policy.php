<?php include_once"helpers/index.php" ?>
<section class="middle_part py-5">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="heading w-100 text-center">
               <h2>Returns Policy: Electronics</h2>
            </div>
            <div class='main-cont'>
               <p><span style="font-size: small;">Returns Policy: Electronics</span></p>
               <p><span style="font-size: small;">Returns Policy makes it possible for you to return a product if you receive a damaged or defective product or if the product is significantly different from what was purchased.</span></p>
               <p><span style="font-size: small;">For all products, the policy on the product page shall prevail over the general returns policy.</span></p>
               <p><span style="font-size: small;">Please read all sections carefully to understand the conditions and cases under which returns will be accepted.</span></p>
               <table class="wishlist-table policy-table"  cellspacing="0" cellpadding="6" bgcolor="#ffffff">
                  <tbody>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="465">
                           <p class="table_font_family"><span style="font-size: small;">Products</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="176">
                           <p class="table_font_family"><span style="font-size: small;">Return window</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="465">
                           <p><span style="font-size: small;">Small &amp; Kitchen Appliances, Audio and Home Theatre Systems, Cameras, Computers and Laptops, Gaming, Head Phones and Speakers, Health &amp; Grooming, Smart Phones, Tablet, Wearable Tech</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="176">
                           <p><span style="font-size: small;">7 days from the date of delivery</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="465">
                           <p><span style="font-size: small;">Air Conditioners, Washing and Drying, Refrigerators, LED TV, Microwaves &amp; Purifiers</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="176">
                           <p><span style="font-size: small;">7 days from the date of installation</span></p>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <p class="table_font_family"><span style="font-size: small;">Section A: Return Window</span></p>
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
               <p>&nbsp;</p>
               <p class="table_font_family"><span style="font-size: small;">Section B: Return Reasons</span></p>
               <p><span style="font-size: small;">Product purchased on lSmart will be acceptable for returns if:</span></p>
               <p><span style="font-size: small;">Dead on arrival (DOA)</span></p>
               <p><span style="font-size: small;">Defective</span></p>
               <p><span style="font-size: small;">Wrong product received</span></p>
               <p><span style="font-size: small;">Used Product/Broken Seal</span></p>
               <p><span style="font-size: small;">Physical damage</span></p>
               <p>&nbsp;</p>
               <p><span style="font-size: small;">If you&rsquo;ve shopped on lSmart, here are the reasons for which returns are not allowed:</span></p>
               <p><span style="font-size: small;">Products that have already been used or installed (except installation done by brand authorized personnel in case of large appliances)</span></p>
               <p><span style="font-size: small;">In case product is relocated from delivery address for installation / demonstration by customer, it would not be eligble for return (in case of damaged&nbsp; / faulty etc.)</span></p>
               <p><span style="font-size: small;">Product you deem no longer in need or change of mind</span></p>
               <p><span style="font-size: small;">Software, Video Games (discs), Printer Cartridges</span></p>
               <p><span style="font-size: small;">Products that have been tampered with or are missing serial numbers / IMEIs</span></p>
               <p><span style="font-size: small;">Personalised/engraved items, as mandated by the seller</span></p>
               <p><span style="font-size: small;">If the product has heating or lagging issue at acceptable levels. Heating and lagging depend upon phone&rsquo;s specification. In such cases, you can visit brand service centre to get your phone checked.</span></p>
               <p><span style="font-size: small;">Return will not be accepted for subjective aspects like performance not as per expectation, colour shade difference etc.</span></p>
               <p><span style="font-size: small;">Dents, scratches on packaging not impacting the product performance do not qualify for returns</span></p>
               <p><span style="font-size: small;">Product becomes non-returnable if primary packaging is found open before installation.</span></p>
               <p><span style="font-size: small;">Returns are not possible in cases where the seller has mandated a no return policy, please refer to information on product page before making a purchase.</span></p>
               <p><span style="font-size: small;">If the request is outside return window</span></p>
               <p><span style="font-size: small;">If the Job sheet from authorised service centre is not provided for defective product</span></p>
               <p><span style="font-size: small;">If DOA certificate is not provided for dead on arrival products from brand authorised service centre</span></p>
               <p class="table_font_family"><span style="font-size: small;">Section C: Return Approval Process</span></p>
               <p><span style="font-size: small;">You can raise a return request from My Account section of your lSmart account.</span></p>
               <p><span style="font-size: small;">If you have purchased an electronic product, you will receive a call from our Returns Team within 2 business days of initiating a return request, to troubleshoot the issue you are facing.</span></p>
               <p><span style="font-size: small;">For Smartphones, we will send you a link to download to diagnose issue with defective smartphone and return will be approved basis the diagnostic result. In some cases, you may require to provide Job sheet from the brand authorised service centre.</span></p>
               <p><span style="font-size: small;">For Dead on Arrival complaint DOA certificate will be required from brand authorised service centre.</span></p>
               <p><span style="font-size: small;">For Appliances, we will arrange a brand authorised technician visit to determine the issue within 5-6 business days. Return will be accepted if the technician confirms the issue in writing on a Job Sheet. Please retain a copy of Job Sheet as it will be needed by us to process the return request.</span></p>
               <p><span style="font-size: small;">For all Damaged/Wrong product received complaints you will be required to provide following to investigate further:</span></p>
               <p><span style="font-size: small;">Damaged/Wrong product: Images 1) All sides of brand box 2) Image of damaged part 3) Serial number / IMEI printed on box and product both. For smartphones in working condition, we will request you to share the screen image of IMEI. (Dial *#06# to display IMEI)</span></p>
               <p><span style="font-size: small;">Our Returns Team will review the documents and pick-up will be arranged if the return reason complies with our policy and is approved by our Returns Team.</span></p>
               <p class="table_font_family"><span style="font-size: small;">Section D: Pick-Up</span></p>
               <p><span style="font-size: small;">We will pick-up your product as per below mentioned timelines once the return is approved.</span></p>
               <table class="wishlist-table policy-table" cellspacing="0" cellpadding="6" bgcolor="#ffffff">
                  <tbody>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="437">
                           <p class="table_font_family"><span style="font-size: small;">Products</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="195">
                           <p class="table_font_family"><span style="font-size: small;">Return Pick-Up Timelines</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="437">
                           <p><span style="font-size: small;">Small &amp; Kitchen Appliances, Audio and Home Theatre Systems, Cameras, Computers and Laptops, Gaming, Head Phones and Speakers, Health &amp; Grooming, Smart Phones, Tablet, Wearable Tech</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="195">
                           <p><span style="font-size: small;">3 business days</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="437">
                           <p><span style="font-size: small;">Air Conditioners, Washing and Drying, Refrigerators, LED TV, Microwaves &amp; Purifiers</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="195">
                           <p><span style="font-size: small;">7 business days</span></p>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <p>&nbsp;</p>
               <p>&nbsp;</p>
               <p><span style="font-size: small;">During pick-up, your product will be checked for the following conditions:</span></p>
               <table class="wishlist-table policy-table" cellspacing="0" cellpadding="6" bgcolor="#ffffff">
                  <tbody>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p class="table_font_family"><span style="font-size: small;">Check Point</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p class="table_font_family"><span style="font-size: small;">Conditions</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p><span style="font-size: small;">If Correct Product is returned</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p><span style="font-size: small;">IMEI/ name/ image/ brand/ serial number/ article number/ bar code should match and MRP tag should be un-detached and clearly visible.</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p><span style="font-size: small;">If Complete Product is returned</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p><span style="font-size: small;">All in-the-box accessories (like remote control, starter kits, instruction manuals, chargers, headphones, etc.), freebies and combos (if any) should be present.</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p><span style="font-size: small;">If product is Unused</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p><span style="font-size: small;">The product should be unused, unsoiled, without any stains and with non-tampered quality check seals/ warranty seals (wherever applicable). Before returning a Mobile/ Laptop/ Tablet, the device should be formatted and Screen Lock (Pin, Pattern or Fingerprint) must be disabled. iCloud lock must be disabled for Apple devices.</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p><span style="font-size: small;">If product is not damaged</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p><span style="font-size: small;">The product (including SIM trays/ charging port/ headphone port, back-panel etc.) should be undamaged and without any scratches, dents, tears or holes.</span></p>
                        </td>
                     </tr>
                     <tr>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="138">
                           <p><span style="font-size: small;">Packaging</span></p>
                        </td>
                        <td style="background: #ffffff;" bgcolor="#ffffff" width="497">
                           <p><span style="font-size: small;">Product's original packaging/ box is mandatory</span></p>
                           <p><span style="font-size: small;">We will not accept returns without packaging for all appliances, Laptops, Smartphones, Tablets, all accessories of value greater than Rs.2500</span></p>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <p><span style="font-size: small;">The pick-up executive will refuse to accept the return if any of the above conditions are not met.</span></p>
               <p><span style="font-size: small;">We will make 3 attempts to pick-up the product from your place post which the return request will be cancelled.</span></p>
               <p><span style="font-size: small;">In case of returns where you would like item(s) to be picked up from a different address, the address can only be changed if pick-up service is available at the new address.</span></p>
               <p class="table_font_family"><span style="font-size: small;">Section D: Refund</span></p>
               <p><span style="font-size: small;">We will take 48 hours to process the refund once the product has cleared the Quality Check. The refund amount will be credited to your bank account within 3-4 working days. In the case of certain public sector banks, it can take up to 10-15 working days</span></p>
               <p><span style="font-size: small;">If you have paid via Cash on Delivery (COD), a refund will be credited to the bank account provided by you at the time of initiation of returns</span></p>
               <p><span style="font-size: small;">If you have paid using a credit/debit card or via net banking, the refund will be credited back to the account you used to place the original order</span></p>
               <p><span style="font-size: small;">Amount paid through CLiQ Cash will credited back to CLiQ Cash account</span></p>
               <p><span style="font-size: small;">Note:</span></p>
               <p><span style="font-size: small;">Unfortunately, we don&rsquo;t have a replacement policy yet. We can&rsquo;t replace products that you&rsquo;ve already ordered. Instead, you can request a return and place a separate order for the new product(s) you want to order.</span></p>
               <p><span style="font-size: small;">All Products should be returned unused, in their original packaging along with the original price tags, labels, packing, barcodes, user manual, warranty card and invoices, accessories, freebies and original boxes defined as essentials.&nbsp;If any product is returned without the essentials, the product shall not be accepted for return and shall be sent back to you.</span></p>
               <p><span style="font-size: small;">For Appliances, please do not open the packing/box till the Brand Authorized technician visits your place for installation or demo.</span></p>
               <p><span style="font-size: small;">Kindly retain the packaging for 7-10 days of delivery as it may be needed just in case you need to return the product.</span></p>
               <p><span style="font-size: small;">In case of any missing accessory like charger, headphone, remote etc., we will investigate the issue with seller and our logistics partner. If it is found missing in our investigation, then we will either ship the missing product/s to you or will reimburse you the cost of missing product/s as per the applicable market price.</span></p>
               <p><br /> </p>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include_once"helpers/footer.php" ?>