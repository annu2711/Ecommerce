<?php include_once"helpers/index.php" ?>

<section class="middle_part py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="heading w-100 text-center">
							<h2>Careers</h2>
						</div>
                        <div class='main-cont'>
                            <h6>BEQUEST IS LOOKING FOR POSITIVE AND PROACTIVE PEOPLE TO JOIN THE TEAM.</h6>
                           <p>There is nothing more important than the people who work with us when it comes to building brands.</p>
                            <p>A job at Bequest Group is a career made by you, with development opportunities, benefits and a working culture that embraces diversity. So whether you're looking for an internship, graduate opportunities, or a job opening to progress your professional career, at Bequest Group you can shape your own path as you work with brands and people that drive our sustainable business growth.</p>
                            <p>With us you have the chance to advance and grow - Only you set the limits! There's never a better time to start your career with us than NOW!</p>
                        </div>
                    </div>
                </div>
            </div>
</section>

<?php include_once"helpers/footer.php" ?>